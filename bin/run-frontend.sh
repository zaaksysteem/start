#! /bin/bash
choice=$1

DC=$(which docker-compose)
[ -z "$DC" ] && DC="docker compose"

usage() {
    local rc=$1

    [ -z "$rc" ] && rc=0

    echo "Please supply a command: reset, gulp, styles, frontend, intern, wordaddin or init" >&2;
    exit $rc

}

start_frontend() {
    $DC restart frontend-frontendclient
}

intern() {
    $DC exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=intern npm start"
}

wordaddin() {
    $DC exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/client && CLIENT_APP=wordaddin npm start"
}

frontend() {
    $DC exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-wp"
}

gulp() {
    $DC exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-gulp"
}

styles() {
    $DC exec -T frontend-frontendclient bash -c "cd /opt/zaaksysteem/frontend && npm run start-gulp-styles"
}

case $choice in
    init)
        start_frontend
        $DC exec -T frontend-frontendclient \
            /opt/zaaksysteem/dev-bin/npm_container.sh
        ;;
    intern)
        intern
        ;;
    wordaddin)
        wordaddin
        ;;
    frontend)
        frontend
        ;;
    gulp)
        gulp
        ;;
    styles)
        styles
        ;;
    dev)
        start_frontend
        intern & wordaddin & frontend & gulp &
        ;;
    reset)
        start_frontend
        $DC exec -T frontend-frontendclient \
            /opt/zaaksysteem/dev-bin/reset-frontend.sh
        ;;
    *) usage 1;;

esac



