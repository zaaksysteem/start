#! /bin/bash

docker-compose run --no-deps --rm frontend-frontendclient \
    cat /etc/nginx/ssl/ca.crt \
    | tee dev.zaaksysteem.nl.pem

echo "CA certificate for dev environment copied to 'dev.zaaksysteem.nl.pem'"
