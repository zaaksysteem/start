{{/*
Service account template
*/}}
{{- define "default.serviceaccount" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if and $.Values.global.serviceAccount.create $app.enabled -}}
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "serviceAccountName" (list $ $app.name) }}
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
  annotations:
    {{- $.Values.global.serviceAccount.annotations | toYaml | nindent 4 }}
    {{- if $app.serviceAccount }}
    {{- $app.serviceAccount.annotations | toYaml | nindent 4 }}
    {{- end }}
{{- end }}
{{- end }}
