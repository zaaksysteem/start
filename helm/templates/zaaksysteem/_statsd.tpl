{{- define "statsd.volume" -}}
{{- if .Values.global.statsd.enabled -}}
- name: statsd
  configMap:
    defaultMode: 0444
    name: {{ include "zaaksysteem.fullname" . }}-statsd
{{- end }}
{{- end }}

{{- define "statsd.service" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
apiVersion: v1
kind: Service
metadata:
  name: {{ include "fullname" (list $ $app.name) }}-metrics
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: metrics
      port: 9102
      targetPort: metrics
      protocol: TCP
  selector:
    {{- include "selectorLabels" (list $ $app.name) | nindent 4 }}
{{- end }}

{{- define "statsd.container" -}}
{{- if .Values.global.statsd.enabled -}}
- name: statsd
  image: "{{ .Values.global.statsd.image.repository }}:{{ .Values.global.statsd.image.tag }}"

  securityContext:
    capabilities:
      drop:
        - ALL
    readOnlyRootFilesystem: true
    allowPrivilegeEscalation: false

  command:
    - statsd_exporter
    - --log.format=json
    - --no-statsd.parse-dogstatsd-tags
    - --no-statsd.parse-librato-tags
    - --no-statsd.parse-signalfx-tags
    - --statsd.listen-udp=:8125
    - --statsd.listen-tcp=
    - --web.listen-address=:9102
    - --statsd.mapping-config=/etc/zaaksysteem/mappings.yaml

  ports:
    - name: statsd-udp
      containerPort: 8125
      protocol: UDP
    - name: metrics
      containerPort: 9102
      protocol: TCP

  volumeMounts:
    - name: statsd
      mountPath: /etc/zaaksysteem/mappings.yaml
      subPath: mappings.yaml

  livenessProbe:
    initialDelaySeconds: {{ .Values.global.statsd.livenessProbe.initialDelaySeconds }}
    timeoutSeconds: {{ .Values.global.statsd.livenessProbe.timeoutSeconds }}
    periodSeconds: {{ .Values.global.statsd.livenessProbe.periodSeconds }}
    failureThreshold: {{ .Values.global.statsd.livenessProbe.failureThreshold }}
    httpGet:
      path: "/-/health"
      port: 9102

  readinessProbe:
    initialDelaySeconds: {{ .Values.global.statsd.readinessProbe.initialDelaySeconds }}
    timeoutSeconds: {{ .Values.global.statsd.readinessProbe.timeoutSeconds }}
    periodSeconds: {{ .Values.global.statsd.readinessProbe.periodSeconds }}
    httpGet:
      path: "/-/health"
      port: 9102
{{- end }}
{{- end }}