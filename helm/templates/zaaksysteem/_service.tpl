{{/*
Service
*/}}
{{- define "default.service" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if $app.enabled -}}
apiVersion: v1
kind: Service
metadata:
  name: {{ $app.name }}
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
spec:
  type: {{ $app.service.type }}
  ports:
    - port: {{ $app.service.port }}
      targetPort: {{ $app.service.targetPort | default "http" }}
      protocol: TCP
      name: {{ $app.service.name | default "http" }}
  {{- if $app.service.isHeadless }}
  {{/* Headless services, used for StatefulSets */}}
  clusterIP: None
  {{- end }}
  selector:
    {{- include "selectorLabels" (list $ $app.name) | nindent 4 }}
{{- end }}
{{- end }}
