{{- if .Values.backend.virus_scanner.enabled -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ include "fullname" (list . .Values.backend.virus_scanner.name) }}"
  labels:
    app.kubernetes.io/name: {{ .Values.backend.virus_scanner.name }}
    app.kubernetes.io/instance: {{ .Release.Name }}
data:
  log4perl.conf: |
    {{- include "zaaksysteem.log4perl.conf" (list . .Values.backend.virus_scanner) | nindent 4 }}
  virus_scanner.conf: |
    <Zaaksysteem::Service::HTTP>
        request_handler_namespace = Zaaksysteem::VirusScanner::Service::RequestHandlers
        callback_template = http://{{ .Values.backend.perl_api_internal.name }}:{{ .Values.backend.perl_api_internal.service.port }}/api/v1/file/{file_id}/virus_scan_status
        download_template = http://{{ .Values.backend.perl_api_internal.name }}:{{ .Values.backend.perl_api_internal.service.port }}/api/v1/file/{file_id}/download
        platform_key = {{ .Values.global.platform_key }}
    </Zaaksysteem::Service::HTTP>
  clamd.conf: |
    StreamMaxLength 2048M
    PidFile /tmp/clamd.pid
    LocalSocket /tmp/clamd.sock
    TCPSocket 3310
    User clamav
    LogFile /dev/stdout
    LogTime yes
    LogFileUnlock yes
    ConcurrentDatabaseReload no

  freshclam.conf: |
    UpdateLogFile /dev/stdout
    PidFile /tmp/freshclam.pid
    DatabaseOwner clamav
    DatabaseMirror http://clamav-mirror.zaaksysteem-clamav-mirror.svc.cluster.local:9083
    ScriptedUpdates yes
    NotifyClamd /etc/clamav/clamd.conf

---
# This is a temporary copy of the "unprivileged" /init script for ClamAV, until
# ClamAV releases proper unprivileged(-capable) docker images.
# https://github.com/Cisco-Talos/clamav/issues/478
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ include "fullname" (list . .Values.backend.virus_scanner.name) }}-init"
  labels:
    app.kubernetes.io/name: {{ .Values.backend.virus_scanner.name }}
    app.kubernetes.io/instance: {{ .Release.Name }}
data:
  init: |
    #!/sbin/tini /bin/sh
    # SPDX-License-Identifier: GPL-2.0-or-later
    #
    # Copyright (C) 2021 Olliver Schinagl <oliver@schinagl.nl>
    # Copyright (C) 2021-2022 Cisco Systems, Inc. and/or its affiliates. All rights reserved.
    #
    # A beginning user should be able to docker run image bash (or sh) without
    # needing to learn about --entrypoint
    # https://github.com/docker-library/official-images#consistency

    set -eu

    # run command if it is not starting with a "-" and is an executable in PATH
    if [ "${#}" -gt 0 ] && \
    [ "${1#-}" = "${1}" ] && \
    command -v "${1}" > "/dev/null" 2>&1; then
        # Ensure healthcheck always passes
        CLAMAV_NO_CLAMD="true" exec "${@}"
    else
        if [ "${#}" -ge 1 ] && \
        [ "${1#-}" != "${1}" ]; then
            # If an argument starts with "-" pass it to clamd specifically
            exec clamd "${@}"
        fi
        # else default to running clamav's servers

        # Ensure we have some virus data, otherwise clamd refuses to start
        if [ ! -f "/var/lib/clamav/main.cvd" ]; then
            echo "Updating initial database"
            freshclam --foreground --stdout
        fi

        if [ "${CLAMAV_NO_FRESHCLAMD:-false}" != "true" ]; then
            echo "Starting Freshclamd"
            freshclam \
                    --checks="${FRESHCLAM_CHECKS:-1}" \
                    --daemon \
                    --foreground \
                    --stdout \
                    --user="clamav" \
                &
        fi

        if [ "${CLAMAV_NO_CLAMD:-false}" != "true" ]; then
            echo "Starting ClamAV"
            if [ -S "/tmp/clamd.sock" ]; then
                unlink "/tmp/clamd.sock"
            fi
            clamd --foreground &
            while [ ! -S "/tmp/clamd.sock" ]; do
                if [ "${_timeout:=0}" -gt "${CLAMD_STARTUP_TIMEOUT:=1800}" ]; then
                    echo
                    echo "Failed to start clamd"
                    exit 1
                fi
                printf "\r%s" "Socket for clamd not found yet, retrying (${_timeout}/${CLAMD_STARTUP_TIMEOUT}) ..."
                sleep 1
                _timeout="$((_timeout + 1))"
            done
            echo "socket found, clamd started."
        fi

        if [ "${CLAMAV_NO_MILTERD:-true}" != "true" ]; then
            echo "Starting clamav milterd"
            clamav-milter &
        fi

        # Wait forever (or until canceled)
        exec tail -f "/dev/null"
    fi

    exit 0
{{- end }}