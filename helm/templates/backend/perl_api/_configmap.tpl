{{ define "perl_api.configmap" }}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- if $app.enabled -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ include "fullname" (list $ $app.name) }}"
  labels:
    app.kubernetes.io/name: {{ $app.name }}
    app.kubernetes.io/instance: {{ $.Release.Name }}
data:
  log4perl.conf: |
    {{- include "zaaksysteem.log4perl.conf" (list $ $app) | nindent 4 }}
{{- end }}
{{- end }}