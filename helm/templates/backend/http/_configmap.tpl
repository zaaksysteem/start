{{/*
Configmap template
*/}}
{{- define "http.configmap" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
{{- $pyramid_params := index . 2 }}
{{- if $app.enabled -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ include "fullname" (list $ $app.name) }}"
  labels:
    app.kubernetes.io/name: {{ $app.name }}
    app.kubernetes.io/instance: {{ $.Release.Name }}
data:
  production.ini: |
{{- include "zaaksysteem.pyramid.production.ini" $pyramid_params | nindent 4 }}

{{- include "zaaksysteem.python.logging.conf" (dict "logLevel" $.Values.global.logLevel) | nindent 4 }}
{{- end }}
{{- end }}