{{- if .Values.backend.queue_cleaner.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" (list . .Values.backend.queue_cleaner.name) }}
  labels:
    {{- include "common.labels" (list . .Values.backend.queue_cleaner.name) | nindent 4 }}
spec:
  # Queue cleaner should only be running once.
  # If multiple copies run at once, they interfere with each other.
  replicas: 1
  strategy:
    {{- .Values.backend.queue_cleaner.strategy | default .Values.global.strategy | toYaml | nindent 4 }}
  selector:
    matchLabels:
      {{- include "selectorLabels" (list . .Values.backend.queue_cleaner.name) | nindent 6 }}
  template:
    metadata:
      {{- with .Values.global.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "selectorLabels" (list . .Values.backend.queue_cleaner.name) | nindent 8 }}
    spec:
      {{- with .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "serviceAccountName" (list . .Values.backend.queue_cleaner.name) }}
      securityContext:
        {{- include "zaaksysteem.default_pod_security_context" . | nindent 8 }}

      volumes:
        - name: zaaksysteem-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" . }}
        - name: instance-config
          configMap:
            defaultMode: 0444
            name: {{ include "zaaksysteem.fullname" . }}-customerd
        - name: tmp
          emptyDir: {}
        {{- include "statsd.volume" . | nindent 8 }}

      containers:
        - name: {{ .Values.backend.queue_cleaner.name }}
          securityContext:
            {{- include "zaaksysteem.default_security_context" . | nindent 12 }}

          image: "{{ .Values.backend.queue_cleaner.image.repository }}:{{ .Values.backend.queue_cleaner.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}

          env:
            - name: TELEGRAF_HOST
              value: localhost
            - name: TELEGRAF_PORT
              value: "8125"
            - name: API_HOSTNAME
              value: "{{ .Values.backend.perl_api_internal.name }}:{{ .Values.backend.perl_api_internal.service.port }}"

          command:
            - bash
            - ./run_interval
            - ./queue_cleanup
            - "-p"
            - "{{ .Values.backend.queue_cleaner.parallel_count | default 5 }}"
            - "-i"
            - "{{ .Values.backend.queue_cleaner.interval_minutes | default 15 }}"
            - "-s"
            - "{{ .Values.global.instance_config_type }}"
            - "-c"
            {{- if eq .Values.global.instance_config_type "file" }}
            - "/etc/zaaksysteem/customer.d"
            {{- else if eq .Values.global.instance_config_type "redis" }}
            - "{{ .Values.global.redis.configuration.host }}:{{ .Values.global.redis.configuration.port }}"
            {{- end }}

          volumeMounts:
            - name: zaaksysteem-config
              mountPath: /etc/zaaksysteem/zaaksysteem.conf
              subPath: zaaksysteem.python.conf
            - name:  tmp
              mountPath: /tmp
            {{- if eq .Values.global.instance_config_type "file" }}
            - name: instance-config
              mountPath: /etc/zaaksysteem/customer.d
            {{- end }}

          resources:
            {{- toYaml .Values.backend.queue_cleaner.resources | nindent 12 }}

        {{- include "statsd.container" . | nindent 8 }}

      {{- with .Values.backend.queue_cleaner.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        {{- include "zaaksysteem.affinity" (dict "name" (.Values.backend.queue_cleaner.name) "Release" $.Release) | nindent 8 }}
      {{- with .Values.backend.queue_cleaner.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}