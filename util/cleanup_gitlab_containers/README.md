# cleanup-gitlab-containers

This tooling is used to remove old containers from the gitlab container registry.

All repos that wille be cleaned are listed in `repositories_to_clean.txt`

Requirements:
Python >= 3.11

regctl is used to remove tags. This is one of the few tools that is able to remove
a tag without removing the image and all other tags. For more information see
<https://github.com/regclient>

To run this script local set the following env variables:

```bash
export CI_REGISTRY=registry.gitlab.com
```

```bash
export CI_PROJECT_PATH=xxllnc/zaakgericht/zaken/start
```

```bash
export CI_REGISTRY_USER=<user> 
```

```bash
export CI_REGISTRY_PASSWORD=<token>
```

Create a venv and activate it:

```bash
cd util/cleanup_gitlab_containers
python3 -m venv env
source env/bin/activate
```

Then install the dependencies in the activated venv:

```bash
pip install -r requirements.txt
```

Run the script locally with the following command:

```bash
python cleanup_gitlab_containers.py
```

To actually delete the tags add the option --delete-tags

```bash
python cleanup_gitlab_containers.py --delete-tags
```

Set log level to debug

```bash
export LOG_LEVEL=DEBUG
```

## gitlab scheduled job

The clean-up script is rune every day from a scheduled gitlab pipeline.
It is possible to trigger the job manually and set the logging to debug.
To get the debug logging add a variable `LOG_LEVEL` with the value `DEBUG`
when triggering the schedule in Gitlab.
