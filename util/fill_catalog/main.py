import boto3
from zsapi import SCRIPT_DIR, TODAY
from zsapi.database import Database
import logging
import argparse
from queue import Queue
from threading import Thread
from pathlib import Path
import json

logger = logging.getLogger(__name__)
database_credentials_path = SCRIPT_DIR / "credentials.json"

TEMPLATES = {
    "zaaksysteem-gov": {
        "database": "accept_production_4a7bc4",
        "filestore": "zs-accept_production_4a7bc4",
        "bucket": "gov-zaaksysteem-20230614092552890600000002",
    },
    "zaaksysteem-com": {
        "database": "accept_development_ee748b",
        "filestore": "accept_development_ee748b",
        "bucket": "com-zaaksysteem-20230616060842032000000003",
    },
}


def parse_arguments() -> argparse.Namespace:
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "-n",
        "--namespace",
        required=True,
        choices=["zaaksysteem-gov", "zaaksysteem-com"],
        help="Namespace where environment lives",
    )
    argument_parser.add_argument(
        "-d", "--database", required=True, help="Database of the environment to fill"
    )
    argument_parser.add_argument(
        "-f", "--filestore", help="filestore name of the environment to fill"
    )
    argument_parser.add_argument(
        "-c",
        "--cleanup",
        help="Cleanup database copy created during filling of the catalog",
        action="store_true",
    )
    return argument_parser.parse_args()


def get_database_credentials_from_file(path: Path, namespace: str) -> dict:
    with open(path, "r") as f:
        creds_dict = json.load(f)
        return creds_dict.get(namespace)


def ignore_loggers() -> None:
    loggers_to_ignore = [
        "boto3",
        "botocore",
        "urllib3",
        "s3transfer",
    ]
    for logger_name in loggers_to_ignore:
        _logger = logging.getLogger(logger_name)
        _logger.setLevel(logging.WARNING)


def _list_objects(bucket: str, prefix: str, start_after=None) -> tuple[list, int]:
    s3 = boto3.resource("s3").meta.client
    try:
        if start_after:
            objects_list = s3.list_objects_v2(
                Bucket=bucket, Prefix=prefix, StartAfter=start_after
            )["Contents"]
        else:
            objects_list = s3.list_objects_v2(Bucket=bucket, Prefix=prefix)["Contents"]
        object_count = len(objects_list)
        logger.info(f"Got {object_count} for bucket {bucket}, prefix {prefix}")
    except KeyError:
        logger.error(f"Did not get any objects for bucket {bucket} and prefix {prefix}")
        return [], 0
    return [_obj["Key"] for _obj in objects_list], object_count


def gather_objects(bucket: str, prefix: str) -> list[str]:
    object_keys, object_count = _list_objects(bucket, prefix)
    last_object_key = f"{prefix}/{object_keys[-1]}"
    while object_count == 1000:
        extra_objects, object_count = _list_objects(
            bucket, prefix, start_after=last_object_key
        )
        object_keys.extend(extra_objects)
    return object_keys


def copy_worker(
    worker_id: int, object_queue: Queue, template_bucket: str, filestore: str
) -> None:
    s3 = boto3.resource("s3").meta.client
    files_copied = 0
    while not object_queue.empty():
        key = object_queue.get()
        copy_source = {
            "Bucket": template_bucket,
            "Key": key,
        }
        new_key = key.split('/')[-1]
        new_path = f"{filestore}/{new_key}"
        s3.copy(copy_source, template_bucket, new_path)
        files_copied += 1
    logger.info(f"Worker-{worker_id}: Copied {files_copied} files to {filestore}")


def copy_s3_objects(
    object_list: list[str], template_bucket: str, filestore: str
) -> None:
    object_queue = Queue()
    for key in object_list:
        object_queue.put(key)

    worker_count = 25
    threads: list[Thread] = []
    for worker_id in range(worker_count):
        thread = Thread(
            target=copy_worker,
            args=(worker_id, object_queue, template_bucket, filestore),
        )
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()


def copy_database(template_database: str, namespace: str, database: str) -> None:
    database_credentials = get_database_credentials_from_file(
        database_credentials_path, namespace
    )
    copy_database_name = f'backup_{database}_{TODAY.replace("-", "_")}'
    with Database(database_credentials) as db:
        db.connection.autocommit = True
        rename_query = f"""
ALTER DATABASE {database} RENAME TO {copy_database_name};
"""
        database_copy_query = f"""
CREATE DATABASE {database} TEMPLATE {template_database};
"""
        db.run_query(rename_query)
        db.run_query(database_copy_query)
        logger.info(
            f"Renamed original database to {copy_database_name} and created new database from {template_database}"
        )
        logger.warning(
            f"DON'T FORGET TO CLEANUP DATABASE COPY {copy_database_name} AFTER TESTING IF ENVIRONMENT STILL WORKS:\npython3 main.py -c -d {copy_database_name} -n {namespace}"
        )


def cleanup_database_copy(database_copy_name: str, namespace: str) -> None:
    database_credentials = get_database_credentials_from_file(
        database_credentials_path, namespace
    )
    with Database(database_credentials) as db:
        db.connection.autocommit = True
        drop_query = f"""DROP DATABASE {database_copy_name};"""
        try:
            drop_databsae = input(
                f"YOU ARE ABOUT TO DROP THIS DATABASE: {database_copy_name}, ARE YOU SUPER SURE? Type YES: "
            )
        except KeyboardInterrupt:
            logger.info("Quiting on user request")
            exit(0)
        if drop_databsae == "YES":
            db.run_query(drop_query)
            logger.info(f"Dropped database: {database_copy_name}")


def main():
    ignore_loggers()
    arguments = parse_arguments()
    namespace = arguments.namespace
    database = arguments.database
    if arguments.cleanup:
        cleanup_database_copy(database, namespace)
        return
    filestore = arguments.filestore
    template_database = TEMPLATES[namespace]["database"]
    template_filestore = TEMPLATES[namespace]["filestore"]
    template_bucket = TEMPLATES[namespace]["bucket"]

    objects_to_copy = gather_objects(template_bucket, template_filestore)
    copy_s3_objects(objects_to_copy, template_bucket, filestore)
    copy_database(template_database, namespace, database)


if __name__ == "__main__":
    main()
