import boto3
from zsapi import SCRIPT_DIR
from zsapi.database import Database, LocalDatabase
from pathlib import Path
import json
import logging
from queue import Queue
from threading import Thread
import argparse
import time

argument_parser = argparse.ArgumentParser()
argument_parser.add_argument(
    "-p", "--prefix", help="prefix of files to move", required=True
)
arguments = argument_parser.parse_args()

logger = logging.getLogger(__name__)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

databases_config_path = SCRIPT_DIR / "databases.json"
local_database_path = SCRIPT_DIR / "files.db"
database_filestore_map_path = SCRIPT_DIR / "database_filestore_map.json"
skip_path = SCRIPT_DIR / "skip.json"
buckets = {
    "zaaksysteem-gov": "gov-zaaksysteem-20230614092552890600000002",
    "zaaksysteem-com": "com-zaaksysteem-20230616060842032000000003",
}
NAMESPACES = ["zaaksysteem-gov", "zaaksysteem-com"]


def load_skip_list(path: Path) -> dict:
    with open(path, "r") as f:
        return json.load(f)


skip_list = load_skip_list(skip_path)


def get_database_credentials_from_file(path: Path, namespace: str) -> dict:
    with open(path, "r") as f:
        creds = json.load(f)
        return creds[namespace]


def save_database_filestore_map(path: Path) -> None:
    database_filestore_mapping_query = """
select
    index_hstore :: hstore -> 'database' as database,
    index_hstore :: hstore -> 'filestore' as filestore,
    index_hstore :: hstore -> 'fqdn' as fqdn,
    uuid
from
    object_data
where
    object_class = 'instance';
"""
    database_filestore_map = {}
    mintlab_credentials = get_database_credentials_from_file(
        databases_config_path, "zaaksysteem-mlb"
    )
    with Database(
        mintlab_credentials, database_name="zaaksysteem_mintlab"
    ) as mintlab_databae:
        for database, filestore, fqdn, uuid in mintlab_databae.run_query(
            database_filestore_mapping_query
        )[0]:
            if database:
                database_filestore_map[database] = filestore
    with open(path, "w") as f:
        json.dump(database_filestore_map, f, indent=2)


def load_database_filestore_map(path: Path) -> dict:
    with open(path, "r") as f:
        return json.load(f)


def cache_filestore_rows() -> None:
    database_filestore_map = load_database_filestore_map(database_filestore_map_path)
    create_files_table_query = """
CREATE TABLE IF NOT EXISTS files 
(
    uuid TEXT PRIMARY KEY, 
    database_name TEXT NOT NULL, 
    filestore TEXT NOT NULL,
    copied INTEGER DEFAULT 0, 
    deleted INTEGER DEFAULT 0
    );
"""
    get_filestore_uuids_query = """select uuid from filestore where date_created > '2023-11-01 00:00:00.000000';"""
    insert_query = (
        """INSERT INTO files (uuid, database_name, filestore) VALUES (?, ?, ?);"""
    )
    with LocalDatabase(local_database_path) as local_database:
        local_database.run_query(create_files_table_query)
        for namespace in NAMESPACES:
            database_credentials = get_database_credentials_from_file(
                databases_config_path, namespace
            )
            with Database(database_credentials) as template1:
                database_names = template1.list_databases()
                logger.info(f"Got {len(database_names)} to process for {namespace}")
            for database_name in database_names:
                with Database(
                    database_credentials, database_name=database_name
                ) as database:
                    filestore = database_filestore_map.get(database_name)
                    logger.info(f"Got {filestore} for {database_name}")
                    rows_to_insert: list[tuple[str, str, str]] = [
                        (row[0], database_name, filestore)
                        for row in database.run_query(get_filestore_uuids_query)[0]
                    ]
                    logger.info(
                        f"Got {len(rows_to_insert)} rows to insert for {database_name}:{filestore}"
                    )
                    local_database.run_many(insert_query, rows_to_insert)


def move_file(worker_id: int, key: str, filestore: str, bucket: str) -> None:
    s3 = boto3.resource("s3")
    client = s3.meta.client
    new_path = f"{filestore}/{key}"
    copy_source = {"Bucket": bucket, "Key": key}
    copy_timer_start = time.perf_counter()
    client.copy(copy_source, bucket, new_path)
    copy_timer_end = time.perf_counter()
    logger.info(
        f"Worker-{worker_id}: Copied {key} to {new_path} in {(copy_timer_end - copy_timer_start):.2f} seconds"
    )
    version_id_timer_start = time.perf_counter()
    version_id = client.get_object(Bucket=bucket, Key=key)["VersionId"]
    version_id_timer_end = time.perf_counter()
    logger.info(
        f"Worker-{worker_id}: VersionId for {key}: {version_id} in {(version_id_timer_end - version_id_timer_start):.2f} seconds"
    )
    delete_timer_start = time.perf_counter()
    s3.meta.client.delete_object(Bucket=bucket, Key=key, VersionId=version_id)
    delete_timer_end = time.perf_counter()
    logger.info(
        f"Worker-{worker_id}: Deleted {key} {version_id} from {bucket} in {(delete_timer_end - delete_timer_start):.2f} seconds"
    )


def worker(worker_id: int, object_queue: Queue):
    logger.debug(f"Worker-{worker_id}: Starting work")
    objects_moved = 0
    with LocalDatabase(local_database_path) as local_database:
        while not object_queue.empty():
            key, bucket = object_queue.get()
            if key in skip_list:
                continue
            try:
                (
                    uuid,
                    database_name,
                    filestore,
                    copied,
                    deleted,
                ) = local_database.run_query(
                    f"SELECT * FROM files WHERE uuid = '{key}';"
                )[0]
            except IndexError:
                logger.warning(
                    f"Worker-{worker_id}: {key} not found in local database, might be a directory. Skipping"
                )
                skip_list[key] = True
                continue
            if copied == 1 or deleted == 1:
                logger.error(
                    f"Worker-{worker_id}: Local database claims that object {key} already was copied: {copied} or deleted: {deleted}, skipping object"
                )
                continue
            if filestore == "gemeenscha_accept_e6fde6":
                logger.info(
                    f"Worker-{worker_id}: {key} belongs to gemeenscha_accept_e6fde6 skipping.."
                )
                continue
            move_file(worker_id, key, filestore, bucket)
            local_database.run_query(
                f"UPDATE files SET copied=1, deleted=1 WHERE uuid = '{key}';"
            )
            objects_moved += 1
    logger.info(f"Worker-{worker_id}: Finished work on {objects_moved} objects")


def main():
    if not database_filestore_map_path.exists():
        save_database_filestore_map(database_filestore_map_path)
    amount_of_cached_rows_query = "SELECT COUNT(*) FROM files;"
    with LocalDatabase(local_database_path) as local_database:
        cached_rows = local_database.run_query(amount_of_cached_rows_query)[0][0]
        logger.info(f"Got {cached_rows} cached rows in local database")
        if cached_rows == 0:
            cache_filestore_rows()

        for namespace in NAMESPACES:
            bucket = buckets[namespace]
            s3 = boto3.resource("s3")
            objects_count = 1
            skipped = 0
            while objects_count > 0:
                batch_counter_start = time.perf_counter()
                try:
                    objects: list[dict] = s3.meta.client.list_objects_v2(
                        Bucket=bucket, Prefix=arguments.prefix
                    )["Contents"]
                except KeyError:
                    logger.warning("No objects returned, exiting..")
                    exit(0)
                objects_count = len(objects)
                if skipped == objects_count:
                    logger.warning(
                        f"Got {objects_count} objects back and skipped {skipped} objects so exiting.."
                    )
                    exit(1)
                logger.info(f"Moving {objects_count} objects for bucket: {bucket}")
                object_queue = Queue()
                for _object in objects:
                    key = _object["Key"]
                    if key in skip_list:
                        logger.warning(f"Encountered an already tried object: {key}")
                        skipped += 1
                    object_queue.put((key, bucket))

                worker_count = objects_count if objects_count < 150 else 150
                logger.info(
                    f"Launching {worker_count} workers for {objects_count} objects"
                )
                threads: list[Thread] = []
                for worker_id in range(worker_count):
                    thread = Thread(target=worker, args=(worker_id, object_queue))
                    threads.append(thread)
                    thread.start()

                for thread in threads:
                    thread.join()

                with open(skip_path, "w") as f:
                    json.dump(skip_list, f, indent=2)
                batch_counter_end = time.perf_counter()
                batch_time = batch_counter_end - batch_counter_start
                logger.warning(
                    f"batch of {objects_count} took {batch_time:.2f} seconds to complete"
                )
                logger.warning(
                    f"That is {(batch_time / objects_count):.2f} seconds per object"
                )


if __name__ == "__main__":
    main()
