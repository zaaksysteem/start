import logging
from zsapi import SCRIPT_DIR
from zsapi.database import Database, LocalDatabase
from cleanup_buckets import (
    get_database_credentials_from_file,
    databases_config_path,
    load_database_filestore_map,
    database_filestore_map_path,
)
from get_objects import local_info_db_path
from threading import Thread
from queue import Queue
from datetime import datetime, timedelta

databases_queue = Queue()
logger = logging.getLogger(__name__)
cache_db_path = SCRIPT_DIR / "cache.db"
database_credentials = get_database_credentials_from_file(
    databases_config_path, "zaaksysteem-gov"
)
database_filestore_map = load_database_filestore_map(database_filestore_map_path)


INSERT_QUERY = """REPLACE INTO cache (uuid, database_name) VALUES (?, ?);"""

type LastModified = str
type Query = str


def get_filestore_uuids_query(last_modified: LastModified) -> Query:
    last_modified_datetime = datetime.strptime(
        last_modified, "%Y-%m-%dT%H:%M:%S.000000Z"
    )
    lower = last_modified_datetime - timedelta(days=1)
    higher = last_modified_datetime + timedelta(days=1)
    lower_str = lower.strftime("%Y-%m-%d %H:%M:%S.000000")
    higher_str = higher.strftime("%Y-%m-%d %H:%M:%S.000000")
    return f"""
select 
    uuid 
from 
    filestore
where
    date_created > '{lower_str}'
AND
    date_created < '{higher_str}';
"""


def worker(worker_id: int, last_modified_query: Query) -> None:
    with LocalDatabase(cache_db_path) as cache_db:
        while not databases_queue.empty():
            database_name = databases_queue.get()
            with Database(database_credentials, database_name=database_name) as db:
                uuids_database = [
                    (row[0], database_name)
                    for row in db.run_query(last_modified_query)[0]
                ]
                logger.info(
                    f"Worker-{worker_id}: Got {len(uuids_database)} rows for {database_name}"
                )
                cache_db.run_many(INSERT_QUERY, uuids_database)


def main():
    with Database(database_credentials) as template1:
        database_names = template1.list_databases()
        for database_name in database_names:
            databases_queue.put(database_name)

    with LocalDatabase(local_info_db_path) as info_db, LocalDatabase(
        cache_db_path
    ) as cache_db:
        get_uuid_last_modified_query = """
select 
    uuid,
    last_modified
from 
    objects
where 
    filestore = '';
"""
        results = info_db.run_query(get_uuid_last_modified_query)
        uuid_last_modified_list = [row for row in results]

        for uuid, last_modified in uuid_last_modified_list:
            last_modified_query = get_filestore_uuids_query(last_modified)
            worker_count = 100
            threads: list[Thread] = []
            for worker_id in range(worker_count):
                thread = Thread(target=worker, args=(worker_id, last_modified_query))
                threads.append(thread)
                thread.start()

            for thread in threads:
                thread.join()

            owner_database_name_query = f"""
select 
    database_name 
from 
    cache 
where 
    uuid = '{uuid}';
"""
            try:
                owner_database_name = cache_db.run_query(owner_database_name_query)[0][0]
            except IndexError:
                owner_database_name = ''
            owner_filestore_name = database_filestore_map.get(owner_database_name)
            if owner_filestore_name:
                logger.info(f"Found owner for file {uuid} -> {owner_filestore_name}")

                update_info_db_query = f"""
update
    objects
set
    filestore = '{owner_filestore_name}'
where
    uuid = '{uuid}';
"""
                info_db.run_query(update_info_db_query)
            else:
                logger.warning(f"Did not find owner for file {uuid}!")


if __name__ == "__main__":
    main()
