# Fill catalog script

## Installation

```
# clone zsapi repo to a path of your choice
# make note of the path you cloned the repo to
git clone git@gitlab.com:xxllnc/zaakgericht/support/zs-api.git

cd util/fill_catalog
python3.12 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pip install $PATH_TO_ZSAPI_REPOSITORY

cp ../../backend/database/credentials.json ./
```

## Usage

`python3 main.py -h`
```
usage: main.py [-h] -n {zaaksysteem-gov,zaaksysteem-com} -d DATABASE [-f FILESTORE] [-c]

options:
  -h, --help            show this help message and exit
  -n {zaaksysteem-gov,zaaksysteem-com}, --namespace {zaaksysteem-gov,zaaksysteem-com}
                        Namespace where environment lives
  -d DATABASE, --database DATABASE
                        Database of the environment to fill
  -f FILESTORE, --filestore FILESTORE
                        filestore name of the environment to fill
  -c, --cleanup         Cleanup database copy created during filling of the catalog
```

fill catalog:
- go to the contact page of the owner of the environment
    - for example: https://mintlab.zaaksysteem.nl/main/contact-view/organization/e2c93b36-2c92-45ac-85d0-b6daca7fdefa/data
- go to "omgevingen"
- find the environment to fill 
    - for example:  `rijnland-schieland-ontwikkeling.zaaksysteem.nl`
- click on the "Details bekijken" icon
- database is under `Database`
    - for example: `hoogheemra_development_70ad63`
- filestore is under `Storage Bucket`
    - for example: `hoogheemra_development_70ad63`
- namespace is either `zaaksysteem-gov` or `zaaksysteem-com`
    - almost always `zaaksysteem-gov`
- go to: https://xxllnc.awsapps.com/start#/
- click `AWS Account`
- click `xxllnc-prod-zaken`
- click `Command line or programmatic access`
- Under Option1: `Set AWS environment variables (Short-term credentials)`
    - click on the code block, see below for example
- paste the copied code in the terminal where you are going to run the script from (`util/fill_catalog`)
- run the command, see below for example

## Example
temp aws credentials
```
export AWS_ACCESS_KEY_ID="REDACTED"
export AWS_SECRET_ACCESS_KEY="REDACTED"
export AWS_SESSION_TOKEN="REDACTED"
```
commmand to run:
```
python3 main.py -n zaaksystem-gov -d hoogheemra_development_70ad63 -f hoogheemra_development_70ad63
```
cleanup:

script will warn you to cleanup the temporary database copy
```
[14-12-2023 14:01:27] [WARNING] [__main__]: DON'T FORGET TO CLEANUP DATABASE COPY backup_stadskanto_production_73154f_14_12_2023 AFTER TESTING IF ENVIRONMENT STILL WORKS:
python3 main.py -c -d backup_stadskanto_production_73154f_14_12_2023 -n zaaksysteem-gov
```

so after you logged in to the environment to check if the catalog is filled run the following:

```
python3 main.py -c -d backup_stadskanto_production_73154f_14_12_2023 -n zaaksysteem-gov
```

it will prompt you to enter `YES` if you are certain you want to drop the database

## Logs
Find log files at: `util/fill_catalog/logs/`
