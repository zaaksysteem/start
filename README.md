# Introduction

This repository contains all the `zaaksysteem` components.

Working with our `zaaksysteem` requires working in a lot of different libraries
/ packages / services. This repository is a "mono-repo", which makes working
on different parts of our `zaaksysteem` a lot easier by providing a lot of
helper utils. These utils are described below.

## Getting started

If you're running Docker Desktop on Windows or MacOS, make sure Docker can use
at least 10GB of memory. By default the virtual machines on those operating
systems only have 2GB of memory.

### TLDR

Go to [yq](https://github.com/mikefarah/yq/tags) and find the
appropriate download link. For example `https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64`.

_Note:_ yq version 4.x is required.

```Shell
git clone git@gitlab.com:xxllnc/zaakgericht/zaken/start.git
cd start

mkdir -p ~/.local/bin
curl -L -o ~/.local/bin/yq <<< DOWNLOAD LINK >>> && chmod +x ~/.local/bin/yq

bin/zs zaaksysteem setup
bin/zs devmode init

docker-compose up -d

bin/extract_ca_certificate.sh
```

The `zaaksysteem` development environment is now available at
[dev.zaaksysteem.nl](https://dev.zaaksysteem.nl/)

To get rid of the security error (unknown CA), you can tell your browser or
operating system to trust the certificate stored in `dev.zaaksysteem.nl.pem`.

By default, pre-built Docker images are used. See below for how to set up
development mode for frontend and backend.

### Long Version

Clone the `zaaksysteem` repository and change the current working directory
to `start`.

```Shell
git clone git@gitlab.com:xxllnc/zaakgericht/zaken/start.git
cd start
```

If yq _4.x_ is not installed then install it in `HOME/.local/bin`. Go to [yq](https://github.com/mikefarah/yq/tags)
and find the appropriate download link. For example `https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64`.

_Note:_ yq version 4.x is required.

```Shell
mkdir -p ~/.local/bin
curl -L -o ~/.local/bin/yq <<< DOWNLOAD LINK >>> && chmod +x ~/.local/bin/yq
# example: curl -L -o ~/.local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64 && chmod +x ~/.local/bin/yq
```

The next step is to setup the development environment by preparing
configuration. This command will create the required configuration files from
their `.dist` counterparts in `etc/`, and create the storage bucket in the
`Minio` container. `Minio` is a service we use to store files in our local
development environments. Go to [MinIO](https://hub.docker.com/r/minio/minio)
for details.

```Shell
bin/zs zaaksysteem setup
```

Now we set the development mode to the initial state. See [Development mode](#development-mode)
for more information.

```Shell
bin/zs devmode init
```

That's it. Now start your development `zaaksysteem` by running
`docker-compose up`, like this. To run the containers in the background option
`-d` is used.

```Shell
docker-compose up -d
```

This could take a while. Find yourself some coffee, and when Docker tells you
everything is ok, wait two minutes more, and browse to [dev.zaaksysteem.nl](https://dev.zaaksysteem.nl)
for your first encounter.

In case you cannot access the page, make sure to update your `HOSTS` file on
your os with the following entries:

> 127.0.0.1 dev.zaaksysteem.nl
> 127.0.0.1 mailhog.dev.zaaksysteem.nl
> 127.0.0.1 rabbitmq.dev.zaaksysteem.nl
> 127.0.0.1 minio.dev.zaaksysteem.nl
> 127.0.0.1 minio-ui.dev.zaaksysteem.nl
> 127.0.0.1 redis.dev.zaaksysteem.nl

If you have DNS rebind protection you might want to list `dev.zaaksysteem.nl`
and its subdomains as exceptions. Or you add them in your `HOSTS` file.

```Shell
# This list is not full list of hosts
127.0.0.1   localhost dev.zaaksysteem.nl mailhog.dev.zaaksysteem.nl rabbitmq.dev.zaaksysteem.nl minio.dev.zaaksysteem.nl minio-ui.dev.zaaksysteem.nl redis.dev.zaaksysteem.nl
```

You can log in with the following user accounts:

| Username    | Password    | Description                                                             |
| ----------- | ----------- | ----------------------------------------------------------------------- |
| `admin`     | `admin`     | Main "administrator" user; this one we use the most during development. |
| `beheerder` | `beheerder` | Secondary administrator account                                         |
| `gebruiker` | `gebruiker` | Normal user account with few rights.                                    |

Of course you're free to create more users, roles and groups in your development
environment as needed.

You'll probably see a HTTPS security error (unknown CA). To extract the CA
certificate used by your dev environment, run this:

```Shell
bin/extract_ca_certificate.sh
```

This puts the CA certificate in `dev.zaaksysteem.nl.pem` -- tell your browser
or OS to trust it and the error should disappear. For chrome users make sure
to import the cert in the `trusted root certification authorities` / `Vertrouwde basiscertificeringsinstanties` folder.

### Development mode

By default, Docker images are used, which means changes in the source code
won't be picked up by the services. To make that work, select a frontend and/or
backend development mode:

```Shell
bin/zs devmode frontend {mode}
bin/zs devmode backend {mode}
```

The frontend development supports 3 modes:

- `image` (use a pre-build container image, default)
- `container` (runs the development server in a container)
- `local` (to run the development server locally).

Run `docker maintenance` after switching modes:

```Shell
bin/zs docker maintenance
```

To run the development server for `local` frontend development, use the
following:

```Shell
# And then run the local development server:
cd frontend-mono
yarn install
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

The backend development supports 2 modes:

- `image` (use a pre-build container image, default)
- `container` (runs the backend in a container)

So if you're a backend developer, you can use:

```Shell
bin/zs devmode frontend image
bin/zs devmode backend container
```

A frontend developer might want:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend image
```

Or if you're running Linux on your development machine:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend container
```

_Note:_ don't forget to run `docker maintenance` after switching modes:

```Shell
bin/zs docker maintenance
```

For Visual Studio Code users workspace settings files (`.code-workspace`)
exists.

- Use `frontend-dev.code-workspace` for frontend development.
- Use `python-dev.code-workspace` for backend development.

## Typical Development

Below are examples to start development on our systems, depending on the part
of the system you're working on, here are some options.

### Python

For Python development, you are probably working on domain code, combined with
a HTTP daemon or RabbitMQ consumer.

Make sure you have installed Python version **3.11**. For Linux users, more
information can be found [here](https://tecadmin.net/how-to-install-python-3-11-on-debian/).

Also make sure you have installed the `venv` package. If you're installing
Python from source, it's included, if you're using (Debian) packages, install
the package `python3.11-venv` (using `apt`).

When developing, you can edit the files directly in the repository and don't
have to do that inside a container or similar. The source is volume-mounted
into the containers (if "devmode" is set correctly, see above).

If you use the `python-dev.code-workspace` workspace configuration in VSCode
locations of virtualenvs and plugin settings will be configured automatically.

Typical development involves the services, the domain (business logic) and the
database schema. For the HTTP daemon of the domain "case management", you
would work in:

- backend/http-cm
- backend/domains (contains domain code and database schema)

If you're changing something on a deeper level, in the framework or infrastructure wrappers, you'll also be working in:

- backend/minty
- backend/minty-pyramid (integration into the "pyramid" web framework)
- backend/minty-amqp (a framework for AMQP consumers)
- backend/minty-infra-* (wrappers for infrastructure, like S3, SQLAlchemy, etc.)

#### Tools for virtual environments

To make tests (and code editors) work outside the containers, you an create
virtual environments for the different subprojects, and install their
dependencies.

As an example, assume you're working on the `http-cm` service. First go to the
main `start` directory. Run the commands from this folder.

```Shell
bin/zs python setup http-cm
source .venv/http-cm/bin/activate
```

This will create a new, empty virtual environment in `.venv/http-cm` in the
root of the `start` repository and activates it in your current shell. It does
not yet install the requirements.

_Note:_ Remove the (old) virtual environment when upgrading to a new Python
version.

To install the requirements, you can run the `vinstall` command:

```Shell
bin/zs python vinstall http-cm
```

This will install all dependencies of the `http-cm` service in its virtual
environment, including the subproject itself in "editable" mode. If the
subproject depends on `backend/domains`, that is also installed in "editable"
mode.

If you're changing more than a service and domain code, for instance, when
you're adding functionality to the `minty` framework, you can also install that
in "editable" mode so the test suite picks up any changes you make:

```Shell
bin/zs python vinstall http-cm minty
```

You can run the tests for a subprojects:

```Shell
cd backend/http-cm
# Only if virtualenv is not yet active
source ../../.venv/http-cm/bin/activate
pytest
```

If you also want to get a report of test coverage so you can check if you've
covered all code paths with tests before submitting a merge request, you can
run it like this:

```Shell
pytest --cov=. --cov-report=html --cov-branch
```

There are some more useful command line options for pytest:

- `-x` stops the test suite the moment an error is found and does not run more
  tests
- `-v` for more verbose error logs (can be used multiple times for even more
  verbosity)
- `-s` to show things you `print()` to stdout/stderr. Can be very useful when
  debugging.

#### Tools for using Docker/containers

By default, the containers are built in a "read only" mode to save on resources
(watching files for changes is expensive!), and won't auto-restart when you
change files.

To configure a specific service to pick up edits you make, and auto-reload, you
run the following script. This needs to be re-run every time after re-creating
the container, for example after rebuilding it completely:

```Shell
./bin/zs python dinstall http-cm

docker-compose logs http-cm # View output from the Docker container for the backend python service `http-cm`
```

This will install "http-cm" and its main dependency, "domains" in "editable"
mode and makes the service automatically restart on changes in those
subprojects.

If you're also changing other dependencies that are included in `start`, like
`minty-pyramid`, you can mark that as "editable" as well:

```Shell
./bin/zs python dinstall http-cm minty-pyramid
```

Run `bin/zs python` from the main `start` directory to get more information
and help.

### Perl

Perl is the most straightforward way for developing. Just edit some files in
`backend/perl-api` folder in the `start` folder (repository root folder) and
run the necessary `docker-compose` commands to reload the engine.

```Shell
vim backend/perl-api/lib/Zaaksysteem/Constants.pm

docker-compose stop perl-api
docker-compose up -d perl-api
```

### JavaScript (React)

Running the React codebase in `local` development mode requires the following
to be installed locally:

- [Brew](https://brew.sh/) (optional) to make managing/installing packages easier
- [Node](https://github.com/nvm-sh/nvm) a recent version of Node
- [Yarn](https://classic.yarnpkg.com/en/docs/install/) a recent version of Yarn

Once installed, you can run the development environment from the the `start`
folder (repository root folder).

```Shell
cd frontend-mono
yarn install
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

This will serve the React apps locally, and will refresh any changes you make
to the code via Hot Module Reloading.

To use the frontend `container` development mode, all you need to do is enable
it (see [Development mode](#development-mode)) and rebuild/restart your
containers. This will run the above `yarn lerna:start` command inside a
container.

See `./docs/` for more details on installation and running of the
frontend development tools.

### Javascript (Angular)

Use this script to start the app / area you wish to develop in:

```Shell
./bin/run-frontend.sh {command}
```

where `{command}` is one of the following:

- reset
- gulp
- styles
- frontend
- intern
- wordaddin
- init

This will start the development server in the `frontend-frontendclient`
container, and start watching files. Your changes should then be made available
on your local development environment.

_Note:_ Use `./bin/run-frontend.sh` to list all commands that are available.

## End to End (e2e) tests

See [e2etests Readme](e2etests/README.md)

## Development Certificate

Follow these steps to install the Development Certificate:

### MacOS / Chrome

In Chrome, go to Settings -> Privacy and Security, Manage Certificates. This will open up the Keychain.

Drag the textfile into this window.

Right-click on the 'Zaaksysteem Development CA' and select 'Get Info'.

Open 'Trust' and set 'When using this certificate' to 'Always Trust'.

## How does it fit together

Todo, but let's start with this diagram:

![Overview of repositories](assets/repositories_zoom_70percent.png)

## Admin Interfaces

Some of the services used by Zaaksysteem come with web-based admin tools. These are available at
the following URLs:

- [RabbitMQ](https://rabbitmq.dev.zaaksysteem.nl/) (message queue; use guest / guest to log in)
- [Minio](https://minio.dev.zaaksysteem.nl/) (S3-compatible file store; see etc/minty_config.conf for login keys)
- [Redis](https://redis.dev.zaaksysteem.nl/) (temporary storage for session data)
- [Mailhog](https://mailhog.dev.zaaksysteem.nl/) (development web-mail server)

## Settings

Editorsettings are, when possible, described in the editorconfig. Here are some
advised settings for some common editors,

### Visual Studio Code (VSCode)

To use VSCode efficiently, we've added workspace files to this repository and,
extension recommendations and pre-configured settings in all (sub-)repositories.

These settings will enable automatic formatting using `black` and sorting of
imports using `isort` on save, and will run the `ruff` linter while typing.

#### Recommended Settings

Use the provided `.code-workspace` files to get the recommended VSCode settings.

## Contributing

Please read [CONTRIBUTING](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/CONTRIBUTING.rst)
for details on our code of conduct, and the process for submitting pull requests to us.

## License

Copyright (c) 2020, Minty Team and all persons listed in
[CONTRIBUTORS](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/CONTRIBUTORS.rst)

This project is licensed under the EUPL, v1.2. See the
[EUPL-1.2.txt](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/LICENSES/EUPL-1.2.txt)
in the `LICENSES` directory for details.

Selected components are licensed under Commercial license from Material-UI SAS. See the
[MUI-Commercial.txt](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/LICENSES/MUI-Commercial.txt)
in the `LICENSES` directory for details.
