export interface EmployeeData {
  username: string;
  initials: string;
  firstName: string;
  lastName: string;
  displayName: string;
  emailAddress: string;
  userFunction: string;
  password: string;
  roles: Array<string>;
}
export const employeeData: EmployeeData[] = [
  {
    username: 'autoadmin',
    initials: 'AA',
    firstName: 'Auto',
    lastName: 'Admin',
    displayName: 'Auto Admin',
    emailAddress: 'autoadmin@xxllnc.nl',
    userFunction: 'Admin',
    password: 'autoadmin',
    roles: ['Administrator', 'Zaaksysteembeheerder', 'Persoonsverwerker'],
  },
  {
    username: 'autobeheerder',
    initials: 'AB',
    firstName: 'Auto',
    lastName: 'Beheerder',
    displayName: 'Auto Beheerder',
    emailAddress: 'autobeheerder@xxllnc.nl',
    userFunction: 'Beheerder',
    password: 'autobeheerder',
    roles: ['Zaaksysteembeheerder', 'Persoonsverwerker'],
  },
  {
    username: 'autobehandelaar',
    initials: 'AB',
    firstName: 'Auto',
    lastName: 'Behandelaar',
    displayName: 'Auto Behandelaar',
    emailAddress: 'autobehandelaar@xxllnc.nl',
    userFunction: 'Behandelaar',
    password: 'autobehandelaar',
    roles: ['Persoonsverwerker'],
  },
];
