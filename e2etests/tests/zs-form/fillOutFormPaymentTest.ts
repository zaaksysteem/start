/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/no-networkidle */
import { Page, expect, test } from '@playwright/test';
import { TestSettings } from '../../types';
import { locators } from '../../utils';

interface PropsPayment {
  formPage: Page;
  reason: string;
  amount: string;
  testSettings: TestSettings;
}

export const fillOutFormPaymentTest = async ({
  formPage,
  reason,
  amount,
  testSettings,
}: PropsPayment): Promise<string> => {
  const { personAuthentication, superLongTimeout } = testSettings;
  // after submitting the form the casenumber will be set
  let caseNumber = '';

  await test.step('Check explanation page', async () => {
    await expect(formPage.locator('h2')).toHaveText('Uitleg test');
    await expect(
      formPage.getByText(
        'Dit formulier dient om de ogone/inigenico betalings koppeling te testen'
      )
    ).toBeVisible();
    await expect(
      formPage.getByText('Dit is een test voor de Ogone betaling')
    ).toBeVisible();

    // Click next to go to the next page and check if the next page is loaded
    await formPage.getByRole('button', { name: 'Volgende' }).press('Enter');
    await expect(
      formPage.getByText('Donatie bedrag', { exact: true })
    ).toBeVisible();
  });

  await test.step('Fill donation page', async () => {
    await formPage.getByRole('textbox').fill(amount);

    const redenDonatie = formPage
      .locator('//*[@class="ql-editor ql-blank"]')
      .first();
    await redenDonatie.press('Control+b');
    await redenDonatie.fill(reason);

    await formPage.getByRole('button', { name: 'Volgende' }).press('Enter');
    await expect(
      formPage.getByRole('heading', {
        name: 'Controleer uw invoer en overige acties',
      })
    ).toBeVisible();
  });

  await test.step('Check overview page and submit form', async () => {
    await expect(
      formPage
        .locator(
          locators.labelLeftOnPersonFormOverview('Reden van donatie', '2')
        )
        .first()
    ).toHaveText(`${reason}`);
    await expect(
      formPage
        .locator(locators.labelLeftOnPersonFormOverview('Donatie bedrag', '2'))
        .first()
    ).toHaveText(`${amount}`);

    await formPage.getByRole('button', { name: 'Versturen' }).press('Enter');
    await expect(
      formPage.locator(locators.labelLeftOnForm('Zaaknummer'))
    ).toBeVisible({ timeout: superLongTimeout });

    caseNumber = await formPage
      .locator(locators.labelLeftOnForm('Zaaknummer'))
      .innerText();

    await expect(
      formPage.locator(locators.labelLeftOnForm('Product of dienst'))
    ).toHaveText(personAuthentication.loginForm);
    await formPage
      .locator(locators.labelLeftOnForm('Bedrag'))
      .scrollIntoViewIfNeeded();
    const amountFound = await formPage
      .locator(locators.labelLeftOnForm('Bedrag'))
      .innerText();
    expect(
      amountFound === `€ ${amount}` ||
        amountFound === `€ ${amount.replace(',', '.')}`
    ).toBeTruthy();
  });

  await test.step('Open ideal', async () => {
    await formPage
      .getByRole('button', { name: 'Betalen' })
      .first()
      .press('Enter');
    await expect(
      formPage.getByText('Betaalbevestiging').locator('visible=true')
    ).toBeVisible();

    await formPage.getByRole('button', { name: 'iDEAL' }).click();
    await expect(
      formPage.getByText(
        'Kies uw bank en klik op "Ga verder" om bij uw bank met iDEAL te betalen.'
      )
    ).toBeVisible();

    await formPage
      .getByRole('combobox', { name: 'Selecteer uw bank' })
      .selectOption('9999+TST');
    await formPage.getByRole('button', { name: 'Ga verder' }).click();
    await expect(
      formPage.getByRole('heading', { name: 'iDEAL simulator' })
    ).toBeVisible();
  });

  return Promise.resolve(caseNumber);
};
