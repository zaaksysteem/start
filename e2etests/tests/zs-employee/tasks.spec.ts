import { Page, TestInfo, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { createCase } from '../../utils/createCase';
import {
  dateOnOverview,
  testrondeData,
  dateNotification,
  dateNotificationWithPoint,
} from './testrondeData';
import {
  fillAttribute,
  registerCaseAndCheckMessages,
} from '../zs-form/formFillingAndCaseRegistration';
import { executeSteps } from '../../utils/executeSteps';
import { BSN2 } from '../../testfiles';
import { finishMainCase } from './finishMainCase';

const {
  longTimeout,
  baseUrl,
  clickDelay,
  beheerderUser,
  beheerderPassword,
  typeDelay,
  shortTimeout,
} = testSettings;
const mainCaseTypeName = 'Auto TESTRONDE V2';
const BSN = BSN2;
let caseNumberInUrl = '';
let userName = '';

test.describe
  .serial(`Create and process tasks (${mainCaseTypeName}) @all`, () => {
  const selectCasetype = async (page: Page, casetypeName: string) => {
    const iframe = page.frameLocator('#react-iframe');
    const caseTypeSelector = iframe.getByPlaceholder('Zaaktype selecteren…');

    await caseTypeSelector.click({ delay: clickDelay });
    await caseTypeSelector.fill(`${casetypeName.toUpperCase()}`);
    await expect(
      iframe.getByRole('option', { name: `${casetypeName}`, exact: true })
    ).toBeVisible({ timeout: longTimeout });
    await caseTypeSelector.press('ArrowDown', { delay: typeDelay });
    await caseTypeSelector.press('Enter', { delay: typeDelay });
    await caseTypeSelector.press('Tab', { delay: typeDelay });
  };

  const logoffAndOn = async (page: Page) => {
    await page.locator('[aria-label="Hoofdmenu openen"]').click();
    await page.getByRole('menuitem', { name: ' Uitloggen' }).click();
    // eslint-disable-next-line playwright/no-conditional-in-test
    await new Promise(rez => setTimeout(rez, shortTimeout));

    await expect(page).toHaveURL(
      new RegExp(`${baseUrl}auth/(?:page|login(?:\\?referer.+)?)`)
    );
    await page.getByPlaceholder('Gebruikersnaam').fill(beheerderUser);
    await page.getByPlaceholder('Wachtwoord').fill(beheerderPassword);
    await page.getByRole('button', { name: 'Inloggen' }).click();

    await page.waitForURL(/intern/);
    await expect(page.getByLabel('Widget Favoriete zaaktypen')).toBeVisible({
      timeout: longTimeout,
    });

    await page.context().storageState({ path: authPathBeheerder });
  };

  test.use({ storageState: authPathBeheerder });

  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  const attributeSelection = [
    'auto_tst_k1001_v2_adres',
    'auto_tst_k1008_meervoudige_keuze',
    'auto_tst_k1009_datum',
    'auto_tst_k1011_document',
    'auto_tst_k1015_numeriek',
    'auto_tst_k1019_tekstveld',
  ];
  const firstPageAttributes = Object.values(testrondeData).filter(
    attribute => attribute.onPages?.toString().includes('form1')
  );
  const secondPageAttributes = Object.values(testrondeData).filter(
    attribute =>
      attribute.onPages?.toString().includes('form2') &&
      attributeSelection.includes(attribute.magicString ?? '')
  );

  test('Tasks', async ({ page }, testInfo: TestInfo) => {
    const iframe = page.frameLocator('#react-iframe');
    const employeeSelector = iframe.getByPlaceholder('Zoek een medewerker…');
    const caseTypeSelector = iframe.getByPlaceholder('Zaaktype selecteren…');
    const dateDueField = iframe.getByPlaceholder('Datum toevoegen');
    const assigneeSelector = iframe.getByPlaceholder('Zoek een behandelaar…');
    const departmentSelector = iframe.getByPlaceholder('Kies een afdeling…');
    const iframeButton = (text: string) =>
      iframe.getByRole('button', { name: text });

    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: mainCaseTypeName, BSN });
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      firstPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to next page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await expect(page.getByText('Vul ze allemaal in!')).toBeVisible();
    });

    await test.step("Set 'Nee' to K1016 field, so that rules don't trigger", async () => {
      await page
        .getByText('auto_tst_k1016_enkelvoudige_keuze')
        .scrollIntoViewIfNeeded();
      await page.getByLabel('Nee').click();
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      secondPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to overview page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.allocation-picker-option-button');
    });

    await test.step(`Register the case ${mainCaseTypeName}`, async () => {
      caseNumberInUrl = await registerCaseAndCheckMessages({
        page,
        waitForText: 'Extra testen',
        testScriptName: testInfo.title,
      });
    });

    await test.step(`Add task`, async () => {
      await page.getByRole('link', { name: 'Open tab Taken' }).click();
      await iframeButton('Toevoegen').click();
      await iframe.getByRole('textbox').fill('Handel de deelzaak af');
      await iframe.getByRole('textbox').press('Enter');
      await expect(
        iframe
          .locator('div')
          .filter({ hasText: /^Handel de deelzaak af$/ })
          .nth(1)
      ).toBeVisible();
    });

    await test.step(`Get the employee name`, async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      userName =
        // eslint-disable-next-line playwright/no-conditional-in-test
        (await page.locator('a.side-menu-user-link.ng-binding').innerText()) ??
        '';
      await page.getByRole('menuitem', { name: 'Hoofdmenu sluiten' }).click();
    });

    await test.step(`Open task, add note and asign`, async () => {
      await iframe
        .locator('li')
        .filter({ hasText: 'Handel de deelzaak af' })
        .locator('button[name="goToTaskEdit"]')
        .press('Enter', { delay: clickDelay });
      await iframe.getByPlaceholder('Notitie').click({ delay: clickDelay });
      await iframe
        .getByPlaceholder('Notitie')
        .fill('Dit is de taak voor de deelzaak afhandeling');
      await employeeSelector.click();
      await employeeSelector.fill(userName.toUpperCase());
      await expect(iframe.getByText(userName)).toBeVisible();
      await employeeSelector.press('ArrowDown');
      await employeeSelector.press('Enter');
      await dateDueField.click();
      await dateDueField.fill(`${dateOnOverview}`);
      await dateDueField.press('Tab');
      await iframeButton('Opslaan').press('Enter');
    });

    await test.step(`Check adding of task in`, async () => {
      await expect(
        iframe.getByText(
          `Handel de deelzaak af${dateNotification}•Notitie•${userName}`
        )
      ).toBeVisible();
    });

    await test.step('Force getting new notifications signs', async () => {
      await logoffAndOn(page);
    });

    await test.step('Check adding of notification in hamburger menu by red dot', async () => {
      try {
        await expect(
          page.locator('//button[contains(@class,"has-unread")]')
        ).toBeVisible({ timeout: shortTimeout });
      } catch {
        await logoffAndOn(page);

        await expect(
          page.locator('//button[contains(@class,"has-unread")]')
        ).toBeVisible();
      }
    });

    await test.step('Check tasks in widget Mijn opsenstaande taken', async () => {
      await page
        .getByLabel('Mijn openstaande taken')
        .locator('button')
        .filter({ hasText: 'Dagen' })
        .click();
      await page.getByLabel('Sorteren op undefined oplopend').last().click();
      await page.waitForLoadState('domcontentloaded');

      await expect(
        page
          .getByLabel('Mijn openstaande taken')
          .getByText(
            `${caseNumberInUrl} Handel de deelzaak af ${dateNotificationWithPoint} 0`
          )
          .nth(0)
      ).toBeVisible();
    });

    await test.step('Make widget Taken visible', async () => {
      await page
        .locator('.gridster-container > div:nth-child(2) > div')
        .first()
        .scrollIntoViewIfNeeded();
    });

    await test.step('Check assigned task in widget Taken', async () => {
      await iframe
        .locator('//div[contains(.//text(),"Taken")]//parent::div')
        .getByLabel('Zaaknummer')
        .click();

      await iframe.getByTestId('ArrowBackIcon').click();
      await page.waitForLoadState('domcontentloaded');
      await expect(
        iframe
          .locator('//div[contains(.//text(),"Taken")]//parent::div')
          .getByRole('row')
          .nth(1)
      ).toHaveText(
        `${caseNumberInUrl}Handel de deelzaak af${userName}${dateNotification}0`
      );
    });

    await test.step('Check unassigned task in widget Taken', async () => {
      await iframe
        .getByTestId('CancelIcon')
        .nth(0)
        .click({ delay: clickDelay });

      await expect(
        iframe
          .locator('//div[contains(.//text(),"Taken")]//parent::div')
          .getByRole('row')
          .nth(1)
      ).toHaveText(
        new RegExp(
          `${caseNumberInUrl}Registratiefase wijzigen en nieuwe kenme.*`
        )
      );
    });

    await test.step('Filter in widget Taken', async () => {
      await iframeButton('Filter').click();
      await iframeButton('Wis alle filters').click();

      await assigneeSelector.click();
      await assigneeSelector.fill(`${userName.toUpperCase()}`);
      await expect(
        iframe.getByRole('option', { name: userName })
      ).toBeVisible();
      await assigneeSelector.press('ArrowDown');
      await assigneeSelector.press('Enter');

      await departmentSelector.click({ delay: clickDelay });
      await departmentSelector.fill('AUTOMATISEREN');
      await expect(
        iframe.getByRole('option', { name: 'Automatiseren' })
      ).toBeVisible();
      await departmentSelector.press('ArrowDown');
      await departmentSelector.press('Enter');
      await departmentSelector.press('Tab', { delay: clickDelay });

      await selectCasetype(page, mainCaseTypeName);
      await iframeButton('Opslaan').click({ delay: clickDelay });
      await expect(
        iframe
          .locator('//div[contains(.//text(),"Taken")]//parent::div')
          .getByRole('row')
          .nth(1)
      ).toHaveText(
        `${caseNumberInUrl}Handel de deelzaak af${userName}${dateNotification}0`
      );
    });

    await test.step('Check setting of filtering', async () => {
      await iframeButton('Filter').click();

      await expect(iframeButton(`${userName}`).nth(1)).toBeVisible();
      await expect(iframeButton('Automatiseren').nth(1)).toBeVisible();
      await expect(iframeButton(`${mainCaseTypeName}`).nth(1)).toBeVisible();
    });

    await test.step('Change and check filtering on other casetype', async () => {
      await caseTypeSelector.click();
      await iframe
        .getByTestId('CancelIcon')
        .nth(5)
        .click({ delay: clickDelay });
      await selectCasetype(page, 'Auto TESTRONDE Deelzaak V2');
      await iframe
        .getByRole('button', { name: 'Opslaan' })
        .click({ delay: clickDelay });

      await expect(
        iframe.getByText('Er zijn geen taken met de opgegeven filters.')
      ).toBeVisible();
    });

    await test.step('Clear filtering', async () => {
      await iframeButton('Filter').click();

      await iframeButton('Wis alle filters').click();
      await iframeButton('Opslaan').click();
    });

    await test.step('Check notification icon with red dot showing', async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      await expect(
        page.locator('//li[contains(@class,"has-unread")]')
      ).toBeVisible();
    });

    await test.step('Check adding of task notification', async () => {
      await expect(
        page.locator('zs-notification-list-item').filter({
          hasText: `Zaak ${caseNumberInUrl}: Taak "Handel de deelzaak af" toegewezen. ${dateOnOverview}, `,
        })
      ).toBeVisible();
    });

    await test.step('Archive the notifications', async () => {
      await page.getByLabel('Alle notificaties archiveren').click();
      await page.getByRole('button', { name: 'Alles archiveren' }).click();
    });

    await test.step('Check notification icon now without red dot', async () => {
      await expect(
        page.locator('//li[contains(@class,"has-unread")]')
      ).toBeHidden();
      await page.getByRole('menuitem', { name: 'Hoofdmenu sluiten' }).click();
    });

    await test.step('Check archiving results in no red dot in hamburger menu', async () => {
      await expect(
        page.locator('//button[contains(@class,"has-unread")]')
      ).toBeHidden();
    });

    await test.step('Open case through lin in widget Mijn openstaande taken', async () => {
      await page
        .getByRole('link', { name: caseNumberInUrl, exact: true })
        .last()
        .click();
    });

    await test.step('Finish the new task', async () => {
      await page.getByRole('link', { name: 'Open tab Taken' }).click();
      await iframe
        .locator('li')
        .filter({
          hasText: `Handel de deelzaak af${dateNotification}•Notitie•${userName}`,
        })
        .locator('button[name="taskListDone"]')
        .click();
    });

    await finishMainCase({ page, mainCaseTypeName });
  });
});
