/* eslint-disable playwright/expect-expect */
/* eslint-disable camelcase */
import { test } from '@playwright/test';
import { authPathBeheerder, testSettings } from './../../playwright.config';
import { createCase } from '../../utils/createCase';
import {
  fillOutFormMainTestCaseWithRules,
  fillOutFormMainTestCaseWithoutRules,
} from './../zs-form/fillOutFormMainTestCase';

const caseTypeName = 'Auto TESTRONDE V2';

test.describe
  .serial(`Fill main test case (${caseTypeName}) by employee @all @select`, () => {
  test.use({ storageState: authPathBeheerder });
  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  // eslint-disable-next-line playwright/expect-expect
  test('Fill it without triggering rules', async ({ page }) => {
    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: caseTypeName });
    });

    await fillOutFormMainTestCaseWithoutRules({
      formPage: page,
      formName: caseTypeName,
    });
  });

  test('Fill it and trigger the rules', async ({ page }) => {
    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: caseTypeName });
    });

    await fillOutFormMainTestCaseWithRules({
      formPage: page,
      formName: caseTypeName,
    });
  });
});
