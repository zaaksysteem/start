import { Page, expect, test } from '@playwright/test';
import {
  generateApplicantNameToClick,
  personDataEnvironment,
} from '../../testfiles';

interface PropsCheck {
  page: Page;
  field: string;
  BSN: string;
}
export const checkField = async ({ page, field, BSN }: PropsCheck) => {
  const valueExpected =
    field === 'gender'
      ? personDataEnvironment[BSN][field].charAt(0)
      : personDataEnvironment[BSN][field];
  await page.locator(`input[name="${field}"]`).first().scrollIntoViewIfNeeded();
  await expect(page.locator(`input[name="${field}"]`).first()).toHaveAttribute(
    'value',
    valueExpected
  );
};

export const checkFill = async ({ page, field, BSN }: PropsCheck) => {
  await page.locator(`input[name="${field}"]`).first().scrollIntoViewIfNeeded();
  const valueInField =
    (await page
      .locator(`input[name="${field}"]`)
      .first()
      .getAttribute('value')) ?? '';
  if (valueInField !== personDataEnvironment[BSN][field]) {
    console.log(
      `Field ${field} should be: "${personDataEnvironment[BSN][field]}" but is "${valueInField}"`
    );
    await page.locator(`input[name="${field}"]`).first().click();
    await page
      .locator(`input[name="${field}"]`)
      .first()
      .fill(personDataEnvironment[BSN][field]);
  }
};

interface Props {
  page: Page;
  BSN: string;
}

export const addPersonContact = async ({ page, BSN }: Props) => {
  const {
    firstNames,
    insertions,
    familyName,
    nobleTitle,
    gender,
    residenceCountry,
    residenceStreet,
    residenceHouseNumber,
    residenceHouseNumberSuffix,
    residenceZipcode,
    residenceCity,
    foreignAddress1,
    foreignAddress2,
    foreignAddress3,
    addressInCommunity,
    hasCorrespondenceAddress,
    correspondenceStreet,
    correspondenceHouseNumber,
    correspondenceHouseNumberSuffix,
    correspondenceZipcode,
    correspondenceCity,
    phoneNumber,
    mobileNumber,
    email,
  } = personDataEnvironment[BSN];

  const dutchResidence = residenceCountry === 'Nederland';

  await test.step(`Start adding contact BSN ${BSN}`, async () => {
    await page.getByLabel('Hoofdmenu openen').click();
    await page.getByRole('menuitem', { name: ' Contact zoeken' }).click();
    await page.getByRole('button', { name: 'Nieuw' }).click();
    await page.getByRole('button', { name: 'Persoon' }).click();
    await page.waitForLoadState('domcontentloaded');
  });

  await test.step(`Set general data contact BSN ${BSN}`, async () => {
    await page.locator('input[name="np-burgerservicenummer"]').fill(BSN);
    await page.locator('input[name="np-voornamen"]').fill(firstNames);
    await page.locator('input[name="np-voorvoegsel"]').fill(insertions);
    await page.locator('input[name="np-geslachtsnaam"]').fill(familyName);
    await page.locator('input[name="np-adellijke_titel"]').fill(nobleTitle);
    await page.getByLabel(gender).check();
  });

  await test.step(`Set supplementary data contact BSN ${BSN}`, async () => {
    await page
      .locator('input[name="npc-telefoonnummer"]')
      .scrollIntoViewIfNeeded();
    await page.locator('input[name="npc-telefoonnummer"]').fill(phoneNumber);
    await page.locator('input[name="npc-mobiel"]').fill(mobileNumber);
    await page.locator('input[name="npc-email"]').fill(email);
  });

  await test.step(`Set residence address country contact BSN ${BSN}`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (!dutchResidence) {
      await page.getByLabel('Openen').click();
      await page.locator('input[name="np-landcode"]').fill(residenceCountry);
      await page.locator('input[name="np-landcode"]').press('ArrowDown');
      await page.getByText(residenceCountry, { exact: true }).click();
      await page.locator('input[name="np-landcode"]').press('Tab');
    }
  });

  await test.step(`Set rest of residence address contact BSN ${BSN}`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (dutchResidence) {
      await page.locator('input[name="np-postcode"]').fill(residenceZipcode);
      await page
        .locator('input[name="np-huisnummer"]')
        .fill(residenceHouseNumber);
      await page
        .locator('input[name="np-huisnummertoevoeging"]')
        .fill(residenceHouseNumberSuffix);
      await page.locator('input[name="np-straatnaam"]').fill(residenceStreet);
      await page.locator('input[name="np-woonplaats"]').fill(residenceCity);

      // eslint-disable-next-line playwright/no-conditional-in-test
      if (addressInCommunity === 'true') {
        await page.getByLabel('Binnengemeentelijk').check();
      }
    } else {
      await page
        .locator('input[name="np-adres_buitenland1"]')
        .fill(foreignAddress1);
      await page
        .locator('input[name="np-adres_buitenland2"]')
        .fill(foreignAddress2);
      await page
        .locator('input[name="np-adres_buitenland3"]')
        .fill(foreignAddress3);
    }
  });

  await test.step(`Set correspondence address contact BSN ${BSN}`, async () => {
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (hasCorrespondenceAddress === 'true') {
      await page.getByLabel('Briefadres').check();
      await page
        .locator('input[name="np-briefadres_postcode"]')
        .fill(correspondenceZipcode);
      await page
        .locator('input[name="np-briefadres_huisnummer"]')
        .fill(correspondenceHouseNumber);
      await page
        .locator('input[name="np-briefadres_huisnummertoevoeging"]')
        .fill(correspondenceHouseNumberSuffix);
      await page
        .locator('input[name="np-briefadres_straatnaam"]')
        .fill(correspondenceStreet);
      await page
        .locator('input[name="np-briefadres_woonplaats"]')
        .fill(correspondenceCity);
    }
  });

  await test.step(`Save new contact BSN ${BSN}`, async () => {
    await page.getByRole('button', { name: 'Aanmaken' }).click();
    await expect(page.getByText('Contact aangemaakt')).toBeVisible();
  });

  await test.step(`Open new contact BSN ${BSN}`, async () => {
    await page.getByRole('button', { name: 'Bekijken' }).click();
  });
};

export const addInternalNote = async ({ page, BSN }: Props) => {
  const { internalNote } = personDataEnvironment[BSN];

  await test.step(`Set internal note BSN ${BSN}`, async () => {
    await page.locator('input[name="internalNote"]').fill(internalNote);
    await page.locator('button[name="saveAdditionalInfo"]').click();
  });

  await test.step(`Check visibility internal note BSN ${BSN}`, async () => {
    await expect(page.getByText(internalNote)).toBeVisible();
  });
};

interface OpenFoundPersonAndCheckSetSup {
  page: Page;
  BSN: string;
  withBirthdate?: boolean;
}

export const openFoundPersonAndCheckSetSup = async ({
  page,
  BSN,
  withBirthdate = true,
}: OpenFoundPersonAndCheckSetSup) => {
  await test.step(`Open contact BSN ${BSN}`, async () => {
    const contactNameToClick = generateApplicantNameToClick({
      BSN,
      withBirthdate,
    });

    await expect(page.getByText(`${contactNameToClick}`).first()).toBeVisible();
    await page.getByText(`${contactNameToClick}`).first().click();
  });

  await test.step(`Check/reset supplementary data BSN ${BSN}`, async () => {
    await checkFill({
      page,
      field: 'phoneNumber',
      BSN,
    });
    await checkFill({ page, field: 'mobileNumber', BSN });
    await checkFill({ page, field: 'email', BSN });
    await checkFill({ page, field: 'internalNote', BSN });

    const saveAdditionalInfoEnabled = await page
      .locator('button[name="saveAdditionalInfo"]')
      .isEnabled();
    // eslint-disable-next-line playwright/no-conditional-in-test
    if (saveAdditionalInfoEnabled) {
      await page.locator('button[name="saveAdditionalInfo"]').click();
    }
  });
};
