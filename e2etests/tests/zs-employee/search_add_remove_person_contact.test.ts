import { expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { addInternalNote, checkField } from './searchAddPersonContact';
import {
  BSN4,
  generateApplicantNameToClick,
  personDataEnvironment,
} from '../../testfiles';
import { locators } from '../../utils';

const { baseUrl, shortTimeout, superLongTimeout, environment } = testSettings;
let isFound = false;
let uuid = '';

test.describe.serial(`Search, add and remove person contact @all`, () => {
  test.use({ storageState: authPathBeheerder });
  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  test(`BSN4: Search contact v2, make and remove contact with BSN ${BSN4}`, async ({
    page,
  }) => {
    const BSN = BSN4;
    const {
      firstNames,
      insertions,
      familyName,
      nobleTitle,
      gender,
      residenceCountry,
      residenceStreet,
      residenceHouseNumber,
      residenceHouseNumberSuffix,
      residenceZipcode,
      residenceCity,
      addressInCommunity,
      hasCorrespondenceAddress,
      correspondenceStreet,
      correspondenceHouseNumber,
      correspondenceHouseNumberSuffix,
      correspondenceZipcode,
      correspondenceCity,
      phoneNumber,
      mobileNumber,
      email,
    } = personDataEnvironment[BSN];

    async function fillByLabel(label: string, data: string) {
      await page.getByLabel(label).first().scrollIntoViewIfNeeded();
      await page.getByLabel(label).first().fill(data);
    }

    await test.step('Search through the contact rightcorner searchfield', async () => {
      await page.getByLabel('Hoofdmenu openen').click();
      await page.getByRole('menuitem', { name: ' Contact zoeken' }).click();
      await page.getByRole('banner').getByTestId('SearchIcon').click();
      await page.getByLabel('Openen').click();
      await page.getByRole('option', { name: 'Contacten' }).click();
      await page
        .getByPlaceholder('Zoeken in xxllnc zaken')
        .first()
        .fill(familyName);
      await page.waitForResponse(res => res.status() === 200);

      try {
        await expect(page.getByText('Geen resultaten gevonden.')).toBeVisible({
          timeout: shortTimeout,
        });
        await page.getByRole('button', { name: 'Sluiten' }).click();
      } catch {
        isFound = true;
      }
    });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (!isFound) {
      await test.step(`Start adding new contact BSN ${BSN}`, async () => {
        await page
          .getByRole('link', { name: 'Dashboard', exact: true })
          .click();
        await page.hover('[aria-label="Groene plus knop menu openen"]');
        await page.getByRole('menuitem', { name: 'Contact aanmaken' }).click();
      });

      await test.step(`Set general data contact ${BSN}`, async () => {
        await page.getByLabel('BSN').click();
        await page.getByLabel('BSN').fill(BSN);
        await page.getByLabel('Voornamen').fill(firstNames);
        await page.getByLabel('Voorvoegsel').fill(insertions);
        await page.getByLabel('Achternaam*').fill(familyName);
        await page.getByLabel('Adellijke titel').fill(nobleTitle);
        await page.getByLabel(gender).check();
      });

      await test.step(`Set supplementary data contact BSN ${BSN}`, async () => {
        await fillByLabel('Telefoonnummer', phoneNumber);
        await fillByLabel('Telefoonnummer (mobiel)', mobileNumber);
        await fillByLabel('Emailadres', email);
      });
      await test.step(`Set residence address country contact BSN ${BSN}`, async () => {
        const locie = page.locator(
          `//select/option[.//text()="${residenceCountry}"]`
        );
        // eslint-disable-next-line playwright/no-conditional-in-test
        const optionNumber = (await locie.getAttribute('value')) ?? 1;
        await page
          .getByRole('combobox', { name: 'LandDit veld is verplicht' })
          .selectOption(optionNumber.toString());
      });

      await test.step(`Set rest of residence address contact BSN ${BSN}`, async () => {
        await fillByLabel('Postcode*', residenceZipcode);
        await fillByLabel('Huisnummer*', residenceHouseNumber);
        await fillByLabel('Huisnummertoevoeging*', residenceHouseNumberSuffix);
        await fillByLabel('Straat*', residenceStreet);
        await fillByLabel('Woonplaats*', residenceCity);

        // eslint-disable-next-line playwright/no-conditional-in-test
        if (addressInCommunity === 'true') {
          await page.getByLabel('Binnengemeentelijk*').check();
        }
      });

      await test.step(`Set correspondence address contact BSN ${BSN}`, async () => {
        // eslint-disable-next-line playwright/no-conditional-in-test
        if (hasCorrespondenceAddress === 'true') {
          await page.getByLabel('Briefadres*').check();
          await fillByLabel('Briefadres postcode*', correspondenceZipcode);
          await fillByLabel(
            'Briefadres huisnummer*',
            correspondenceHouseNumber
          );
          await fillByLabel(
            'Briefadres huisnummer toevoeging*',
            correspondenceHouseNumberSuffix
          );
          await fillByLabel('Briefadres straat*', correspondenceStreet);
          await fillByLabel('Briefadres woonplaats*', correspondenceCity);
        }
      });

      await test.step(`Save new contact BSN ${BSN}`, async () => {
        await page.getByRole('button', { name: 'Aanmaken' }).click();
        await expect(page.getByText('Het contact is aangemaakt.')).toBeVisible({
          timeout: superLongTimeout,
        });
      });

      await test.step(`Add internal note to new contact BSN ${BSN}`, async () => {
        await page.getByLabel('Contact bekijken').click();
        await addInternalNote({ page, BSN });
      });
    }

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (isFound) {
      await test.step(`Open old contact BSN ${BSN}`, async () => {
        const contactNameToClick = generateApplicantNameToClick({
          BSN,
          withBirthdate: false,
        });

        await expect(
          page.getByText(`${contactNameToClick}`).first()
        ).toBeVisible();
        await page.getByText(`${contactNameToClick}`).first().click();
      });
    }

    await test.step(`Check new/old contact BSN ${BSN}`, async () => {
      await checkField({ page, field: 'firstNames', BSN });
      await checkField({ page, field: 'insertions', BSN });
      await checkField({ page, field: 'familyName', BSN });
      await checkField({ page, field: 'nobleTitle', BSN });
      await checkField({ page, field: 'gender', BSN });
      await checkField({ page, field: 'residenceStreet', BSN });
      await checkField({ page, field: 'residenceHouseNumber', BSN });
      await checkField({ page, field: 'residenceZipcode', BSN });
      await checkField({ page, field: 'correspondenceStreet', BSN });
      await checkField({
        page,
        field: 'correspondenceHouseNumber',
        BSN,
      });
      await checkField({ page, field: 'correspondenceZipcode', BSN });
      await checkField({ page, field: 'phoneNumber', BSN });
      await checkField({ page, field: 'mobileNumber', BSN });
      await checkField({ page, field: 'email', BSN });
    });

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (environment === 'dev') {
      await test.step(`Save UUID BSN ${BSN}`, async () => {
        uuid = await page.locator(locators.objectShowField('UUID')).innerText();
      });

      await test.step(`Remove contact BSN ${BSN}`, async () => {
        await page
          .getByRole('link', { name: 'Gegevensmagazijn Gegevensmagazijn' })
          .click();
      });

      await test.step(`Remove and fill queue for contact BSN ${BSN}`, async () => {
        await page.getByPlaceholder('Typ om te zoeken…').click();
        await page.getByPlaceholder('Typ om te zoeken…').fill(BSN);
        await page
          .getByLabel('row')
          .filter({ hasText: uuid })
          .getByRole('button', { name: 'Verwijderen' })
          .click();
      });

      await test.step(`Check setting on remove row for contact BSN ${BSN}`, async () => {
        await expect(
          page
            .getByText(
              /Actie voor object met ID (\d*) in de verwijderwachtrij geplaatst./gm
            )
            .first()
        ).toBeVisible();
      });

      await test.step('Run qitems in dev environment', async () => {
        await page.goto(`${baseUrl}/api/queue/pending/run_pending`);
      });

      await test.step('Check running qitems', async () => {
        await expect(page.getByText('"Ran 1 items"')).toBeVisible();
      });
    }
  });
});
