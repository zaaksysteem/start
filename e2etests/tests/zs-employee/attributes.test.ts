/* eslint-disable playwright/no-conditional-in-test */
import { Page, TestInfo, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { createCase } from '../../utils/createCase';
import { Attribute, testrondeData, testrondeDataAlt } from './testrondeData';
import {
  emptyValueFilledIn,
  expectedValueFilledIn,
  fillAttribute,
  registerCaseAndCheckMessages,
} from '../zs-form/formFillingAndCaseRegistration';
import { executeSteps } from '../../utils/executeSteps';
import { BSN2 } from '../../testfiles';
import { finishMainCase } from './finishMainCase';
import { searchCaseInSearchfield } from '../zs-person/searchCaseInZaakSysteem';
import { locatorPlaceholder } from '../../utils';

const { clickDelay, shortTimeout, longTimeout } = testSettings;
const mainCaseTypeName = 'Auto TESTRONDE V2';
const BSN = BSN2;
let caseNumberInUrl = '';

test.describe
  .serial(`Tests: Attributes are changed in case (${mainCaseTypeName}) @all`, () => {
  test.use({ storageState: authPathBeheerder });
  test.setTimeout(800 * 1000);

  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  const firstPageAttributes = Object.values(testrondeData).filter(
    attribute => attribute.onPages?.toString().includes('form1')
  );
  const secondPageAttributes = Object.values(testrondeData).filter(
    attribute =>
      attribute.onPages?.toString().includes('form2') &&
      attribute.inputType !== 'Geolocatie' &&
      attribute.inputType !== 'Locatie met kaart'
  );
  //Filled in dates get changed to one day prior in Gitlab
  //Will probably work when MINTY-9138 is fixed
  const formFilledInAttributes = Object.values(testrondeData).filter(
    attribute =>
      attribute.onPages?.toString().includes('overview') &&
      attribute.inputType !== 'Geolocatie' &&
      attribute.inputType !== 'Locatie met kaart' &&
      attribute.inputType !== 'Datum'
  );
  const emptyToChangeAttributesEmployee = Object.values(
    formFilledInAttributes
  ).filter(
    attribute =>
      attribute.inputType?.includes('Adres') ||
      attribute.inputType?.includes('Stra') ||
      attribute.inputType === 'Document' ||
      attribute.inputType === 'Meervoudige keuze'
  );
  const changedByEmployee = Object.values(testrondeDataAlt).filter(
    attribute =>
      attribute.inputType !== 'Locatie met kaart' &&
      attribute.inputType !== 'Datum'
  );
  const emptyToChangeAttributesAdmin = Object.values(testrondeDataAlt).filter(
    attribute =>
      attribute.inputType?.includes('Adres') ||
      attribute.inputType?.includes('Stra') ||
      attribute.inputType === 'Meervoudige keuze'
  );
  const changedByAdmin = Object.values(testrondeData).filter(
    attribute =>
      attribute.onPages?.toString().includes('form2') &&
      attribute.inputType !== 'Document' &&
      attribute.inputType !== 'Geolocatie' &&
      attribute.inputType !== 'Locatie met kaart' &&
      attribute.inputType !== 'Afbeelding' &&
      attribute.inputType !== 'Datum'
  );

  test(`Attributes are changed in case (${mainCaseTypeName})`, async ({
    page,
  }, testInfo: TestInfo) => {
    const getAttributeToChange = page.getByPlaceholder('Voeg een kenmerk toe');
    const setAttributeValue = page.getByLabel('Kenmerken wijzigen');

    const setAddress = async (attribute: Attribute) => {
      const locatorAddress =
        attribute.inputType === 'Adres V2' ||
        attribute.inputType === 'Adres (Google Maps)'
          ? setAttributeValue.getByPlaceholder(locatorPlaceholder)
          : setAttributeValue.getByRole('textbox', {
              name: attribute.publicName,
              exact: true,
            });
      const valueToSelect = attribute.overviewValue?.replace(' + −', '');
      await locatorAddress.click();
      await locatorAddress.pressSequentially(attribute.addressValue ?? '');
      await expect(
        setAttributeValue.getByRole('button', { name: valueToSelect })
      ).toBeVisible();
      await setAttributeValue
        .getByRole('button', { name: valueToSelect })
        .click();
      attribute.inputType === 'Adres V2' ||
      attribute.inputType === 'Adres (Google Maps)'
        ? await expect(
            setAttributeValue.getByRole('button', {
              name: attribute.overviewValue,
            })
          ).toBeHidden()
        : await expect(
            page.getByLabel(`Je hebt ${attribute.overviewValue} geselecteerd`)
          ).toBeVisible();
      if (attribute.addressValue2 !== undefined) {
        await locatorAddress.pressSequentially(attribute.addressValue2 ?? '');
        await expect(
          setAttributeValue.getByRole('button', {
            name: attribute.overviewValue2,
          })
        ).toBeVisible();
        await setAttributeValue
          .getByRole('button', { name: attribute.overviewValue2 })
          .click();
        await expect(
          page.getByLabel(`Je hebt ${attribute.overviewValue2} geselecteerd`)
        ).toBeVisible();
      }
    };

    const switchPhases = async (page: Page) => {
      await page
        .getByLabel('Huidige fase Extra testen')
        .click({ delay: clickDelay });
      await page.waitForLoadState('domcontentloaded');
      await page
        .getByLabel('Afgeronde fase Registreren')
        .click({ delay: clickDelay });
      try {
        await expect(
          page.getByText('Datum van de test + release tag')
        ).toBeVisible();
      } catch {
        await page.getByLabel('Afgeronde fase Registreren').click();
      }
      await page.waitForLoadState('domcontentloaded');
    };

    const saveChanges = async () => {
      await page.getByRole('button', { name: 'Opslaan' }).click();
      await page.waitForResponse(res => res.status() === 200);
      await page.waitForLoadState('domcontentloaded');
      await switchPhases(page);
    };

    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: mainCaseTypeName, BSN });
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      firstPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to next page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await expect(page.getByText('Vul ze allemaal in!')).toBeVisible();
    });

    await test.step("Set 'Nee' to K1016 field, so that rules don't trigger", async () => {
      await page
        .getByText('auto_tst_k1016_enkelvoudige_keuze')
        .scrollIntoViewIfNeeded();
      await page.getByLabel('Nee').click();
    });

    await executeSteps(
      attribute =>
        `Fill value for ${attribute.nameAttribute} in ${mainCaseTypeName}`,
      secondPageAttributes,
      fillAttribute(page)
    );

    await executeSteps(
      attribute =>
        `Check input value of ${attribute.nameAttribute} in ${mainCaseTypeName} for choice Nee`,
      secondPageAttributes,
      expectedValueFilledIn(page)
    );

    await test.step('Go to overview page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.allocation-picker-option-button');
    });

    await test.step(`Register the case ${mainCaseTypeName}`, async () => {
      caseNumberInUrl = await registerCaseAndCheckMessages({
        page,
        waitForText: 'Extra testen',
        testScriptName: testInfo.title,
      });
    });

    await test.step('Go to phase Registration', async () => {
      await page.getByLabel('Afgeronde fase Registreren').click();
    });

    await executeSteps(
      attribute =>
        `After making case ${mainCaseTypeName}: Check value of ${attribute.nameAttribute} for choice Nee`,
      formFilledInAttributes,
      expectedValueFilledIn(page)
    );

    await executeSteps(
      attribute =>
        `Employee: Empty values of ${attribute.nameAttribute} before changing for choice Nee`,
      emptyToChangeAttributesEmployee,
      emptyValueFilledIn(page)
    );

    await test.step('Employee: Save the emptied values', async () => {
      await saveChanges();
    });

    await executeSteps(
      attribute =>
        `Employee: Fill value of ${attribute.publicName} for choice Nee`,
      changedByEmployee,
      fillAttribute(page)
    );

    await test.step('Employee: Save newly filled in values', async () => {
      await saveChanges();
    });

    await executeSteps(
      attribute =>
        `Employee: Check after saving, changed value of ${attribute.publicName} for choice Nee`,
      changedByEmployee,
      expectedValueFilledIn(page)
    );

    await executeSteps(
      attribute =>
        `Admin: Empty values of ${attribute.nameAttribute} before changing for choice Nee`,
      emptyToChangeAttributesAdmin,
      emptyValueFilledIn(page)
    );

    await test.step('Admin: Save cleared values', async () => {
      await saveChanges();
    });

    for (const attribute of changedByAdmin.filter(
      attribute => attribute.inputType !== 'Enkelvoudige keuze'
    )) {
      await test.step('Admin: Start changing of attributes', async () => {
        await page
          .getByText(`${attribute.publicName}`)
          .first()
          .scrollIntoViewIfNeeded();

        await page.getByLabel('Actie menu openen').click();
        await page
          .getByRole('menuitem', { name: ' Beheeracties' })
          .press('Enter');
        await page
          .getByRole('menuitem', { name: 'Kenmerken wijzigen' })
          .press('Enter');
      });

      await test.step(`Admin: Change of attribute ${attribute.publicName}`, async () => {
        const valueToSet =
          attribute.fillValue ??
          attribute.valueDefault ??
          attribute.checkValue ??
          attribute.selectValue;
        await getAttributeToChange.click({ delay: clickDelay });
        await getAttributeToChange.pressSequentially(`${attribute.publicName}`);
        await getAttributeToChange.press('ArrowDown');
        await getAttributeToChange.press('Enter');
        if (
          attribute.inputType?.includes('Adres') ||
          attribute.inputType?.includes('Stra')
        ) {
          await setAddress(attribute);
        } else if (attribute.inputType === 'Keuzelijst') {
          await setAttributeValue
            .getByLabel(`${attribute.publicName}*`)
            .selectOption(`string:${valueToSet}`);
        } else if (attribute.inputType === 'Meervoudige keuze') {
          await setAttributeValue.getByLabel(`${valueToSet}`).check();
          attribute.checkValue2 !== undefined
            ? await setAttributeValue
                .getByLabel(`${attribute.checkValue2}`)
                .check()
            : '';
        } else if (attribute.inputType === 'Rich text') {
          await setAttributeValue.locator('p').fill(`${valueToSet}`);
        } else if (attribute.inputType === 'Groot tekstveld') {
          await setAttributeValue.locator('textarea').fill(`${valueToSet}`);
        } else {
          await setAttributeValue
            .getByLabel(`${attribute.publicName}*`)
            .fill(`${valueToSet}`);
        }
      });

      await test.step('Admin: Change all values in case', async () => {
        await page
          .getByRole('button', { name: 'Kenmerken wijzigen' })
          .click({ delay: clickDelay });
        await expect(
          page.getByText('Beheeractie is succesvol uitgevoerd')
        ).toBeVisible({ timeout: longTimeout });
        if (attribute.inputType === 'Adres (Google Maps)') {
          try {
            await page
              .locator('li')
              .filter({ hasText: 'Bezig met ophalen van locatie' })
              .getByLabel('Melding sluiten')
              .first()
              .click({ timeout: shortTimeout });
          } catch {
            //Do nothing
          }
        } else {
          await page
            .locator('li')
            .filter({ hasText: 'Beheeractie is succesvol uitgevoerd' })
            .getByLabel('Melding sluiten')
            .click();
        }
      });
    }

    await test.step('Admin: check that there is nothing to save', async () => {
      await expect(
        page.getByRole('button', { name: 'Opgeslagen' })
      ).toBeDisabled();
    });

    await test.step('Admin: switch phases to refresh data', async () => {
      await switchPhases(page);
    });

    await executeSteps(
      attribute =>
        `Admin changes: Check after changing, the value of ${attribute.publicName} for choice Nee`,
      changedByAdmin,
      expectedValueFilledIn(page)
    );

    await searchCaseInSearchfield(page, caseNumberInUrl);

    await finishMainCase({ page, mainCaseTypeName });
  });
});
