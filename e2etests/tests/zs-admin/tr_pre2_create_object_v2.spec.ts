/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/expect-expect */
import { BrowserContext, Page, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import {
  navigateAdministration,
  checkOrAddAttribute,
  searchAttribute,
} from './administration';
import {
  testrondeData,
  testrondeObjectTypeV2,
} from '../zs-employee/testrondeData';

const { options, baseUrl } = testSettings;
const categoryAndFolder = 'Automatische E2E TESTRONDE';
const testrondeDataInObject = Object.values(testrondeData).filter(
  attribute => attribute.inObject
);
let objectIsPresent = false;

test.describe.serial('Create objecttype for E2E TESTRONDE @db2', () => {
  let page: Page;
  let context: BrowserContext;
  test.use({ storageState: authPathBeheerder });

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options);
    page = await context.newPage();

    await page.goto(baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  async function addRoleAndRights(
    page: Page,
    department: string,
    roleNr: number
  ) {
    await page
      .getByRole('button', { name: 'Rol en afdeling toevoegen' })
      .click();
    await page
      .getByPlaceholder('Selecteer een afdeling')
      .nth(roleNr)
      .fill(department);
    await page.getByText(department).last().click();
    await page.getByPlaceholder('Selecteer een rol').nth(roleNr).click();
    await page.getByPlaceholder('Selecteer een rol').nth(roleNr).fill('behand');
    await page.getByRole('option', { name: /Behandelaar/ }).click();
    await page.getByLabel('Mag beheren').nth(roleNr).check();
  }

  test('Fill general data for object type V2', async ({}) => {
    await test.step('Open admin section', async () => {
      await navigateAdministration({ page, menuItem: 'Catalogus' });
    });

    objectIsPresent = (await searchAttribute(testrondeObjectTypeV2, page))
      .found;

    if (!objectIsPresent) {
      await test.step('Go to the Auto objects folder', async () => {
        await page.getByPlaceholder('Zoeken in de catalogus…').click();
        await page
          .getByPlaceholder('Zoeken in de catalogus…')
          .fill(`${categoryAndFolder}`);
        await page.getByPlaceholder('Zoeken in de catalogus…').press('Enter');
        await page.getByRole('link', { name: `${categoryAndFolder}` }).click();
      });

      await test.step('Start creating the object', async () => {
        await page.getByTestId('AddIcon').nth(1).click();
        await page.getByRole('button', { name: 'Objecttype v2' }).click();
      });

      await test.step('Add object name', async () => {
        await page
          .getByPlaceholder('Geef een herkenbare naam voor het objecttype')
          .click();
        await page
          .getByPlaceholder('Geef een herkenbare naam voor het objecttype')
          .fill(testrondeObjectTypeV2);
      });

      await test.step('Add object title', async () => {
        await page
          .getByPlaceholder(
            'Geef een titel voor de objecten van dit objecttype'
          )
          .click();
        await page
          .getByPlaceholder(
            'Geef een titel voor de objecten van dit objecttype'
          )
          .fill(
            'Auto Testronde [[ j("$.attributes.custom_fields.auto_tst_k1000_datum_release_tag") ]] -' +
              ' [[ j("$.attributes.custom_fields.auto_tst_o0001_object_naam") ]]'
          );
      });

      await test.step('Add object subtitle', async () => {
        await page
          .getByPlaceholder(
            'Geef een subtitel voor de objecten van dit objecttype'
          )
          .click();
        await page
          .getByPlaceholder(
            'Geef een subtitel voor de objecten van dit objecttype'
          )
          .fill('Object V2 Aangemaakt en gebruikt in de automatische test');
      });

      await test.step('Add object external source', async () => {
        await page
          .getByPlaceholder('Referentie voor bron buiten Zaaksysteem.nl')
          .click();
        await page
          .getByPlaceholder('Referentie voor bron buiten Zaaksysteem.nl')
          .fill('Playwright');
      });

      await test.step('Go to the Attributes page', async () => {
        await page.getByRole('button', { name: 'Volgende' }).click();
      });
    }
  });

  testrondeDataInObject.forEach(attribute => {
    test(`Search and add the attribute ${attribute.nameAttribute} to the object`, async ({}) => {
      test.skip(objectIsPresent, 'The object is already present');
      await test.step(`Search the attribute ${attribute.nameAttribute}`, async () => {
        await page.getByPlaceholder('Zoek en voeg toe').click();
        await page
          .getByPlaceholder('Zoek en voeg toe')
          .fill(`${attribute.nameAttribute}`);
        await page
          .getByRole('option', {
            name: `${attribute.nameAttribute}`,
            exact: true,
          })
          .first()
          .click();
      });

      await test.step(`Set the attribute ${attribute.nameAttribute} title`, async () => {
        await page.locator('button[name="openDetailsDialog"]').last().click();
        await page.getByRole('textbox', { name: 'Titel' }).click();
        await page
          .getByRole('textbox', { name: 'Titel' })
          .fill(`${attribute.title}`);
      });

      await test.step(`Set the attribute ${attribute.nameAttribute} explanation (external)`, async () => {
        await page.locator('//textarea[@name="external_description"]').click();
        await page
          .locator('//textarea[@name="external_description"]')
          .fill(`${attribute.explanationExternal}`);
      });

      await test.step(`Set the attribute ${attribute.nameAttribute} mandatory`, async () => {
        let mandatory = false;
        for (let index = 0; index < Object.entries(attribute).length; index++) {
          const keyProperty = Object.keys(attribute)[index];
          if (keyProperty === 'mandatory') {
            mandatory = Object.values(attribute)[index] === true;
            break;
          }
        }
        mandatory ? await page.getByLabel('Verplicht veld').check() : '';
      });

      await test.step(`Set the attribute ${attribute.nameAttribute} system attribute`, async () => {
        let systemAttribute = false;
        for (let index = 0; index < Object.entries(attribute).length; index++) {
          const keyProperty = Object.keys(attribute)[index];
          if (keyProperty === 'systemAttribute') {
            systemAttribute = Object.values(attribute)[index] === true;
            break;
          }
        }
        systemAttribute
          ? await page.getByLabel('Gebruik als systeemkenmerk').check()
          : '';
      });

      await test.step(`Save the attribute ${attribute.nameAttribute}`, async () => {
        await page.getByRole('button', { name: 'Opslaan' }).click();
      });
    });
  });

  test('Fill the rest of the object pages', async ({}) => {
    test.skip(objectIsPresent, 'The object is already present');
    await test.step('Go to the Relations page', async () => {
      await page.getByRole('button', { name: 'Volgende' }).click();
    });

    await test.step('Go to the Rights page', async () => {
      await page.getByRole('button', { name: 'Volgende' }).click();
    });

    await test.step('Add the role and rights for AUTO behandelaar', async () => {
      await addRoleAndRights(page, 'Automatiseren', 0);
    });

    await test.step('Add the role and rights for FO behandelaar', async () => {
      await addRoleAndRights(page, 'Frontoffice', 1);
    });

    await test.step('Add the role and rights for BE behandelaar', async () => {
      await addRoleAndRights(page, 'Backoffice', 2);
    });

    await test.step('Go to the Overview page', async () => {
      await page.getByRole('button', { name: 'Volgende' }).click();
    });

    await test.step('Set what is changed and save', async () => {
      await page.getByLabel('Algemeen').check();
      await page.getByLabel('Kenmerken').check();
      await page.getByLabel('Rechten').check();
      await page.getByPlaceholder('Geef omschrijving op').click();
      await page
        .getByPlaceholder('Geef omschrijving op')
        .fill('Het object V2 is aangemaakt');
      await page.getByRole('button', { name: 'Opslaan' }).click();
      await page.waitForLoadState('domcontentloaded');
    });
  });

  Object.values(testrondeData)
    .filter(
      attribute =>
        attribute.magicString === 'auto_relatie_kenmerk_testronde_v2_object'
    )
    .forEach(attribute => {
      test(`Check and if not present make the v2 object relation attribute`, async ({}) => {
        //The relation attribute for TESTRONDE v2 objects is being made here
        !objectIsPresent
          ? await checkOrAddAttribute({
              page,
              ...attribute,
              desiredFolderName: `${categoryAndFolder}`,
            })
          : '';
      });
    });
});
