/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/expect-expect */
/* eslint-disable playwright/missing-playwright-await */
import { BrowserContext, Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { employeeData } from '../../testfiles';
import { navigateAdministration } from './administration';

const { longTimeout, shortTimeout, baseUrl, beheerderPassword, options } =
  testSettings;
const employeesToAdd = ['autoadmin', 'autobeheerder', 'autobehandelaar'];
const newDepartment = 'Automatiseren';
let employeeFound = false;
let departmentFound = false;

test.describe.serial('Adding department and/or employees @db1', () => {
  let page: Page;
  let context: BrowserContext;
  test.use({ storageState: authPathBeheerder });

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options);
    page = await context.newPage();

    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  test('Check/add department', async ({}, testInfo) => {
    await test.step('Go to Gebruikers', async () => {
      await navigateAdministration({ page, menuItem: 'Gebruikers' });
    });

    await test.step('Open the main department', async () => {
      await expect(
        page
          .frameLocator('iframe[title="Beheer formulier"]')
          .getByRole('row', {
            name: /^(Development.*|GBT.*|HoofdOrganisatie.*)$/,
          })
          .locator('span')
          .first()
      ).toBeVisible();
      await page
        .frameLocator('iframe[title="Beheer formulier"]')
        .getByRole('row', {
          name: /^(Development.*|GBT.*|HoofdOrganisatie.*)$/,
        })
        .locator('span')
        .first()
        .click();
      await expect(
        page
          .frameLocator('iframe[title="Beheer formulier"]')
          .getByRole('row', { name: 'Backoffice' })
          .locator('span')
          .first()
      ).toBeVisible();
    });

    await test.step('Check presense department', async () => {
      await expect(
        page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByText(newDepartment)
          .nth(0)
      )
        .toBeVisible({ timeout: shortTimeout })
        .then(() => {
          departmentFound = true;
        })
        .catch(error => {
          console.log(
            `${testInfo.title} The error ${error} was given, when searching for the deparment`
          );
          departmentFound = false;
        });
    });

    if (!departmentFound) {
      await test.step('Add department', async () => {
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('link', { name: ' ' })
          .first()
          .click();
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('link', { name: 'Afdeling' })
          .first()
          .click();
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('textbox')
          .fill(newDepartment);
        await page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByRole('button', { name: 'Opslaan' })
          .click();
        await page.waitForRequest(`${baseUrl}api/v1/session/current`);
      });
    }
  });

  test('Open the department', async ({}) => {
    await test.step('Open again the main department', async () => {
      // reload added ivm disconnected screen
      await page.reload({ waitUntil: 'domcontentloaded' });
      await page
        .frameLocator('iframe[title="Beheer formulier"]')
        .getByRole('row', {
          name: /^(Development |GBT |HoofdOrganisatie )$/,
        })
        .locator('span')
        .first()
        .click();
      await page.waitForLoadState('domcontentloaded');
      await expect(
        page
          .frameLocator('internal:attr=[title="Beheer formulier"i]')
          .getByText('Backoffice')
          .nth(0)
      ).toBeVisible({ timeout: longTimeout });
    });

    await test.step('Open the department', async () => {
      await page
        .frameLocator('internal:attr=[title="Beheer formulier"i]')
        .getByText(newDepartment)
        .nth(0)
        .click();
      await page.waitForLoadState('domcontentloaded');
    });
  });

  employeesToAdd.forEach(function fEmployeeToAdd(employeeInTest) {
    const {
      username,
      initials,
      firstName,
      lastName,
      emailAddress,
      userFunction,
      password,
      displayName,
      roles,
    } =
      employeeData.find(employee => employee.username === employeeInTest) ??
      employeeData[0];

    test(`Check/add employee ${employeeInTest}`, async ({}, testInfo) => {
      await test.step(`Check if employee ${employeeInTest} is present`, async () => {
        await expect(
          page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByText(`${employeeInTest}`)
            .nth(0)
        )
          .toBeVisible({ timeout: shortTimeout })
          .then(() => {
            employeeFound = true;
          })
          .catch(error => {
            console.log(
              `${testInfo.title}: When searching for ${employeeInTest} the error was given: ${error}`
            );
            employeeFound = false;
          });
      });

      if (!employeeFound) {
        await test.step(`Start adding employee ${employeeInTest}`, async () => {
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByRole('link', { name: ' ' })
            .first()
            .click();
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByRole('link', { name: 'Medewerker' })
            .click();
        });

        await test.step(`Fill employee data ${employeeInTest}`, async () => {
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="initials"]')
            .fill(`${initials}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="givenname"]')
            .fill(`${firstName}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="sn"]')
            .fill(`${lastName}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="displayname"]')
            .fill(`${displayName}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="mail"]')
            .fill(`${emailAddress}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="title"]')
            .fill(`${userFunction}`);
          const locator = page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('//select/option[contains(.//text(),"Automatiseren")]');
          // eslint-disable-next-line playwright/no-conditional-in-test
          const optionNumber = (await locator.getAttribute('value')) || -1;
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByRole('combobox')
            .selectOption(optionNumber.toString());
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="username"]')
            .fill(`${username}`);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="username"]')
            .press('Tab');
          // eslint-disable-next-line playwright/no-conditional-in-test
          const usePassword =
            // eslint-disable-next-line playwright/no-conditional-in-test
            baseUrl.indexOf('dev.') === 8
              ? `${password}`
              : `${beheerderPassword}`;
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="password"]')
            .click();
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="password"]')
            .fill(usePassword);
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="password_confirmation"]')
            .click();
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .locator('input[name="password_confirmation"]')
            .fill(usePassword);
        });

        await test.step(`Save the new employee ${employeeInTest}`, async () => {
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByRole('button', { name: 'Opslaan' })
            .click();
          await expect(
            page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByRole('row', { name: `${newDepartment}` })
              .locator('span')
              .first()
          ).toBeHidden();
          await page.waitForLoadState('domcontentloaded');
        });

        await test.step(`Open again the department for ${employeeInTest}`, async () => {
          // eslint-disable-next-line playwright/no-conditional-in-test
          await page
            .frameLocator('iframe[title="Beheer formulier"]')
            .getByRole('row', {
              name: /^(Development |GBT |HoofdOrganisatie )$/,
            })
            .locator('span')
            .first()
            .click();
          await page
            .frameLocator('internal:attr=[title="Beheer formulier"i]')
            .getByText(newDepartment)
            .nth(0)
            .click();
          await page.waitForLoadState('domcontentloaded');
        });
      }
    });

    roles.forEach(role => {
      test(`Add the role ${role} for ${employeeInTest}`, async ({}) => {
        if (!employeeFound) {
          await test.step(`Add the role ${role} for ${employeeInTest}`, async () => {
            const countRole = await page.getByText(`${role}`).count();
            await page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByText(`${role}`)
              .nth(countRole - 1)
              .hover();
            await page.mouse.down();
            await page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByText(`${employeeInTest}`)
              .hover();
            await page.mouse.up();
            await page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByText(`${displayName}`)
              .nth(0)
              .hover();
          });

          await test.step(`Check message about the role ${role} for ${employeeInTest}`, async () => {
            await expect(
              page
                .frameLocator('iframe[title="Beheer formulier"]')
                .getByText(`Rol "${role}" toegevoegd aan "${displayName}"`)
            ).toBeVisible();
          });

          await test.step(`Close message about the role ${role} for ${employeeInTest}`, async () => {
            await page
              .frameLocator('iframe[title="Beheer formulier"]')
              .getByRole('button', { name: 'x' })
              .click();
          });
        }
      });
    });
  });
});
