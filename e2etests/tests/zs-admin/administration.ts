/* eslint-disable playwright/no-conditional-in-test */
import test, { expect, Page } from '@playwright/test';
import { testSettings } from '../../playwright.config';

const { baseUrl, typeDelay } = testSettings;
interface NavigateAdministrationProps {
  page: Page;
  menuItem: string;
}

export const navigateAdministration = async ({
  page,
  menuItem,
}: NavigateAdministrationProps) => {
  await page.getByLabel('Hoofdmenu openen').click();
  await page.getByRole('menuitem', { name: ' Catalogus' }).click();
  menuItem !== 'Catalogus'
    ? await page.getByRole('link', { name: menuItem }).click()
    : '';
  await page.waitForLoadState('domcontentloaded');
};

interface CheckOrAddCategoryOrFolderProps {
  page: Page;
  categoryOrFolder: string;
}
export const checkOrAddCategoryOrFolder = async ({
  page,
  categoryOrFolder,
}: CheckOrAddCategoryOrFolderProps) => {
  await test.step(`Search for ${categoryOrFolder}`, async () => {
    await page.waitForLoadState('domcontentloaded');
    await page
      .getByPlaceholder('Zoeken in de catalogus')
      .fill(categoryOrFolder);
    const urlRequest2 = `${baseUrl}api/v2/admin/catalog/search?keyword=${encodeURIComponent(
      categoryOrFolder
    )}*`;
    await Promise.all([
      page.waitForRequest(urlRequest2),
      page.waitForResponse(res => res.status() === 200),
      page
        .getByPlaceholder('Zoeken in de catalogus')
        .press('Enter', { delay: typeDelay }),
    ]);
  });

  await test.step(`Add if not found ${categoryOrFolder}`, async () => {
    await page.waitForLoadState('domcontentloaded');
    let countFound = await page.getByText(categoryOrFolder).count();
    let index = 0;
    while (countFound === 0 && index < 15) {
      index++;
      countFound = await page.getByText(categoryOrFolder).count();
    }
    if (countFound === 0) {
      await page.getByTestId('CloseIcon').nth(0).click();
      await page.getByTestId('AddIcon').nth(1).click();
      await page.getByRole('button', { name: 'Map' }).click();
      await page.getByPlaceholder('Mapnaam').fill(categoryOrFolder);
      await page.getByRole('button', { name: 'Opslaan' }).click();
      try {
        await expect(
          page.getByRole('link', { name: 'Catalogus', exact: true })
        ).toBeVisible();
      } catch {
        await page.getByRole('button', { name: 'OK' }).click();
        await page.getByRole('button', { name: 'Annuleren' }).click();
      }
      await page
        .getByPlaceholder('Zoeken in de catalogus')
        .fill(categoryOrFolder);
      await page.getByPlaceholder('Zoeken in de catalogus').press('Enter');
      await expect(
        page.getByRole('link', { name: categoryOrFolder })
      ).toBeVisible();
      await expect(page.getByText('Map').first()).toBeVisible();
    }
    await page.getByTestId('CloseIcon').click();
  });
};

export async function searchAttribute(nameAttribute: string, page: Page) {
  await test.step(`Search for attribute ${nameAttribute}`, async () => {
    await page.getByRole('link', { name: 'Catalogus', exact: true }).click();
    await page.waitForLoadState('domcontentloaded');
    await page.getByPlaceholder('Zoeken in de catalogus').fill(nameAttribute);
    const urlRequest2 = `${baseUrl}api/v2/admin/catalog/search?keyword=${encodeURIComponent(
      nameAttribute
    )}*`;
    await Promise.all([
      page.waitForRequest(urlRequest2),
      page.waitForResponse(res => res.status() === 200),
      page
        .getByPlaceholder('Zoeken in de catalogus')
        .press('Enter', { delay: typeDelay }),
    ]);
  });

  let found = false;
  let countFound = 0;
  await test.step(`Check if not found, attribute ${nameAttribute}`, async () => {
    await page.waitForLoadState('domcontentloaded');

    countFound = await page.getByText(nameAttribute, { exact: false }).count();
    let index = 0;
    while (countFound === 0 && index < 5) {
      index++;
      countFound = await page
        .getByText(nameAttribute, { exact: false })
        .count();
    }
    found = countFound > 0;
  });
  return { found, countFound };
}

export interface AttributeProps {
  nameAttribute?: string;
  publicName?: string;
  inputType?: string;
  sensitiveData?: boolean;
  explanationInternal?: string;
  magicString?: string;
  valueDefault?: string;
  multipleValuesAllowed?: boolean;
  relationType?: string;
  objectType?: string;
  docCategory?: string;
  docConfidentiality?: string;
  docDirection?: string;
  choices?: Array<string>;
  desiredFolderName?: string;
}
interface CheckOrAddAttributeProps extends AttributeProps {
  page: Page;
}
export const checkOrAddAttribute = async ({
  page,
  nameAttribute = '',
  publicName = '',
  inputType = '',
  sensitiveData = false,
  magicString = nameAttribute,
  explanationInternal = '',
  valueDefault = '',
  multipleValuesAllowed = false,
  relationType = 'Contact',
  objectType = '',
  docCategory = 'inkomend',
  docConfidentiality = 'Openbaar',
  docDirection = 'intern',
  choices = [''],
  desiredFolderName = 'AUTO Kenmerken',
}: CheckOrAddAttributeProps) => {
  let { found, countFound } = await searchAttribute(nameAttribute, page);

  let rightOneNotPresent = true;
  if (found) {
    await test.step(`Check if wright one is there, attribute ${nameAttribute}`, async () => {
      await page.locator('button[name="buttonBarInfo"]').click();
      await page.waitForLoadState('domcontentloaded');
      for (let index = 0; index < countFound; index++) {
        const urlRequest3 = 'api/v2/admin/catalog/get_entry_detail.*';
        await Promise.all([
          page.waitForRequest(new RegExp(baseUrl + urlRequest3)),
          page.waitForResponse(res => res.status() === 200),
          page
            .getByRole('row', { name: 'row' })
            .getByLabel('')
            .nth(index)
            .check(),
        ]);
        await page.waitForLoadState('domcontentloaded');
        const isExtensionIcon = await page
          .locator(
            '(//*[contains(@class, "Mui-checked")]//ancestor::div[3]//span/*[@viewBox="0 0 24 24"])[2]'
          )
          .getAttribute('data-testid');
        if (isExtensionIcon === 'ExtensionIcon') {
          const folderName = await page
            .locator(
              '(//div/span/*[@viewBox="0 0 24 24"]//ancestor::div//a)[last()]'
            )
            .innerText();
          if (folderName === desiredFolderName) {
            await page
              .getByRole('row', { name: 'row' })
              .getByLabel('')
              .nth(index)
              .uncheck();
            rightOneNotPresent = false;
            break;
          }
        }
        await page
          .getByRole('row', { name: 'row' })
          .getByLabel('')
          .nth(index)
          .uncheck();
      }
      await page.locator('button[name="buttonBarInfo"]').click();
    });
  } else {
    rightOneNotPresent = true;
  }
  await page.getByTestId('CloseIcon').first().click();

  if (rightOneNotPresent) {
    await test.step(`Start adding if not found, attribute ${nameAttribute}`, async () => {
      await page.getByRole('link', { name: desiredFolderName }).click();
      await page.getByTestId('AddIcon').nth(1).click();
      await page.getByRole('button', { name: 'Kenmerk' }).click();
    });

    await test.step(`Fill name attribute ${nameAttribute}`, async () => {
      await page.getByPlaceholder('Naam kenmerk').click();
      await page.getByPlaceholder('Naam kenmerk').fill(nameAttribute);
      magicString !== nameAttribute
        ? await page
            .locator('//input[@name= "magic_string" ]')
            .fill(magicString)
        : '';
    });

    await test.step(`Fill public name attribute ${nameAttribute}`, async () => {
      await page.getByPlaceholder('Publieke naam').click();
      await page.getByPlaceholder('Publieke naam').fill(publicName);
    });

    await test.step(`Fill sensitive data attribute ${nameAttribute}`, async () => {
      sensitiveData ? await page.getByLabel('Gevoelig gegeven').check() : '';
    });

    await test.step(`Fill input type attribute ${nameAttribute}`, async () => {
      await page.getByPlaceholder('Maak een keuze…').click();
      await page.getByPlaceholder('Maak een keuze…').fill(inputType);
      await page.getByPlaceholder('Maak een keuze…').press('ArrowDown');
      await page.getByPlaceholder('Maak een keuze…').press('Enter');
    });

    if (
      inputType === 'Meervoudige keuze' ||
      inputType === 'Keuzelijst' ||
      inputType === 'Enkelvoudige keuze'
    ) {
      await test.step(`Fill choices attribute ${nameAttribute}`, async () => {
        for (const choice of choices) {
          await page.getByPlaceholder('Voeg toe').fill(`${choice}`);
          await page.getByRole('button', { name: 'Voeg optie toe' }).click();
        }
      });
    }

    if (inputType === 'Relatie') {
      await test.step(`Fill relation attribute ${nameAttribute}`, async () => {
        await page.getByPlaceholder('Maak een keuze…').nth(1).click();
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(1)
          .fill(relationType);
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(1)
          .press('ArrowDown');
        await page.getByPlaceholder('Maak een keuze…').nth(1).press('Enter');
        if (relationType === 'Object') {
          await page.getByPlaceholder('Maak een keuze…').nth(2).click();
          await page
            .getByPlaceholder('Maak een keuze…')
            .nth(2)
            .fill(objectType);
          await expect(page.getByText(objectType)).toBeVisible();
          await page
            .getByPlaceholder('Maak een keuze…')
            .nth(2)
            .press('ArrowDown');
          await page.getByPlaceholder('Maak een keuze…').nth(2).press('Enter');
        }
      });
    }

    await test.step(`Check multiple values, attribute ${nameAttribute}`, async () => {
      multipleValuesAllowed
        ? await page.getByLabel('Meerdere waarden toegestaan').check()
        : '';
    });

    if (inputType === 'Document') {
      await test.step(`Fill document attributes for attribute ${nameAttribute}`, async () => {
        await page.getByPlaceholder('Maak een keuze…').nth(1).click();
        await page.getByPlaceholder('Maak een keuze…').nth(1).fill(docCategory);
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(1)
          .press('ArrowDown');
        await page.getByPlaceholder('Maak een keuze…').nth(1).press('Enter');
        await page.getByPlaceholder('Maak een keuze…').nth(2).click();
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(2)
          .fill(docConfidentiality);
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(2)
          .press('ArrowDown');
        await page.getByPlaceholder('Maak een keuze…').nth(2).press('Enter');
        await page.getByPlaceholder('Maak een keuze…').nth(3).click();
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(3)
          .fill(docDirection);
        await page
          .getByPlaceholder('Maak een keuze…')
          .nth(3)
          .press('ArrowDown');
        await page.getByPlaceholder('Maak een keuze…').nth(3).press('Enter');
      });
    }

    await test.step(`Fill help text for attribute ${nameAttribute}`, async () => {
      await page.locator('textarea[name="help"]').click();
      await page.locator('textarea[name="help"]').fill(explanationInternal);
    });

    if (valueDefault !== '' && valueDefault !== 'undefined') {
      await test.step(`Fill default value attribute ${nameAttribute}`, async () => {
        await page.locator('textarea[name="value_default"]').click();
        await page.locator('textarea[name="value_default"]').fill(valueDefault);
      });
    }

    await test.step(`Fill commit message attribute ${nameAttribute}`, async () => {
      await page.locator('input[name="commit_message"]').click();
      await page
        .locator('input[name="commit_message"]')
        .fill('Created for the Automatic e2e tests');
    });

    await test.step(`Save attribute ${nameAttribute}`, async () => {
      await page.getByRole('button', { name: 'Opslaan' }).click();

      await expect(
        page.getByRole('link', { name: 'Catalogus' }).nth(0)
      ).toBeVisible();
    });
  }
};

interface WhatsChangedAndPublishProps {
  page: Page;
  whatsChangedArray: string[];
  publishText: string;
  testScriptName: string;
}
export const whatsChangedAndPublish = async ({
  page,
  whatsChangedArray,
  publishText,
  testScriptName,
}: WhatsChangedAndPublishProps) => {
  await test.step(`Goto finish ${testScriptName}`, async () => {
    await page
      .frameLocator('iframe[title="Beheer formulier"]')
      .getByRole('link', { name: 'afronden' })
      .click();
    await expect(
      page
        .frameLocator('iframe[title="Beheer formulier"]')
        .getByText('Componenten gewijzigd')
    ).toBeVisible();
  });

  await test.step(`Fill publishtext ${testScriptName}`, async () => {
    await page
      .frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('textbox')
      .click();
    await page
      .frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('textbox')
      .fill(publishText);
  });

  await test.step(`Check the changed ${testScriptName}`, async () => {
    for (let index = 0; index < whatsChangedArray.length; index++) {
      await page
        .frameLocator('iframe[title="Beheer formulier"]')
        .getByLabel(whatsChangedArray[index])
        .check();
    }
  });

  await test.step(`Publish changed ${testScriptName}`, async () => {
    await page
      .frameLocator('internal:attr=[title="Beheer formulier"i]')
      .getByRole('button', { name: 'Publiceren' })
      .click();
    await page.waitForLoadState('domcontentloaded');
  });
};
