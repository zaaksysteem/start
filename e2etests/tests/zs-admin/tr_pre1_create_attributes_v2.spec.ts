/* eslint-disable playwright/no-conditional-in-test */
/* eslint-disable playwright/expect-expect */
import { BrowserContext, Page, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import {
  navigateAdministration,
  checkOrAddAttribute,
  checkOrAddCategoryOrFolder,
} from './administration';
import { testrondeData } from '../zs-employee/testrondeData';

const { options, baseUrl } = testSettings;
const categoryAndFolder = 'Automatische E2E TESTRONDE';

test.describe.serial('Create attributes for E2E TESTRONDE @db1', () => {
  let page: Page;
  let context: BrowserContext;
  test.use({ storageState: authPathBeheerder });

  test.beforeAll(async ({ browser }) => {
    context = await browser.newContext(options);
    page = await context.newPage();

    await page.goto(baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  test(`Check and if not present make categoryAndFolder ${categoryAndFolder}`, async ({}) => {
    await navigateAdministration({ page, menuItem: 'Catalogus' });

    await checkOrAddCategoryOrFolder({
      page,
      categoryOrFolder: `${categoryAndFolder}`,
    });
  });

  Object.values(testrondeData)
    .filter(
      attribute =>
        attribute.magicString !== 'auto_relatie_kenmerk_testronde_v2_object'
    )
    .forEach(attribute => {
      //All the attributes for TESTRONDE are being made here
      test(`Check and if not present make attribute ${attribute.nameAttribute} @fill_empty_db`, async ({}) => {
        await checkOrAddAttribute({
          page,
          ...attribute,
          desiredFolderName: `${categoryAndFolder}`,
        });
      });
    });
});
