import { BrowserContext, Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { personDataEnvironment, BSN2 } from '../../testfiles/personData';
import { searchCaseInZsLogboek } from './../zs-person/searchCaseInZaakSysteem';
import { loginFormCheckPersonData } from './../zs-person/loginFormCheckPersonData';
import { fillOutFormMainTestCaseWithoutRules } from './../zs-form/fillOutFormMainTestCase';
import { locators } from '../../utils';
import { finishMainCase } from './../zs-employee/finishMainCase';

const { baseUrl, superLongTimeout } = testSettings;
const MainTestCase = 'Auto TESTRONDE V2';
const BSN = BSN2;
const person = personDataEnvironment[BSN];

test.describe
  .serial(`Person fills, applies and pays form ${MainTestCase}. Employee treats case @all @select`, () => {
  let formContext: BrowserContext;
  let formPage: Page;

  // caseNumber is set in loginForm and it is used to check the payment
  let caseNumber = '';

  test.beforeAll(async ({ browser }) => {
    // page context as person that is logged in with DigiD
    formContext = await browser.newContext(testSettings.options);
    formPage = await formContext.newPage();
  });

  test.afterAll(async () => {
    await formContext.close();
  });

  test(`Login, fill out form, submit and pay ${MainTestCase} for choice Nee`, async () => {
    await loginFormCheckPersonData({
      formPage,
      loginFormName: MainTestCase,
      person,
      testSettings,
    });

    await fillOutFormMainTestCaseWithoutRules({
      formPage,
      formName: MainTestCase,
    });

    await test.step('Submit form and save caseNumber', async () => {
      await formPage.getByRole('button', { name: 'Versturen' }).press('Enter');
      await expect(
        formPage.locator(locators.labelLeftOnForm('Zaaknummer'))
      ).toBeVisible({ timeout: superLongTimeout });

      caseNumber = await formPage
        .locator(locators.labelLeftOnForm('Zaaknummer'))
        .innerText();

      expect(caseNumber.length).toBeGreaterThan(0);
    });

    await test.step(`Pay ${MainTestCase} for choise Nee`, async () => {
      await formPage
        .getByRole('button', { name: 'Betalen', exact: true })
        .click();
      await formPage.getByRole('button', { name: 'iDEAL' }).click();
      await formPage.getByTitle('Selecteer uw bank').selectOption('9999+TST');
      await formPage.getByRole('button', { name: 'Ga verder' }).click();
      await formPage.getByRole('button', { name: 'SUCCESS' }).click();
      await expect(
        formPage.getByRole('heading', { name: ' Uw betaling is gelukt' })
      ).toBeVisible();
    });
  });

  test.describe
    .serial('Check and finish case in zaaksysteem @all @select', () => {
    test.use({ storageState: authPathBeheerder });

    test('Go to zaaksysteem as employee', async ({ page }) => {
      await test.step('Open zaaksysteem', async () => {
        await page.goto(baseUrl);
        await expect(
          page.getByRole('button', { name: 'Hoofdmenu openen' })
        ).toBeVisible();
      });

      await searchCaseInZsLogboek(page, caseNumber);

      await test.step(`Open case ${caseNumber}`, async () => {
        await page.getByRole('link', { name: caseNumber }).first().click();
      });

      await test.step(`Take case into treatment ${caseNumber}`, async () => {
        await page
          .getByRole('button', { name: 'In behandeling nemen', exact: true })
          .click();
      });

      await test.step(`Accept documents in case ${caseNumber}`, async () => {
        await page.getByRole('button', { name: 'Fase' }).hover();
        await page
          .getByRole('link', { name: ' Documenten in de wachtrij' })
          .click();
        await page.getByRole('button', { name: 'Accepteren' }).first().click();
        await page.getByRole('button', { name: 'Accepteren' }).first().click();
        await page.getByRole('button', { name: 'Accepteren' }).click();
        await page.getByLabel('Fasen').click();
      });

      await finishMainCase({ page, mainCaseTypeName: MainTestCase });
    });
  });
});
