import { BrowserContext, Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import {
  personDataEnvironment,
  BSN1,
  BSN2,
  Person,
} from '../../testfiles/personData';
import { loginFormCheckPersonData } from './loginFormCheckPersonData';
import { fillOutFormPaymentTest } from '../zs-form/fillOutFormPaymentTest';
import { searchCaseInZsLogboek } from './searchCaseInZaakSysteem';

const { superLongTimeout, personAuthentication, longTimeout } = testSettings;
export interface PaymentTests {
  paymentResult: string;
  person: Person;
  reason: string;
  amount: string;
  resultHeaderText: string;
  resultText1: string;
}

const loginFormName = personAuthentication.loginForm;
const paymentTests: PaymentTests[] = [
  {
    paymentResult: 'CANCELLED',
    person: personDataEnvironment[BSN2],
    reason:
      'Ik betaal dit bedrag NIET om het CANCELLED zijn van een betaling te testen',
    amount: '119,00',
    resultHeaderText: 'Betaalmethode geannuleerd',
    resultText1:
      'De betaling voor uw aanvraag is door u geannuleerd, de aanvraag wordt niet in behandeling genomen en is vernietigd.',
  },
  {
    paymentResult: 'FAILURE',
    person: personDataEnvironment[BSN1],
    reason: 'Ik betaal dit bedrag NIET om FAILURE van de betaling te testen',
    amount: '541,00',
    resultHeaderText: 'Betaling niet aanvaard',
    resultText1:
      'De betaling voor uw aanvraag is door u geannuleerd, de aanvraag wordt niet in behandeling genomen en is vernietigd.',
  },
];

paymentTests.forEach(paymentTest => {
  const {
    amount,
    paymentResult,
    reason,
    person,
    resultText1,
    resultHeaderText,
  } = paymentTest;
  const { baseUrl } = testSettings;

  test.describe
    .serial(`Person pays through form and Digid login. Employee treats case. Payment result: ${paymentResult} @all`, () => {
    let formContext: BrowserContext;
    let formPage: Page;

    // caseNumber is set in loginForm and it is used to check the payment
    let caseNumber = '';

    test.beforeAll(async ({ browser }) => {
      // page context as person that is logged in with DigiD
      formContext = await browser.newContext(testSettings.options);
      formPage = await formContext.newPage();
    });

    test.afterAll(async () => {
      await formContext.close();
    });

    test('Login form', async () => {
      await loginFormCheckPersonData({
        formPage,
        loginFormName,
        person,
        testSettings,
      });

      await expect(formPage.locator('h2')).toHaveText('Uitleg test');
    });

    test('Fill out form, submit and initiate payment', async () => {
      caseNumber = await fillOutFormPaymentTest({
        formPage,
        amount,
        reason,
        testSettings,
      });

      expect(caseNumber.length).toBeGreaterThan(0);
    });

    test(`Test payment, clicking on button ${paymentResult}`, async () => {
      await formPage.getByRole('button', { name: `${paymentResult}` }).click();

      await expect(
        formPage.getByRole('heading', { name: resultHeaderText })
      ).toBeVisible({ timeout: longTimeout });
      await expect(
        formPage.getByRole('button', { name: 'Annuleren' })
      ).toBeVisible();

      formPage.once('dialog', dialog => {
        void dialog.accept();
      });
      await formPage.getByRole('button', { name: 'Annuleren' }).click();

      await expect(formPage.getByRole('button', { name: 'OK' })).toBeVisible();
      await formPage.getByRole('button', { name: 'OK' }).click();

      await expect(formPage.getByText(`${resultText1}`)).toBeVisible();
    });

    test.describe
      .serial('Check case and payment result in zaaksysteem @all', () => {
      test.use({ storageState: authPathBeheerder });

      test('Open case in zaaksysteem and check if it is registered', async ({
        page,
      }) => {
        await test.step('Open zaaksysteem', async () => {
          await page.goto(baseUrl);
          await expect(
            page.getByRole('button', { name: 'Hoofdmenu openen' })
          ).toBeVisible();
        });

        await searchCaseInZsLogboek(page, caseNumber);

        await test.step(`Check case deleted logging for test ${paymentResult}`, async () => {
          await expect(
            page.getByText(`Zaak ${caseNumber} vernietigd door Ogone`).first()
          ).toBeVisible({ timeout: superLongTimeout });
        });

        await test.step(`Open deleted case for test ${paymentResult} and check error message`, async () => {
          await page
            .getByRole('link', { name: `${caseNumber}` })
            .first()
            .click();
          await expect(
            page.getByText(
              'Er ging iets mis bij het laden van de zaak. ' +
                'Neem contact op met uw beheerder voor meer informatie'
            )
          ).toBeVisible();
        });
      });
    });
  });
});
