import {
  FullResult,
  Reporter,
  TestCase,
  TestResult,
  TestStep,
  FullConfig,
  TestError,
} from '@playwright/test/reporter';
import { calcDuration } from '../utils/calcDuration';
import { testSettings } from '../playwright.config';

const { environment } = testSettings;

class MyReporter implements Reporter {
  onBegin(config: FullConfig) {
    console.log(`Starting the run on ${environment}`);
    console.log(`The config version: ${config.version}`);
  }

  onTestBegin(test: TestCase) {
    console.log(`Starting test ${test.title}`);
  }

  onStepEnd(test: TestCase, result: TestResult, step: TestStep): void {
    const milliseconds = step.duration;
    const durationFormatted =
      milliseconds >= 1000
        ? calcDuration({ milliseconds })
        : '0:00.' + milliseconds.toString();
    console.log(
      `The test case: ${test.title}, step: ${step.title}, duration: ${durationFormatted}`
    );
  }

  onTestEnd(test: TestCase, result: TestResult) {
    const milliseconds = result.duration;
    const durationFormatted =
      milliseconds >= 1000
        ? calcDuration({ milliseconds })
        : '0:00.' + milliseconds.toString();
    console.log(
      `Finished test ${test.title}: ${result.status}, duration: ${durationFormatted}`
    );
  }

  onError(error: TestError): void {
    console.log(
      `An error occured: ${error.message}, value: ${error.value}, stack: ${error.stack}`
    );
  }

  onEnd(result: FullResult) {
    console.log(`Finished the run: ${result.status}`);
  }
}
export default MyReporter;
