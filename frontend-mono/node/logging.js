// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import StatsD from 'hot-shots';

const client = new StatsD({
  port: process.env.STATSD_PORT || 9125,
  host: process.env.STATSD_HOST || '127.0.0.1',
  errorHandler: console.log,
});

export const increment = hostname => {
  client.increment(
    `zsnl_frontend.silo.${
      process.env.ZS_SILO_ID || 'unknown'
    }.${hostname.replace(/\./g, '_')}.frontend_request_count`
  );
};

export const cspReqestTiming = (hostname, requestFn) => {
  return client.asyncTimer(
    requestFn,
    `zsnl_frontend.silo.${
      process.env.ZS_SILO_ID || 'unknown'
    }.${hostname.replace(/\./g, '_')}.csp_request_duration`
  );
};
