// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fileURLToPath } from 'url';
import * as path from 'path';
import fs from 'fs-extra';

const { copySync } = fs;

const thisFileDirectory = path.dirname(fileURLToPath(import.meta.url));

export default () => {
  const p1 = path.resolve(thisFileDirectory, 'clienthtml');
  const p2 = path.resolve(thisFileDirectory, '..', 'build', 'apps');

  return {
    apply(compiler) {
      compiler.hooks.done.tap('PublicDirectoryPlugin', () => {
        copySync(p1, p2, {
          recursive: true,
        });
      });
    },
  };
};
