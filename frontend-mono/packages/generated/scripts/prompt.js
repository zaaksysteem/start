// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const inquirer = require('inquirer');
const readSchemaFile = require('./library/readSchemaFile');
const generateApiTypes = require('./generateApiTypes');

process.on('unhandledRejection', error => {
  throw error;
});

async function promptForSchema() {
  const schemas = await readSchemaFile();

  const { domainsToGenerate } = await inquirer.prompt({
    type: 'checkbox',
    name: 'domainsToGenerate',
    validate: input => Boolean(input.length),
    message: 'Which domain do you wish to regenerate?',
    choices: ['All domains', ...Object.keys(schemas.domains)],
  });

  if (domainsToGenerate.includes('All domains')) {
    return schemas.domains;
  }

  return domainsToGenerate.reduce(
    (acc, name) => ({
      ...acc,
      [name]: schemas.domains[name],
    }),
    {}
  );
}

async function promptForBranch() {
  const { branch } = await inquirer.prompt({
    type: 'list',
    name: 'branch',
    message: 'Which branch do you wish to use?',
    choices: ['development', 'master', 'preprod', 'other'],
  });

  if (branch === 'other') {
    const { customBranch } = await inquirer.prompt({
      type: 'input',
      name: 'customBranch',
      message: 'Please enter the branch name',
    });

    return customBranch;
  }

  return branch;
}

async function start() {
  const schemas = await promptForSchema();
  const branch = await promptForBranch();

  await generateApiTypes(schemas, branch);
}

start();
