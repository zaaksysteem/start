// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const fetchAndResolveApiSpecs = require('./fetchAndResolveApiSpecs');
const compileTypes = require('./compileTypes');
const writeTypes = require('./writeTypes');

process.on('unhandledRejection', error => {
  throw error;
});

function countFileTypes(entities) {
  return entities.reduce((acc, ent) => {
    const count = Array.isArray(ent) ? countFileTypes(ent) : 1;

    return acc + count;
  }, 0);
}

function countTypes(type) {
  const [, api_docs, entities] = type;

  return countFileTypes(api_docs) + countFileTypes(entities);
}

async function generateApiTypes(schemas, branch) {
  const startTime = Date.now();

  console.group(`Generating types from ${branch} for the following domains:`);
  Object.keys(schemas).map(item => console.log(` - ${item}`));

  const resolvedSchemas = await fetchAndResolveApiSpecs(branch, schemas);
  const types = await compileTypes(resolvedSchemas);

  console.groupEnd();

  const numTypes = types.reduce((acc, type) => acc + countTypes(type), 0);
  const time = `${(Date.now() - startTime) / 1000} seconds`;

  console.group(`\nDone: Successfully generated ${numTypes} types in ${time}`);

  await writeTypes(types, branch);

  console.groupEnd();
}

module.exports = generateApiTypes;
