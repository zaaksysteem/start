// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { signal } from '@preact/signals-react';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

type SnackbarProps = {
  message: string;
  link?: string;
  sx?: any;
  download?: boolean;
  autoHideDuration?: number | null;
  interpolations?: Object;
};

export const serverError = signal<V2ServerErrorsType | string | null>(null);

export const snackbarMessage = signal<SnackbarProps>({
  message: '',
});

export const openServerError = (err: V2ServerErrorsType | string) => {
  serverError.value = err;
};

export const openSnackbar = (snackbarProps: SnackbarProps | string) => {
  snackbarMessage.value =
    typeof snackbarProps === 'string'
      ? { message: snackbarProps }
      : snackbarProps;
};

export const snackbarMargin = { marginBottom: '45px' };
