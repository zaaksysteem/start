// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { APIJobs } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const cancelJob = async (job_uuid: string) =>
  request<APIJobs.CancelJobResponseBody>('POST', '/api/v2/jobs/cancel_job', {
    job_uuid,
  });

export const deleteJob = async (job_uuid: string) =>
  request<APIJobs.DeleteJobResponseBody>('POST', '/api/v2/jobs/delete_job', {
    job_uuid,
  });

export const createJob = async (payload: APIJobs.CreateJobRequestBody) =>
  request<APIJobs.DeleteJobResponseBody>(
    'POST',
    '/api/v2/jobs/create_job',
    payload
  );
