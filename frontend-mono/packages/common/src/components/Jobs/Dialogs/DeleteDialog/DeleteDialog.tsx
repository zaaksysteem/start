// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { useMutation } from '@tanstack/react-query';
import { default as jobsLocale } from '@zaaksysteem/common/src/components/Jobs/Jobs.locale';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { createJob } from '@zaaksysteem/common/src/components/Jobs/Jobs.requests';
import { useTranslation } from 'react-i18next';
import { FiltersType } from '../../../../../../../apps/main/src/modules/advancedSearch/AdvancedSearch.types.filters';
import { V2ServerErrorsType } from '../../../../types/ServerError';
import { KindType } from '../../../../../../../apps/main/src/modules/advancedSearch/AdvancedSearch.types';
import { rules, validationMap } from './DeleteDialog.library';
import locale from './DeleteDialog.locale';
import {
  getIntro,
  createPayload,
  getFormDefinition,
} from './DeleteDialog.library';

export type DeleteDialogPropsType = {
  kind: KindType;
  onClose: any;
  selectedRows: useSelectionBehaviourReturnType['selectedRows'];
  everythingSelected: useSelectionBehaviourReturnType['everythingSelected'];
  filters?: FiltersType | null;
};

/* eslint complexity: [2, 10] */
const DeleteDialog: FunctionComponent<DeleteDialogPropsType> = ({
  onClose,
  selectedRows,
  everythingSelected,
  filters,
  kind,
}) => {
  const [t] = useTranslation();
  const createMutation = useMutation<any, V2ServerErrorsType, any>(payload =>
    createJob(payload)
  );
  return (
    <FormDialog
      key={'copy-dialog'}
      icon="memory"
      open={true}
      title={t('deleteDialog:title')}
      fieldComponents={{
        intro: getIntro({ t, everythingSelected, selectedRows, kind }),
      }}
      formDefinition={getFormDefinition(t)}
      rules={rules}
      validationMap={validationMap}
      onClose={onClose}
      onSubmit={async (values: any) => {
        const { friendlyName, simulation } = values;
        createMutation.mutate(
          createPayload({
            filters,
            friendlyName,
            selectedRows,
            everythingSelected,
            kind,
            simulation,
          }),
          {
            onSuccess: () => {
              openSnackbar({
                message: t(`deleteDialog:types.delete.success`),
                sx: snackbarMargin,
              });
              onClose();
            },
            onError: openServerError,
          }
        );
      }}
    />
  );
};

export default (props: DeleteDialogPropsType) => (
  <I18nResourceBundle resource={jobsLocale} namespace="jobs">
    <I18nResourceBundle resource={locale} namespace="deleteDialog">
      <DeleteDialog {...props} />
    </I18nResourceBundle>
  </I18nResourceBundle>
);
