// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  Rule,
  showFields,
  hideFields,
} from '@zaaksysteem/common/src/components/form/rules';
import * as yup from 'yup';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { filtersToAPIFilters } from '../../../../../../../apps/main/src/modules/advancedSearch/query/useResults';
import { DeleteDialogPropsType } from './DeleteDialog';

export type DeleteDialogFormShape = {
  intro: string;
  friendlyName: string;
  simulation: boolean;
  approve: boolean;
};

export const rules = [
  new Rule()
    .when('simulation', field => field.value === true)
    .then(hideFields(['approve']))
    .else(showFields(['approve'])),
];

export const validationMap = {
  approve: ({ t }: { t: i18next.TFunction }) =>
    yup
      .boolean()
      .required('-')
      .oneOf([true], t('deleteDialog:fields.approve.error')),
};

export const getFormDefinition = (
  t: i18next.TFunction
): FormDefinition<DeleteDialogFormShape> => {
  const format = t('common:dates.dateFormatTextShort');
  return [
    {
      name: 'intro',
      type: 'intro',
      readOnly: true,
      value: '',
    },
    {
      name: 'friendlyName',
      type: fieldTypes.TEXT,
      value: `${t('jobs:jobTypes.delete_case')} - ${fecha.format(new Date(), format)}`,
      placeholder: t('deleteDialog:fields.friendlyName.placeholder'),
    },
    {
      name: 'simulation',
      type: fieldTypes.SWITCH,
      value: true,
      label: t('deleteDialog:fields.simulation.label'),
      suppressWrapperLabel: true,
    },
    {
      name: 'approve',
      type: fieldTypes.CHECKBOX,
      value: false,
      label: t('deleteDialog:fields.approve.label'),
      suppressWrapperLabel: true,
    },
  ];
};

export const createPayload = ({
  filters,
  friendlyName,
  selectedRows,
  everythingSelected,
  kind,
  simulation,
}: {
  kind: DeleteDialogPropsType['kind'];
  filters: DeleteDialogPropsType['filters'];
  friendlyName: string;
  everythingSelected: DeleteDialogPropsType['everythingSelected'];
  selectedRows: DeleteDialogPropsType['selectedRows'];
  simulation: boolean;
}) => {
  const getJobType = () => {
    if (kind === 'case') {
      return simulation ? 'simulate_delete_case' : 'delete_case';
    }
  };

  let selection = {};

  if (everythingSelected) {
    selection = {
      filters: filtersToAPIFilters(kind, filters),
      type: 'filter',
    };
  } else if (!everythingSelected && selectedRows.length) {
    selection = { id_list: selectedRows, type: 'id_list' };
  }

  return {
    job_description: {
      job_type: getJobType(),
      selection,
    },
    friendly_name: friendlyName,
  };
};

export const getIntro = ({
  t,
  everythingSelected,
  selectedRows,
  kind,
}: {
  t: i18next.TFunction;
  everythingSelected: DeleteDialogPropsType['everythingSelected'];
  selectedRows: DeleteDialogPropsType['selectedRows'];
  kind: DeleteDialogPropsType['kind'];
}) => {
  return () => {
    const kindTranslation = t(`common:kind.${kind}`, {
      count: selectedRows.length,
    });
    const amountTranslation = everythingSelected
      ? t(`deleteDialog:amountAll`, { kind: kindTranslation })
      : t('deleteDialog:amountNr', {
          kind: kindTranslation,
          nr: selectedRows.length,
        });

    const introTexts = [
      t('deleteDialog:intro.1', {
        amount: amountTranslation,
        action: t('deleteDialog:types.delete.action'),
      }),
    ];
    const introRest = Object.entries(
      t('deleteDialog:intro', { returnObjects: true })
    )
      .map(entry => entry[1])
      .filter((entry, index) => index !== 0);

    const full = [introTexts, ...introRest].map((text, index) => (
      <div key={index} style={{ marginBottom: 14 }}>
        {text as string}
      </div>
    ));

    return <div>{full}</div>;
  };
};
