// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { BreadCrumbItemType } from '../components/DocumentExplorer/types/types';
import {
  ItemType,
  DocumentItemType,
} from '../components/FileExplorer/types/FileExplorerTypes';

export const navigateAction = (id: string) => {
  return {
    type: 'navigate',
    payload: {
      id,
    },
  };
};

export const setLoadingAction = (loading: boolean) => {
  return {
    type: 'setLoading',
    payload: {
      loading,
    },
  };
};

export const doSearchAction = (search: string) => {
  return {
    type: 'doSearch',
    payload: {
      search,
    },
  };
};

export const setItemsAction = (
  items: ItemType[],
  search: string,
  path: BreadCrumbItemType[]
) => {
  return {
    type: 'setItems',
    payload: {
      items,
      path,
      search,
    },
  };
};

export const setSearchItemsAction = (result: DocumentItemType[]) => {
  return {
    type: 'setSearchItems',
    payload: {
      rows: result,
    },
  };
};

export const toggleSelectedAction = (uuid: string) => {
  return {
    type: 'toggleSelected',
    payload: {
      uuid,
    },
  };
};
