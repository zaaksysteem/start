// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DocumentItemType } from '../../../FileExplorer/types/FileExplorerTypes';
import { useRejectedStyles } from './Rejected.styles';

type RejectedButtonPropsType = {
  t: i18next.TFunction;
  rowData: DocumentItemType;
};
const Rejected: React.FunctionComponent<RejectedButtonPropsType> = ({
  t,
  rowData,
}) => {
  const classes = useRejectedStyles();
  const name = rowData.rejectionName?.replaceAll('"', '');
  const reason = rowData.rejectionReason;
  const rejected = t('DocumentExplorer:columns.name.rejected', {
    name,
    reason,
  });

  return (
    <Tooltip title={rejected} className={classes.wrapper}>
      <Icon size="small" color="inherit">
        {iconNames.warning}
      </Icon>
    </Tooltip>
  );
};

export default Rejected;
