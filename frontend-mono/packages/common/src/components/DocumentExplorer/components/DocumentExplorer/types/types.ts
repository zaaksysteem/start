// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ItemType } from '../../FileExplorer/types/FileExplorerTypes';
import { useDocumentExplorerStyles } from '../DocumentExplorer.style';

export type BreadCrumbItemType = {
  id: string;
  label: string;
};

export type StoreShapeType = {
  items: ItemType[];
  loading: boolean;
  path: BreadCrumbItemType[];
  location: ValidLocation;
  search: string;
  createDocumentsOpen: boolean;
};

export enum PresetLocations {
  Home = 'HOME',
  Search = 'SEARCH',
}

export type ValidLocation = PresetLocations | string;

export type ValidLocationOrNull = ValidLocation | null;

export type HandleItemClickType = (
  event: React.MouseEvent,
  item: BreadCrumbItemType
) => void;

export type ClassesType = ReturnType<typeof useDocumentExplorerStyles>;

export type FiltersType = { assignment?: (item: ItemType) => boolean };
