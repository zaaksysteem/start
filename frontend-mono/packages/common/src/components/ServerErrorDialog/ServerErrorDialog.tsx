// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import { login } from '@zaaksysteem/common/src/library/auth';
import { TranslatedErrorMessage } from '../../library/request/TranslatedErrorMessage';
import { serverError } from '../../signals';
import { translateKeyGlobally } from '../../library/translateKeyGlobally';
import Alert from '../dialogs/Alert/Alert';

export const ServerErrorDialog = () => {
  const err = serverError.value;
  const [t] = useTranslation();
  const [opened, setOpened] = React.useState(Boolean(err));

  useEffect(() => {
    setOpened(true);
  }, [err]);

  if (!err) {
    return null;
  }

  const title = t('dialog.error.title');

  const close = () => setOpened(false);
  const closeButton = { text: t('dialog.close'), action: close };

  const goToLogin = () => {
    login(window.location.pathname);
  };
  const loginButton = { text: t('login'), action: goToLogin };

  return (err as any).response?.status === 401 ? (
    <Alert
      primaryButton={loginButton}
      secondaryButton={closeButton}
      onClose={close}
      title={title}
      open={opened}
    >
      {t('serverErrors.status.401')}
    </Alert>
  ) : (
    <Alert
      primaryButton={closeButton}
      onClose={close}
      title={title}
      open={opened}
    >
      {(err as any).response ? (
        <TranslatedErrorMessage errorObj={err as any} />
      ) : (
        translateKeyGlobally(t, (err as any).message || err)
      )}
    </Alert>
  );
};

export default ServerErrorDialog;
