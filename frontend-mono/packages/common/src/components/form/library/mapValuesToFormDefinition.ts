// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormDefinition,
  PartialFormValuesType,
} from '../types/formDefinition.types';

export default function mapValuesToFormDefinition<Values>(
  values: PartialFormValuesType<Values>,
  fields: FormDefinition<Values>
): FormDefinition<Values> {
  return fields.map(item => {
    if (typeof values[item.name] !== 'undefined') {
      return {
        ...item,
        value: values[item.name],
      };
    }

    return item;
  });
}
