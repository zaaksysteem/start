// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormikErrors } from 'formik';
import { FormValuesType } from '../types';

export function mergeErrors<Values>(name: keyof Values & string) {
  return (errors: FormikErrors<FormValuesType<Values>>) => {
    const [firstError] = Object.entries(errors)
      .filter(([key, error]) => error && key.startsWith(name))
      .map<string>(([, error]) => error as string);

    return firstError;
  };
}
