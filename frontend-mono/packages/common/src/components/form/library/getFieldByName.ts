// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AnyFormDefinitionField,
  FormDefinition,
  FormShapeType,
} from '../types';

function findFieldByName<Values>(
  fields: AnyFormDefinitionField<Values>[],
  name?: keyof Values
): AnyFormDefinitionField<Values> {
  // Since we force the name to always be a keyof Values, we can "safely" assume this will always return a value
  return fields.find(
    item => item.name === name
  ) as AnyFormDefinitionField<Values>;
}

export function getAllFieldsAsObject<Values>(
  formDefinition: FormDefinition<Values>
) {
  const obj = {} as FormShapeType<keyof Values>;
  formDefinition.forEach(field => (obj[field.name] = field));

  return obj;
}

export function getFieldByName<Values>(
  formDefinition: FormDefinition<Values>,
  name?: keyof Values
): AnyFormDefinitionField<Values> {
  return findFieldByName<Values>(formDefinition, name);
}

export default getFieldByName;
