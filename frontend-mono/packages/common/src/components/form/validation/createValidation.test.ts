// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '../types/formDefinition.types';
import { createValidation, ValidationMap } from './createValidation';

describe('createValidation', () => {
  describe('Custom rules', () => {
    const validationMap: ValidationMap = {
      custom_field: jest.fn(),
    };
    const formDefinition: FormDefinition<{ custom_field: string }> = [
      {
        name: 'custom_field',
        format: 'text',
        required: true,
        value: '',
        type: 'custom',
      },
    ];
    const values = {
      custom_field: '',
    };

    test('Should be allowed', async () => {
      const schema = createValidation({
        formDefinition,
        validationMap,
        t: jest.fn().mockReturnValue({}),
      });
      await schema.validate(values);

      expect(validationMap.custom_field).toBeCalled();
    });
  });
});
