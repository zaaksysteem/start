// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  FormDefinitionNumberField,
  FormDefinitionTextField,
  FormDefinitionEmailField,
} from '../types/formDefinition.types';
import {
  createNumberSchema,
  createEmailSchema,
  createTextSchema,
  createArraySchema,
} from './createSchema';

describe("Validation schema's", () => {
  describe('Number schema', () => {
    const baseSchema: FormDefinitionNumberField = {
      format: 'number',
      name: 'test',
      value: 0,
    };
    const tests = {
      min: { input: 1, value: 2, expectation: false },
      max: { input: 10, value: 3, expectation: false },
      moreThan: { input: 1, value: 3, expectation: false },
      lessThan: { input: 10, value: 3, expectation: false },
    };

    Object.entries(tests).forEach(([key, testProps]) => {
      test(`Setting a ${key} rule should be allowed`, async () => {
        const schema = createNumberSchema({
          ...baseSchema,
          [key]: testProps.value,
        });
        const result = await schema.isValid(testProps.input);
        expect(result).toEqual(testProps.expectation);
      });
    });
  });

  describe('Email schema', () => {
    const baseSchema: FormDefinitionEmailField = {
      format: 'email',
      name: 'test',
      value: '',
    };
    const tests = {
      email: {
        valid: 'test@zaaksysteem.nl',
        invalid: 'test',
      },
      magic_string: {
        valid: '[[ test_magic_string ]]',
        invalid: 'test',
      },
    };

    Object.entries(tests).forEach(([key, testProps]) => {
      test(`Setting a ${key} value should be allowed`, async () => {
        const schema = createEmailSchema(
          {
            ...baseSchema,
            allowMagicString: true,
          },
          jest.fn() as i18next.TFunction
        );

        const validResult = await schema.isValid(testProps.valid);
        const invalidResult = await schema.isValid(testProps.invalid);

        expect(validResult).toEqual(true);
        expect(invalidResult).toEqual(false);
      });
    });
  });

  describe('Text schema', () => {
    const baseSchema: FormDefinitionTextField = {
      format: 'text',
      name: 'test',
      value: '',
    };
    const tests = {
      min: { input: 'abcd', value: 5, expectation: false },
      max: { input: 'abcd', value: 3, expectation: false },
    };

    Object.entries(tests).forEach(([key, testProps]) => {
      test(`Setting a ${key} rule should be allowed`, async () => {
        const schema = createTextSchema({
          ...baseSchema,
          [key]: testProps.value,
        });
        const result = await schema.isValid(testProps.input);
        expect(result).toEqual(testProps.expectation);
      });
    });
  });

  describe('Array schema', () => {
    const baseEmailField: FormDefinitionEmailField = {
      format: 'email',
      name: 'test',
      value: [''],
      multiValue: true,
      allowMagicString: true,
    };
    const emailSchema = createEmailSchema(
      baseEmailField,
      jest.fn() as i18next.TFunction
    );

    const tests = {
      no: {
        input: ['noreply@zaaksysteem.nl', 'test'],
        value: false,
        expectation: false,
      },
      minItems: {
        input: ['noreply@zaaksysteem.nl'],
        value: 2,
        expectation: false,
      },
      maxItems: {
        input: ['noreply@zaaksysteem.nl', 'test@zaaksysteem.nl'],
        value: 1,
        expectation: false,
      },
    };

    Object.entries(tests).forEach(([key, testProps]) => {
      test(`Setting a ${key} rule should be allowed`, async () => {
        const schema = createArraySchema(
          {
            ...baseEmailField,
            ...(testProps.value ? { [key]: testProps.value } : {}),
          },
          jest.fn() as i18next.TFunction
        );
        const result = await schema.of(emailSchema).isValid(testProps.input);
        expect(result).toEqual(testProps.expectation);
      });
    });
  });
});
