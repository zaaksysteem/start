// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAmountAndTypeStyles = makeStyles({
  wrapper: {
    display: 'flex',
    gap: 20,
  },
  amount: {
    minWidth: 100,
    maxWidth: 200,
  },
  type: {
    flexGrow: 1,
  },
});
