// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { useAmountAndTypeStyles } from './AmountAndType.style';

type AmountAndTypeType = {
  amount: string;
  type: string;
};

export const AmountAndType: FormFieldComponentType<AmountAndTypeType> = ({
  value,
  name,
  onChange,
  choices,
  readOnly,
}) => {
  const classes = useAmountAndTypeStyles();

  if (!choices) return <div>Bad config. Supply choices and value.</div>;

  const parsedValue: AmountAndTypeType = {
    amount: value?.amount || '',
    type: value?.type || choices[0],
  };

  const handleChange = (event: React.ChangeEvent<any>) => {
    const { name: fieldName, value: newValue } = event.target;

    onChange({
      target: {
        name,
        value:
          fieldName === 'amount'
            ? { amount: newValue, type: parsedValue.type }
            : { amount: parsedValue.amount, type: newValue },
      },
    });
  };

  const getReadOnlyValue = () =>
    value ? `${parsedValue.amount} ${parsedValue.type}` : undefined;

  return readOnly ? (
    <ReadonlyValuesContainer value={getReadOnlyValue()} />
  ) : (
    <div className={classes.wrapper}>
      <div className={classes.amount}>
        <TextField
          name="amount"
          displayType="number"
          onChange={handleChange}
          value={parsedValue.amount}
        />
      </div>
      <div className={classes.type}>
        <Select
          name="type"
          onChange={handleChange}
          choices={choices}
          value={parsedValue.type}
          isClearable={false}
          nestedValue={true}
        />
      </div>
    </div>
  );
};

export default AmountAndType;
