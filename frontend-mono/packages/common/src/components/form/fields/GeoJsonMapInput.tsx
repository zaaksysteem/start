// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';
import { FormFieldComponentType } from '../types/Form2.types';

export const GeoJsonMapInput: FormFieldComponentType<
  GeoJSON.GeoJsonObject,
  { context: IntegrationContextType }
> = ({ value, name, onChange, readOnly, config: { context } }) => {
  return (
    <GeoMap
      geoFeature={value}
      name={name}
      context={context}
      onFeatureDraw={feature => onChange({ target: { value: feature, name } })}
      canDrawFeatures={!readOnly}
      minHeight={370}
    />
  );
};
