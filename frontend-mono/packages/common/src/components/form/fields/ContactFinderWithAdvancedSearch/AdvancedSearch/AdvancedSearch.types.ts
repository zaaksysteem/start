// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

export type ContactTypeType = 'person' | 'organization';
export type ContactTypeV1Type = 'person' | 'company';

type IntegrationRowType = {
  instance: {
    id: number;
    interface_config: {
      search_form_title: string;
      gbav_search: number;
      local_search: number;
      search_extern_webform_only: number;
      gbav_search_role_restriction: number;
    };
  };
};

export type StufConfigIntegrationsResponseBodyType = {
  result: { instance: { rows: IntegrationRowType[] | null } };
};

export type FetchStufConfigIntegrationsType =
  () => Promise<StufConfigIntegrationsResponseBodyType>;

export type StufConfigIntegrationType = { label: string; value: string };

export type FilterIntegrationsType = (
  rows: IntegrationRowType[],
  session: SessionType
) => IntegrationRowType[];

export type FormatIntegrationsType = (
  rows: IntegrationRowType[]
) => StufConfigIntegrationType[];

export type GetStufConfigIntegrationsType = (
  session: SessionType
) => Promise<StufConfigIntegrationType[]>;

export type PersonFields = {
  searchIn: string; // integration number
  bsn: string;
  sedulaNumber: string;
  dateOfBirth: string;
  familyName: string;
  prefix: string;
  zipCode: string;
  streetNumber: string;
  streetNumberLetter: string;
  suffix: string;
};

export type OrganizationFields = {
  searchIn: string;
  rsin: string;
  cocNumber: string;
  cocLocationNumber: string;
  tradeName: string;
  street: string;
  zipCode: string;
  number: string;
  letter: string;
  suffix: string;
};

type GenderType = 'male' | 'female' | 'perm_identity';
type GenderV1Type = 'M' | 'V' | null;
export type ValuesType = PersonFields | OrganizationFields;

export type ResultType = {
  uuid: string;
  type: ContactTypeType;
  integrationId: string;
  name: string;
  gender: GenderType;
  isDeceased: boolean;
  isSecret: boolean;
  hasCorrespondenceAddress: boolean;
  cocNumber: string | null;
  cocLocationNumber: string | null;
  address: string | null;
  rawResponse: ResultRowType;
};

export type ResultsType = ResultType[] | null;

export type PerformImportContactType = (
  contactToImport: ResultType,
  contactType: ContactTypeType
) => Promise<ImportContactResponseBodyType>;

export type ImportContactType = (
  data: any,
  id: string,
  contactType: ContactTypeType
) => Promise<ImportContactResponseBodyType>;

type CountryType = { instance: { label: string } };

type AddressType = {
  instance: {
    street: string | null;
    street_number: string | null;
    street_number_letter: string | null;
    street_number_suffix: string | null;
    zipcode: string | null;
    municipality: string | null;
    foreign_address_line1: string | null;
    foreign_address_line2: string | null;
    foreign_address_line3: string | null;
    country: CountryType;
  };
};

type SubjectType = {
  reference: string | null;
  instance: {
    date_of_birth: string;
    date_of_death: string | null;
    is_secret: 1 | null;
    gender: GenderV1Type;
    coc_number: string;
    coc_location_number: string;
    address_residence: AddressType;
    address_correspondence: AddressType;
  };
};

type ResultRowType = {
  instance: {
    display_name: string;
    subject_type: ContactTypeV1Type;
    subject: SubjectType;
  };
  reference: string;
};

export type SearchResponseBodyType = {
  result: { instance: { rows: ResultRowType[] } };
};

export type ImportContactResponseBodyType = {
  result: ResultRowType;
};

type DataType = {
  query: {
    match: {
      subject_type: ContactTypeV1Type;
      [key: string]: any;
    };
  };
};

export type SearchType = (
  data: DataType,
  id: string | null
) => Promise<SearchResponseBodyType>;

export type PerformSearchType = (
  contactType: ContactTypeType,
  values: ValuesType
) => Promise<ResultType[]>;

export type FormatGenderType = (genderLetter: GenderV1Type) => GenderType;

export type FormatResultsType = (
  rows: ResultRowType[],
  integrationId: string
) => ResultType[];

export type GetPersonFormDefinitionType = (
  t: i18next.TFunction,
  stufConfigIntegrations: StufConfigIntegrationType[]
) => AnyFormDefinitionField[];

export type GetPersonRulesType = (
  countryCode: string,
  stufConfigIntegrations: StufConfigIntegrationType[]
) => any;

export type GetOrganizationFormDefinitionType = (
  t: i18next.TFunction
) => AnyFormDefinitionField[];

export type GetOrganizationRulesType = (hasKvKApi: boolean) => any;

export type GetRulesType = (formType: ContactTypeType) => any[];
