// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  setRequired,
  setOptional,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { COUNTRY_CODE_CURACAO } from '@zaaksysteem/common/src/constants/countryCodes.constants';
import { findField } from '../AdvancedSearch.library';
import {
  GetPersonFormDefinitionType,
  GetPersonRulesType,
  PersonFields,
} from './../AdvancedSearch.types';
import { getRequiredPersonFields } from './Form.library';

export const getFormDefinition: GetPersonFormDefinitionType = (
  t,
  stufConfigIntegrations
) => [
  {
    name: 'searchIn',
    label: `${t('fields.searchIn')}:`,
    type: fieldTypes.FLATVALUE_SELECT,
    value: 'intern',
    choices: [
      { label: t('integration.searchIn.intern'), value: 'intern' },
      ...stufConfigIntegrations,
    ],
    required: false,
    isClearable: false,
  },
  {
    name: 'bsn',
    label: t('fields.bsn'),
    placeholder: t('fields.bsn'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'sedulaNumber',
    label: t('fields.sedulaNumber'),
    placeholder: t('fields.sedulaNumber'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'dateOfBirth',
    label: t('fields.dateOfBirth'),
    type: fieldTypes.DATEPICKER,
    value: null,
    required: false,
  },
  {
    name: 'familyName',
    label: t('fields.familyName'),
    placeholder: t('fields.familyName'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'prefix',
    label: t('fields.prefix'),
    placeholder: t('fields.prefix'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'zipCode',
    label: t('fields.zipCode'),
    placeholder: t('fields.zipCode'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'streetNumber',
    label: t('fields.streetNumber'),
    placeholder: t('fields.streetNumber'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'streetNumberLetter',
    label: t('fields.streetNumberLetter'),
    placeholder: t('fields.streetNumberLetter'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
  {
    name: 'suffix',
    label: t('fields.streetNumberSuffix'),
    placeholder: t('fields.streetNumberSuffix'),
    type: fieldTypes.TEXT,
    value: '',
    required: false,
  },
];

/* eslint complexity: [2, 24] */
const personFormValidations = (
  fields: AnyFormDefinitionField[],
  countryCode: string
) => {
  const allRequiredFields = getRequiredPersonFields(countryCode).flat();
  const bsn = findField(fields, 'bsn');
  const sedulaNumber = findField(fields, 'sedulaNumber');
  const familyName = findField(fields, 'familyName');
  const dateOfBirth = findField(fields, 'dateOfBirth');
  const zipCode = findField(fields, 'zipCode');
  const streetNumber = findField(fields, 'streetNumber');

  const allRequiredFieldsAreEmpty =
    !bsn.value &&
    !sedulaNumber.value &&
    !familyName.value &&
    !dateOfBirth.value &&
    !zipCode.value &&
    !streetNumber.value;

  if (allRequiredFieldsAreEmpty) {
    fields = setRequired(allRequiredFields)(fields);
  } else {
    fields = setOptional(allRequiredFields)(fields);
  }

  const formIsValid =
    bsn.value ||
    sedulaNumber.value ||
    (familyName.value && dateOfBirth.value) ||
    (zipCode.value && streetNumber.value);

  if (bsn.value) {
    fields = setRequired(['bsn'])(fields);
  }

  if (sedulaNumber.value) {
    fields = setRequired(['sedulaNumber'])(fields);
  }

  if (
    (dateOfBirth.value && familyName.value) ||
    (!formIsValid && (dateOfBirth.value || familyName.value))
  ) {
    fields = setRequired(['dateOfBirth', 'familyName'])(fields);
  }

  if (
    (zipCode.value && streetNumber.value) ||
    (!formIsValid && (zipCode.value || streetNumber.value))
  ) {
    fields = setRequired(['zipCode', 'streetNumber'])(fields);
  }

  return fields;
};

export const getRules: GetPersonRulesType = (
  countryCode,
  stufConfigIntegrations
) => [
  new Rule<PersonFields>()
    .when(() => true)
    .then(fields => personFormValidations(fields, countryCode)),
  new Rule<PersonFields>()
    .when(() => Boolean(stufConfigIntegrations.length))
    .then(showFields(['searchIn']))
    .else(hideFields(['searchIn'])),
  new Rule<PersonFields>()
    .when(() => countryCode !== COUNTRY_CODE_CURACAO)
    .then(hideFields(['sedulaNumber']))
    .then(setOptional(['sedulaNumber'])),
];
