// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useAdvancedSearchStyles } from '../AdvancedSearch.styles';
import { ContactTypeType, ResultsType } from '../AdvancedSearch.types';
import {
  getRequiredOrganizationFields,
  getRequiredPersonFields,
} from '../Form/Form.library';
import { getOrganizationColumns, getPersonColumns } from './Results.columns';

export type ResultsPropsType = {
  formType: ContactTypeType;
  countryCode: string;
  results: ResultsType;
  onRowClick: (event: any) => any;
};

const Results: React.ComponentType<ResultsPropsType> = ({
  formType,
  countryCode,
  results,
  onRowClick,
}) => {
  const classes = useAdvancedSearchStyles();
  const [t] = useTranslation('ContactFinderWithAdvancedSearch');
  const contactType = t(`common:entityType.${formType}`).toLowerCase();

  const requiredFields =
    formType === 'person'
      ? getRequiredPersonFields(countryCode)
      : getRequiredOrganizationFields();

  const columns =
    formType === 'person'
      ? getPersonColumns(t, classes)
      : getOrganizationColumns(t, classes);

  if (!results)
    return (
      <div className={classes.placeholder}>
        <span>{`${t('results.searchFor', { contactType })}:`}</span>
        <div className={classes.filters}>
          {requiredFields.map((group, index) => (
            <div key={index} className={classes.filters}>
              {index !== 0 && (
                <span className={classes.or}>{t('common:or')}</span>
              )}
              <span>
                {group.map(filter => t(`fields.${filter}`)).join(' + ')}
              </span>
            </div>
          ))}
        </div>
      </div>
    );

  if (results.length === 0)
    return (
      <div className={classes.placeholder}>
        <span>{t('results.noResults')}</span>
      </div>
    );

  return (
    <div className={classes.table}>
      <React.Suspense>
        <SortableTable
          columns={columns}
          rows={results}
          onRowClick={onRowClick}
          noRowsMessage={t('noResults')}
        />
      </React.Suspense>
    </div>
  );
};

export default Results;
