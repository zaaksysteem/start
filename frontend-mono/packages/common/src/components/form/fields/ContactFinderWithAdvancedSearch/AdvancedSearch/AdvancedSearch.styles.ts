// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAdvancedSearchStyles = makeStyles(
  ({
    typography,
    mintlab: { greyscale },
    palette: { common, coral, primary },
  }: Theme) => {
    return {
      wrapper: {
        display: 'flex',
        gap: 20,
      },
      searchWrapper: {
        width: '50%',
        display: 'flex',
        flexDirection: 'column',
        gap: 20,
      },
      searchHeader: {
        display: 'flex',
        gap: 20,
      },
      formWrapper: {
        display: 'flex',
        flexDirection: 'column',
        gap: 20,
      },
      submit: {
        display: 'flex',
        justifyContent: 'flex-end',
      },
      resultsWrapper: {
        width: '50%',
        display: 'flex',
      },
      placeholder: {
        margin: 'auto',
        marginTop: 60,
        padding: 20,
        borderRadius: 8,
        width: 300,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        gap: 30,
        border: `1px solid ${greyscale.main}`,
      },
      filters: {
        display: 'flex',
        flexDirection: 'column',
        gap: 10,
      },
      or: {
        fontWeight: typography.fontWeightBold,
      },
      table: {
        flex: 1,
      },
      icon: {
        color: common.black,
      },
      nameCell: {
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        gap: 5,
        '&>* :last-child': {
          color: coral.darker,
          marginLeft: 6,
          maxWidth: 'min-content',
        },
      },
      name: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
      },
      chipRoot: {
        '&&': {
          padding: '10px 5px 10px 15px',
          borderRadius: 20,
          backgroundColor: greyscale.main,
          '&:hover': {
            backgroundColor: greyscale.darkest,
          },
        },
      },
      chipActive: {
        '&&': {
          backgroundColor: primary.main,
          '&:hover': {
            backgroundColor: primary.main,
          },
          cursor: 'default',
        },
      },
      chipIcon: {
        color: 'white',
      },
      chipLabel: { color: 'white' },
    };
  }
);
