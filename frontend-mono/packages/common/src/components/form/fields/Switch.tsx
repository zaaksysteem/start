// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

/* eslint complexity: [2, 7] */
const SwitchCmp: FormFieldComponentType<any> = props => {
  const { value, readOnly } = props;
  const [t] = useTranslation();

  if (readOnly) {
    return <ReadonlyValuesContainer value={value ? t('yes') : t('no')} />;
  } else {
    return (
      <FormControlLabel
        control={<Switch {...props} value={value || null} />}
        label={props.label}
      />
    );
  }
};

export default SwitchCmp;
