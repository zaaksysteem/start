// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select, {
  renderOptionWithIcon,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { usePrevious } from '@zaaksysteem/common/src/hooks/usePrevious';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchContactChoices } from './ContactFinder.library';

const ContactFinder: FormFieldComponentType<ValueType<string>, any> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  const { choices } = restProps;
  const [internalChoices, setInternalChoices] = useState([] as any);
  const [loading, setLoading] = useState(false);
  const prev = usePrevious(choices);

  const fetchContactChoicesFunction = fetchContactChoices(config);

  const [fetchChoicesDebounced] = useDebouncedCallback(
    async (ev: any, input: string) => {
      const keyword = input.trim();

      if (keyword.length >= 3) {
        setLoading(true);
        const result = (await fetchContactChoicesFunction(input)) || [];
        setInternalChoices(result);
        setLoading(false);
        return result;
      }
    },
    200
  );

  useEffect(() => {
    if (choices && prev !== choices) {
      setInternalChoices(choices);
    }
  }, [choices]);

  const iconMapping = {
    organization: iconNames.domain,
    employee: iconNames.badge,
    person: iconNames.person,
  } as any;

  const determineIcon = (option: any) => iconMapping[option.type];
  const determineZsIconName = (option: any) =>
    `entityType.inverted.${option.type}`;

  return (
    <Select
      {...restProps}
      value={value}
      choices={internalChoices}
      loading={loading}
      isMulti={multiValue}
      onInputChange={fetchChoicesDebounced}
      renderTags={renderTagsWithIcon(determineIcon)}
      renderOption={renderOptionWithIcon(determineZsIconName)}
      filterOption={() => true}
    />
  );
};

export default ContactFinder;
