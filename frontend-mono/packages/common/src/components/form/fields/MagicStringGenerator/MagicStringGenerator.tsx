// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { generateMagicString } from './MagicStringGenerator.library';

export const MAGIC_STRING_GENERATOR = 'msg';

const MagicStringGenerator: FormFieldComponentType<
  string,
  { rawName?: string }
> = props => {
  const rawName = props?.config?.rawName;

  React.useEffect(() => {
    rawName &&
      generateMagicString(rawName).then(ms => {
        props.setFieldValue(props.name, ms, false);
      });
  }, [rawName]);

  return <TextField {...props} />;
};

export default MagicStringGenerator;
