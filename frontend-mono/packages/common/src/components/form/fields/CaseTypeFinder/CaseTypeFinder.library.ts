// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { useDebouncedCallback } from 'use-debounce';
import { useQuery } from '@tanstack/react-query';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement, APIAdmin } from '@zaaksysteem/generated';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';

export type CaseTypeType = {
  id: string;
  name: string;
  initiator_source: string;
  type_of_requestors: (
    | 'niet_natuurlijk_persoon'
    | 'preset_client'
    | 'natuurlijk_persoon_na'
    | 'natuurlijk_persoon'
    | 'medewerker'
  )[];
  preset_requestor: ValueType<string> | null;
};

export interface CaseTypeOptionType extends ValueType<string> {
  fetched?: boolean;
  data: CaseTypeType;
}

export const fetchCaseType = async (
  case_type_uuid: string
): Promise<CaseTypeType> => {
  const body =
    await request<APICaseManagement.GetCaseTypeActiveVersionResponseBody>(
      'GET',
      buildUrl<APICaseManagement.GetCaseTypeActiveVersionRequestParams>(
        '/api/v2/cm/case_type/get_active_version',
        { case_type_uuid }
      )
    ).catch(openServerError);

  return body && body?.data
    ? {
        id: case_type_uuid,
        name: body.data.attributes.name,
        type_of_requestors: body.data.attributes.requestor.type_of_requestors,
        initiator_source: body.data.attributes.initiator_source,
        preset_requestor:
          body.data.relationships?.preset_requestor?.data || null,
      }
    : {
        id: '',
        name: '',
        type_of_requestors: [],
        initiator_source: '',
        preset_requestor: null,
      };
};

export const saveCaseTypeToRemember = (caseTypeUuid?: string) => {
  const remember = Boolean(caseTypeUuid);

  // It might be ok not to implement ServerErrorDialog here
  request('POST', '/api/user/settings', {
    remember_casetype: {
      casetype_id: caseTypeUuid,
      remember,
    },
  });
};

export const useCaseTypeChoicesQuery = (
  includeOffline: boolean,
  t: i18next.TFunction,
  type?: SubjectTypeType
) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= 3) setInput(value);
  }, 500);
  const enabled = Boolean(input);

  const getChoices = async (keyword: string) => {
    const res = await request<APIAdmin.SearchCatalogResponseBody>(
      'GET',
      buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
        keyword,
        type: 'case_type' as any,
        'filter[relationships.case_type.online]': includeOffline
          ? ('online,offline' as any)
          : 'online',
        ...(type && {
          'filter[relationships.case_type.requestor_type]': type,
        }),
      })
    ).catch(openServerError);

    return res
      ? res.data.map(
          ({
            id,
            meta: { summary },
            attributes: { is_active, description },
          }) => {
            const offline = !is_active ? t('common:state.offline') : '';
            const subLabel = [offline, description].filter(Boolean).join(' - ');

            return {
              value: id,
              data: {
                id,
                name: summary,
                type_of_requestors: [],
                initiator_source: '',
                preset_requestor: null,
              },
              label: summary,
              subLabel,
              fetched: false,
            };
          }
        )
      : [];
  };

  const data = useQuery(
    ['caseTypes', input, includeOffline] as const,
    ({ queryKey: [__, keyword] }) => getChoices(keyword || ''),
    { enabled }
  );

  return {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && debouncedCallback(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };
};
