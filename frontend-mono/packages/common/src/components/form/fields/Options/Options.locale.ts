// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    namePlaceholder: 'Voeg toe',
    duplicateName: 'Deze naam is reeds in gebruik. Vul een andere naam in.',
    addOption: 'Voeg optie toe',
    deleteOption: 'Verwijder optie',
    hideInactive: 'Verberg inactieve mogelijkheden',
    showInactive: 'Toon inactieve mogelijkheden',
  },
};
