// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useOptionsListStylesheet = makeStyles(
  ({ palette: { primary } }: Theme) => ({
    list: {
      borderRadius: '5px',
      transition: 'background-color 0.3s ease',
    },
    draggingOver: {
      backgroundColor: primary.lightest,
    },
    hidden: {
      display: 'none',
    },
  })
);
