// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import LoginWrapper from './LoginWrapper';

export { navToLogin } from './LoginWrapper.library';
export default LoginWrapper;
