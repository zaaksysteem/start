// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import useSession from './useSession';

export {
  hasActiveIntegration,
  hasCapability,
  hasSystemRole,
} from './useSession.helpers';
export type { SessionType, SignatureUploadRoleType } from './useSession.types';
export default useSession;
