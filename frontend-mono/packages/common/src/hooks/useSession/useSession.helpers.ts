// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  ActiveIntegrationType,
  CapabilitiesType,
  SessionType,
  SystemRoleType,
} from './useSession.types';

export const hasActiveIntegration = (
  session: SessionType,
  integration: ActiveIntegrationType
) => session.active_interfaces.includes(integration);

export const hasCapability = (
  session: SessionType,
  capability: CapabilitiesType
) => session.logged_in_user.capabilities.includes(capability);

export const hasSystemRole = (
  session: SessionType,
  systemRole: SystemRoleType
) => session.logged_in_user.system_roles.includes(systemRole);
