// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useCallback, useEffect } from 'react';
import type { BlockerFunction } from 'react-router-dom';
import { useBeforeUnload, useBlocker } from 'react-router-dom';

const useBlockNavigation = (when: boolean, allowedRoute?: string) => {
  // browser navigation
  useBeforeUnload(
    useCallback(
      (event: BeforeUnloadEvent) => {
        if (when) {
          event.preventDefault();
          return (event.returnValue = '');
        }
      },
      [when]
    )
  );

  // internal navigation
  let shouldBlock = useCallback<BlockerFunction>(
    //@ts-ignore
    ({ nextLocation }) => {
      const withinAllowedRoute = allowedRoute
        ? nextLocation.pathname.includes(allowedRoute)
        : false;

      return when && !withinAllowedRoute;
    },
    [when]
  );

  let blocker = useBlocker(shouldBlock);

  const resolvedBlock = (proceed?: boolean) => {
    if (blocker.proceed) {
      if (proceed) {
        blocker.proceed();
      } else {
        blocker.reset();
      }
    }
  };

  useEffect(() => {
    if (blocker.state === 'blocked' && !when) {
      resolvedBlock();
    }
  }, [blocker, when]);

  const blocked = blocker.state === 'blocked';

  return { blocked, resolvedBlock };
};

export default useBlockNavigation;
