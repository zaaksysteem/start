// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useEffect, useState, Dispatch, SetStateAction } from 'react';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  TableRowProps,
  RowMouseEventHandlerParams,
  SortableTablePropsType,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';

type OnPageSelectFunctionType = (
  event: React.MouseEvent | React.ChangeEvent,
  isSelected: boolean,
  selectedRows: string[]
) => void;

type OnSelectEverythingFunctionType = (event: React.MouseEvent<any>) => void;

export type UseSelectionBehaviourPropsType = {
  afterPageSelected?: OnPageSelectFunctionType;
  afterRowClick?: (params: RowMouseEventHandlerParams) => void;
  afterPageParamChange?: () => void;
  afterEverythingSelected?: (
    event: React.MouseEvent<any>,
    selectedRows: boolean
  ) => void;
  page?: number;
  resultsPerPage?: number;
  selectEverythingTranslations?: SortableTablePropsType['selectEverythingTranslations'];
  rows?: any[];
};

export type useSelectionBehaviourReturnType = {
  selectedRows: string[];
  setSelectedRows: Dispatch<SetStateAction<string[]>>;
  pageSelected: boolean;
  onRowClick: TableRowProps['onRowClick'];
  onSelectPage: OnPageSelectFunctionType;
  everythingSelected: boolean;
  setEverythingSelected: Dispatch<SetStateAction<boolean>>;
  onSelectEverything: OnSelectEverythingFunctionType;
  selectEverythingTranslations: SortableTablePropsType['selectEverythingTranslations'];
  selectable: boolean;
  resetAll: () => void;
};

const hasValue = (value: any) => value !== undefined && value !== null;

export const useSelectionBehaviour = ({
  afterPageSelected,
  afterRowClick,
  afterPageParamChange,
  afterEverythingSelected,
  page,
  resultsPerPage,
  selectEverythingTranslations,
  rows = [],
}: UseSelectionBehaviourPropsType): useSelectionBehaviourReturnType => {
  const [lastClicked, setLastClicked] = useState<string | null>(null);
  const [selectedRows, setSelectedRows] = useState<string[]>([]);
  const [everythingSelected, setEverythingSelected] = useState(false);

  const rowUuids = rows.map(row => row.uuid);

  useEffect(() => {
    if (hasValue(page) && hasValue(resultsPerPage)) {
      resetAll();
      afterPageParamChange && afterPageParamChange();
    }
  }, [page, resultsPerPage]);

  useEffect(() => setEverythingSelected(false), [selectedRows]);

  const onSelectPage: OnPageSelectFunctionType = (
    event,
    isSelected,
    selected
  ) => {
    if (isSelected) {
      setEverythingSelected(false);
      setSelectedRows(selected);
    } else {
      setSelectedRows([]);
    }
    afterPageSelected && afterPageSelected(event, isSelected, selected);
  };

  const onSelectEverything: OnSelectEverythingFunctionType = event => {
    setEverythingSelected(!everythingSelected);
    afterEverythingSelected &&
      afterEverythingSelected(event, !everythingSelected);
  };

  const onRowClick: TableRowProps['onRowClick'] = (
    params: RowMouseEventHandlerParams
  ) => {
    const {
      rowData: { uuid },
      event: { shiftKey },
    } = params;

    const checked = !selectedRows.includes(uuid);

    const [shiftStart, shiftEnd] = rowUuids.reduce(
      (acc, rowUuid, index) =>
        rowUuid === uuid || rowUuid === lastClicked ? [...acc, index] : acc,
      []
    );

    const newSelected = rowUuids.filter((rowUuid, index) =>
      rowUuid === uuid || (shiftKey && index >= shiftStart && index <= shiftEnd)
        ? checked
        : selectedRows.includes(rowUuid)
    );

    setLastClicked(uuid);
    setSelectedRows(newSelected);

    afterRowClick && afterRowClick(params);
  };

  const resetAll = () => {
    setSelectedRows([]);
    setEverythingSelected(false);
  };

  const isPageSelected = () => {
    if (!isPopulatedArray(rows)) return false;

    return rows.length === selectedRows.length;
  };

  return {
    selectedRows,
    setSelectedRows,
    pageSelected: isPageSelected(),
    onSelectPage,
    onRowClick,
    everythingSelected,
    setEverythingSelected,
    onSelectEverything,
    selectEverythingTranslations,
    selectable: true,
    resetAll,
  };
};
