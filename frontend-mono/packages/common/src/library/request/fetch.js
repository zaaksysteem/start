// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/*
 Generic client module for Zaaksysteem API transactions.
 There MUST NOT be knowledge of the Zaaksysteem client *application* here.
*/
import { dictionary } from '@mintlab/kitchen-sink/source';
import { cachedFetch } from './cachedFetch';

const { assign, keys } = Object;

/**
 * Unquoted RFC 6265 `cookie-value`.
 *
 * @type {string}
 */
const cookieValue = "[\\w!#$%&'()*+./:<=>?@[\\]^`{|}~-]+";

/**
 * @type {RegExp}
 */
const CSRF_EXPRESSION = new RegExp(
  `(?:^|;)\\s?XSRF-TOKEN\\s?=\\s?(${cookieValue})\\s?(?:;|$)`
);

/**
 * Get a custom CSRF HTTP header from the related cookie (if any).
 *
 * CSRF is currently the only scenario where we read a cookie client-side.
 * Libraries (rightfully) deal with the entire historical mess attached
 * to the problem domain, but we only deal with our own API here.
 *
 * @return {Object|null}
 */
export function getCsrfHeaders() {
  const { cookie } = document;
  const matches = CSRF_EXPRESSION.exec(cookie);

  if (matches) {
    const [, backReference] = matches;

    return {
      ['X-XSRF-TOKEN']: decodeURIComponent(backReference),
    };
  }

  return null;
}

/**
 * @type {Object}
 */
const baseInit = {
  credentials: 'same-origin',
  headers: {
    Accept: 'application/json',
  },
};

/**
 * @param {Object} init
 * @return {Object}
 */
export const getInit = (url, init) => {
  let internalCall;

  try {
    const newUrl = new URL(url, window.location.origin);

    // we need to determine whether a call is internal or external
    // if the url is of format `/path/name?search=params` it will pass first check
    // if the url is of format `https://origin.zaaksysteem.nl/path/name?search=params` it will pass the second check
    // if the url is of format `https://third.party.nl/path/name?search=params` it will fail both checks

    internalCall =
      url === newUrl.pathname + newUrl.search ||
      new URL(url).origin === window.location.origin;
  } catch (er) {
    internalCall = false;
  }

  return assign({}, baseInit, init, {
    headers: assign(
      {},
      baseInit.headers,
      init.headers,
      internalCall ? getCsrfHeaders() : {}
    ),
  });
};

/***
 * @type {Object}
 */
const methodInitFactories = dictionary({
  /**
   * @param {string} method
   * @return {Object}
   *   `fetch` init parameter Object
   */
  GET(url, method, body, type, headers) {
    return getInit(url, {
      method,
      headers,
    });
  },
  /**
   * @param {string} method
   * @param {Array|Object} body
   * @param {string} type
   * @param {Object}
   * @return {Object}
   *   `fetch` init parameter Object
   */
  POST(url, method, body, type, headers) {
    const parsedBody = type === 'json' ? JSON.stringify(body) : body;

    return getInit(url, {
      method,
      body: parsedBody,
      headers: {
        ...headers,
        ...(type === 'json' && { ['Content-Type']: 'application/json' }),
        ...(type === 'formdata' && { ['enctype']: 'multipart/form-data' }),
      },
    });
  },

  /**
   * @param {string} method
   * @param {Array|Object} body
   * @return {Object}
   *   `fetch` init parameter Object
   */
  DELETE(url, method, body) {
    return getInit(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
  },
});

/**
 * @type {Array}
 */
export const methods = keys(methodInitFactories);

/**
 * @param {string} method
 * @param {Array|Object} body
 * @param {string} type json | formdata
 * @return {RequestInit}
 */
function getRequestInit(url, method, body, type = 'json', headers = {}) {
  const factory = methodInitFactories[method];

  if (factory) {
    return factory(url, method, body, type, headers);
  }

  throw new Error(`Method '${method}' is not implemented.`);
}

/**
 * @param {Object} response
 * @return {Promise}
 */
function onFulfilled(response) {
  if (response.ok) {
    return response;
  }
  return Promise.reject(response);
}

/**
 * @param {Object} response
 * @return {Promise}
 */
function onRejected(response) {
  return Promise.reject(response);
}

/**
 * Generic HTTP request function for the Zaaksysteem JSON API.
 * Supports GET and POST.
 *
 * @example
 * json('POST', '/api/order', { id: 42 });
 *
 * @param {string} method The request method.
 * @param {string} url The request URL.
 * @param {Array|Object} [body] The request body.
 * @param {string} type json | formdata
 * @return {Promise}
 */
export const fetch = (
  method,
  url,
  body,
  type = 'json',
  cached = false,
  headers
) =>
  (cached ? cachedFetch : window.fetch)(
    url,
    getRequestInit(url, method, body, type, headers)
  )
    .then(onFulfilled)
    .catch(onRejected);
