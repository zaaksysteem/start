// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { navigate } from './url';

/*
 * Authentication and authorization utilities.
 */

export const LOGIN_PATH = '/auth/login';

/**
 * Redirect the current user to the login page.
 *
 * @param {string} referer
 */
export function login(referer) {
  navigate(`${LOGIN_PATH}?referer=${referer}`);
}
