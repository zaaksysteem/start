// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Returns the type of individual elements in an array type
 */
export type ArrayElement<A> = A extends readonly (infer T)[] ? T : never; //test
