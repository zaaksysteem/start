// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { CommunicationContextType } from './types/Context.types';

export const CommunicationContext = React.createContext(
  null as unknown as CommunicationContextType
);
