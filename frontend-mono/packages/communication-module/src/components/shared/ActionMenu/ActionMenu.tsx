// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import DropdownMenu from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/DropdownMenu';
import { DropdownMenuList } from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/DropdownMenu';
import {
  ActionType,
  NestedActionType,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/library/DropdownMenuList';

const checkConditionOrTrue = (action: ActionType): boolean => {
  const { condition } = action;

  if (typeof condition === 'undefined') {
    return true;
  }

  return typeof condition === 'function' ? condition() : condition;
};

const isGroupedActions = (
  actions: NestedActionType
): actions is ActionType[][] =>
  Boolean(actions.length) && Array.isArray(actions[0]);

const filterConditionalActions = (
  actions: NestedActionType
): NestedActionType => {
  return isGroupedActions(actions)
    ? actions.map(group => group.filter(checkConditionOrTrue))
    : actions.filter(checkConditionOrTrue);
};

const containsActions = (actions: NestedActionType): boolean => {
  return isGroupedActions(actions)
    ? actions.some(group => group.length > 0)
    : actions.length > 0;
};

const ActionMenu = ({
  actions,
  scope,
}: {
  actions: NestedActionType;
  scope: string;
}) => {
  const conditionalActions = filterConditionalActions(actions);

  return containsActions(conditionalActions) ? (
    <DropdownMenu
      trigger={
        <Button name="communicationActionMenuTrigger" icon="more_vert" />
      }
    >
      <DropdownMenuList items={conditionalActions} scope={scope} />
    </DropdownMenu>
  ) : null;
};

export default ActionMenu;
