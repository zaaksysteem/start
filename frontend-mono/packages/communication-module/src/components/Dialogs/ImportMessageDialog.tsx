// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { ImportMessageFormFields } from '../../types/Message.types';
import { importMessage } from '../../Communication.requests';
import { GET_THREADS } from '../../Communication.constants';

export type ImportMessageDialogPropsType = {
  caseUuid: string;
  onClose: () => void;
};

const formDefinition: FormDefinition<ImportMessageFormFields> = [
  {
    name: 'file_uuid',
    type: fieldTypes.UPLOAD,
    value: null,
    required: true,
    format: 'file',
    accept: ['.msg', '.eml', '.mail'],
    multiValue: false,
    hint: 'dialogs.importMessage.select.hint',
  },
];

const ImportMessageDialog: React.FunctionComponent<
  ImportMessageDialogPropsType
> = ({ onClose, caseUuid }) => {
  const [t] = useTranslation('communication');
  const queryClient = useQueryClient();

  const { mutateAsync, isLoading } = useMutation<void, V2ServerErrorsType, any>(
    values =>
      importMessage({
        file_uuid: values.file_uuid.value.toString(),
        case_uuid: caseUuid,
      }),
    {
      onError: openServerError,
      onSuccess: () => {
        queryClient.refetchQueries(GET_THREADS);
        onClose();
      },
    }
  );

  return (
    <FormDialog<ImportMessageFormFields>
      formDefinition={formDefinition}
      formDefinitionT={t}
      onSubmit={mutateAsync}
      onClose={onClose}
      saving={isLoading}
      title={t('dialogs.importMessage.title')}
      scope="import-message-dialog"
    />
  );
};

export default ImportMessageDialog;
