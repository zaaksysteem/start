// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  TYPE_CONTACT_MOMENT,
  TYPE_EMAIL,
  TYPE_NOTE,
  TYPE_PIP_MESSAGE,
} from '../../../Communication.constants';
import { CommunicationContextCapabilitiesType } from '../../../types/Context.types';

export const getDefaultCreateTypeFromCapabilities = (
  capabilities: CommunicationContextCapabilitiesType
) => {
  const options: [string, () => boolean][] = [
    [TYPE_PIP_MESSAGE, () => capabilities.canCreatePipMessage],
    [TYPE_EMAIL, () => capabilities.canCreateEmail],
    [TYPE_CONTACT_MOMENT, () => capabilities.canCreateContactMoment],
    [TYPE_NOTE, () => capabilities.canCreateNote],
  ];

  const [defaultType] = options
    .filter(([, condition]) => condition())
    .map(([type]) => type);

  return defaultType || '';
};
export const canCreate = (capabilites: CommunicationContextCapabilitiesType) =>
  [
    capabilites.canCreateContactMoment,
    capabilites.canCreateEmail,
    capabilites.canCreateNote,
    capabilites.canCreatePipMessage,
  ].some(Boolean);
