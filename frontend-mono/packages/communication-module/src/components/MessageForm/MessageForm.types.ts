// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { Rule } from '@zaaksysteem/common/src/components/form/rules';

// PREVIEW

type PreviewRequestBodyType = {
  body: string;
  case_uuid: string;
};

type PreviewResponseBodyType = { body: string; subject: string };

export type FetchPreviewType = (
  data: PreviewRequestBodyType
) => Promise<PreviewResponseBodyType>;

export type GetPreviewType = (
  values: FormValuesType,
  case_uuid: string
) => Promise<FormValuesType>;

// PIP

type PipRequestBody = APICommunication.CreateExternalMessageRequestBody;

export type SavePipType = (data: PipRequestBody) => any;

export type SavePipActionType = (values: any, thread_uuid?: string) => any;

// EMAIL INTEGRATION

export type HtmlEmailTemplateData = {
  label: string;
  template: string;
  imageUuid: string;
};

type HtmlEmailTemplateSettingsType = {
  label: string;
  template: string;
  image: {
    uuid: string;
  }[];
};

export type EmailIntegrationType = {
  instance: {
    interface_config: {
      api_user: string;
      sender_name: string;
      rich_email_templates: HtmlEmailTemplateSettingsType[];
    };
  };
};

type EmailIntegrationResponseBody = {
  result: {
    instance: {
      rows: EmailIntegrationType[];
    };
  };
};

export type GetEmailIntegrationsType = () => Promise<EmailIntegrationType>;

export type FetchEmailIntegrationsType =
  () => Promise<EmailIntegrationResponseBody>;

// EMAIL

type ParticipantType = APICommunication.MessageParticipant;

export type CreateParticipantType = (
  subject: { value: string; label: string; uuid?: string },
  role: 'to' | 'cc' | 'bcc'
) => ParticipantType;

export type GetParticipantsType = (values: any) => ParticipantType[];

type EmailRequestBody = APICommunication.CreateExternalMessageRequestBody;

export type SaveEmailType = (data: EmailRequestBody) => any;

export type SaveEmailActionType = (values: any, thread_uuid?: string) => any;

export type GetHtmlEmailTemplateDataType = (
  emailIntegration?: EmailIntegrationType,
  htmlEmailTemplateName?: string
) => HtmlEmailTemplateData | undefined;

// CONTACT_MOMENT

export type ContactMomentRequestBody =
  APICommunication.CreateContactMomentRequestBody;

export type SaveContactMomentType = (data: ContactMomentRequestBody) => any;

export type SaveContactMomentActionType = (values: any) => any;

// NOTE

export type NoteRequestBody = APICommunication.CreateNoteRequestBody;

export type SaveNoteType = (data: NoteRequestBody) => any;

export type SaveNoteActionType = (
  values: any,
  caseUuid?: string,
  contactUuid?: string
) => any;

// REST

export type ComponentPropsType = Pick<
  MessageFormComponentPropsType,
  | 'onSubmit'
  | 'cancel'
  | 'caseUuid'
  | 'contactUuid'
  | 'threadUuid'
  | 'firstMessage'
  | 'lastMessage'
>;

export interface MessageFormComponentPropsType<Values = any> {
  onSubmit?: () => void;
  cancel: () => void;
  caseUuid?: string;
  contactUuid?: string;
  threadUuid?: string;
  firstMessage?: any;
  lastMessage?: any;
  save: (values: Values) => void;
  customValidator?: (values: Values) => boolean;
  busy: boolean;
  enablePreview?: boolean;
  formDefinition: FormDefinition<Values>;
  htmlEmailTemplateData?: HtmlEmailTemplateData;
  formName: string;
  primaryButtonLabelKey?: string;
  rules?: Rule[];
}
