// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import GenericForm from '../GenericForm/GenericForm';
import { CommunicationContext } from '../../../Communication.context';
import { ComponentPropsType } from '../MessageForm.types';
import { GET_THREADS } from '../../../Communication.constants';
import {
  getPostexFormDefinition,
  getPostexRules,
} from './Postex.formDefinition';
import { sendPostex, usePostexInfoQuery } from './Postex.library';

const Postex: React.ComponentType<ComponentPropsType> = ({
  cancel,
  caseUuid,
}) => {
  const [t] = useTranslation('communication');
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const commContext = React.useContext(CommunicationContext);
  const resolvedCaseUuid = commContext.caseUuid || caseUuid || '';
  const session = useSession();

  const { data } = usePostexInfoQuery();

  const { mutate, isLoading } = useMutation({
    mutationKey: ['savePostex'],
    mutationFn: async (values: any) => {
      return sendPostex(
        values,
        data?.uuid,
        commContext.caseNumber || 0,
        session.logged_in_user.uuid || ''
      ).then(res => {
        if (res.result.instance.ok) {
          openSnackbar(t('snackMessages.postexSent'));
          queryClient.refetchQueries(GET_THREADS);
          navigate('..');
        } else {
          openSnackbar(t('serverErrors.sysin/postex/send_message'));
        }
      });
    },
    onError: () => {
      openSnackbar(t('serverErrors.sysin/postex/send_message'));
    },
  });

  const formDefinition = getPostexFormDefinition(
    resolvedCaseUuid,
    commContext.contactType || '',
    Boolean(commContext.subjectRelations?.find(rel => rel.authorized)),
    Boolean(
      commContext.subjectRelations?.find(
        rel => !['coordinator', 'behandelaar', 'aanvrager'].includes(rel.role)
      )
    )
  );

  const rules = React.useMemo(() => getPostexRules(commContext), []);

  return (
    <GenericForm
      customValidator={(values: any) => {
        const { attachments } = values;

        if (
          attachments &&
          attachments.length &&
          attachments.every((file: any) =>
            file.label.toLowerCase().endsWith('.pdf')
          )
        ) {
          return true;
        }

        return false;
      }}
      rules={rules}
      cancel={cancel}
      busy={isLoading}
      save={mutate}
      formDefinition={formDefinition}
      formName="postex-form"
      primaryButtonLabelKey="forms.send"
    />
  );
};

export default Postex;
