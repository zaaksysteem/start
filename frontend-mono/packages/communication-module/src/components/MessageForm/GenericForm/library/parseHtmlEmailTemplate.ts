// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { HtmlEmailTemplateData } from '../../MessageForm.types';

export const parseHtmlEmailTemplate = (
  values: any,
  htmlEmailTemplateData: HtmlEmailTemplateData
) => {
  const template = encodeURIComponent(htmlEmailTemplateData.template);
  const content = encodeURIComponent(values.content);
  // replace line breaks
  const parsedContent = content.replace(/%0A/g, '<br />');

  const parsedHtml = template
    // {{message}}
    .replace('%7B%7Bmessage%7D%7D', parsedContent)
    // replace linebreaks
    .replace('%0A', '<br />')
    // {{image_url}}
    .replace(
      '%7B%7Bimage_url%7D%7D',
      `/api/v1/file/${htmlEmailTemplateData.imageUuid}/download?inline=1`
    )
    // <script>
    .replace('%3Cscript%3E', '');

  return {
    ...values,
    content: decodeURIComponent(parsedHtml),
  };
};
