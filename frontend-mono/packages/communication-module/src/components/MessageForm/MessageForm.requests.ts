// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchEmailIntegrationsType,
  FetchPreviewType,
  SaveContactMomentType,
  SaveEmailType,
  SaveNoteType,
  SavePipType,
} from './MessageForm.types';

export const fetchPreview: FetchPreviewType = async data => {
  const url = '/api/mail/preview';

  return await request('POST', url, data);
};

export const saveContactMoment: SaveContactMomentType = body => {
  const url = '/api/v2/communication/create_contact_moment';

  return request('POST', url, body);
};

export const saveEmail: SaveEmailType = data => {
  const url = '/api/v2/communication/create_external_message';

  return request('POST', url, data);
};

export const saveNote: SaveNoteType = data => {
  const url = '/api/v2/communication/create_note';

  return request('POST', url, data);
};

export const savePip: SavePipType = data => {
  const url = '/api/v2/communication/create_external_message';

  return request('POST', url, data);
};

export const fetchEmailIntegrations: FetchEmailIntegrationsType = () => {
  const url = '/api/v1/sysin/interface/get_by_module_name/emailconfiguration';

  return request('GET', url);
};
