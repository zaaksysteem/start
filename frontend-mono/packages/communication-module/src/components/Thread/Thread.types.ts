// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';

export interface ThreadType {
  id: string;
  type: 'note' | 'contact_moment' | 'email' | 'pip_message' | 'postex';
  summary: string;
  date: Date;
  tag: string;
  numberOfMessages: number;
  caseNumber?: number;
  lastMessage: AnyThreadDescriptionType;
  unread: boolean;
  hasAttachment: boolean;
  relatedCaseActive: boolean;
}

export interface ThreadDescriptionType {
  createdByName: string;
}

export interface ThreadDescriptionContactMomentType
  extends ThreadDescriptionType {
  type: 'contact_moment';
  direction: string;
  channel: string;
  withName: string;
}

export interface ThreadDescriptionPipMessageType extends ThreadDescriptionType {
  type: 'pip_message';
  subject: string;
}

export interface ThreadDescriptionEmailType extends ThreadDescriptionType {
  type: 'email';
  subject: string;
  failureReason?: string;
}

export interface ThreadDescriptionNoteType extends ThreadDescriptionType {
  type: 'note';
}

export interface ThreadDescriptionPostexType extends ThreadDescriptionType {
  type: 'postex';
  subject: string;
  failureReason?: string;
}

export type AddThreadToCaseValuesType = Pick<
  APICommunication.LinkThreadToCaseRequestBody,
  'case_uuid'
>;

export type AnyThreadDescriptionType =
  | ThreadDescriptionContactMomentType
  | ThreadDescriptionPipMessageType
  | ThreadDescriptionEmailType
  | ThreadDescriptionPostexType
  | ThreadDescriptionNoteType;
