// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { NoteMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

type NoteThreadTitlePropsType = {
  message: NoteMessageType;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const NoteThreadTitle: React.FunctionComponent<NoteThreadTitlePropsType> = ({
  message,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const createdByName = message?.sender?.name;
  const title = t('thread.note.title', { createdByName });

  return <ThreadTitle {...rest} title={title} />;
};

export default NoteThreadTitle;
