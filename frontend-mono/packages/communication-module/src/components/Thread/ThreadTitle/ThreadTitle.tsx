// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import Tag from '../../shared/Tag/Tag';
import TopLink from '../../shared/TopLink/TopLink';
import { CommunicationContextType } from '../../../types/Context.types';
import { useThreadTitleStyle } from './ThreadTitle.style';

export type ThreadTitlePropsType = {
  title: string;
  context: CommunicationContextType;
  caseNumber?: number;
  showLinkToCase?: boolean;
  openThreadToCaseDialog?: () => void;
};

/* eslint complexity: [2, 8] */
const ThreadTitle: React.FunctionComponent<ThreadTitlePropsType> = ({
  title,
  context,
  caseNumber,
  showLinkToCase,
  openThreadToCaseDialog,
}) => {
  const classes = useThreadTitleStyle();
  const [t] = useTranslation('communication');

  return (
    <div className={classes.wrapper}>
      <Typography variant="h4" classes={{ root: classes.title }}>
        {title}
      </Typography>
      {showLinkToCase && caseNumber && context && context.context && (
        <Tag style={classes.tag}>
          <TopLink
            style={classes.link}
            href={`/${
              context.context === 'pip' ? 'pip' : 'intern'
            }/zaak/${caseNumber}`}
          >
            {`${t('thread.tags.case')} ${caseNumber}`}
          </TopLink>
        </Tag>
      )}
      {openThreadToCaseDialog && (
        <Button
          name="addThreadToCase"
          icon={iconNames.add}
          onClick={openThreadToCaseDialog}
        >
          {t('threads.actions.addThreadToCase')}
        </Button>
      )}
    </div>
  );
};

export default ThreadTitle;
