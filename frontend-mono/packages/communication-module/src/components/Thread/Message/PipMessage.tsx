// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { ThreadType } from '../Thread.types';
import { ExternalMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type PipMessagePropsType = {
  message: ExternalMessageType;
  thread: ThreadType;
  expanded?: boolean;
};

export const PipMessage: React.FunctionComponent<PipMessagePropsType> = ({
  message,
  thread,
  expanded = false,
}) => {
  const [t] = useTranslation('communication');
  const { content, isUnread, sender, type, summary, subject, attachments } =
    message;
  const [isExpanded, setExpanded] = useState(isUnread || expanded);
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);

  return (
    <Accordion
      sx={{
        padding: 0,
        boxShadow: 'none',
        '&:before': {
          height: 0,
        },
      }}
      expanded={isExpanded}
    >
      <AccordionSummary
        onClick={() => setExpanded(!isExpanded)}
        sx={{
          height: '100px',
          padding: 0,
          boxShadow: 'none',
          '& .MuiExpansionPanelSummary-content': {
            margin: 0,
            display: 'inline',
          },
        }}
      >
        <MessageHeader
          message={message}
          thread={thread}
          title={title}
          icon={icon}
          info={isExpanded ? subject : summary}
          {...(sender &&
            sender.type === 'employee' && {
              subTitle: t('thread.pipMessage.subTitle'),
            })}
        />
      </AccordionSummary>
      <AccordionDetails sx={{ padding: 0, display: 'inline' }}>
        <MessageContent
          content={content}
          type={type}
          attachments={attachments}
        />
      </AccordionDetails>
    </Accordion>
  );
};

export default PipMessage;
