// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICommunication } from '@zaaksysteem/generated';
import { ExternalMessageParticipantsType } from '../../types/Message.types';

const mapAttachments = (attachment: APICommunication.AttachmentEntity) => ({
  id: attachment.id,
  name: attachment.attributes.name,
  size: attachment.attributes.size,
  download: attachment.links.download.href
    ? { url: attachment.links.download.href }
    : undefined,
  preview: attachment.links.preview
    ? {
        url: attachment.links.preview?.href,
        contentType: attachment.links.preview.meta['content-type'],
      }
    : undefined,
});

export const mapGenericMessageProperties = (
  message: APICommunication.MessageEntity
) => ({
  content: message.attributes.content,
  createdDate: message.meta.message_date || message.meta.created,
  id: message.id,
  summary: message.meta.summary,
  type: message.type,
  threadUuid: message.relationships.thread.data.id,
  hasAttachements: undefined,
  isUnread: undefined,
});

export const mapNoteMessage = (message: APICommunication.MessageNote) => ({
  ...mapGenericMessageProperties(message),
  sender: {
    name: message.relationships.created_by.data.attributes.name,
    type: message.relationships.created_by.data.attributes.type,
  },
});

export const mapContactMomentMessage = (
  message: APICommunication.MessageContactMoment
) => ({
  ...mapGenericMessageProperties(message),
  channel: message.attributes.channel,
  direction: message.attributes.direction,
  recipient: {
    name: message.relationships.recipient.data.attributes.name,
    type: message.relationships.recipient.data.attributes.type,
  },
  sender: {
    name: message.relationships.created_by.data.attributes.name,
    type: message.relationships.created_by.data.attributes.type,
  },
});

const toParticipantsObject = (
  participants: APICommunication.MessageParticipant[]
): ExternalMessageParticipantsType => {
  return participants.reduce<ExternalMessageParticipantsType>(
    (acc, { address, role, display_name, uuid }) => {
      const participant = {
        email: address,
        name: display_name || address,
        uuid,
      };

      return {
        ...acc,
        [role]: [...acc[role], participant],
      };
    },
    {
      from: [],
      to: [],
      cc: [],
      bcc: [],
    }
  );
};

export const mapExternalMessage = (
  message: APICommunication.MessageExternal
) => {
  const {
    attributes: { subject, message_type, participants, is_imported },
    relationships: { attachments, created_by },
    meta: { read_employee, read_pip, failure_reason },
  } = message;
  const mappedAttachments = attachments.data.map(mapAttachments);

  return {
    ...mapGenericMessageProperties(message),
    subject: subject || '',
    attachments: mappedAttachments,
    hasAttachements: Boolean(mappedAttachments.length),
    type: message_type,
    ...(participants
      ? { participants: toParticipantsObject(participants) }
      : {}),
    sender: created_by
      ? {
          name: created_by.data.attributes.name,
          type: created_by.data.attributes.type,
        }
      : null,
    hasSourceFile: is_imported,
    isUnread:
      window.location.href.includes('/pip/') ||
      window.location.href.includes('/my-pip/')
        ? !read_pip
        : !read_employee,
    failureReason: failure_reason,
  };
};
