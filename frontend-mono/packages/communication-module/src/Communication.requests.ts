// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { TYPE_EMAIL } from './Communication.constants';
import {
  FetchSubjectRelationsType,
  SubjectRelationsRequestParams,
  SubjectRelationsResponseBody,
} from './types/SubjectRelations';

export const fetchCaseRelations: FetchSubjectRelationsType = async caseUuid => {
  const url = buildUrl<SubjectRelationsRequestParams>(
    '/api/v2/cm/case/get_subject_relations',
    {
      case_uuid: caseUuid,
      include: ['subject'],
    }
  );

  const response = await request<SubjectRelationsResponseBody>('GET', url);

  return response;
};

export const addThreadToCase = (case_uuid: string, thread_uuid: string) => {
  const url = '/api/v2/communication/link_thread_to_case';
  const data: APICommunication.LinkThreadToCaseRequestBody = {
    case_uuid,
    thread_uuid,
    type: TYPE_EMAIL,
  };

  return request('POST', url, data);
};

export const importMessage = (
  payload: APICommunication.ImportEmailMessageRequestBody
) => {
  const url = '/api/v2/communication/import_email_message';

  return request('POST', url, payload);
};
