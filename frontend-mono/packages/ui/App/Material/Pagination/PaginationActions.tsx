// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Box, IconButton } from '@mui/material';
import { Icon, iconNames } from '../Icon';
import { usePageButtonStyle } from './Pagination.style';
import {
  decrement,
  getPageButtons,
  increment,
  pageExists,
} from './Pagination.library';

const seekOffset = 10;

type TablePaginationActionsPropsType = {
  page: number;
  pageCount: number;
  onChangePage: (destination: number) => void;
};

export const TablePaginationActions = ({
  page,
  pageCount,
  onChangePage,
}: TablePaginationActionsPropsType) => {
  const styles = usePageButtonStyle();

  const PaginationButton = ({
    icon,
    destination,
    style,
    scope,
  }: {
    style: any;
    icon: React.ReactNode;
    destination: number;
    scope: string;
  }) => {
    return (
      <IconButton
        sx={{
          ...style,
          ...styles.paginatorButton,
          '&.Mui-disabled': {
            ...(destination === page ? styles.currentPage : styles.disabled),
          },
        }}
        disabled={!pageExists(destination, pageCount) || destination === page}
        onClick={() => {
          onChangePage(destination);
        }}
        data-scope={scope}
      >
        {icon}
      </IconButton>
    );
  };

  return (
    <Box sx={styles.pageButtonsWrapper}>
      {page === 0 ? null : (
        <Box>
          <PaginationButton
            destination={decrement(page)}
            icon={<Icon>{iconNames.navigate_before}</Icon>}
            style={styles.smallStep}
            scope="pagination-button-before-sm"
          />
          <PaginationButton
            destination={decrement(page, seekOffset)}
            icon={`-${seekOffset}`}
            style={styles.bigStep}
            scope="pagination-button-before-lg"
          />
        </Box>
      )}
      <Box sx={styles.pageJumpButtons}>
        {getPageButtons(page, pageCount).map((button, key) => (
          <PaginationButton
            key={key}
            {...button}
            style={{}}
            scope="pagination-button-jump"
          />
        ))}
      </Box>
      <Box>
        <PaginationButton
          destination={increment(page, seekOffset)}
          icon={`+${seekOffset}`}
          style={styles.bigStep}
          scope="pagination-button-after-lg"
        />
        <PaginationButton
          destination={increment(page)}
          icon={<Icon>{iconNames.navigate_next}</Icon>}
          style={styles.smallStep}
          scope="pagination-button-after-sm"
        />
      </Box>
    </Box>
  );
};

export default TablePaginationActions;
