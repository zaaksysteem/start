// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const FIRST_PAGE = 0;

/**
 * The number of pages around the selected page.
 * ZS-TODO: configuration
 * This would better come from a parameter with a default
 * value higher up the business logic hierarchy.
 */
const ADJACENT_PAGES = 2;

const INTEGER_OFFSET = 1;

export const increment = (value: number, offset = INTEGER_OFFSET) =>
  value + offset;

export const decrement = (value: number, offset = INTEGER_OFFSET) =>
  value - offset;

export const getPageButtons = (page: number, pageCount: number) =>
  symmetricallyIncrementByOneFromZeroUpTo(ADJACENT_PAGES)
    .filter(index => pageExists(page + index, pageCount))
    .map(index => ({
      destination: page + index,
      icon: increment(page + index),
    }));

export const pageExists = (destination: number, pageCount: number) =>
  destination >= FIRST_PAGE && destination < pageCount;

const createSymmetricalLength = (offset: number) => {
  const factor = 2;

  return increment(offset * factor);
};

export const symmetricallyIncrementByOneFromZeroUpTo = (length: number) =>
  [...Array(createSymmetricalLength(length)).keys()].map(item => item - length);
