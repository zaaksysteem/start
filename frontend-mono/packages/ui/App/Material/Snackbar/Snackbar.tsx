// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import MuiSnackbar from '@mui/material/Snackbar';
import { Button } from '@mintlab/ui/App/Material/Button';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '../Icon/Icon';
import { useSnackbarStyle } from './Snackbar.style';

type SnackbarPropsType = {
  sx?: React.ComponentProps<typeof MuiSnackbar>['sx'];
  anchorOrigin?: React.ComponentProps<typeof MuiSnackbar>['anchorOrigin'];
  action?: {
    action?: () => void;
    url?: string;
    text: string;
  };
  autoHideDuration?: number | null;
  handleClose: () => void;
  closeLabel?: string;
  scope: string;
  open: boolean;
  message?: React.ReactNode;
  color?: string;
};

const Snackbar = ({
  scope,
  handleClose,
  autoHideDuration,
  anchorOrigin,
  message,
  closeLabel,
  action,
  open,
  sx,
}: SnackbarPropsType) => {
  const style = useSnackbarStyle();
  const {
    palette: { common, elephant },
  } = useTheme<Theme>();

  return (
    <MuiSnackbar
      sx={{ ...style.root, ...sx }}
      onClose={(ev, reason) => reason !== 'clickaway' && handleClose()}
      anchorOrigin={anchorOrigin}
      open={open}
      message={message || ''}
      autoHideDuration={
        autoHideDuration === null ? null : autoHideDuration || 5000
      }
      data-scope={scope}
      action={
        <>
          {action && (
            <Button
              name="snack-action"
              action={
                action.url
                  ? () => {
                      (window.top || window).location.href =
                        action.url as string;
                    }
                  : action.action
              }
              iconSize="small"
              variant="outlined"
              sx={{
                color: common.white,
                border: `1px solid ${common.white}`,
                '&:hover': {
                  border: `1px solid ${common.white}`,
                  backgroundColor: elephant.darkest,
                },
              }}
            >
              {action.text}
            </Button>
          )}
          <IconButton
            key="close"
            aria-label={closeLabel}
            color="inherit"
            className="close"
            onClick={handleClose}
            sx={style.iconButton}
          >
            <Icon size="small">{iconNames.close}</Icon>
          </IconButton>
        </>
      }
    />
  );
};

export default Snackbar;
