// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import MuiTooltip, { TooltipProps } from '@mui/material/Tooltip';
import { useTooltipStyles } from './Tooltip.style';

type TooltipPropsType = TooltipProps & {
  type?: 'default' | 'info' | 'error';
  title: string | React.ReactElement;
  children: any;
  disabled?: boolean;
  noWrap?: boolean;
  classes?: {
    popper?: string;
    all?: string;
    default?: string;
    wrapper?: string;
  };
  sx?: any;
};

export const Tooltip: React.ComponentType<TooltipPropsType> = ({
  type = 'default',
  title,
  children,
  disabled = false,
  noWrap = false,
  classes = {},
  placement = 'top',
  ...rest
}) => {
  const tooltipClasses = { ...useTooltipStyles(), ...classes };

  const [isOverflowed, setIsOverflow] = useState(false);
  const textElementRef = useRef();
  useEffect(() => {
    setIsOverflow(
      // @ts-ignore
      textElementRef.current.scrollWidth > textElementRef.current.clientWidth
    );
  }, [window.innerWidth]);

  const tooltipDisabled = disabled || (noWrap && !isOverflowed);

  return (
    <MuiTooltip
      title={tooltipDisabled ? '' : title}
      placement={placement}
      classes={{
        tooltip: classNames(tooltipClasses.all, tooltipClasses[type]),
        popper: tooltipClasses.popper,
      }}
      {...rest}
      disableFocusListener={tooltipDisabled}
      disableHoverListener={tooltipDisabled}
      disableTouchListener={tooltipDisabled}
    >
      <div
        ref={textElementRef}
        className={classNames(tooltipClasses.wrapper, {
          [tooltipClasses.noWrap]: noWrap,
        })}
        {...(rest.sx && { style: rest.sx })}
      >
        {children}
      </div>
    </MuiTooltip>
  );
};

export default Tooltip;
