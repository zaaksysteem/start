// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTooltipStyles = makeStyles(
  ({ palette: { primary, grey, error }, mintlab: { radius } }: Theme) => ({
    wrapper: {
      width: '100%',
    },
    noWrap: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
    all: {
      padding: '5px 5px 2px 5px',
      maxWidth: '100%',
      borderRadius: radius.tooltip,
      '&&': {
        fontSize: 14,
      },
    },
    popper: {
      opacity: 1,
    },
    default: {
      backgroundColor: grey[600],
    },
    error: {
      backgroundColor: error.dark,
    },
    info: {
      backgroundColor: primary.lightest,
      color: primary.main,
    },
  })
);
