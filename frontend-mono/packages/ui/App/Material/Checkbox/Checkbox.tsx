// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiCheckbox from '@mui/material/Checkbox';
import MuiFormControlLabel from '@mui/material/FormControlLabel';
import { SxProps } from '@mui/material';

type CheckboxPropsType = {
  sx?: SxProps;
  checked?: boolean;
  disabled?: boolean;
  label?: string;
  name: string;
  color?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  onClick?: React.MouseEventHandler<HTMLLabelElement>;
  size?: any;
  scope?: string;
};

export const Checkbox = ({
  checked = false,
  disabled = false,
  label,
  name,
  onChange,
  onClick,
  size,
  scope,
  sx,
}: CheckboxPropsType) => (
  <MuiFormControlLabel
    onClick={onClick}
    label={label}
    sx={sx}
    control={
      <MuiCheckbox
        checked={checked}
        disabled={disabled}
        name={name}
        onChange={onChange}
        size={size}
        inputProps={{ 'data-scope': scope } as any}
      />
    }
  />
);

export default Checkbox;
