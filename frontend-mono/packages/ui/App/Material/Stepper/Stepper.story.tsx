// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories } from '../../story';
import { Stepper, StepType } from './Stepper';

stories(module, __dirname, {
  Default() {
    const steps: StepType[] = [
      { name: 'Step 1', completed: true },
      { name: 'Step 2', error: true },
      { name: 'Step 3', active: true },
      { name: 'Step 3', disabled: true },
    ];
    return <Stepper steps={steps} />;
  },
});
