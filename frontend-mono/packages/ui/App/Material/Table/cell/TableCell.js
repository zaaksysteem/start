// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiTableCell from '@mui/material/TableCell';
import { withStyles } from '@mui/styles';
import classNames from 'classnames';
import { tableStyleSheet } from '../Table.style';

/**
 * *Material Design* **TableCell**.
 * - facade for *Material-UI* `TableCell`
 * - all props are spread to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Table
 * @see /npm-mintlab-ui/documentation/consumer/manual/Table.html
 * @see https://material-ui.com/api/snackbar/
 *
 * @reactProps {React.children} children
 * @reactProps {boolean} heading
 * @reactProps {Object} classes
 * @reactProps {string} className
 *
 * @return {ReactElement}
 */
const TableCell = ({ children, heading, classes, className, ...rest }) => (
  <MuiTableCell
    className={classNames(classes.tableCell, {
      [classes.tableHeadCell]: heading,
      [className]: className,
    })}
    {...rest}
  >
    {children}
  </MuiTableCell>
);

export default withStyles(tableStyleSheet)(TableCell);
