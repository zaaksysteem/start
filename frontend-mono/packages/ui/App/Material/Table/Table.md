# 🔌 `Table` component

> *Material Design* **Table**.

The `rows` prop is asynchronously populated with the `getNewPage` prop method.

Aborting pending requests is the reponsibility of the consuming component's 
implementation of `getNewPage`. 

## See also

- [`Table` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Table)
- [`Table` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-table)
