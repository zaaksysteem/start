// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';

export const PopoverDatePicker = React.lazy(
  /* webpackChunkName: "popover-picker" */ () => import('./PopoverDatePicker')
);
export const DialogDatePicker = React.lazy(
  /* webpackChunkName: "dialog-picker" */ () => import('./DialogDatePicker')
);
export const StaticDatePicker = React.lazy(
  /* webpackChunkName: "static-picker" */ () => import('./StaticDatePicker')
);
export const DateTimePicker = React.lazy(
  /* webpackChunkName: "datetime-picker" */ () => import('./DateTimePicker')
);
export const TimePicker = React.lazy(
  () => import(/* webpackChunkName: "time-picker" */ './TimePicker')
);
