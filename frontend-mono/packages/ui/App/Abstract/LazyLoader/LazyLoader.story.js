// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories } from '../../story';
import LazyLoader from '.';

stories(module, __dirname, {
  Default() {
    return (
      <LazyLoader
        promise={() => import('./story/Foo')}
        title="Get Out Of Your Lazy Bed!"
      />
    );
  },
});
