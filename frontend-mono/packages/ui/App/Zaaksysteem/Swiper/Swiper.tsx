// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { SCROLL_TARGET, scrollToTop } from './Swiper.library';
import { useSwiperStyles } from './Swiper.styles';

type SwiperPropsType = {
  view: string;
  views: string[];
  minWidth: number;
  onLoadLeft: () => void;
  onLoadRight: () => void;
  ContentLeft: React.ReactElement;
  ContentRight: React.ReactElement;
};
type SwipePointsType = { [key: string]: number }[];

const SWIPE_DISTANCE = 50;
const SWIPE_TIME = 100;

const getWidth = (minWidth: number) => Math.min(window.innerWidth, minWidth);

export const Swiper: React.ComponentType<SwiperPropsType> = ({
  view,
  views,
  minWidth,
  onLoadLeft,
  onLoadRight,
  ContentLeft,
  ContentRight,
}) => {
  const navigate = useNavigate();
  const classes = useSwiperStyles();

  const [width, setWidth] = useState(getWidth(minWidth));
  const left = view === views[0];
  const [middle, setMiddle] = useState(left ? width : 0);

  const firstRender = useRef(true);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }

    setMiddle(left ? width : 0);

    if (left) {
      onLoadLeft();
    } else {
      onLoadRight();
    }

    scrollToTop();
  }, [view]);

  useEffect(() => {
    function handleWindowResize() {
      const newWidth = getWidth(minWidth);
      setWidth(newWidth);
      setMiddle(left ? newWidth : 0);
    }

    window.addEventListener('resize', handleWindowResize);

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, [left]);

  const [startClick, setStartClick] = useState<number>(0);
  const [swipePoints, setSwipePoints] = useState<SwipePointsType>([]);

  const currentLocation = swipePoints[0]?.location || startClick;
  const xMovement = currentLocation - startClick;

  const tableWrapperStyle: any = {
    width,
    position: 'absolute',
    height: '100%',
    overflowY: 'auto',
  };

  return (
    <div
      className={classes.wrapper}
      onTouchStart={event => {
        setStartClick(event.touches[0].pageX);
      }}
      onTouchMove={event => {
        const location = event.touches[0].pageX;
        const time = new Date().valueOf();

        setSwipePoints([{ time, location }, ...swipePoints]);

        setMiddle(left ? width + xMovement : 0 + xMovement);
      }}
      /* eslint complexity: [2, 10] */
      onTouchEnd={() => {
        const fullSwipe = left
          ? xMovement <= width / -2
          : xMovement >= width / 2;

        const currentTime = new Date().valueOf();

        const lastLocation = swipePoints
          .reverse()
          .find(({ time }) => currentTime - SWIPE_TIME < time)?.location;

        setSwipePoints([]);

        const distance = currentLocation - (lastLocation || currentLocation);
        const fastSwipe = left
          ? distance <= -SWIPE_DISTANCE
          : distance >= SWIPE_DISTANCE;

        if (fullSwipe || fastSwipe) {
          setMiddle(left ? 0 : width);
          const newside = left ? views[1] : views[0];

          navigate(`../${newside}`);
        } else {
          setMiddle(left ? width : 0);
        }
      }}
    >
      <div
        style={{
          ...tableWrapperStyle,
          left: Math.min(middle - width, 0),
        }}
      >
        <div id={SCROLL_TARGET} />
        {ContentLeft}
      </div>
      <div
        style={{
          ...tableWrapperStyle,
          right: Math.min(-middle, 0),
        }}
      >
        <div id={SCROLL_TARGET} />
        {ContentRight}
      </div>
    </div>
  );
};
