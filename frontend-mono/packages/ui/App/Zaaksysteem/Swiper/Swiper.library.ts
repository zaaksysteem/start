// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const SCROLL_TARGET = 'scroll-target';

export const scrollToTop = () => {
  document.getElementById(SCROLL_TARGET)?.scrollIntoView();
};
