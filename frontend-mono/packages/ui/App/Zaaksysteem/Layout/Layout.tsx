// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, createContext, useRef } from 'react';
import { Outlet } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useTranslation } from 'react-i18next';
import { Loader } from '../Loader';
import NavigationBar from './NavigationBar/NavigationBar';
import { useLayoutStyles } from './Layout.styles';
import Topbar, { TopbarPropsType } from './Topbar/Topbar';
import { KccContextType } from './Kcc/Kcc.types';
import locale from './Layout.locale';
import Kcc from './Kcc';
import { useKccContext } from './Kcc/Kcc.library';

type TopbarPortalContextType = TopbarPropsType['topbarPortalRef'];

export const TopbarPortalContext = createContext<TopbarPortalContextType>(null);

export const KccContext = createContext<KccContextType>({
  hasKccIntegration: false,
  kccUserStatus: 0,
  setKccUserStatus: () => {},
});

const Layout = () => {
  const classes = useLayoutStyles();
  const [t] = useTranslation('layout');

  const ref = useRef(null);
  const [topbarPortalRef] = useState<TopbarPortalContextType>(ref);
  const topbarContext = topbarPortalRef;

  const kccContext = useKccContext();

  return (
    <TopbarPortalContext.Provider value={topbarContext}>
      <KccContext.Provider value={kccContext}>
        <a href="#main-content" className={classes.skip}>
          {t('skipToContent')}
        </a>
        <div className={classes.wrapper}>
          <NavigationBar />
          <div className={classes.view}>
            <Topbar topbarPortalRef={topbarPortalRef} />
            <main
              id="main-content"
              className={classes.content}
              style={
                // Remove when moving to dashboard v2
                window.location.href.includes('/main/intern')
                  ? { overflow: 'hidden' }
                  : {}
              }
            >
              <React.Suspense fallback={<Loader delay={200} />}>
                <Outlet />
              </React.Suspense>
            </main>
            <Kcc />
          </div>
        </div>
      </KccContext.Provider>
    </TopbarPortalContext.Provider>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="layout">
    <Layout />
  </I18nResourceBundle>
);
