// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fetchIntegrations, search } from './next2know.requests';
import {
  GetIntegrationType,
  FormatIntegrationType,
  PerformSearchType,
  FormatResultsType,
  FormatSingleResultType,
  SingleResultType,
  GroupedResultType,
  FormatGroupedResultType,
  CreateHeadersType,
} from './next2know.types';

const formatIntegration: FormatIntegrationType = integration => {
  const {
    instance: {
      interface_config: { endpoint, result_grouping: grouping, token },
    },
  } = integration;

  return {
    endpoint,
    grouping,
    token,
  };
};

export const getIntegration: GetIntegrationType = async () => {
  const response = await fetchIntegrations();

  const {
    result: {
      instance: { rows: integrations },
    },
  } = response;

  // only a single integration is supported
  const integration = integrations[0];

  // most customers don't use this integration
  if (!integration) return null;

  // login details for next2know are configured in the integration
  // backend uses these login details to request a token from next2know
  // if no token is returned, it means something went wrong
  // without a token the integration can be considered to be inactive
  if (!integration.instance.interface_config.token) return null;

  return formatIntegration(integration);
};

const getDescription = (caseNumber: string, description: string) =>
  [caseNumber, description]
    .filter(text => Boolean(text) && text !== '-')
    .join(' - ');

const type: 'external' = 'external';

const formatSingleResult: FormatSingleResultType = ({
  _id: uuid,
  _index: index,
  _source: {
    Naam: title,
    Omschrijving,
    Externe_identificatiekenmerken_nummer_binnen_systeem: caseNumber,
  },
}) => ({
  uuid,
  type,
  title,
  description: getDescription(caseNumber, Omschrijving),
  url: `/main/extern/next2know/${index}/${uuid}`,
});

const formatGroupedResult: FormatGroupedResultType = (
  t,
  { key, doc_count },
  index,
  forceKeyword,
  setExternalSearchOverwrite
) => ({
  uuid: `${index}`,
  type,
  title: key,
  description: t('external.next2know.docCount', { doc_count }),
  action: () => {
    setExternalSearchOverwrite('next2know', { grouping: 'none' });
    forceKeyword(key);
  },
});

const formatResults: FormatResultsType = (
  t,
  hits,
  grouping,
  forceKeyword,
  setExternalSearchOverwrite
) => {
  const results =
    grouping === 'dossier_name'
      ? (hits as GroupedResultType[]).map((result, index) =>
          formatGroupedResult(
            t,
            result,
            index,
            forceKeyword,
            setExternalSearchOverwrite
          )
        )
      : (hits as SingleResultType[]).map(formatSingleResult);

  return results;
};

export const createHeaders: CreateHeadersType = ({
  token_type,
  access_token,
}) => ({
  Authorization: `${token_type} ${access_token}`,
});

export const performSearch: PerformSearchType = async (
  t,
  keyword,
  integration,
  forceKeyword,
  setExternalSearchOverwrite
) => {
  const { endpoint, grouping, token } = integration;

  const params = {
    group_by: grouping === 'dossier_name' ? 'Naam' : undefined,
    // eslint-disable-next-line id-length
    q: `Naam:"${keyword}" OR Omschrijving:"${keyword}"`,
    size: 100,
  };

  const headers = createHeaders(token);

  const response = await search(endpoint, params, headers);

  const results = formatResults(
    t,
    response.data.hits,
    grouping,
    forceKeyword,
    setExternalSearchOverwrite
  );

  return results;
};
