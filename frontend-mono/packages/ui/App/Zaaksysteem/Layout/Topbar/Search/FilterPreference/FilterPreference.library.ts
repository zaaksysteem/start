// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetFilterPreferenceType,
  SetFilterPreferenceType,
  FilterType,
} from './../Search.types';

const FILTER_PREFERENCE = 'filter-preference';

export const getFilterPreference: GetFilterPreferenceType = () =>
  (localStorage.getItem(FILTER_PREFERENCE) as FilterType) || null;

export const setFilterPreference: SetFilterPreferenceType = filter => {
  if (filter) {
    localStorage.setItem(FILTER_PREFERENCE, filter);
  } else {
    localStorage.removeItem(FILTER_PREFERENCE);
  }
};
