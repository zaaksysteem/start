// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useProfileIconStyles } from './ProfileIcon.styles';

export type InfoPropsType = {
  open: number;
  setOpen: (open: number) => void;
};

export const ProfileIcon: React.ComponentType<InfoPropsType> = ({
  open,
  setOpen,
}) => {
  const classes = useProfileIconStyles();

  return (
    <div className={classes.wrapper}>
      <IconButton name="user" onClick={() => setOpen(open ? 0 : 1)}>
        <Icon size="small">{iconNames.person}</Icon>
      </IconButton>
    </div>
  );
};

export default ProfileIcon;
