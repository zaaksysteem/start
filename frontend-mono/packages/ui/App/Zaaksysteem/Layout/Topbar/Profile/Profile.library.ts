// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormatNotificationType,
  GetNotificationsType,
  PerformArchiveType,
  PerformUndoArchiveType,
  PerformArchiveAllType,
  PerformMarkReadType,
  EventTypeDictType,
} from './Profile.types';
import {
  fetchNotifications,
  archive,
  undoArchive,
  archiveAll,
  markRead,
} from './Profile.requests';

const eventTypeDict: EventTypeDictType = {
  'api/v1/update/attributes': 'attribute',
  'api/v1/update/documents': 'document',
  'case/contact_moment/created': 'contactMoment',
  'case/document/accept': 'document',
  'case/document/assign': 'document',
  'case/document/create': 'document',
  'case/document/reject': 'document',
  'case/email/created': 'email',
  'case/note/create': 'note',
  'case/note/created': 'note',
  'case/pip/feedback': 'pip',
  'case/pip/updateField': 'pip',
  'case/subject/add': 'contact',
  'case/task/new_assignee': 'task',
  'case/task/set_completion': 'task',
  'case/update/field': 'attribute',
  'document/assign': 'document',
  'email/send': 'email',
  'user/apply': 'user',
  'xential/sjabloon': 'document',
};

const formatNotification: FormatNotificationType = ({
  id,
  is_read,
  message,
  logging_id: { case_id: caseId, event_type, timestamp: date },
}) => ({
  id,
  isRead: Boolean(is_read),
  message,
  caseId,
  type: Object.prototype.hasOwnProperty.call(eventTypeDict, event_type)
    ? eventTypeDict[event_type]
    : 'other',
  date,
});

export const getNotifications: GetNotificationsType = async () => {
  const response = await fetchNotifications();

  const notifications = response.result.map(formatNotification);

  return notifications;
};

export const performArchive: PerformArchiveType = async id => {
  await archive({ messages: id });
};

export const performUndoArchive: PerformUndoArchiveType = async id => {
  await undoArchive({ messages: id });
};

export const performArchiveAll: PerformArchiveAllType = async () => {
  await archiveAll();
};

export const performMarkRead: PerformMarkReadType = async id => {
  await markRead({ messages: id });
};
