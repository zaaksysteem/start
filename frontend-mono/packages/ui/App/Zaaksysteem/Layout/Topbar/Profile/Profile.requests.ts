// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import {
  FetchNotificationsType,
  FetchNotificationResponseBodyType,
  ArchiveType,
  UndoArchiveType,
  ArchiveAllType,
  MarkReadType,
} from './Profile.types';

export const useGetContactQuery = (open: number) => {
  const session = useSession();
  const uuid = session.logged_in_user.uuid;
  const params = { uuid, type: 'employee' };
  const url = buildUrl('/api/v2/cm/contact/get_contact', params);

  return useQuery(
    ['profileContact'],
    async () => {
      const response = await request('GET', url).catch(openServerError);
      const { attributes, meta, relationships } = response.data;

      const name = meta.summary;
      const email = attributes.contact_information.email;
      const phoneNumber = attributes.contact_information.phone_number;
      const department = relationships.department.meta.summary;
      const roles = (relationships.roles as any[])
        .map(role => role.meta.summary)
        .sort((first, second) => first.localeCompare(second));

      return {
        name,
        email,
        phoneNumber,
        department,
        roles,
      };
    },
    { enabled: Boolean(open) }
  );
};

export const fetchNotifications: FetchNotificationsType = async () => {
  const url = buildUrl('/api/message/get_for_user', { rows: 1000 });

  const response = await request<FetchNotificationResponseBodyType>('GET', url);

  return response;
};

export const archive: ArchiveType = async body => {
  const url = '/api/message/archive';

  await request('POST', url, body);
};

export const undoArchive: UndoArchiveType = async body => {
  const url = '/api/message/unarchive';

  await request('POST', url, body);
};

export const archiveAll: ArchiveAllType = async () => {
  const url = '/api/message/archive/all';

  await request('POST', url);
};

export const markRead: MarkReadType = async body => {
  const url = '/api/message/mark_read';

  await request('POST', url, body);
};
