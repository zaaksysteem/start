// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    person: {
      label: {
        'np-burgerservicenummer': 'BSN',
        'np-persoonsnummer': 'ID Nummer',
        'np-voornamen': 'Voornamen',
        'np-voorvoegsel': 'Voorvoegsel',
        'np-geslachtsnaam': 'Achternaam',
        'np-adellijke_titel': 'Adellijke titel',
        'np-geslachtsaanduiding': 'Geslacht',
        'np-landcode': 'Land',
        'npc-telefoonnummer': 'Telefoonnummer',
        'npc-mobiel': 'Telefoonnummer (mobiel)',
        'npc-email': 'Emailadres',
        briefadres: 'Briefadres',
        'np-straatnaam': 'Straat',
        'np-huisnummer': 'Huisnummer',
        'np-huisnummertoevoeging': 'Huisnummertoevoeging',
        'np-postcode': 'Postcode',
        'np-woonplaats': 'Woonplaats',
        'np-in_gemeente': 'Binnengemeentelijk',
        'np-adres_buitenland1': 'Adresregel 1',
        'np-adres_buitenland2': 'Adresregel 2',
        'np-adres_buitenland3': 'Adresregel 3',
        'np-correspondentie_straatnaam': 'Briefadres straat',
        'np-correspondentie_huisnummer': 'Briefadres huisnummer',
        'np-correspondentie_huisnummertoevoeging':
          'Briefadres huisnummertoevoeging',
        'np-correspondentie_postcode': 'Briefadres postcode',
        'np-correspondentie_woonplaats': 'Briefadres woonplaats',
      },
      sex: {
        male: 'Man',
        female: 'Vrouw',
        other: 'Anders',
      },
    },
    organization: {
      label: {
        vestiging_landcode: 'Land',
        rechtsvorm: 'Rechtsvorm',
        dossiernummer: 'KVK-nummer',
        vestigingsnummer: 'Vestigingsnummer',
        handelsnaam: 'Handelsnaam',
        'npc-telefoonnummer': 'Telefoonnummer',
        'npc-mobiel': 'Telefoonnummer (mobiel)',
        'npc-email': 'Emailadres',
        'org-briefadres': 'Correspondentie adres',
        vestiging_straatnaam: 'Straat',
        vestiging_huisnummer: 'Huisnummer',
        vestiging_huisletter: 'Huisletter',
        vestiging_huisnummertoevoeging: 'Toevoeging',
        vestiging_postcode: 'Postcode',
        vestiging_woonplaats: 'Woonplaats',
        vestiging_adres_buitenland1: 'Adresregel 1',
        vestiging_adres_buitenland2: 'Adresregel 2',
        vestiging_adres_buitenland3: 'Adresregel 3',
        correspondentie_landcode: 'Correspondentie land',
        correspondentie_straatnaam: 'Correspondentie straat',
        correspondentie_huisnummer: 'Correspondentie huisnummer',
        correspondentie_huisletter: 'Correspondentie huisletter',
        correspondentie_huisnummertoevoeging:
          'Correspondentie huisnummertoevoeging',
        correspondentie_postcode: 'Correspondentie postcode',
        correspondentie_woonplaats: 'Correspondentie woonplaats',
        correspondentie_adres_buitenland1: 'Correspondentie Adresregel 1',
        correspondentie_adres_buitenland2: 'Correspondentie Adresregel 2',
        correspondentie_adres_buitenland3: 'Correspondentie Adresregel 3',
      },
    },
    snack: {
      created: 'Contact aangemaakt',
    },
    error: {
      title: 'Er is iets fout gegaan',
    },
  },
};

export default locale;
