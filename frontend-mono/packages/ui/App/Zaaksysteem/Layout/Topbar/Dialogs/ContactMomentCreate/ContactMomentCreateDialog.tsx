// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getContactSelectOption } from './ContactMomentCreateDialog.library';
import { ContactMomentCreateDialogPropsType } from './ContactMomentCreateDialog.types';
import { ContactMomentCreateDialogForm } from './ContactMomentCreateDialogForm';
import locale from './ContactMomentCreateDialog.locale';

/**
 * Acts as a wrapper around the ContactMoment Create form, and loads in
 * the given contact's details from the backend
 * when applicable, and passes these to the form component.
 */
const ContactMomentCreateDialog: React.ComponentType<
  ContactMomentCreateDialogPropsType
> = props => {
  const { contact, open, onClose, container, contactChannel } = props;
  const [contactOption, setContactOption] = useState<ContactType | null>(null);
  const [loaded, setLoaded] = useState(false);

  React.useEffect(() => {
    (async () => {
      if (!open) return;

      setLoaded(false);

      if (contact) setContactOption(await getContactSelectOption(contact));

      setLoaded(true);
    })();
  }, [open]);

  return loaded ? (
    <I18nResourceBundle resource={locale} namespace="contactMomentCreate">
      <ContactMomentCreateDialogForm
        open={open}
        onClose={onClose}
        container={container}
        contactOption={contactOption}
        contactChannel={contactChannel}
      />
    </I18nResourceBundle>
  ) : null;
};

export default ContactMomentCreateDialog;
