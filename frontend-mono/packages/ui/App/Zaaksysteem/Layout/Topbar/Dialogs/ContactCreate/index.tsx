// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useErrorDialog } from '@zaaksysteem/common/src/hooks/useErrorDialog';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import { openServerError } from '@zaaksysteem/common/src/signals';
import locale from './ContactCreateDialog.locale';
import {
  getCountryCodes,
  getLegalEntityTypes,
} from './ContactCreateDialog.library';
import {
  ContactCreateDialogModulePropsType,
  CountryCodeType,
  LegalEntityTypeType,
  SnackType,
} from './ContactCreateDialog.types';
import ContactCreateDialog from './ContactCreateDialog';

const ContactCreateDialogModule: React.ComponentType<
  ContactCreateDialogModulePropsType
> = props => {
  const [t] = useTranslation('contactCreate');
  const [errorDialog, openErrorDialog] = useErrorDialog();
  const [snack, setSnack] = useState<SnackType>();
  const [countryCodes, setCountryCodes] = useState<CountryCodeType[]>();
  const [legalEntityTypes, setLegalEntityTypes] =
    useState<LegalEntityTypeType[]>();

  const { open, type } = props;

  useEffect(() => {
    if (open) {
      getCountryCodes().then(setCountryCodes).catch(openServerError);
      if (type === 'organization') {
        getLegalEntityTypes().then(setLegalEntityTypes).catch(openServerError);
      }
    }
  }, [open]);

  return (
    <>
      {countryCodes && !(type === 'organization' && !legalEntityTypes) && (
        <ContactCreateDialog
          {...props}
          countryCodes={countryCodes}
          legalEntityTypes={legalEntityTypes}
          setSnack={setSnack}
          openErrorDialog={openErrorDialog}
        />
      )}
      {errorDialog}
      {snack ? (
        <Snackbar
          scope="contact-created-snack"
          handleClose={() => setSnack(null)}
          message={t('snack.created')}
          open={Boolean(snack)}
          action={{
            url: snack.link,
            text: t('common:verbs.view'),
          }}
        />
      ) : null}
    </>
  );
};

export default (props: ContactCreateDialogModulePropsType) => (
  <I18nResourceBundle resource={locale} namespace="contactCreate">
    <ContactCreateDialogModule {...props} />
  </I18nResourceBundle>
);
