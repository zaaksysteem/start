// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { KccContext } from '../Layout';
import { getPhoneCalls, getActivePhoneCalls } from './Kcc.library';
import Kcc from './Kcc';

// 5 seconds (yes, that's often)
const refetchInterval = 5 * 1000;

const PHONE_CALLS = 'phoneCalls';
export const ACTIVE_PHONE_CALLS = 'activePhoneCalls';

const KccModule: React.ComponentType = () => {
  const { kccUserStatus } = useContext(KccContext);
  const queryClient = useQueryClient();

  const { data: phoneCalls } = useQuery(
    [PHONE_CALLS, [kccUserStatus]],
    getPhoneCalls,
    {
      onError: openServerError,
      staleTime: 0,
      refetchInterval,
      initialData: [],
      enabled: Boolean(kccUserStatus),
    }
  );

  const { data: activePhoneCalls } = useQuery(
    [ACTIVE_PHONE_CALLS, kccUserStatus, phoneCalls],
    getActivePhoneCalls,
    {
      onError: openServerError,
      staleTime: 0,
      initialData: [],
      // users can manually activate contacts, even without an active kcc integration
      // so this should always be active
      enabled: true,
    }
  );

  const clearPhoneCall = (id: number) => {
    queryClient.setQueriesData(
      [PHONE_CALLS],
      phoneCalls.filter(phoneCall => phoneCall.id !== id)
    );
  };

  const clearActivePhoneCalls = () => {
    queryClient.setQueriesData([ACTIVE_PHONE_CALLS], []);
  };

  return (
    <div>
      <Kcc
        phoneCalls={[...activePhoneCalls, ...phoneCalls]}
        clearPhoneCall={clearPhoneCall}
        clearActivePhoneCalls={clearActivePhoneCalls}
      />
    </div>
  );
};

export default KccModule;
