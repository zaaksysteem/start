# 🔌 `ZsIcon` component

> Zaaksysteem Icon facade for _Zaaksysteem Icon_.

## Usage

Pass in the size as props, and the name as children.

## Example

    <ZsIcon size="large">
      contactChannel.phone
    <ZsIcon/>

## See also

- [`ZsIcon` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/ZsIcon)
- [`ZsIcon` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-zsicon)
