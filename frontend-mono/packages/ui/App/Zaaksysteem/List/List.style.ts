// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const mainPadding = 15;
const mainRadius = 4;

export const useListStyle = makeStyles(({ palette, typography }: Theme) => ({
  listContainer: {
    backgroundColor: 'transparent',
    borderRadius: 4,
    display: 'flex',
    flexDirection: 'column',
    gap: '15px',
  },
  itemContainer: {
    boxShadow: '0 1px 3px rgba(0,0,0,0.3)',
    padding: mainPadding,
    borderRadius: mainRadius,
    backgroundColor: 'white',
  },
  itemContainerHover: {
    '&:hover': {
      boxShadow: '0 1px 6px rgba(0,0,0,0.4)',
    },
  },
  itemContainerDragging: {
    backgroundColor: palette.sahara.lighter,
    transition: 'all 0.03s',
  },
  itemContainerSimple: {
    display: 'flex',
    justifyContent: 'space-between',
    '&>button': {
      padding: 0,
    },
    ...typography.body2,
  },
}));
