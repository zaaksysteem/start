// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import classNames from 'classnames';
import { moveItem } from './moveItem';
import { useListStyle } from './List.style';

export type SortableListPropsType<T> = {
  renderItem: (item: T, index: number) => React.ReactElement;
  onReorder: (value: T[]) => void;
  value: T[];
};

function SortableList<T extends { id: string }>({
  renderItem,
  onReorder,
  value,
}: SortableListPropsType<T>) {
  const classes = useListStyle();

  return (
    //@ts-ignore
    <DragDropContext
      onDragEnd={({ destination, source }) => {
        if (destination) {
          onReorder(moveItem(value, source.index, destination.index));
        }
      }}
    >
      {/* @ts-ignore */}
      <Droppable droppableId="attribute-options-droppable">
        {({ innerRef, droppableProps, placeholder }) => (
          <div className={classes.listContainer} ref={innerRef}>
            {value.map((item, index: number) => (
              //@ts-ignore
              <Draggable
                key={item.id}
                draggableId={item.id}
                index={index}
                {...droppableProps}
              >
                {(draggableProvided, draggableSnapshot) => {
                  const {
                    dragHandleProps,
                    draggableProps,
                    innerRef: draggableRef,
                  } = draggableProvided;
                  const { isDragging, isDropAnimating } = draggableSnapshot;
                  return (
                    <div
                      className={classNames({
                        [classes.itemContainer]: true,
                        [classes.itemContainerHover]: true,
                        [classes.itemContainerDragging]: isDragging,
                      })}
                      ref={draggableRef}
                      {...dragHandleProps}
                      {...draggableProps}
                      style={
                        isDragging && !isDropAnimating
                          ? {
                              ...draggableProps.style,
                              transform:
                                'rotate(-2deg) ' +
                                draggableProps.style?.transform,
                            }
                          : draggableProps.style
                      }
                    >
                      {renderItem(item, index)}
                    </div>
                  );
                }}
              </Draggable>
            ))}
            {placeholder as any}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default SortableList;
