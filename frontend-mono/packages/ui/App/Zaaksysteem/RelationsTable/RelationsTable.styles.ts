// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useRelationsTableStyles = makeStyles(
  ({ palette: { primary, common, elephant } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
    search: {
      width: 300,
    },
    relationTypeIcons: {
      display: 'flex',
      gap: 10,
      color: elephant.darker,
    },
    removeIcon: {
      color: common.white,
      backgroundColor: primary.main,
      '&:hover': {
        boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
        backgroundColor: primary.lighter,
      },
    },
    reverse: {
      MozTransform: 'scale(-1, 1)',
      WebkitTransform: 'scale(-1, 1)',
      OTransform: 'scale(-1, 1)',
      MsTransform: 'scale(-1, 1)',
      transform: 'scale(-1, 1)',
    },
  })
);
