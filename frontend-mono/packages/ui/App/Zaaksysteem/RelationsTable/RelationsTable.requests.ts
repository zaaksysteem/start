// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  FetchRelationsType,
  RelateObjectResponseBody,
  RelateObjectToType,
  ResponseBodyType,
  SearchRequestParams,
  SearchResponseBody,
  SearchType,
  UnrelateObjectResponseBody,
  UnrelateObjectType,
} from './RelationsTable.types';

export const fetchRelations: FetchRelationsType = async (
  context,
  uuid,
  type
) => {
  const params = context === 'subject' ? { subject_uuid: uuid } : { uuid };
  const url = buildUrl(`/api/v2/cm/${context}/get_related_${type}`, params);

  const response = await request<ResponseBodyType>('GET', url);

  return response;
};

export const search: SearchType = async params => {
  const response = await request<SearchResponseBody>(
    'GET',
    buildUrl<SearchRequestParams>('/api/v2/cm/search', params)
  );

  return response;
};

export const relateObjectTo: RelateObjectToType = async body => {
  const url = '/api/v2/cm/custom_object/relate_custom_object_to';
  const response = await request<RelateObjectResponseBody>('POST', url, body);

  return response;
};

export const unrelateObjectFrom: UnrelateObjectType = async body => {
  const url = '/api/v2/cm/custom_object/unrelate_custom_object_from';
  const response = await request<UnrelateObjectResponseBody>('POST', url, body);

  return response;
};
