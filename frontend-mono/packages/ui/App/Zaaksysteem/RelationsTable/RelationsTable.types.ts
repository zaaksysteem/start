// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { RowType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useRelationsTableStyles } from './RelationsTable.styles';

export type ContextType = 'case' | 'custom_object' | 'subject';
export type RelationsTypeType = 'cases' | 'objects' | 'subjects';

export type SupportedToAddType = {
  [key in ContextType]: RelationsTypeType[];
};

export type GetSupportedToAddType = (
  context: ContextType,
  type: RelationsTypeType
) => boolean;

type ClassesType = ReturnType<typeof useRelationsTableStyles>;
type UnrelateType = (source: string, relation: string) => Promise<void>;

export type GetColumnType = (
  classes: ClassesType,
  t: i18next.TFunction,
  uuid: string,
  canRemove: boolean,
  unrelate: UnrelateType
) => ColumnType[];
export type GetColumnsType = (
  classes: ClassesType,
  t: i18next.TFunction,
  uuid: string,
  type: RelationsTypeType,
  canRemove: boolean,
  unrelate: UnrelateType
) => ColumnType[];

export interface RelatedCaseRowType extends RowType {
  nr: number;
  progress: number;
  status: string;
  extra?: string;
  caseType: string;
  result?: string;
  assignee?: string;
}

export interface RelatedObjectRowType extends RowType {
  objectType: string;
  relatedToMagicStrings: string[];
  relatedFromMagicStrings: string[];
  relationSource: string[];
}

export interface RelatedSubjectRowType extends RowType {
  name: string;
  subjectType?: string;
  roleType?: any;
}

export type ResponseBodyType =
  | APICaseManagement.GetRelatedCasesResponseBody
  | APICaseManagement.GetRelatedObjectsResponseBody
  | APICaseManagement.GetRelatedSubjectsResponseBody
  | APICaseManagement.GetRelatedSubjectsForSubjectResponseBody
  | APICaseManagement.GetRelatedObjectsForSubjectResponseBody;

export type RawRelationType = ResponseBodyType['data'][0];

export type FormatRelationsType = (
  relations: RawRelationType[],
  uuid: string
) => RelationsType[];

export type RelationsType =
  | RelatedCaseRowType
  | RelatedObjectRowType
  | RelatedSubjectRowType;

export type FetchRelationsType = (
  context: ContextType,
  uuid: string,
  type: RelationsTypeType
) => Promise<ResponseBodyType>;

export type GetRelationsType = (
  context: ContextType,
  uuid: string,
  type: RelationsTypeType
) => Promise<RelationsType[]>;

export type OnRowClickType = (
  relation: RelationsType,
  type: RelationsTypeType
) => void;

// SEARCH

export type SearchResponseBody = APICaseManagement.SearchResponseBody;
export type SearchRequestParams = APICaseManagement.SearchRequestParams;

export type ChoiceType = { label: string; value: string };

export type MapChoiceType = (
  result: SearchResponseBody['data'][0]
) => ChoiceType;

export type PerformSearchType = (
  keyword: string,
  type: RelationsTypeType
) => Promise<ChoiceType[]>;

export type SearchType = (
  params: SearchRequestParams
) => Promise<SearchResponseBody>;

// RELATE OBJECT

export type RelateObjectRequestBody =
  APICaseManagement.RelateCustomObjectToRequestBody;
export type RelateObjectResponseBody =
  APICaseManagement.RelateCustomObjectToResponseBody;

export type RelateObjectToType = (
  body: RelateObjectRequestBody
) => RelateObjectResponseBody;

export type PerformRelateType = (
  context: ContextType,
  uuid: string,
  type: RelationsTypeType,
  value: string
) => Promise<void>;

// UNRELATE OBJECT

export type UnrelateObjectRequestBody =
  APICaseManagement.UnrelateCustomObjectFromRequestBody;
export type UnrelateObjectResponseBody =
  APICaseManagement.UnrelateCustomObjectFromResponseBody;

export type UnrelateObjectType = (
  body: UnrelateObjectRequestBody
) => UnrelateObjectResponseBody;

export type PerformUnrelateType = (
  context: ContextType,
  uuid: string,
  type: RelationsTypeType,
  value: string
) => Promise<void>;
