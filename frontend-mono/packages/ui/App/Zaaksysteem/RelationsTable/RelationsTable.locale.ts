// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    cases: {
      title: 'Zaken',
      columns: {
        nr: 'Nr.',
        status: 'Status',
        progress: 'Voortgang',
        caseType: 'Zaaktype',
        extra: 'Extra informatie',
        assignee: 'Behandelaar',
        result: 'Resultaat',
      },
      placeholder: 'Voeg een zaak toe…',
    },
    objects: {
      title: 'Objecten',
      columns: {
        name: 'Naam',
        objectType: 'Objecttype',
        relationTypes: 'Relatietypes',
        relationType: {
          to: 'Het gerelateerde object is opgenomen in een kenmerk van het object. $t(objects.columns.canNotUnrelate)',
          from: 'Het object is opgenomen in een kenmerk van het gerelateerde object. $t(objects.columns.canNotUnrelate)',
          manual:
            'Het gerelateerde object is handmatig gerelateerd aan het object. $t(objects.columns.canUnrelate)',
        },
        canNotUnrelate:
          'Deze relatie kan verbroken worden door het object uit het kenmerk te verwijderen.',
        canUnrelate: 'Deze relatie kan hier verbroken worden.',
      },
      placeholder: 'Voeg een object toe…',
    },
    subjects: {
      title: 'Betrokkenen',
      columns: {
        name: 'Naam',
        subjectType: 'Type',
        roleType: 'Rol',
        person: 'Burger',
        organization: 'Organisatie',
        employee: 'Medewerker',
      },
      placeholder: 'Voeg een contact toe…',
    },
  },
};
