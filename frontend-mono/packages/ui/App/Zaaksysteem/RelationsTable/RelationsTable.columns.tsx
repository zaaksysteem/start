// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Progress from '@mintlab/ui/App/Zaaksysteem/Progress/Progress';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import {
  GetColumnType,
  GetColumnsType,
  RelatedCaseRowType,
  RelatedObjectRowType,
  RelatedSubjectRowType,
} from './RelationsTable.types';

const getCasesColumns: GetColumnType = (classes, t) => [
  {
    name: 'nr',
    width: 100,
    label: t('cases.columns.nr'),
  },
  {
    name: 'status',
    width: 150,
    label: t('cases.columns.status'),
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }: { rowData: RelatedCaseRowType }) => {
      return <div>{t(`common:case.status.${rowData.status}`) as string}</div>;
    },
  },
  {
    name: 'progress',
    width: 130,
    label: t('cases.columns.progress'),
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }: { rowData: RelatedCaseRowType }) => {
      return (
        <div>
          <Progress percentage={rowData.progress} />
        </div>
      );
    },
  },
  {
    name: 'caseType',
    width: 1,
    flexGrow: 1,
    label: t('cases.columns.caseType'),
  },
  {
    name: 'extra',
    width: 1,
    flexGrow: 1,
    label: t('cases.columns.extra'),
  },
  {
    name: 'assignee',
    width: 1,
    flexGrow: 1,
    label: t('cases.columns.assignee'),
  },
  {
    name: 'result',
    width: 1,
    flexGrow: 1,
    label: t('cases.columns.result'),
  },
];

const getObjectsColumns: GetColumnType = (
  classes,
  t,
  uuid,
  canRemove,
  unrelate
) => [
  {
    name: 'name',
    width: 400,
    label: t('objects.columns.name'),
  },
  {
    name: 'objectType',
    width: 1,
    flexGrow: 1,
    label: t('objects.columns.objectType'),
    /* eslint-disable-next-line */
        cellRenderer: ({ rowData }: { rowData: RelatedObjectRowType }) => {
      return (
        <Tooltip title={rowData.objectType} noWrap={true}>
          <span> {rowData.objectType} </span>
        </Tooltip>
      );
    },
  },
  ...(canRemove
    ? [
        {
          name: 'relationTypes',
          width: 120,
          label: t('objects.columns.relationTypes'),
          cellRenderer: ({
            rowData: {
              relationSource,
              relatedToMagicStrings,
              relatedFromMagicStrings,
            },
          }: any) => (
            <div className={classes.relationTypeIcons}>
              {Boolean(relatedToMagicStrings.length) && (
                <div>
                  <Tooltip title={t('objects.columns.relationType.to')}>
                    <Icon size="small">{iconNames.start}</Icon>
                  </Tooltip>
                </div>
              )}
              {Boolean(relatedFromMagicStrings.length) && (
                <div className={classes.reverse}>
                  <Tooltip title={t('objects.columns.relationType.from')}>
                    <Icon size="small">{iconNames.start}</Icon>
                  </Tooltip>
                </div>
              )}
              {Boolean(relationSource.length) && (
                <div>
                  <Tooltip title={t('objects.columns.relationType.manual')}>
                    <Icon size="small">{iconNames.sync_alt}</Icon>
                  </Tooltip>
                </div>
              )}
            </div>
          ),
        },
        {
          name: 'remove',
          width: 100,
          cellRenderer: ({
            rowData: { uuid: relationUuid, relationSource },
          }: {
            rowData: RelatedObjectRowType;
          }) =>
            relationSource.length ? (
              <IconButton
                onClick={(event: any) => {
                  event.stopPropagation();
                  const source = relationSource[0];
                  const relation = source === uuid ? relationUuid : uuid;

                  unrelate(source, relation);
                }}
                disableRipple={true}
                size="small"
                classes={{
                  root: classes.removeIcon,
                }}
              >
                <Icon size="extraSmall" color="inherit">
                  {iconNames.close}
                </Icon>
              </IconButton>
            ) : (
              <></>
            ),
        },
      ]
    : []),
];

const getSubjectColumns: GetColumnType = (classes, t) => [
  {
    name: 'name',
    width: 400,
    label: t('subjects.columns.name'),
  },
  {
    name: 'subjectType',
    width: 1,
    flexGrow: 1,
    label: t('subjects.columns.subjectType'),
    // eslint-disable-next-line
    cellRenderer: ({ rowData }: { rowData: RelatedSubjectRowType }) => (
      <>{t(`subjects.columns.${rowData.subjectType}`)}</>
    ),
  },
  {
    name: 'rolType',
    width: 1,
    flexGrow: 1,
    label: t('subjects.columns.roleType'),
    cellRenderer: ({ rowData }: { rowData: RelatedSubjectRowType }) => {
      if (!rowData || !rowData?.roleType) return '';

      const getItems = rowData.roleType.map((item: any) => {
        return item.meta.summary;
      });

      return getItems.join(', ');
    },
  },
];

const columnGetters = {
  cases: getCasesColumns,
  objects: getObjectsColumns,
  subjects: getSubjectColumns,
};

export const getColumns: GetColumnsType = (
  classes,
  t,
  uuid,
  type,
  canRemove,
  unrelate
) => {
  const columnGetter = columnGetters[type];
  const columns = columnGetter(classes, t, uuid, canRemove, unrelate);

  return columns;
};
