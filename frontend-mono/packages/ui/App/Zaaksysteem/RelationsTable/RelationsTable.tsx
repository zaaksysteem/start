// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
//@ts-ignore
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import {
  ChoiceType,
  ContextType,
  RelationsType,
  RelationsTypeType,
} from './RelationsTable.types';
import { getColumns } from './RelationsTable.columns';
import {
  getRelations,
  getSupportedToAdd,
  onRowClick,
  performRelate,
  performSearch,
  performUnrelate,
} from './RelationsTable.library';
import { useRelationsTableStyles } from './RelationsTable.styles';

export type RelationsTablePropsType = {
  context: ContextType;
  uuid: string;
  type: RelationsTypeType;
  canAdd?: boolean;
  canRemove?: boolean;
};

const RelationsTable: React.FunctionComponent<RelationsTablePropsType> = ({
  context,
  uuid,
  type,
  canAdd = false,
  canRemove = false,
}) => {
  const [t] = useTranslation('relationsTable');
  const classes = useRelationsTableStyles();
  const queryClient = useQueryClient();

  const [choices, setChoices] = useState<ChoiceType[]>();

  const query = `${context}-view-${type}-relations`;
  const { data: rows } = useQuery<RelationsType[], V2ServerErrorsType>(
    [query, uuid],
    () => getRelations(context, uuid, type),
    { onError: openServerError }
  );

  if (!rows) {
    return <Loader />;
  }

  const unrelate = async (source: string, relation: string) => {
    await performUnrelate(context, source, type, relation);

    queryClient.invalidateQueries([query]);
  };

  const columns = getColumns(classes, t, uuid, type, canRemove, unrelate);
  const rowHeight = 50;
  const minHeight = rowHeight * 2;
  const height = 25 + rowHeight * (rows.length + 1);

  const supportedToAdd = getSupportedToAdd(context, type);

  const isObjectToObject = context === 'custom_object' && type === 'objects';
  const existingUuids = rows.reduce(
    (acc, row) =>
      // @ts-ignore
      !isObjectToObject || row.relationSource?.length
        ? [...acc, row.uuid]
        : acc,
    [] as string[]
  );

  const onInputChange = async ({ target: { value } }: any) => {
    const results = await performSearch(value, type);

    const filteredResults = results.filter(
      result => !existingUuids.includes(result.value) && result.value !== uuid
    );

    setChoices(filteredResults);
  };

  const onChange = async ({ target: { value } }: any) => {
    await performRelate(context, uuid, type, value);

    queryClient.invalidateQueries([query]);
  };

  return (
    <div className={classes.wrapper}>
      <div style={{ minHeight, height }}>
        <Typography variant="h4">{t(`${type}.title`)}</Typography>
        <SortableTable
          rows={rows}
          // @ts-ignore
          columns={columns}
          noRowsMessage={t('common:general.noResults')}
          loading={false}
          rowHeight={rowHeight}
          onRowClick={(event: any) => onRowClick(event.rowData, type)}
        />
      </div>
      {canAdd && supportedToAdd && (
        <div className={classes.search}>
          <Select
            name="searchType"
            onInputChange={onInputChange}
            onChange={onChange}
            choices={choices}
            filterOption={() => true}
            placeholder={t(`${type}.placeholder`)}
            nestedValue={true}
            isClearable={false}
          />
        </div>
      )}
    </div>
  );
};

export default RelationsTable;
