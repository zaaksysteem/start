// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ButtonBase from '@mui/material/ButtonBase';
import { useTheme } from '@mui/material';
import { addScopeProp } from '../../../library/addScope';
import { Theme } from '../../../../types/Theme';

export const BannerButton = ({ action, children, variant, scope }: any) => {
  const {
    palette,
    mintlab: { radius },
    typography,
  } = useTheme<Theme>();

  return (
    <ButtonBase
      sx={{
        color: palette.common.white,
        padding: '0px 18px',
        height: '26px',
        borderRadius: radius.bannerButton,
        fontWeight: typography.fontWeightMedium,
        fontSize: '14px',
        lineHeight: '0px',
        //@ts-ignore
        backgroundColor: palette[variant || 'primary'].main,
      }}
      onClick={action}
      {...addScopeProp(scope, 'button')}
    >
      {children}
    </ButtonBase>
  );
};

export default BannerButton;
