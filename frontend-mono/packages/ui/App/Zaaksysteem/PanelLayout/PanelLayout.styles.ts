// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePanelLayoutStyles = makeStyles(
  ({ mintlab: { greyscale }, breakpoints }: Theme) => ({
    wrapper: {
      display: 'flex',
      flex: 1,
      height: '100%',
      minWidth: 1,
    },
    panel: {
      borderRight: `1px solid ${greyscale.darker}`,
      overflow: 'auto',
    },
    fluid: {
      flex: 1,
    },
    side: {
      width: '244px',
      overflow: 'hidden',
    },
    folded: {
      width: '60px',
    },
  })
);
