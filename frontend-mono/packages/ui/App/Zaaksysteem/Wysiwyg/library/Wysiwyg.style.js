// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const wysiwygStyleSheet = ({
  palette: { error },
  typography: { fontFamily },
  mintlab: { radius, greyscale },
}) => ({
  defaultClass: {
    backgroundColor: greyscale.dark,
    '& .rdw-option-wrapper': {
      backgroundColor: 'transparent',
    },
    borderRadius: radius.wysiwyg,
    fontFamily,
  },
  errorClass: {
    backgroundColor: error.light,
    '& .rdw-option-wrapper': {
      backgroundColor: error.light,
    },
  },
  editorClass: {
    minHeight: 70,
    maxHeight: 250,
    padding: '5px 10px',
    '&& .public-DraftStyleDefault-block': {
      margin: 0,
    },
    '&:hover': {
      cursor: 'text',
    },
  },
  disabledClass: {
    opacity: 0.4,
  },
  readOnlyEditorClass: {
    border: `1px solid ${greyscale.darker}`,
    borderRadius: 0,
  },
  disabledToolbarClass: {
    display: 'none !important',
  },
});
