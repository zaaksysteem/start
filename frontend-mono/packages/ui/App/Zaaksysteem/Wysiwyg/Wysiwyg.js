// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import LazyLoader from '../../Abstract/LazyLoader';

/**
 * @param {Object} props
 * @return {ReactElement}
 */
export const Wysiwyg = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "ui.wysiwyg" */
        './library/Wysiwyg'
      )
    }
    {...props}
  />
);

export default Wysiwyg;
