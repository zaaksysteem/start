// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Badge, Box } from '@mui/material';
import { capitalize } from '@mintlab/kitchen-sink/source';

type CountPropsType = {
  count?: number;
  className?: string;
  children?: any;
  vertical?: 'top' | 'bottom';
  horizontal?: 'left' | 'right';
  max?: number;
  margin?: number;
};

const Counter: React.FunctionComponent<CountPropsType> = ({
  count,
  children,
  className,
  max,
  vertical = 'top',
  horizontal = 'right',
  margin = 0,
}) => {
  const sx = {
    [`margin${capitalize(horizontal)}`]: margin / 8,
  };

  return (
    <Badge
      badgeContent={count}
      max={max}
      anchorOrigin={{ vertical, horizontal }}
      classes={{ badge: className }}
    >
      <Box sx={sx}>{children}</Box>
    </Badge>
  );
};

export default Counter;
