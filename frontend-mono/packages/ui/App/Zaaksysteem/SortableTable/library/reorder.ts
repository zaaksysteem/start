// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ColumnType } from '../SortableTable.types';

export const reorder = (
  list: any[],
  startIndex: number,
  endIndex: number
): any[] => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const getDefaultSorting = (columns: ColumnType[]) => {
  const [defaultColumn] = columns.filter(thisColumn => thisColumn.defaultSort);
  return defaultColumn?.name || '';
};
