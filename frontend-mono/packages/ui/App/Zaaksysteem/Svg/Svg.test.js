// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import Svg from '.';

/**
 * @test {Svg}
 */
describe('The `Svg` component', () => {
  test('creates the asserted HTML', () => {
    const wrapper = shallow(
      <Svg viewBox={1}>
        <rect />
      </Svg>
    );
    const actual = wrapper.html();
    const asserted =
      '<span class="" style="width:1px;height:1px"><svg viewBox="0 0 1 1"><rect></rect></svg></span>';

    expect(actual).toBe(asserted);
  });
});
