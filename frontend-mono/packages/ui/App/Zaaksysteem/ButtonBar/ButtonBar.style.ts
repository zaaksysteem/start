// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useButtonBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      flexWrap: 'wrap',
    },
    textButton: {
      height: 44,
      display: 'flex',
      alignItems: 'center',
    },
    divider: {
      width: 1,
      height: 30,
      margin: '5px 10px 0 10px',
      background: greyscale.darker,
    },
  })
);
