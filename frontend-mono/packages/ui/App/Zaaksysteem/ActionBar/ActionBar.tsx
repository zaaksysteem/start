// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ButtonBar, {
  ButtonBarPropsType,
} from '@mintlab/ui/App/Zaaksysteem/ButtonBar';
import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Icon from '@mintlab/ui/App/Material/Icon';
import { Box } from '@mui/material';
import { useActionBarStyles } from './ActionBar.style';
import { FilterType } from './ActionBar.types';

type ActionBarPropsType = {
  filters?: FilterType[];
} & ButtonBarPropsType;

/* eslint complexity: [2, 8] */
const ActionBar: React.FunctionComponent<ActionBarPropsType> = ({
  filters,
  actions,
  advancedActions,
  permanentActions,
}) => {
  const classes = useActionBarStyles();

  const hasFilters = filters && filters.length > 0;
  const hasActions =
    actions?.length || advancedActions?.length || permanentActions?.length;

  if (!hasFilters && !hasActions) return null;

  return (
    <div className={classes.wrapper}>
      <div className={classes.filters}>
        {filters?.map(({ type, width = 250, name, icon, ...rest }, index) => (
          <Box
            key={`filter-${index}`}
            sx={{ minWidth: 200, width: 1, maxWidth: width, height: 40 }}
          >
            {type === 'text' && (
              <TextField
                {...rest}
                startAdornment={
                  icon ? (
                    <Icon size="small" color="inherit">
                      {icon}
                    </Icon>
                  ) : undefined
                }
              />
            )}
            {type === 'select' && <Select {...rest} />}
            {type === 'checkbox' && name && <Checkbox {...rest} name={name} />}
          </Box>
        ))}
      </div>
      <ButtonBar
        actions={actions}
        advancedActions={advancedActions}
        permanentActions={permanentActions}
      />
    </div>
  );
};

export default ActionBar;
