// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { useMiniProgressStyles } from './MiniProgress.style';

type MiniProgressPropsType = {
  progress: number;
  showPercentage?: boolean;
};

const MiniProgress: FunctionComponent<MiniProgressPropsType> = ({
  progress,
  showPercentage = false,
}) => {
  const classes = useMiniProgressStyles();

  return (
    <div className={classes.progressWrapper}>
      <div className={classes.progressBarWrapper}>
        <div
          className={classes.progressBarInner}
          style={{ width: `${progress}%` }}
        />
      </div>
      {showPercentage && <div>{progress}%</div>}
    </div>
  );
};

export default MiniProgress;
