// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const INCREMENT = 1;

export const iterator = (length: number) =>
  Array.from(Array(length).keys()).map(number => number + INCREMENT);
