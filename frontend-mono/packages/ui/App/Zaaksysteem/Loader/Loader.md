# 🔌 `Loader` component

> React loader component based on *SpinKit*.

## See also

- [`Loader` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Loader)
- [`Loader` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-loader)

## External resources

- [SpinKit](https://github.com/tobiasahlin/SpinKit)
