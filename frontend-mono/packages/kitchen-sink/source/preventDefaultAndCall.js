// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Executes callback and prevents default event behavior when
 * `altKey`, `ctrlKey`, `shiftKey`, `metaKey` are not pressed
 * @param {Function} callback
 * @return {Function}
 */
export const preventDefaultAndCall = callback => event => {
  const { altKey, ctrlKey, shiftKey, metaKey } = event;

  const shouldPreventDefault = [altKey, ctrlKey, shiftKey, metaKey].every(
    value => value === false
  );

  if (shouldPreventDefault) {
    event.preventDefault();
    callback && callback(event);
  }
};

export default preventDefaultAndCall;
