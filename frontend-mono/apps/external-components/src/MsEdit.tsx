// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIDocument } from '@zaaksysteem/generated/types/APIDocument.types';

const msOfficeIframe = document.createElement('iframe');
const form = document.createElement('form');

msOfficeIframe.name = 'office_frame';
msOfficeIframe.id = 'office_frame';

msOfficeIframe.title = 'Office Frame';
msOfficeIframe.frameBorder = '0';

msOfficeIframe.setAttribute('allowfullscreen', 'true');
msOfficeIframe.setAttribute(
  'sandbox',
  'allow-scripts allow-same-origin allow-forms allow-popups allow-top-navigation allow-popups-to-escape-sandbox'
);
msOfficeIframe.style.position = 'absolute';
msOfficeIframe.style.top = '0';
msOfficeIframe.style.left = '0';
msOfficeIframe.style.width = '100%';
msOfficeIframe.style.height = '100%';
msOfficeIframe.style.zIndex = '1000';

msOfficeIframe.addEventListener('load', function () {
  setTimeout(() => {
    (msOfficeIframe?.contentWindow as any)?.postMessage(
      {
        MessageId: 'Host_PostmessageReady',
        SendTime: new Date().getTime(),
        Values: {},
      },
      '*'
    );
  }, 3000);
});

const id = (window.location.href.split('documentUuid=') || [])[1] || '';

if (!id) {
  alert('Document uuid?');
} else {
  (async () => {
    const response = await request<APIDocument.EditDocumentOnlineResponseBody>(
      'GET',
      buildUrl<APIDocument.EditDocumentOnlineRequestParams>(
        '/api/v2/document/edit_document_online',
        {
          id,
          editor_type: 'msonline',
          close_url: window.location.origin + window.location.pathname,
          host_page_url: window.location.origin + window.location.pathname,
        }
      )
    );

    const inputTokenA = document.createElement('input');
    const inputTokenB = document.createElement('input');
    form.id = 'office_form';
    form.name = 'office_form';
    form.action = response.action_url;
    form.target = 'office_frame';
    form.method = 'post';
    inputTokenA.type = 'hidden';
    inputTokenA.name = 'access_token';
    inputTokenA.value = response.access_token;
    inputTokenB.type = 'hidden';
    inputTokenB.name = 'access_token_ttl';
    inputTokenB.value = response.access_token_ttl;

    msOfficeIframe.addEventListener('load', () => {
      setTimeout(function () {
        (msOfficeIframe.contentWindow as any).postMessage(
          {
            MessageId: 'Host_PostmessageReady',
            SendTime: new Date().getTime(),
            Values: {},
          },
          '*'
        );
      }, 3000);
    });

    form.appendChild(inputTokenA);
    form.appendChild(inputTokenB);

    document.body.appendChild(msOfficeIframe);
    document.body.appendChild(form);
    form.submit();
  })();
}
