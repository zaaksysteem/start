// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { getDataFuncType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { TimelineItemType } from '@zaaksysteem/common/src/components/Timeline/Timeline.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { PAGE_LENGTH } from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { openServerError } from '@zaaksysteem/common/src/signals';

type GetDataType = ({
  uuid,
}: {
  uuid: string;
}) => getDataFuncType<TimelineItemType, any>;

export const getData: GetDataType = ({ uuid }) =>
  async function getDataAsync({ pageNum, keyword, startDate, endDate }: any) {
    const urlParams: APICaseManagement.GetCustomObjectEventLogsRequestParams = {
      page: pageNum,
      page_size: PAGE_LENGTH,
      ...(uuid && { custom_object_uuid: uuid }),
      ...(startDate && {
        period_start:
          fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00',
      }),
      ...(endDate && {
        period_end:
          fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00',
      }),
    };

    const url =
      buildUrl<APICaseManagement.GetCustomObjectEventLogsRequestParams>(
        `/api/v2/cm/custom_object/get_custom_object_event_logs`,
        urlParams
      );
    const result = (await request('GET', url).catch(
      openServerError
    )) as APICaseManagement.GetCustomObjectEventLogsResponseBody | null;

    if (!result || !result.data) return { rows: [] };

    return {
      rows: result.data.map(row => {
        const { attributes } = row;

        return {
          id: attributes?.id || '',
          type: attributes?.type || '',
          author: attributes?.user || '',
          date: new Date(attributes?.date || ''),
          description: attributes?.description || '',
        };
      }),
    };
  };
