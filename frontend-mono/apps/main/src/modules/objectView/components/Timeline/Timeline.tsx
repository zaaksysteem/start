// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Timeline from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { DataType } from '../../ObjectView.types';
import { useStyles } from './Timeline.styles';
import { getData } from './Timeline.library';

type ObjectViewTimelineProps = Pick<DataType, 'object'>;

const ObjectViewTimeline: React.FunctionComponent<ObjectViewTimelineProps> = ({
  object,
}) => {
  const classes = useStyles();
  const getDataFunction = getData({ uuid: object.versionIndependentUuid });

  return (
    <div className={classes.wrapper}>
      <div className={classes.timelineWrapper}>
        <Timeline getData={getDataFunction} />
      </div>
    </div>
  );
};

export default ObjectViewTimeline;
