// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttributesStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 20px',
    gap: 20,
  },
  header: {
    display: 'flex',
    justifyContent: 'end',
    position: 'relative',
  },
  buttons: {
    position: 'absolute',
    display: 'flex',
    padding: 20,
    gap: 10,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  colContent: {
    flex: 1,
  },
  dialogContent: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
}));
