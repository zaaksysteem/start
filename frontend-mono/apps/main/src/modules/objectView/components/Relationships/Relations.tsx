// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import RelationsTable from '@mintlab/ui/App/Zaaksysteem/RelationsTable';
import { DataType } from '../../ObjectView.types';
import { useRelationshipsStyles } from './Relations.styles';

type RelationshipsPropsType = Pick<DataType, 'object'>;

const Relationships: React.FunctionComponent<RelationshipsPropsType> = ({
  object: { versionIndependentUuid: uuid },
}) => {
  const classes = useRelationshipsStyles();

  return (
    <div className={classes.wrapper}>
      <RelationsTable
        context="custom_object"
        uuid={uuid}
        type="cases"
        canAdd={true}
      />
      <RelationsTable
        context="custom_object"
        uuid={uuid}
        type="objects"
        canAdd={true}
        canRemove={true}
      />
      <RelationsTable context="custom_object" uuid={uuid} type="subjects" />
    </div>
  );
};

export default Relationships;
