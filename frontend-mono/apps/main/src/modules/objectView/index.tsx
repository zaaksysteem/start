// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import locale from './ObjectView.locale';
import ObjectView from './ObjectView';
import { fetchData } from './ObjectView.library';
import { DataType } from './ObjectView.types';

export type ObjectViewModuleParamsType = {
  uuid: string;
  ['*']: 'attributes' | 'timeline' | 'map' | 'relationships';
};

export const OBJECT_VIEW_DATA = 'objectViewData';

const ObjectViewModule: React.ComponentType = () => {
  const { uuid } = useParams<
    keyof ObjectViewModuleParamsType
  >() as ObjectViewModuleParamsType;

  const { data, isFetching } = useQuery<DataType, V2ServerErrorsType>(
    [OBJECT_VIEW_DATA],
    () => fetchData(uuid),
    { onError: openServerError }
  );

  if (!data || isFetching) {
    return <Loader />;
  }

  return (
    <Sheet>
      <ObjectView {...data} />
      <TopbarTitle title={data.object.title} />
    </Sheet>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="objectView">
    <ObjectViewModule />
  </I18nResourceBundle>
);
