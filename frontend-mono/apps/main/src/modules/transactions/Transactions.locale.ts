// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    transactions: 'Transactieoverzicht',
    integration: {
      placeholder: 'Selecteer een koppelprofiel…',
      all: 'Alle koppelprofielen',
    },
    withErrors: 'Alleen met fout',
    keyword: 'Typ om te zoeken…',
    table: {
      noResults: 'Geen resultaten',
      labelRowsPerPage: 'Per pagina',
      unknown: 'Onbekend',
      columns: {},
      values: {
        direction: {
          incoming: 'Inkomend',
          outgoing: 'Uitgaand',
        },
      },
      selectEverything: {
        selectEverything: 'Selecteer alles',
        deselectEverything: 'Deselecteer alles',
        select: 'Alleen de transacties op deze pagina zijn geselecteerd.',
        selectButton: 'Selecteer alle transacties',
        deselect: 'Alle transacties zijn geselecteerd.',
        deselectButton: 'Selecteer alleen de objecten op deze pagina',
      },
    },
    action: {
      button: 'Actie uitvoeren',
      retry: 'Opnieuw uitvoeren',
      delete: 'Verwijderen',
      dialog: {
        warning: {
          verb: {
            retry: 'opnieuw te proberen',
            delete: 'te verwijderen',
          },
          selected:
            'U staat op het punt om {{count}} geselecteerde transactie(s) {{verb}}.',
          all: 'U staat op het punt om de volgende transacties {{verb}}',
        },
        check: 'Weet u zeker dat u door wilt gaan?',
        integration: 'Koppelprofiel',
        withError: 'Met fout',
        withKeyword: 'Met tekst',
      },
      snack: {
        retry: 'Actie succesvol uitgevoerd.',
        delete: 'Transactie(s) succesvol verwijderd.',
      },
    },
    manual: {
      button: 'Handmatige transactie',
      dialog: {
        title: 'Handmatige transactie',
        fields: {
          integration: 'Koppelprofiel',
          unsupported:
            'Dit koppelprofiel ondersteunt geen handmatige transacties.',
          text: 'Bericht (tekst)',
          file: 'Bericht (upload)',
          references: 'Referenties',
        },
      },
      snack: 'Transactie geaccepteerd',
    },
    transaction: {
      transactionUuid: 'Transactie UUID',
      status: 'Status',
      record: 'Naam (Voorbeeld)',
      direction: 'Richting',
      externalId: 'Extern ID',
      nextAttempt: 'Volgende poging',
      created: 'Aangemaakt',
      records: 'Records',
      errors: 'Fouten',
      requestData: 'Request data',
      errorMessage: 'Error',
    },
    record: {
      recordUuid: 'Record UUID',
      result: 'Resultaat',
      preview: 'Voorbeeld',
      executed: 'Uitgevoerd',
      input: 'Request',
      output: 'Response',
    },
  },
};
