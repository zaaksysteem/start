// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { IntegrationType, FiltersType } from '../Transactions.types';
import { getTransactions } from './Transactions.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const useDebounce = (value: FiltersType, delay: number) => {
  const [debouncedValue, setDebouncedValue] = React.useState(value);

  useEffect(() => {
    const handler: NodeJS.Timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
};

type TransactionsPropsType = {
  integrations: IntegrationType[];
};

/* eslint complexity: [2, 7] */
const Transactions: React.ComponentType<TransactionsPropsType> = ({
  integrations,
}) => {
  const [t] = useTranslation('transactions');

  const [searchParams] = useSearchParams();
  const { integrationId, withError } = Object.fromEntries([...searchParams]);

  const defaultIntegration = (integrations || []).find(
    integration => integration.value === integrationId
  );

  const [filters, setFilters] = useState<FiltersType>({
    integration: defaultIntegration || integrations[0],
    withError: Boolean(withError) && withError === '1',
    keyword: '',
    page: 0,
    numRows: 20,
  });

  const debouncedFilters = useDebounce(filters, 300);

  const { data } = useQuery(
    ['transactions', debouncedFilters],
    () => getTransactions(debouncedFilters, integrations),
    {
      onSuccess: () => selectionProps.resetAll(),
      onError: openServerError,
      refetchOnWindowFocus: true,
      staleTime: 0,
    }
  );

  const selectionProps = useSelectionBehaviour({
    rows: data?.rows,
    selectEverythingTranslations: t('table.selectEverything', {
      returnObjects: true,
    }) as any,
    page: filters.page,
    resultsPerPage: filters.numRows,
  });

  const { selectedRows, everythingSelected, resetAll } = selectionProps;

  // selection works based on UUID, but the old POST endpoints still use ID
  // when implementing v2 POST endpoints:
  // - remove selectedRowIds and pass selectedRows
  // - remove id from transactions in types and library formatter
  // - change SelectedRowsType to string[]
  // - change selectedRows in DataTable to use .uuid instead of .id
  const selectedRowIds = selectedRows.map(
    selectedRow =>
      data?.rows.find(row => row.uuid === selectedRow)?.id as number
  );

  const queryClient = useQueryClient();
  const refreshTransactions = () =>
    queryClient.invalidateQueries(['transactions']);

  return (
    <Sheet>
      <ActionBar
        integrations={integrations}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRowIds}
        everythingSelected={everythingSelected}
        refreshTransactions={refreshTransactions}
        resetAll={resetAll}
      />
      <DataTable
        data={data}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRowIds}
        selectionProps={selectionProps}
      />
      <TopbarTitle title={t('transactions')} />
    </Sheet>
  );
};

export default Transactions;
