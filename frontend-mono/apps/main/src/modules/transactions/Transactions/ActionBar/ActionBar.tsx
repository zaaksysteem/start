// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  IntegrationType,
  FiltersType,
  SetFiltersType,
  SelectedRowsType,
  EverythingSelectedType,
  RefreshTransactionsType,
} from '../../Transactions.types';
import ActionDialog from './ActionDialog';
import ManualDialog from './ManualDialog';

type ActionBarPropsType = {
  integrations: IntegrationType[];
  filters: FiltersType;
  setFilters: SetFiltersType;
  selectedRows: SelectedRowsType;
  everythingSelected: EverythingSelectedType;
  refreshTransactions: RefreshTransactionsType;
  resetAll: useSelectionBehaviourReturnType['resetAll'];
};

const ActionBarWrapper: React.ComponentType<ActionBarPropsType> = ({
  integrations,
  filters,
  setFilters,
  selectedRows,
  everythingSelected,
  refreshTransactions,
  resetAll,
}) => {
  const [t] = useTranslation('transactions');
  const [dialogOpen, setDialogOpen] = useState<
    'manual' | 'retry' | 'delete' | null
  >(null);
  const actionDialogOpen = dialogOpen === 'retry' || dialogOpen === 'delete';

  return (
    <>
      <ActionBar
        filters={[
          {
            type: 'select',
            value: filters.integration.value,
            name: 'data-type',
            isClearable: false,
            onChange: (event: any) => {
              const integration = integrations.find(
                integration => integration.value == event.target.value.value
              );

              if (!integration) return;

              setFilters({ ...filters, integration });
            },
            placeholder: t('integration.placeholder'),
            choices: integrations,
          },
          {
            type: 'text',
            value: filters.keyword,
            onChange: (event: any) => {
              setFilters({ ...filters, keyword: event.target.value });
            },
            placeholder: t('keyword'),
            closeAction: () => setFilters({ ...filters, keyword: '' }),
          },
          {
            type: 'checkbox',
            name: 'action-bar-filters',
            label: t('withErrors'),
            onChange: () => {
              setFilters({ ...filters, withError: !filters.withError });
            },
            checked: filters.withError,
          },
        ]}
        actions={[
          {
            name: 'manual',
            label: t('manual.button'),
            action: () => {
              setDialogOpen('manual');
            },
            icon: iconNames.add,
            disabled: false,
          },
          ...(selectedRows.length
            ? [
                {
                  name: 'retry',
                  label: t('action.retry'),
                  action: () => {
                    setDialogOpen('retry');
                  },
                  icon: iconNames.refresh,
                  disabled: !selectedRows.length,
                },
                {
                  name: 'delete',
                  label: t('action.delete'),
                  action: () => {
                    setDialogOpen('delete');
                  },
                  icon: iconNames.delete,
                  disabled: !selectedRows.length,
                },
              ]
            : []),
        ]}
      />
      {actionDialogOpen && (
        <ActionDialog
          type={dialogOpen}
          filters={filters}
          selectedRows={selectedRows}
          everythingSelected={everythingSelected}
          refreshTransactions={refreshTransactions}
          resetAll={resetAll}
          onClose={() => setDialogOpen(null)}
          open={actionDialogOpen}
        />
      )}
      {dialogOpen === 'manual' && (
        <ManualDialog
          integrations={integrations}
          refreshTransactions={refreshTransactions}
          onClose={() => setDialogOpen(null)}
          open={dialogOpen === 'manual'}
        />
      )}
    </>
  );
};

export default ActionBarWrapper;
