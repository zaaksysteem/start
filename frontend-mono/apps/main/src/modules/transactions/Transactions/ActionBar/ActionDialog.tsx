// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import {
  snackbarMargin,
  openServerError,
  openSnackbar,
} from '@zaaksysteem/common/src/signals';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  FiltersType,
  ActionType,
  SelectedRowsType,
  RefreshTransactionsType,
  EverythingSelectedType,
} from '../../Transactions.types';
import { performActionAction } from '../Transactions.library';

type FiltersDialogPropsType = {
  type: ActionType;
  filters: FiltersType;
  selectedRows: SelectedRowsType;
  everythingSelected: EverythingSelectedType;
  refreshTransactions: RefreshTransactionsType;
  resetAll: useSelectionBehaviourReturnType['resetAll'];
  onClose: () => void;
  open: boolean;
};

/* eslint complexity: [2, 7] */
const ActionDialog: React.ComponentType<FiltersDialogPropsType> = ({
  type,
  filters,
  selectedRows,
  everythingSelected,
  refreshTransactions,
  resetAll,
  onClose,
  open,
}) => {
  const [t] = useTranslation('transactions');
  const dialogEl = useRef();

  const title = t(`action.${type}`);

  const performAction = () => {
    performActionAction(type, filters, selectedRows, everythingSelected)
      .then(() => {
        openSnackbar({
          message: t(`action.snack.${type}`),
          sx: snackbarMargin,
        });
        refreshTransactions();
        resetAll();
        onClose();
      })
      .catch(openServerError);
  };

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'transactions-action-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon={type === 'retry' ? iconNames.refresh : iconNames.delete}
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        {!everythingSelected && (
          <p>
            {t('action.dialog.warning.selected', {
              count: selectedRows.length,
              verb: t(`action.dialog.warning.verb.${type}`),
            })}
          </p>
        )}
        {everythingSelected && (
          <>
            <p>{`${t('action.dialog.warning.all')}:`}</p>
            <ul>
              {filters.integration.value && (
                <li>{`${t('action.dialog.integration')}: ${
                  filters.integration.label
                }`}</li>
              )}
              {filters.withError && <li>{t('action.dialog.withError')}</li>}
              {filters.keyword && (
                <li>{`${t('action.dialog.withKeyword')}: ${
                  filters.keyword
                }`}</li>
              )}
            </ul>
          </>
        )}
        <p>{t('action.dialog.check')}</p>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t(`action.${type}`),
                onClick() {
                  performAction();
                },
              },
              {
                text: t('common:dialog.cancel'),
                onClick: onClose,
              },
            ],
            'datastore-bulk-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default ActionDialog;
