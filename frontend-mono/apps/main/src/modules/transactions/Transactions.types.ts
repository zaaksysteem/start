// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIAdmin } from '@zaaksysteem/generated';

// INTEGRATIONS

type ManualTypeType = 'text' | 'file' | 'references' | 'unsupported';

export type IntegrationsResponseBodyType = APIAdmin.GetIntegrationsResponseBody;

export type IntegrationType = {
  uuid: string;
  id: number;
  value: string;
  manualTypes: ManualTypeType[];
  label: string;
};

export type FetchIntegrationsType = () => Promise<IntegrationsResponseBodyType>;
export type GetIntegrationType = () => Promise<IntegrationType[]>;

// TRANSACTIONS

export type TransactionsParamsType = APIAdmin.GetTransactionsRequestParams;
export type TransactionsResponseBodyType = APIAdmin.GetTransactionsResponseBody;

export type TransactionType = {
  uuid: string;
  id: number;
  status: 'success' | 'error' | 'pending';
  name: string;
  interface: string;
  record: {
    preview?: string | null;
  };
  direction: string;
  externalId: string;
  nextAttempt: string | null;
  created: string;
  records: number;
  errors: number;
  errorMessage: string | null;
};

export type FetchTransactionsType = (
  params: TransactionsParamsType
) => Promise<TransactionsResponseBodyType>;

export type TransactionsDataType = {
  count: number;
  rows: TransactionType[];
};

export type GetTransactionsType = (
  values: any,
  integrations: IntegrationType[]
) => Promise<TransactionsDataType>;

export type TransactionParamsType = APIAdmin.GetTransactionRequestParams;
export type TransactionResponseBodyType = APIAdmin.GetTransactionResponseBody;

export type FetchTransactionType = (
  transactionUuid: string
) => Promise<TransactionResponseBodyType>;

export type GetTransactionType = (
  transactionUuid: string,
  integrations: IntegrationType[]
) => Promise<TransactionType>;

export type RefreshTransactionsType = () => void;

// RECORDS

export type RecordsParamsType = APIAdmin.GetTransactionRecordsRequestParams;
export type RecordsResponseBodyType =
  APIAdmin.GetTransactionRecordsResponseBody;

export type RecordType = {
  uuid: string;
  name: string;
  preview?: string | null;
  result: boolean;
  executed: string;
  input: string;
  output: string;
};

export type FetchRecordsType = (
  params: RecordsParamsType
) => Promise<RecordsResponseBodyType>;

export type GetRecordsType = (transactionUuid: string) => Promise<RecordType[]>;

export type SetRecordType = (record: RecordType) => void;

// REQUEST DATA

export type RequestDataParamsType = APIAdmin.GetTransactionDataRequestParams;
export type RequestDataResponseBodyType =
  APIAdmin.GetTransactionDataResponseBody;

export type FetchRequestDataType = (
  params: RequestDataParamsType
) => Promise<RequestDataResponseBodyType>;

export type RequestDataType = string;

export type GetRequestDataType = (
  transactionUuid: string
) => Promise<RequestDataType>;

// OTHER

export type FiltersType = {
  integration: IntegrationType;
  withError: boolean;
  keyword: string;
  page: number;
  numRows: number;
  sortBy?: string;
  sortDirection?: string;
};

export type SetFiltersType = React.Dispatch<React.SetStateAction<FiltersType>>;

export type SelectedRowsType = number[];
export type SetSelectedType = React.Dispatch<
  React.SetStateAction<SelectedRowsType>
>;

export type EverythingSelectedType = boolean;
export type SetAllSelectedType = React.Dispatch<
  React.SetStateAction<EverythingSelectedType>
>;

// ACTION

export type ActionType = 'retry' | 'delete';

export type PerformActionActionType = (
  action: ActionType,
  filters: FiltersType,
  selected: SelectedRowsType,
  allSelected: EverythingSelectedType
) => Promise<boolean>;

export type PerformActionType = (
  action: ActionType,
  data: {
    freeform_filter: string;
    interface_id?: number;
    'records.is_error': number | string;
    selection_id: number[];
    selection_type: string;
  }
) => Promise<any>;

// MANUAL TRANSACTION

export type PerformManualTransactionType = (values: any) => Promise<boolean>;

export type PostManualTransactionType = (data: {
  interface: number;
  input_data?: string;
  input_filestore_uuid?: string;
  input_references?: string;
}) => Promise<any>;
