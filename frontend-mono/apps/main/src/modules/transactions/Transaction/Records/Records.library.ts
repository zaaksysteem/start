// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { cellRenderer } from './RecordsCell';

export const supportSort = ['direction', 'externalId', 'created'];

export const getColumns = (t: i18next.TFunction, classes: any) =>
  [
    {
      name: 'recordUuid',
      width: 310,
    },
    {
      name: 'preview',
      width: 1,
      minWidth: 200,
      flexGrow: 1,
    },
    {
      name: 'result',
      width: 100,
    },
    {
      name: 'executed',
      width: 200,
    },
  ].map(column => ({
    ...column,
    label: t(`record.${column.name}`),
    disableSort: true,
    cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
  }));
