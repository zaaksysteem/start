// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useDataTableStyles, useTableStyles } from './Records.style';
import { RecordType } from './../../Transactions.types';
import { getColumns } from './Records.library';
import RecordDialog from './RecordDialog';

const headerHeight = '50px';
const rowHeight = '42px';
const paginatorHeight = '45px';

type RecordsPropsType = {
  records: RecordType[];
};

const Records: React.ComponentType<RecordsPropsType> = ({ records }) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const [record, setRecord] = useState<RecordType | null>(null);
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('transactions');

  const columns = getColumns(t as any, classes);
  const rows = records;

  const minRows = Math.min(3, rows.length);
  const minTableHeight = `calc(${headerHeight} + ${minRows} * ${rowHeight} + ${paginatorHeight})`;

  return (
    <div className={classes.wrapper} style={{ minHeight: minTableHeight }}>
      <div
        style={{
          flex: '1 1 auto',
        }}
      >
        <SortableTable
          styles={{ ...sortableTableStyles, ...tableStyles }}
          rows={rows}
          //@ts-ignore
          columns={columns}
          rowHeight={42}
          loading={false}
          noRowsMessage={t('table.noResults')}
          onRowClick={({ rowData }: { rowData: RecordType }) =>
            setRecord(rowData)
          }
        />
        {record && (
          <RecordDialog
            record={record}
            onClose={() => setRecord(null)}
            open={Boolean(record)}
          />
        )}
      </div>
    </div>
  );
};

export default Records;
