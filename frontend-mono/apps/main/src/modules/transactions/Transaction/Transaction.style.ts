// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTransactionStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    sheet: {
      overflowY: 'auto',
    },
    formWrapper: {
      padding: '20px 20px 0 20px',
      borderBottom: `1px solid ${greyscale.darker}`,
    },
  })
);
