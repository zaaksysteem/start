// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    noActions: 'Er zijn geen acties voor deze fase.',
    checkboxTooltip: 'Automatisch uitvoeren bij faseovergang',
    titleTooltip: '{{noun}} bewerken of direct {{verb}}',
    lockTooltip:
      'Instelling opgeslagen. Actie kan niet door regels worden beïnvloed. Klik om vrij te geven.',
    allocation: {
      noun: 'Toewijziging',
      verb: 'Wijzigen',
      labels: {
        department: 'Afdeling',
        role: 'Rol',
      },
      placeholders: {
        department: 'Selecteer een afdeling…',
        role: 'Selecteer een rol…',
      },
    },
    case: {
      noun: {
        deelzaak: 'Deelzaak',
        gerelateerd: 'Gerelateerde zaak',
        vervolgzaak_datum: 'Vervolgzaak',
        vervolgzaak: 'Vervolgzaak',
      },
      verb: 'Starten',
      labels: {
        relaties_eigenaar_type: 'Type aanvrager',
        relaties_eigenaar_uuid: 'Aanvrager',
        relaties_eigenaar_role: 'Type betrokkene',
        relaties_relatie_type: 'Relatietype',
        relaties_start_delay_date: 'Starten na',
        relaties_start_delay: 'Starten na X dagen',
        relaties_required: 'Afhandelen in fase',
        relaties_group_uuid: 'Afdeling',
        relaties_role_uuid: 'Rol',
        relaties_subject_role: 'Betrokkenen met deze rollen kopiëren',
        relaties_automatisch_behandelen:
          'Zaak automatisch in behandeling nemen',
        relaties_betrokkene_uuid: 'Betrokkene toevoegen',
        relaties_betrokkene_role: 'Betrokkene rol',
        relaties_betrokkene_prefix: 'Betrokkene magicstring',
        relaties_betrokkene_authorized: 'Betrokkene machtigen voor zaak',
        relaties_betrokkene_notify: 'Bevestingsmail voor machtiging versturen',
        copy: 'Kopiëren',
      },
      values: {
        relaties_eigenaar_type: {
          aanvrager: 'Aanvrager van de huidige zaak',
          ontvanger: 'Ontvanger van de huidige zaak',
          behandelaar: 'Behandelaar van de huidige zaak',
          betrokkene: 'Een betrokkene van de huidige zaak',
          anders: 'Anders',
        },
        relaties_relatie_type: {
          vervolgzaak_datum: 'Vervolgzaak (datum)',
          vervolgzaak: 'Vervolgzaak (periode)',
          deelzaak: 'Deelzaak',
          gerelateerd: 'Gerelateerde zaak',
        },
        relaties_subject_role: {
          relaties_kopieren_kenmerken: 'Kenmerken kopiëren',
          relaties_copy_related_objects: 'Gerelateerde objecten kopiëren',
          relaties_copy_related_cases: 'Gerelateerde zaken kopiëren',
          relaties_copy_related_relations: 'Gerelateerde betrokkenen kopiëren',
        },
      },
      placeholders: {
        relaties_eigenaar_uuid: 'Selecteer een aanvrager…',
        relaties_group_uuid: 'Selecteer een afdeling…',
        relaties_role_uuid: 'Selecteer een rol…',
        relaties_betrokkene_uuid: 'Selecteer een betrokkene…',
      },
    },
    email: {
      noun: 'Bericht',
      verb: 'Versturen',
      preview: {
        open: 'Voorbeeld',
        close: 'Voorbeeld sluiten',
        labels: {
          to: 'Aan',
          cc: 'CC',
          bcc: 'BCC',
          subject: 'Onderwerp',
          body: 'Bericht',
          attachments: 'Bijlagen',
        },
      },
      labels: {
        notificaties_rcpt: 'Type ontvanger',
        notificaties_behandelaar: 'Collega',
        notificaties_email: 'E-mailadres',
        notificaties_betrokkene_role: 'Type betrokkene',
        notificaties_cc: 'CC',
        notificaties_bcc: 'BCC',
        notificaties_subject: 'Onderwerp',
        notificaties_body: 'Bericht',
        notificatie_attachments: 'Bijlagen',
      },
      placeholders: {
        notificaties_behandelaar: 'Selecteer een collega…',
        notificaties_email: 'voorbeeld@zaaksysteem.nl',
        notificaties_cc: 'voorbeeld@zaaksysteem.nl; voorbeeld@xxllnc.nl',
        notificaties_bcc: 'voorbeeld@zaaksysteem.nl; voorbeeld@xxllnc.nl',
      },
    },
    object_action: {
      noun: 'Object',
      verb: 'Aanmaken',
      values: {
        description: 'Objectmutatie nu uitvoeren?',
      },
    },
    subject: {
      noun: 'Betrokkene',
      verb: 'Toevoegen',
      labels: {
        name: 'Betrokkene',
        role: 'Rol betrokkene',
        magicstringPrefix: 'Magicstring prefix',
        authorized: 'Gemachtigd voor deze zaak',
        notify: 'Verstuur bevestiging per email',
      },
    },
    template: {
      noun: 'Sjabloon',
      verb: 'Aanmaken',
      externalWarning:
        'Let op! Dit sjabloon wordt gegenereerd met een externe documentgenerator.',
      labels: {
        filename: 'Naam',
        targetFormat: 'Opslagformaat',
        caseDocument: 'Zaakdocument',
      },
      values: {
        caseDocument: {
          none: 'Geen',
        },
        targetFormat: {
          odt: 'odt (OpenDocument Text)',
          pdf: 'pdf (PDF/A-1)',
          docx: 'docx (Office Open XML Document)',
        },
      },
    },
    orDirect: 'of direct',
  },
};
