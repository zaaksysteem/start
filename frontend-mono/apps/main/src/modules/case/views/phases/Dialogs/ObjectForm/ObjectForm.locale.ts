// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    form: {
      create: 'Aanmaken',
      update: 'Bijwerken',
      deactivate: 'Uitschakelen',
    },
    confirm: {
      deactivate: {
        title: 'Uitschakelen',
        description: 'Weet u zeker dat u dit object wilt uitschakelen?',
      },
    },
    inactiveWarning: {
      admin:
        'Het object is uitgeschakeld. Ga naar het objectbeeld om het object in te schakelen.',
      normal:
        'Het object is uitgeschakeld. Vraag een beheerder om het object in te schakelen.',
    },
  },
};
