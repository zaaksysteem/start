// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  Rule,
  hasValue,
  valueEquals,
  valueOneOf,
  isTruthy,
  showFields,
  hideFields,
  updateFields,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { ActionType } from '../Actions.types';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import {
  getRcptChoices,
  getCaseDocumentChoices,
  getTargetFormatChoices,
  getCaseRelationTypeChoices,
  getCaseRequiredPhaseChoices,
  getCopyValues,
  getCopyChoices,
  getRequestorTypeChoices,
} from './PhaseActionsDialog.library';

type GetFormDefinitionType = ({
  t,
  action,
  caseObj,
  caseType,
}: {
  t: i18next.TFunction;
  action: ActionType;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
}) => AnyFormDefinitionField[];

const getAllocationFormDefinition: GetFormDefinitionType = ({ t }) => [
  {
    name: 'group_uuid',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    label: t('allocation.labels.department'),
    required: true,
    placeholder: t('allocation.placeholders.department'),
    nestedValue: true,
  },
  {
    name: 'role_uuid',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: true,
    label: t('allocation.labels.role'),
    placeholder: t('allocation.placeholders.role'),
  },
];

const getAllocationRules = () => [
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('group_uuid', 'role_uuid', 'parentRoleUuid')),
];

const getCaseFormDefinition: GetFormDefinitionType = ({
  t,
  action,
  caseObj,
  caseType,
  phaseNumber,
}) => [
  {
    name: 'relaties_eigenaar_type',
    type: fieldTypes.SELECT,
    value: action.data.eigenaar_type || 'aanvrager',
    label: t('case.labels.relaties_eigenaar_type'),
    choices: getRequestorTypeChoices(t),
  },
  {
    name: 'relaties_eigenaar_uuid',
    type: fieldTypes.CONTACT_FINDER,
    value: { label: action.data.eigenaar_id, value: action.data.eigenaar_uuid },
    required: true,
    label: t('case.labels.relaties_eigenaar_uuid'),
    placeholder: t('case.placeholders.relaties_eigenaar_uuid'),
    config: {
      subjectTypes: ['person', 'organization'],
    },
  },
  {
    name: 'relaties_eigenaar_role',
    type: fieldTypes.SELECT,
    value: action.data.eigenaar_role || null,
    required: true,
    label: t('case.labels.relaties_eigenaar_role'),
    choices: caseObj.caseRoles,
  },
  {
    name: 'relaties_relatie_type',
    type: fieldTypes.SELECT,
    value: action.data.relatie_type,
    label: t('case.labels.relaties_relatie_type'),
    choices: getCaseRelationTypeChoices(t, caseType, phaseNumber),
    nestedValue: true,
  },
  {
    name: 'relaties_start_delay_date',
    type: fieldTypes.DATEPICKER,
    value: action.data.start_delay,
    label: t('case.labels.relaties_start_delay_date'),
  },
  {
    name: 'relaties_start_delay',
    type: fieldTypes.NUMERIC,
    value: action.data.start_delay,
    label: t('case.labels.relaties_start_delay'),
  },
  {
    name: 'relaties_required',
    type: fieldTypes.SELECT,
    value: action.data.required,
    label: t('case.labels.relaties_required'),
    choices: getCaseRequiredPhaseChoices(caseType, phaseNumber),
    nestedValue: true,
  },
  {
    name: 'relaties_group_uuid',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    label: t('case.labels.relaties_group_uuid'),
    required: true,
    placeholder: t('case.placeholders.relaties_group_uuid'),
    nestedValue: true,
  },
  {
    name: 'relaties_role_uuid',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: true,
    label: t('case.labels.relaties_role_uuid'),
    placeholder: t('case.placeholders.relaties_role_uuid'),
  },
  {
    name: 'relaties_automatisch_behandelen',
    type: fieldTypes.CHECKBOX,
    value: Boolean(action.data.automatisch_behandelen),
    label: t('case.labels.relaties_automatisch_behandelen'),
    suppressLabel: true,
  },
  {
    name: 'copy',
    type: fieldTypes.CHECKBOX_GROUP,
    value: getCopyValues(action) || [],
    label: t('case.labels.copy'),
    choices: getCopyChoices(t),
  },
  {
    name: 'relaties_subject_role',
    type: fieldTypes.SELECT,
    value: action.data.subject_role || [],
    label: t('case.labels.relaties_subject_role'),
    choices: caseObj.caseRoles,
    isMulti: true,
  },
  {
    name: 'relaties_betrokkene_uuid',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    label: t('case.labels.relaties_betrokkene_uuid'),
    placeholder: t('case.placeholders.relaties_betrokkene_uuid'),
    config: {
      subjectTypes: ['person', 'organization'],
    },
  },
  {
    name: 'relaties_betrokkene_role',
    type: fieldTypes.SELECT,
    value: action.data.betrokkene_role || null,
    label: t('case.labels.relaties_betrokkene_role'),
    choices: caseObj.caseRoles,
    nestedValue: true,
  },
  {
    name: 'relaties_betrokkene_prefix',
    type: fieldTypes.TEXT,
    label: t('case.labels.relaties_betrokkene_prefix'),
    value: action.data.betrokkene_prefix || '',
    readOnly: true,
  },
  {
    name: 'relaties_betrokkene_authorized',
    type: fieldTypes.CHECKBOX,
    value: Boolean(action.data.betrokkene_authorized),
    label: t('case.labels.relaties_betrokkene_authorized'),
    suppressLabel: true,
  },
  {
    name: 'relaties_betrokkene_notify',
    type: fieldTypes.CHECKBOX,
    value: Boolean(action.data.betrokkene_notify),
    label: t('case.labels.relaties_betrokkene_notify'),
  },
];

const getCaseRules = () => [
  new Rule()
    .when('relaties_eigenaar_type', valueEquals('anders'))
    .then(showFields(['relaties_eigenaar_uuid']))
    .else(hideFields(['relaties_eigenaar_uuid'])),
  new Rule()
    .when('relaties_eigenaar_type', valueEquals('betrokkene'))
    .then(showFields(['relaties_eigenaar_role']))
    .else(hideFields(['relaties_eigenaar_role'])),
  new Rule()
    .when('relaties_relatie_type', valueEquals('vervolgzaak_datum'))
    .then(showFields(['relaties_start_delay_date']))
    .else(hideFields(['relaties_start_delay_date'])),
  new Rule()
    .when('relaties_relatie_type', valueEquals('vervolgzaak'))
    .then(showFields(['relaties_start_delay']))
    .else(hideFields(['relaties_start_delay'])),
  new Rule()
    .when('relaties_relatie_type', valueEquals('deelzaak'))
    .then(showFields(['relaties_required']))
    .else(hideFields(['relaties_required'])),
  new Rule()
    .when(() => true)
    .then(
      transferDataAsConfig(
        'relaties_group_uuid',
        'relaties_role_uuid',
        'parentRoleUuid'
      )
    ),
  new Rule()
    .when('copy', valueOneOf(['relaties_copy_related_relations']))
    .then(showFields(['relaties_subject_role']))
    .else(hideFields(['relaties_subject_role'])),
  new Rule()
    .when('relaties_betrokkene_uuid', field => hasValue(field))
    .then(
      showFields([
        'relaties_betrokkene_role',
        'relaties_betrokkene_prefix',
        'relaties_betrokkene_authorized',
        'relaties_betrokkene_notify',
      ])
    )
    .else(
      hideFields([
        'relaties_betrokkene_role',
        'relaties_betrokkene_prefix',
        'relaties_betrokkene_authorized',
        'relaties_betrokkene_notify',
      ])
    )
    .and((fields: any) =>
      fields.map(
        updateFields(
          ['relaties_betrokkene_authorized', 'relaties_betrokkene_notify'],
          {
            value: false,
          }
        )
      )
    ),
  new Rule()
    .when('relaties_betrokkene_authorized', isTruthy)
    .then(showFields(['relaties_betrokkene_notify']))
    .else(hideFields(['relaties_betrokkene_notify'])),
  new Rule()
    .when('relaties_betrokkene_role', field => hasValue(field))
    .then(fields => {
      const roleField = fields.find(
        field => field.name === 'relaties_betrokkene_role'
      );
      // @ts-ignore
      const magicString = roleField.value.toLowerCase();

      return fields.map(
        updateFields(['relaties_betrokkene_prefix'], { value: magicString })
      );
    }),
];

/* eslint complexity: [2, 11] */
const getEmailFormDefinition: GetFormDefinitionType = ({
  t,
  action,
  caseObj,
}) => [
  {
    name: 'notificaties_rcpt',
    type: fieldTypes.SELECT,
    label: t('email.labels.notificaties_rcpt'),
    value: action.data.rcpt || 'aanvrager',
    choices: getRcptChoices(t),
    nestedValue: true,
  },
  {
    name: 'notificaties_behandelaar',
    type: fieldTypes.CONTACT_FINDER,
    value: action.data.behandelaar || null,
    required: true,
    label: t('email.labels.notificaties_behandelaar'),
    placeholder: t('email.placeholders.notificaties_behandelaar'),
    config: {
      subjectTypes: ['employee'],
    },
  },
  {
    name: 'notificaties_email',
    type: fieldTypes.EMAIL,
    label: t('email.labels.notificaties_email'),
    placeholder: t('email.placeholders.notificaties_email'),
    value: action.data.email || '',
    required: true,
  },
  {
    name: 'notificaties_betrokkene_role',
    type: fieldTypes.SELECT,
    label: t('email.labels.notificaties_betrokkene_role'),
    value: action.data.betrokkene_role || null,
    required: true,
    choices: caseObj.caseRoles,
    nestedValue: true,
  },
  {
    name: 'notificaties_cc',
    type: fieldTypes.EMAIL,
    label: t('email.labels.notificaties_cc'),
    placeholder: t('email.placeholders.notificaties_cc'),
    value: action.data.cc || '',
  },
  {
    name: 'notificaties_bcc',
    type: fieldTypes.EMAIL,
    label: t('email.labels.notificaties_bcc'),
    placeholder: t('email.placeholders.notificaties_bcc'),
    value: action.data.bcc || '',
  },
  {
    name: 'notificaties_subject',
    type: fieldTypes.TEXT,
    label: t('email.labels.notificaties_subject'),
    value: action.data.subject || '',
    required: true,
  },
  {
    name: 'notificaties_body',
    type: fieldTypes.TEXTAREA,
    label: t('email.labels.notificaties_body'),
    value: action.data.body || '',
    required: true,
    rows: 7,
  },
  ...(action.data.case_document_attachments.length
    ? [
        {
          name: 'case_document_attachments',
          type: fieldTypes.TEXTAREA,
          label: t('email.labels.notificatie_attachments'),
          value:
            action.data.case_document_attachments.map(
              ({ name, extension }: any) => `${name}${extension}`
            ) || '',
          readOnly: true,
        },
      ]
    : []),
];

const getEmailRules = () => [
  new Rule()
    .when('notificaties_rcpt', valueEquals('overig'))
    .then(showFields(['notificaties_email']))
    .else(hideFields(['notificaties_email'])),
  new Rule()
    .when('notificaties_rcpt', valueEquals('betrokkene'))
    .then(showFields(['notificaties_betrokkene_role']))
    .else(hideFields(['notificaties_betrokkene_role'])),
  new Rule()
    .when('notificaties_rcpt', valueEquals('behandelaar'))
    .then(showFields(['notificaties_behandelaar']))
    .else(hideFields(['notificaties_behandelaar'])),
];

const getObjectFormDefintiion: GetFormDefinitionType = ({ t }) => [
  {
    name: 'description',
    type: fieldTypes.TEXT,
    value: t('object_action.values.description'),
    suppressLabel: true,
    readOnly: true,
  },
];

const getSubjectFormDefinition: GetFormDefinitionType = ({ t, action }) => [
  {
    name: 'name',
    type: fieldTypes.TEXT,
    label: t('subject.labels.name'),
    value: action.data.naam,
    readOnly: true,
  },
  {
    name: 'role',
    type: fieldTypes.TEXT,
    label: t('subject.labels.role'),
    value: action.data.rol,
    readOnly: true,
  },
  {
    name: 'magicstringPrefix',
    type: fieldTypes.TEXT,
    label: t('subject.labels.magicstringPrefix'),
    value: action.data.magic_string_prefix,
    readOnly: true,
  },
  {
    name: 'authorized',
    type: fieldTypes.TEXT,
    label: t('subject.labels.authorized'),
    value: action.data.gemachtigd ? t('common:yes') : t('common:no'),
    readOnly: true,
  },
  {
    name: 'notify',
    type: fieldTypes.TEXT,
    label: t('subject.labels.notify'),
    value: action.data.gemachtigd ? t('common:yes') : t('common:no'),
    readOnly: true,
  },
];

const getTemplateFormDefinition: GetFormDefinitionType = ({
  t,
  action,
  caseType,
}) => [
  ...(action.data.interface_id
    ? [
        {
          name: 'externalWarning',
          type: fieldTypes.TEXT,
          value: t('template.externalWarning'),
        },
      ]
    : []),
  {
    name: 'filename',
    type: fieldTypes.TEXT,
    value: action.data.filename,
    label: t('template.labels.filename'),
    required: true,
  },
  {
    name: 'target_format',
    type: fieldTypes.RADIO_GROUP,
    value: action.data.target_format || 'docx',
    label: t('template.labels.targetFormat'),
    disabled: Boolean(action.data.interface_id),
    choices: getTargetFormatChoices(t),
    nestedValue: true,
  },
  {
    name: 'bibliotheek_kenmerken_id',
    type: fieldTypes.SELECT,
    value: action.data.bibliotheek_kenmerken_id || 'none',
    label: t('template.labels.caseDocument'),
    choices: getCaseDocumentChoices(t, caseType),
    nestedValue: true,
  },
];

export const getFormDefinition = {
  allocation: getAllocationFormDefinition,
  case: getCaseFormDefinition,
  email: getEmailFormDefinition,
  object_action: getObjectFormDefintiion,
  subject: getSubjectFormDefinition,
  template: getTemplateFormDefinition,
};

export const getRules = {
  allocation: getAllocationRules,
  case: getCaseRules,
  email: getEmailRules,
  object_action: () => [],
  subject: () => [],
  template: () => [],
};
