// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';

export const fetchObject = async (objectUuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid: objectUuid,
    }
  );

  const result = await request<APICaseManagement.GetCustomObjectResponseBody>(
    'GET',
    url
  );

  return result;
};

export const fetchObjectType = async (objectTypeUuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/get_custom_object_type',
    {
      uuid: objectTypeUuid,
      use: 'write',
    }
  );

  const result =
    await request<APICaseManagement.GetCustomObjectTypeResponseBody>(
      'GET',
      url
    );

  return result;
};

export const fetchPersistentObjectType = async (objectTypeUuid: string) => {
  const url =
    buildUrl<APICaseManagement.GetPersistentCustomObjectTypeRequestParams>(
      '/api/v2/cm/custom_object_type/get_persistent_custom_object_type',
      {
        uuid: objectTypeUuid,
        use: 'write',
      }
    );

  const result =
    await request<APICaseManagement.GetPersistentCustomObjectTypeResponseBody>(
      'GET',
      url
    );

  return result;
};

export const createObjectAction = async (
  objectTypeUuid: string,
  values: { [key: string]: any }
) => {
  const data: APICaseManagement.CreateCustomObjectRequestBody = {
    uuid: v4(),
    custom_object_type_uuid: objectTypeUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
  };

  return request<APICaseManagement.CreateCustomObjectResponseBody>(
    'POST',
    '/api/v2/cm/custom_object/create_custom_object',
    data
  );
};

export const updateObjectAction = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuids,
    },
  };

  return request<APICaseManagement.UpdateCustomObjectResponseBody>(
    'POST',
    '/api/v2/cm/custom_object/update_custom_object',
    data
  );
};

export const deactivateObjectAction = (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuids,
    },
    status: 'inactive',
  };

  return request<APICaseManagement.UpdateCustomObjectResponseBody>(
    'POST',
    '/api/v2/cm/custom_object/update_custom_object',
    data
  );
};
