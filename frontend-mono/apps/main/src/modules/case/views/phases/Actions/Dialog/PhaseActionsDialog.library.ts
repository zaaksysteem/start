// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { CaseTypeType } from '../../../../Case.types';
import { ActionType } from '../Actions.types';

export const getRcptChoices = (t: i18next.TFunction) =>
  ['Behandelaar', 'Aanvrager', 'Gemachtigde', 'Betrokkene', 'Overig'].map(
    label => ({ label, value: label.toLowerCase() })
  );

export const getTargetFormatChoices = (t: i18next.TFunction) =>
  ['odt', 'pdf', 'docx'].map(value => ({
    label: t(`template.values.targetFormat.${value}`),
    value,
  }));

export const getCaseDocumentChoices = (
  t: i18next.TFunction,
  caseType: CaseTypeType
) => {
  const defaultOption = {
    label: t('template.values.caseDocument.none'),
    value: 'none',
  };

  const fields = caseType.fields || [];
  const documentFields = fields.filter(field => field.type === 'file');
  const documentOptions = documentFields.map(({ label, catalogue_id }) => ({
    label,
    value: catalogue_id,
  }));

  return [defaultOption, ...documentOptions];
};

export const getCaseRelationTypeChoices = (
  t: i18next.TFunction,
  caseType: CaseTypeType,
  phaseNumber: number
) => {
  const isLastPhase = caseType.phases.length === phaseNumber;

  return [
    ...(isLastPhase ? ['vervolgzaak_datum', 'vervolgzaak'] : []),
    'deelzaak',
    'gerelateerd',
  ].map(value => ({
    label: t(`case.values.relaties_relatie_type.${value}`),
    value,
  }));
};

export const getCaseRequiredPhaseChoices = (
  caseType: CaseTypeType,
  phaseNumber: number
) =>
  caseType.phases.reduce(
    (
      acc: { label: string; value: string }[],
      phase: { phase: string },
      index: number
    ) =>
      index + 1 >= phaseNumber
        ? [...acc, { label: phase.phase, value: `${index + 1}` }]
        : acc,
    []
  );

// this is what they're called when we submit
export const copyKeys = [
  'relaties_kopieren_kenmerken',
  'relaties_copy_related_objects',
  'relaties_copy_related_cases',
  'relaties_copy_related_relations',
];

// this is what they're called in the response
const copyDataKeys = [
  'kopieren_kenmerken',
  'copy_related_objects',
  'copy_related_cases',
  'copy_subject_role',
];

// check whether the copy settings are 0 or 1 in the response
// and when 1, add it to the values
export const getCopyValues = (action: ActionType) =>
  copyDataKeys.reduce((acc: string[], value: string, index: number) => {
    return action.data[value] ? [...acc, copyKeys[index]] : acc;
  }, []);

export const getCopyChoices = (t: i18next.TFunction) =>
  copyKeys.map(value => ({
    label: t(`case.values.relaties_subject_role.${value}`),
    value,
  }));

export const getRequestorTypeChoices = (t: i18next.TFunction) =>
  ['aanvrager', 'ontvanger', 'behandelaar', 'betrokkene', 'anders'].map(
    value => ({
      label: t(`case.values.relaties_eigenaar_type.${value}`),
      value,
    })
  );
