// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';

export const fetchPhaseActions = async (caseNumber: number) => {
  const url = `/api/v0/case/${caseNumber}/actions`;

  const response = await request('GET', url);

  return response.result;
};

export const checkCall = async (
  caseNumber: number,
  data: { automatic: boolean; id: number }
) => {
  const url = `/zaak/${caseNumber}/action/update`;

  const response = await request('POST', url, data);

  return response;
};

export const untaintCall = async (caseNumber: number, data: { id: number }) => {
  const url = `/zaak/${caseNumber}/action/untaint`;

  const response = await request('POST', url, data);

  return response;
};

export const saveCall = async (
  caseNumber: number,
  actionType: string,
  data: any
) => {
  const url = `/zaak/${caseNumber}/action/data/?action_type=${actionType}`;

  const response = await request('POST', url, data);

  return response;
};
