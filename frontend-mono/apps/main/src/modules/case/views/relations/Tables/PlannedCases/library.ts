// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { PlannedCaseType } from '../../Relations.types';
import {
  fetchPlannedCases,
  addPlannedCase,
  editPlannedCase,
  removePlannedCase,
} from '../requests';

export const isOnce = (period?: string) => period === 'once';

type GetPlannedCasesType = (caseNumber: number) => Promise<PlannedCaseType[]>;

export const getPlannedCases: GetPlannedCasesType = async caseNumber => {
  const response = await fetchPlannedCases(caseNumber);
  const plannedCases = response.result.map(
    ({
      casetype,
      interval_period,
      interval_value,
      next_run,
      runs_left,
      title,
      uuid,
    }: any) => ({
      uuid,
      name: uuid,
      casetype_uuid: casetype,
      casetype_title: title,
      next_run,
      runs_left,
      interval_value,
      interval_period,
    })
  );

  return plannedCases;
};

type AddplannedCaseActionType = (
  caseUuid: string,
  values: any
) => Promise<boolean>;

export const addPlannedCaseAction: AddplannedCaseActionType = async (
  caseUuid,
  {
    casetype,
    copy_relations,
    interval_period,
    interval_value,
    repeating,
    next_run,
    runs_left,
  }
) => {
  const body = {
    case_uuid: caseUuid,
    casetype_uuid: casetype.value,
    copy_relations,
    next_run,
    job: 'CreateCase',
    type: 'scheduled_job',
    ...(repeating
      ? {
          interval_period: interval_period.value,
          interval_value,
          runs_left,
        }
      : {
          interval_period: 'once',
          interval_value: 1,
          runs_left: 1,
        }),
  };

  const result = await addPlannedCase(body);

  return Boolean(result.instance);
};

type EditPlannedCaseActionType = (
  relationUuid: string,
  values: any
) => Promise<boolean>;

export const editPlannedCaseAction: EditPlannedCaseActionType = async (
  relationUuid,
  {
    copy_relations,
    interval_period,
    interval_value,
    repeating,
    next_run,
    runs_left,
  }
) => {
  const body = {
    copy_relations,
    next_run,
    job: 'CreateCase',
    type: 'scheduled_job',
    ...(repeating
      ? {
          interval_period: interval_period.value,
          interval_value,
          runs_left,
        }
      : {
          interval_period: 'once',
          interval_value: 1,
          runs_left: 1,
        }),
  };

  const result = await editPlannedCase(relationUuid, body);

  return Boolean(result.data?.success);
};

type RemovePlannedCaseActionType = (relationUuid: string) => Promise<boolean>;

export const removePlannedCaseAction: RemovePlannedCaseActionType =
  async relationUuid => {
    const result = await removePlannedCase(relationUuid);

    return Boolean(result);
  };
