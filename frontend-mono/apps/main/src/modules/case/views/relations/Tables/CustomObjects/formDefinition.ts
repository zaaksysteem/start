// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

/* eslint complexity: [2, 9] */
export const getFormDefinition = (t: i18next.TFunction) => {
  return [
    {
      name: 'customObject',
      type: fieldTypes.OBJECT_FINDER,
      value: null,
      required: true,
      isClearable: true,
      label: t('customObjects.dialog.labels.customObject'),
      placeholder: t('customObjects.dialog.placeholders.customObject'),
    },
    {
      name: 'copy',
      type: fieldTypes.CHECKBOX,
      value: false,
      required: false,
      label: t('customObjects.dialog.labels.copy'),
      suppressLabel: true,
    },
  ];
};
