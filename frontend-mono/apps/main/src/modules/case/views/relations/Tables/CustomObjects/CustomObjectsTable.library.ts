// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import {
  fetchCustomObject,
  fetchCustomObjects,
  relateDocument,
  saveCaseValues,
} from '../requests';

const getCustomFieldNames = (
  relationships: any,
  caseUuid: string,
  caseType: CaseTypeType
): string[] => {
  const caseRelationships = relationships.cases;

  if (!caseRelationships) return [];

  const customFields = caseType.phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.customFields],
    []
  );

  const relationshipToThisCase = caseRelationships.filter(
    (caseRelation: any) => caseRelation.data.id === caseUuid
  );

  const customFieldNames = relationshipToThisCase.map((caseRelation: any) => {
    const fieldUuid = caseRelation?.meta?.source_custom_field_uuid;

    if (!fieldUuid) return;

    const customField = customFields.find(
      (field: any) => field.uuid === fieldUuid
    );

    return customField?.name;
  });

  return customFieldNames;
};

export const getCustomObjects = async (
  caseUuid: string,
  caseType: CaseTypeType
) => {
  const response = await fetchCustomObjects(caseUuid);
  const customObjects = response.data.map(
    ({
      attributes: { name, title, status, version_independent_uuid },
      relationships,
    }) => ({
      uuid: version_independent_uuid as string,
      // this is not a mistake. An object is based on an object type
      // title is the name of the specific object
      // name is the name of the object type
      name: title,
      type: name,
      active: status === 'active',
      customFieldNames: getCustomFieldNames(relationships, caseUuid, caseType),
    })
  );

  return customObjects;
};

type FieldType = { type: string; value: any };
type DocumentType = { label: string; value: string; key: string };
type ObjectFieldsType = { [key: string]: FieldType };

const copyAttributeValuesToCase = async (
  caseObj: CaseObjType,
  caseType: CaseTypeType,
  caseTypeStrings: string[],
  objectFields: ObjectFieldsType
) => {
  const values = Object.entries(objectFields).reduce((acc, [key, field]) => {
    if (
      field &&
      field.type === 'relationship' &&
      caseTypeStrings.includes(key)
    ) {
      const fieldDefinitions = caseType.fields;
      const fieldDefinition = fieldDefinitions?.find(
        field => field.magic_string === key
      );
      const relationshipType = fieldDefinition.properties.relationship_type;

      // v2 doesn't return relationship.custom_object values in the way v0 expects
      if (relationshipType === 'custom_object') {
        if (!fieldDefinition.multiple_values) {
          return { ...acc, [key]: field.value[0] };
        } else {
          return {
            ...acc,
            [key]: {
              specifics: null,
              type: 'relationship',
              value: field.value,
            },
          };
        }
      } else {
        return { ...acc, [key]: field.value };
      }
    } else if (
      field &&
      field.type !== 'document' &&
      caseTypeStrings.includes(key)
    ) {
      return { ...acc, [key]: field.value };
    } else {
      return acc;
    }
  }, {});

  await saveCaseValues(caseObj.number, values);
};

/* eslint complexity: [2, 14]  */
const copyDocumentValuesToCase = async (
  caseObj: CaseObjType,
  caseTypeStrings: string[],
  objectFields: ObjectFieldsType
) => {
  const documentFields = Object.entries(objectFields).reduce(
    (acc, [key, field]) => {
      if (field && field.type === 'document' && caseTypeStrings.includes(key)) {
        const documents = (field.value || []).map((document: any) => ({
          ...document,
          key,
        }));

        return [...acc, ...documents];
      } else {
        return acc;
      }
    },
    [] as DocumentType[]
  );

  const promises = documentFields.map(async field =>
    relateDocument({
      case_uuid: caseObj.uuid,
      file_uuid: field.value,
      skip_intake: true,
      magic_string: [field.key],
    })
  );

  await Promise.all(promises);
};

export const copyValuesToCase = async (
  caseObj: CaseObjType,
  caseType: CaseTypeType,
  objectUuid: string
) => {
  const object = await fetchCustomObject(objectUuid);
  const objectFields = object.data.attributes.custom_fields as ObjectFieldsType;

  // only store stuff in attributes that exist in the case
  const caseTypeStrings = caseType.fields?.reduce(
    (acc: any, { magic_string }: any) =>
      magic_string ? [...acc, magic_string] : acc,
    []
  ) as string[];

  // save attribute values in v0
  await copyAttributeValuesToCase(
    caseObj,
    caseType,
    caseTypeStrings,
    objectFields
  );

  // save documents by creating copies
  await copyDocumentValuesToCase(caseObj, caseTypeStrings, objectFields);
};
