// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { getFormDefinition } from './formDefinition';

type DialogPropsType = {
  isOpen: boolean;
  close: () => void;
  relate: (uuid: string) => Promise<void>;
};

const Dialog: React.ComponentType<DialogPropsType> = ({
  isOpen,
  close,
  relate,
}) => {
  const [t] = useTranslation('caseRelations');

  return (
    <FormDialog
      formDefinition={getFormDefinition(t as any)}
      title={t(`customObjects.dialog.title`)}
      icon={iconNames.work}
      onClose={close}
      scope="relate-custom-object"
      open={isOpen}
      onSubmit={(values: any) => relate(values)}
      saveLabel={t(`customObjects.dialog.submit`)}
      minHeight={100}
    />
  );
};

export default Dialog;
