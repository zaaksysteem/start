// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import React from 'react';
import {
  useApplyLabelsMutation,
  useCaseDocumentLabelsQuery,
  useRemoveLabelsMutation,
} from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';
import { dragGhost } from './dragGhost';

export const LabelsContainer = (props: {
  setDragSource: (src: any) => any;
}) => {
  const { data: labels } = useCaseDocumentLabelsQuery();
  const { mutate: removeLabels } = useRemoveLabelsMutation();
  const { mutate: applyLabels } = useApplyLabelsMutation();
  const [selectedRows, selections] = useSelectedAcceptedRows();

  return (
    <Box className="labelsContainer">
      {labels &&
        labels.map(label => {
          const allSelectedHaveTheLabel = selectedRows.every(
            row =>
              row?.appliedLabelIds && row?.appliedLabelIds.includes(label.id)
          );

          return (
            <Chip
              key={label.uuid}
              draggable={true}
              sx={{ height: '26px' }}
              label={label.label}
              onDragStart={ev => {
                props.setDragSource(['label', label.uuid]);
                ev.dataTransfer.dropEffect = 'move';

                if (ev.target instanceof HTMLElement) {
                  dragGhost.innerHTML = '';
                  dragGhost.appendChild(ev.target.cloneNode(true));
                  ev.dataTransfer.setDragImage(dragGhost, 30, 11);
                  document.body.appendChild(dragGhost);
                }
              }}
              onClick={() => {
                if (selections.hasFiles) {
                  const payload = {
                    document_label_uuids: [label.uuid],
                    document_uuids: selectedRows
                      .filter(row => row.type === 'file')
                      .map(row => row?.uuid || ''),
                  };
                  (allSelectedHaveTheLabel ? removeLabels : applyLabels)(
                    payload
                  );
                }
              }}
            />
          );
        })}
    </Box>
  );
};
