// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { ChipButton } from '../ChipButton';
import { useUploadFilesMutation } from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';

/* eslint complexity: [2, 9] */
export const ReplaceDocument = () => {
  const [selectedRows, selections] = useSelectedAcceptedRows();
  const selectedRow = selectedRows[0];
  const [t] = useTranslation('case');
  const inputRef = React.createRef<HTMLInputElement>();
  const { mutateAsync: uploadFiles } = useUploadFilesMutation();

  return (
    <>
      {selections.single && selections.onlyFiles && (
        <ChipButton
          icon="cached"
          title={t('documentTableActions.replace')}
          onClick={() => {
            inputRef.current?.click();
          }}
        />
      )}
      <input
        ref={inputRef}
        style={{ display: 'none' }}
        multiple={false}
        type="file"
        onChange={() =>
          uploadFiles({
            files: inputRef.current?.files,
            replaces: selectedRow.uuid,
          })
        }
      />
    </>
  );
};
