// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { ChipButton } from '../ChipButton';
import { relocatingView } from '../Documents.signals';
import { useCaseObjectQuery } from '../../../Case.library';
import { useSelectedAcceptedRows } from '../Documents.library';

export const SendMessage = () => {
  const [t] = useTranslation('case');
  const { data } = useCaseObjectQuery();
  const [, selections, selectedRowIds] = useSelectedAcceptedRows();

  return (
    !selections.onlyFolders && (
      <ChipButton
        onClick={() => {
          if (data) {
            window.open(
              `/intern/zaak/${data.number}/communicatie/?view=message&documents=${selectedRowIds.value}`,
              '_top'
            );
          }
        }}
        enabled={!relocatingView.value && data?.status !== 'resolved'}
        icon="mail_outline"
        title={t('documentTableActions.message')}
      />
    )
  );
};
