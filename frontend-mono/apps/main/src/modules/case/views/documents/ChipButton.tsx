// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as React from 'react';
import Icon, { IconNameType } from '@mintlab/ui/App/Material/Icon';
import { Chip } from '@mui/material';

export const ChipButton = ({
  title,
  icon,
  href,
  enabled = true,
  onClick,
}: {
  title: string;
  icon: IconNameType;
  enabled?: boolean;
  href?: string;
  onClick?: (ev: any) => void;
}) => {
  return (
    //@ts-ignore
    <Chip
      sx={{
        height: '28px',
        padding: '0px 0px 0px 7px',
        '& .MuiChip-label': {
          paddingLeft: '7px',
        },
      }}
      label={title}
      color="default"
      onClick={
        enabled
          ? href
            ? () => {
                window.location.href = href;
              }
            : onClick
          : undefined
      }
      enabled={enabled}
      icon={
        <Icon sx={{ opacity: 0.7 }} size="extraSmall">
          {icon}
        </Icon>
      }
    />
  );
};
