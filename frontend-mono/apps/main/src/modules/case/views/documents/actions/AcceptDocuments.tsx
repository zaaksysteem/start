// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { actionButtonHoverTweak } from '../actionButtonHoverTweak';
import { TooltipButton } from '../TooltipButton';
import {
  useAcceptDocumentsMutation,
  useDocumentsQuery,
} from '../Documents.requests';
import { useSelectedPendingRows } from '../Documents.library';
import { ChipButton } from '../ChipButton';

export const AcceptDocument = (props: { uuid: string; title: string }) => {
  const [t] = useTranslation('case');
  const { data } = useDocumentsQuery();
  const { mutate: acceptDocuments } = useAcceptDocumentsMutation();
  const uuid = props.uuid;

  return (
    <TooltipButton
      onClick={() =>
        acceptDocuments([
          {
            uuid,
            as_version_of: data?.find(
              target => target.accepted && target.title === props.title
            )?.uuid,
          },
        ])
      }
      name="documentsAccept"
      icon="check_circle"
      buttonSx={actionButtonHoverTweak}
      title={t('documentTableActions.accept')}
    />
  );
};

export const BulkAccept = () => {
  const [t] = useTranslation('case');
  const { mutate: acceptDocuments } = useAcceptDocumentsMutation();
  const { data } = useDocumentsQuery();
  const [selectedRows] = useSelectedPendingRows();

  return (
    <ChipButton
      onClick={() =>
        acceptDocuments(
          selectedRows.map(row => ({
            duplicateOf: data?.find(
              target => target.accepted && target.title === row.title
            )?.uuid,
            uuid: row.uuid,
          }))
        )
      }
      icon="check_circle"
      title={t('documentTableActions.accept')}
    />
  );
};
