// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { Link } from 'react-router-dom';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import DropdownMenu from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import MenuItem from '@mui/material/MenuItem/MenuItem';
import { ChipButton } from '../ChipButton';
import { useCaseObjectQuery } from '../../../Case.library';
import { useSelectedAcceptedRows } from '../Documents.library';
import { DocumentsNodeType } from '../Documents.types';

export const useEditActions = (
  selectedRows: DocumentsNodeType[],
  selectedRowIds: any,
  onClick?: () => void
) => {
  const [t] = useTranslation('case');

  const session = useSession();
  const xentialActive = session.active_interfaces.includes('xential');
  const zohoActive = session.configurable.edit_document_online;
  const msOnlineActive = session.configurable.edit_document_msonline;

  const caseObject = useCaseObjectQuery();

  const ext = selectedRows[0].extension?.toLowerCase();

  return selectedRows[0]
    ? [
        {
          id: 'local',
          show: Boolean(selectedRows[0].locallyEditable),
          onClick: () => {
            fetch(`/file/${selectedRowIds.value[0]}/generate_edit_invitation`)
              .then(res => res.json())
              .then(res => window.parent?.open(res.result));
          },
        },
        {
          id: 'odf',
          show: ext === 'odt',
          href:
            '/external-components/exposed/webodf?' +
            new URLSearchParams({
              caseId: caseObject.data?.number.toString() || '',
              fileId: selectedRowIds.value[0],
              title: selectedRows[0].title,
              uuid: selectedRows[0].uuid,
              caseUuid: caseObject.data?.uuid || '',
              documents: 'v2',
              unsafeinlinestyles: '1',
            }),
          target: '_parent',
        },
        {
          id: 'xential',
          onClick: () => {
            fetch(
              `/file/edit_file?file_id=${selectedRowIds.value[0]}&interface_module=xential`
            )
              .then(res => res.json())
              .then(res => window.parent?.open(res.redirect_url));
          },
          show: xentialActive && ['odt', 'doc', 'docx'].includes(ext || ''),
        },
        {
          id: 'zoho',
          href:
            '/api/v2/document/edit_document_online?id=' + selectedRows[0].uuid,
          show: zohoActive && ['odt', 'doc', 'docx'].includes(ext || ''),
          target: '_parent',
        },
        {
          id: 'word',
          show:
            msOnlineActive &&
            [
              'odt',
              'doc',
              'docx',
              'dotx',
              'wopitest',
              'wopitestx',
              'xls',
              'xlsx',
              'ppt',
              'pptx',
            ].includes(ext || ''),
          href:
            '/external-components/index.html?component=msedit&documentUuid=' +
            selectedRows[0].uuid,
          target: '_parent',
        },
      ].map(item => {
        const text = t('documentTableActions.editTypes.' + item.id);
        return item.show ? (
          <MenuItem
            key={item.id}
            onClick={() => {
              item && item.onClick && item.onClick();

              if (onClick) {
                onClick();
              }
            }}
          >
            {item.href ? (
              <Link
                style={{
                  width: '100%',
                  height: '100%',
                  textDecoration: 'auto',
                  color: 'inherit',
                }}
                to={item.href}
                target="_blank"
              >
                {text}
              </Link>
            ) : (
              text
            )}
          </MenuItem>
        ) : null;
      })
    : null;
};

/* eslint complexity: [2, 14] */
export const EditDocument = () => {
  const [t] = useTranslation('case');

  const [selectedRows, selections, selectedRowIds] = useSelectedAcceptedRows();

  const editActions = useEditActions(selectedRows, selectedRowIds);

  return (
    selections.single &&
    selections.onlyFiles && (
      <DropdownMenu
        transformOrigin={{
          vertical: -10,
          horizontal: 'center',
        }}
        trigger={
          <ChipButton icon="edit" title={t('documentTableActions.edit')} />
        }
      >
        {editActions}
      </DropdownMenu>
    )
  );
};

type EditSingleDocumentPropsType = { row: any; onClick: () => void };

export const EditSingleDocument: React.ComponentType<
  EditSingleDocumentPropsType
> = ({ row, onClick }) => {
  const [t] = useTranslation('case');

  const editActions = useEditActions([row], { value: [row.id] }, onClick);

  return (
    <DropdownMenu
      trigger={<MenuItem>{t('documentTableActions.edit')}</MenuItem>}
    >
      {editActions}
    </DropdownMenu>
  );
};
