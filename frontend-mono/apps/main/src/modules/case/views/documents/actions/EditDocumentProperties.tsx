// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Rule,
  hideFields,
  setLabel,
  showFields,
  valueEquals,
  valueOneOf,
} from '@zaaksysteem/common/src/components/form/rules';
import Button from '@mintlab/ui/App/Material/Button/Button';
import { Box } from '@mui/material';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  useFileIntegrityQuery,
  useGetCategories,
  useUpdateDocumentInfoMutation,
} from '../Documents.requests';
import { TooltipButton } from '../TooltipButton';
import { ChipButton } from '../ChipButton';
import { actionButtonHoverTweak } from '../actionButtonHoverTweak';
import { DocumentsNodeType, SelectionsHook } from '../Documents.types';
import { useCaseObjectQuery } from '../../../Case.library';
import { editFormDefinition } from './editInfo.formDefinition';

/* eslint complexity: [2, 20] */
export const PropertiesDialog = (props: {
  onClose: () => void;
  row?: DocumentsNodeType;
  selectedRows?: DocumentsNodeType[];
  overwrites?: string;
}) => {
  const [t] = useTranslation('case');
  const sr0 = props.selectedRows && props.selectedRows[0];
  const file = props.row || sr0;
  const headingStyle = { marginBottom: '7px', fontWeight: '600' };
  const [activatedEditing, setActivatedEditing] = React.useState(false);
  const caseObject = useCaseObjectQuery();
  const { data: documentCategories, isLoading } = useGetCategories();
  const { data: fileIntegrity } = useFileIntegrityQuery(file?.id || '');
  const { mutateAsync: updateDocumentInfo, isLoading: saving } =
    useUpdateDocumentInfoMutation(file?.uuid || '', props.onClose);

  if (!file) {
    return;
  }

  const pendingNewVersionFile = Boolean(file.overwrites && !file.accepted);
  const pendingNewVersionFileAndEditingInactive =
    pendingNewVersionFile && !activatedEditing;
  const canEdit = caseObject.data?.canEdit;

  return (
    <>
      <FormDialog
        saving={saving}
        disableSubmit={pendingNewVersionFile ? !activatedEditing : !canEdit}
        initializing={isLoading}
        onSubmit={data => {
          updateDocumentInfo({
            ...data,
            name: (
              document.querySelector('input[name="name"]') as HTMLInputElement
            ).value,
          }).then(props.onClose);
        }}
        icon="info"
        title={t('documentTableActions.properties')}
        formDefinitionT={t}
        rules={[
          new Rule()
            .when('origin', valueEquals('Inkomend'))
            .then(showFields(['originDate']))
            .then(
              setLabel(
                ['originDate'],
                t('documentTabDialogs.moreInfo.incomingDate')
              )
            ),
          new Rule()
            .when('origin', valueEquals('Uitgaand'))
            .then(showFields(['originDate']))
            .then(
              setLabel(
                ['originDate'],
                t('documentTabDialogs.moreInfo.outgoingDate')
              )
            ),
          new Rule()
            .when(
              'origin',
              valueOneOf(['Intern', null, undefined, ''] as any[])
            )
            .then(hideFields(['originDate'])),
        ]}
        onClose={props.onClose}
        formDefinition={editFormDefinition(
          file,
          documentCategories?.result || [],
          !canEdit || pendingNewVersionFileAndEditingInactive
        )}
        prependContents={
          pendingNewVersionFileAndEditingInactive && canEdit ? (
            <Button
              id="activateEditing"
              name="activateEditing"
              sx={{ width: '100%', margin: '5px 0 30px 0' }}
              onClick={() => {
                setActivatedEditing(true);
                setTimeout(() => {
                  const nameInput = document.querySelector(
                    'input[name="name"]'
                  ) as HTMLInputElement;
                  nameInput.value = nameInput.value + ' (1)';
                }, 400);
              }}
            >
              {t('documentTabDialogs.moreInfo.enableEdit')}
            </Button>
          ) : null
        }
        appendContents={
          <>
            <Button
              id="extraInfoButton"
              sx={{ width: '100%' }}
              variant="default"
              onClick={() => {
                // We want to avoid autoscroll to the top of the modal
                //@ts-ignore
                document.getElementById('extraInfoBox').style.display = 'flex';
                //@ts-ignore
                document.getElementById('extraInfoButton').style.display =
                  'none';
              }}
              name="seeExtraInfo"
            >
              {t('documentTabDialogs.moreInfo.expand')}
            </Button>
            <Box
              id="extraInfoBox"
              sx={{
                marginTop: '20px',
                display: 'none',
                gap: '20px',
                flexDirection: 'column',
              }}
            >
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.name')}
                </Box>
                <span>{file.title}</span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.documentNumber')}
                </Box>
                <span>{file.id}</span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.mimetype')}
                </Box>
                <span>{file.mimetype}</span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.size')}
                </Box>
                <span>
                  {file.size && file.size > 10 ** 6
                    ? (Math.round(file.size / 10 ** 6) || 1) + ' MB'
                    : file.size
                      ? (Math.round(file.size / 10 ** 6) || 1) + ' KB'
                      : '1 KB'}
                </span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.version')}
                </Box>
                <span>{file.version}</span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.md5')}
                </Box>
                <span>{file.md5}</span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.integrity')}
                </Box>
                <span>
                  {fileIntegrity
                    ? fileIntegrity.result
                      ? t('documentTabDialogs.moreInfo.verified')
                      : t('documentTabDialogs.moreInfo.unverified')
                    : t('documentTabDialogs.moreInfo.loading')}
                </span>
              </Box>
              <Box>
                <Box sx={headingStyle}>
                  {t('documentTabDialogs.moreInfo.status')}
                </Box>
                <span>{t('documentTabDialogs.moreInfo.original')}</span>
              </Box>
            </Box>
          </>
        }
      />
    </>
  );
};

export const EditDocumentProperties = (props: {
  row?: DocumentsNodeType;
  useSelectedRows: SelectionsHook;
}) => {
  const [t] = useTranslation('case');
  const [dialogOpened, setDialogOpened] = React.useState(false);
  const [selectedRows, selections] = props.useSelectedRows();

  return (
    <>
      {props.row ? (
        <TooltipButton
          buttonSx={actionButtonHoverTweak}
          onClick={() => setDialogOpened(true)}
          name="documentsProperties"
          icon="info"
          title={t('documentTableActions.properties')}
        />
      ) : (
        selections.single &&
        selections.onlyFiles && (
          <ChipButton
            enabled={true}
            icon="info"
            title={t('documentTableActions.properties')}
            onClick={() => setDialogOpened(true)}
          />
        )
      )}

      {dialogOpened && (
        <PropertiesDialog
          selectedRows={selectedRows}
          onClose={() => setDialogOpened(false)}
          row={props.row}
        />
      )}
    </>
  );
};
