// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';

export const useDocumentVersionsStyle = () => {
  const {
    mintlab: { greyscale },
    typography,
  } = useTheme<Theme>();

  return {
    panel: {
      padding: '0',
      border: '1px solid rgba(0, 0, 0, .125)',
      boxShadow: 'none',
      fontFamily: typography.fontFamily,
      '&:not(:last-child)': {
        borderBottom: 0,
      },
      '&:before': {
        display: 'none',
      },
      '&$expanded': {
        margin: 'auto',
      },
    },
    expandedPanel: {
      margin: '0',
    },
    summaryContent: {
      margin: '20px 0',
      '&$expanded': {
        margin: '12px 0',
      },
      '&>*:last-child': {
        padding: '0',
      },
    },
    summaryExpanded: {},
    summaryExpandIcon: {
      left: '8px',
      right: 'auto',
    },
    summaryWrapper: {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      padding: '0 10px 0 0 ',
    },
    avatar: { backgroundColor: greyscale.evenDarker },
    dateTime: {
      flexGrow: '1',
      display: 'flex',
    },
    versionTitle: {
      fontWeight: typography.fontWeightMedium,
      marginLeft: '20px',
      marginRight: 'auto',
      wordBreak: 'break-word',
    },
    detailsActive: {
      paddingTop: '24px',
    },
    detailsCard: {
      marginBottom: '10px',
      background: greyscale.dark,
      width: '100%',
      padding: '10px',

      '&>div:last-child': {
        paddingBottom: '0px',
      },

      '&& .MuiCardContent-root': {
        padding: '0 16px',
        '&>div:not(:last-child)': {
          borderBottom: `1px solid ${greyscale.darker}`,
        },
      },
    },
    detailWrapper: {
      display: 'flex',
      padding: '10px 0px',
    },
    detailTitle: {
      minWidth: '100px',
    },
    detailValue: {
      fontWeight: typography.fontWeightMedium,
    },
    logDescription: {
      fontFamily: 'monospace',
    },
    logEntry: {
      display: 'flex',
    },
    logDate: {
      minWidth: '155px',
      display: 'flex',
      marginLeft: 'auto',
      opacity: 0.6,
      justifyContent: 'end',
      alignItems: 'center',
    },
  };
};
