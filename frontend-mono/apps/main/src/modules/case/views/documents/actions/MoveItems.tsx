// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { ChipButton } from '../ChipButton';
import { currentMode } from '../Documents.signals';

export const MoveItems = () => {
  const [t] = useTranslation('case');

  return (
    <ChipButton
      icon="folder_move"
      title={t('documentTableActions.move')}
      onClick={() => (currentMode.value = 'relocating')}
    />
  );
};
