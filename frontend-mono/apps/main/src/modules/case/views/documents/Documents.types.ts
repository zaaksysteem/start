// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Signal } from '@preact/signals-react';

export type DocumentsNodeType = {
  hierarchy: { id: string; title: string }[];
  id: string;
  uuid: string;
  title: string;
  type: 'file' | 'folder';

  documentCategory?: string;
  documentStatus?: string;
  appearance?: string;
  structure?: string;

  locked?: boolean;
  description?: string;
  trustLevel?: string;
  overwrites?: string;
  name?: string;
  inTrash?: boolean;
  children?: string[];
  accepted?: boolean;
  archiveable?: boolean;
  origin?: string;
  source?: string;
  originDate?: string;
  _originDate?: Date | null;
  pronomFormat?: string;
  extension?: string;
  downloadUrl?: string;
  previewUrl?: string;
  createdAt?: string;
  _createdAt?: Date;
  createdBy?: string;
  signable?: boolean;
  version?: number;
  appliedLabelIds?: number[];
  md5?: string;
  size?: number;
  mimetype?: string;
  published?: { pip: boolean; website: boolean };
  hasLockedFiles?: boolean;
  locallyEditable?: boolean;
};

export type DocumentLabelType = { label: string; uuid: string; id: number };

export type DocumentsTreeType = DocumentsNodeType[];
export type DragSource =
  | ['', string]
  | ['system']
  | ['tableItem']
  | ['label', string];

export type SelectionsHook = () => [
  DocumentsNodeType[],
  {
    exist: boolean;
    single: boolean;
    multiple: boolean;
    hasFolders: boolean;
    hasFiles: boolean;
    onlyFiles: boolean;
    onlyFolders: boolean;
    hasNonEmptyFolders: boolean;
  },
  Signal<string[]>,
];

export type ActiveFiltering = {
  target:
    | 'title'
    | 'createdAt'
    | 'origin'
    | 'originDate'
    | 'extension'
    | 'author';
  desc: boolean;
  untouched?: boolean;
};

export type DocumentUploadOptions = {
  files?: FileList | File[] | null;
  items?: DataTransferItem[];
  replaces?: string;
  targetUuid?: string;
};
