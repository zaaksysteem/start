// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { TooltipButton } from '../TooltipButton';
import { rejectFile, relocatingView } from '../Documents.signals';
import { actionButtonHoverTweak } from '../actionButtonHoverTweak';
import {
  useDeletePermanentMutation,
  useMoveDocumentToIntakeMutation,
} from '../Documents.requests';
import { DocumentsNodeType } from '../Documents.types';
import { ChipButton } from '../ChipButton';
import { useSelectedPendingRows } from '../Documents.library';

/* eslint complexity: [2, 20] */
export const RejectDocument = (props: { file?: DocumentsNodeType }) => {
  const [t] = useTranslation('case');
  const [selectedPendingRows] = useSelectedPendingRows();

  return (
    <>
      {props.file ? (
        <TooltipButton
          onClick={() => {
            rejectFile.value = {
              title: props.file?.title,
              id: props.file?.id,
            };
          }}
          enabled={!relocatingView.value}
          name="documentsReject"
          icon="cancel"
          buttonSx={actionButtonHoverTweak}
          title={t('documentTableActions.reject')}
        />
      ) : (
        <ChipButton
          onClick={() => {
            rejectFile.value = {
              title: selectedPendingRows[0].title,
              id: selectedPendingRows[0].id,
            };
          }}
          enabled={!relocatingView.value}
          icon="cancel"
          title={t('documentTableActions.reject')}
        />
      )}
    </>
  );
};

export const RejectDocumentDialog = () => {
  const [t] = useTranslation('case');
  const onClose = () => {
    rejectFile.value = {};
  };

  const { mutateAsync: deleteDocuments, isLoading: deleting } =
    useDeletePermanentMutation();
  const { mutateAsync: moveToIntake, isLoading: updating } =
    useMoveDocumentToIntakeMutation();

  const choices = ['Intake', 'Delete'].map(value => ({
    label: `documentTabDialogs.reject.action${value}`,
    value,
  }));

  return (
    <>
      {rejectFile.value.id && (
        <FormDialog
          saving={deleting || updating}
          onSubmit={data => {
            return (
              data.action === 'Intake'
                ? moveToIntake({
                    file_id: Number(rejectFile.value?.id),
                    rejection_reason: data.note,
                  })
                : deleteDocuments({
                    files: [rejectFile.value.id || ''],
                    rest: {
                      accepted: false,
                      reject_to_queue: 0,
                      rejection_reason: data.note,
                    },
                  })
            ).then(onClose);
          }}
          icon="cancel"
          title={t('documentTabDialogs.reject.title')}
          formDefinitionT={t}
          onClose={onClose}
          formDefinition={[
            {
              choices,
              format: 'text',
              value: choices[0].value,
              name: 'action',
              type: fieldTypes.RADIO_GROUP,
              label: t('documentTabDialogs.reject.action', {
                fileName: rejectFile.value?.title,
              }),
            },
            {
              format: 'text',
              value: '',
              name: 'note',
              label: 'documentTabDialogs.reject.note',
              type: fieldTypes.TEXT,
            },
          ]}
        />
      )}
    </>
  );
};
