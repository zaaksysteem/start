// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { ChipButton } from '../ChipButton';
import { useCaseObjectQuery } from '../../../Case.library';
import { requestDocumentZip } from '../Documents.requests';
import { DocumentsNodeType, SelectionsHook } from '../Documents.types';

export const DocumentsDownload = (props: {
  useSelectedRows: SelectionsHook;
}) => {
  const [selectedRows, selections, selectedRowIds] = props.useSelectedRows();
  const caseObject = useCaseObjectQuery();
  const [t] = useTranslation('case');

  return !selections.onlyFolders ? (
    <ChipButton
      icon="download"
      title={
        selections.single
          ? t('documentTableActions.download')
          : t('documentTableActions.downloadZip')
      }
      onClick={() => {
        if (selections.single) {
          window.open(
            `/zaak/${caseObject?.data?.number}/document/${selectedRowIds.value[0]}/download`
          );
        } else {
          const selectedFolders = selectedRows.filter(
            row => row.type === 'folder'
          );

          const folders = [] as DocumentsNodeType[];

          selectedFolders.forEach(folder1 => {
            !selectedFolders.find(folder2 =>
              folder2.children?.includes(folder1.id)
            ) && folders.push(folder1);
          });

          const files = selectedRows.filter(row => row.type === 'file');

          requestDocumentZip(
            caseObject?.data?.uuid || '',
            folders.map(folder => folder.uuid),
            files.map(file => file.uuid)
          );
        }
      }}
    />
  ) : null;
};
