// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { IconNameType, IconSizeType } from '@mintlab/ui/App/Material/Icon';

export const TooltipButton = ({
  title,
  name,
  icon,
  href,
  enabled = true,
  sx = {},
  buttonSx = {},
  target,
  onClick,
  iconSize,
}: {
  title: string;
  name: string;
  icon: IconNameType;
  iconSize?: IconSizeType;
  enabled?: boolean;
  buttonSx?: any;
  href?: string;
  sx?: any;
  target?: any;
  onClick?: (ev: any) => void;
}) => {
  return (
    <Tooltip sx={{ width: 'inherit', ...sx }} title={title}>
      <Button
        sx={buttonSx}
        onClick={onClick}
        disabled={!enabled}
        iconSize={iconSize || 'small'}
        name={name}
        href={href}
        icon={icon}
        target={target}
      />
    </Tooltip>
  );
};
