// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { ChipButton } from '../ChipButton';
import { useMaskerModuleQuery } from '../Documents.requests';
import { useSelectedAcceptedRows } from '../Documents.library';

export const Anonymize = () => {
  const session = useSession();
  const datamaskActive = session.active_interfaces.includes(
    'koppelapp_document_masker'
  );
  const { data: maskerModuleData } = useMaskerModuleQuery(datamaskActive);
  const [selectedRows] = useSelectedAcceptedRows();

  return datamaskActive ? (
    <ChipButton
      icon="security"
      title={maskerModuleData?.label || ''}
      onClick={() => {
        const encoder = new TextEncoder();

        const info = btoa(
          String.fromCharCode.apply(
            null,
            //@ts-ignore
            encoder.encode(
              JSON.stringify({
                user_id: session.logged_in_user.uuid,
                user_name: session.logged_in_user.display_name,
                docs: selectedRows.map(row => ({
                  id: Number(row.id),
                  uuid: row.uuid,
                })),
              })
            )
          )
        );

        window
          ?.open(maskerModuleData?.endpoint + '?i=' + info, '_blank')
          ?.focus();
      }}
    />
  ) : null;
};
