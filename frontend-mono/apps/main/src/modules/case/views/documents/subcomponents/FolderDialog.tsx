// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  useCreateFolderMutation,
  useRenameFolderMutation,
} from '../Documents.requests';
import { useCaseObjectQuery } from '../../../Case.library';
import { useSelectedAcceptedRows } from '../Documents.library';
import { DocumentsNodeType } from '../Documents.types';

export const FolderDialog = ({
  onClose,
  edit,
  row,
}: {
  onClose: () => void;
  edit: boolean;
  row?: DocumentsNodeType;
}) => {
  const [t] = useTranslation('case');
  const [selectedRows] = useSelectedAcceptedRows();
  const caseObject = useCaseObjectQuery();
  const { mutateAsync: createFolder } = useCreateFolderMutation();
  const { mutateAsync: renameFolder } = useRenameFolderMutation();
  const target = row || selectedRows.filter(row => row.type === 'folder')[0];

  return (
    <FormDialog
      title={t(
        edit
          ? 'documentTabDialogs.editFolder.title'
          : 'documentTabDialogs.createFolder.title'
      )}
      onClose={onClose}
      formDefinition={[
        {
          name: 'name',
          label: t('documentTabDialogs.createFolder.name'),
          type: 'text',
          value: edit ? target.title : '',
          format: '',
          autoComplete: 'off',
          required: true,
          autoFocus: true,
        },
      ]}
      //@ts-ignore
      onSubmit={({ name }) =>
        (edit
          ? renameFolder({
              name,
              directory_id: Number(target.id),
            })
          : createFolder({ name, case_id: caseObject.data?.number || 0 })
        ).then(onClose)
      }
    />
  );
};
