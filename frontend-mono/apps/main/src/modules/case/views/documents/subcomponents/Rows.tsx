// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Checkbox, Chip } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  LockOutlined,
  PriorityHigh,
  Share,
  VisibilityOff,
} from '@mui/icons-material';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { DocumentsNodeType } from '../Documents.types';
import { previewDialog, versionsDialog } from '../Documents.signals';
import {
  useCaseDocumentLabelsQuery,
  useRemoveLabelsMutation,
} from '../Documents.requests';

export const ExtensionRow = (props: { row: DocumentsNodeType }) => {
  const [t] = useTranslation('case');
  return (
    <td className="row-extension light">
      {props.row.archiveable || props.row.type === 'folder' ? (
        props.row.extension
      ) : (
        <Tooltip
          sx={{ width: 'initial', display: 'flex' }}
          title={t('documentTableHeaders.unarchiveableFile', {
            fileExtension: props.row.extension,
          })}
        >
          <PriorityHigh
            sx={{
              border: '2px solid currentColor',
              borderRadius: '100%',
              width: 'initial',
              display: 'flex',
              opacity: 0.75,
            }}
            fontSize="small"
          />
        </Tooltip>
      )}
    </td>
  );
};

export const CheckboxRow = ({
  row,
  selectedRows,
  disabled,
}: {
  row: DocumentsNodeType;
  selectedRows: DocumentsNodeType[];
  disabled: boolean;
}) => {
  const [t] = useTranslation('case');

  return (
    <td className="row-checkbox">
      {row.locked ? (
        <Tooltip
          sx={{
            display: 'flex',
            justifyContent: 'center',
            opacity: 0.8,
          }}
          title={t('documentTableHeaders.fileLocked')}
        >
          <LockOutlined />
        </Tooltip>
      ) : (
        <Checkbox
          disabled={disabled}
          indeterminate={
            row.children &&
            row.children.some(child =>
              Boolean(selectedRows.find(sr => sr.id === child))
            ) &&
            !selectedRows.find(selectedRow => selectedRow.id === row.id)
          }
          checked={Boolean(
            selectedRows.find(selectedRow => selectedRow.id === row.id)
          )}
          size="small"
          name={'select-' + row.id}
        />
      )}
    </td>
  );
};

export const VersionRow = ({ row }: { row: DocumentsNodeType }) => {
  return (row.version || 1) > 1 ? (
    <Button
      onClick={() => {
        versionsDialog.value = {
          opened: true,
          fileId: Number(row.id),
        };
      }}
      sx={{
        width: '30px',
        height: '30px',
        boxShadow: 'none',
        color: 'rgba(0, 0, 0, 0.55)',
        padding: '5px',
        margin: 0,
        minWidth: 'auto',
        background: 'rgba(0, 0, 0, 0.05)',
        borderRadius: '100%',

        '&:hover': {
          background: 'rgba(0, 0, 0, 0.15)',
        },
      }}
      name="version"
    >
      v{row.version}
    </Button>
  ) : null;
};

/* eslint complexity: [2, 20] */
export const TitleRow = ({
  row,
  onTopClick,
}: {
  row: DocumentsNodeType;
  onTopClick?: () => void;
}) => {
  const [t] = useTranslation('case');
  const { data: labels } = useCaseDocumentLabelsQuery();
  const { mutate: removeLabels } = useRemoveLabelsMutation();

  return (
    <td className="row-title" onClick={onTopClick}>
      <Box
        {...{
          ...(row.type === 'file'
            ? {
                onClick: () => {
                  window.top?.postMessage({
                    type: 'goFullscreen',
                  });

                  previewDialog.value = {
                    opened: true,
                    title: row.title,
                    contentType: 'application/pdf',
                    downloadUrl: row.downloadUrl,
                    url: row.previewUrl,
                  };
                },
                className: 'fileCell rowTitleText',
                role: 'button',
              }
            : { className: 'rowTitleText' }),
        }}
        style={{ fontSize: '1rem' }}
      >
        {row.title || row.hierarchy.at(-1)?.title}
      </Box>
      <Box className="titleIcons">
        {['Confidentieel', 'Geheim', 'Zeer geheim', 'Vertrouwelijk'].includes(
          row.trustLevel || ''
        ) && (
          <Tooltip
            sx={{ width: 'initial', display: 'flex' }}
            title={t('documentTableHeaders.elevatedTrustLevel')}
          >
            <VisibilityOff
              sx={{ width: 'initial', display: 'flex', opacity: 0.5 }}
              fontSize="small"
            />
          </Tooltip>
        )}
        {row.published && Object.values(row.published).some(val => val) && (
          <Tooltip
            sx={{ width: 'initial', display: 'flex' }}
            title={`${t('documentTableHeaders.published')} ${[
              row.published.pip ? t('documentTableHeaders.pip') : '',
              row.published.website ? t('documentTableHeaders.website') : '',
            ]
              .filter(Boolean)
              .join(', ')}`}
          >
            <Share sx={{ opacity: 0.5 }} fontSize="small" />
          </Tooltip>
        )}
      </Box>
      <Box className="titleLabels">
        {row.overwrites && !row.accepted ? (
          <Chip
            size="small"
            sx={{ margin: '1px 0 1px 8px' }}
            label={t('documentTableHeaders.newVersion')}
          />
        ) : null}
        {row.appliedLabelIds &&
          labels &&
          row.appliedLabelIds.map(id => (
            <Chip
              size="small"
              sx={{ margin: '1px 0 1px 8px' }}
              key={id}
              label={labels.find(label => label.id === id)?.label}
              onDelete={
                row.locked
                  ? undefined
                  : () =>
                      labels &&
                      removeLabels({
                        document_uuids: [row.uuid || ''],
                        document_label_uuids: [
                          labels.find(label => label.id === id)?.uuid || '',
                        ],
                      })
              }
            />
          ))}
        <span
          style={{ marginLeft: '12px', marginBottom: '-2px', opacity: 0.6 }}
        >
          {row.description}
        </span>
        {row.accepted === false && row.hierarchy.length > 1 ? (
          <Box
            sx={{
              display: 'flex',
              marginLeft: '12px',
              opacity: 0.6,
              alignItems: 'center',
            }}
          >
            <Icon
              style={{
                display: 'flex',
                fontSize: '12px',
                alignItems: 'center',
                height: '20px',
                marginRight: '3px',
              }}
            >
              {iconNames.folder}
            </Icon>
            {row.hierarchy[row.hierarchy.length - 2].title}
          </Box>
        ) : null}
      </Box>
    </td>
  );
};
