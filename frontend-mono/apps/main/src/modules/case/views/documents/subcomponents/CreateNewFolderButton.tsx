// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { trashBinView } from '../Documents.signals';
import { TooltipButton } from '../TooltipButton';
import { FolderDialog } from './FolderDialog';

export const CreateNewFolderButton = () => {
  const [t] = useTranslation('case');
  const [dialogOpened, setDialogOpened] = React.useState(false);

  return (
    <>
      <TooltipButton
        title={t('documentTableActions.createFolder')}
        sx={{
          height: '45px',
          marginTop: '6px',
          marginLeft: '3px',
          visibility: trashBinView.value ? 'hidden' : 'visible',
        }}
        iconSize="medium"
        name="documentTabAddFolder"
        icon="create_new_folder"
        onClick={() => setDialogOpened(true)}
      />
      {dialogOpened && (
        <FolderDialog onClose={() => setDialogOpened(false)} edit={false} />
      )}
    </>
  );
};
