// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { TEXTAREA } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { TEXT } from '../../../../../components/Form/Constants/fieldTypes';
import { ChipButton } from '../ChipButton';
import { useMergeDocumentsMutation } from '../Documents.requests';
import { useCaseObjectQuery } from '../../../Case.library';
import { useSelectedAcceptedRows } from '../Documents.library';

export const MergeAction = () => {
  const [t] = useTranslation('case');
  const [dialogOpened, setDialogOpened] = React.useState(false);
  const onClose = () => setDialogOpened(false);
  const { mutateAsync: mergeDocuments, isLoading: saving } =
    useMergeDocumentsMutation(onClose);
  const [files, selections] = useSelectedAcceptedRows();
  const caseObject = useCaseObjectQuery();

  return (
    <>
      {selections.onlyFiles && selections.multiple && (
        <ChipButton
          onClick={() => setDialogOpened(true)}
          icon="merge"
          title={t('documentTableActions.merge')}
        />
      )}

      {dialogOpened && (
        <FormDialog
          minHeight={480}
          saving={saving}
          onSubmit={values =>
            mergeDocuments({
              fileUuids: files.map(file => file.uuid || ''),
              caseUuid: caseObject.data?.uuid || '',
              caseId: caseObject.data?.number || 0,
              filename: values.name,
            })
          }
          icon="merge"
          title={t('documentTabDialogs.merge.title')}
          formDefinitionT={t}
          onClose={onClose}
          formDefinition={[
            {
              type: TEXTAREA,
              minHeight: '125px',
              value: t('documentTabDialogs.merge.disclaimer'),
              format: 'text',
              name: 'none',
              readOnly: true,
            },
            {
              type: TEXT,
              required: true,
              format: 'text',
              endAdornment: '.pdf',
              label: t('documentTabDialogs.merge.name'),
              value: files[0].name,
              name: 'name',
            },
            {
              type: TEXTAREA,
              minHeight: files.length * 15 + 50 + 'px',
              value:
                t('documentTabDialogs.merge.mergedFiles') +
                files.map(file => '\n- ' + file.title).join(''),
              format: 'text',
              name: 'none2',
              readOnly: true,
            },
          ]}
        />
      )}
    </>
  );
};
