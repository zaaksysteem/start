// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTranslation } from 'react-i18next';
import React from 'react';
import { ChipButton } from '../ChipButton';
import { currentMode } from '../Documents.signals';
import { useFileRestoreMutation } from '../Documents.requests';
import { findAndMark, useSelectedAcceptedRows } from '../Documents.library';

export const DocumentsRestore = () => {
  const [t] = useTranslation('case');
  const [, , selectedRowIds] = useSelectedAcceptedRows();
  const { mutateAsync: restoreFiles } = useFileRestoreMutation();

  return (
    <ChipButton
      icon="refresh"
      title={t('documentTableActions.restore')}
      onClick={() => {
        restoreFiles(selectedRowIds.value).then(() => {
          currentMode.value = 'documents';
          findAndMark(selectedRowIds.value);
        });
      }}
    />
  );
};
