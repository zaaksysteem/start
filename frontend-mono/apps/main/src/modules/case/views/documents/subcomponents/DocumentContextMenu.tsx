// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useCaseObjectQuery } from '../../../Case.library';
import { documentContext, rejectFile } from '../Documents.signals';
import { PropertiesDialog } from '../actions/EditDocumentProperties';
import { UNSUPPORTED_COPY_TO_PDF_EXTENSIONS } from '../Documents.library';
import {
  useAcceptDocumentsMutation,
  useBulkDeleteMutation,
  useCopyToPdfMutation,
  useDocumentsQuery,
} from '../Documents.requests';
import { EditSingleDocument } from '../actions/EditDocument';
import { FolderDialog } from './FolderDialog';

/* eslint complexity: [2, 99] */
export const DocumentContextMenu = () => {
  const { data } = useDocumentsQuery();
  const row = documentContext.value.row;
  const { mutateAsync: deleteRows } = useBulkDeleteMutation();
  const [folderRenameOpened, setFolderRenameOpened] = React.useState(false);
  const [propertiesDialogOpened, setPropertiesDialogOpened] =
    React.useState(false);
  const caseObject = useCaseObjectQuery();
  const { mutate: copyToPdf } = useCopyToPdfMutation();
  const [t] = useTranslation('case');
  const { mutate: acceptDocuments } = useAcceptDocumentsMutation();
  const closeMenu = () => {
    documentContext.value = { ...documentContext.value, opened: false };
  };

  return (
    <>
      <Menu
        open={Boolean(documentContext.value.opened)}
        disableScrollLock={true}
        anchorReference="anchorPosition"
        onClose={closeMenu}
        onContextMenu={ev => {
          ev.preventDefault();
          closeMenu();
        }}
        anchorPosition={{
          top: (documentContext.value.posY || 0) - 6,
          left: (documentContext.value.posX || 0) + 2,
        }}
      >
        {row && !row.accepted && caseObject.data?.canEdit ? (
          <MenuItem
            onClick={() => {
              acceptDocuments([
                {
                  as_version_of: data?.find(
                    target => target.accepted && target.title === row.title
                  )?.uuid,
                  uuid: row.uuid,
                },
              ]);
              closeMenu();
            }}
          >
            {t('documentTableActions.accept')}
          </MenuItem>
        ) : null}
        {row && !row.accepted && caseObject.data?.canEdit ? (
          <MenuItem
            onClick={() => {
              rejectFile.value = { title: row.title, id: row.id };
              closeMenu();
            }}
          >
            {t('documentTableActions.reject')}
          </MenuItem>
        ) : null}
        {row?.type === 'folder' && caseObject.data?.canEdit ? (
          <MenuItem
            onClick={() => {
              row && setFolderRenameOpened(true);
              closeMenu();
            }}
          >
            {t('documentTableActions.renameFolder')}
          </MenuItem>
        ) : null}
        {row?.type === 'file' ? (
          <MenuItem
            onClick={() => {
              window.open(
                `${window.location.origin}${row.downloadUrl}`,
                '_blank'
              );
              closeMenu();
            }}
          >
            {t('documentTableActions.download')}
          </MenuItem>
        ) : null}
        {/* {row?.type === 'file' ? (
          <MenuItem
            onClick={() => {
              window.open(
                `${window.location.origin}${row.previewUrl}`,
                '_blank'
              );
              closeMenu();
            }}
          >
            {t('documentTableActions.previewPdf')}
          </MenuItem>
        ) : null} */}
        {row?.accepted && caseObject.data?.canEdit ? (
          <MenuItem
            onClick={() => {
              row && deleteRows([row]);
              closeMenu();
            }}
          >
            {t('documentTableActions.delete')}
          </MenuItem>
        ) : null}
        {row?.accepted && caseObject.data?.canEdit ? (
          <EditSingleDocument row={row} onClick={closeMenu} />
        ) : null}
        {row?.type === 'file' &&
        caseObject.data?.canEdit &&
        row.accepted &&
        !UNSUPPORTED_COPY_TO_PDF_EXTENSIONS.includes(row.extension || '') ? (
          <MenuItem
            onClick={() => {
              copyToPdf({
                caseId: caseObject?.data?.number || 0,
                fileId: row.id,
              });
              closeMenu();
            }}
          >
            {t('documentTableActions.copyAsPdf')}
          </MenuItem>
        ) : null}
        {row?.type === 'file' && row.accepted ? (
          <MenuItem
            onClick={() => {
              window.open(
                `/intern/zaak/${caseObject?.data?.number}/communicatie/?view=message&documents=${row.id}`,
                '_top'
              );
              closeMenu();
            }}
          >
            {t('documentTableActions.message')}
          </MenuItem>
        ) : null}
        {row?.type === 'file' ? (
          <MenuItem
            onClick={() => {
              setPropertiesDialogOpened(true);
              closeMenu();
            }}
          >
            {t('documentTableActions.properties')}
          </MenuItem>
        ) : null}
      </Menu>
      {folderRenameOpened && (
        <FolderDialog
          onClose={() => setFolderRenameOpened(false)}
          edit={true}
          row={row}
        />
      )}
      {propertiesDialogOpened && (
        <PropertiesDialog
          onClose={() => setPropertiesDialogOpened(false)}
          row={row}
        />
      )}
    </>
  );
};
