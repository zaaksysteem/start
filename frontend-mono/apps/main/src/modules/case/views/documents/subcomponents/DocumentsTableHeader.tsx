// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Box, Checkbox } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button/Button';
import FolderMove from '@mintlab/ui/App/Material/Icon/library/FolderMove';
import { KeyboardArrowDown } from '@mui/icons-material';
import {
  ActiveFiltering,
  DocumentsNodeType,
  DragSource,
  SelectionsHook,
} from '../Documents.types';
import { actionButtonHoverTweak } from '../actionButtonHoverTweak';
import { useRelocationMutation } from '../Documents.requests';

const turnOnHighlighted = () => {
  document.body.style.setProperty(
    '--mainFolderDropzoneBackground',
    '#feffad99'
  );
  document.body.style.setProperty(
    '--mainFolderDropzoneShadow',
    '0px 0px 5px 2px #0000001f'
  );
};

const turnOffHighlighted = () => {
  document.body.style.setProperty('--mainFolderDropzoneBackground', '');
  document.body.style.setProperty('--mainFolderDropzoneShadow', 'none');
};

const iconStyle = {
  fontSize: '27px',
  opacity: 0.7,
  marginRight: '10px',
  position: 'absolute',
};

turnOffHighlighted();

/* eslint complexity: [2, 128] */
export const DocumentsTableHeader = (props: {
  actions: React.ReactNode;
  allRows: DocumentsNodeType[];
  rowIsPartOfTheTable: (row: DocumentsNodeType) => boolean;
  useSelectedRows: SelectionsHook;
  dragSource: DragSource;
  activeFiltering?: ActiveFiltering;
  setFiltering?: (filtering: ActiveFiltering) => void;
  showActionsColumn?: boolean;
}) => {
  const [selectedRows, selections, bucket] = props.useSelectedRows();
  const [t] = useTranslation('case');
  const { mutateAsync: relocate } = useRelocationMutation();

  const getAriaSort = (name: ActiveFiltering['target']) =>
    props.activeFiltering &&
    props.activeFiltering.target === name &&
    props.activeFiltering.desc
      ? 'descending'
      : props.activeFiltering &&
          props.activeFiltering.target === name &&
          !props.activeFiltering.desc
        ? 'ascending'
        : 'none';

  const setFilteringCallback = (name: ActiveFiltering['target']) => () =>
    props.setFiltering &&
    props?.setFiltering({
      target: name,
      desc:
        props.activeFiltering?.target === name
          ? !props.activeFiltering.desc
          : true,
    });

  return (
    <tr className="header" role="row">
      {props.dragSource[0] == 'tableItem' ? (
        <Box
          onDragOver={ev => {
            ev.preventDefault();
            turnOnHighlighted();
          }}
          onDragLeave={ev => {
            ev.preventDefault();
            turnOffHighlighted();
          }}
          onDrop={ev => {
            ev.preventDefault();
            turnOffHighlighted();

            if (props.dragSource[0] == 'tableItem') {
              relocate({
                destination: null,
                rows: selectedRows.filter(row => row.accepted),
              });
            }
          }}
          className="pillBanner mainFolderDropzoneBanner"
        >
          <FolderMove sx={iconStyle} />
          <span className="selected-count">
            {t(
              `documentTableHeaders.mainFolderDropzoneBanner${
                props.dragSource[0] === 'tableItem' ? 'Intern' : 'Extern'
              }`
            )}
          </span>
        </Box>
      ) : selections.exist ? (
        <>
          <Box className="pillBanner documentsBanner">
            <div
              style={{
                display: 'inline-flex',
                alignItems: 'center',
              }}
            >
              <Button
                iconSize="small"
                onClick={() => (bucket.value = [])}
                name="removeDocumentSelections"
                icon="close"
                sx={{
                  ...actionButtonHoverTweak,
                  position: 'absolute',
                }}
              />
              <span className="selected-count">
                {selectedRows.length
                  ? `${selectedRows.length} ${t('documentTableHeaders.selected')}`
                  : t('documentTableHeaders.noSelections')}
              </span>
            </div>
            <Box className="actionsContainer">{props.actions}</Box>
          </Box>
        </>
      ) : (
        <>
          <th className="row-checkbox">
            <Checkbox
              name="selectAll"
              onClick={() => {
                bucket.value = (
                  bucket.value.length === 0 ? props.allRows : []
                ).map(row => row.id);
              }}
            />
          </th>
          <th
            style={{
              marginLeft: '15px',
              cursor: props.setFiltering ? 'pointer' : 'initial',
            }}
            className="row-title"
            tabIndex={0}
            aria-label={t('documentTableHeaders.title')}
            aria-sort={getAriaSort('title')}
            onClick={setFilteringCallback('title')}
            onKeyUp={ev =>
              ev.key === 'Enter' && setFilteringCallback('title')()
            }
          >
            {t('documentTableHeaders.title')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'title' &&
            !props.activeFiltering.untouched ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th
            style={{ cursor: props.setFiltering ? 'pointer' : 'initial' }}
            className="row-extension"
            tabIndex={0}
            aria-label={t('documentTableHeaders.extension')}
            aria-sort={getAriaSort('extension')}
            onClick={setFilteringCallback('extension')}
          >
            {t('documentTableHeaders.extension')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'extension' ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th
            style={{ cursor: props.setFiltering ? 'pointer' : 'initial' }}
            className="row-origin"
            tabIndex={0}
            aria-label={t('documentTableHeaders.origin')}
            aria-sort={getAriaSort('origin')}
            onClick={setFilteringCallback('origin')}
          >
            {t('documentTableHeaders.origin')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'origin' ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th
            style={{ cursor: props.setFiltering ? 'pointer' : 'initial' }}
            className="row-origindate"
            tabIndex={0}
            aria-label={t('documentTableHeaders.date')}
            aria-sort={getAriaSort('originDate')}
            onClick={setFilteringCallback('originDate')}
          >
            {t('documentTableHeaders.date')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'originDate' ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th className="row-version">{}</th>
          <th
            style={{ cursor: props.setFiltering ? 'pointer' : 'initial' }}
            className="row-created"
            tabIndex={0}
            aria-label={t('documentTableHeaders.created')}
            aria-sort={getAriaSort('createdAt')}
            onClick={setFilteringCallback('createdAt')}
          >
            {t('documentTableHeaders.created')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'createdAt' ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th
            style={{ cursor: props.setFiltering ? 'pointer' : 'initial' }}
            className="row-author"
            tabIndex={0}
            aria-label={t('documentTableHeaders.author')}
            aria-sort={getAriaSort('author')}
            onClick={setFilteringCallback('author')}
          >
            {t('documentTableHeaders.author')}
            {props.activeFiltering &&
            props.activeFiltering.target === 'author' ? (
              <KeyboardArrowDown
                sx={
                  !props.activeFiltering.desc
                    ? { transform: 'rotate(180deg)' }
                    : {}
                }
              />
            ) : null}
          </th>
          <th className="row-placeholder" />
          {props.showActionsColumn && (
            <th style={{ paddingLeft: '5px' }} className="row-actions">
              {t('documentTableHeaders.actions')}
            </th>
          )}
        </>
      )}
    </tr>
  );
};
