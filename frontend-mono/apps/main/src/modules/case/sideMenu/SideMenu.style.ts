// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSideMenuStyles = makeStyles(
  ({
    mintlab: { greyscale, shadows },
    palette: { common, primary },
  }: Theme) => ({
    wrapper: {
      position: 'relative',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '260px',
      '&>*:not(:last-child)': {
        borderBottom: `1px solid ${greyscale.darker}`,
      },
      borderRight: `1px solid ${greyscale.darker}`,
    },
    caseActionsWrapper: {
      padding: '8px 8px',
      height: 55,
      display: 'flex',
      alignItems: 'center',
    },
    caseActionsIcon: {
      backgroundColor: primary.main,
      color: common.white,
    },
    folded: {
      width: 55,
    },
    foldButtonWrapper: {
      position: 'absolute',
      top: '90%',
      right: -16,
      zIndex: 1,
    },
    foldButton: {
      backgroundColor: greyscale.dark,
      border: greyscale.darkest,
      '&:hover': {
        backgroundColor: greyscale.darker,
      },
      boxShadow: shadows.sharp,
    },
  })
);
