// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchDocumentAttributesType,
  FetchDocumentAttributesParamsType,
  DocumentAttributesResponseBodyType,
  FetchThumbnailType,
  ThumbnailResponseBodyType,
  FetchRelatedDocumentsType,
  FetchRelatedDocumentsParamsType,
  RelatedDocumentsResponseBodyType,
} from './Next2Know.types';

export const fetchDocumentAttributes: FetchDocumentAttributesType = async (
  endpoint,
  searchIndex,
  documentId,
  headers
) => {
  const url = buildUrl<FetchDocumentAttributesParamsType>(
    `${endpoint}/documents/${searchIndex}/${documentId}`,
    { format: 'array' }
  );

  const response = await request<DocumentAttributesResponseBodyType>(
    'GET',
    url,
    undefined,
    undefined,
    headers
  );

  return response.data;
};

export const fetchThumbnail: FetchThumbnailType = async (
  endpoint,
  searchIndex,
  documentId,
  headers
) => {
  const url = `${endpoint}/thumbnails/${searchIndex}/${documentId}`;

  let response = null;

  try {
    response = await request<ThumbnailResponseBodyType>(
      'GET',
      url,
      undefined,
      undefined,
      headers
    );
  } catch {
    // thumbnails often don't exist
  }

  return response;
};

export const fetchRelatedDocuments: FetchRelatedDocumentsType = async (
  endpoint,
  params,
  headers
) => {
  const url = buildUrl<FetchRelatedDocumentsParamsType>(
    `${endpoint}/documents/_all/search`,
    params
  );

  let relatedDocuments = null;

  try {
    const response = await request<RelatedDocumentsResponseBodyType>(
      'GET',
      url,
      undefined,
      undefined,
      headers
    );

    relatedDocuments = response.data.hits;
  } catch {
    // documents are not always a part of a file, and thus have no related documents
  }

  return relatedDocuments;
};
