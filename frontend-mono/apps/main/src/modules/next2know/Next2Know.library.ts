// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createHeaders } from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Search/ExternalSearch/next2know/next2know.library';
import {
  fetchDocumentAttributes,
  fetchRelatedDocuments,
  fetchThumbnail,
} from './Next2Know.requests';
import { FindAttributeType, GetDocumentInfoType } from './Next2Know.types';

const NAME_KEY = 'Naam';
const CASE_NUMBER_KEY = 'Externe_identificatiekenmerken_nummer_binnen_systeem';

export const findAttribute: FindAttributeType = (documentAttributes, key) =>
  documentAttributes.find(attr => attr.name === key)?.value as string;

export const getDocumentInfo: GetDocumentInfoType = async (
  integration,
  searchIndex,
  documentId
) => {
  const { endpoint, token } = integration;
  const headers = createHeaders(token);

  const documentAttributes = await fetchDocumentAttributes(
    endpoint,
    searchIndex,
    documentId,
    headers
  );

  const thumbnail = await fetchThumbnail(
    endpoint,
    searchIndex,
    documentId,
    headers
  );

  const title = findAttribute(documentAttributes, NAME_KEY);
  const caseNumber = findAttribute(documentAttributes, CASE_NUMBER_KEY);

  const params = {
    // eslint-disable-next-line id-length
    q: `${CASE_NUMBER_KEY}:${caseNumber}`,
    size: 100,
  };

  const relatedDocuments = await fetchRelatedDocuments(
    endpoint,
    params,
    headers
  );

  return {
    title,
    documentAttributes,
    thumbnail,
    relatedDocuments,
  };
};
