// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { nl } from 'date-fns/locale';

export default {
  nl: {
    dateFnsLocale: nl,
    exportFiles: 'Exportbestanden',
    columns: {
      name: 'Naam',
      type: 'Type',
      status: 'Status',
      started: 'Gestart',
      finished: 'Beëindigd',
      expires: 'Verloopt',
      resultsFile: 'Succes',
      errorFile: 'Fouten',
      actions: 'Acties',
      caseId: 'Zaaknummer',
      remarks: 'Opmerkingen',
      warnings: 'Waarschuwingen',
      errors: 'Fouten',
    },
    status: {
      pending: 'Voorbereiden…',
      running: 'Bezig…',
      cancelled: 'Geannuleerd',
      finished: 'Afgerond',
      processed: 'Verwerkt',
    },
    refresh: 'Ververs resultaten',
    actions: {
      cancel: {
        title: 'Bevestiging annuleren Job',
        body: 'Weet u zeker dat u de Job met de naam "{{name}}" wil annuleren?',
        message: 'Job geannuleerd',
        buttonTitle: 'Annuleer Job',
      },
      delete: {
        title: 'Bevestig verwijderen Job',
        body: 'Weet u zeker dat u de Job met de naam "{{name}}" wil verwijderen?',
        message: 'Job verwijderd',
        buttonTitle: 'Verwijder Job',
      },
      copy: {
        buttonTitle: 'Opnieuw uitvoeren',
      },
      newSavedSearch: {
        buttonTitle: 'Nieuwe zoekopdracht',
      },
      preview: {
        buttonTitle: 'Open verkenner',
      },
      download: {
        buttonTitle: 'Download bestand',
      },
    },
    noName: '(Geen naam)',
  },
};
