// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  exportFilesDataGrid: {
    '&& .MuiDataGrid-columnHeaderTitleContainerContent': {
      flex: 1,
    },
  },
  wrapper: {
    maxWidth: '80%',
  },
  gridToolbar: {
    display: 'flex',
    alignItems: 'center',
    gap: 10,
    marginLeft: 'auto',
  },
  actionsCell: {
    display: 'flex',
    gap: 6,
  },
  nameCell: {
    display: 'flex',
    gap: 8,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    '&>:nth-child(1)': {
      width: 20,
    },
    '&>:nth-child(2)': {
      flex: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  },
  fileCell: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  fileHeader: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
}));
