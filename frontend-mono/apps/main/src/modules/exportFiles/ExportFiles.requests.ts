// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIJobs, APICaseManagement } from '@zaaksysteem/generated';
import * as i18next from 'i18next';
//@ts-ignore
import * as Papaparse from 'papaparse';
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import {
  GenericResultsFileType,
  GenericErrorFileType,
  AnyEntryType,
  FieldType,
  JobEntryType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { getURL } from './library/ExportFiles.library';

type getJobsType = (t: i18next.TFunction) => () => Promise<AnyEntryType[]>;
export const getJobs: getJobsType = t => async () => {
  let formattedResponse: AnyEntryType[] = [];
  const responseJobs = await request<APIJobs.GetJobsResponseBody>(
    'GET',
    '/api/v2/jobs/get_jobs'
  ).catch(openServerError);

  const responseLegacyDownloads =
    await request<APICaseManagement.GetExportFileResponseBody>(
      'GET',
      '/api/v2/cm/export/get_exports?page=1&page_size=100'
    ).catch(openServerError);

  // Legacy downloads
  formattedResponse = (responseLegacyDownloads?.data || []).reduce(
    (
      acc: AnyEntryType[],
      current: APICaseManagement.GetExportFileResponseBody['data'],
      index: number
    ) => {
      acc.push({
        id: current.id,
        uuid: current.id,
        name: current.attributes.name,
        status: 'finished',
        progress: 100,
        expires: current.attributes.expires_at,
        type: 'legacy_download',
        friendlyName: current.attributes.name,
        isLastInGroup: index + 1 === responseLegacyDownloads?.data.length,
        token: current.attributes.token,
        fileUUID: current.attributes.fileUUID,
      });
      return acc;
    },
    formattedResponse
  );

  // Job entries
  formattedResponse = (responseJobs?.data || []).reduce<any>(
    (acc: AnyEntryType[], current) => {
      acc.push({
        id: current.id,
        uuid: current.id,
        name: current.attributes.friendly_name || t('exportFiles:noName'),
        status: current.attributes.status,
        progress: Math.floor(current.attributes.progress_percent || 0),
        started: current.attributes.started_at,
        expires: current.attributes.expires_at,
        type: current.type,
        jobType: current.attributes.job_description.job_type,
        friendlyName: current.attributes.friendly_name,
        simulation:
          current.attributes.job_description.job_type.indexOf('simul') > -1,
        selection: current.attributes.job_description.selection,
        resultsFileSize: current.attributes.results_file_size,
        resultsFileType: current.attributes.results_file_type,
        errorsFileSize: current.attributes.errors_file_size,
        totalItemCount: current.attributes.total_item_count,
      });
      return acc;
    },
    formattedResponse
  );

  return formattedResponse;
};

export const getMapper =
  (jobType: JobEntryType['jobType'], fieldType: FieldType) =>
  (row: string[]) => {
    const simulation = jobType.indexOf('simul') > -1;
    const getGenericSuccess = () => {
      let uuid,
        caseID,
        remarks,
        warning = false;
      if (simulation) {
        uuid = row[0];
        caseID = row[1];
        remarks = row[3];
        warning = Boolean(remarks);
      } else {
        uuid = row[0];
        caseID = row[1];
        remarks = row[2];
      }
      return {
        type: warning ? 'warning' : 'result',
        jobType,
        id: uuid,
        caseID,
        simulation,
        remarks,
      } as GenericResultsFileType;
    };
    const getGenericError = () => {
      const [uuid, caseID, errors] = row;
      return {
        type: 'error',
        jobType,
        id: uuid,
        caseID,
        simulation,
        errors,
      } as GenericErrorFileType;
    };

    if (!jobType) return '';
    if (jobType.indexOf('delete_case') > -1) {
      return fieldType === 'resultsFile'
        ? getGenericSuccess()
        : getGenericError();
    } else if (jobType.indexOf('run_archive_export') > -1) {
      return fieldType === 'resultsFile'
        ? getGenericSuccess()
        : getGenericError();
    }
  };

export const getFile = (job: AnyEntryType) => async (params: any) => {
  if (!isPopulatedArray(params?.queryKey)) return null;
  const parse = params?.queryKey[1]?.parse;
  const fieldType = params?.queryKey[1]?.fieldType;
  const jobType = params?.queryKey[1]?.jobType;
  const url = getURL({ fieldType, job });

  const response = await fetch(url || '');
  const contents = await response.text();

  if (parse) {
    const parsed = Papaparse.parse(contents, {
      delimiter: ',',
      header: false,
      skipEmptyLines: true,
    });

    return isPopulatedArray(parsed?.data)
      ? parsed?.data.map(getMapper(jobType, fieldType))
      : contents;
  } else {
    return contents;
  }
};
