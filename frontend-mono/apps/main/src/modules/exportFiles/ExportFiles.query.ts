// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { useQuery } from '@tanstack/react-query';
import * as i18next from 'i18next';
import { openServerError } from '@zaaksysteem/common/src/signals';
import {
  AnyFileType,
  AnyEntryType,
  FieldType,
  JobEntryType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import { getJobs, getFile } from './ExportFiles.requests';
import {
  QUERY_KEY_JOBS,
  QUERY_KEY_FILE,
  REFETCH_INTERVAL,
} from './library/ExportFiles.library';

export const useJobsQuery = (t: i18next.TFunction) => {
  return useQuery<AnyEntryType[], any>([QUERY_KEY_JOBS], getJobs(t), {
    onError: openServerError,
    staleTime: 0,
    refetchInterval: REFETCH_INTERVAL,
    refetchOnWindowFocus: true,
  });
};

export const useFileQuery = ({
  fieldType,
  enabled = true,
  parse = false,
  onSuccess,
  job,
}: {
  fieldType: FieldType;
  job: JobEntryType;
  onSuccess?: any;
  parse?: boolean;
  enabled?: boolean;
}) => {
  const { jobType, uuid } = job;

  return useQuery<AnyFileType[], any>(
    [
      QUERY_KEY_FILE,
      {
        jobUUID: uuid,
        fieldType,
        jobType,
        parse,
      },
    ],
    getFile(job),
    {
      onError: openServerError,
      enabled,
      onSuccess,
    }
  );
};
