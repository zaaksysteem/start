// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
// import Module from 'module';
import React, {
  useState,
  useMemo,
  useCallback,
  FunctionComponent,
} from 'react';
import { DataGridPro } from '@mui/x-data-grid-pro';
import { useTheme } from '@mui/material';
import GRID_DEFAULT_LOCALE_TEXT from '@zaaksysteem/common/src/locale/datagrid.locale';
import { useTranslation } from 'react-i18next';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useStyles } from './ExportFiles.styles';
import { getColumns } from './library/getColumns';
import { useJobsQuery } from './ExportFiles.query';
import GridToolBar from './components/GridToolBar';
import ViewerDialog from './dialogs/Viewer/Viewer';
import NewSavedSearchDialog from './dialogs/NewSavedSearch/NewSavedSearch';
import ConfirmDialogs from './dialogs/ConfirmDialogs/ConfirmDialogs';
import { DialogType } from './ExportFiles.types';
import { getRowClassName, getRowSpacing } from './library/ExportFiles.library';
import CopyDialog from './dialogs/CopyDialog/CopyDialog';
import { useClientUtils } from './hooks/useClientUtils';
import { useDateFnsLocale } from './hooks/useDateFnsLocale';

/* eslint complexity: [2, 12] */
const ExportFiles: FunctionComponent = () => {
  const classes = useStyles();
  const [t] = useTranslation();
  const theme = useTheme<Theme>();
  const [dialog, setDialog] = useState<DialogType | null>(null);
  const [localeModule, setLocaleModule] = useState<Locale | null>(null);
  const [invalidate] = useClientUtils();
  useDateFnsLocale({
    onLoad: mod => setLocaleModule(mod),
  });

  //@ts-ignore
  const jobsQuery = useJobsQuery(t);

  const memoizedGridColumns: any[] = useMemo(
    //@ts-ignore
    () => getColumns({ classes, t, setDialog, theme, localeModule }),
    [localeModule]
  );

  const memoizedSlots: any = useMemo(
    () => ({
      toolbar: () => (
        <GridToolBar setDialog={setDialog} t={t} classes={classes} />
      ),
    }),
    []
  );

  const handleCloseDialog = useCallback(() => setDialog(null), []);

  if (jobsQuery.isLoading || !localeModule) return <Loader />;

  return (
    <Sheet
      classes={{
        sheet: classes.wrapper,
      }}
    >
      {dialog && (
        <ConfirmDialogs dialog={dialog} setDialog={setDialog} t={t as any} />
      )}
      <DataGridPro
        key={'datagrid-jobs'}
        rows={jobsQuery?.data || []}
        className={classes.exportFilesDataGrid}
        columns={memoizedGridColumns}
        pagination={true}
        disableRowSelectionOnClick={true}
        disableColumnSelector={true}
        localeText={GRID_DEFAULT_LOCALE_TEXT}
        loading={jobsQuery?.isLoading}
        slots={memoizedSlots}
        slotProps={{
          toolbar: {
            showQuickFilter: true,
          },
        }}
        getRowClassName={getRowClassName(classes)}
        getRowSpacing={getRowSpacing()}
      />
      {dialog?.type === 'viewer' && (
        <ViewerDialog
          job={dialog?.parameters.job}
          fieldType={dialog?.parameters.fieldType}
          onClose={handleCloseDialog}
          t={t as any}
        />
      )}
      {dialog?.type === 'newSavedSearch' && (
        <NewSavedSearchDialog
          job={dialog?.parameters?.job}
          onClose={handleCloseDialog}
          t={t as any}
        />
      )}
      {dialog?.type === 'copyJob' && (
        <CopyDialog
          job={dialog?.parameters?.job}
          onSuccess={() => invalidate()}
          onClose={handleCloseDialog}
          t={t as any}
        />
      )}
    </Sheet>
  );
};

export default ExportFiles;
