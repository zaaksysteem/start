// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import * as i18next from 'i18next';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { useMutation } from '@tanstack/react-query';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import {
  deleteJob,
  cancelJob,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.requests';
import { useClientUtils } from '../../hooks/useClientUtils';
import { DialogType } from '../../ExportFiles.types';

type ConfirmDialogsType = {
  dialog: DialogType;
  setDialog: Dispatch<SetStateAction<DialogType | null>>;
  t: i18next.TFunction;
};

/* eslint complexity: [2, 10] */
const ConfirmDialogs: FunctionComponent<ConfirmDialogsType> = ({
  dialog,
  setDialog,
  t,
}) => {
  const [invalidate] = useClientUtils();
  const deleteMutation = useMutation<any, V2ServerErrorsType, any>(
    (job_uuid: string) => deleteJob(job_uuid)
  );
  const cancelMutation = useMutation<any, V2ServerErrorsType, any>(
    (job_uuid: string) => cancelJob(job_uuid)
  );
  return (
    <>
      {dialog && dialog.type === 'deleteJob' && (
        <ConfirmDialog
          open={true}
          onConfirm={() => {
            deleteMutation.mutate(dialog.parameters.uuid, {
              onSuccess: () => {
                openSnackbar({
                  message: t('exportFiles:actions.delete.message'),
                  sx: snackbarMargin,
                });
                invalidate();
                setDialog(null);
              },
              onError: openServerError,
            });
          }}
          onClose={() => {
            setDialog(null);
          }}
          title={t('exportFiles:actions.delete.title')}
          body={t('exportFiles:actions.delete.body', {
            name: dialog?.parameters?.name || '',
          })}
        />
      )}
      {dialog && dialog.type === 'cancelJob' && (
        <ConfirmDialog
          open={true}
          onConfirm={() => {
            cancelMutation.mutate(dialog.parameters.uuid, {
              onSuccess: () => {
                openSnackbar({
                  message: t('exportFiles:actions.cancel.message'),
                  sx: snackbarMargin,
                });
                invalidate();
                setDialog(null);
              },
              onError: openServerError,
            });
          }}
          onClose={() => {
            setDialog(null);
          }}
          title={t('exportFiles:actions.cancel.title')}
          body={t('exportFiles:actions.cancel.body', {
            name: dialog?.parameters?.name || '',
          })}
        />
      )}
    </>
  );
};

export default ConfirmDialogs;
