// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { GridRenderCellParams, GridRowParams } from '@mui/x-data-grid-pro';
import { getJobKind } from '@zaaksysteem/common/src/components/Jobs/JobDialogs.library';
import {
  AnyFileType,
  FieldType,
  JobEntryType,
} from '@zaaksysteem/common/src/components/Jobs/Jobs.types';

export const getColumns = ({
  fieldType,
  t,
  jobType,
  theme,
}: {
  fieldType: FieldType;
  t: i18next.TFunction;
  jobType: JobEntryType['jobType'];
  theme: Theme;
}) => {
  const renderSimulationCell = ({ row }: GridRenderCellParams<AnyFileType>) =>
    t(`common:${row.simulation ? 'yes' : 'no'}`);

  if (fieldType === 'resultsFile') {
    return [
      {
        field: 'type',
        headerName: '',
        width: 50,
        renderCell: ({ row }: GridRenderCellParams<AnyFileType>) => (
          <Icon
            size="small"
            sx={{
              color:
                row.type === 'warning'
                  ? theme.palette.review.main
                  : theme.palette.grass.main,
              marginLeft: '6px',
            }}
          >
            {row.type === 'warning'
              ? iconNames.warning
              : iconNames.check_circle}
          </Icon>
        ),
      },
      {
        field: 'caseID',
        headerName: t('viewer:columns.caseID'),
        width: 100,
      },
      {
        field: 'simulation',
        headerName: t('viewer:columns.simulation'),
        width: 100,
        renderCell: renderSimulationCell,
      },
      {
        field: 'remarks',
        headerName: t('viewer:columns.remarks'),
        flex: 1,
      },
    ];
  } else if (fieldType === 'errorFile') {
    return [
      {
        field: 'type',
        headerName: '',
        width: 50,
        renderCell: () => (
          <Icon
            size="small"
            sx={{
              color: theme.palette.error.dark,
              marginLeft: '6px',
            }}
          >
            {iconNames.error}
          </Icon>
        ),
      },
      {
        field: 'simulation',
        headerName: t('viewer:columns.simulation'),
        width: 100,
        renderCell: renderSimulationCell,
      },
      {
        field: 'caseID',
        headerName: t('viewer:columns.caseID'),
        width: 100,
      },
      {
        field: 'errors',
        headerName: t('viewer:columns.errors'),
        flex: 1,
      },
    ];
  }

  return [];
};

export const openURL = (params: GridRowParams<AnyFileType>) => {
  const { row } = params;
  const kind = getJobKind(row.jobType);
  const getURL = () => {
    if (kind === 'case') {
      return `/intern/zaak/${row.caseID}`;
    }
  };
  top?.window.open(getURL(), '_blank');
};
