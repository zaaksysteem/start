// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  contentWrapper: {
    height: '75vh',
    display: 'flex',
  },
  tableWrapper: {
    display: 'flex',
    flexGrow: 1,
  },
  viewerDataGrid: {
    '.MuiDataGrid-cell:focus': {
      outline: 'none',
    },
    '& .MuiDataGrid-row:hover': {
      cursor: 'pointer',
    },
  },
  actionButtons: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '10px 20px 10px 20px',
  },
}));
