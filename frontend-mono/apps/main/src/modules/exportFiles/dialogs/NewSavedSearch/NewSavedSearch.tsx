// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useEffect, useState } from 'react';
import * as i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import { getJobKind } from '@zaaksysteem/common/src/components/Jobs/JobDialogs.library';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { JobEntryType } from '@zaaksysteem/common/src/components/Jobs/Jobs.types';
import attributesLocale from '@zaaksysteem/common/src/locale/attributes.locale';
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useFileQuery } from '../../ExportFiles.query';
import { useSingleSaveMutation } from '../../../advancedSearch/query/useSingle';
import { useSingleUpdateLabelsMutation } from '../../../advancedSearch/query/useLabels';
import { getCombinedResults } from '../../library/ExportFiles.library';
import locale from './NewSavedSearch.locale';
import {
  getSavedSearchPayload,
  getFormDefinition,
  isValidChoice,
  CombinedResultsType,
} from './NewSavedSearch.library';

type NewSavedSearchDialogPropsType = {
  onClose: () => void;
  t: i18next.TFunction;
  job: JobEntryType;
};

/* eslint complexity: [2, 22] */
export const NewSavedSearchDialog: FunctionComponent<NewSavedSearchDialogPropsType> =
  React.memo(({ onClose, t, job }) => {
    const { jobType } = job;
    const [allSavesDone, setAllSavesDone] = useState<boolean>(false);
    const [afterSaveParams, setAfterSaveParams] = useState<{
      [key: string]: any;
    } | null>(null);
    const [initialized, setInitialized] = useState<boolean>(false);
    const [abort, setAbort] = useState<boolean>(false);
    const [combinedResults, setCombinedResults] =
      useState<CombinedResultsType | null>(null);
    const kind = getJobKind(jobType);
    const [searchT] = useTranslation();

    const savedSearchSaveMutation = useSingleSaveMutation({
      kind,
      identifier: null,
      mode: 'new',
    });

    const labelsUpdateMutation = useSingleUpdateLabelsMutation();

    const resultQuery = useFileQuery({
      fieldType: 'resultsFile',
      job,
      parse: true,
    });
    const errorQuery = useFileQuery({
      fieldType: 'errorFile',
      job,
      parse: true,
    });

    // After results files have been fetched
    useEffect(() => {
      if (resultQuery.isFetched && errorQuery.isFetched) {
        const results = getCombinedResults(resultQuery?.data, errorQuery?.data);
        if (
          !isPopulatedArray(results.errors) &&
          !isPopulatedArray(results.warnings) &&
          !isPopulatedArray(results.successes)
        ) {
          setAbort(true);
        } else {
          setCombinedResults(results);
        }

        setInitialized(true);
      }
    }, [resultQuery.isFetched, errorQuery.isFetched]);

    // After saving
    useEffect(() => {
      if (!allSavesDone || !afterSaveParams) return;
      openSnackbar({
        message: t('newSavedSearchDialog:snacks.success'),
        sx: snackbarMargin,
      });
      if (afterSaveParams?.open) {
        const url = `/main/advanced-search/${kind}/view/${afterSaveParams?.uuid}`;
        //@ts-ignore
        window.open(url, '_blank').focus();
      }
      onClose();
    }, [allSavesDone, afterSaveParams]);

    return (
      <FormDialog
        key={'newsavedsearch-dialog'}
        icon="saved_search"
        open={true}
        saving={
          savedSearchSaveMutation.isLoading || labelsUpdateMutation.isLoading
        }
        title={t('newSavedSearchDialog:title')}
        initializing={!initialized}
        formDefinition={getFormDefinition({
          combinedResults,
          kind,
          abort,
          t,
          job,
        })}
        validationMap={{
          //@ts-ignore
          basedOn: isValidChoice({ combinedResults, t }),
        }}
        initialTouched={{ basedOn: true }}
        onClose={onClose}
        disableSubmit={abort}
        onSubmit={async formValues => {
          const { open, labels, name } = formValues;
          const savedSearchPayload = getSavedSearchPayload({
            combinedResults,
            formValues,
            //@ts-ignore
            t: searchT,
            kind,
          });
          savedSearchSaveMutation.mutate(savedSearchPayload, {
            onSuccess: (uuid: string) => {
              if (isPopulatedArray(labels)) {
                labelsUpdateMutation.mutate(
                  {
                    uuid,
                    labels: labels.map(label => label.value),
                  },
                  {
                    onError: openServerError,
                    onSuccess: () => setAllSavesDone(true),
                  }
                );
              } else {
                setAllSavesDone(true);
              }
              setAfterSaveParams({
                uuid,
                open,
                name,
              });
            },
            onError: openServerError,
          });
        }}
      />
    );
  });

export default (props: NewSavedSearchDialogPropsType) => (
  <I18nResourceBundle resource={attributesLocale} namespace="attributes">
    <I18nResourceBundle resource={locale} namespace="newSavedSearchDialog">
      <NewSavedSearchDialog {...props} />
    </I18nResourceBundle>
  </I18nResourceBundle>
);
