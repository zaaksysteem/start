// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Job opnieuw uitvoeren',
    success: 'Job succesvol aangemaakt',
    kind: {
      case: 'zaken',
      custom_object: 'objecten',
    },
    recalculate: {
      label: 'Herbereken {{kind}}',
    },
    fields: {
      name: {
        label: 'Naam',
        placeholder: 'Naam Job',
      },
      simulation: {
        label: 'Uitvoeren als simulatie',
      },
      approve: {
        error: 'Dit veld is verplicht.',
        label:
          'Ik bevestig het uitvoeren van deze bulkactie en ben mij bewust van de gevolgen.',
      },
    },
    summary: {
      intro:
        'Middels dit scherm kan de gekozen Job "{{name}}" opnieuw worden uitgevoerd. ',
      simulation: {
        yes: 'De Job zal gesimuleerd worden en <0>niet</0> daadwerkelijk worden uitgevoerd. ',
        no: 'De Job zal <0>daadwerkelijk</0> worden uitgevoerd en <0>niet</0> worden gesimuleerd. ',
      },
      recalculate: {
        types: {
          id_list: 'de eerder geselecteerde {{kind}}',
          filter: 'de gekozen filters',
        },
        yes: `De {{kind}} zullen opnieuw worden berekend op basis van de huidige situatie, gebruik makend van {{type}}.`,
        no: 'De {{kind}} uit de originele Job zullen worden overgenomen.',
      },
    },
  },
};
