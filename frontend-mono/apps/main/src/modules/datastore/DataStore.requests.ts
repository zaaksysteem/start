// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { DataTypeType, ActionTypeType } from './DataStore.types';

export const fetchData = async (dataType: DataTypeType, filters: any) => {
  const url = buildUrl(`/datastore/search/${dataType}`, filters);

  const response = await request('GET', url);

  return response;
};

export const startExport = async (dataType: DataTypeType, filters: any) => {
  const url = `/datastore/csv/${dataType}`;

  const response = await request('POST', url, filters);

  return response;
};

export const updateContact = async (
  actionType: ActionTypeType,
  contactType: 'np' | 'nnp',
  id: number
) => {
  const url = `/gegevensmagazijn/${contactType}/${actionType}`;
  const data = {
    selection_id: [id],
    selection_type: 'subset',
  };

  const response = await request('POST', url, data);

  return response;
};

export const bulkAction = async (
  type: ActionTypeType,
  contactType: 'np' | 'nnp',
  data: any
) => {
  const url = `/gegevensmagazijn/${contactType}/${type}`;

  const response = await request('POST', url, data);

  return response;
};
