// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { v4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  Rule,
  hideFields,
  setDisabled,
  setLabel,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import {
  getDialogTitle,
  useDocumentTemplateQuery,
  useSaveDocumentTemplateMutation,
} from '../library';
import { documentTemplateFormDefinition } from './documentTemplate.formDefinition';

/* eslint complexity: [2, 11] */
export const useDocumentTemplateDialog = (folderId: string) => {
  const [t] = useTranslation('catalog');
  const [openedData, setOpenedData] = React.useState<false | { id?: string }>(
    false
  );

  const { mutateAsync, isLoading: saving } = useSaveDocumentTemplateMutation(
    () => setOpenedData(false)
  );

  const id = openedData && openedData.id;
  const editMode = Boolean(id);
  const title = getDialogTitle(t as any, 'documentTemplate', editMode);

  const { data, isLoading } = useDocumentTemplateQuery(Boolean(id), id || '');

  const dialog = openedData && (
    <FormDialog
      icon="insert_drive_file"
      open={true}
      formDefinitionT={t}
      initializing={Boolean(id) && isLoading}
      saving={saving}
      initialValues={{
        ...(data?.data.attributes
          ? {
              name: data?.data.attributes.name,
              integration_reference:
                data?.data.attributes.integration_reference,
              integration_uuid:
                data?.data.attributes.integration_uuid || 'default',
              help: data?.data.attributes.help,
              file: {
                value: data?.data.attributes.file_uuid,
                key: data?.data.attributes.file_uuid,
                label: data?.data.attributes.file_name,
              },
              category_uuid: data?.data.attributes.category_uuid,
            }
          : {}),
        commit_message: id ? t('defaultEdit') : t('defaultCreate'),
      }}
      rules={[
        new Rule()
          .when(() => Boolean(id))
          .then(setDisabled(['integration_uuid'])),
        new Rule()
          //@ts-ignore
          .when('integration_uuid', field => {
            return field.value === 'default';
          })
          .then(showFields(['file']))
          .and(hideFields(['integration_reference']))
          .else(hideFields(['file']))
          .and(showFields(['integration_reference'])),
        new Rule()
          .when(
            'integration_uuid',
            //@ts-ignore
            field => field.value?.module === 'stuf_dcr'
          )
          .then(
            setLabel(
              ['integration_reference'],
              t('documentTemplate.fields.templateExternalName.labelStufDCR')
            )
          ),
        new Rule()
          .when(
            'integration_uuid',
            //@ts-ignore
            field => field.value?.module === 'xential'
          )
          .then(
            setLabel(
              ['integration_reference'],
              t('documentTemplate.fields.templateExternalName.labelXential')
            )
          ),
      ]}
      title={title}
      formDefinition={documentTemplateFormDefinition(
        openedData.id ? '' : t('defaultCreate')
      )}
      onClose={() => setOpenedData(false)}
      onSubmit={payload => {
        const url = openedData.id
          ? '/api/v2/admin/catalog/edit_document_template'
          : '/api/v2/admin/catalog/create_document_template';

        return mutateAsync({
          payload: {
            document_template_uuid: openedData.id || v4(),
            fields: {
              category_uuid: folderId || payload.category_uuid || null,
              help: payload.help,
              commit_message: payload.commit_message,
              name: payload.name,
              file_uuid:
                payload.integration_uuid === 'default'
                  ? payload.file.value
                  : null,
              ...(payload.integration_uuid === 'default'
                ? {}
                : {
                    integration_uuid: payload.integration_uuid.value,
                    integration_reference: payload.integration_reference,
                  }),
            },
          },
          url,
        });
      }}
    />
  );

  return {
    openDocumentTemplateDialog: setOpenedData,
    documentTemplateDialog: dialog,
  };
};
