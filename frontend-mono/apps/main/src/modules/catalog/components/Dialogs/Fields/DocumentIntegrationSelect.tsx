// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { APIAdmin } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';

const useChoices = () => {
  const [t] = useTranslation('catalog');

  const data = useQuery<any[], V2ServerErrorsType>(
    ['document_integrations'],
    async () => {
      const body =
        await request<APIAdmin.GetActiveDocumentIntegrationsResponseBody>(
          'GET',
          '/api/v2/admin/integrations/get_active_document_integrations'
        );

      const integrations = body.data.map((int: any) => ({
        id: int.id,
        value: int.id,
        label: int.attributes.name,
        module: int.attributes.module,
      }));

      return [
        { id: 'default', value: 'default', label: t('system') },
        ...integrations,
      ];
    },
    { enabled: true, onError: openServerError }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [
    selectProps,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

export const DocumentIntegrationSelect: FormFieldComponentType<
  ValueType<string>,
  any
> = ({ value, multiValue, styles, config, ...restProps }) => {
  const [selectProps, emptyChoicesResult] = useChoices();

  return (
    <Select
      isClearable={false}
      {...restProps}
      {...selectProps}
      value={
        selectProps.choices.find(choice => choice.value === value) || value
      }
      disabled={Boolean(restProps?.disabled || emptyChoicesResult)}
      isMulti={multiValue}
    />
  );
};
