// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { TEXT } from '../../../../../components/Form/Constants/fieldTypes';
import { useChangeOnlineStatusMutation } from '../library';

export const useChangeOnlineStatusDialog = () => {
  const [t] = useTranslation('catalog');
  const [openedData, setOpenedData] = React.useState<[string, boolean]>([
    '',
    false,
  ]);

  const { mutateAsync, isLoading: saving } = useChangeOnlineStatusMutation(() =>
    setOpenedData(['', false])
  );

  const type = openedData[1] ? 'offline' : 'online';

  const dialog = openedData[0] && (
    <FormDialog
      icon="extension"
      open={true}
      formDefinitionT={t}
      saving={saving}
      title={t('changeOnlineStatus.fields.title', { type })}
      onClose={() => setOpenedData(['', false])}
      formDefinition={[
        {
          name: 'reason',
          type: TEXT,
          value: '',
          required: true,
          label: t('changeOnlineStatus.fields.reason.label', { type }),
          placeholder: t('changeOnlineStatus.fields.reason.placeholder', {
            type,
          }),
        },
      ]}
      onSubmit={values => {
        return mutateAsync({
          active: !openedData[1],
          case_type_uuid: openedData[0],
          reason: values.reason,
        });
      }}
    />
  );

  return {
    openChangeOnlineStatusDialog: setOpenedData,
    changeOnlineStatusDialog: dialog,
  };
};
