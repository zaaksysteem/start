// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import * as fieldTypes from '../../../../../components/Form/Constants/fieldTypes';
import DocumentAttributeFinder from '../Fields/DocumentAttributeFinder';

export const emailTemplateFormDefinition: FormDefinition = [
  {
    name: 'label',
    type: fieldTypes.TEXT,
    value: name,
    required: true,
    label: 'emailTemplate.fields.label.label',
    placeholder: 'emailTemplate.fields.label.label',
  },
  {
    name: 'sender',
    type: fieldTypes.TEXT,
    value: '',
    required: false,
    label: 'emailTemplate.fields.sender.label',
    placeholder: 'emailTemplate.fields.sender.placeholder',
  },
  {
    name: 'sender_address',
    type: fieldTypes.TEXT,
    format: 'email',
    allowMagicString: true,
    value: '',
    required: false,
    label: 'emailTemplate.fields.sender_address.label',
    placeholder: 'emailTemplate.fields.sender_address.placeholder',
  },
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'emailTemplate.fields.subject.label',
    placeholder: 'emailTemplate.fields.subject.label',
  },
  {
    name: 'message',
    type: fieldTypes.TEXTAREA,
    value: '',
    required: true,
    label: 'emailTemplate.fields.message.label',
    isMultiline: true,
  },
  {
    name: 'attachments',
    component: DocumentAttributeFinder,
    type: '',
    placeholder: 'form:choose',
    value: [],
    required: false,
    label: 'emailTemplate.fields.attachments.label',
    isMulti: true,
  },
  {
    name: 'commit_message',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'emailTemplate.fields.commit_message.label',
    help: 'emailTemplate.fields.commit_message.help',
  },
  {
    name: 'category_uuid',
    type: fieldTypes.TEXT,
    value: '',
    hidden: true,
  },
];
