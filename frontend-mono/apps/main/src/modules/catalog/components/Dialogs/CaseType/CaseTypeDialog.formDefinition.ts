// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { getCaseTypeStatusChoices } from '../library';

export const getFormDefinition = (
  t: i18next.TFunction,
  duplicating: boolean
): FormDefinition => {
  return [
    {
      name: 'name',
      type: fieldTypes.TEXT,
      value: '',
      required: true,
      label: t('caseTypeDialog.fields.labels.name'),
      placeholder: t('caseTypeDialog.fields.placeholders.name'),
    },
    {
      name: 'status',
      type: fieldTypes.RADIO_GROUP,
      value: 'offline',
      disabled: !duplicating,
      label: t('caseTypeDialog.fields.labels.status'),
      choices: getCaseTypeStatusChoices(t),
    },
    {
      name: 'update_description',
      type: fieldTypes.TEXTAREA,
      value: '',
      label: t('caseTypeDialog.fields.labels.update_description'),
    },
  ];
};
