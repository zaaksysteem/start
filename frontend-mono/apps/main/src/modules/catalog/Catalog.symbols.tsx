// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { QueryClient } from '@tanstack/react-query';

export const CATALOG_ITEMS = 'CATALOG_ITEMS';
export const DETAILS_VIEW = 'DETAILS_VIEW';
export const CASE_TYPE_VERSION_HISTORY = 'CASE_TYPE_VERSION_HISTORY';

export const invalidateAfterChange = (client: QueryClient) => {
  client.invalidateQueries([CATALOG_ITEMS]);
  client.invalidateQueries([DETAILS_VIEW]);
};
