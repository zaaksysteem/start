// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { attribute } from './locales/attribute';
import { changeOnlineStatus } from './locales/changeOnlineStatus';
import { detailView } from './locales/detailView';
import { documentTemplate } from './locales/documentTemplate';
import { emailTemplate } from './locales/emailTemplate';
import { folder } from './locales/folder';
import { history } from './locales/history';
import { serverErrors } from './locales/serverErrors';

export default {
  nl: {
    ...attribute,
    ...changeOnlineStatus,
    ...documentTemplate,
    ...emailTemplate,
    ...folder,
    ...history,
    ...detailView,
    ...serverErrors,
    actions: {
      import: 'Importeren',
      createCaseType: 'Zaaktype aanmaken',
      createObjectType: 'Objecttype aanmaken',
      createEmailTemplate: 'E-mailsjabloon aanmaken',
      createAttribute: 'Kenmerk aanmaken',
      createFolder: 'Map aanmaken',
      createDocumentTemplate: 'Sjabloon aanmaken',
    },
    buttonBar: {
      create: 'Aanmaken',
      edit: 'Bewerken',
      delete: 'Verwijderen',
      details: 'Details',
      duplicate: 'Dupliceren',
      export: 'Exporteren',
      move: 'Verplaatsen',
      history: 'Versiebeheer',
    },
    column: {
      name: 'Naam',
      type: 'Type',
    },
    items: {
      offline: 'Offline',
    },
    addElement: {
      title: 'Element aanmaken',
    },
    title: 'Catalogus',
    system: 'Zaaksysteem',
    search: 'Zoeken in de catalogus…',
    move: {
      label: 'Verplaats {{numToMove}} item',
      label_plural: 'Verplaatsen {{numToMove}} items',
      description: 'Ga naar de map waarnaar het item verplaatst moet worden.',
      description_plural:
        'Ga naar de map waarnaar de items verplaatst moeten worden.',
      moveButton: 'Verplaatsen naar deze map',
      success: 'De verplaatsing is succesvol uitgevoerd.',
    },
    delete: {
      success: 'Het item is succesvol verwijderd.',
      confirm: {
        title: 'Weet u het zeker?',
        placeholder: 'Reden',
        label: 'Geef hieronder een reden op.',
        description: 'U kunt deze actie niet ongedaan maken.',
        ok: 'Verwijderen',
      },
    },
    activateCaseType: {
      reason: 'reden',
      title: 'Zaaktypeversie activeren',
      menuTitle: 'Zaaktypeversie activeren',
      description:
        'Geef hieronder de reden op voor het activeren van deze versie.',
      error: 'Geef een reden op.',
      success: 'Het zaaktype is succesvol geactiveerd.',
    },
    defaultCreate: 'Nieuw aangemaakt',
    defaultEdit: 'Wijziging',
    caseTypeDialog: {
      action: {
        create: 'Aanmaken',
        duplicate: 'Dupliceren',
      },
      snack: {
        created: "Zaaktype '{{name}}' aangemaakt.",
        duplicated: "Zaaktype '{{name}}' gedupliceerd.",
      },
      fields: {
        labels: {
          name: 'Naam',
          status: 'Status',
          update_description: 'Wijzigingsomschrijving',
        },
        values: {
          status: {
            online: 'Online',
            offline: 'Offline',
          },
        },
        placeholders: {
          name: 'Naam',
        },
      },
      changedComponents: {
        common: 'Algemeen',
      },
    },
  },
};
