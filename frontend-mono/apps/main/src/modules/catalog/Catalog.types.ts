// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type CatalogItemTypeType =
  | 'folder'
  | 'case_type'
  | 'object_type'
  | 'attribute'
  | 'email_template'
  | 'document_template'
  | 'custom_object_type';

export type CatalogItemType = {
  active: boolean;
  type: CatalogItemTypeType;
  name: string;
  id: string;
};

export type CatalogItemDetailsType = {
  type: CatalogItemTypeType;
  lastModified: string;
  id: string;
  versionIndependentUuid: string;
  locationFolder: {
    name: string;
    id: string;
  };
  version?: string;
  magicString?: string;
  identification?: string;
  valueType?: string;
  enclosedDocument?: { link: string; name: string };
  status?: string;
  usedInCaseTypes?: string[];
  caseTypeContext?: string[];
};
