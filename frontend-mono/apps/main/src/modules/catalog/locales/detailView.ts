// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const detailView = {
  detailView: {
    enclosedDocument: 'Ingesloten document',
    itemsSelected: 'onderdelen geselecteerd',
    lastModified: 'Gewijzigd',
    valueFallback: 'Onbekend',
    magicString: 'Magicstring',
    used_in_case_types: 'Zaaktypen',
    used_in_object_types: 'Objecttypen',
    identification: 'Identificatie',
    uuid: 'UUID',
    versionUuid: 'Versie UUID',
    document: 'Document',
    valueType: 'Invoertype',
    versionTitle: 'Versie',
    relationsTitle: 'Gebruik',
    directLinksTitle: 'Directe links',
    copyToClipboard: 'Kopieer naar klembord.',
    copiedToClipboard: 'Gekopieerd naar klembord',
    locationFolder: 'Locatie',
    internalUrl: 'Intern registratieformulier',
    caseTypeApi: 'Zaaktype API',
    caseTypeV2: 'Zaaktypebeheer v2 (tijdelijk)',
    openInNewWindow: 'Open in nieuw venster',
    saveFile: 'Bestand opslaan',
    rootFolder: 'Hoofdmap',
    active: 'Actief',
    concept: 'Concept',
    status: 'Status',
  },
};
