// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { FormStepType } from '@zaaksysteem/common/src/components/form/hooks/useSteppedForm';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { AttributeItemType } from './FormFields/AttributeList/types';

export type ActionType = 'create' | 'edit';

type CreateObjectRequestBodyType =
  APICaseManagement.CreateCustomObjectRequestBody;
type UpdateObjectRequestBodyType =
  APICaseManagement.UpdateCustomObjectRequestBody;

type ObjectRequestBodyType =
  | CreateObjectRequestBodyType
  | UpdateObjectRequestBodyType;

export type ObjectTypeResponseBodyType =
  APICaseManagement.GetPersistentCustomObjectTypeResponseBody;

type AuthorizationTypes =
  APICaseManagement.CustomObjectTypeAuthorization['authorization'];

type CustomObjectTypeAuthorizationDefinition =
  APICaseManagement.CustomObjectTypeAuthorizationDefinition['authorizations'];

type FieldTypeAttributes =
  APICaseManagement.CustomObjectTypeCustomField['attributes'];

type RelationshipDefinition =
  APICaseManagement.CustomObjectTypeRelationshipDefinition;

type AuthorizationType = {
  department: null | string | ValueType<string>;
  role: string;
  permission: AuthorizationTypes;
};

export type FetchObjectTypeType = (
  uuid: string
) => Promise<ObjectTypeResponseBodyType>;

export type ObjectTypeType = Pick<
  FieldTypeAttributes,
  'uuid' | 'title' | 'subtitle' | 'name' | 'status'
> & {
  custom_field_definition?: FieldTypeAttributes['custom_field_definition'];
  objectTypeRelations?: { id: string; name: string }[];
  caseTypeRelations?: { id: string; name: string }[];
  authorizations?: AuthorizationType[];
  audit_log?: { updated_components: string[]; description: string };
  external_reference: string;
};

export type MapCustomFieldsType = (custom_field_definition: any) => any;

export type MapAuthorizationsType = (
  authorizations: CustomObjectTypeAuthorizationDefinition
) => AuthorizationType[];

export type MapRelationshipsType = (
  relationships: RelationshipDefinition
) => any;

export type FormatObjectType = (
  response: ObjectTypeResponseBodyType
) => ObjectTypeType;

export type GetObjectTypeType = (uuid: string) => Promise<ObjectTypeType>;

export type ObjectTypeFormShapeType = {
  uuid?: string;
  name: string;
  title: string;
  subtitle: string;
  external_reference: string;
  components_changed: string[];
  changes: string;
  custom_fields: AttributeItemType[];
  authorizations: AuthorizationType[];
  caseTypeRelations: { id: string; name: string }[];
  objectTypeRelations: { id: string; name: string }[];
};

export type CreateInitialValuesFromObjectType = (
  objectType: ObjectTypeType
) => Partial<ObjectTypeFormShapeType>;

export type GetObjectTypeFormDefinition = (
  t: i18next.TFunction
) => FormDefinition<ObjectTypeFormShapeType>;

export type GetObjectTypeFormSteps = (
  t: i18next.TFunction
) => (Pick<FormStepType, 'title' | 'description'> & { fieldNames: any })[];

export type SaveObjectTypeType = (
  action: string,
  values: ObjectTypeFormShapeType,
  uuid: string,
  folderUuid?: string
) => Promise<void>;

export type FormatDataType = (
  values: ObjectTypeFormShapeType,
  folderUuid?: string
) => ObjectRequestBodyType;

export type SubmitObjectTypeType = (
  action: string,
  data: ObjectRequestBodyType,
  uuid: string
) => Promise<void>;
