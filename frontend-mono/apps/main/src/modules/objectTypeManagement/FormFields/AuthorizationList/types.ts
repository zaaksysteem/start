// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';

type AuthorizationTypes =
  APICaseManagement.CustomObjectTypeAuthorization['authorization'];

export type AuthorizationFormPropsType = {
  t: i18next.TFunction;
};

export type GetFormDefinitionType = {
  t: i18next.TFunction;
};

export type AuthorizationType = {
  department: null | string | ValueType<string>;
  role: string;
  permission: AuthorizationTypes;
};
