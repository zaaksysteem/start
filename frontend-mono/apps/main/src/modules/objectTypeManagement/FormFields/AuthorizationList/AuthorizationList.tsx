// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { useAuthorizationListStyle } from './Authorization.style';
import AuthorizationForm from './AuthorizationForm';
import { AuthorizationType } from './types';

export const AuthorizationList: React.ComponentType<
  FormRendererFormField<any, any, AuthorizationType[]>
> = props => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useAuthorizationListStyle();
  const listClasses = useListStyle();
  const { fields, add } = useMultiValueField<any, AuthorizationType>(props);

  return (
    <div className={classNames(listClasses.listContainer)}>
      {fields.map(fieldProps => (
        <AuthorizationForm {...fieldProps} key={fieldProps.name} t={t as any} />
      ))}

      <div className={classes.addButtonWrapper}>
        <button
          type="button"
          className={classes.addButton}
          onClick={add as any}
          title={t('form.fields.authorization.addButton')}
        >
          {t('form.fields.authorization.addButton')}
          <Icon>{iconNames.add_circle_outline}</Icon>
        </button>
      </div>
    </div>
  );
};
