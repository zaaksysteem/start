// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useWidgetHeaderStyles } from '../Widget.style';
import Search from './Search';

type WidgetHeaderPropsType = {
  title: string;
  widgetUuid?: string;
  children?: React.ReactNode;
  onSearch?: (input: string) => void;
  onClose?: () => void;
  searchValue?: string;
  searchMode?: 'manual' | 'auto';
};

const deleteWidgetPostMessage = (widgetUuid: string) => {
  window.top?.postMessage(
    { type: 'deleteWidget', data: { widgetUuid } },
    window.location.origin
  );
};

const WidgetHeader: React.ComponentType<WidgetHeaderPropsType> = ({
  title,
  widgetUuid,
  children,
  onSearch,
  onClose,
  searchMode = 'auto',
  searchValue,
}) => {
  const classes = useWidgetHeaderStyles();
  const removeAction = widgetUuid
    ? () => deleteWidgetPostMessage(widgetUuid)
    : onClose;

  return (
    <div className={classes.widgetHeader}>
      <div className={[classes.title, 'widget-draggable-handle'].join(' ')}>
        {title}
      </div>
      {children}
      <div>
        {onSearch ? (
          <Search
            onSearch={onSearch}
            searchMode={searchMode}
            initialValue={searchValue}
          />
        ) : null}
        {removeAction ? (
          <Button
            name="deleteWidget"
            className={classes.delete}
            iconSize="small"
            icon="close"
            action={removeAction}
          />
        ) : null}
      </div>
    </div>
  );
};

export default WidgetHeader;
