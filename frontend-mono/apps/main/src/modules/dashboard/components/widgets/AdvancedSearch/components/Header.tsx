// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTranslation } from 'react-i18next';
import IconButton from '@mui/material/IconButton';
import WidgetHeader from '../../components/WidgetHeader';
import { useWidgetHeaderStyles } from '../../Widget.style';
import { SettingsType } from '../AdvancedSearch';
import ConfigDialog from './ConfigDialog';

type HeaderPropsType = {
  title: string;
  widgetUuid: string;
  onClose?: () => void;
  t: ReturnType<typeof useTranslation>['t'];
  settings: SettingsType | null;
  setValues: (settings: SettingsType) => void;
  forceOpen: boolean;
};

const AdvancedSearchHeader: React.ComponentType<HeaderPropsType> = ({
  title,
  widgetUuid,
  setValues,
  onClose,
  forceOpen,
  settings,
  t,
}) => {
  const classes = useWidgetHeaderStyles();
  const [configOpen, setConfigOpen] = useState(false);

  return (
    <WidgetHeader
      title={title}
      widgetUuid={widgetUuid}
      onClose={onClose}
      searchMode={'manual'}
      searchValue={settings?.keyword}
      onSearch={(searchVal: string) => {
        setValues({
          keyword: searchVal,
        });
      }}
    >
      <div className={classes.flexibleSpace} />
      <IconButton onClick={() => setConfigOpen(true)}>
        <Icon size="small" color="inherit">
          {iconNames.settings}
        </Icon>
      </IconButton>
      {configOpen || forceOpen ? (
        <ConfigDialog
          t={t}
          value={settings}
          onClose={() => setConfigOpen(false)}
          onSubmit={values => {
            setConfigOpen(false);
            setValues(values);
          }}
        />
      ) : null}
    </WidgetHeader>
  );
};

export default AdvancedSearchHeader;
