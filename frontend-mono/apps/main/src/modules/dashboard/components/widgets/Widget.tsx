// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './Widget.locale';
import { useWidgetStyles } from './Widget.style';

type WidgetPropsType = {
  header: React.ReactChild;
  children: React.ReactChild;
};

const Widget: React.ComponentType<WidgetPropsType> = ({ header, children }) => {
  const classes = useWidgetStyles();

  return (
    <I18nResourceBundle resource={locale} namespace="widget">
      <div className={classes.wrapper}>
        {header}
        {children}
      </div>
    </I18nResourceBundle>
  );
};

export default Widget;
