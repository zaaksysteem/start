// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable id-length */

import FindInPageIcon from '@mui/icons-material/FindInPage';
import FolderSpecialIcon from '@mui/icons-material/FolderSpecial';
import LibraryAddCheckIcon from '@mui/icons-material/LibraryAddCheck';
import LinkIcon from '@mui/icons-material/Link';
import React from 'react';
import Rule from '@zaaksysteem/common/src/components/form/rules/library/Rule';
import {
  externalUrlFormDefinition,
  advancedSearchFormDefinition,
  advancedSearchFormRules,
} from '../dialogs/WidgetFormDefinitions';
import ExternalUrl from './ExternalUrl/ExternalUrl';
import AdvancedSearch from './AdvancedSearch/AdvancedSearch';
import Tasks from './Tasks/Tasks';
import FavoriteCasetype from './FavoriteCasetype/FavoriteCasetype';

export const SEARCH = 'saved_search';
export const FAVORITE_CASETYPE = 'favorite_case_type';
export const TASKS = 'tasks';
export const EXTERNAL_URL = 'external_url';

export const widgetTypes = {
  [TASKS]: {
    component: Tasks,
    gridDefaults: { w: 6, h: 3, minW: 3, minH: 3 },
  },
  [EXTERNAL_URL]: {
    component: ExternalUrl,
    gridDefaults: { w: 3, h: 2, minW: 3, minH: 2 },
  },
  [SEARCH]: {
    component: AdvancedSearch,
    gridDefaults: { w: 6, h: 2, minW: 6, minH: 2 },
  },
  [FAVORITE_CASETYPE]: {
    component: FavoriteCasetype,
    gridDefaults: { w: 3, h: 2, minW: 3, minH: 2 },
  },
};

interface WidgetForm {
  formDefinitions: any;
  rules?: Rule[];
}

export interface Widget {
  name: string;
  type: keyof typeof widgetTypes;
  icon: JSX.Element;
  action: 'add' | 'config';
  form?: WidgetForm;
  minHeight?: number;
}

export const availableWidgets: Widget[] = [
  {
    name: 'Zoekopdracht',
    type: SEARCH,
    icon: <FindInPageIcon />,
    action: 'config',
    minHeight: 650,
    form: {
      formDefinitions: advancedSearchFormDefinition,
      rules: advancedSearchFormRules,
    },
  },
  {
    name: 'Favoriete zaaktypen',
    type: FAVORITE_CASETYPE,
    icon: <FolderSpecialIcon />,
    action: 'add',
  },
  {
    name: 'Taken',
    type: TASKS,
    icon: <LibraryAddCheckIcon />,
    action: 'add',
  },
  {
    name: 'Externe URL',
    type: EXTERNAL_URL,
    icon: <LinkIcon />,
    action: 'config',
    form: {
      formDefinitions: externalUrlFormDefinition,
    },
  },
];
