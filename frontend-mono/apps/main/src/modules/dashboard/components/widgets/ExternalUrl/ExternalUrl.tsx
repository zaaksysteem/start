// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FC } from 'react';
import Widget from '../Widget';
import WidgetHeader from '../components/WidgetHeader';

interface ExternalUrlProps {
  title: string;
  url: string;
  onClose: () => {};
}

const ExternalUrlModule: FC<ExternalUrlProps> = ({ title, url, onClose }) => (
  <Widget header={<WidgetHeader title={title} onClose={onClose} />}>
    <iframe
      style={{ padding: 10, width: '100%', height: 'calc(100% - 60px)' }}
      title={title}
      src={url}
      frameBorder="0"
    />
  </Widget>
);

export default ExternalUrlModule;
