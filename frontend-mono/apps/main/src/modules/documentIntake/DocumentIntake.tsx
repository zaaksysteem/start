// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment, useState, useRef, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import fecha from 'fecha';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { isDocument } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/isDocument';
import useInfiniteScroll from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { InfiniteLoader, SizeInfo, SortDirectionType } from 'react-virtualized';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import InfiniteTableLoader from '@zaaksysteem/common/src/components/InfiniteTableLoader/InfiniteTableLoader';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import Assignment from './components/Assignment';
import { useIntakeStyles } from './DocumentIntake.styles';
import Preview from './Preview/Preview';
import ActionBar from './ActionBar/ActionBar';
import DocumentPreviewPanel from './DocumentPreviewPanel/DocumentPreviewPanel';
import {
  getSelectedPreview,
  getData,
  mergeListSelected,
} from './DocumentIntake.library';
import { AssignmentType, DataParams } from './DocumentIntake.types';

const PAGE_LENGTH = 20;
const THRESHOLD = 5;
const REMOTE_ROW_COUNT = 99999;

const DocumentIntake: React.ComponentType = () => {
  const classes = useDocumentExplorerStyles();
  const intakeClasses = useIntakeStyles();
  const tableStyles = useSortableTableStyles();
  const [t] = useTranslation();

  const [assignment, setAssignment] = useState<AssignmentType>('all');

  const [tableDimensions, setTableDimensions] = useState<SizeInfo | null>(null);
  const [sortBy, setSortBy] = useState<string | null>(null);
  const [sortDirection, setSortDirection] = useState<SortDirectionType | null>(
    null
  );
  const [previewItem, setPreviewItem] = useState<null | DocumentItemType>(null);
  const [search, setSearch] = useState<string>('');

  const [initialLoading, setInitialLoading] = useState<boolean>(true);
  const infiniteLoaderRef = useRef<InfiniteLoader>(null);
  const { list, isRowLoaded, loadMoreRows, resetList, loading } =
    useInfiniteScroll<DocumentItemType, DataParams>({
      ref: infiniteLoaderRef,
      pageLength: PAGE_LENGTH,
      getData,
      getDataParams: {
        pageLength: PAGE_LENGTH,
        search,
        assignment,
        sortBy,
        sortDirection,
      },
    });

  useEffect(() => {
    (async function () {
      if (initialLoading) {
        await loadMoreRows({ startIndex: 1 });
        setInitialLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    resetList();
  }, [search, assignment, sortBy, sortDirection]);

  const selectionProps = useSelectionBehaviour({ rows: list });

  const { name, mimeType, description } = getColumns({
    t,
    classes,
  });

  const listWithSelected = mergeListSelected(list, selectionProps.selectedRows);
  const selectedItems = listWithSelected.filter(
    (item: DocumentItemType) => item.selected
  );

  const selectedPreview = getSelectedPreview(
    selectedItems as DocumentItemType[]
  );

  const columns = [
    name({
      fileNameAction: item => {
        if (item.preview) {
          return (event: React.MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            if (item.preview) {
              setPreviewItem(item);
            }
          };
        }
      },
      /* eslint-disable-next-line */
      iconRenderer: (item, icon) => {
        return item?.thumbnail?.url ? (
          <Tooltip
            classes={{
              popper: intakeClasses.popper,
            }}
            enterDelay={300}
            title={<Preview url={item.thumbnail.url} />}
          >
            {icon}
          </Tooltip>
        ) : (
          icon
        );
      },
      tooltipSuffix: item =>
        item.document_number ? ' | ' + item.document_number : undefined,
      showNotAccepted: false,
      showRejected: true,
    }),
    {
      label: '',
      name: 'assignment',
      width: 40,
      showFromWidth: 600,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => (
        <Assignment assignment={rowData.assignment} />
      ),
    },
    mimeType(),
    description(),
    {
      label: t('DocumentExplorer:columns.modified.label'),
      name: 'modified',
      width: 126,
      showFromWidth: 650,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => {
        const modifiedDate = rowData.modified
          ? fecha.format(new Date(rowData.modified), 'DD-M-YYYY HH:mm')
          : '';
        const modifiedBy = rowData.modifiedBy;
        const byLabel = modifiedBy ? t('documentIntake:by') : '';
        return (
          <div>
            <Tooltip
              title={`${modifiedDate} ${byLabel} ${modifiedBy || ''}`}
              placement={'top-start'}
              enterDelay={400}
            >
              <span>{modifiedDate}</span>
            </Tooltip>
          </div>
        );
      },
    },
    {
      label: '',
      name: 'open',
      width: 66,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: DocumentItemType }) => {
        if (isDocument(rowData) && rowData.preview?.url) {
          return (
            <Tooltip
              title={t('DocumentExplorer:columns.open.tooltip')}
              placement={'top-start'}
              enterDelay={400}
            >
              <IconButton
                title={t('DocumentExplorer:columns.open.label')}
                color="inherit"
                onClick={(event: any) => {
                  event.preventDefault();
                  event.stopPropagation();
                  top?.window.open(rowData.preview?.url, '_blank');
                }}
              >
                <Icon size="small" color="primary">
                  {iconNames.open_in_new}
                </Icon>
              </IconButton>
            </Tooltip>
          );
        }

        return <Fragment />;
      },
    },
    {
      label: '',
      name: 'download',
      width: 66,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: any) => {
        if (isDocument(rowData)) {
          return (
            <Tooltip
              title={t('DocumentExplorer:columns.download.tooltip')}
              placement={'top-start'}
              enterDelay={400}
            >
              <IconButton
                name={t('DocumentExplorer:columns.download.tooltip')}
                onClick={(event: React.MouseEvent) => {
                  if (rowData.download)
                    window.open(rowData.download.url, '_blank');
                  event.preventDefault();
                  event.stopPropagation();
                }}
                title={t('DocumentExplorer:columns.download.label')}
                color="inherit"
              >
                <Icon size="small" color="primary">
                  {iconNames.save_alt}
                </Icon>
              </IconButton>
            </Tooltip>
          );
        }

        return <Fragment />;
      },
    },
  ];

  return (
    <div className={classNames(classes.wrapper, intakeClasses.wrapper)}>
      <ActionBar
        onChange={(value: string) => setSearch(value)}
        onClose={() => setSearch('')}
        assignment={assignment}
        setAssignment={setAssignment}
        selectedItems={selectedItems}
        resetList={resetList}
      />
      <div className={intakeClasses.contentWrapper}>
        <DocumentPreviewPanel
          url={selectedPreview?.url}
          contentType={selectedPreview?.contentType}
          t={t as any}
        />
        <div className={intakeClasses.contentPanel}>
          <div className={classes.table}>
            <InfiniteTableLoader
              loading={loading}
              tableDimensions={tableDimensions}
              offset={150}
            />
            {/* @ts-ignore */}
            <InfiniteLoader
              isRowLoaded={isRowLoaded}
              loadMoreRows={loadMoreRows}
              rowCount={REMOTE_ROW_COUNT}
              threshold={THRESHOLD}
              ref={infiniteLoaderRef}
            >
              {({ onRowsRendered, registerChild }) => (
                <SortableTable
                  externalRef={registerChild}
                  onRowsRendered={onRowsRendered}
                  rows={listWithSelected}
                  //@ts-ignore
                  columns={columns}
                  rowHeight={50}
                  noRowsMessage={
                    initialLoading || loading
                      ? t('documentIntake:loadingMessage')
                      : t('documentIntake:noRowsMessage')
                  }
                  styles={tableStyles}
                  sortDirectionDefault="DESC"
                  sortInternal={false}
                  sorting="column"
                  onSort={(
                    sortBy: string,
                    sortDirection: SortDirectionType
                  ) => {
                    setSortBy(sortBy);
                    setSortDirection(sortDirection);
                  }}
                  onResize={(table: SizeInfo) => setTableDimensions(table)}
                  {...selectionProps}
                />
              )}
            </InfiniteLoader>
          </div>
        </div>
      </div>
      <DocumentPreviewModal
        open={Boolean(previewItem)}
        title={previewItem?.name || ''}
        url={previewItem?.preview?.url || ''}
        downloadUrl={previewItem?.download?.url}
        contentType={previewItem?.preview?.contentType || ''}
        onClose={() => setPreviewItem(null)}
      />
    </div>
  );
};

export default DocumentIntake;
