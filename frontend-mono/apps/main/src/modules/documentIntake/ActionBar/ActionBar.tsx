// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import { Box } from '@mui/material';
import { openServerError } from '@zaaksysteem/common/src/signals';
import ActionBar from '@mintlab/ui/App/Zaaksysteem/ActionBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import CaseCreateDialog from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/Dialogs/CaseCreate/CaseCreateDialog';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { DocumentUploadDialog } from '@zaaksysteem/common/src/components/dialogs/DocumentUploadDialog/DocumentUploadDialog';
import { AssignmentType, DialogsType } from '../DocumentIntake.types';
import { AddToCaseDialog } from '../AddToCase/AddToCaseDialog';
import AssignDialog from '../Assign/AssignDialog';
import RejectDialog from '../Reject/RejectDialog';
import PropertiesDialog from '../Properties/PropertiesDialog';
import { deleteDocument, useAssignmentChoices } from './ActionBar.library';

const DELAY = 300;
const MINLENGTH = 3;

type ActionBarPropsType = {
  onChange: (value: string) => void;
  onClose: () => void;
  assignment: AssignmentType;
  setAssignment: (assignement: AssignmentType) => void;
  selectedItems: DocumentItemType[];
  resetList: () => void;
};

/* eslint complexity: [2, 10] */
const ActionBarWrapper: React.ComponentType<ActionBarPropsType> = ({
  onChange,
  onClose,
  assignment,
  setAssignment,
  selectedItems,
  resetList,
}) => {
  const [t] = useTranslation('documentIntake');
  const assignmentChoices = useAssignmentChoices(t as any);

  const [searchTerm, setSearchTerm] = useState('');
  const [dialog, setDialog] = useState<DialogsType | null>(null);
  const closeDialog = () => setDialog(null);
  const onConfirm = () => {
    closeDialog();
    resetList();
  };

  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= MINLENGTH) onChange(value);
  }, DELAY);

  const handleChange = (value: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = value.target.value;
    setSearchTerm(searchValue);
    debouncedCallback(searchValue.trim());
  };

  const handleClose = () => {
    setSearchTerm('');
    onClose();
  };

  const handleSearchKeyPress = (input: React.KeyboardEvent) => {
    const key = input.key.toLowerCase();

    if (key === 'enter') onChange(searchTerm);
    if (key === 'escape') handleClose();
  };

  const hasSelectedItems = selectedItems.length > 0;
  const selectedSingleItem = selectedItems.length === 1;
  const selectedUuid = selectedItems.length ? selectedItems[0].uuid : '';
  // in the documentintake context there should only be documents
  // but we decided to keep this check just in case
  const selectedOnlyDocuments = selectedItems.every(
    item => item.type === 'document'
  );
  const isAssigned = Object.values(
    (selectedItems[0]?.type === 'document' && selectedItems[0]?.assignment) ||
      {}
  ).some(obj => obj?.name !== null);

  return (
    <>
      <ActionBar
        filters={[
          {
            type: 'select',
            value: assignment,
            name: 'filter-assignment',
            isClearable: false,
            isMulti: false,
            onChange: (ev: any) => {
              setAssignment(ev.target.value.value);
            },
            choices: assignmentChoices,
          },
          {
            value: searchTerm,
            type: 'text',
            name: 'search',
            icon: iconNames.search,
            placeholder: t('search'),
            onChange: handleChange,
            onKeyPress: handleSearchKeyPress,
            closeAction: searchTerm ? handleClose : undefined,
          },
        ]}
        actions={
          hasSelectedItems
            ? [
                {
                  name: 'caseCreate',
                  condition: selectedSingleItem && selectedOnlyDocuments,
                },
                {
                  name: 'addToCase',
                  condition: selectedOnlyDocuments,
                },
                {
                  name: 'delete',
                  icon: iconNames.delete,
                  condition: true,
                },
                {
                  name: 'properties',
                  icon: iconNames.info,
                  condition: selectedSingleItem,
                },
              ]
                .filter(item => item.condition)
                .map(item => ({
                  ...item,
                  label: t(`action.${item.name}`),
                  action: () => {
                    setDialog(item.name as DialogsType);
                  },
                }))
            : undefined
        }
        advancedActions={
          hasSelectedItems
            ? [
                {
                  name: 'assign',
                  condition: true,
                },
                {
                  name: 'reject',
                  condition: selectedSingleItem && isAssigned,
                },
              ].map(item => ({
                ...item,
                label: t(`action.${item.name}`),
                action: () => {
                  setDialog(item.name as DialogsType);
                },
              }))
            : undefined
        }
        permanentActions={[
          {
            name: 'fileUpload',
            icon: iconNames.upload_file,
            label: t('common:verbs.upload'),
            action: () => setDialog('fileUpload'),
          },
        ]}
      />

      <CaseCreateDialog
        open={dialog === 'caseCreate'}
        onClose={closeDialog}
        selectedDocumentUuid={selectedUuid}
        contactChannel="post"
      />
      <AddToCaseDialog
        open={dialog === 'addToCase'}
        onConfirm={onConfirm}
        onClose={closeDialog}
        selectedDocuments={selectedItems as DocumentItemType[]}
      />
      <ConfirmDialog
        open={dialog === 'delete'}
        onConfirm={async () => {
          const promises = selectedItems.map(item =>
            deleteDocument({
              document_uuid: item.uuid || '',
              reason: t('delete.reason'),
            })
          );
          Promise.all(promises)
            .then(() => {
              resetList();
            })
            .catch(openServerError)
            .finally(closeDialog);
        }}
        onClose={closeDialog}
        title={t('delete.title')}
        body={
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <div>{t('delete.body')}</div>
            <div>{t('delete.warning')}</div>
          </Box>
        }
      />
      {dialog === 'properties' && (
        <PropertiesDialog
          onConfirm={onConfirm}
          onClose={closeDialog}
          uuid={selectedUuid}
        />
      )}
      <AssignDialog
        open={dialog === 'assign'}
        onConfirm={onConfirm}
        onClose={closeDialog}
        selectedDocuments={selectedItems}
      />
      <RejectDialog
        open={dialog === 'reject'}
        onConfirm={onConfirm}
        onClose={closeDialog}
        selectedDocuments={selectedItems}
      />

      <DocumentUploadDialog
        open={dialog === 'fileUpload'}
        onConfirm={onConfirm}
        onClose={closeDialog}
      />
    </>
  );
};

export default ActionBarWrapper;
