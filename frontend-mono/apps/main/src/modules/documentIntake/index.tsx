// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
// @ts-ignore
import documentExplorerLocale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import useUnauthorizedBanner from '@zaaksysteem/common/src/hooks/useUnauthorizedBanner';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { hasCapability } from '@zaaksysteem/common/src/hooks/useSession';
import locale from './DocumentIntake.locale';
import DocumentIntake from './DocumentIntake';

const DocumentIntakeModule = () => {
  const [t] = useTranslation('documentIntake');
  const classes = useDocumentExplorerStyles();

  const Banner = useUnauthorizedBanner(session =>
    hasCapability(session, 'documenten_intake_subject')
  );

  return Banner ? (
    Banner
  ) : (
    <Sheet classes={{ sheet: classes.sheet }}>
      <DocumentIntake />
      <TopbarTitle title={t('title')} />
    </Sheet>
  );
};

export default () => (
  <I18nResourceBundle
    resource={documentExplorerLocale}
    namespace="DocumentExplorer"
  >
    <I18nResourceBundle resource={locale} namespace="documentIntake">
      <DocumentIntakeModule />
    </I18nResourceBundle>
  </I18nResourceBundle>
);
