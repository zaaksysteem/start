// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDataTableStyles = makeStyles(() => ({
  wrapper: {
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
  },
  table: {
    flexGrow: 1,
    overflow: 'auto',
    position: 'relative',
  },
  tableLoading: {
    opacity: 0.5,
  },
  loader: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  pagination: {
    width: `100%`,
    position: 'relative',
  },
}));

export const useTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      '&:hover': {
        backgroundColor: primary.lightest,
      },
    },
  })
);
