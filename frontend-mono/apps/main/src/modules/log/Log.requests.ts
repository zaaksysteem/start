// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const fetchData = async (params: any) => {
  const url = buildUrl('/api/v1/eventlog', params);

  const response = await request('GET', url);

  return response.result.instance;
};

export const fetchUsers = async (keyword: string) => {
  const url = buildUrl('/api/v2/cm/search', {
    keyword,
    type: ['employee'],
  });

  const response = await request('GET', url);

  return response;
};

export const startExport = async (params: any) => {
  const url = buildUrl('/api/v1/eventlog/export', params);

  const response = await request('GET', url);

  return response;
};
