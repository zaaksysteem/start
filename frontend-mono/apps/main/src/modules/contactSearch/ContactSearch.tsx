// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { useStyles } from './ContactSearch.styles';
import { ContactSearchSideMenu } from './ContactSearchSideMenu';
import { ContactType, ResultType, SearchType } from './ContactSearch.types';
import { performSearch } from './ContactSearch.library';
import SearchForm from './components/SearchForm';
import ResultsTable from './components/ResultsTable';

export interface ContactSearchPropsType {}

export type ContactSearchParamsType = {
  type: ContactType;
};

/* eslint complexity: [2, 8] */
const ContactSearch: React.FunctionComponent<ContactSearchPropsType> = () => {
  const { type } = useParams<
    keyof ContactSearchParamsType
  >() as ContactSearchParamsType;
  const classes = useStyles();
  const session = useSession();
  const countryCode = session.account.instance.country_code;
  const [results, setResults] = useState<ResultType[]>();

  const width = useWidth();
  const isCompact = ['xs', 'sm', 'md'].includes(width);
  const foldMenu = ['xs', 'sm', /* 'md', */ 'lg'].includes(width);

  useEffect(() => {
    setResults(undefined);
  }, [type]);

  const search: SearchType = values => {
    performSearch(type, values, countryCode)
      .then((results: ResultType[]) => {
        setResults(results);
      })
      .catch(openServerError);
  };

  return (
    <PanelLayout>
      <Panel type="side" folded={foldMenu}>
        <ContactSearchSideMenu type={type} folded={foldMenu} />
      </Panel>
      {isCompact ? (
        <Panel key={type}>
          {results ? (
            <ResultsTable
              type={type}
              results={results}
              setResults={setResults}
              isCompact={isCompact}
            />
          ) : (
            <SearchForm type={type} search={search} countryCode={countryCode} />
          )}
        </Panel>
      ) : (
        <>
          <Panel key={type} className={classes.searchPanel}>
            <SearchForm type={type} search={search} countryCode={countryCode} />
          </Panel>
          <Panel>
            <ResultsTable
              type={type}
              results={results}
              setResults={setResults}
              isCompact={isCompact}
            />
          </Panel>
        </>
      )}
    </PanelLayout>
  );
};

export default ContactSearch;
