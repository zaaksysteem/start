// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { openServerError } from '@zaaksysteem/common/src/signals';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import { queryClient } from '../../queryClient';
import ContactView from './ContactView';
import locale from './ContactView.locale';
import { useStyles } from './ContactView.styles';
import { SubjectType, TabType } from './ContactView.types';
import { fetchSubject } from './ContactView.requests';
import {
  formatTitle,
  formatSubTitle,
  getNotifications,
} from './ContactView.library';

export type ContactViewParamsType = {
  uuid: string;
  type: SubjectTypeType;
  ['*']: TabType;
};

const ContactViewModule: React.FunctionComponent = () => {
  const classes = useStyles();
  const { uuid, type } = useParams<
    keyof ContactViewParamsType
  >() as ContactViewParamsType;

  const [t] = useTranslation('contactView');

  const { data: subject } = useQuery<SubjectType, V2ServerErrorsType>(
    ['subject', uuid],
    () => fetchSubject(uuid, type),
    { onError: openServerError }
  );

  const refreshSubject = () => {
    queryClient.invalidateQueries(['subject']);
  };

  if (!subject) {
    return <Loader />;
  }

  return (
    <Sheet key={uuid}>
      <div className={classes.wrapper}>
        <NotificationBar notifications={getNotifications(t as any, subject)} />
        <ContactView
          uuid={uuid}
          type={type}
          subject={subject}
          refreshSubject={refreshSubject}
        />
        <TopbarTitle
          title={formatTitle(subject)}
          description={formatSubTitle(subject)}
        />
      </div>
    </Sheet>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="contactView">
    <ContactViewModule />
  </I18nResourceBundle>
);
