// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useLocationStyles = makeStyles(() => ({
  wrapper: {
    padding: 20,
    boxSizing: 'border-box',
  },
}));
