// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useSession, {
  hasActiveIntegration,
} from '@zaaksysteem/common/src/hooks/useSession';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import { fetchContactInformation } from './Information.actions';
import { useInformationStyles } from './Information.style';
import Person from './views/Person';
import Organization from './views/Organization';
import Employee from './views/Employee';
import { SubjectType } from './../ContactView.types';
import locale from './Information.locale';

const TableDictionary = {
  person: Person,
  organization: Organization,
  employee: Employee,
};

type InformationPropsType = {
  subject: SubjectType;
  refreshSubject: () => void;
};

const Information: React.FunctionComponent<InformationPropsType> = ({
  subject,
  refreshSubject,
}) => {
  const session = useSession();
  const classes = useInformationStyles();
  const View = TableDictionary[subject.type];
  const altAuthActive = hasActiveIntegration(session, 'auth_twofactor');

  const { status, data } = useQuery<
    ReturnType<typeof fetchContactInformation>,
    V2ServerErrorsType
  >(
    ['contactInformation', subject, altAuthActive],
    () => fetchContactInformation(subject, altAuthActive),
    { onError: openServerError }
  );

  return (
    <I18nResourceBundle resource={locale} namespace="information">
      <div className={classes.wrapper}>
        {status === 'loading' ? (
          <Loader />
        ) : (
          data && (
            <View
              data={data as any}
              session={session}
              refreshSubject={refreshSubject}
              setSnackOpen={() =>
                openSnackbar('De gegevens zijn succesvol opgeslagen.')
              }
            />
          )
        )}
      </div>
    </I18nResourceBundle>
  );
};

export default Information;
