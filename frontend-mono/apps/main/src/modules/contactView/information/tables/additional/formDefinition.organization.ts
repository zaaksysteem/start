// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export type FormValuesType = {
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferredContactChannel: string;
  internalNote: string;
  anynymousUser: boolean;
};

const getChoiceWithTranslation =
  (t: i18next.TFunction, name: string) => (value: string) => {
    return {
      value,
      label: t(`additional.${name}Value.${value}`),
    };
  };

export const getOrganizationFormDefinition = ({
  t,
  subject,
  contactChannelEnabled,
  hasEditCapability,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  hasEditCapability: boolean;
  contactChannelEnabled: boolean;
}): AnyFormDefinitionField[] => {
  const { anonymousUser } = subject;
  return [
    {
      name: 'phoneNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'mobileNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'email',
      type: fieldTypes.EMAIL,
      readOnly: anonymousUser,
    },
    ...(contactChannelEnabled
      ? [
          {
            name: 'preferredContactChannel',
            type: fieldTypes.FLATVALUE_SELECT,
            isClearable: false,
            defaultValue: 'pip',
            readOnly: anonymousUser,
            choices: ['pip', 'email', 'mail', 'phone'].map(
              getChoiceWithTranslation(t, 'preferredContactChannel')
            ),
          },
        ]
      : []),
    {
      name: 'internalNote',
      type: fieldTypes.TEXT,
      readOnly: anonymousUser,
    },
    {
      name: 'anonymousUser',
      type: fieldTypes.CHECKBOX,
      readOnly: !hasEditCapability,
    },
  ].map(field => ({
    label: t(
      `additional.${field.name === 'anonymousUser' ? 'anonymousUserOrg' : field.name}`
    ),
    value: subject[field.name],
    ...field,
  }));
};
