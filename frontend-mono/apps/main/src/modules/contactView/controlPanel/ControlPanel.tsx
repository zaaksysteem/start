// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { Typography } from '@mui/material';
import ButtonBar from '@mintlab/ui/App/Zaaksysteem/ButtonBar';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { useQuery } from '@tanstack/react-query';
import {
  ControlPanelType,
  DataType,
  EnvironmentType,
  HostType,
} from './ControlPanel.types';
import { useStyles } from './ControlPanel.styles';
import EnvironmentDialog from './Dialogs/Environment';
import HostsDialog from './Dialogs/Host';
import ControlPanelDetailsDialog from './Dialogs/ControlPanelDetails';
import Environments from './Environments/Environments';
import Hosts from './Hosts/Hosts';
import { CONTROL_PANEL_DATA } from './ControlPanel.constants';
import { getData } from './ControlPanel.requests';

export type ControlPanelPropsType = {
  controlPanel: ControlPanelType;
};

/* eslint complexity: [2, 99] */
const ControlPanel = ({ controlPanel }: ControlPanelPropsType) => {
  const classes = useStyles();
  const [t] = useTranslation('controlPanel');

  const [viewDetails, setViewDetails] = useState<boolean>(false);

  const [createEnvironment, setCreateEnvironment] = useState<boolean>(false);
  const [environmentToEdit, setEnvironmentToEdit] =
    useState<EnvironmentType | null>(null);

  const [createHost, setCreateHost] = useState<boolean>(false);
  const [hostToEdit, setHostToEdit] = useState<HostType | null>(null);

  const { data } = useQuery<DataType, V2ServerErrorsType>(
    [CONTROL_PANEL_DATA, controlPanel.uuid],
    () => getData(controlPanel.uuid),
    { onError: openServerError }
  );

  if (!data) {
    return <Loader />;
  }

  const { environments, hosts } = data;

  return (
    <div className={classes.wrapper}>
      <div>
        <div className={classes.header}>
          <Typography variant="h3">{t('environments.title')}</Typography>
          <ButtonBar
            actions={[
              {
                label: t('controlPanel.actions.details'),
                name: 'info',
                icon: iconNames.info,
                action: () => setViewDetails(true),
              },
            ]}
            permanentActions={[
              {
                label: `${t('environment')} ${t('common:verbs.create').toLowerCase()}`,
                name: 'create-environment',
                icon: iconNames.add,
                action: () => setCreateEnvironment(true),
              },
            ]}
          />
        </div>
        <Environments
          controlPanel={controlPanel}
          environments={environments}
          setEnvironmentToEdit={setEnvironmentToEdit}
        />
      </div>
      <div>
        <div className={classes.header}>
          <Typography variant="h3">{t('hosts.title')}</Typography>
          <ButtonBar
            actions={[
              {
                label: `${t('host')} ${t('common:verbs.create').toLowerCase()}`,
                name: 'create-host',
                icon: iconNames.add,
                action: () => setCreateHost(true),
              },
            ]}
          />
        </div>
        <Hosts hosts={hosts} setHostToEdit={setHostToEdit} />
      </div>
      {viewDetails && (
        <ControlPanelDetailsDialog
          controlPanel={controlPanel}
          environments={environments}
          onClose={() => setViewDetails(false)}
          open={viewDetails}
        />
      )}
      {(createEnvironment || environmentToEdit) && (
        <EnvironmentDialog
          controlPanel={controlPanel}
          environment={environmentToEdit}
          onClose={() => {
            setCreateEnvironment(false);
            setEnvironmentToEdit(null);
          }}
          open={createEnvironment || Boolean(environmentToEdit)}
        />
      )}
      {(createHost || hostToEdit) && (
        <HostsDialog
          controlPanel={controlPanel}
          environments={environments}
          host={hostToEdit}
          onClose={() => {
            setCreateHost(false);
            setHostToEdit(null);
          }}
          open={createHost || Boolean(hostToEdit)}
        />
      )}
    </div>
  );
};

export default ControlPanel;
