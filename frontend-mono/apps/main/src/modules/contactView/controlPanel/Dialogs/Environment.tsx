// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import { ControlPanelType, EnvironmentType } from '../ControlPanel.types';
import { saveEnvironment } from '../ControlPanel.requests';
import { queryClient } from '../../../../queryClient';
import { CONTROL_PANEL_DATA } from '../ControlPanel.constants';
import FqdnField, { FQDN } from './Fields/FqdnField';
import { getFormDefinition } from './Environment.formDefinition';

type EnvironmentDialogPropsType = {
  controlPanel: ControlPanelType;
  environment: EnvironmentType | null;
  onClose: () => void;
  open: boolean;
};

const EnvironmentDialog: React.ComponentType<EnvironmentDialogPropsType> = ({
  controlPanel,
  environment,
  onClose,
  open,
}) => {
  const [t] = useTranslation('controlPanel');

  const icon = environment ? iconNames.edit : iconNames.add;
  const saveLabel = environment
    ? t('common:verbs.edit')
    : t('common:verbs.create');
  const title = `${t('environment')} ${saveLabel.toLowerCase()}`;

  const formDefinition = getFormDefinition(t as any, controlPanel, environment);

  const save = async (values: any) => {
    await saveEnvironment(controlPanel, values, environment);

    queryClient.invalidateQueries([CONTROL_PANEL_DATA]);
    openSnackbar(t(`environments.snacks.${environment ? 'update' : 'create'}`));

    onClose();
  };

  return (
    <FormDialog
      saving={false}
      onSubmit={save}
      icon={icon}
      title={title}
      formDefinitionT={t as any}
      onClose={onClose}
      formDefinition={formDefinition}
      open={open}
      saveLabel={saveLabel}
      fieldComponents={{
        [FQDN]: FqdnField,
      }}
    />
  );
};

export default EnvironmentDialog;
