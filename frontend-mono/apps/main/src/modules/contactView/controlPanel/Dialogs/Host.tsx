// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { openSnackbar } from '@zaaksysteem/common/src/signals';
import {
  ControlPanelType,
  EnvironmentType,
  HostType,
} from '../ControlPanel.types';
import { saveHost } from '../ControlPanel.requests';
import { queryClient } from '../../../../queryClient';
import { CONTROL_PANEL_DATA } from '../ControlPanel.constants';
import { getFormDefinition } from './Host.formDefinition';

type HostsDialogPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  host: HostType | null;
  onClose: () => void;
  open: boolean;
};

const HostsDialog: React.FunctionComponent<HostsDialogPropsType> = ({
  controlPanel,
  environments,
  host,
  onClose,
  open,
}) => {
  const [t] = useTranslation('controlPanel');

  const icon = host ? iconNames.edit : iconNames.add;
  const saveLabel = host ? t('common:verbs.edit') : t('common:verbs.create');
  const title = `${t('host')} ${saveLabel.toLowerCase()}`;

  const formDefinition = getFormDefinition(
    t as any,
    host,
    controlPanel,
    environments
  );

  const save = async (values: any) => {
    await saveHost(controlPanel, values, host);

    queryClient.invalidateQueries([CONTROL_PANEL_DATA]);
    openSnackbar(t(`hosts.snacks.${host ? 'update' : 'create'}`));

    onClose();
  };

  return (
    <FormDialog
      saving={false}
      onSubmit={save}
      icon={icon}
      title={title}
      formDefinitionT={t as any}
      onClose={onClose}
      formDefinition={formDefinition}
      open={open}
      saveLabel={saveLabel}
    />
  );
};

export default HostsDialog;
