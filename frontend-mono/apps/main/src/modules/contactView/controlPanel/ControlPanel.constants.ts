// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  CustomerTypeType,
  OtapType,
  SoftwareVersionType,
} from './ControlPanel.types';

export const CONTROL_PANEL = 'controlPanel';
export const CONTROL_PANEL_DATA = 'controlPanelData';

export const customerTypes: CustomerTypeType[] = [
  'commercial',
  'government',
  'lab',
  'development',
  'staging',
  'acceptance',
  'preprod',
  'testing',
  'production',
];

// order is relevant for sorting the table
export const otapTypes: OtapType[] = [
  'production',
  'accept',
  'testing',
  'development',
];

export const softwareVersions: SoftwareVersionType[] = ['master'];
