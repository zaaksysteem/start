// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: Theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: 20,
    gap: 50,
  },
  setup: {
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
    gap: 10,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    gap: 20,
    padding: '0 30px 0 16px',
    alignItems: 'center',
    '&>:first-child': {
      flexGrow: 1,
    },
  },
  inactive: {
    opacity: 0.5,
  },
  tableRow: {
    '&:hover': {
      backgroundColor: greyscale.dark,
    },
    '&>.MuiTableCell-root': {
      padding: '5px 16px',
    },
  },
  tableCellSmall: {
    width: 110,
  },
  tableCellMedium: {
    width: 150,
  },
  tableCellIcon: {
    width: 70,
  },
  tableCellIconWithPadding: {
    width: 85,
  },
  placeholder: {
    padding: 20,
  },
}));
