// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const fillCSStemplateFile = (
  cssEntries: (readonly [string, string, boolean])[]
) => `
:root {
  ${cssEntries
    .map(([name, value, imports]) =>
      imports ? (value ? `@import url(${value});` : '') : `--${name}: ${value};`
    )
    .join('\n')}
}

/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */

/* Document
   ========================================================================== */

/**
 * 1. Correct the line height in all browsers.
 * 2. Prevent adjustments of font size after orientation changes in iOS.
 */

 html {
  line-height: 1.15; /* 1 */
  -webkit-text-size-adjust: 100%; /* 2 */
}

/* Sections
   ========================================================================== */

/**
 * Remove the margin in all browsers.
 */

body {
  margin: 0;
}

/* Grouping content
   ========================================================================== */

/**
 * 1. Add the correct box sizing in Firefox.
 * 2. Show the overflow in Edge and IE.
 */

hr {
  box-sizing: content-box; /* 1 */
  height: 0; /* 1 */
  overflow: visible; /* 2 */
}

pre {
  font-family: monospace, monospace; /* 1 */
  font-size: 1em; /* 2 */
}

/* Text-level semantics
   ========================================================================== */

/**
 * 1. Remove the bottom border in Chrome 57-
 * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
 */

abbr[title] {
  border-bottom: none; /* 1 */
  text-decoration: underline; /* 2 */
  text-decoration: underline dotted; /* 2 */
}

/**
 * Add the correct font weight in Chrome, Edge, and Safari.
 */

b,
strong {
  font-weight: bolder;
}

code,
kbd,
samp {
  font-family: monospace, monospace; /* 1 */
  font-size: 1em; /* 2 */
}

/**
 * Add the correct font size in all browsers.
 */

small {
  font-size: 80%;
}

sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

/* Forms
   ========================================================================== */

/**
 * 1. Change the font styles in all browsers.
 * 2. Remove the margin in Firefox and Safari.
 */

button,
input,
optgroup,
select,
textarea {
  font-family: inherit; /* 1 */
  font-size: 100%; /* 1 */
  line-height: 1.15; /* 1 */
  margin: 0; /* 2 */
}


/**
 * Remove the inheritance of text transform in Edge, Firefox, and IE.
 * 1. Remove the inheritance of text transform in Firefox.
 */

button,
select { /* 1 */
  text-transform: none;
}

/**
 * Correct the inability to style clickable types in iOS and Safari.
 */

button,
[type="button"],
[type="reset"],
[type="submit"] {
  -webkit-appearance: button;
}

/**
 * Remove the inner border and padding in Firefox.
 */

button::-moz-focus-inner,
[type="button"]::-moz-focus-inner,
[type="reset"]::-moz-focus-inner,
[type="submit"]::-moz-focus-inner {
  border-style: none;
  padding: 0;
}

/**
 * Restore the focus styles unset by the previous rule.
 */

button:-moz-focusring,
[type="button"]:-moz-focusring,
[type="reset"]:-moz-focusring,
[type="submit"]:-moz-focusring {
  outline: 1px dotted ButtonText;
}

/**
 * Correct the padding in Firefox.
 */

fieldset {
  padding: 0.35em 0.75em 0.625em;
}

legend {
  box-sizing: border-box; /* 1 */
  color: inherit; /* 2 */
  display: table; /* 1 */
  max-width: 100%; /* 1 */
  padding: 0; /* 3 */
  white-space: normal; /* 1 */
}

/**
 * Add the correct vertical alignment in Chrome, Firefox, and Opera.
 */

progress {
  vertical-align: baseline;
}

/**
 * Correct the cursor style of increment and decrement buttons in Chrome.
 */

[type="number"]::-webkit-inner-spin-button,
[type="number"]::-webkit-outer-spin-button {
  height: auto;
}

/**
 * 1. Correct the odd appearance in Chrome and Safari.
 * 2. Correct the outline style in Safari.
 */

[type="search"] {
  -webkit-appearance: textfield; /* 1 */
  outline-offset: -2px; /* 2 */
}

/**
 * Remove the inner padding in Chrome and Safari on macOS.
 */

[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}

::-webkit-file-upload-button {
  -webkit-appearance: button; /* 1 */
  font: inherit; /* 2 */
}

/* Interactive
   ========================================================================== */

/*
 * Add the correct display in Edge, IE 10+, and Firefox.
 */

details {
  display: block;
}

/*
 * Add the correct display in all browsers.
 */

summary {
  display: list-item;
}


 h1 {
   font-size: var(--heading1-fontsize);
   color: var(--heading1-color);
   font-weight: var(--heading1-weight);
   text-transform: none;
   font-family: var(--heading1-font-family); }
 
 h2 {
   font-size: var(--heading2-fontsize);
   color: var(--heading2-color);
   font-weight: var(--heading2-weight);
   font-family: var(--heading2-font-family);
   text-transform: none; }
 
 #container {
   margin: 0 auto;
   background: #fff;
   min-height: 100%;
   border: var(--container-border);
   border-top: none;
   border-bottom: none;
   box-shadow: none;
   position: relative;
   padding: 0;
   border-radius: none; }
 
 #main {
   width: var(--template-width);
   margin: 0;
   padding: 20px;
   box-sizing: border-box; }
 
 #logo {
   height: var(--logo-pip-height);
   margin: var(--logo-pip-margin);
   width: var(--logo-pip-width);
   text-indent: -9999px; }
   #logo a {
     display: block;
     width: var(--logo-pip-width);
     height: var(--logo-pip-height);
     background-image: url(/api/v2/style/get_content?name=logo-pip);
     background-repeat: no-repeat;
     background-position: left center;
     background-size: var(--logo-pip-bg-size); }
 
 header {
   height: var(--header-height);
   padding: var(--header-padding);
   -webkit-box-sizing: border-box;
   -moz-box-sizing: border-box;
   box-sizing: border-box;
   background-color: var(--header-color); }
 
 .webform_inner {
   position: static; }
 
 .webform .block-header .block-header-inner > div {
   line-height: var(--title-height); }
 
 .block-header {
   padding: var(--title-padding);
   color: var(--title-text-color);
   height: var(--title-height);
   display: table;
   vertical-align: middle;
   table-layout: fixed;
   width: 100%;
   background-color: var(--title-bg-color) !important;
   background-image: none;
   background-repeat: no-repeat;
   background-position: center center;
   font-family: var(--font-family);
   border-bottom: medium none color;
   border-radius: none; }
   .block-header h1 {
     line-height: var(--title-height);
     margin: 0;
     font-size: var(--title-font-size) !important;
     color: var(--title-text-color) !important;
     font-weight: var(--title-font-weight); }
 
 .block .block-header .header-actions {
   height: var(--title-height);
   line-height: var(--title-height); }
   .block .block-header .header-actions .button-secondary {
     border: none !important;
     background: none;
     color: var(--button-form-actions-color);
     padding: 2px 10px; }
     .block .block-header .header-actions .button-secondary:hover {
       color: var(--button-form-actions-color);
       background: rgba(255, 255, 255, 0.2); }
       .block .block-header .header-actions .button-secondary:hover i {
         text-decoration: none; }
       .block .block-header .header-actions .button-secondary:hover span {
         text-decoration: none; }
     .block .block-header .header-actions .button-secondary i {
       color: var(--button-form-actions-color);
       margin-right: 4px;
       font-size: 1.4rem; }
 
 .block.succes > .block-header {
   line-height: var(--title-height);
   padding: var(--title-padding);
   margin: 0;
   color: #fff; }
 
 .pip-nav {
   background-color: var(--pip-nav-background-color);
   background-image: none;
   border-bottom: none;
   font-weight: inherit;
   font-size: var(--pip-nav-font-size);
   font-family: var(--font-family); }
   .pip-nav ul li.current a, .pip-nav ul li a:hover {
     text-decoration: none; }
   .pip-nav ul li.current a {
     color: var(--pip-nav-anchor-color-active); }
   .pip-nav ul li.current a,
   .pip-nav ul li.current a:hover {
     background: var(--pip-nav-bg-active); }
   .pip-nav ul li a {
     color: var(--pip-nav-anchor-color);
     padding: 1em;
     font-weight: inherit; }
     .pip-nav ul li a:hover {
       background: var(--pip-nav-bg-hover);
       color: var(--pip-nav-anchor-color); }
   .pip-nav ul.right li a {
     filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50);
     opacity: 0.5; }
     .pip-nav ul.right li a:hover {
       filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=100);
       opacity: 1; }
 
 #main {
   padding: 0; }
 
 .block-content,
 .pip-content {
   padding: 20px; }
 
 body {
   font-family: var(--font-family);
   font-size: var(--font-size); }
 
 #container {
   width: var(--template-width);
   margin: 0px auto;
   background: #fff;
   -webkit-box-sizing: content-box;
   -moz-box-sizing: content-box;
   box-sizing: content-box; }
 
 .stap-titel {
   font-size: var(--heading2-fontsize);
   font-weight: 698; }
 
 .pip a {
   color: var(--anchor-color);
   text-decoration: var(--anchor-text-decoration); }
   .pip a:hover {
     color: var(--anchor-color-hover);
     text-decoration: var(--anchor-text-decoration-hover); }
 
 .pip-login .pip-login-block {
   border-radius: var(--border-radius); }
 
 .pip-case-progress .pip-case-progress-bar {
   background: var(--pip-progress-bar-bg); }
 
 .pip-case-progress .pip-case-progress-percentages {
   background: var(--primary-color); }
 
 .pip-case-form-phase-title {
   border-radius: var(--border-radius);
   background: var(--primary-color);
   border-color: var(--primary-color);
   color: #fff; }
   .pip-case-form-phase-title.pip-case-form-phase-active:hover {
     background: #0a0a0a;
     border-color: #0a0a0a; }
   .pip-case-form-phase-title.pip-case-form-phase-current {
     padding: 1.15em .8em 1.15em .7em;
     font-size: 1.3em;
     background: var(--primary-color); }
     .pip-case-form-phase-title.pip-case-form-phase-current .pip-case-form-phase-title-icon {
       margin-right: 5px; }
     .pip-case-form-phase-title.pip-case-form-phase-current:hover {
       background: #0a0a0a !important; }
 
 .form-navigation {
   padding: 0px;
   margin: var(--form-navigation-margin); }
   .form-navigation li {
     line-height: 1.6; }
   .form-navigation .stap {
     background: #eee; }
     .form-navigation .stap:after {
       border-color: rgba(233, 242, 244, 0);
       border-left-color: #eee; }
     .form-navigation .stap:before {
       border-color: rgba(143, 173, 182, 0);
       border-left-color: #fff; }
     .form-navigation .stap.done:after {
       border-color: rgba(233, 242, 244, 0);
       border-left-color: #eee; }
     .form-navigation .stap.active {
       background: var(--step-active-bg-color);
       color: white;
       font-weight: var(--step-active-anchor-font-weight); }
       .form-navigation .stap.active:after {
         border-color: rgba(233, 242, 244, 0);
         border-left-color: var(--step-active-bg-color); }
       .form-navigation .stap.active a {
         color: white;
         font-weight: var(--step-active-anchor-font-weight); }
 
 .pip .field-help .icon-question-sign,
 .field-help .icon-question-sign {
   color: var(--icon-question-sign-color);
   text-decoration: none; }
   .pip .field-help .icon-question-sign:hover,
   .field-help .icon-question-sign:hover {
     filter: brightness(0.85); }
 
 .pip-case-document-icon i {
   color: var(--primary-color); }
 
 a.button {
   text-decoration: none; }
   a.button:hover {
     text-decoration: none; }
 
 .button.button-id {
   border: 1px solid var(--button-id-bg-color);
   background: var(--button-id-bg-color);
   color: #fff;
   font-weight: normal; }
   .button.button-id:hover {
     background: black;
     border: 1px solid black; }
   .button.button-id:active {
     background: #282831;
     border: 1px solid #282831; }
 
 .pip-case-result {
   font-weight: bold;
   font-size: 1.5em;
   border: 2px solid var(--primary-color);
   padding: 12px 15px;
   margin: 20px 0 -10px; }
   .pip-case-result i {
     margin-right: 10px;
     margin-left: 5px;
     color: var(--primary-color); }
 
 .saml_error {
   -webkit-border-radius: 3px;
   -moz-border-radius: 3px;
   -ms-border-radius: 3px;
   -o-border-radius: 3px;
   border-radius: 3px;
   padding: 5px 10px 5px 35px;
   background: #e60000;
   color: white;
   position: relative; }
   .saml_error:before {
     content: "\f071";
     font-family: 'FontAwesome';
     position: absolute;
     left: 10px;
     font-size: 1.2em; }
 
 .id-knop {
   background: var(--button-id-bg-color);
   display: block;
   margin: 0 0 30px;
   border-radius: var(--border-radius);
   color: white !important;
   text-decoration: none;
   text-align: center;
   font-size: 1.385em;
   font-weight: normal;
   line-height: 1;
   text-transform: none;
   height: auto;
   border: 1px solid var(--button-id-bg-color);
   padding: 21px 0 20px;
   width: 100%; }
 
 .id-knop:hover {
   cursor: pointer;
   background: black;
   border-color: black; }
 
 .id-knop:active {
   border: 1px solid #282831;
   position: relative;
   top: 1px;
   background: #282831; }
 
 .id-knop-groot {
   line-height: 2;
   font-size: 1.5em; }
 
 .id-knop.id-knop-digid img {
   vertical-align: middle;
   display: inline-block; }
 
 .dropdown-pijl {
   display: none; }
 
 .pip-skip-area {
   left: -999px;
   position: absolute;
   top: auto;
   width: 1px;
   height: 1px;
   overflow: hidden;
   z-index: -999;
   border: none;
   background: none; }
   .pip-skip-area:focus, .pip-skip-area:active {
     position: relative;
     left: auto;
     top: auto;
     height: auto;
     width: auto;
     overflow: auto;
     z-index: 999; }
 
 @media only screen and (max-width: var(--template-width)) {
   #container {
     width: 100%; } }
 
 @media only screen and (max-width: 960px) {
   .pip-login .pip-login-block,
   #loginwrap .input_medium,
   .spot-enlighter-wrapper input[type=text],
   #container,
   #main,
   .pip-content {
     width: 100%; }
   #container,
   #main {
     border: none; }
   #main {
     padding: 12px;
     box-sizing: border-box; }
   input[type=text],
   input[type=password],
   input[type=email],
   input[type=url],
   input[type=number],
   textarea,
   select:focus {
     font-size: 16px; }
   .row .column,
   .row .columns {
     position: relative;
     padding-left: 0em;
     padding-right: 0em;
     width: 100% !important;
     float: left; }
   .row .row {
     margin-left: 0;
     margin-right: 0; }
   .column > label {
     font-weight: bold;
     margin-bottom: 5px;
     display: inline-block; }
   .row .large-offset-1 {
     margin: 0 !important; }
   .row .large-offset-2 {
     margin: 0 !important; }
   .row .large-offset-3 {
     margin: 0 !important; }
   .row .large-offset-4 {
     margin: 0 !important; }
   .row .large-offset-5 {
     margin: 0 !important; }
   .row .large-offset-6 {
     margin: 0 !important; }
   .row .large-offset-7 {
     margin: 0 !important; }
   .row .large-offset-8 {
     margin: 0 !important; }
   .row .large-offset-9 {
     margin: 0 !important; }
   .row .large-offset-10 {
     margin: 0 !important; }
   .row .large-offset-11 {
     margin: 0 !important; }
   .row .large-offset-12 {
     margin: 0 !important; }
   .kenmerk-veld > .column.large-4 {
     padding-bottom: 0; }
   .kenmerk-veld > .column.large-8 {
     padding-top: 0; }
   .block-header h1.header-title,
   .block-header h1 {
     padding-right: 0; }
   label.titel,
   .form label.titel {
     margin-bottom: 0px; }
   .form-navigation .stap {
     border-bottom-width: 1px;
     background-image: none !important;
     display: block;
     margin-right: 3px; }
   .block .block-header .header-actions .button-secondary .button-text {
     display: none; }
   .header h1.header-title {
     width: 100%;
     -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
     box-sizing: border-box;
     padding-right: 35px;
     text-overflow: ellipsis;
     overflow: hidden;
     white-space: nowrap; }
   .webform .block-header h1.header-title {
     padding-right: 68px; }
   .block .block-header .header-actions.right {
     float: none;
     position: absolute;
     right: 10px;
     flex-shrink: 0; }
   .block .block-header .header-actions .button-secondary i {
     margin: 0; }
   .subnote {
     padding-bottom: 20px;
     margin-bottom: 0px; }
     .subnote br {
       display: none; }
   .pip-login .pip-login-block.pip-login-block-left {
     margin: 0 0 15px; }
   .pip-nav {
     padding: 0; }
   .wym_skin_default .wym_buttons li.wym_tools_enlarge {
     display: none; }
   .dropdown {
     max-width: 280px; }
   .field-help {
     left: 0;
     padding: 0; }
   .qmatic-modal .modal-body {
     width: 100%;
     top: 0;
     bottom: 0;
     height: 100%; }
   .modal-content {
     position: absolute;
     top: 57px;
     left: 0;
     right: 0;
     bottom: 0; }
   .qmatic-modal-list-time {
     border: none;
     padding: 0; }
   .spinner-groot > div > span.spinner-groot-message {
     width: 100%; }
   .pip-overview-case-date-wrap,
   .pip-overview-case-finish-button-text {
     display: none; }
   .pip-overview-case-finish-button-text-2 {
     text-transform: capitalize; }
   .pip-nav-menu {
     display: none;
     opacity: 0;
     width: 100%; }
   .pip-nav-menu > li {
     display: block;
     width: 100%;
     margin: 0; }
   .pip-nav-menu > li > a {
     display: block;
     width: 100%;
     text-decoration: none;
     -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
     box-sizing: border-box; }
   .toggle-pip-nav {
     display: block;
     position: relative;
     cursor: pointer;
     -webkit-touch-callout: none;
     -webkit-user-select: none;
     user-select: none;
     padding: .7em 1em;
     vertical-align: middle; }
     .toggle-pip-nav i {
       margin-right: 5px;
       font-size: 1.6em;
       vertical-align: middle; }
   .toggle-pip-nav-text {
     display: inline-block;
     vertical-align: middle;
     font-size: 1.2em; }
   #toggle:checked + div .pip-nav-menu {
     display: block;
     opacity: 1; }
   #toggle:checked + div button {
     background: #08080f; }
   #toggle:checked + div button .mdi:before {
     content: "\\F156"; }
   .pip-case-form-phase-title-meta {
     float: none;
     margin-left: 5px;
     filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=60);
     opacity: 0.6;
     display: inline-block;
     vertical-align: middle; }
   .field-can-edit {
     padding: 0; }
   .field-has-required-permissions {
     position: static; }
   .attribute-change .attribute-change-edit-button {
     position: static;
     margin-top: 10px; }
   .mobile-hide {
     display: none; }
   .woz-objects {
     margin-bottom: 10px; }
   .woz .woz-taxatierapport-label,
   .woz .woz-taxatierapport-text-smal,
   .woz .woz-taxatierapport-text {
     width: 100%; }
   .woz .woz-taxatierapport-text-smal strong {
     font-weight: normal; }
   .woz .woz-taxatierapport-label {
     margin-bottom: 5px; }
   .woz .woz-taxatierapport-text-border {
     border-bottom: 1px solid #ddd;
     padding-bottom: .8em;
     margin-bottom: .8em; }
   .woz .woz-info {
     padding: 0; }
   .woz .woz-photo {
     position: static;
     margin: 20px 0;
     max-width: 100%;
     min-width: 100%;
     width: auto !important; }
   .pip-login .pip-login-block {
     padding: 20px; }
   .dropdown-content.dropdown-content-overflow {
     max-width: 250px; }
   .mintloader span.slepen {
     display: none; }
   .mintloader .button {
     margin-bottom: 3px; }
   .pip-case-document-state {
     position: static;
     margin-top: auto; }
   #main {
     padding: 0; }
   .block-content,
   .pip-content {
     padding: 12px; } }
 
 @media only screen and (max-width: 768px) {
   .form * select {
     width: 100%; } }
 
 .button-primary, .button-secondary {
   border: none;
   background-image: none; }
   .button-primary:hover, .button-primary:active, .button-primary.button-active, .button-primary[disabled='disabled']:hover, .button-primary[disabled='disabled']:active, .button-secondary:hover, .button-secondary:active, .button-secondary.button-active, .button-secondary[disabled='disabled']:hover, .button-secondary[disabled='disabled']:active {
     background-image: none;
     -webkit-box-shadow: compact(none, false, false, false, false, false, false, false, false, false);
     -moz-box-shadow: compact(none, false, false, false, false, false, false, false, false, false);
     box-shadow: compact(none, false, false, false, false, false, false, false, false, false); }
   .button-primary[disabled='disabled'], .button-secondary[disabled='disabled'] {
     background: none; }
 
 .button-secondary {
   border-radius: var(--border-radius);
   background: #fff;
   color: #000;
   border: 1px solid #cccccc;
   font-weight: normal;
   font-size: 1em;
   font-family: var(--font-family);
   border-radius: var(--button-radius); }
   .button-secondary:hover {
     background: #f2f2f2;
     color: #000;
     border: 1px solid #bfbfbf; }
   .button-secondary:active {
     background: white;
     color: #000;
     border: 1px solid #cccccc; }
   .button-secondary[disabled='disabled'] {
     background: #fff;
     border-color: #fff; }
 
 .button-primary {
   border-radius: var(--border-radius);
   border: 1px solid var(--button-primary-bg);
   background: var(--button-primary-bg);
   color: var(--button-primary-color);
   font-weight: normal;
   font-size: 1em;
   font-family: var(--font-family);
   border-radius: var(--button-radius); }
   .button-primary:hover {
     border: 1px solid var(--button-primary-bg-hover);
     background: var(--button-primary-bg-hover);
     color: var(--button-primary-color); }
   .button-primary:active {
     border: 1px solid #272733;
     background: #272733;
     color: var(--button-primary-color); }
   .button-primary[disabled='disabled'] {
     background: var(--button-primary-bg);
     border-color: var(--button-primary-bg); }

.gemeente_custom-styles .branding a {
  background-image: url(/api/v2/style/get_content?name=logo-login);
  background-size: var(--login-logo-size-mode);
}

.top-bar>h1.branding>a {
  background-image: url(/api/v2/style/get_content?name=logo-login);
  background-size: var(--login-logo-size-mode);
}

#pip-nav-close {
  color: var(--pip-nav-anchor-color)
}

.button.button-id {
  background-color: #1a1a1a
}

.button.button-id:hover {
  background-color: #010101
}
`;
