// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { createContext, useState } from 'react';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import { LOCALSTATE_KEY } from '../library/config';
import { LocalStateType } from '../AdvancedSearch.types';

export const localStateContext = createContext(null as any);
const defaultState: LocalStateType = {
  UIState: {
    searchesBarExpanded: false,
  },
};

const getDefaultState = () => {
  const current = localStorage.getItem(LOCALSTATE_KEY);
  if (current) {
    return JSON.parse(current);
  } else {
    localStorage.setItem(LOCALSTATE_KEY, JSON.stringify(defaultState));
    return defaultState;
  }
};

const LocalStateProvider = (props: any) => {
  const [localState, setlocalState] = useState<LocalStateType>(getDefaultState);

  useTransition(
    (previousState: LocalStateType) => {
      if (previousState && previousState !== localState) {
        localStorage.setItem(LOCALSTATE_KEY, JSON.stringify(localState));
      }
    },
    [localState]
  );

  return (
    <localStateContext.Provider value={[localState, setlocalState]}>
      {props.children}
    </localStateContext.Provider>
  );
};

export default LocalStateProvider;
