// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { permissionsStyles } from './permissions';
import { columnsStyles } from './columns';
import { editFormStyles } from './editForm';
import { mainStyles } from './main';
import { filtersStyles } from './filters';
import { leftBarStyles } from './leftBar';
import { infoDialogStyles } from './infoDialog';
import { summaryStyles } from './summary';

export const useStyles = makeStyles(
  // @ts-ignore
  ({
    palette: {
      elephant,
      primary,
      secondary,
      common,
      basalt,
      coral,
      cloud,
      error,
      northsea,
    },
    mintlab: { greyscale, shadows },
    typography,
    breakpoints,
  }: Theme) => {
    return {
      wrapper: {
        display: 'flex',
        flexFlow: 'column',
        flexDirection: 'column',
        overflow: 'hidden',
        height: '100%',
      },
      hide: {
        '&&': {
          display: 'none',
        },
      },
      show: {
        display: 'inherit',
      },
      content: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        overflow: 'hidden',
        position: 'relative',
      },
      mainContent: {
        padding: 20,
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        position: 'relative',
        overflow: 'hidden',
      },
      dividerTop: {
        borderBottom: 'none',
        borderTop: `1px solid ${greyscale.light}`,
      },
      noDivider: {
        border: 'none',
      },
      loadMore: {
        display: 'flex',
        margin: '20px 0px',
        justifyContent: 'center',
        alignItems: 'center',
      },
      deleteButton: {
        '&&': {
          color: primary.main,
          '&:hover': {
            color: primary.main,
          },
        },
      },
      expandCollapseButton: {
        '&&': {
          color: common.white,
          backgroundColor: primary.main,
        },
      },
      expandCollapseButtonWrapper: {
        position: 'absolute',
        bottom: 73,
        left: 401,
      },
      expandCollapseButtonWrapperExpanded: {
        left: 10,
      },
      helpIcon: {
        '& svg': {
          fill: elephant.light,
        },
      },
      ...mainStyles({ primary, typography, elephant, shadows }),
      ...leftBarStyles({
        typography,
        primary,
        basalt,
        cloud,
        northsea,
        common,
        elephant,
        greyscale,
      }),
      ...editFormStyles({
        primary,
        breakpoints,
        error,
        elephant,
        typography,
        greyscale,
        common,
      }),
      ...permissionsStyles({ greyscale, primary, elephant }),
      ...columnsStyles({ greyscale, elephant }),
      ...filtersStyles({ common, greyscale, typography, elephant, coral }),
      ...infoDialogStyles(),
      ...summaryStyles({ common, secondary, elephant }),
    };
  }
);
