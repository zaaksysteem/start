// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FILTERS_SUMMARY_MAX_HEIGHT } from '../library/config';

type SummaryStylesPropsType = Pick<
  Theme['palette'],
  'secondary' | 'common' | 'elephant'
>;
export const summaryStyles = ({
  secondary,
  common,
  elephant,
}: SummaryStylesPropsType) => {
  return {
    summaryWrapper: {
      border: `1px solid ${elephant.lighter}`,
      borderRadius: 4,
      fontSize: 13,
      padding: '12px 20px 4px 16px',
      cursor: 'pointer',
      height: 'auto',
    },
    summaryHeader: {
      display: 'flex',
      marginBottom: 8,
      '&>:nth-child(1)': {
        flex: 1,
        gap: 4,
        display: 'flex',
        alignItems: 'center',
      },
    },
    summaryEntriesWrapper: {
      position: 'relative',
    },
    summaryFilter: {
      display: 'flex',
      marginBottom: 14,
      alignItems: 'flex-start',
      gap: 6,
      '&>:nth-child(1)': {
        overflowWrap: 'anywhere',
        width: 168,
        display: 'flex',
        alignItems: 'flex-start',
        gap: 6,
        '&>:nth-child(2)': {
          paddingTop: 2,
        },
      },
      '&>:nth-child(2)': {
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        gap: 6,
      },
    },
    summaryValueWrapper: {
      display: 'flex',
      gap: 6,
      alignItems: 'center',
    },
    summaryValue: {
      padding: '2px 4px',
      borderRadius: 4,
      backgroundColor: secondary.main,
      color: common.black,
      fontSize: 12,
      fontWeight: 200,
    },
    summaryExpanded: {
      maxHeight: FILTERS_SUMMARY_MAX_HEIGHT,
      overflow: 'auto',
    },
    summaryShowMore: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      background: `linear-gradient(to bottom, transparent 50%, ${common.white} 100%)`,
      '&>:nth-child(1)': {
        position: 'absolute',
        top: FILTERS_SUMMARY_MAX_HEIGHT - 30,
        left: '50%',
      },
    },
  };
};
