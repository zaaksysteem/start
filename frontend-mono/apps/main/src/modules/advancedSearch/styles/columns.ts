// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const columnsStyles = ({ greyscale, elephant }: any) => {
  return {
    columnsColumnEntry: {
      padding: '4px 4px 4px 0px',
      border: `2px solid ${elephant.lightest}`,
      borderRadius: 4,
      display: 'flex',
      flexDirection: 'row' as 'row',
      marginBottom: 4,
      alignItems: 'center',
      backgroundColor: 'white',
    },
    columnsWrapper: {
      flexDirection: 'column',
      display: 'flex',
      width: '100%',
      gap: 16,
    },
    columnsMethodChoice: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      margin: 0,
      columnGap: 16,
    },
    columnsEntryDragIndicator: {
      color: greyscale.darker,
      marginRight: 6,
    },
    columnsEntryLabel: {
      width: 236,
      whiteSpace: 'normal',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    columnsEntryActionButtons: {
      flex: 1,
      display: 'flex',
      justifyContent: 'flex-end',
      gap: 6,
      '&>*': {
        width: 32,
      },
    },
    columnsSystemAttributeSelectors: {
      display: 'flex',
      width: `100%`,
      '&>:nth-child(1)': {
        width: '100%',
        marginRight: 16,
      },
    },
    columnsSingleSelectWrapper: {
      display: 'flex',
      justifyContent: 'flex-end',
      '&>:nth-child(1)': {
        width: '65%',
      },
    },
    columnsChoiceControls: {
      display: 'flex',
      flexDirection: 'column',
      gap: 16,
    },
  };
};
