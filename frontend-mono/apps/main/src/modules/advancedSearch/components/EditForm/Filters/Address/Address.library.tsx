// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { asArray } from '@mintlab/kitchen-sink/source';
import { FieldProps } from 'formik';
import { useDebouncedCallback } from 'use-debounce';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { buildUrl } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { hasValue } from '../../../../../../library/validation/value';
import { PDOK_LOOKUP_URL, PDOK_SUGGEST_URL } from '../../../../library/config';
import { AddressType } from './Address.types';

const ADDRESSFINDER_QUERYKEY = 'advancedSearch-addressFinder';
const DELAY = 400;
const MAX_ROWS = 100;

export const useAddressChoicesQuery = ({
  field,
  type,
  isMulti,
}: {
  field: FieldProps['field'];
  type: AddressType;
  isMulti: any;
}) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const enabled = Boolean(input && input?.length > 2);

  const data = useQuery(
    [ADDRESSFINDER_QUERYKEY, input, type],
    async ({ queryKey: [___, keyword] }) => {
      const result = await request(
        'GET',
        buildUrl(PDOK_SUGGEST_URL, {
          // eslint-disable-next-line id-length
          q: keyword,
          fq: type === 'nummeraanduiding' ? 'type:adres' : 'type:weg',
          rows: MAX_ROWS,
        })
      ).catch(openServerError);

      if (
        !result ||
        !result.response?.docs ||
        result.response.docs.length === 0
      )
        return [];

      return result.response.docs.map((result: any) => ({
        label: result?.weergavenaam || '',
        value: result?.id,
        type,
      }));
    },
    { enabled }
  );

  const [setInputDebounced] = useDebouncedCallback(
    async (val: any) => setInput(val),
    DELAY
  );

  const doPdokLookup = async (id: string) => {
    const result = await request(
      'GET',
      buildUrl(PDOK_LOOKUP_URL, {
        id,
      })
    ).catch(openServerError);
    return isPopulatedArray(result?.response?.docs)
      ? result?.response?.docs[0]
      : null;
  };

  /* eslint complexity: [2, 12] */
  const selectProps = {
    onInputChange: (ev: any, val: any) => setInputDebounced(val),
    onChange: async (event: React.ChangeEvent<any>) => {
      const {
        target: { value },
      } = event;
      let newValue = [];
      if (hasValue(value)) {
        for (const val of asArray(value)) {
          if (!val.id) {
            const doc = await doPdokLookup(val.value);
            if (!doc) return;
            newValue.push({
              ...val,
              id:
                val.type === 'nummeraanduiding'
                  ? `nummeraanduiding-${doc.nummeraanduiding_id || ''}`
                  : `openbareruimte-${doc?.openbareruimte_id || ''}`,
            });
          } else {
            newValue.push(val);
          }
        }
      }

      field.onChange({
        ...event,
        target: {
          ...event.target,
          value: newValue ? (isMulti ? newValue : newValue[0]) : null,
        },
      });
    },
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
    freeSolo: true,
  };

  return selectProps;
};
