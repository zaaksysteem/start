// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    wrapper: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    typesWrapper: {
      width: '100%',
      marginBottom: 12,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    label: {
      marginRight: 12,
    },
    typeSelect: {
      width: '50%',
    },
  };
});
