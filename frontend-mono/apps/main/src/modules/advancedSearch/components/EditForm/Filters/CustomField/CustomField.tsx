// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Address from '../../Filters/Address/Address';
import Options from '../Options/Options';
import {
  ValidateFuncType,
  ErrorType,
  KindType,
} from '../../../../AdvancedSearch.types';
import { CustomAttributeTypes } from '../../../../AdvancedSearch.types.filters';
import { FilterCommonPropsType } from '../Filters.types';
import { validateMinSelected, validate } from '../../../../library/validations';
import DateEntries from '../DateEntries/DateEntries';
import CreatableSelect from '../Select/CreatableSelect';
import Relationship from '../Relationship/Relationship';
import { getDateMode } from '../../../../library/config';
import NumberEntries from '../NumberEntries/NumberEntries';
import { getRegExpValidationType } from '../library/Filters.library';
import Document from '../Document/Document';
import { useStyles } from './CustomField.styles';

const TRANSLATION_BASE = 'editForm.fields.filters';

interface CustomFieldPropsType extends FilterCommonPropsType {
  type: CustomAttributeTypes['type'];
  placeholder?: string;
  validate?: ValidateFuncType<any> | undefined;
  kind: KindType;
  [key: string]: any;
}

/* eslint complexity: [2, 10] */
const CustomField: FunctionComponent<CustomFieldPropsType> = props => {
  const classes = useStyles();
  const { identifier, t, name, formik, index, type, filter, kind } = props;
  const commonProps = {
    identifier,
    t,
    name,
    formik,
    index,
    filter,
    classes,
  };

  const noMsg = t(`${TRANSLATION_BASE}.typeNotSupported`, {
    type,
  }) as string;

  /* eslint complexity: [2, 12] */
  const getField = () => {
    if (
      kind === 'custom_object' ||
      (kind === 'case' &&
        [
          'email',
          'numeric',
          'bankaccount',
          'text',
          'textarea',
          'richtext',
        ].includes(type))
    ) {
      const validationType = getRegExpValidationType(type);
      const translations: { [key: string]: string } = t(
        `${TRANSLATION_BASE}.types.${validationType}`,
        {
          returnObjects: true,
        }
      );

      return (
        <CreatableSelect
          {...commonProps}
          placeholder={translations.placeholder}
          validate={(values: ValueType<string>[]) => {
            const validMinSelected = validateMinSelected(
              commonProps.identifier,
              t(`${TRANSLATION_BASE}.fields.generic.multiString.errorMessage`)
            )(values);
            const regExpValidation = (values || []).reduce<null | ErrorType>(
              (acc, current) => {
                const regexpTest = validate(
                  commonProps.identifier,
                  translations.errorMessage,
                  validationType
                )(current.value);
                if (regexpTest) acc = regexpTest;
                return acc;
              },
              null
            );

            return validMinSelected || regExpValidation || undefined;
          }}
        />
      );
    } else if (
      [
        'bag_adres',
        'bag_adressen',
        'bag_straat_adres',
        'bag_straat_adressen',
        'bag_openbareruimte',
        'bag_openbareruimtes',
        'address_v2',
      ].includes(type)
    ) {
      const addressType =
        type.indexOf('openbareruimte') > -1
          ? 'openbareruimte'
          : 'nummeraanduiding';

      return (
        <Address
          {...commonProps}
          addressType={addressType}
          isMulti={true}
          isClearable={true}
          placeholder={t(
            `${TRANSLATION_BASE}.fields.generic.location.placeholder`
          )}
          validate={(value: any) =>
            validateMinSelected(
              commonProps.identifier,
              t(`${TRANSLATION_BASE}.fields.generic.location.errorMessage`)
            )(value)
          }
        />
      );
    } else if (['checkbox', 'option', 'select'].includes(type)) {
      return <Options {...commonProps} name={name} />;
    } else if (['relationship'].includes(type)) {
      return <Relationship {...commonProps} name={name} />;
    } else if (['date'].includes(type)) {
      return <DateEntries {...commonProps} mode={getDateMode(type)} />;
    } else if (type.indexOf('valuta') > -1) {
      return <NumberEntries {...commonProps} currency={true} />;
    } else if (['file'].includes(type)) {
      return <Document {...commonProps} classes={classes} />;
    } else {
      return noMsg;
    }
  };

  return (
    <div className={classes.wrapper}>
      <div>{getField()}</div>
    </div>
  );
};

export default CustomField;
