// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, FunctionComponent } from 'react';
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
import { get } from '@mintlab/kitchen-sink/source';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Contacts from '../Contacts/Contacts';
import {
  IdentifierType,
  EditFormStateType,
} from '../../../../AdvancedSearch.types';
import { FilterCustomAttributeType } from '../../../../AdvancedSearch.types.filters';
import { useAttributeDetailsQuery } from '../../../../query/useAttributeDetails';
import CustomObject from './CustomObject';

type RelationshipPropsType = {
  formik: FormikProps<EditFormStateType>;
  identifier: IdentifierType;
  name: string;
  t: i18next.TFunction;
  index: number;
  classes: any;
};

const TRANSLATION_BASE = 'editForm.fields.filters.fields.relation';

const Relationship: FunctionComponent<RelationshipPropsType> = props => {
  const { formik, name, identifier, t, index, classes } = props;
  const parentName = name.split('.').slice(0, -1).join('.');
  const filter = get(
    formik.values,
    parentName
  ) as FilterCustomAttributeType['values'];
  const { attributeUuid } = filter;
  const attributeDetails = useAttributeDetailsQuery(attributeUuid, {
    staleTime: 0,
  });
  const relData = attributeDetails?.data?.attributes?.relationship_data;

  useEffect(() => {
    if (relData) {
      formik.setFieldValue(parentName, {
        ...filter,
        additionalData: relData,
      });
    }
  }, [attributeDetails.data]);

  if (!attributeDetails.data) return <Loader />;

  if (relData.type === 'custom_object') {
    return <CustomObject objectTypeName={relData.name} {...props} />;
  } else if (relData.type === 'subject') {
    return (
      <Contacts
        name={name}
        identifier={identifier}
        t={t}
        index={index}
        formik={formik}
        showContactTypes={true}
        translations={{
          'form:choose': t(`${TRANSLATION_BASE}.placeholder`),
          errorMessage: t(`${TRANSLATION_BASE}.errorMessage`),
        }}
      />
    );
  } else if (relData.type === 'case') {
    return (
      <div className={classes.text}>
        {t(`${TRANSLATION_BASE}.caseNotSupported`) as string}
      </div>
    );
  }
};

export default Relationship;
