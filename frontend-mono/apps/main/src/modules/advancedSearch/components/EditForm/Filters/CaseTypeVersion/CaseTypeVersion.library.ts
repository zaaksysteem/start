// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import * as i18next from 'i18next';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { CaseTypeVersionValueType } from '../../../../AdvancedSearch.types.filters';
import { TRANSLATION_BASE } from './CaseTypeVersion';

export const getVersions = async (uuid: string) => {
  const results = await request(
    'GET',
    buildUrl('/api/v2/admin/catalog/get_case_type_history', {
      case_type_id: uuid,
    })
  );

  if (!results || !results.data || !results.data?.length) return [];

  return results.data
    .map((result: any) => {
      return {
        casetype_uuid: result.case_type_id,
        casetype_name: result.attributes.name,
        casetype_version_number: result.attributes.version,
        casetype_version_date: result.attributes.last_modified,
        casetype_version_uuid: result.attributes.id,
      } as CaseTypeVersionValueType;
    })
    .sort(
      (sortA: CaseTypeVersionValueType, sortB: CaseTypeVersionValueType) =>
        sortB.casetype_version_number - sortA.casetype_version_number
    );
};

export const getVersionChoices = (
  versions: CaseTypeVersionValueType[],
  t: i18next.TFunction
) =>
  versions && versions.length
    ? versions.map(version => ({
        label: `${t(`${TRANSLATION_BASE}.versionShort`)}${
          version.casetype_version_number
        }, ${fecha.format(
          new Date(version.casetype_version_date),
          t('common:dates.dateAndTimeFormat')
        )}`,
        value: version.casetype_version_uuid,
      }))
    : [];
