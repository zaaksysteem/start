// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import * as i18next from 'i18next';
import { ColumnType } from '../../../AdvancedSearch.types';
import { getCategories, getDefaultColumns } from '../EditForm.library';

export const mapColumnToChoice = (column: ColumnType): ValueType<string> => ({
  value: column.type,
  label: column.label,
  //@ts-ignore
  data: column,
});

export const getAttributesChoices = (t: i18next.TFunction) => {
  const defaults = getDefaultColumns('case', t);
  const categories = getCategories('case', t);
  const findCategory = (findCategory: string) => {
    const category = Object.entries(categories).filter(([categoryKey, val]) => {
      return val.includes(findCategory);
    });
    return category ? category[0][0] : '';
  };

  return defaults.map(cat => ({
    value: cat.type,
    label: cat.label,
    data: cat,
    categoryLabel: t(
      `editForm.fields.columns.categories.${findCategory(cat.type)}`
    ),
  }));
};

export const handleOnChange =
  (handleSelectOnChange: (event: any) => void) =>
  (event: React.ChangeEvent<any>) => {
    const { data } = event.target.value;
    const { type, label, magicString } = data;

    if (magicString) {
      const columnObj: ColumnType = {
        uuid: v4(),
        type,
        label,
        source: ['attributes', 'custom_fields', magicString],
        magicString,
        visible: true,
      };
      handleSelectOnChange({
        target: {
          ...event.target,
          value: {
            ...event.target.value,
            data: columnObj,
          },
        },
      });
    } else {
      handleSelectOnChange(event);
    }
  };

export const filterOption =
  (columns: ColumnType[]) =>
  (option: ValueType<string, any>, input?: string) => {
    const columnPresent = columns.find(col => col.type === option.data.type);

    if (columnPresent) {
      return false;
    } else if (input && option.label) {
      //@ts-ignore
      return option.label.toLowerCase().indexOf(input.toLowerCase()) !== -1;
    }

    return true;
  };
