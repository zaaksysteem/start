// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { useTheme } from '@mui/material';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Field } from 'formik';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { getFiltersChoices } from '../Filters/library/Filters.library';
import { useCustomFields } from '../../../query/useCustomFields';
import { useAttributeChoicesQuery } from '../components/AttributeFinder/AttributeFinder.library';
import { editFormSx } from '../../../styles/editForm';
import { CustomFieldType } from '../../../AdvancedSearch.types';
import {
  FiltersSelectPropsType,
  FiltersSelectCmpPropsType,
} from './FiltersSelect.types';
import { handleOnChange, filterOptionFunc } from './FiltersSelect.library';

const TRANSLATION_BASE = 'editForm.fields.filtersSelect';

const FiltersSelectCmp: FunctionComponent<FiltersSelectCmpPropsType> = ({
  arrayHelpersRef,
  choices,
  form,
  field,
  t,
  kind,
  inputRef,
}) => {
  const theme = useTheme<Theme>();
  const parsedChoices: (ValueType<any> & { categoryLabel: string })[] = [];
  let selectedObjectType;
  const selectProps = useAttributeChoicesQuery({
    config: {
      enabled: kind === 'case',
    },
  });

  if (kind === 'custom_object') {
    selectedObjectType = get(form, 'values.selectedObjectType.value') as string;
  }
  const customFields = useCustomFields({
    uuid: selectedObjectType || null,
    config: {
      enabled: kind === 'custom_object' && Boolean(selectedObjectType),
      staleTime: 1000,
    },
  });

  const { loading, onInputChange } = selectProps;
  const { values } = form;

  if (isPopulatedArray(choices)) {
    const push = choices.map(choice => {
      return {
        categoryLabel: t(`editForm.attributeCategories.systemAttributes`),
        ...choice,
      };
    });
    parsedChoices.push(...push);
  }
  if (kind === 'case') {
    if (isPopulatedArray(selectProps.choices)) {
      const push = selectProps.choices.map(choice => {
        return {
          categoryLabel: t(`editForm.attributeCategories.customAttributes`),
          ...choice,
        };
      });
      parsedChoices.push(...push);
    }
  } else if (kind === 'custom_object') {
    if (isPopulatedArray(customFields.data)) {
      const push = customFields.data.map((customField: CustomFieldType) => ({
        label: customField.name,
        value: customField.uuid,
        data: customField,
        categoryLabel: t('editForm.attributeCategories.customAttributes'),
        filterType: 'custom_attribute',
      }));
      parsedChoices.push(...push);
    }
  }

  return (
    <div style={{ width: '100%' }}>
      <Select
        {...field}
        variant="generic"
        choices={parsedChoices}
        isClearable={false}
        /* eslint complexity: [2, 15] */
        onChange={handleOnChange(arrayHelpersRef, form, field, kind)}
        loading={loading}
        onInputChange={onInputChange}
        filterOption={filterOptionFunc(values)}
        placeholder={t(`${TRANSLATION_BASE}.placeholder`)}
        freeSolo={true}
        groupBy={option => option.categoryLabel}
        sx={editFormSx().lightSelect}
        inputRef={inputRef}
        startAdornment={
          <Icon size="small" sx={{ color: theme.palette.elephant.light }}>
            {iconNames.search}
          </Icon>
        }
      />
    </div>
  );
};

export const FiltersSelect: FunctionComponent<FiltersSelectPropsType> = ({
  t,
  arrayHelpersRef,
  kind,
  inputRef,
}) => {
  if (!arrayHelpersRef) return null;
  return (
    <Field
      name="selectedFilter"
      component={FiltersSelectCmp}
      choices={getFiltersChoices({ t, kind })}
      arrayHelpersRef={arrayHelpersRef}
      t={t}
      kind={kind}
      inputRef={inputRef}
    />
  );
};

export default FiltersSelect;
