// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { AttributeFinderPropsType } from './AttributeFinder.types';
import AttributeFinderCustomObject from './AttributeFinder.CustomObject';
import AttributeFinderCase from './AttributeFinder.Case';

const AttributeFinder: FunctionComponent<AttributeFinderPropsType> = props =>
  props.kind === 'custom_object' ? (
    <AttributeFinderCustomObject {...props} />
  ) : (
    <AttributeFinderCase {...props} />
  );

export default AttributeFinder;
