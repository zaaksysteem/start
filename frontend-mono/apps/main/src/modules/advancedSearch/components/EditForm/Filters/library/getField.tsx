// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { FormikProps } from 'formik';
import { renderTagsWithIcon } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  EditFormStateType,
  KindType,
  ErrorType,
} from '../../../../AdvancedSearch.types';
import { FilterType } from '../../../../AdvancedSearch.types.filters';
import {
  validate,
  validateMinSelected,
  validateNumbers,
} from '../../../../library/validations';
import CreatableSelect from '../Select/CreatableSelect';
import DateEntries from '../DateEntries/DateEntries';
import SelectField from '../Select/Select';
import CustomField from '../CustomField/CustomField';
import Contacts from '../Contacts/Contacts';
import CaseType from '../CaseType/CaseType';
import Checkboxes from '../Checkboxes/Checkboxes';
import CaseTypeVersion from '../CaseTypeVersion/CaseTypeVersion';
import DepartmentRole from '../DepartmentRole/DepartmentRole';
import Address from '../Address/Address';
import NumberEntries from '../NumberEntries/NumberEntries';
import { GenericText } from '../Generic/Generic';
import { FilterCommonPropsType } from '../Filters.types';
import { getDateMode } from '../../../../library/config';
import { mapTranslationToChoice } from '../../../../library/library';
import Department from '../Department/Department';
import {
  hasNoValue,
  getChoicesFromTranslation,
  getRegExpValidationType,
} from './Filters.library';

const TRANSLATION_BASE = 'editForm.fields.filters';

// eslint-disable-next-line complexity
export const getField = ({
  t,
  filter,
  index,
  name,
  formik,
  kind,
  disabled = false,
}: {
  t: i18next.TFunction;
  filter: FilterType;
  index: number;
  name: string;
  formik: FormikProps<EditFormStateType>;
  kind: KindType;
  disabled: boolean;
}) => {
  if (!filter) return null;
  const { type, uuid, values } = filter;

  const commonProps: FilterCommonPropsType = {
    name,
    identifier: filter.uuid,
    t,
    index,
    formik,
    filter,
  };

  switch (type) {
    case 'attributes.last_modified':
    case 'attributes.completion_date':
    case 'attributes.registration_date':
    case 'attributes.target_date':
    case 'attributes.stalled_since':
    case 'attributes.stalled_until':
    case 'attributes.destruction_date':
    case 'relationship.requestor.date_of_birth':
    case 'relationship.requestor.date_of_marriage':
    case 'relationship.requestor.date_of_death': {
      return <DateEntries {...commonProps} mode={getDateMode(type)} />;
    }
    case 'keyword':
    case 'attributes.case_phase':
    case 'attributes.subject':
    case 'attributes.custom_number':
    case 'relationship.case_type.identification':
    case 'attributes.external_reference':
    case 'relationship.requestor.zipcode':
    case 'relationship.requestor.coc_number':
    case 'relationship.requestor.coc_location_number':
    case 'relationship.requestor.noble_title':
    case 'relationship.requestor.trade_name':
    case 'relationship.requestor.correspondence_zipcode':
    case 'relationship.requestor.correspondence_street':
    case 'relationship.requestor.correspondence_city':
    case 'relationship.requestor.street':
    case 'relationship.custom_object.id':
    case 'relationship.requestor.city': {
      // @ts-ignore
      const validationType = getRegExpValidationType(type);
      const translations: { [key: string]: string } = t(
        `${TRANSLATION_BASE}.types.${validationType}`,
        {
          returnObjects: true,
        }
      );

      return (
        <CreatableSelect
          {...commonProps}
          placeholder={translations.placeholder}
          validate={(values: ValueType<string>[]) => {
            const validMinSelected = validateMinSelected(
              commonProps.identifier,
              t(`${TRANSLATION_BASE}.fields.generic.multiString.errorMessage`)
            )(values);

            const regExpValidation = isPopulatedArray(values)
              ? values.reduce<null | ErrorType>((acc, current) => {
                  const regexpTest = validate(
                    commonProps.identifier,
                    translations.errorMessage,
                    validationType
                  )(current.value);
                  if (regexpTest) acc = regexpTest;
                  return acc;
                }, null)
              : undefined;

            return validMinSelected || regExpValidation || undefined;
          }}
        />
      );
    }
    case 'attributes.parent_number':
    case 'attributes.case_number': {
      if (
        type === 'attributes.case_number' &&
        filter?.options?.truncate === true
      ) {
        const kindTranslation = t(
          `${TRANSLATION_BASE}.fields.caseNumber.caseNr`,
          {
            count: filter?.values?.length,
          }
        );
        const filterOnTranslation = t(
          `${TRANSLATION_BASE}.fields.generic.text.filterOn`
        );

        return (
          <GenericText
            text={`${filterOnTranslation} ${filter?.values?.length} ${kindTranslation}.`}
          />
        );
      } else {
        return (
          <CreatableSelect
            {...commonProps}
            valuesTransformFunction={(values: any) =>
              values.map((thisValue: any) => ({
                ...thisValue,
                value: Number.isFinite(Number(thisValue.value))
                  ? Number(thisValue.value)
                  : thisValue.value,
              }))
            }
            placeholder={
              t(
                `${TRANSLATION_BASE}.fields.generic.number.placeholder`
              ) as string
            }
            validate={(value: any) => {
              const validMinSelected = validateMinSelected(
                commonProps.identifier,
                t(`${TRANSLATION_BASE}.fields.generic.number.errorMessageMin`)
              )(value);
              const validNumbers = validateNumbers(
                commonProps.identifier,
                t(`${TRANSLATION_BASE}.fields.generic.number.errorMessageNrs`)
              )(value);
              return validMinSelected || validNumbers || undefined;
            }}
          />
        );
      }
    }
    case 'relationship.case_type.version': {
      return <CaseTypeVersion {...commonProps} />;
    }
    case 'attributes.case_price': {
      return <NumberEntries {...commonProps} />;
    }
    case 'relationship.case_type.confidentiality': {
      return (
        <Checkboxes
          {...commonProps}
          choices={Object.entries(
            t(`${TRANSLATION_BASE}.fields.casetypeConfidentiality.choices`, {
              returnObjects: true,
            })
          ).map(mapTranslationToChoice)}
          validate={value =>
            !isPopulatedArray(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.types.checkboxes.minEntriesErrorMessage`
                  ) as string,
                  uuid: filter.uuid,
                }
              : undefined
          }
        />
      );
    }
    case 'attributes.status': {
      const choices =
        kind === 'custom_object'
          ? ['active', 'inactive', 'draft'].map((entry: string) => ({
              label: t(
                `${TRANSLATION_BASE}.fields.status.choices.${entry}`
              ) as string,
              value: entry,
            }))
          : ['new', 'open', 'stalled', 'resolved'].map((entry: string) => ({
              label: t(`case.status.${entry}`) as string,
              value: entry,
            }));

      return (
        <Checkboxes
          {...commonProps}
          disabled={disabled}
          choices={choices}
          validate={value =>
            !isPopulatedArray(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.fields.status.errorMessage`
                  ) as string,
                  uuid: filter.uuid,
                }
              : undefined
          }
        />
      );
    }
    case 'attributes.archive_status': {
      const choices = Object.entries(
        t(`${TRANSLATION_BASE}.fields.archiveStatus.choices`, {
          returnObjects: true,
        })
      ).map(mapTranslationToChoice);
      return (
        <SelectField
          {...commonProps}
          choices={choices}
          validate={value =>
            hasNoValue(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.fields.archiveStatus.errorMessage`
                  ),
                  uuid,
                }
              : undefined
          }
          placeholder={t(
            `${TRANSLATION_BASE}.fields.archiveStatus.placeholder`
          )}
          inputProps={{ readOnly: true }}
        />
      );
    }
    case 'attributes.type_of_archiving': {
      const choices = Object.entries(
        t(`${TRANSLATION_BASE}.fields.typeOfArchiving.choices`, {
          returnObjects: true,
        })
      ).map(mapTranslationToChoice);
      return (
        <SelectField
          {...commonProps}
          choices={choices}
          inputProps={{ readOnly: true }}
        />
      );
    }
    case 'relationship.requestor.investigation':
    case 'relationship.requestor.is_secret': {
      const choices = Object.entries(
        t(`${TRANSLATION_BASE}.fields.generic.yesNo.choices`, {
          returnObjects: true,
        })
      ).map(mapTranslationToChoice);
      return (
        <SelectField
          {...commonProps}
          choices={choices}
          nestedValue={true}
          inputProps={{ readOnly: true }}
        />
      );
    }
    case 'attributes.archival_state': {
      const choices = Object.entries(
        t(`${TRANSLATION_BASE}.fields.archivalState.choices`, {
          returnObjects: true,
        })
      ).map(mapTranslationToChoice);

      return (
        <SelectField
          {...commonProps}
          disabled={disabled}
          choices={choices}
          validate={value =>
            !value
              ? {
                  uuid,
                  value: t(
                    `${TRANSLATION_BASE}.fields.archivalState.errorMessage`
                  ),
                }
              : undefined
          }
          placeholder={t(
            `${TRANSLATION_BASE}.fields.archivalState.placeholder`
          )}
          nestedValue={true}
          inputProps={{ readOnly: true }}
        />
      );
    }
    case 'relationship.assignee.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={false}
          translations={{
            'form:choose': t(`${TRANSLATION_BASE}.fields.assignee.placeholder`),
            errorMessage: t(`${TRANSLATION_BASE}.fields.assignee.errorMessage`),
          }}
        />
      );
    }
    case 'relationship.requestor.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={true}
          translations={{
            'form:choose': t(
              `${TRANSLATION_BASE}.fields.requestor.placeholder`
            ),
            errorMessage: t(
              `${TRANSLATION_BASE}.fields.requestor.errorMessage`
            ),
          }}
        />
      );
    }
    case 'relationship.coordinator.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={false}
          showActiveChoice={true}
          translations={{
            'form:choose': t(
              `${TRANSLATION_BASE}.fields.coordinator.placeholder`
            ),
            errorMessage: t(
              `${TRANSLATION_BASE}.fields.coordinator.errorMessage`
            ),
          }}
        />
      );
    }
    case 'attributes.urgency': {
      return (
        <SelectField
          {...commonProps}
          inputProps={{ readOnly: true }}
          choices={['normal', 'medium', 'high', 'late'].map(value => ({
            label: t(
              `${TRANSLATION_BASE}.fields.urgency.choices.${value}`
            ) as string,
            value,
          }))}
        />
      );
    }
    case 'relationship.case_type.id': {
      return <CaseType {...commonProps} />;
    }
    case 'attributes.payment_status': {
      const paymentStatusObjects = t(
        `${TRANSLATION_BASE}.fields.paymentStatus.choices`,
        {
          returnObjects: true,
        }
      );
      return (
        <Checkboxes
          {...commonProps}
          choices={Object.entries(paymentStatusObjects).map(
            mapTranslationToChoice
          )}
          validate={value =>
            !isPopulatedArray(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.fields.paymentStatus.errorMessage`
                  ) as string,
                  uuid: filter.uuid,
                }
              : undefined
          }
        />
      );
    }
    case 'attributes.department_role': {
      return <DepartmentRole {...commonProps} />;
    }
    case 'attributes.case_location':
    case 'attributes.correspondence_address': {
      return (
        <Address
          {...commonProps}
          placeholder={
            t(
              `${TRANSLATION_BASE}.fields.generic.location.placeholder`
            ) as string
          }
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.generic.location.errorMessage`)
          )}
          isClearable={false}
        />
      );
    }
    case 'attributes.channel_of_contact': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'contactChannel')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.contactChannel.errorMessage`)
          )}
        />
      );
    }
    case 'attributes.result': {
      const resultObjects = t(`${TRANSLATION_BASE}.fields.result.choices`, {
        returnObjects: true,
      });
      return (
        <SelectField
          {...commonProps}
          choices={Object.entries(resultObjects).map(mapTranslationToChoice)}
          isMulti={true}
          placeholder={t(`${TRANSLATION_BASE}.fields.result.placeholder`)}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.result.errorMessage`)
          )}
          renderTags={renderTagsWithIcon('check_circle')}
          inputProps={{ readOnly: true }}
        />
      );
    }
    case 'attributes.confidentiality': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'confidentiality')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.confidentiality.errorMessage`)
          )}
        />
      );
    }
    case 'attributes.has_unaccepted_changes': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'hasUnacceptedChanges')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.hasUnacceptedChanges.errorMessage`)
          )}
        />
      );
    }
    case 'attributes.period_of_preservation_active': {
      return (
        <SelectField
          {...commonProps}
          inputProps={{ readOnly: true }}
          choices={['active', 'not-active'].map((entry: string) => ({
            label: t(
              `${TRANSLATION_BASE}.fields.periodOfPreservation.choices.${entry}`
            ) as string,
            value: entry,
          }))}
        />
      );
    }
    case 'attributes.retention_period_source_date': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(
            t,
            'retentionPeriodSourceDate'
          ).sort((optionA, optionB) =>
            optionA.label.localeCompare(optionB.label)
          )}
          validate={validateMinSelected(
            commonProps.identifier,
            t(
              `${TRANSLATION_BASE}.fields.retentionPeriodSourceDate.errorMessage`
            )
          )}
        />
      );
    }
    case 'relationship.requestor.gender': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'requestorGender')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.generic.multiOption.errorMessageMin`)
          )}
        />
      );
    }
    case 'relationship.requestor.department': {
      return (
        <Department
          {...commonProps}
          isMulti={true}
          placeholder={t(
            `${TRANSLATION_BASE}.fields.requestorDepartment.placeholder`
          )}
          validate={validateMinSelected(
            commonProps.identifier,
            t(
              `${TRANSLATION_BASE}.fields.requestorDepartment.minEntriesErrorMessage`
            )
          )}
          filterSelectedOptions={false}
        />
      );
    }
    case 'attributes.value': {
      const { type } = values;
      return <CustomField {...commonProps} type={type} kind={kind} />;
    }
    default:
      return null;
  }
};
