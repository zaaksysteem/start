// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import TextField from '@mintlab/ui/App/Material/TextField';
import { makeStyles } from '@mui/styles';
import { Field } from 'formik';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';

interface FilterTextPropsType extends FilterCommonPropsType {
  placeholder?: string;
  validate?: ValidateFuncType<any>;
}

const TextFieldPassThrough = (props: any) => {
  const { field } = props;
  return <TextField {...props} {...field} />;
};

const Text: FunctionComponent<FilterTextPropsType> = ({
  name,
  placeholder,
  validate,
  ...rest
}) => {
  const theseStyles = useStyles();
  return (
    <div className={theseStyles.wrapper}>
      <Field
        name={name}
        component={TextFieldPassThrough}
        {...(validate && { validate })}
        {...(placeholder && { placeholder })}
        {...rest}
      />
    </div>
  );
};

const useStyles = makeStyles(({ mintlab: { greyscale } }: any) => {
  return {
    wrapper: {
      width: '100%',
      backgroundColor: greyscale.light,
    },
  };
});

export default Text;
