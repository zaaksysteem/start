// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { KindType } from '../../../../AdvancedSearch.types';
import {
  FilterType,
  FilterTypesTypeCombined,
} from '../../../../AdvancedSearch.types.filters';
import HelpListing from '../../components/HelpListing';
import { isCustomAttribute } from '../../../../library/library';

// to Select choices
export type OptionType = {
  label: string;
  value: FilterType['type'];
};

const TRANSLATION_BASE = 'editForm.fields.filters';

export const hasNoValue = (value: any) =>
  typeof value === 'undefined' ||
  value === null ||
  JSON.stringify(value) === '{}' ||
  value === '';

export const getOperatorOptions = (t: i18next.TFunction, options: string[]) =>
  options.map(option => ({
    label: t(`${TRANSLATION_BASE}.operators.${option}`),
    value: option,
  }));

export const getHelp = ({
  filter,
  kind,
  classes,
  t,
}: {
  filter: FilterType;
  kind: KindType;
  classes: any;
  t: i18next.TFunction;
}) => {
  const { type } = filter;

  const getContents = () => {
    if (type === 'attributes.case_phase') {
      return (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}.help.phase.help`)}
        />
      );
    } else if (type === 'keyword') {
      return kind === 'case' ? (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}.help.keywordCase.intro`)}
          list={[
            t(`${TRANSLATION_BASE}.help.keywordCase.requestor`),
            t(`${TRANSLATION_BASE}.help.keywordCase.recipient`),
            t(`${TRANSLATION_BASE}.help.keywordCase.assignee`),
            t(`${TRANSLATION_BASE}.help.keywordCase.other`),
          ]}
        />
      ) : (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}.help.keywordCustomObject.intro`)}
          list={[t(`${TRANSLATION_BASE}.help.keywordCustomObject.fields`)]}
        />
      );
    } else if (type === 'attributes.urgency') {
      return (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}.help.urgency.intro`)}
          list={[
            t(`${TRANSLATION_BASE}.help.urgency.normal`),
            t(`${TRANSLATION_BASE}.help.urgency.medium`),
            t(`${TRANSLATION_BASE}.help.urgency.high`),
            t(`${TRANSLATION_BASE}.help.urgency.late`),
          ]}
        />
      );
    } else if (type === 'relationship.custom_object.id') {
      return (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}.help.objectUUID.help`)}
        />
      );
    }
    return null;
  };

  const helpContents = getContents();
  return helpContents ? (
    <div className={classes.filterOperatorHelp}>
      <Tooltip enterDelay={200} title={helpContents}>
        <Icon
          size="extraSmall"
          color="inherit"
          classes={{ wrapper: classes.helpIcon }}
        >
          {iconNames.help_outline}
        </Icon>
      </Tooltip>
    </div>
  ) : null;
};

// Determines which filters are allowed to be added
export const getFiltersChoices = ({
  t,
  kind,
}: {
  t: i18next.TFunction;
  kind: KindType;
}): OptionType[] => {
  let options: [string, FilterType['type']][];

  const subcategoryTrCase = t(`attributes:subcategories.case`);
  const subcategoryTrCaseType = t('attributes:subcategories.caseType');
  const subcategoryTrRequestor = t('attributes:subcategories.requestor');
  const subcategoryTrObject = t('attributes:subcategories.object');

  if (kind === 'custom_object') {
    options = [
      [t(`${TRANSLATION_BASE}.fields.keyword.label`), 'keyword'],
      [
        t(`${TRANSLATION_BASE}.fields.modified.label`),
        'attributes.last_modified',
      ],
      [t(`${TRANSLATION_BASE}.fields.status.label`), 'attributes.status'],
      [
        t(`${TRANSLATION_BASE}.fields.archiveStatus.label`),
        'attributes.archive_status',
      ],
    ];
  } else if (kind === 'case') {
    options = [
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.keyword.label`
        )}`,
        'keyword',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.requestor.label`
        )}`,
        'relationship.requestor.id',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.assignee.label`
        )}`,
        'relationship.assignee.id',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.coordinator.label`
        )}`,
        'relationship.coordinator.id',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.urgency.label`
        )}`,
        'attributes.urgency',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.archivalState.label`
        )}`,
        'attributes.archival_state',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.typeOfArchiving.label`
        )}`,
        'attributes.type_of_archiving',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.caseType.label`
        )}`,
        'relationship.case_type.id',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.paymentStatus.label`
        )}`,
        'attributes.payment_status',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.departmentRole.label`
        )}`,
        'attributes.department_role',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.caseLocation.label`
        )}`,
        'attributes.case_location',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.contactChannel.label`
        )}`,
        'attributes.channel_of_contact',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.result.label`
        )}`,
        'attributes.result',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.registrationDate.label`
        )}`,
        'attributes.registration_date',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.completionDate.label`
        )}`,
        'attributes.completion_date',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.destructionDate.label`
        )}`,
        'attributes.destruction_date',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.targetDate.label`
        )}`,
        'attributes.target_date',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.stalledUntil.label`
        )}`,
        'attributes.stalled_until',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.stalledSince.label`
        )}`,
        'attributes.stalled_since',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.confidentiality.label`
        )}`,
        'attributes.confidentiality',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.hasUnacceptedChanges.label`
        )}`,
        'attributes.has_unaccepted_changes',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.periodOfPreservation.label`
        )}`,
        'attributes.period_of_preservation_active',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.status.label`
        )}`,
        'attributes.status',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.retentionPeriodSourceDate.label`
        )}`,
        'attributes.retention_period_source_date',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.parentNumber.label`
        )}`,
        'attributes.parent_number',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.customNumber.label`
        )}`,
        'attributes.custom_number',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.externalReference.label`
        )}`,
        'attributes.external_reference',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.caseNumber.label`
        )}`,
        'attributes.case_number',
      ],
      [
        `${subcategoryTrCase} - ${t(`${TRANSLATION_BASE}.fields.phase.label`)}`,
        'attributes.case_phase',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.subject.label`
        )}`,
        'attributes.subject',
      ],
      [
        `${subcategoryTrCase} - ${t(`${TRANSLATION_BASE}.fields.price.label`)}`,
        'attributes.case_price',
      ],
      [
        `${subcategoryTrCase} - ${t(
          `${TRANSLATION_BASE}.fields.correspondenceAddress.label`
        )}`,
        'attributes.correspondence_address',
      ],
      [
        `${subcategoryTrObject} - ${t(
          `${TRANSLATION_BASE}.fields.objectUUID.label`
        )}`,
        'relationship.custom_object.id',
      ],
      [
        `${subcategoryTrCaseType} - ${t(
          `${TRANSLATION_BASE}.fields.casetypeVersion.label`
        )}`,
        'relationship.case_type.version',
      ],
      [
        `${subcategoryTrCaseType} - ${t(
          `${TRANSLATION_BASE}.fields.casetypeIdentification.label`
        )}`,
        'relationship.case_type.identification',
      ],
      [
        `${subcategoryTrCaseType} - ${t(
          `${TRANSLATION_BASE}.fields.casetypeConfidentiality.label`
        )}`,
        'relationship.case_type.confidentiality',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorZipcode.label`
        )}`,
        'relationship.requestor.zipcode',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCorrespondenceZipcode.label`
        )}`,
        'relationship.requestor.correspondence_zipcode',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCocNumber.label`
        )}`,
        'relationship.requestor.coc_number',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorInvestigation.label`
        )}`,
        'relationship.requestor.investigation',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorIsSecret.label`
        )}`,
        'relationship.requestor.is_secret',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorNobleTitle.label`
        )}`,
        'relationship.requestor.noble_title',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCocLocationNumber.label`
        )}`,
        'relationship.requestor.coc_location_number',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorTradeName.label`
        )}`,
        'relationship.requestor.trade_name',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorGender.label`
        )}`,
        'relationship.requestor.gender',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorDepartment.label`
        )}`,
        'relationship.requestor.department',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCorrespondenceStreet.label`
        )}`,
        'relationship.requestor.correspondence_street',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCorrespondenceCity.label`
        )}`,
        'relationship.requestor.correspondence_city',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorStreet.label`
        )}`,
        'relationship.requestor.street',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorCity.label`
        )}`,
        'relationship.requestor.city',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorDateOfBirth.label`
        )}`,
        'relationship.requestor.date_of_birth',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorDateOfMarriage.label`
        )}`,
        'relationship.requestor.date_of_marriage',
      ],
      [
        `${subcategoryTrRequestor} - ${t(
          `${TRANSLATION_BASE}.fields.requestorDateOfDeath.label`
        )}`,
        'relationship.requestor.date_of_death',
      ],
    ];
  } else {
    options = [];
  }

  const defaultOptions = options
    .sort((optionA: string[], optionB: string[]) =>
      optionA[0].localeCompare(optionB[0])
    )
    .map(entry => ({
      label: entry[0],
      value: entry[1] as any,
    }));

  return [...defaultOptions];
};

/* eslint complexity: [2, 10] */
export const getEntryPoint = (filter: FilterType, baseName: string) =>
  isCustomAttribute(filter)
    ? `${baseName}.values.values`
    : `${baseName}.values`;

export const getChoicesFromTranslation = (
  t: i18next.TFunction,
  fieldName: string
) => {
  const objects = t(`${TRANSLATION_BASE}.fields.${fieldName}.choices`, {
    returnObjects: true,
  });

  return Object.entries(objects).map(paymentStatus => ({
    label: paymentStatus[1] as string,
    value: paymentStatus[0] as string,
  }));
};

export const getRegExpValidationType = (type: FilterTypesTypeCombined) => {
  const typeMap: Partial<{
    [key in FilterTypesTypeCombined]: string;
  }> = {
    'relationship.requestor.zipcode': 'zipcode',
    'relationship.requestor.correspondence_zipcode': 'zipcode',
    'relationship.requestor.coc_number': 'cocNr',
    'relationship.custom_object.id': 'uuid',
    email: 'email',
    numeric: 'numeric',
    bankaccount: 'bankaccount',
  };
  return typeMap[type] || 'text';
};
