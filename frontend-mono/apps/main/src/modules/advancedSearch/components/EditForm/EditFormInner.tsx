// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import { Blocker } from 'react-router-dom';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { Box } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import Typography from '@mui/material/Typography';
import type { BlockerFunction } from 'react-router-dom';
import { useBlocker, useLocation, useNavigate } from 'react-router-dom';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import objectScan from 'object-scan';
import CircularProgress from '@mui/material/CircularProgress';
import { Field, Form, FormikProps } from 'formik';
import { QueryClient } from '@tanstack/react-query';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import { getFormikPassThrough } from '../FormikPassthrough';
import {
  AuthorizationsType,
  EditFormStateType,
  KindType,
  ModeType,
  ClassesType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { isUUID, getURLMatches } from '../../library/library';
import { editFormSx } from '../../styles/editForm';
import ObjectTypeSelect from './ObjectTypeSelect/ObjectTypeSelect';
import TemplateSelect from './TemplateSelect/TemplateSelect';
import { isFormValid } from './EditForm.library';
import { useSelectedObjectTypeReset } from './EditForm.sideEffects';
import EditFormContent from './EditFormContent';

const NameField = getFormikPassThrough(TextField);

type EditFormInnerPropsType = {
  formik: FormikProps<EditFormStateType>;
  classes: ClassesType;
  tabValue: number;
  handleTabChange: any;
  kind: KindType;
  client: QueryClient;
  setTabValue: Dispatch<SetStateAction<number>>;
  saveMutation: any;
  authorizations?: AuthorizationsType[];
  mode: ModeType;
  t: i18next.TFunction;
  blockerRef: React.MutableRefObject<Blocker | null>;
  identifier?: IdentifierType | null;
  ignoreBlocking?: boolean;
};

/* eslint complexity: [2, 20] */
const EditFormInner: FunctionComponent<EditFormInnerPropsType> = ({
  formik,
  classes,
  tabValue,
  handleTabChange,
  kind,
  client,
  setTabValue,
  saveMutation,
  authorizations,
  mode,
  t,
  identifier,
  blockerRef,
  ignoreBlocking,
}) => {
  let shouldBlock = React.useCallback<BlockerFunction>(
    //@ts-ignore
    ({ currentLocation, nextLocation }) => {
      if (ignoreBlocking) return false;
      return formik.dirty && currentLocation.pathname !== nextLocation.pathname;
    },
    [formik.values, ignoreBlocking]
  );
  const blocker = useBlocker(shouldBlock);
  if (blocker) blockerRef.current = blocker;
  const { values, errors } = formik;
  const errorPaths = objectScan(['**'], {
    joined: true,
    filterFn: ({ value }: any) => typeof value === 'string' && !isUUID(value),
  })(errors).reverse();
  const showEditForm =
    (kind === 'custom_object' && values.selectedObjectType) || kind === 'case';

  const location = useLocation();
  const navigate = useNavigate();
  const urlMatches = getURLMatches();

  useSelectedObjectTypeReset({
    formik,
    values,
    dependencies: [values.selectedObjectType],
    setTabValue,
    kind,
    t,
  });

  return (
    <>
      {blocker ? (
        <ConfirmDialog
          open={Boolean(blocker && blocker.state === 'blocked')}
          onConfirm={() => blocker.proceed?.()}
          onClose={() => blocker.reset?.()}
          title={t('dialogs.discardConfirm.title')}
          body={<div>{t('dialogs.discardConfirm.content') as string}</div>}
        />
      ) : null}
      {/* @ts-ignore */}
      <Form className={classes.editFormWrapper}>
        <div className={classes.editFormTop}>
          <IconButton
            onClick={() =>
              location?.state?.from
                ? window.history.back()
                : navigate(
                    `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}`
                  )
            }
            disableRipple={true}
            title={t('editForm.buttons.back') as string}
            size="small"
            className={classes.editFormBackBtn}
          >
            <Icon size="small" color="inherit">
              {iconNames.arrow_back}
            </Icon>
            {t('editForm.buttons.back') as string}
          </IconButton>
        </div>
        <Box
          className={classes.defaultSeperator}
          sx={{ height: 14, marginTop: '14px' }}
        />
        <div className={classes.editFormMiddle}>
          <div className={classes.editFormGeneralFields}>
            <div
              className={classNames(
                classes.filterRowContent,
                classes.filterRowWithLabel
              )}
            >
              <Typography variant="subtitle1">
                {t('editForm.fields.name.label') as string}
              </Typography>
              <Field
                id="name"
                name="name"
                placeholder={t('editForm.fields.name.placeholder')}
                component={NameField}
                variant="generic"
                sx={editFormSx().lightSelect}
                validate={(value: string) =>
                  !value || value.trim() === ''
                    ? t('editForm.fields.name.errorMessage')
                    : null
                }
              />
            </div>
            {kind === 'custom_object' ? (
              <div
                className={classNames(
                  classes.filterRowContent,
                  classes.filterRowWithLabel
                )}
              >
                <Typography variant="subtitle1">
                  {t('editForm.fields.objectType.label') as string}
                </Typography>
                <Field
                  name="selectedObjectType"
                  component={ObjectTypeSelect}
                  kind={kind}
                  t={t}
                  validate={(value: string) =>
                    !value ? t('editForm.fields.objectType.errorMessage') : null
                  }
                />
              </div>
            ) : null}
            {kind === 'case' ? (
              <div className={classes.templateFieldWrapper}>
                <div
                  className={classNames(
                    classes.filterRowContent,
                    classes.filterRowWithLabel
                  )}
                >
                  <Typography variant="subtitle1">
                    {t('editForm.fields.template.label') as string}
                  </Typography>
                  <Field
                    name="template"
                    component={TemplateSelect}
                    kind={kind}
                    t={t}
                  />
                </div>
                <div className={classes.templateHelp}>
                  <Tooltip enterDelay={200} title={t('template.help')}>
                    <Icon
                      size="extraSmall"
                      classes={{ wrapper: classes.helpIcon }}
                    >
                      {iconNames.help_outline}
                    </Icon>
                  </Tooltip>
                </div>
              </div>
            ) : null}
          </div>

          <Box
            className={classes.defaultSeperator}
            sx={{ height: 20, marginTop: '16px' }}
          />

          {showEditForm ? (
            <EditFormContent
              classes={classes}
              handleTabChange={handleTabChange}
              authorizations={authorizations}
              mode={mode}
              t={t}
              kind={kind}
              formik={formik}
              client={client}
              tabValue={tabValue}
              identifier={identifier}
            />
          ) : null}
        </div>
        <Box
          className={classes.defaultSeperator}
          sx={{ height: 14, marginTop: '14px' }}
        />
        <div className={classes.editFormBottom}>
          {isPopulatedArray(errorPaths) && (
            <div className={classes.editFormBottomErrors}>
              {get(errors, errorPaths[0])}
            </div>
          )}
          <div className={classes.editFormBottomButtons}>
            <Button
              name="submitSavedSearch"
              fullWidth={true}
              action={() => formik.submitForm()}
              disabled={!isFormValid({ formik, saveMutation })}
              {...(saveMutation.isLoading && {
                endIcon: <CircularProgress size={15} color="info" />,
              })}
            >
              {t('verbs.save')}
            </Button>
          </div>
        </div>
      </Form>
    </>
  );
};

export default EditFormInner;
