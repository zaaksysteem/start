// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import { getArchiveExportTemplate } from '../../library/config';
import {
  SavedSearchType,
  EditFormStateType,
  KindType,
} from '../../AdvancedSearch.types';
import { getNewState } from './EditForm.library';

// Side effects when selecting a template
export const onSavedSearchTemplateChange = ({
  formik,
  value,
}: {
  formik: FormikProps<EditFormStateType>;
  value: SavedSearchType['template'];
}) => {
  if (value !== 'archive_export') return;

  const { filters } = formik.values;
  const template = getArchiveExportTemplate();

  const currentFilters = formik.values?.filters?.filters.filter(
    filter => !template.map(addF => addF.type).includes(filter.type)
  );

  const newValues = {
    ...filters,
    operator: 'and',
    filters: [...template, ...(currentFilters || [])],
  };
  formik.setFieldValue('filters', newValues);
};

// Resets when selected object type is deselected and set to null
export const useSelectedObjectTypeReset = ({
  formik,
  values,
  dependencies,
  setTabValue,
  kind,
  t,
}: {
  formik: FormikProps<EditFormStateType>;
  values: any;
  dependencies: any[];
  setTabValue: (tabValue: number) => void;
  kind: KindType;
  t: i18next.TFunction;
}) => {
  useTransition((prev: any) => {
    if (values.selectedObjectType === null && prev !== null) {
      formik.setValues(getNewState({ kind, t }));
      setTabValue(0);
    }
  }, dependencies);
};
