// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useRef } from 'react';
import * as i18next from 'i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FieldArray, FormikProps, FieldArrayRenderProps, Field } from 'formik';
import classNames from 'classnames';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { FiltersSelect } from '../FiltersSelect/FiltersSelect';
import {
  KindType,
  ClassesType,
  EditFormStateType,
} from '../../../AdvancedSearch.types';
import { FilterType } from '../../../AdvancedSearch.types.filters';
import { editFormSx } from '../../../styles/editForm';
import FilterTypeComponent from './FilterType';
import { getOperatorOptions } from './library/Filters.library';

type FiltersPropsType = {
  kind: KindType;
  formik: FormikProps<EditFormStateType>;
  classes: ClassesType;
  show: boolean;
  t: i18next.TFunction;
  [key: string]: any;
};

// This is the main FieldArray
/* eslint complexity: [2, 12] */
const Filters = ({
  kind,
  formik,
  classes,
  show = true,
  t,
}: FiltersPropsType) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const { errors, values } = formik;
  const [arrayHelpersRef, setArrayHelpersRef] =
    useState<FieldArrayRenderProps | null>(null);
  const filters = values.filters?.filters;
  const hasFilters = Boolean(filters && filters.length > 0);
  const objectTypeSelected = Boolean(values.selectedObjectType);
  const showFilters =
    (kind === 'custom_object' && objectTypeSelected) || kind === 'case';
  const showFiltersSelect =
    (kind === 'custom_object' && Boolean(values.selectedObjectType)) ||
    kind === 'case';
  const name = 'filters.filters';

  return (
    <div
      className={classNames(classes.filtersContentWrapper, {
        [classes.show]: show,
        [classes.hide]: !show,
      })}
    >
      {showFiltersSelect && (
        <FiltersSelect
          classes={classes}
          arrayHelpersRef={arrayHelpersRef}
          t={t}
          kind={kind}
          formik={formik}
          inputRef={inputRef}
        />
      )}
      {showFilters && (
        <>
          <FieldArray
            name={name}
            render={arrayHelpers => {
              if (!arrayHelpersRef) setArrayHelpersRef(arrayHelpers);
              return (
                <>
                  {hasFilters && (
                    <div className={classes.filtersOperator}>
                      <Field
                        name={`filters.operator`}
                        t={t}
                        component={(props: any) => {
                          const { choices, value, field, disabled } = props;
                          return (
                            <Select
                              {...field}
                              variant="generic"
                              choices={choices}
                              value={value}
                              nestedValue={true}
                              isClearable={false}
                              disabled={disabled}
                              sx={editFormSx().lightSelect}
                              inputProps={{ readOnly: true }}
                            />
                          );
                        }}
                        disabled={values.template === 'archive_export'}
                        value={values.filters?.operator}
                        choices={getOperatorOptions(t, ['and', 'or'])}
                      />
                    </div>
                  )}
                  {values?.filters?.filters?.map(
                    (filter: FilterType, index: number) => (
                      <FilterTypeComponent
                        key={`filter-${index}`}
                        name={name}
                        filter={filter}
                        index={index}
                        arrayHelpers={arrayHelpers}
                        values={values}
                        formik={formik}
                        errors={errors}
                        classes={classes}
                        t={t}
                        kind={kind}
                      />
                    )
                  )}
                  {!hasFilters && (
                    <div className={classes.noFiltersLabel}>
                      {t('editForm.fields.filters.noFiltersLabel') as string}
                    </div>
                  )}
                </>
              );
            }}
          />
          <IconButton
            onClick={() => {
              if (!inputRef?.current) return;
              inputRef?.current.scrollIntoView();
              inputRef?.current.focus();
            }}
            disableRipple={true}
            size="small"
          >
            <Tooltip
              title={t('editForm.fields.filters.addFilter')}
              placement="top"
              sx={{ width: '100%', display: 'flex', justifyContent: 'center' }}
            >
              <Icon size="small" color="primary" sx={{ width: 'auto' }}>
                {iconNames.add}
              </Icon>
            </Tooltip>
          </IconButton>
        </>
      )}
    </div>
  );
};

export default Filters;
