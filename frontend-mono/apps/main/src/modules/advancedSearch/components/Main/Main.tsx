// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import * as i18next from 'i18next';
import {
  AuthorizationsType,
  ClassesType,
  AdvancedSearchParamsType,
} from '../../AdvancedSearch.types';
import { getURLMatches, hasAccess } from '../../library/library';
import { useSingleQuery } from '../../query/useSingle';
import Results from './Results/Results';

type MainPropsType = {
  classes: ClassesType;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
};

/* eslint complexity: [2, 20] */
const Main: FunctionComponent<MainPropsType> = ({ classes, t }) => {
  const session = useSession();
  const { mode, identifier, kind } = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;
  const currentQuery = useSingleQuery({
    identifier,
    kind,
    t,
    session,
  });
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const authorizations = currentQuery?.data?.authorizations;

  //General authorizations checks
  if (
    authorizations &&
    !hasAccess(authorizations, 'readwrite') &&
    (mode === 'edit' || mode === 'new')
  ) {
    navigate(
      `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}`
    );
  }

  if (!identifier) {
    return <p>{t('selectMessage') as string}</p>;
  }

  return (
    ['view', 'edit'].includes(mode as string) &&
    identifier && (
      <Results
        classes={classes}
        identifier={identifier}
        kind={kind}
        t={t}
        mode={mode}
        authorizations={authorizations}
        currentQuery={currentQuery}
      />
    )
  );
};

export default Main;
