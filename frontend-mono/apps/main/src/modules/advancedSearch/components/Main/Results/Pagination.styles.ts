// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    resultsTablePaging: {
      transform: 'translate3d(0, 0, 0)',
      height: 45,
    },
    pageIndicator: {
      alignSelf: 'center',
      justifySelf: 'center',
      marginRight: 10,
    },
    actionButtons: {
      marginLeft: 20,
      marginBottom: 3,
      display: 'flex',
      flexShrink: 0,
    },
    actions: {
      '&&.MuiTablePagination-actions': {
        margin: 0,
      },
    },
  };
});
