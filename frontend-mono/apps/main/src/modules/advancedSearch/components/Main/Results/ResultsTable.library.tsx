// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { GridColumnMenu } from '@mui/x-data-grid-pro';
import React, { FunctionComponent } from 'react';

export const CustomColumnMenu: FunctionComponent<any> = props => (
  <GridColumnMenu
    {...props}
    slots={{
      columnMenuColumnsItem: null,
    }}
  />
);
