// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { APISearchRowType } from '../../../../../AdvancedSearch.types';
import { useStyles } from './StatusIcon.styles';

type StatusIconPropsType = {
  status: APISearchRowType['attributes']['status'];
  archivalState: APISearchRowType['attributes']['archival_state'];
  destructionDate: APISearchRowType['attributes']['destruction_date'];
  unreadMessagesCount?: APISearchRowType['attributes']['unread_message_count'];
  unacceptedAttributesCount?: APISearchRowType['attributes']['unaccepted_attribute_update_count'];
  unacceptedFilesCount?: APISearchRowType['attributes']['unaccepted_files_count'];
  t: i18next.TFunction;
};

/* eslint complexity: [2, 10] */
const StatusIcon: FunctionComponent<StatusIconPropsType> = ({
  status,
  archivalState,
  destructionDate,
  unreadMessagesCount,
  unacceptedAttributesCount,
  unacceptedFilesCount,
  t,
}) => {
  const classes = useStyles();
  const destructionDatePassed = destructionDate
    ? Date.parse(new Date().toISOString()) > Date.parse(destructionDate)
    : false;

  const getIconConfig = () => {
    if (archivalState === 'overdragen') {
      return {
        icon: iconNames.arrow_circle_right,
        color: 'review' as any,
        info: t('case.status.transfer'),
      };
    } else if (
      archivalState === 'vernietigen' &&
      status === 'resolved' &&
      destructionDatePassed
    ) {
      return {
        icon: iconNames.cancel,
        color: 'danger' as any,
        info: t('case.status.destroy'),
      };
    } else if (status === 'resolved') {
      return {
        icon: iconNames.check_circle,
        color: 'secondary' as any,
        info: t('case.status.resolved'),
      };
    } else if (status === 'new') {
      return {
        icon: iconNames.stars,
        color: 'review' as any,
        info: t('case.status.new'),
      };
    } else if (status === 'open') {
      return {
        icon: iconNames.play_circle_filled,
        color: 'primary' as any,
        info: t('case.status.open'),
      };
    } else if (status === 'stalled') {
      return {
        icon: iconNames.pause_circle_filled,
        color: 'elephant' as any,
        info: t('case.status.stalled'),
      };
    }
  };
  const iconConfig = getIconConfig();

  if (!iconConfig) return null;

  const notificationsCount =
    (unreadMessagesCount || 0) +
    (unacceptedAttributesCount || 0) +
    (unacceptedFilesCount || 0);

  return (
    <div className={classes.caseStatusWrapper}>
      <Tooltip title={iconConfig.info} placement="left-start">
        <>
          <Icon size="small" color={iconConfig.color}>
            {iconConfig.icon}
          </Icon>
          {notificationsCount > 0 && (
            <div className={classes.caseStatusNr}>{notificationsCount}</div>
          )}
        </>
      </Tooltip>
    </div>
  );
};

export default StatusIcon;
