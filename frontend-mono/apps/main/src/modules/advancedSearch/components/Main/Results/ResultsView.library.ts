// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { UseQueryResult } from '@tanstack/react-query';
import { getSystemColumns } from '../../../library/config';
import {
  CapabilitiesType,
  ColumnType,
  KindType,
  SavedSearchType,
} from '../../../AdvancedSearch.types';

export const useColumns = ({
  kind,
  capabilities,
  currentQuery,
  showSummary,
}: {
  kind: KindType;
  capabilities: CapabilitiesType;
  currentQuery: UseQueryResult<SavedSearchType>;
  showSummary: boolean;
}) => {
  const systemColumns = getSystemColumns(kind);

  const parseRegularColumns = (cols: ColumnType[]) => {
    if (!isPopulatedArray(cols)) return [];
    return [
      ...cols,
      ...(capabilities?.assigneeControls
        ? systemColumns.filter(col => col.type === 'assigneeControls')
        : []),
      ...systemColumns.filter(col => col.type === 'actions'),
    ];
  };
  const parseSummaryColumns = (cols: ColumnType[]) => {
    if (!isPopulatedArray(cols)) return [];
    return cols.filter(col =>
      capabilities?.assigneeControls === false &&
      col.type === 'assigneeControls'
        ? false
        : true
    );
  };

  const regularColumns = parseRegularColumns(
    currentQuery?.data?.columns as ColumnType[]
  );
  const summaryColumns = parseSummaryColumns(systemColumns);
  const columns = showSummary ? summaryColumns : regularColumns;

  return { columns };
};
