// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import * as i18next from 'i18next';
import classNames from 'classnames';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import useMeasure from 'react-use-measure';
import {
  FILTERS_SUMMARY_DEFAULT_EXPANDED,
  FILTERS_SUMMARY_MAX_HEIGHT,
} from '../../../../../library/config';
import { KindType } from '../../../../../AdvancedSearch.types';
import { FiltersType } from '../../../../../AdvancedSearch.types.filters';
import { parseFilters } from './FiltersSummary.library';
import { getFiltersSummary } from './FiltersSummary.library';

type FiltersSummaryPropsType = {
  filters: FiltersType | null;
  t: i18next.TFunction;
  classes: any;
  kind: KindType;
};

type SummaryType = ReturnType<typeof getFiltersSummary>;

/* eslint complexity: [2, 12] */
const FiltersSummary: FunctionComponent<FiltersSummaryPropsType> = ({
  filters,
  t,
  classes,
  kind,
}) => {
  const [expanded, setExpanded] = useState<boolean>(
    FILTERS_SUMMARY_DEFAULT_EXPANDED
  );

  const [measureRef, filtersMeasure] = useMeasure({ scroll: true });
  const [parentMeasureRef, filtersParentMeasure] = useMeasure({ scroll: true });

  const parsedFilters = parseFilters(filters);
  if (!parsedFilters) return null;

  const summary = getFiltersSummary(parsedFilters, t, kind);
  if (!summary || !isPopulatedArray(summary?.filters)) return null;

  const hasScrolled = filtersParentMeasure.top - filtersMeasure.top > 0;
  const showMore = filtersMeasure.height > FILTERS_SUMMARY_MAX_HEIGHT;
  const handleExpand = () => setExpanded(!expanded);

  return (
    <Tooltip
      title={t('summary.expand') as string}
      placement="bottom"
      enterDelay={100}
      leaveDelay={100}
    >
      <div
        role="button"
        tabIndex={-1}
        onKeyDown={handleExpand}
        onClick={handleExpand}
        className={classes.summaryWrapper}
      >
        <div
          className={classNames(classes.summaryEntriesWrapper, {
            [classes.summaryExpanded]: expanded,
          })}
          ref={parentMeasureRef}
        >
          <>
            {expanded && showMore && !hasScrolled && (
              <div className={classes.summaryShowMore}>
                <Icon size="large" color="primary">
                  {iconNames.more_horiz}
                </Icon>
              </div>
            )}
            {expanded ? (
              <SummaryCmp
                summary={summary}
                measureRef={measureRef}
                classes={classes}
              />
            ) : (
              <HeaderCmp summary={summary} classes={classes} t={t} />
            )}
          </>
        </div>
      </div>
    </Tooltip>
  );
};

const SummaryCmp = ({
  summary,
  measureRef,
  classes,
}: {
  summary: SummaryType;
  measureRef: ReturnType<typeof useMeasure>[0];
  classes: any;
}) => {
  if (!summary || !isPopulatedArray(summary.filters)) return null;
  const summaryItems = summary.filters.map((filter, index) => {
    return (
      <div key={`summary-filter-${index}`} className={classes.summaryFilter}>
        <div>
          <Icon size="extraSmall" color="inherit">
            {filter.type === 'customAttribute'
              ? iconNames.extension
              : iconNames.folder}
          </Icon>
          <span>{filter.title}</span>
        </div>
        <div>
          {filter.filter.values.map((value: any, valueIndex: number) => {
            return (
              <div key={valueIndex} className={classes.summaryValueWrapper}>
                <div className={classes.summaryValue}>{value}</div>{' '}
                <div>
                  {valueIndex !== filter.filter.values.length - 1
                    ? filter.filter.operator
                    : null}
                </div>
              </div>
            );
          })}
          {index !== summary?.filters.length - 1
            ? `${summary?.operator}`
            : null}
        </div>
      </div>
    );
  });

  return <div ref={measureRef}>{summaryItems}</div>;
};

const HeaderCmp = ({
  classes,
  summary,
  t,
}: {
  classes: any;
  summary: SummaryType;
  t: i18next.TFunction;
}) => {
  return (
    <header className={classes.summaryHeader}>
      <div>
        <Icon size="small" color="inherit">
          {iconNames.filter_list}
        </Icon>
        {summary?.filters.length} {t('summary.activeFilters') as string}
      </div>
    </header>
  );
};

export default FiltersSummary;
