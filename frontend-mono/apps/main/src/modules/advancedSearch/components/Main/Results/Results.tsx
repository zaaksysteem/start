// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import { UseQueryResult } from '@tanstack/react-query';
import { createPortal } from 'react-dom';
import * as i18next from 'i18next';
import classNames from 'classnames';
import {
  AuthorizationsType,
  ClassesType,
  IdentifierType,
  KindType,
  ModeType,
  SavedSearchType,
  ViewType,
} from '../../../AdvancedSearch.types';

import MainButtons from '../MainButtons';
import ResultsTopBar from './components/ResultsTopBar';
import ResultsView from './ResultsView';
import { Label } from './Results.library';
import FiltersSummary from './components/FiltersSummary/FiltersSummary';

type ResultsType = {
  classes: ClassesType;
  identifier: IdentifierType;
  kind: KindType;
  t: i18next.TFunction;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
  currentQuery: UseQueryResult<SavedSearchType>;
};

const Results: FunctionComponent<ResultsType> = ({
  classes,
  identifier,
  kind,
  t,
  mode,
  authorizations,
  currentQuery,
}) => {
  const [totalResults, setTotalResults] = useState(0);
  const [view, setView] = useState<ViewType>('table');
  const [topBarLeftRef, setTopBarLeftRef] = useState<HTMLElement | null>(null);
  const [topBarRightRef, setTopBarRightRef] = useState<HTMLElement | null>(
    null
  );

  if (!currentQuery.data) return null;

  const getPortal = () =>
    createPortal(
      <>
        <Label
          mode={mode}
          classes={classes}
          currentQuery={currentQuery}
          totalResults={totalResults}
          t={t}
        />
        <MainButtons
          identifier={identifier}
          mode={mode}
          authorizations={authorizations}
          t={t}
        />
      </>,
      topBarLeftRef as Element
    );

  return (
    <>
      <div className={classNames(classes.mainTopBar)}>
        <div className={classes.mainTopBarLeft} ref={setTopBarLeftRef} />
        <div className={classes.mainTopBarRight} ref={setTopBarRightRef} />
      </div>
      {topBarLeftRef && getPortal()}

      <div className={classes.resultsWrapper}>
        <ResultsTopBar
          menuRef={topBarRightRef}
          view={view}
          t={t}
          setView={setView}
        />
        <FiltersSummary
          filters={currentQuery?.data?.filters}
          t={t}
          classes={classes}
          kind={kind}
        />
        <ResultsView
          key={identifier}
          kind={kind}
          identifier={identifier}
          view={view}
          setTotalResults={setTotalResults}
          portalRef={topBarLeftRef}
          topBarRightRef={topBarRightRef}
        />
      </div>
    </>
  );
};

export default Results;
