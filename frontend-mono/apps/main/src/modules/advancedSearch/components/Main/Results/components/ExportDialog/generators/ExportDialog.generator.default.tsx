// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect, FunctionComponent, useRef } from 'react';
import * as i18next from 'i18next';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import { QueryClient, useQuery } from '@tanstack/react-query';
import { GridInitialState } from '@mui/x-data-grid-pro';
import {
  UseResultsQueryQueryParamsType,
  getQueryConfig,
} from '../../../../../../query/useResults';
import {
  QUERY_KEY_EXPORT_RESULTS,
  QUERY_KEY_EXPORT_RESULTS_COUNT,
} from '../../../../../../query/constants';
import {
  ExportFormatType,
  ParseResultsModeType,
  ExportResultsRowType,
  GetResultsReturnType,
  CursorCollectionType,
} from '../../../../../../AdvancedSearch.types';
import { parseResults, resultsToPapaparse } from '../ExportDialog.library';

type ExportGeneratorPropsType = {
  key: string;
  numPages: number;
  format: ExportFormatType;
  parseResultsMode: ParseResultsModeType;
  baseQueryParams: Omit<UseResultsQueryQueryParamsType, 'page'>;
  handleDataDone: (downloadFile: any) => void;
  t: i18next.TFunction;
  handleProgressUpdate: (progressPercentage: any) => void;
  queryClient: QueryClient;
  page: number;
  everythingSelected: boolean;
  selectedRows: string[];
  handleError: () => void;
  dataGridState: GridInitialState;
};

/* eslint complexity: [2, 8] */
const ExportGeneratorDefault: FunctionComponent<ExportGeneratorPropsType> = ({
  numPages,
  format,
  parseResultsMode,
  baseQueryParams,
  handleDataDone,
  t,
  handleProgressUpdate,
  queryClient,
  page,
  everythingSelected,
  selectedRows,
  handleError,
  dataGridState,
}) => {
  const [startQueue, setStartQueue] = useState<number[] | null>(null);
  const [queue, setQueue] = useState<number[] | null>(null);
  const [currentPage, setCurrentPage] = useState<number | null>(null);
  const previousPageRef = useRef<number | null>(null);
  const cursorCollectionRef = useRef<CursorCollectionType | null>(null);
  const [resultsPages, setResultsPages] = useState<GetResultsReturnType[]>([]);

  const commonQueryParams = {
    queryKey: QUERY_KEY_EXPORT_RESULTS,
    t,
    parseResultsMode,
    onError: handleError,
    keepPreviousData: true,
  };

  useEffect(() => {
    let startQueue;
    startQueue = !everythingSelected
      ? [page]
      : Array.from(Array(numPages).keys()).map((pageNr: number) => pageNr + 1);

    setStartQueue(startQueue);
    setQueue(startQueue);
    setCurrentPage(startQueue[0]);
  }, []);

  useTransition(
    async (prevQueue: number[]) => {
      const progress =
        100 -
        ((queue?.length || 0) / (startQueue ? startQueue?.length : 0)) * 100;
      handleProgressUpdate(progress);

      if (!queue && prevQueue?.length && baseQueryParams?.columns) {
        const parsedResults: ExportResultsRowType[] = parseResults({
          pages: resultsPages,
          selectedRows,
          everythingSelected,
          columns: baseQueryParams.columns,
          dataGridState,
          parseResultsMode,
          t,
        });
        handleDataDone(
          await resultsToPapaparse(parsedResults, format === 'CSV' ? ',' : '\t')
        );
      }
    },
    [queue]
  );

  useEffect(() => {
    return () => {
      queryClient.removeQueries([QUERY_KEY_EXPORT_RESULTS]);
      queryClient.invalidateQueries([QUERY_KEY_EXPORT_RESULTS_COUNT]);
    };
  }, []);

  const getTargetCursor = () => {
    if (baseQueryParams.cursor) return baseQueryParams.cursor;
    if (
      !currentPage ||
      !previousPageRef.current ||
      !cursorCollectionRef.current
    )
      return null;
    return cursorCollectionRef.current.next;
  };

  const config = getQueryConfig({
    queryParams: {
      ...baseQueryParams,
      cursor: getTargetCursor(),
    },
    page: currentPage,
    enabled: Boolean(currentPage && currentPage !== previousPageRef?.current),
    fullContent: true,
    ...commonQueryParams,
  });

  useQuery({
    ...config,
    onSuccess: result => {
      previousPageRef.current = currentPage;
      //@ts-ignore
      cursorCollectionRef.current = result?.meta?.cursor;

      if (result) setResultsPages([...resultsPages, result]);
      if (queue && queue.length > 1) {
        setQueue(queue.slice(1));
        setCurrentPage(queue[1]);
      } else {
        setQueue(null);
      }
    },
  });

  return null;
};

export default ExportGeneratorDefault;
