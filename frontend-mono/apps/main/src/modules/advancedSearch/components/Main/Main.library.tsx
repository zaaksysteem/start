// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as i18next from 'i18next';
import { UseQueryResult } from '@tanstack/react-query';
import { ModeType, SavedSearchType } from '../../AdvancedSearch.types';

export const getLabel = ({
  mode,
  currentQuery,
  t,
}: {
  mode: ModeType;
  currentQuery: UseQueryResult<SavedSearchType>;
  t: i18next.TFunction;
}) => (mode === 'new' ? t('new') : currentQuery?.data?.name || '');
