// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import * as i18next from 'i18next';
import { useNavigate, useParams } from 'react-router-dom';
import {
  AuthorizationsType,
  ModeType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { getURLMatches, hasAccess, isUUID } from '../../library/library';

type MainButtonsPropsType = {
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
};

const MainButtons = ({
  identifier,
  mode,
  authorizations,
  t,
}: MainButtonsPropsType) => {
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const { kind } = useParams();

  return (
    <>
      {mode === 'view' &&
        identifier &&
        isUUID(identifier) &&
        authorizations &&
        hasAccess(authorizations, 'readwrite') && (
          <Button
            name="editAuthorization"
            action={() => {
              navigate(
                `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/edit/${identifier}`,
                {
                  state: { from: 'mainButtons' },
                }
              );
            }}
          >
            {t('buttons.edit')}
          </Button>
        )}
    </>
  );
};

export default MainButtons;
