// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  useEffect,
  FunctionComponent,
  useState,
  Dispatch,
  SetStateAction,
  useRef,
  MutableRefObject,
} from 'react';
import { GridInitialState, GridApiCommon } from '@mui/x-data-grid-pro';
import deepEqual from 'fast-deep-equal';
import attributesLocale from '@zaaksysteem/common/src/locale/attributes.locale';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import Button from '@mintlab/ui/App/Material/Button';
import type { TFunction } from 'i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { useQueryClient } from '@tanstack/react-query';
import { createPortal } from 'react-dom';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import { useSingleQuery } from '../../../query/useSingle';
import { useResultsQuery } from '../../../query/useResults';
import { DEFAULT_RESULTS_PER_PAGE } from '../../../library/config';
import {
  QUERY_KEY_RESULTS,
  QUERY_KEY_RESULTS_COUNT,
} from '../../../query/constants';
import {
  IdentifierType,
  KindType,
  ViewType,
  CursorCollectionType,
  CapabilitiesType,
} from '../../../AdvancedSearch.types';
import { useResultsCountQuery } from '../../../query/useResults';
import resultsLocale from '../../../locale/results.locale';
import { APIResultsToResults } from '../../../query/useResults';
import { useStyles } from './ResultsView.styles';
import { getDataGridState, deleteLocaleDataGridState } from './Results.library';
import ResultsTable from './ResultsTable';
import { useColumns } from './ResultsView.library';

import { saveLocalDataGridState } from './Results.library';
import ResultsMap from './ResultsMap';
import ResultsPagination from './components/ResultsPagination';
import ExportDialog from './components/ExportDialog/ExportDialog';
import Actions from './components/Actions';

type ResultsViewPropsType = {
  identifier: IdentifierType;
  kind: KindType;
  view: ViewType;
  setTotalResults?: Dispatch<SetStateAction<number>>;
  keyword?: string;
  setName?: (name: string) => void;
  portalRef?: HTMLElement | null;
  setPage?: Dispatch<SetStateAction<number>>;
  topBarRightRef?: HTMLElement | null;
  capabilities?: CapabilitiesType;
};

// Resultsview / Datagrid flow:

// When rendering:
//
// dataGridState is created, based on a combination of aspects of the
// Saved Search columns (order, visibility, etc.) and aspects of the DataGrid
// (pinned columns, column widths). This is used to (re)initialize the DataGrid
//
// When the user changes something (pinned columns, column widths):
// a copy of the DataGrid state is saving to Local Storage
//
// The table view has the ability to switch dynamically between the regular (full)
// view and the summary view, based on the width of the table. When the switch
// happens:
// - new memoized DataGrid columns will be generated by ResultsTable
// - new rows will be used, since the contents of the columns in the rows
//   will be different
//

/* eslint complexity: [2, 30] */
const ResultsView: FunctionComponent<ResultsViewPropsType> = ({
  identifier,
  kind,
  setTotalResults,
  view = 'table',
  keyword,
  setName,
  portalRef,
  topBarRightRef,
  capabilities = {
    summary: false,
    assigneeControls: false,
    checkboxSelection: true,
  },
}) => {
  const [showSummary, setShowSummary] = useState<boolean>(false);
  const classes = useStyles();
  const [resultsPerPage, setResultsPerPage] = useState(
    DEFAULT_RESULTS_PER_PAGE
  );
  const [page, setPage] = useState<number>(1);
  const [busy, setBusy] = useState<boolean>(false);
  const prevPageRef = useRef<number | null>(null);
  const cursorCollectionRef = useRef<CursorCollectionType | null>(null);
  const [exportDialogOpen, setExportDialogOpen] = useState<boolean>(false);
  const session = useSession();
  const [t] = useTranslation('results') as unknown as TFunction[];
  const queryClient = useQueryClient();

  const currentQuery = useSingleQuery({
    identifier,
    kind,
    t,
    session,
  });

  const { columns } = useColumns({
    kind,
    capabilities,
    currentQuery,
    showSummary,
  });

  const dataGridState: GridInitialState = getDataGridState({
    identifier,
    columns,
  });

  const resetToInitialState = (apiRef: MutableRefObject<GridApiCommon>) => {
    deleteLocaleDataGridState(identifier);
    const newState = getDataGridState({
      identifier,
      columns,
    });
    apiRef?.current?.restoreState(newState);
  };

  const getCursor = () => {
    if (!prevPageRef.current || !cursorCollectionRef?.current) return;
    return page > prevPageRef.current
      ? cursorCollectionRef.current.next
      : cursorCollectionRef.current.previous;
  };

  const filters = currentQuery?.data?.filters;
  const currentSuccess = currentQuery.isSuccess;
  const queryParamsBase = {
    kind,
    identifier,
    resultsPerPage,
    columns,
    filters,
    geo: view === 'map',
    keyword,
    ...(currentQuery?.data?.sortColumn && {
      sortColumn: currentQuery?.data?.sortColumn,
      sortOrder: currentQuery?.data?.sortOrder,
    }),
  };
  const queryParams = {
    ...queryParamsBase,
    cursor: getCursor(),
  };

  const getResultsEnabled = () => {
    if (page > 1 && (!prevPageRef.current || !cursorCollectionRef.current)) {
      return false;
    }
    return currentSuccess && Boolean(identifier);
  };

  const resultsQuery = useResultsQuery({
    queryParams,
    enabled: getResultsEnabled(),
    page,
    keepPreviousData: true,
    capabilities,
  });

  const resultsCountQuery = useResultsCountQuery({
    queryParams: cloneWithout(
      queryParams,
      'page',
      'cursor',
      'resultsPerPage',
      'sortColumn',
      'sortOrder'
    ),
    t,
    currentSuccess,
    enabled: currentSuccess && Boolean(identifier),
  });

  const rows = APIResultsToResults({
    classes,
    data: resultsQuery?.data,
    columns,
    t,
    parseResultsMode: 'screen',
    kind,
  });

  const features: Geojson[] | undefined = rows?.reduce<Geojson[]>(
    (acc, current) => {
      if (current?.geoFeatures && current?.geoFeatures.length) {
        acc = [...acc, ...current.geoFeatures];
      }
      return acc;
    },
    []
  );

  const selectionProps = useSelectionBehaviour({
    rows: rows || [],
    page,
    resultsPerPage,
    selectEverythingTranslations: t('selectable', {
      returnObjects: true,
    }) as any,
  });
  const { everythingSelected, selectedRows } = selectionProps;

  const showContextButtons =
    portalRef && (isPopulatedArray(selectedRows) || everythingSelected);

  useTransition(
    (prev: any) => {
      if (!deepEqual(queryParamsBase, prev)) {
        prevPageRef.current = null;
        cursorCollectionRef.current = null;
        //@ts-ignore
        queryClient.invalidateQueries[QUERY_KEY_RESULTS];
        //@ts-ignore
        queryClient.invalidateQueries[QUERY_KEY_RESULTS_COUNT];
        if (setPage) {
          setPage(1);
        }
      }
    },
    [queryParamsBase]
  );

  useTransition(
    (prevDataGridState: any) => {
      saveLocalDataGridState(identifier, dataGridState);
    },
    [dataGridState]
  );

  useEffect(() => {
    const count = resultsCountQuery?.data;
    if (setTotalResults && count !== null) setTotalResults(count as any);
  }, [resultsCountQuery?.data]);

  useEffect(() => {
    if (setName && currentQuery?.data) setName(currentQuery.data.name);
  }, [currentQuery.data]);

  if (currentQuery?.isLoading || resultsQuery?.isLoading) return <Loader />;
  if (!currentQuery?.isLoading && !currentQuery.data)
    return (
      <div className={classes.statusMessage}>
        {t('invalidSavedSearch') as string}
      </div>
    );

  return (
    <>
      {showContextButtons &&
        createPortal(
          <Button
            name="exportBtn"
            label={t('buttons.export')}
            action={() => setExportDialogOpen(true)}
          >
            {t('buttons.export')}
          </Button>,
          portalRef
        )}

      {showContextButtons &&
        createPortal(
          <Actions
            t={t}
            selectedRows={selectedRows}
            everythingSelected={everythingSelected}
            filters={currentQuery?.data?.filters}
            kind={kind}
          />,
          portalRef
        )}

      <div className={classes.resultsMain}>
        {exportDialogOpen && currentQuery.data && (
          <ExportDialog
            open={exportDialogOpen}
            onClose={() => setExportDialogOpen(false)}
            currentQueryData={currentQuery.data}
            kind={kind}
            t={t as any}
            queryClient={queryClient}
            page={page}
            everythingSelected={everythingSelected}
            selectedRows={selectedRows}
            resultsPerPage={resultsPerPage}
            dataGridState={dataGridState}
          />
        )}

        {view === 'table' &&
          rows &&
          columns &&
          columns.length &&
          dataGridState && (
            <ResultsTable
              columns={columns}
              rows={rows}
              kind={kind}
              selectionProps={selectionProps}
              dataGridState={dataGridState}
              capabilities={capabilities}
              isFetching={resultsQuery.isFetching}
              t={t as any}
              identifier={identifier}
              resetToInitialState={resetToInitialState}
              classes={classes}
              topBarRightRef={topBarRightRef}
              showSummary={showSummary}
              setShowSummary={setShowSummary}
              isLoading={resultsQuery.isFetching || busy}
            />
          )}
        {view === 'map' && <ResultsMap features={features} />}
        <ResultsPagination
          //@ts-ignore
          totalResults={resultsCountQuery?.data || 0}
          page={page}
          resultsPerPage={resultsPerPage}
          setPage={setPage}
          setResultsPerPage={setResultsPerPage}
          queryClient={queryClient}
          queryParams={queryParams}
          prevPageRef={prevPageRef}
          cursorCollection={resultsQuery?.data?.meta?.cursor}
          cursorCollectionRef={cursorCollectionRef}
          isLoading={resultsQuery.isFetching}
          setBusy={setBusy}
          busy={busy}
          t={t}
        />
      </div>
    </>
  );
};

export default (props: ResultsViewPropsType) => (
  <I18nResourceBundle resource={resultsLocale} namespace="results">
    <I18nResourceBundle resource={attributesLocale} namespace="attributes">
      <ResultsView {...props} />
    </I18nResourceBundle>
  </I18nResourceBundle>
);
