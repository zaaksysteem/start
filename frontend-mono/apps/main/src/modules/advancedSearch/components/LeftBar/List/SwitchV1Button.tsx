// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';

const SwitchV1Button = () => {
  return (
    <div
      style={{
        width: '100%',
      }}
    >
      <Button
        sx={{
          height: 30,
          fontSize: 11,
          textTransform: 'uppercase',
        }}
        name="submitSavedSearch"
        variant="contained"
        action={() => {
          window.location.href = '/search';
        }}
      >
        Uitgebreid zoeken v1
      </Button>
    </div>
  );
};

export default SwitchV1Button;
