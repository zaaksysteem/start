// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import { QueryClient } from '@tanstack/react-query';
import { v4 } from 'uuid';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useNavigate, Link } from 'react-router-dom';
import { palette } from '@mintlab/ui/App/Material/ThemeProvider/theme';
import classNames from 'classnames';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import {
  openServerError,
  openSnackbar,
  snackbarMargin,
} from '@zaaksysteem/common/src/signals';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import Button from '@mintlab/ui/App/Material/Button';
import {
  ClassesType,
  IdentifierType,
  KindType,
  EditFormStateType,
} from '../../../AdvancedSearch.types';
import { useSingleDeleteMutation } from '../../../query/useSingle';
import { invalidateAllQueries } from '../../../query/library';
import { getURLMatches, hasAccess } from '../../../library/library';
import InfoDialog from './InfoDialog/InfoDialog';
import CopyDialog from './CopyDialog/CopyDialog';
import Labels from './Labels/Labels';

type RowPropsType = {
  data: EditFormStateType;
  classes: ClassesType;
  client: QueryClient;
  t: i18next.TFunction;
  identifier?: IdentifierType;
  kind: KindType;
};

/* eslint complexity: [2, 12] */
const Row: FunctionComponent<RowPropsType> = ({
  data,
  classes,
  client,
  t,
  identifier,
  kind,
}) => {
  const [editingLabels, setEditingLabels] = useState<boolean>(false);
  const [infoDialogOpen, setInfoDialogOpen] = useState<boolean>(false);
  const [copyDialogOpen, setCopyDialogOpen] = useState<boolean>(false);
  const { uuid, authorizations, type } = data;
  const active = identifier === uuid;
  const deleteMutation = useSingleDeleteMutation();
  const navigate = useNavigate();
  const urlMatches = getURLMatches();
  const [deleteUUID, setDeleteUUID] = useState<IdentifierType | null>(null);
  const readWriteAccess = hasAccess(authorizations, 'readwrite');

  const startDelete = () => {
    deleteMutation.mutate(uuid as string, {
      onSuccess: () => {
        invalidateAllQueries(client);
        openSnackbar({ message: t('snacks.deleted'), sx: snackbarMargin });
        navigate(
          `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}`
        );
      },
      onError: openServerError,
    });
  };

  let actions = [];
  actions.push({
    action: () => setCopyDialogOpen(true),
    title: t('actions.copy'),
  });
  if (data?.metaData) {
    actions.push({
      action: () => setInfoDialogOpen(true),
      title: t('actions.moreInfo'),
    });
  }
  if (type === 'user' && readWriteAccess && uuid) {
    actions = [
      {
        action: () =>
          navigate(
            `/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/edit/${uuid}`,
            {
              state: { from: 'list' },
            }
          ),
        title: t('actions.edit'),
      },
      {
        action: () => setDeleteUUID(uuid),
        title: t('actions.delete'),
      },
      ...actions,
    ];
  }

  return (
    <>
      <ConfirmDialog
        open={Boolean(deleteUUID)}
        onConfirm={() => startDelete()}
        onClose={() => setDeleteUUID(null)}
        title={t('confirm')}
        body={
          <div>
            {
              t('deleteConfirm', {
                name: data.name,
              }) as string
            }
          </div>
        }
      />
      <InfoDialog
        open={infoDialogOpen}
        onClose={() => setInfoDialogOpen(false)}
        savedSearch={data}
        classes={classes}
        t={t}
      />
      <CopyDialog
        open={copyDialogOpen}
        onClose={() => setCopyDialogOpen(false)}
        savedSearch={data}
        t={t}
        client={client}
        kind={kind}
        key={v4()}
      />
      <div className={classes.leftBarRowWrapper}>
        <div
          className={classNames(classes.leftBarTop, {
            [classes.savedSearchLinkActive]: active,
            [classes.savedSearchLinkInactive]: !active,
          })}
        >
          <Link
            className={classes.savedSearchLink}
            key={data.uuid}
            to={`/${urlMatches?.params.prefix}/${urlMatches?.params.module}/${kind}/view/${data.uuid}`}
            state={{ from: 'list' }}
          >
            {data.name}
          </Link>
          <>
            {type === 'user' && readWriteAccess && (
              <Tooltip
                title={t('labels.editLabels')}
                enterDelay={400}
                style={{ width: 'auto' }}
              >
                <IconButton
                  onClick={() => setEditingLabels(!editingLabels)}
                  disableRipple={false}
                  size="small"
                  sx={{
                    '&:hover': {
                      backgroundColor: palette.primary.light,
                    },
                  }}
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.bookmarks}
                  </Icon>
                </IconButton>
              </Tooltip>
            )}
            {isPopulatedArray(actions) && (
              <Tooltip
                title={t('labels.more')}
                enterDelay={400}
                style={{ width: 'auto' }}
              >
                <DropdownMenu
                  transformOrigin={{ horizontal: 'center', vertical: 'top' }}
                  trigger={
                    <Button
                      name="savedSearchMore"
                      icon="more_vert"
                      scope={`searches-bar:loadmore`}
                      iconSize="small"
                      sx={{
                        padding: '4px',
                        '&:hover': {
                          backgroundColor: palette.primary.light,
                        },
                      }}
                    />
                  }
                >
                  <DropdownMenuList
                    items={actions.map(({ action, title }) => ({
                      action,
                      label: title,
                      scope: `searches-bar:foldout-menu:${title}`,
                    }))}
                  />
                </DropdownMenu>
              </Tooltip>
            )}
          </>
        </div>
        {type === 'user' &&
          (active || editingLabels || isPopulatedArray(data.labels)) && (
            <Labels
              editing={editingLabels}
              data={data}
              classes={classes}
              setEditingLabels={setEditingLabels}
              t={t}
              readWriteAccess={readWriteAccess}
            />
          )}
      </div>
    </>
  );
};

export default Row;
