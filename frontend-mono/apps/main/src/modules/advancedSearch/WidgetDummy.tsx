// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState, FunctionComponent } from 'react';
import { useParams } from 'react-router-dom';
import ResultsView from './components/Main/Results/ResultsView';
import { KindType } from './AdvancedSearch.types';

type WidgetDummyPropsType = {
  identifier?: string;
  kind: KindType;
  keyword?: string;
};

const WidgetDummy: FunctionComponent<WidgetDummyPropsType> = ({
  identifier,
  kind,
  keyword,
}) => {
  const [, setTotalResults] = useState(0);
  const { identifier: urlIdentifier } = useParams<string>();
  const targetIdentifier = urlIdentifier || identifier || '';

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
      }}
    >
      <ResultsView
        kind={kind}
        identifier={targetIdentifier}
        view={'table'}
        keyword={keyword}
        setTotalResults={setTotalResults}
        capabilities={{
          summary: kind === 'case',
          assigneeControls: targetIdentifier === 'intake',
          checkboxSelection: false,
        }}
      />
    </div>
  );
};

export default WidgetDummy;
