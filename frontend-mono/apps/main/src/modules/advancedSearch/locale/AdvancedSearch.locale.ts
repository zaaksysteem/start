// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Uitgebreid zoeken',
    new: 'Nieuw',
    searchIn: 'Zoeken in',
    selectMessage:
      'Selecteer een opgeslagen zoekopdracht om de resultaten op te vragen, of maak een nieuwe zoekopdracht aan.',
    kind: {
      objects: 'Objecten',
      cases: 'Zaken',
    },
    template: {
      title: 'Sjabloon',
      choices: {
        standard: 'Standaard',
        archive_export: 'Archief export',
      },
      help: 'Wanneer het sjabloon "Archief export" wordt gekozen, worden de filters "Status" en "Archiefstatus" automatisch toegevoegd. Deze kunnen niet gewijzigd of verwijderd worden. De globale operator voor filters wordt op "EN" gezet.',
    },
    savedSearches: 'Opgeslagen zoekopdrachten',
    defaultSearches: {
      allcases: 'Alle zaken',
      mycases: 'Mijn openstaande zaken',
      intake: 'Intake',
      allobjects: 'Alle objecten',
    },
    dialogs: {
      discardConfirm: {
        title: 'Bevesting',
        content:
          'Door het navigeren naar deze pagina zullen uw wijzigingen verloren gaan. Weet u zeker dat u door wilt gaan?',
      },
      copy: {
        title: 'Kopie maken van',
        copy: 'kopie',
        label: 'Naam',
        placeholder: 'Voer de nieuwe naam in',
        copyBtn: 'Maak kopie',
      },
    },
    correspondence: 'Correspondentie',
    snacks: {
      deleted: 'De zoekopdracht is verwijderd.',
      saved: 'De zoekopdracht is opgeslagen.',
      copySaved: 'De kopie van de zoekopdracht is opgeslagen.',
    },
    actions: {
      copy: 'Kopie maken',
      edit: 'Wijzigen',
      delete: 'Verwijderen',
      moreInfo: 'Meer informatie…',
    },
    buttons: {
      edit: 'Wijzigen',
    },
    newSavedSearch: 'Nieuwe zoekopdracht aanmaken',
    loadMore: 'meer laden',
    confirm: 'Bevestigen',
    deleteConfirm:
      'U staat op het punt om de opgeslagen zoekopdracht met de naam "{{ name }}" te verwijderen. Weet u zeker dat u door wilt gaan?',
    editForm: {
      buttons: {
        back: 'Terug',
      },
      operator: 'Operator',
      tabs: {
        filters: 'Filters',
        permissions: 'Delen',
        columns: 'Kolommen',
      },
      methods: {
        systemAttributes: 'Systeemkenmerken',
        systemAttribute: 'Systeemkenmerk',
        searchAttributes: 'Zoek kenmerken',
        attribute: 'Kenmerk',
      },
      attributeCategories: {
        systemAttributes: 'Systeemkenmerken',
        customAttributes: 'Gevonden kenmerken',
        objectAttributes: 'Objectkenmerken',
      },
      fields: {
        name: {
          label: 'Naam zoekopdracht',
          placeholder: 'Zoekopdracht naam',
          errorMessage: 'Vul een naam in voor de zoekopdracht.',
        },
        objectType: {
          label: 'Objecttype',
          placeholder: 'Zoek een objecttype…',
          errorMessage: 'Selecteer een objecttype.',
        },
        template: {
          label: 'Sjabloon',
        },
        filtersSelect: {
          label: 'Filters toevoegen',
          placeholder: 'Zoek een kenmerk…',
        },
        filters: {
          notImplementedYet: 'Dit filtertype is nog niet ontwikkeld.',
          addFilter: 'Nieuw filter toevoegen',
          label: 'Filters',
          noFiltersLabel: 'Deze zoekopdracht heeft nog geen filters.',
          typeNotSupported: 'Filtertype nog niet ondersteund: {{ type }}',
          fields: {
            generic: {
              yesNo: {
                choices: {
                  yes: 'Ja',
                  no: 'Nee',
                },
              },
              text: {
                filterOn: 'Filter op',
              },
              options: {
                errorMessageMin: 'Kies minimaal één optie.',
              },
              number: {
                errorMessageMin: 'Voer minimaal één nummer/getal in.',
                errorMessageNrs: 'Voer alleen nummers/getallen in.',
                placeholder: 'Voer nummers/getallen in…',
              },
              multiString: {
                errorMessage: 'Voer één of meer waardes in.',
                placeholder: 'Voer één of meer waardes in…',
              },
              multiOption: {
                errorMessageMin: 'Kies minimaal één optie.',
              },
              location: {
                addressType: 'Adres',
                streetType: 'Plek of straat',
                searchIn: 'Zoeken naar',
                placeholder: 'Typ om te zoeken…',
                errorMessage: 'Voer minimaal één adres in.',
                help: {
                  nummeraanduiding:
                    'Hiermee zoekt u in de BAG naar een adres, via een "nummeraanduiding". Dit is een aanduiding voor een verblijfsobject, een standplaats of een ligplaats. Voer een straatnaam, en (eventueel) een huisnummer in, om gericht te zoeken.',
                  openbareruimte:
                    'Hiermee zoekt u in de BAG naar een "openbare ruimte" (weg, water, spoorbaan, terrein, kunstwerk, landschappelijk gebied of administratief gebied). Een openbare ruimte is meestal een straat(naam). Voer een (deel) van de naam in om gericht te zoeken.',
                },
              },
            },
            status: {
              label: '$t(attributes:status)',
              errorMessage: 'Kies minimaal één optie.',
              choices: {
                active: 'Actief',
                inactive: 'Inactief',
                draft: 'Opzet',
              },
            },
            archiveStatus: {
              label: 'Archiveerstatus',
              placeholder: 'Kies een waarde…',
              errorMessage: 'Kies een waarde',
              choices: {
                archived: 'Gearchiveerd',
                toDestroy: 'Vernietigen',
                toPreserve: 'Bewaren',
              },
            },
            modified: {
              label: 'Laatst aangepast',
            },
            keyword: {
              label: 'Trefwoord',
            },
            price: {
              label: 'Prijs',
            },
            externalReference: {
              label: 'Externe referentie',
            },
            archivalState: {
              label: 'Archiefstatus',
              choices: {
                vernietigen: 'Te vernietigen',
                overdragen: 'Te bewaren of overdragen',
              },
              placeholder: 'Kies een waarde…',
              errorMessage: 'Kies een waarde.',
            },
            typeOfArchiving: {
              label: 'Archiefnominatie',
              choices: {
                'Bewaren (B)': 'Bewaren (B)',
                Conversie: 'Conversie',
                Migratie: 'Migratie',
                'Overbrengen (O)': 'Overbrengen (O)',
                Overdracht: 'Overdracht',
                Publicatie: 'Publicatie',
                'Vernietigen (V)': 'Vernietigen (V)',
                Vernietigen: 'Vernietigen',
                'Vervallen beperkingen openbaarheid':
                  'Vervallen beperkingen openbaarheid',
              },
            },
            requestor: {
              label: 'Aanvrager',
              placeholder: 'Typ om aanvrager te zoeken…',
              errorMessage: 'Voer minimaal één aanvrager in.',
            },
            assignee: {
              label: 'Behandelaar',
              placeholder: 'Typ om behandelaar te zoeken…',
              errorMessage: 'Voer minimaal één behandelaar in.',
            },
            coordinator: {
              label: 'Coördinator',
              placeholder: 'Typ om coördinator te zoeken…',
              errorMessage: 'Voer minimaal één coördinator in.',
              activeToggleLabel: 'Toon inactief',
            },
            relation: {
              placeholder: 'Typ om betrokkene te zoeken…',
              errorMessage: 'Voer minimaal één betrokkene in.',
              caseNotSupported:
                'Filteren op het relatietype "Zaak" wordt nog niet ondersteund.',
              filterCustomObject:
                'Filter op zaken die voor {{objectTypeName}} een ingevulde waarde hebben.',
            },
            urgency: {
              label: 'Urgentie',
              choices: {
                normal: 'Normaal',
                medium: 'Gemiddeld',
                high: 'Hoog',
                late: 'Te laat',
              },
            },
            caseType: {
              label: '$t(attributes:caseType)',
              placeholder: 'Typ om zaaktype te zoeken…',
              errorMessage: 'Kies minimaal één zaaktype.',
            },
            paymentStatus: {
              label: 'Betaalstatus',
              errorMessage: 'Kies minimaal één optie.',
              choices: {
                success: 'Geslaagd',
                pending: 'Wachten op bevestiging',
                failed: 'Niet geslaagd',
                offline: 'Later betalen',
              },
            },
            contactChannel: {
              label: 'Contactkanaal',
              errorMessage: 'Kies minimaal één optie.',
              choices: {
                behandelaar: 'Behandelaar',
                balie: 'Balie',
                telefoon: 'Telefoon',
                post: 'Post',
                email: 'E-mail',
                webformulier: 'Webformulier',
                'sociale media': 'Sociale media',
                'externe applicatie': 'Externe applicatie',
              },
            },
            departmentRole: {
              department: 'Afdeling',
              role: 'Rol',
              label: 'Afdeling/rol',
              departmentPlaceholder: 'Kies een afdeling…',
              roleDisabledPlaceholder: 'Kies eerst een afdeling…',
              rolePlaceholder: 'Kies een rol (optioneel)…',
              errors: {
                minEntries: 'Voeg minimaal één afdeling/rol-combinatie toe.',
              },
            },
            result: {
              label: '$t(attributes:result)',
              placeholder: 'Typ om optie(s) te vinden…',
              errorMessage: 'Kies minimaal één optie.',
              choices: {
                aangegaan: 'Aangegaan',
                aangehouden: 'Aangehouden',
                aangekocht: 'Aangekocht',
                aangesteld: 'Aangesteld',
                aanvaard: 'Aanvaard',
                afgeboekt: 'Afgeboekt',
                afgebroken: 'Afgebroken',
                afgehandeld: 'Afgehandeld',
                afgesloten: 'Afgesloten',
                afgewezen: 'Afgewezen',
                akkoord: 'Akkoord',
                'akkoord met wijzigingen': 'Akkoord met wijzigingen',
                beëindigd: 'Beëindigd',
                behaald: 'Behaald',
                betaald: 'Betaald',
                'buiten behandeling gesteld': 'Buiten behandeling gesteld',
                'definitief toegekend': 'Definitief toegekend',
                geannuleerd: 'Geannuleerd',
                'gedeeltelijk gegrond': 'Gedeeltelijk gegrond',
                'gedeeltelijk verleend': 'Gedeeltelijk verdeeld',
                gedoogd: 'Gedoogd',
                gegrond: 'Gegrond',
                gegund: 'Gegund',
                geïnd: 'Geïnd',
                geleverd: 'Geleverd',
                geweigerd: 'Geweigerd',
                gewijzigd: 'Gewijzigd',
                'handhaving uitgevoerd': 'Handhaving uitgevoerd',
                ingericht: 'Ingericht',
                ingeschreven: 'Ingeschreven',
                ingesteld: 'Ingesteld',
                ingetrokken: 'Ingetrokken',
                ingewilligd: 'Ingewilligd',
                'niet aangekocht': 'Niet aangekocht',
                'niet aangesteld': 'Niet aangesteld',
                'niet akkoord': 'Niet akkoord',
                'niet behaald': 'Niet behaald',
                'niet betaald': 'Niet betaald',
                'niet doorgegaan': 'Niet doorgegaan',
                'niet gegund': 'Niet gegund',
                'niet geïnd': 'Niet geïnd',
                'niet geleverd': 'Niet geleverd',
                'niet gewijzigd': 'Niet gewijzigd',
                'niet ingesteld': 'Niet ingesteld',
                'niet ingetrokken': 'Niet ingetrokken',
                'niet nodig': 'Niet nodig',
                'niet ontvankelijk': 'Niet ontvankelijk',
                'niet opgelegd': 'Niet opgelegd',
                'niet opgeleverd': 'Niet opgeleverd',
                'niet toegekend': 'Niet toegekend',
                'niet uitgevoerd': 'Niet uitgevoerd',
                'niet vastgesteld': 'Niet vastgesteld',
                'niet verkregen': 'Niet verkregen',
                'niet verleend': 'Niet verleend',
                'niet verstrekt': 'Niet verstrekt',
                'niet verwerkt': 'Niet verwerkt',
                ongegrond: 'Ongegrond',
                ontvankelijk: 'Ontvankelijk',
                opgeheven: 'Opgeheven',
                opgelegd: 'Opgelegd',
                opgeleverd: 'Opgeleverd',
                opgelost: 'Opgelost',
                opgezegd: 'Opgezegd',
                toegekend: 'Toegekend',
                'toezicht uitgevoerd': 'Toezicht uitgevoerd',
                uitgevoerd: 'Uitgevoerd',
                vastgesteld: 'Vastgesteld',
                verhuurd: 'Verhuurd',
                verkocht: 'Verkocht',
                verkregen: 'Verkregen',
                verleend: 'Verleend',
                vernietigd: 'Vernietigd',
                verstrekt: 'Verstrekt',
                verwerkt: 'Verwerkt',
                'voorlopig toegekend': 'Voorlopig toegekend',
                'voorlopig verleend': 'Voorlopig verleend',
              },
            },
            caseLocation: {
              label: 'Adres',
            },
            correspondenceAddress: {
              label: '$t(attributes:correspondenceAddress)',
            },
            registrationDate: {
              label: '$t(attributes:registrationDate)',
            },
            completionDate: {
              label: '$t(attributes:completionDate)',
            },
            destructionDate: {
              label: '$t(attributes:destructionDate)',
            },
            stalledUntil: {
              label: '$t(attributes:stalledUntil)',
            },
            stalledSince: {
              label: '$t(attributes:stalledSince)',
            },
            targetDate: {
              label: '$t(attributes:targetDate)',
            },
            parentNumber: {
              label: '$t(attributes:parentNumber)',
            },
            customNumber: {
              label: '$t(attributes:customNumber)',
            },
            phase: {
              label: '$t(attributes:phase)',
              placeholder: 'Vul een waarde in',
              errorMessage: 'Vul een waarde in.',
            },
            subject: {
              label: '$t(attributes:subject)',
              placeholder: 'Vul een waarde in',
              errorMessage: 'Vul een waarde in.',
            },
            caseNumber: {
              label: '$t(attributes:caseNumber)',
              caseNr_one: 'zaaknummer',
              caseNr_other: 'zaaknummers',
            },
            requestorIsSecret: {
              label: 'Indicatie geheim',
            },
            requestorInvestigation: {
              label: 'In onderzoek',
            },
            confidentiality: {
              label: 'Zaakvertrouwelijkheid',
              choices: {
                public: 'Openbaar',
                internal: 'Intern',
                confidential: 'Vertrouwelijk',
              },
              errorMessage: 'Kies minimaal één optie.',
            },
            hasUnacceptedChanges: {
              label: 'Openstaande handelingen',
              errorMessage: 'Kies minimaal één optie.',
              choices: {
                files: 'Ongeaccepteerde bestanden',
                updates: 'Openstaande wijzigingsvoorstellen',
                messages: 'Ongelezen berichten',
              },
            },
            periodOfPreservation: {
              label: '$t(attributes:triggerArchival)',
              choices: {
                active: 'Geactiveerd',
                'not-active': 'Niet geactiveerd',
              },
            },
            retentionPeriodSourceDate: {
              label: '$t(attributes:retentionPeriodSourceDate)',
              choices: {
                vervallen: 'Vervallen',
                onherroepelijk: 'Onherroepelijk',
                afhandeling: 'Afhandeling',
                verwerking: 'Verwerking',
                geweigerd: 'Geweigerd',
                verleend: 'Verleend',
                geboorte: 'Geboorte',
                'einde-dienstverband': 'Einde-dienstverband',
              },
              errorMessage: 'Kies minimaal één optie.',
            },
            casetypeVersion: {
              label: '$t(attributes:version)',
              placeholderCasetype: 'Zoek zaaktype…',
              placeholderVersion: 'Kies versie…',
              versionShort: `v.`,
              versionLong: 'versie',
              errors: {
                minEntries: 'Voeg minimaal één versie toe.',
              },
            },
            casetypeIdentification: {
              label: '$t(attributes:identification)',
            },
            casetypeConfidentiality: {
              label: 'Vertrouwelijkheidsaanduiding',
              choices: {
                Openbaar: 'Openbaar',
                'Beperkt openbaar': 'Beperkt openbaar',
                Intern: 'Intern',
                Zaakvertrouwelijk: 'Zaakvertrouwelijk',
                Vertrouwelijk: 'Vertrouwelijk',
                Confidentieel: 'Confidentieel',
                Geheim: 'Geheim',
                'Zeer geheim': 'Zeer geheim',
              },
            },
            requestorZipcode: {
              label: '$t(attributes:zipcode)',
            },
            requestorCorrespondenceZipcode: {
              label: `$t(correspondence) $t(attributes:zipcode)`,
            },
            requestorCorrespondenceStreet: {
              label: `$t(correspondence) $t(attributes:street)`,
            },
            requestorCorrespondenceCity: {
              label: `$t(correspondence) $t(attributes:city)`,
            },
            requestorStreet: {
              label: `$t(attributes:street)`,
            },
            requestorCity: {
              label: `$t(attributes:city)`,
            },
            requestorCocNumber: {
              label: 'KVK-nummer',
            },
            requestorCocLocationNumber: {
              label: 'Vestigingsnummer',
            },
            requestorNobleTitle: {
              label: 'Adellijke titel',
            },
            requestorTradeName: {
              label: 'Handelsnaam',
            },
            requestorGender: {
              label: 'Geslachtsaanduiding',
              choices: {
                // eslint-disable-next-line id-length
                M: 'Man',
                // eslint-disable-next-line id-length
                F: 'Vrouw',
                // eslint-disable-next-line id-length
                X: 'Anders',
              },
            },
            requestorDepartment: {
              label: 'Afdeling',
              placeholder: 'Kies één of meerdere afdelingen…',
              minEntriesErrorMessage: 'Kies minimaal één afdeling.',
            },
            requestorDateOfBirth: {
              label: 'Geboortedatum',
            },
            requestorDateOfMarriage: {
              label: 'Huwelijksdatum',
            },
            requestorDateOfDeath: {
              label: 'Overlijdensdatum',
            },
            objectUUID: {
              label: 'Object UUID',
            },
          },
          types: {
            text: {
              errorMessage: 'Vul een waarde in.',
              placeholder: 'Vul een waarde in…',
            },
            zipcode: {
              errorMessage: 'Vul een geldige postcode in.',
              placeholder: 'Vul postcode(s) in…',
            },
            cocNr: {
              errorMessage: 'Vul een geldig KVK-nummer in.',
              placeholder: 'Vul KVK-nummer(s) in…',
            },
            dateEntries: {
              datePlaceholder: 'Selecteer een datum',
              date: {
                placeholder: 'Selecteer een datum…',
                errorMessage: 'Selecteer een datum',
              },
              operators: {
                lt: 'Eerder dan',
                le: 'Eerder dan of op',
                gt: 'Later dan',
                ge: 'Later dan of op',
                eq: 'Op',
                ne: 'Niet op',
              },
              operator: {
                placeholder: 'Kies een optie…',
              },
              titles: {
                absolute: 'Vast datumpunt',
                relative: 'Relatief datumpunt',
                range: 'Periode',
              },
              manualInput: 'Handmatige invoer',
              quickSelect: 'Snelle selectie',
              intervals: {
                minutes: 'minuten',
                hours: 'uur',
                days: 'dagen',
                months: 'maanden',
                years: 'jaar',
              },
              inLabel: 'in',
              between: 'Tussen',
              and: 'en',
              from: 'Van',
              untill: 'Tot',
              presets: {
                now: 'Nu',
                today: 'Vandaag',
                lastDay: 'Een dag terug',
                lastHour: 'Een uur terug',
                last7Days: '7 dagen terug',
                last30Days: '30 dagen terug',
                lastYear: 'Een jaar terug',
              },
              placeholders: {
                relativeNr: 0,
                choose: 'Keuze…',
                time: 'Kies tijd…',
              },
              periods: {
                '-': 'het verleden',
                '+': 'de toekomst',
              },
              includeTimes: 'Inclusief start- en eindtijd',
              errors: {
                timeRange:
                  'De einddatum en -tijd moeten na de begindatum en -tijd liggen.',
                dateRange: 'De einddatum moet op of na de begindatum liggen.',
                minimumEntries: 'Voeg minimaal één datumpunt toe.',
              },
            },
            checkboxes: {
              minEntriesErrorMessage: 'Kies minimaal één optie.',
            },
            numberEntries: {
              placeholder: '0',
              operators: {
                eq: 'Gelijk aan',
                ne: 'Ongelijk aan',
                lt: 'Minder dan',
                le: 'Minder of gelijk aan',
                gt: 'Meer dan',
                ge: 'Meer of gelijk aan',
              },
              errors: {
                minEntries: 'Vul minimaal één regel in.',
              },
            },
            select: {},
            address_v2: {
              placeholder: 'Typ om te zoeken…',
            },
            valuta: {
              placeholder: 'Vul een valutawaarde in',
              errorMessage: 'Vul een geldige valutawaarde in.',
            },
            email: {
              placeholder: 'Vul een e-mailadres in',
              errorMessage: 'Vul een geldig e-mailadres in.',
            },
            numeric: {
              placeholder: 'Vul een nummer in',
              errorMessage: 'Vul een geldig nummer in.',
            },
            bankaccount: {
              placeholder: 'Vul bankrekeningnummer (IBAN) in',
              errorMessage: 'Vul een geldig bankrekeningnummer (IBAN) in.',
            },
            url: {
              placeholder: 'Vul een webadres in',
              errorMessage: 'Vul een geldig webadres in.',
            },
            date: {
              errorMessage: 'Vul een geldige datum in.',
            },
            file: {
              label:
                "Filter op zaken die minimaal één '{{name}}'-document hebben.",
              summary: 'Document aanwezig',
            },
            uuid: {
              placeholder: 'Voer één of meerdere Object UUID(s) in…',
              errorMessage: 'Vul een geldig UUID in.',
            },
          },
          operators: {
            and: 'En',
            or: 'Of',
            eq: 'Gelijk',
            ne: 'Ongelijk',
          },
          addFilters: {
            systemAttributes: 'Selecteer om toe te voegen…',
            searchAttributes: 'Typ om kenmerk te zoeken…',
          },
          customFieldOperators: {
            and: 'En',
            or: 'En/of',
          },
          help: {
            keywordCase: {
              intro:
                'Filteren op trefwoorden wordt toegepast op de volgende velden:',
              requestor:
                'Aanvrager: volledige naam, voornaam, achternaam, naam, BSN, e-mailadres.',
              recipient:
                'Ontvanger: volledige naam, voornaam, achternaam, BSN, e-mailadres.',
              assignee: 'Behandelaar: naam, e-mailadres, telefoonnummer.',
              other:
                'Zaak: coördinator, nummer, onderwerp, datum van registratie, datum van afronden, naam van zaaktype.',
            },
            keywordCustomObject: {
              intro:
                'Filteren op trefwoorden wordt toegepast op de volgende velden:',
              fields: 'Titel, subtitel, externe referentie, inhoud.',
            },
            urgency: {
              intro:
                'De urgentie van een zaak wordt bepaald op basis van de voortgang van de zaak:',
              normal: 'Normaal: van 0 tot 80%',
              medium: 'Gemiddeld: van 80% tot 90%',
              high: 'Hoog: van 90% tot 100%',
              late: 'Te laat: 100% en hoger',
            },
            phase: {
              help: 'Bij het filteren op zaakfase worden zaken teruggegeven waarvan de zaak zich in (één van) de opgegeven fase(s) bevindt. U kunt ook een deel van de naam van de fase opgeven.',
            },
            objectUUID: {
              help: 'Filter op zaken die een relatie hebben naar de opgegeven Object UUID(s).',
            },
          },
        },
        permissions: {
          hint: "Deze zoekopdracht is nog niet gedeeld. Maak een nieuwe afdeling/rol-combinatie aan via de 'Nieuw' knop.",
          mayEdit: 'Mag bewerken',
          add: 'Toevoegen',
          group: {
            label: 'Afdeling',
            placeholder: 'Kies een afdeling…',
            errorMessage: 'Kies een afdeling.',
          },
          role: {
            label: 'Rol',
            placeholder: 'Kies een rol…',
            errorMessage: 'Kies een rol.',
          },
          helpInfo: {
            intro:
              'Bij het aanmaken en bewerken van deze rechten gelden de volgende regels per Afdeling/Rol-combinatie:',
            owner:
              'De eigenaar van de zoekopdracht mag alle aspecten van de zoekopdracht bekijken en wijzigen. Bij het aanmaken van een nieuwe zoekopdracht wordt u automatisch de eigenaar.',
            sharedEditing:
              'De zoekopdracht is met een gebruiker gedeeld *met* bewerken aangevinkt: de gebruiker mag de zoekopdracht gebruiken en wijzigen, behalve het tabblad Delen.',
            sharedNotEditing:
              'De zoekopdracht is met een gebruiker gedeeld *zonder* bewerken aangevinkt: de gebruiker mag alleen de zoekopdracht gebruiken.',
          },
        },
        columns: {
          sortOn: 'Sorteer op {{ column }} ',
          changeVisibility: 'Wijzig zichtbaarheid van kolom',
          changeSorting: 'Wijzig sorteeropties',
          ascending: 'Oplopend',
          descending: 'Aflopend',
          sortOrder: 'Sorteervolgorde',
          errorMessage: 'Kies minimaal één kolom.',
          selectColumn_case: 'Zoek een kolom of kenmerk…',
          selectColumn_custom_object: 'Selecteer kolom…',
          categories: {
            systemAttributes: 'Systeemkenmerken',
            requestor: 'Aanvrager',
            assignee: 'Behandelaar',
            coordinator: 'Coördinator',
            correspondence: 'Correspondentie',
            dates: 'Datums',
            result: 'Resultaat',
            case: 'Zaak',
            caseType: 'Zaaktype',
            caseLocation: 'Zaaklocatie',
            other: 'Overig',
          },
        },
      },
      attributeFinder: {
        placeholder: 'Typ om kenmerk te zoeken…',
      },
    },
    summary: {
      expand: 'Klik om de actieve filters in/uit te klappen',
      activeFilters: 'actieve filters',
    },
    contacts: {
      all: 'Alles',
      employee: 'Medewerker',
      person: 'Persoon',
      organization: 'Organisatie',
    },
    labels: {
      allLabels: 'Alle labels',
      createNew: 'Maak nieuw label of selecteer…',
      filterLabels: 'Filter op labels…',
      editLabels: 'Labels bewerken',
      more: 'Meer…',
    },
    moreInfo: {
      labels: 'Labels',
      created: 'Aangemaakt',
      by: 'door',
      edited: 'Laatst gewijzigd',
      none: 'Geen',
      authorizations: 'Rechten',
      authorizationLevels: {
        admin: 'Beheren',
        readwrite: 'Bewerken',
        read: 'Raadplegen',
      },
      sharedBy: 'Gedeeld door',
    },
    expandCollapse: 'Menu in/uit klappen',
  },
};
