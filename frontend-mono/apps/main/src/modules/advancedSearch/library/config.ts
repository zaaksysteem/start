// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { v4 } from 'uuid';
import {
  FilterType,
  CustomAttributeTypes,
} from '../AdvancedSearch.types.filters';
import { ColumnType, DateModeType, KindType } from '../AdvancedSearch.types';

export const FILTERS_SUMMARY_DEFAULT_EXPANDED = false;
export const FILTERS_SUMMARY_MAX_HEIGHT = 200;
export const SAVED_SEARCHES_PAGE_LENGTH = 20;
export const DEFAULT_RESULTS_PER_PAGE = 50;
export const SKIP_PAGES = 5;
export const TABLE_SUMMARY_BREAKPOINT = 1024;
export const DATAGRID_KEY_PREFIX = 'advancedSearchDataGridState_';
export const LOCALSTATE_KEY = 'advancedSearchLocalState';
export const CHECKBOX_COLUMN_ID = '__check__';
export const PDOK_SUGGEST_URL =
  'https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest';
export const PDOK_LOOKUP_URL =
  'https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup';

// These fields are treated as a single value in the frontend, but are
// converted to/from arrays from/to the backend
export const FIELD_TYPES_AS_SINGLE_VALUE = [
  'attributes.value',
  'attributes.archival_state',
  'attributes.archive_status',
  'attributes.type_of_archiving',
  'relationship.custom_object_type',
  'attributes.urgency',
  'attributes.period_of_preservation_active',
];

export const FIELD_TYPES_YES_NO_BOOLEAN = [
  'relationship.requestor.investigation',
  'relationship.requestor.is_secret',
];

export const FIELD_TYPES_AS_OPTIONS_OBJECT = ['checkbox', 'select', 'option'];

export const SORTABLE_COLUMNS = [
  ['title'],
  ['subtitle'],
  ['date_created'],
  ['externalReference', 'external_reference'],
  ['lastModified', 'last_modified'],
  ['registrationDate', 'registration_date'],
  ['completionDate', 'completion_date'],
  ['caseNumber', 'number'],
];

export const filterTranslationKeys = {
  'attributes.last_modified': 'modified',
  'attributes.status': 'status',
  'attributes.archive_status': 'archiveStatus',
  'attributes.archival_state': 'archivalState',
  'attributes.type_of_archiving': 'typeOfArchiving',
  'attributes.urgency': 'urgency',
  'attributes.department_role': 'departmentRole',
  'relationship.requestor.id': 'requestor',
  'relationship.assignee.id': 'assignee',
  'relationship.coordinator.id': 'coordinator',
  'relationship.case_type.id': 'caseType',
  'attributes.payment_status': 'paymentStatus',
  'attributes.result': 'result',
  'attributes.channel_of_contact': 'contactChannel',
  'attributes.case_location': 'caseLocation',
  'attributes.correspondence_address': 'correspondenceAddress',
  'attributes.registration_date': 'registrationDate',
  'attributes.destruction_date': 'destructionDate',
  'attributes.target_date': 'targetDate',
  'attributes.stalled_until': 'stalledUntil',
  'attributes.stalled_since': 'stalledSince',
  'attributes.completion_date': 'completionDate',
  'attributes.confidentiality': 'confidentiality',
  'attributes.retention_period_source_date': 'retentionPeriodSourceDate',
  'attributes.has_unaccepted_changes': 'hasUnacceptedChanges',
  'attributes.period_of_preservation_active': 'periodOfPreservation',
  'attributes.parent_number': 'parentNumber',
  'attributes.custom_number': 'customNumber',
  'attributes.external_reference': 'externalReference',
  'attributes.case_number': 'caseNumber',
  'attributes.case_phase': 'phase',
  'attributes.subject': 'subject',
  'attributes.case_price': 'price',
  'relationship.case_type.version': 'casetypeVersion',
  'relationship.case_type.identification': 'casetypeIdentification',
  'relationship.case_type.confidentiality': 'casetypeConfidentiality',
  'relationship.requestor.investigation': 'requestorInvestigation',
  'relationship.requestor.is_secret': 'requestorIsSecret',
  'relationship.requestor.coc_location_number': 'requestorCocLocationNumber',
  'relationship.requestor.coc_number': 'requestorCocNumber',
  'relationship.requestor.noble_title': 'requestorNobleTitle',
  'relationship.requestor.trade_name': 'requestorTradeName',
  'relationship.requestor.gender': 'requestorGender',
  'relationship.requestor.department': 'requestorDepartment',
  'relationship.requestor.correspondence_zipcode':
    'requestorCorrespondenceZipcode',
  'relationship.requestor.correspondence_street':
    'requestorCorrespondenceStreet',
  'relationship.requestor.correspondence_city': 'requestorCorrespondenceCity',
  'relationship.requestor.street': 'requestorStreet',
  'relationship.requestor.city': 'requestorCity',
  'relationship.requestor.zipcode': 'requestorZipcode',
  'relationship.requestor.date_of_birth': 'requestorDateOfBirth',
  'relationship.requestor.date_of_marriage': 'requestorDateOfMarriage',
  'relationship.requestor.date_of_death': 'requestorDateOfDeath',
  'relationship.custom_object.id': 'objectUUID',
};

export const filtersKeyNamesReplacements = [
  ['startValue', 'start_value'],
  ['endValue', 'end_value'],
  ['timeSetByUser', 'time_set_by_user'],
  ['magicString', 'magic_string'],
  ['customFieldsOperator', 'custom_fields_operator'],
  ['additionalData', 'additional_data'],
];

/* eslint complexity: [2, 20] */
export const getFieldOperatorsConfig = ({
  filter,
  kind,
}: {
  filter: FilterType;
  kind: KindType;
}) => {
  const { type } = filter;

  if (type === 'attributes.value') {
    const subType = filter.values.type;

    if (kind === 'custom_object') {
      return {
        operators: ['or', 'and'],
        location: 'values.operator',
      };
    } else if (kind === 'case') {
      if (
        [
          'address_v2',
          'bag_adres',
          'bag_adressen',
          'bag_straat_adres',
          'bag_straat_adressen',
          'bag_openbareruimte',
          'bag_openbareruimtes',
          'select',
          'option',
          'date',
          'email',
          'numeric',
          'bankaccount',
        ].includes(subType)
      ) {
        return {
          operators: ['or'],
          location: 'values.operator',
        };
      } else if (
        ['checkbox', 'text', 'richtext', 'textarea'].includes(subType) ||
        subType.indexOf('valuta') > -1
      ) {
        return {
          operators: ['or', 'and'],
          location: 'values.operator',
        };
      } else if (['relationship'].includes(subType)) {
        if (
          !filter.values.additionalData ||
          filter.values?.additionalData?.type !== 'subject'
        )
          return { operators: [], location: null };
        return { operators: ['or'], location: 'values.operator' };
      } else if (['file'].includes(subType)) {
        return {
          operators: [],
        };
      } else {
        return {
          operators: ['eq', 'ne'],
          location: 'values.operator',
        };
      }
    }
  } else if (
    [
      'keyword',
      'attributes.external_reference',
      'attributes.case_price',
      'attributes.subject',
    ].includes(type)
  ) {
    return {
      operators: ['and', 'or'],
      location: 'operator',
    };
  } else if (
    [
      'relationship.assignee.id',
      'relationship.requestor.id',
      'relationship.coordinator.id',
      'relationship.case_type.id',
      'attributes.payment_status',
      'attributes.result',
      'attributes.confidentiality',
      'attributes.retention_period_source_date',
      'attributes.case_location',
      'attributes.correspondence_address',
      'attributes.status',
      'attributes.parent_number',
      'attributes.custom_number',
      'attributes.case_number',
      'attributes.external_reference',
      'attributes.case_phase',
      'relationship.case_type.version',
      'relationship.case_type.identification',
      'relationship.case_type.confidentiality',
      'relationship.requestor.is_secret',
      'relationship.requestor.investigation',
      'relationship.requestor.noble_title',
      'relationship.requestor.coc_location_number',
      'relationship.requestor.trade_name',
      'relationship.requestor.gender',
      'relationship.requestor.department',
      'relationship.requestor.correspondence_zipcode',
      'relationship.requestor.correspondence_street',
      'relationship.requestor.correspondence_city',
      'relationship.requestor.street',
      'relationship.requestor.city',
      'relationship.requestor.zipcode',
      'relationship.custom_object.id',
    ].includes(type)
  ) {
    return {
      operators: ['or'],
      location: 'operator',
    };
  } else if (
    [
      'attributes.registration_date',
      'attributes.completion_date',
      'attributes.destruction_date',
      'attributes.target_date',
      'attributes.stalled_until',
      'attributes.stalled_since',
      'relationship.requestor.date_of_birth',
      'relationship.requestor.date_of_marriage',
      'relationship.requestor.date_of_death',
    ].includes(type)
  ) {
    return {
      operators: ['or'],
      location: 'operator',
    };
  }
  return null;
};

export const getDateMode = (
  type: CustomAttributeTypes['type'] | FilterType['type']
): DateModeType => {
  return [
    'attributes.registration_date',
    'attributes.destruction_date',
    'attributes.completion_date',
    'attributes.target_date',
    'attributes.stalled_until',
    'attributes.stalled_since',
    'relationship.requestor.date_of_birth',
    'relationship.requestor.date_of_marriage',
    'relationship.requestor.date_of_death',
    'attributes.value',
    'date',
  ].includes(type)
    ? 'date'
    : 'datetime';
};

export const getArchiveExportTemplate = (): FilterType[] => {
  return [
    {
      type: 'attributes.status',
      uuid: v4(),
      values: ['resolved'],
    },
    {
      type: 'attributes.archival_state',
      uuid: v4(),
      values: 'overdragen',
    },
  ];
};

export const getSystemColumns = (kind?: KindType): ColumnType[] => {
  return kind === 'case'
    ? [
        {
          uuid: 'summaryStatus',
          type: 'summaryStatus',
          label: '',
          source: ['attributes', 'status'],
          visible: true,
          columnProps: {
            width: 40,
            flex: 0,
          },
        },
        {
          uuid: 'summaryDate',
          type: 'summaryDate',
          label: '',
          source: ['attributes'],
          visible: true,
          columnProps: {
            width: 96,
            flex: 0,
          },
        },
        {
          uuid: 'summaryContents',
          type: 'summaryContents',
          label: '',
          source: ['attributes'],
          includes: ['case_type', 'assignee', 'requestor'],
          visible: true,
        },
        {
          uuid: 'summaryDays',
          type: 'summaryDays',
          label: '',
          source: ['attributes', 'days'],
          visible: true,
          columnProps: {
            width: 50,
            flex: 0,
          },
        },
        {
          uuid: 'assigneecontrols',
          type: 'assigneeControls',
          label: '',
          source: ['relationships', 'role'],
          visible: true,
          columnProps: {
            width: 120,
            flex: 0,
          },
          includes: ['assignee', 'case_role_department'],
        },
      ]
    : [];
};
