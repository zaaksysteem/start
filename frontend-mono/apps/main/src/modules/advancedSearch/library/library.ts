// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { matchPath } from 'react-router';
import { AuthorizationsType, ColumnType } from '../AdvancedSearch.types';
import {
  FilterType,
  FilterCustomAttributeType,
} from '../AdvancedSearch.types.filters';

export const getUniqueColumnIdentifier = (column: ColumnType) => {
  if (column.magicString) {
    return column.magicString;
  } else {
    return column.type;
  }
};

export const isUUID = (str?: string) => {
  if (!str) return false;
  const reg = new RegExp(
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  );
  return reg.test(str);
};

export const getFromQueryValues = <T = any>(
  values: any,
  name: string
): T | null =>
  Object.entries(values).reduce((acc, current) => {
    if (current[0] === name) {
      acc = current[1] as T;
    }
    return acc;
  }, null as any);

export const hasAccess = (
  authorizations: AuthorizationsType[],
  level: AuthorizationsType
): boolean => {
  if (!authorizations) return false;
  if (level === 'read')
    return authorizations.some(
      (auth: any) => ['admin', 'readwrite', 'read'].indexOf(auth) >= 0
    );
  if (level === 'readwrite')
    return authorizations.some(
      (auth: any) => ['admin', 'readwrite'].indexOf(auth) >= 0
    );
  if (level === 'admin')
    return authorizations.some((auth: any) => ['admin'].indexOf(auth) >= 0);
  return false;
};

export const flat = (obj: any, out: any) => {
  if (!obj || !Object.keys(obj).length) return out;
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] == 'object') {
      out = flat(obj[key], out);
    } else {
      out[key] = obj[key];
    }
  });
  return out;
};

export const replaceKeysInJSON = (data: any, replaceArr: any[]) => {
  const replaced = replaceArr.reduce(
    (acc: any, current: string[]) =>
      acc.replaceAll(`"${current[0]}"`, `"${current[1]}"`),
    JSON.stringify(data)
  );
  return JSON.parse(replaced);
};

export function updateObject(object: any, newValue: any, path: any) {
  var stack = path.split('.');
  while (stack.length > 1) {
    object = object[stack.shift()];
  }

  const shifted = stack.shift();
  object[shifted] = { ...object[shifted], ...newValue };
}

export const getTypeOrSubType = (filter: FilterType) =>
  isCustomAttribute(filter) ? filter.values.type : filter.type;

export const isCustomAttribute = (
  filter: FilterType
): filter is FilterCustomAttributeType => filter.type === 'attributes.value';

export const mapTranslationToChoice = ([value, label]: [string, string]) => ({
  value,
  label,
});

export const getURLMatches = () =>
  matchPath(
    { path: `/:prefix/:module/:kind?/:mode?/:identifier?` },
    location.pathname
  );
