// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
//@ts-ignore
import iban from 'iban';
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  REGEXP_DATE_DAY,
  REGEXP_EMAIL_OR_MAGIC_STRING,
  REGEXP_NUMERIC,
  REGEXP_TEXT,
  REGEXP_URL,
  REGEXP_ZIPCODE,
  REGEXP_COCNR,
  REGEXP_UUID,
} from '../../../../../../packages/common/src/constants/regexes';
import { ValidateFuncType } from '../AdvancedSearch.types';

export const validate =
  (identifier: string, errorMessage: string, regExpType: string) =>
  (value: any): ReturnType<ValidateFuncType<any>> => {
    const map = {
      url: REGEXP_URL,
      numeric: REGEXP_NUMERIC,
      email: REGEXP_EMAIL_OR_MAGIC_STRING,
      text: REGEXP_TEXT,
      dateDay: REGEXP_DATE_DAY,
      zipcode: REGEXP_ZIPCODE,
      cocNr: REGEXP_COCNR,
      uuid: REGEXP_UUID,
      bankaccount: () => iban.isValid(value),
    } as any;

    if (value === null || value === undefined) return;
    const errorObj = {
      uuid: identifier,
      value: errorMessage,
    };

    if (typeof map[regExpType] === 'function') {
      return map[regExpType](value) ? undefined : errorObj;
    } else {
      //@ts-ignore
      const regExp = new RegExp(map[regExpType], 'i');
      return regExp.test(value) ? undefined : errorObj;
    }
  };

export const validateMinSelected =
  (identifier: string, errorMessage: string) => (value: any) =>
    !isPopulatedArray(value)
      ? {
          value: errorMessage,
          uuid: identifier,
        }
      : undefined;

export const validateNumbers =
  (identifier: string, errorMessage: string) => (value: ValueType<any>[]) => {
    if (!value) return;

    return value.some(element => !Number.isFinite(Number(element.value)))
      ? {
          value: errorMessage,
          uuid: identifier,
        }
      : undefined;
  };
