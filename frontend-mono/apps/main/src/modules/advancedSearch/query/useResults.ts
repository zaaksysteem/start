// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Dispatch, MutableRefObject, SetStateAction } from 'react';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { asArray } from '@mintlab/kitchen-sink/source';
import * as i18next from 'i18next';
import fecha from 'fecha';
import set from 'date-fns/set';
//@ts-ignore
import objectScan from 'object-scan';
//@ts-ignore
import addDuration from 'date-fns-duration';
import { useQuery, UseQueryOptions, QueryClient } from '@tanstack/react-query';
import add from 'date-fns/add';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { get } from '@mintlab/kitchen-sink/source';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { APIGeo } from '@zaaksysteem/generated/types/APIGeo.types';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import {
  ColumnType,
  IdentifierType,
  KindType,
  SortDirectionType,
  ResultRowType,
  ParseResultsModeType,
  GetResultsReturnType,
  CursorCollectionType,
  SkipDirectionType,
  CapabilitiesType,
} from '../AdvancedSearch.types';
import {
  FilterType,
  DateEntryType,
  FiltersType,
  FilterCustomAttributeType,
} from '../AdvancedSearch.types.filters';
import {
  getFromQueryValues,
  getUniqueColumnIdentifier,
} from '../library/library';
import { updateObject, isCustomAttribute } from '../library/library';
import { getDateMode, SORTABLE_COLUMNS } from '../library/config';
import { getResultValue } from '../components/Main/Results/Results.library';
import { QUERY_KEY_RESULTS, QUERY_KEY_RESULTS_COUNT } from './constants';

const COUNT_STALE_TIME = 60 * 1000;
const DATE_FORMAT = 'YYYY-MM-DD';

// Rows from the search API --> Table rows
type APIResultsToResultsType = {
  classes?: any;
  columns: ColumnType[];
  t: i18next.TFunction;
  parseResultsMode: ParseResultsModeType;
  data?: GetResultsReturnType;
  kind?: KindType;
};

// Rows from the search API --> Table rows
export const APIResultsToResults = ({
  classes,
  data,
  columns,
  t,
  parseResultsMode,
}: APIResultsToResultsType): ResultRowType[] | null => {
  if (!data) return [];
  const { data: rows, included } = data;

  if (!rows || !isPopulatedArray(rows)) return [];

  return rows.map((row: any) => {
    if (row.relationships) {
      Object.entries(row.relationships).forEach(val => {
        const keyName = val[0];
        //@ts-ignore-next-line
        const { id, type } = val[1].data;
        const filterFn = ({ value }: any) =>
          value?.type === type && value?.id === id && value?.attributes;
        const includedPath = objectScan(['**'], {
          joined: true,
          filterFn,
        })(included).filter((path: any) => path.indexOf('relationship') < 0);
        if (isPopulatedArray(includedPath)) {
          updateObject(
            row.relationships,
            get(included, includedPath[0]),
            `${keyName}.data`
          );
        }
      });
    }
    let columnsObj: ResultRowType['columns'] = {};
    const {
      id,
      attributes: { version_independent_uuid = '' },
      geoFeatures,
    } = row;
    columns.map(column => {
      const resultValue = getResultValue({
        column,
        row,
        t,
        parseResultsMode,
        classes,
      });
      if (resultValue)
        columnsObj[getUniqueColumnIdentifier(column)] = resultValue;
    });
    return {
      columns: columnsObj,
      uuid: id,
      versionIndependentUuid: version_independent_uuid,
      geoFeatures,
    };
  });
};

/* eslint complexity: [2, 100] */
export const filtersToAPIFilters = (
  kind: KindType,
  filters?: FiltersType | null,
  keyword?: string | null
) => {
  /**
   * Parses a collection of DateEntries to values the search API can understand.
   * Accounts for:
   * - the 3 scenarios the user may enter via UI: absolute, relative, range
   * - whether the entry represents just a date or a datetime
   * - whether the backend accepts just a date or a datetime
   */
  const dateEntriesToSearchFilters = (
    group: FilterType,
    dateTimeAPI = true
  ) => {
    const base = isCustomAttribute(group) ? group.values : group;
    const filterType = base.type;
    if (!base.values) return;

    //@ts-ignore
    return base.values.flatMap((dateEntry: DateEntryType) => {
      const { type } = dateEntry;

      if (type === 'absolute' && dateEntry.operator) {
        if (getDateMode(filterType) === 'date') {
          if (dateTimeAPI) {
            const dayLater = add(new Date(dateEntry.value as string), {
              days: 1,
            });
            if (['gt', 'le'].includes(dateEntry.operator)) {
              return `${dateEntry.operator} ${dayLater.toISOString()}`;
            } else if (dateEntry.operator === 'eq') {
              return [
                [`gt ${dateEntry.value}`, `lt ${dayLater.toISOString()}`],
              ];
            } else {
              return `${dateEntry.operator} ${dateEntry.value}`;
            }
          } else {
            return `${dateEntry.operator} ${fecha.format(
              new Date(dateEntry.value as string),
              DATE_FORMAT
            )}`;
          }
        } else {
          return `${dateEntry.operator} ${dateEntry.value}`;
        }
      } else if (type === 'relative') {
        const baseDate = set(new Date(), {
          hours: 0,
          minutes: 0,
          seconds: 0,
          milliseconds: 0,
        });

        if (dateTimeAPI) {
          const dayLater = add(baseDate, { days: 1 });
          const dateToAddTo =
            dateEntry.operator && ['gt', 'le'].includes(dateEntry.operator)
              ? dayLater
              : baseDate;
          const calculatedDate = addDuration(dateToAddTo, dateEntry.value);
          return `${dateEntry.operator} ${calculatedDate.toISOString()}`;
        } else {
          const calculatedDate = addDuration(baseDate, dateEntry.value);
          return `${dateEntry.operator} ${fecha.format(
            calculatedDate,
            DATE_FORMAT
          )}`;
        }
      } else if (dateEntry.type === 'range') {
        if (dateEntry.timeSetByUser === true) {
          return [[`ge ${dateEntry.startValue}`, `le ${dateEntry.endValue}`]];
        } else {
          if (dateTimeAPI) {
            const dayLater = add(new Date(dateEntry.endValue as string), {
              days: 1,
            });
            return [
              [`gt ${dateEntry.startValue}`, `lt ${dayLater.toISOString()}`],
            ];
          } else {
            return [
              [
                `ge ${fecha.format(new Date(dateEntry.startValue as string), DATE_FORMAT)}`,
                `le ${fecha.format(new Date(dateEntry.endValue as string), DATE_FORMAT)}`,
              ],
            ];
          }
        }
      }
    });
  };

  const customFieldToSearchFilters = (group: FilterType[]) => {
    const getValues = (filter: FilterCustomAttributeType) => {
      const { type, values, additionalData } = filter.values;

      if (kind === 'custom_object') {
        //@ts-ignore
        return values.map((value: ValueType<string>) => value.value);
      } else if (kind === 'case') {
        switch (type) {
          case 'bag_adres':
          case 'bag_adressen':
          case 'bag_straat_adres':
          case 'bag_straat_adressen':
          case 'bag_openbareruimte':
          case 'bag_openbareruimtes':
          case 'address_v2':
            //@ts-ignore
            return values.map((value: any) => value.id);
          case 'relationship':
            return additionalData?.type === 'subject'
              ? //@ts-ignore
                values.map((value: ValueType<string>) => value.value)
              : [];
          case 'valuta':
          case 'valutaex':
          case 'valutaex21':
          case 'valutaex6':
          case 'valutain':
          case 'valutain21':
          case 'valutain6':
            //@ts-ignore
            return values.map((value: any) => `${value[0]} ${value[1]}`);
          case 'email':
          case 'numeric':
          case 'bankaccount':
          case 'text':
          case 'textarea':
          case 'richtext':
            //@ts-ignore
            return values.map((value: ValueType<string>) => value.value);
          case 'date':
            return dateEntriesToSearchFilters(filter, false);
          case 'file':
            return null;
          case 'checkbox':
          case 'option':
          case 'select':
            return values;
        }
      }
    };

    const getOperator = (group: FilterCustomAttributeType) =>
      group?.values?.operator || null;

    const processed = group.map(thisGroup => {
      if (thisGroup.type === 'attributes.value') {
        const {
          values: { magicString, type, additionalData },
        } = thisGroup;
        const parsedValues = getValues(thisGroup);

        const operator = getOperator(thisGroup);
        return {
          magic_string: magicString,
          type:
            type === 'relationship' && additionalData?.type
              ? `relationship_${additionalData.type}`
              : type,
          ...(parsedValues ? { values: parsedValues } : {}),
          ...(operator ? { operator } : {}),
        };
      }
    });

    return {
      values: processed,
    };
  };

  const unacceptedChangesToSearchFilters = (group: FilterType) => {
    if (group.type === 'attributes.has_unaccepted_changes') {
      return {
        ...(group.values.includes('files') && {
          'attributes.num_unaccepted_files': ['gt 0'],
        }),
        ...(group.values.includes('messages') && {
          'attributes.num_unread_messages': ['gt 0'],
        }),
        ...(group.values.includes('updates') && {
          'attributes.num_unaccepted_updates': ['gt 0'],
        }),
      };
    }
  };

  //@ts-ignore-next-line
  const filterValueToAPIValue = (
    type: FilterType['type'],
    name: string,
    group: FilterType[]
  ) => {
    switch (type) {
      case 'relationship.case_type.version': {
        return {
          //@ts-ignore-next-line
          [name]: group[0].values.map(value => value.casetype_version_uuid),
        };
      }
      case 'attributes.case_price':
        return {
          [name]: {
            //@ts-ignore-next-line
            values: group[0].values.map(value => `${value[0]} ${value[1]}`),
            //@ts-ignore-next-line
            operator: group[0].operator,
          },
        };
      case 'attributes.value': {
        return { [name]: customFieldToSearchFilters(group) };
      }
      case 'keyword':
      case 'attributes.external_reference':
      case 'attributes.case_phase':
      case 'attributes.subject':
        // case 'attributes.external_reference':
        return {
          [name]: {
            //@ts-ignore-next-line
            operator: group[0].operator,
            values: group.flatMap(thisGroup =>
              //@ts-ignore-next-line
              thisGroup.values.map(value => value.value)
            ),
          },
        };
      case 'attributes.status':
      case 'attributes.archive_status':
      case 'attributes.archival_state':
      case 'attributes.type_of_archiving':
      case 'attributes.urgency':
      case 'attributes.channel_of_contact':
      case 'attributes.confidentiality':
      case 'attributes.retention_period_source_date':
        //@ts-ignore-next-line
        return { [name]: asArray(group[0].values) };
      case 'attributes.last_modified':
      case 'attributes.registration_date':
      case 'attributes.destruction_date':
      case 'attributes.target_date':
      case 'attributes.stalled_until':
      case 'attributes.stalled_since':
      case 'attributes.completion_date':
      case 'relationship.requestor.date_of_birth':
      case 'relationship.requestor.date_of_death':
      case 'relationship.requestor.date_of_marriage':
        //@ts-ignore-next-line
        return { [name]: dateEntriesToSearchFilters(group[0]) };
      case 'relationship.requestor.id':
      case 'relationship.assignee.id':
      case 'relationship.coordinator.id':
      case 'relationship.case_type.id':
      case 'attributes.parent_number':
      case 'attributes.custom_number':
      case 'attributes.case_number':
      case 'relationship.case_type.identification':
      case 'relationship.requestor.coc_number':
      case 'relationship.requestor.zipcode':
      case 'relationship.requestor.coc_location_number':
      case 'relationship.requestor.noble_title':
      case 'relationship.requestor.trade_name':
      case 'relationship.requestor.department':
      case 'relationship.requestor.city':
      case 'relationship.requestor.street':
      case 'relationship.requestor.correspondence_city':
      case 'relationship.requestor.correspondence_street':
      case 'relationship.requestor.correspondence_zipcode':
      case 'relationship.custom_object.id':
        return {
          //@ts-ignore-next-line
          [name]: group[0].values.map(value => value.value),
        };
      case 'relationship.custom_object_type':
        //@ts-ignore-next-line
        return { [name]: group[0].values.value };
      case 'attributes.payment_status':
        //@ts-ignore-next-line
        return { [name]: group[0].values };
      case 'attributes.department_role':
        return {
          [name]: {
            //@ts-ignore-next-line
            values: group[0].values.map(value => ({
              department_uuid: value.department.value,
              role_uuid: value.role ? value.role?.value : null,
            })),
          },
        };
      case 'attributes.case_location':
      case 'attributes.correspondence_address':
        return {
          [name]: {
            //@ts-ignore-next-line
            values: group[0].values.map(value => value.id),
          },
        };
      case 'attributes.has_unaccepted_changes':
        return unacceptedChangesToSearchFilters(group[0]);
      case 'attributes.period_of_preservation_active':
        //@ts-ignore-next-line
        return { [name]: group[0].values === 'active' };
      case 'relationship.requestor.investigation':
      case 'relationship.requestor.is_secret':
        return { [name]: Boolean(group[0].values === 'yes') };
      case 'intake':
        return { [name]: true };
      default:
        //@ts-ignore-next-line
        return { [name]: group[0].values };
    }
  };

  const getName = (type: string) => {
    switch (type) {
      case 'relationship.custom_object_type':
        return 'relationship.custom_object_type.id';
      default:
        return type;
    }
  };

  if (!filters) return [];

  let grouped = filters.filters.reduce((acc: any, filter: FilterType) => {
    const { type } = filter;
    if (!acc[type]) acc[type] = [];
    acc[type].push(filter);
    return acc;
  }, {});

  let filtersObj = Object.entries(grouped).reduce((acc: any, group: any) => {
    const [type, filters] = group;
    const name = getName(type);
    const result = filterValueToAPIValue(type, name, filters);

    if (result) acc = { ...acc, ...result };

    return acc;
  }, {});

  if (keyword) {
    const base = filtersObj?.keyword || {
      operator: 'or',
    };
    filtersObj.keyword = {
      ...base,
      values: [...(filtersObj?.keyword?.values || []), keyword],
    };
  }

  if (filters?.operator) filtersObj.operator = filters.operator;

  return filtersObj;
};

const getIncludedParams = (
  columns: ColumnType[] | null,
  kind: KindType,
  capabilities?: CapabilitiesType
) => {
  if (!columns || !isPopulatedArray(columns)) return null;
  let includes = columns.reduce((acc, current) => {
    if (isPopulatedArray(current.includes as any)) {
      acc.push(...(current.includes as any));
    } else if (
      isPopulatedArray(current.source) &&
      current.source[0].includes('relationship')
    )
      acc.push(current.source[1]);
    return acc;
  }, [] as string[]);

  includes =
    kind === 'case' && capabilities?.summary
      ? [...includes, 'assignee', 'requestor']
      : includes;
  includes = includes.filter(
    (value, index, arr) => arr.indexOf(value) === index
  ); //remove duplicates

  return isPopulatedArray(includes) ? includes : null;
};

// const getCustomFields = (columns: ColumnType[] | null) => {
//   if (!columns || !isPopulatedArray(columns)) return null;
//   const customFields = columns
//     .reduce((acc, current) => {
//       if (current.magicString) acc.push(current.magicString);
//       return acc;
//     }, [] as string[])
//     .filter((value, index, arr) => arr.indexOf(value) === index);
//   return isPopulatedArray(customFields) ? customFields : null;
// };

/* eslint complexity: [2, 100] */
export const getResults =
  ({
    count,
    fullContent,
    capabilities,
  }: {
    count: boolean;
    fullContent: boolean;
    capabilities?: CapabilitiesType;
  }) =>
  async (params: any): Promise<GetResultsReturnType> => {
    const { queryKey } = params;
    const index = queryKey.length - 1;
    const kind = getFromQueryValues<KindType>(
      queryKey[index],
      'kind'
    ) as KindType;
    const page = queryKey[1];
    const page_size = getFromQueryValues<number>(
      queryKey[index],
      'resultsPerPage'
    );
    const sortColumn = getFromQueryValues<string>(
      queryKey[index],
      'sortColumn'
    );
    const sortOrder = getFromQueryValues<SortDirectionType>(
      queryKey[index],
      'sortOrder'
    );
    const geo = getFromQueryValues<boolean>(queryKey[index], 'geo');
    const filters = getFromQueryValues<FiltersType | null>(
      queryKey[index],
      'filters'
    );
    const columns = getFromQueryValues<ColumnType[]>(
      queryKey[index],
      'columns'
    );
    const keyword = getFromQueryValues<string>(queryKey[index], 'keyword');
    const includes = getIncludedParams(columns, kind, capabilities);
    const cursor = getFromQueryValues<string>(queryKey[index], 'cursor');

    const url = `${
      kind === 'custom_object'
        ? '/api/v2/cm/custom_object/search'
        : '/api/v2/cm/case/search'
    }${count ? '/count' : ''}`;

    const getSortColumn = () => {
      const foundColumn = SORTABLE_COLUMNS.find(col => col[0] === sortColumn);
      if (!foundColumn) return;
      return `${sortOrder === 'desc' ? '-' : ''}${foundColumn[1] || foundColumn[0]}`;
    };
    const sort = getSortColumn() && sortOrder ? getSortColumn() : null;

    let parsedFilters = filtersToAPIFilters(kind, filters, keyword);
    if (filters?.operator) parsedFilters.operator = filters.operator;

    const postData = {
      ...(cursor && count === false && { cursor }),
      ...(page_size && { page_size }),
      ...(filters && {
        filters: parsedFilters,
      }),
      ...(sort && { sort }),
      ...(includes && !count && { includes }),
      ...(fullContent === true && kind === 'case' && { full_content: true }),
      //...(customFields && { custom_fields: customFields }),
    };

    let results = await request<any>('POST', url, postData).catch(
      (serverError: V2ServerErrorsType) => {
        if (!count) throw serverError;
      }
    );

    if (count) return results?.data?.attributes?.total_results || 0;

    if (geo && results && isPopulatedArray(results?.data)) {
      const geoURL = buildUrl('/api/v2/geo/get_geo_features', {
        uuid: results?.data.map(
          (result: any) =>
            result.attributes.version_independent_uuid || result.id
        ),
      });
      const geoResults = await request<APIGeo.GetGeoFeaturesResponseBody>(
        'GET',
        geoURL
      ).catch((serverError: V2ServerErrorsType) => {
        throw serverError;
      });

      results.data = results.data.map((result: any) => {
        const features =
          geoResults.data.find(
            geoResult =>
              geoResult.id === result.attributes.version_independent_uuid
          )?.attributes?.geo_json?.features || null;
        return {
          ...result,
          ...(features &&
            Array.isArray(features) &&
            features.length && { geoFeatures: features }),
        };
      });
    }

    return {
      data: results.data || [],
      ...(results.included && { included: results.included }),
      page,
      meta: results.meta,
    };
  };

export type UseResultsQueryQueryParamsType = {
  identifier?: IdentifierType | null;
  resultsPerPage: number;
  kind: KindType;
  geo: boolean;
  sortOrder?: SortDirectionType | null;
  sortColumn?: string | null;
  filters?: FiltersType | null;
  columns?: ColumnType[];
  keyword?: string;
  cursor?: string | null;
};
export type GetQueryConfigParamsType = {
  fullContent?: boolean;
  queryParams: Partial<UseResultsQueryQueryParamsType>;
  page?: number | null;
  queryKey?: string;
  capabilities?: CapabilitiesType;
} & Partial<Omit<UseQueryOptions, 'queryKey'>>;

export const useResultsQuery = ({
  queryKey,
  queryParams,
  enabled,
  keepPreviousData,
  onError,
  onSuccess,
  page,
  capabilities,
}: GetQueryConfigParamsType) =>
  useQuery<any>(
    getQueryConfig({
      queryKey,
      queryParams,
      enabled,
      keepPreviousData,
      onSuccess,
      onError,
      page,
      capabilities,
    })
  );

// Fetches the intermediate pages between the current page, and the
// target page
export const prefetchPages = async ({
  page,
  pages,
  direction,
  setPage,
  queryClient,
  queryParams,
  prevPageRef,
  cursorCollectionRef,
  cursorCollection,
}: {
  page: number;
  pages: number;
  direction: SkipDirectionType;
  setPage: Dispatch<SetStateAction<number>>;
  queryClient: QueryClient;
  queryParams: UseResultsQueryQueryParamsType;
  prevPageRef: MutableRefObject<number | null>;
  cursorCollectionRef: MutableRefObject<CursorCollectionType | null>;
  cursorCollection?: CursorCollectionType | null;
}) => {
  let end = direction === 'forward' ? pages + page : page - pages;
  const start = direction === 'forward' ? page + 1 : page - 1;
  let previousPage = page;
  let thisCursorCollection = cursorCollection;

  for (
    let counter = start;
    direction === 'forward' ? counter < end : counter > end;
    direction === 'forward' ? counter++ : counter--
  ) {
    let config = getQueryConfig({
      page: counter,
      queryParams: {
        ...queryParams,
        cursor:
          direction === 'forward'
            ? thisCursorCollection?.next
            : thisCursorCollection?.previous,
      },
    });
    const pageResult = (await queryClient
      .fetchQuery(config)
      .catch(openServerError)) as GetResultsReturnType;
    // @ts-ignore
    thisCursorCollection = pageResult?.meta?.cursor;
    previousPage = counter;
  }

  if (prevPageRef) {
    prevPageRef.current = previousPage;
  }
  if (thisCursorCollection) cursorCollectionRef.current = thisCursorCollection;
  setPage(end);
};

export const useResultsCountQuery = ({
  queryKey = QUERY_KEY_RESULTS_COUNT,
  queryParams,
  onError,
  enabled,
  keepPreviousData = true,
  onSuccess,
}: any) => {
  return useQuery(
    [queryKey, 0, queryParams],
    getResults({ count: true, fullContent: false }),
    {
      onSuccess,
      onError: onError || openServerError,
      enabled,
      keepPreviousData,
      staleTime: COUNT_STALE_TIME,
    }
  );
};

export const getQueryConfig = ({
  queryParams,
  onSuccess,
  onError,
  queryKey = QUERY_KEY_RESULTS,
  enabled = true,
  keepPreviousData = true,
  page,
  fullContent = false,
  capabilities,
}: GetQueryConfigParamsType): UseQueryOptions<GetResultsReturnType> => {
  const {
    identifier,
    resultsPerPage,
    kind,
    geo,
    sortColumn,
    sortOrder,
    filters,
    columns,
    keyword,
    cursor,
  } = queryParams;

  return {
    queryKey: [
      queryKey,
      page,
      {
        identifier,
        resultsPerPage,
        kind,
        sortColumn,
        sortOrder,
        filters,
        geo,
        columns,
        keyword,
        cursor,
      },
    ],
    queryFn: getResults({
      count: false,
      fullContent,
      capabilities,
    }),
    //@ts-ignore
    onError: onError || openServerError,
    onSuccess,
    enabled,
    keepPreviousData,
  };
};
