// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useFieldSetStyles = makeStyles(
  ({ mintlab: { greyscale }, typography }: Theme) => ({
    fieldSetWrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
    },
    fieldsWrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      marginBottom: 20,
    },
    fieldWrapper: {
      display: 'flex',
      gap: 20,
      maxWidth: 800,
      '&>*:nth-of-type(1)': {
        maxWidth: 300,
      },
    },
    fieldWrapperCompact: {
      display: 'flex',
      flexDirection: 'column',
      gap: 5,
    },
    labelWrapper: {
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 8,
      minWidth: 300,
    },
    label: {
      paddingTop: 2,
    },
    input: {
      flexGrow: 1,
    },
    inputBackgroundColor: {
      backgroundColor: greyscale.light,
    },
    icon: {
      width: 20,
      color: greyscale.evenDarker,
    },
  })
);
