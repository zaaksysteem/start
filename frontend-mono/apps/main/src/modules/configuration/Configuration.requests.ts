// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  FetchDataType,
  SubmitDataType,
  SearchResponseBodyType,
  SearchV1Type,
  SearchV2ResponseBodyType,
  SearchV2Type,
  ConfigResponseBodyType,
} from './Configuration.types';

export const fetchData: FetchDataType = async () => {
  const url = '/api/v1/config/panel';

  const response = await request<ConfigResponseBodyType>('GET', url);

  return response.result.instance;
};

export const submitData: SubmitDataType = async data => {
  const url = '/api/v1/config/update';

  await request('POST', url, data);
};

export const searchV1: SearchV1Type = async (baseUrl, match, keyword) => {
  const url = buildUrl(baseUrl, {
    [`query:match:${match}`]: match ? keyword : undefined,
    rows_per_page: 200,
  });

  const response = await request<SearchResponseBodyType>('GET', url);

  return response.result.instance.rows;
};

export const searchV2: SearchV2Type = async (baseUrl, match, keyword) => {
  const url = buildUrl(baseUrl, {
    [match]: keyword,
    rows_per_page: 200,
  });

  const response = await request<SearchV2ResponseBodyType>('GET', url);

  return response.data;
};
