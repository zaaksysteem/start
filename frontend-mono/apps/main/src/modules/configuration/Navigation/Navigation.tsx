// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { SectionIdType, SettingsType } from './../Configuration.types';

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }) => <Link to={href || ''} {...restProps} />
);

const Navigation: React.ComponentType<{
  sectionId: SectionIdType;
  settings: SettingsType;
}> = ({ sectionId, settings }) => {
  const [t] = useTranslation('configuration');

  const menuItems: SideMenuItemType[] = settings.map<SideMenuItemType>(
    ({ id, icon }) => ({
      id,
      icon: <Icon size="small">{icon}</Icon>,
      label: t(`categories.${id}`),
      href: `/main/configuration/${id}`,
      selected: sectionId === id,
      component: MenuItemLink,
    })
  );

  return <SideMenu items={menuItems} />;
};

export default Navigation;
