// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useMutation, useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError, openSnackbar } from '@zaaksysteem/common/src/signals';
import { queryClient } from '../../queryClient';
import {
  convertCaseTypeFromApi,
  revertCaseTypeToApi,
} from './conversions/caseTypeManagement.conversions';
import { useUrl } from './CaseTypeManagement.library';
import {
  CaseTypeRequestParamsType,
  CaseTypeResponseBodyType,
  CaseTypeType,
  EmailIntegrationsResponseBodyType,
  HtmlEmailTemplateType,
  MapIntegrationResponseBodyType,
  MapLayerType,
  SaveType,
  SubjectRoleType,
  SubjectRolesResponseBodyType,
} from './CaseTypeManagement.types';

export const CASE_TYPE = 'caseType';

export const fetchCaseType = async (uuid: string) => {
  const url = buildUrl<CaseTypeRequestParamsType>(
    '/api/v2/admin/catalog/get_versioned_casetype',
    { uuid }
  );

  return await request<CaseTypeResponseBodyType>('GET', url);
};

export const getCaseType = async (uuid: string) => {
  const response = await fetchCaseType(uuid);

  return convertCaseTypeFromApi(response);
};

export const useCaseTypeQuery = () => {
  const { uuid } = useUrl();
  const query = [CASE_TYPE, uuid];

  return useQuery<CaseTypeType, V2ServerErrorsType>(
    query,
    () => getCaseType(uuid),
    {
      onError: openServerError,
    }
  );
};

export const saveCaseType: SaveType = async data => {
  const url = '/api/v2/admin/catalog/update_versioned_casetype';

  return request('POST', url, data);
};

export const useCaseTypeMutation = () => {
  const { uuid } = useUrl();
  const query = [CASE_TYPE, uuid];

  return useMutation<
    any,
    V2ServerErrorsType,
    {
      caseType: CaseTypeType;
      updateDescription: string;
      updatedComponents: string[];
    }
  >(
    query,
    async ({ caseType, updateDescription, updatedComponents }) => {
      const data = {
        ...revertCaseTypeToApi(caseType),
        change_log: {
          update_description: updateDescription,
          update_components: updatedComponents,
        },
      };

      return await saveCaseType(data);
    },
    {
      onSuccess: (resp, { caseType }) => {
        queryClient.setQueryData(query, () => caseType);
        openSnackbar('caseTypeManagement:save.snack');
      },
      onError: openServerError,
    }
  );
};

export const useHtmlEmailTemplatesQuery = () =>
  useQuery<HtmlEmailTemplateType[], V2ServerErrorsType>(
    ['htmlEmailTemplates'],
    async () => {
      const response = await request<EmailIntegrationsResponseBodyType>(
        'GET',
        '/api/v1/sysin/interface/get_by_module_name/emailconfiguration'
      );
      const emailIntegrations = response.result.instance.rows;

      if (!emailIntegrations.length) return [];

      const emailIntegration = emailIntegrations[0];
      const config = emailIntegration.instance.interface_config;
      const { rich_email, rich_email_templates } = config;

      if (!rich_email) return [];

      return (rich_email_templates || []).map(({ label }) => ({
        label,
        value: label,
      }));
    },
    { onError: openServerError }
  );

export const useSubjectRolesQuery = () =>
  useQuery<SubjectRoleType[], V2ServerErrorsType>(
    ['subjectRoles'],
    async () => {
      const response = await request<SubjectRolesResponseBodyType>(
        'GET',
        '/api/v1/subject/role'
      );
      const subjectRoles = response.result.instance.rows;

      return (subjectRoles || []).map(({ instance: { label } }) => ({
        label,
        value: label,
      }));
    },
    { onError: openServerError }
  );

export const useMapLayersQuery = () =>
  useQuery<MapLayerType[], V2ServerErrorsType>(
    ['mapLayers'],
    async () => {
      const response = await request<MapIntegrationResponseBodyType>(
        'GET',
        '/api/v1/map/ol_settings'
      );
      const mapLayers = response.result.instance.wms_layers;

      return (mapLayers || [])
        .filter(({ instance: { active } }) => active)
        .map(({ instance: { label, layer_name } }) => ({
          label,
          value: layer_name,
        }));
    },
    { onError: openServerError }
  );
