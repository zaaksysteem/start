// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { GetDefinitionType } from '../SaveDialog/SaveDialog.types';
import { tabs } from '../../Menu/Menu.library';

export const getDefinition: GetDefinitionType = (t, changedTabs) => {
  return [
    {
      name: 'tabs',
      type: fieldTypes.CHECKBOX_GROUP,
      label: t('reset.fields.sections'),
      value: [],
      choices: tabs
        .filter(tab => changedTabs.includes(tab))
        .map(tab => ({ label: t(`tabs.${tab}`), value: tab })),
    },
  ];
};
