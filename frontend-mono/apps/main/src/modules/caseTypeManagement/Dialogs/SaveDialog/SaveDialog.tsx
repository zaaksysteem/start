// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseTypeType, TabType } from '../../CaseTypeManagement.types';
import { menuItemsConfig } from '../../Menu/Menu.library';
import { useCaseTypeMutation } from '../../CaseTypeManagement.requests';
import { TabStatesType } from './SaveDialog.types';
import { useSaveDialogStyles } from './SaveDialog.styles';

type SaveDialogPropsType = {
  onClose: () => void;
  open: boolean;
  caseType: CaseTypeType;
  tabStates: TabStatesType;
  setInitialCaseType: (caseType: CaseTypeType) => void;
};

const SaveDialog: React.ComponentType<SaveDialogPropsType> = ({
  onClose,
  open,
  caseType,
  tabStates,
  setInitialCaseType,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useSaveDialogStyles();
  const dialogEl = useRef();

  const { mutateAsync: saveCaseType } = useCaseTypeMutation();

  const formDefinition = [
    {
      name: 'updateDescription',
      value: null,
      type: fieldTypes.TEXTAREA,
      required: false,
      label: t('save.fields.updateDescription'),
      rows: 3,
    },
  ];

  const changedTabs = Object.entries(tabStates).reduce<TabType[]>(
    (acc, [key, { changed }]) => {
      return changed ? [...acc, key as TabType] : acc;
    },
    []
  );
  const updatedComponents = [...changedTabs.map(tab => t(`tabs.${tab}`))];

  let {
    fields,
    formik: {
      values: { updateDescription },
      isValid,
    },
  } = useForm({
    formDefinition,
  });

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={'save-casetype-dialog'}
      ref={dialogEl}
      fullWidth={true}
    >
      <DialogTitle
        elevated={true}
        icon="save"
        title={t('save.title')}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.content}>
          <span>{t('save.description')}</span>
          <span>
            <div className={classes.changedTabs}>
              {changedTabs.map(tab => (
                <div key={tab} className={classes.changedTab}>
                  <Icon size="small">{iconNames[menuItemsConfig[tab]]}</Icon>
                  <span>{t(`tabs.${tab}`)}</span>
                </div>
              ))}
            </div>
          </span>
          {Boolean(caseType.children.length) && (
            <span>{t('save.childrenWarning')}</span>
          )}
          <span>{t('save.supportWarning')}</span>
          {fields.map(({ FieldComponent, key, ...rest }: any) => {
            return (
              <FormControlWrapper
                {...rest}
                compact={true}
                key={`${rest.name}-formcontrol-wrapper`}
              >
                <FieldComponent
                  {...rest}
                  t={t}
                  containerRef={dialogEl.current}
                />
              </FormControlWrapper>
            );
          })}
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t('save.publish'),
                onClick() {
                  saveCaseType({
                    caseType,
                    updateDescription,
                    updatedComponents,
                  }).then(() => {
                    onClose();
                    setInitialCaseType(caseType);
                  });
                },
                disabled: !isValid,
              },
              {
                text: t('common:verbs.cancel'),
                onClick: onClose,
              },
            ],
            'save-casetype-dialog'
          )}
        </DialogActions>
      </>
    </Dialog>
  );
};

export default SaveDialog;
