// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const flip = {
  MozTransform: 'scale(-1, 1)',
  WebkitTransform: 'scale(-1, 1)',
  OTransform: 'scale(-1, 1)',
  MsTransform: 'scale(-1, 1)',
  transform: 'scale(-1, 1)',
};

export const rotate = {
  MozTransform: 'rotate(90deg)',
  WebkitTransform: 'rotate(90deg)',
  OTransform: 'rotate(90deg)',
  MsTransform: 'rotate(90deg)',
  transform: 'rotate(90deg)',
};

export const useCaseTypeManagementStyles = makeStyles(
  ({
    transitions,
    mintlab: { greyscale, shadows },
    palette: { common },
  }: Theme) => ({
    sheet: {
      maxWidth: 2400,
    },
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    content: {
      flexGrow: 1,
      overflow: 'auto',
    },
    saving: {
      '& $saveBar': {
        minHeight: 90,
        transition: transitions.create(
          // @ts-ignore
          ['min-height'],
          {
            easing: transitions.easing.sharp,
            duration: transitions.duration.leavingScreen,
          }
        ),
      },
    },
    notSaving: {
      '& $saveBar': {
        minHeight: 0,
        transition: transitions.create(
          // @ts-ignore
          ['min-height'],
          {
            easing: transitions.easing.sharp,
            duration: transitions.duration.leavingScreen,
          }
        ),
      },
    },
    saveBar: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      height: 0,
      gap: 20,
      padding: '0 20px',
      overflow: 'hidden',
      borderTop: `1px solid ${greyscale.darker}`,
      boxShadow: shadows.medium,
      width: '100%',
      backgroundColor: common.white,
    },
  })
);
