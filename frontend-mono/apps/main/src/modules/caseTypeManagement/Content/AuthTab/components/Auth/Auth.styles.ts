// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAuthStyles = makeStyles(
  ({ palette: { common }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      border: `1px solid ${greyscale.darker}`,
      backgroundColor: common.white,
      borderRadius: 8,
      display: 'flex',
      padding: 10,
      paddingLeft: 0,
      marginBottom: 5,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'relative',
      '&:hover': {
        '& $buttonBar': {
          display: 'flex',
        },
      },
    },
    handle: {
      height: '100%',
      padding: 10,
      display: 'flex',
      alignItems: 'center',
      color: greyscale.darker,
    },
    departmentAndRole: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
    },
    buttonBar: {
      display: 'none',
      gap: 10,
      position: 'absolute',
      top: -30,
      right: -5,
      padding: 10,
      '&:hover': {
        display: 'flex',
      },
      '&>div>button': {
        backgroundColor: common.white,
        border: `1px solid ${greyscale.darker}`,
        '&:hover': {
          backgroundColor: greyscale.dark,
        },
        '&:disabled': {
          backgroundColor: common.white,
        },
      },
    },
  })
);
