// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAuthTabStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    padding: 20,
    gap: 20,
    overflowX: 'auto',
    minWidth: 1280 + 2 * 20,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: 50,
    paddingBottom: 100,
  },
  type: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
    alignItems: 'center',
  },
  typeHeader: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  },
  levels: {
    display: 'flex',
    flexDirection: 'row',
    gap: 20,
  },
}));
