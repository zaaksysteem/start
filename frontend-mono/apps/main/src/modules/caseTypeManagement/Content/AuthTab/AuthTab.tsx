// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DragDropContext } from 'react-beautiful-dnd';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import Button from '@mintlab/ui/App/Material/Button';
import { useUpdateCaseType } from '../../CaseTypeManagement.library';
import { CaseTypeType } from '../../CaseTypeManagement.types';
import { useCaseTypeQuery } from '../../CaseTypeManagement.requests';
import { useAuthTabStyles } from './AuthTab.styles';
import { download, getAuthSets, getAuthsValue } from './AuthTab.library';
import {
  AuthCopyType,
  AuthLevelType,
  AuthSetType,
  AuthType,
  AuthTypeType,
} from './AuthTab.types';
import TypeHeader from './components/TypeHeader/TypeHeader';
import Level from './components/Level/Level';

const AuthTab: React.ComponentType = () => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useAuthTabStyles();

  const { data: caseType } = useCaseTypeQuery() as { data: CaseTypeType };

  const authorizations = caseType.authorization;
  const authSets = getAuthSets(authorizations);

  const [sets, setSets] = useState<AuthSetType>(authSets);
  const [copy, setCopy] = useState<AuthCopyType>(null);

  const updateCaseType = useUpdateCaseType();

  useEffect(() => {
    updateCaseType({ authorizations: getAuthsValue(sets) });
  }, [sets]);

  const title = t('auth.title');
  const description = t('auth.description');

  const onDragEnd = (event: any) => {
    if (!event.destination) return;

    const {
      source: { droppableId: sourceId, index: sourceIndex },
      destination: { droppableId: destinationId, index: destinationIndex },
      draggableId,
    } = event;

    const [sourceType, sourceLevel]: [AuthTypeType, AuthLevelType] =
      sourceId.split('-');
    const [destinationType, destinationLevel]: [AuthTypeType, AuthLevelType] =
      destinationId.split('-');

    const sourceList = sets[sourceType][sourceLevel];
    const movedAuth = sourceList.find(
      auth => auth.uuid === draggableId
    ) as AuthType;
    const newAuth = {
      ...movedAuth,
      type: destinationType,
      level: destinationLevel,
    };

    let newSets = { ...sets };

    if (sourceId !== destinationId) {
      let newSourceList = [...sourceList];
      newSourceList.splice(sourceIndex, 1);
      newSets[sourceType][sourceLevel] = newSourceList;
    }

    let newDestinationList = [
      ...sets[destinationType][destinationLevel],
    ].filter(auth => auth.uuid !== draggableId);
    newDestinationList.splice(destinationIndex, 0, newAuth);
    newSets[destinationType][destinationLevel] = newDestinationList;

    setSets(newSets);
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <SubHeader title={title} description={description} />
        <Button
          variant="outlined"
          name="download"
          icon="download"
          action={() => {
            download(t as any, caseType);
          }}
        >
          {t('common:verbs.download')}
        </Button>
      </div>
      <div className={classes.content}>
        {/* @ts-ignore */}
        <DragDropContext onDragEnd={onDragEnd}>
          {Object.entries(sets).map(([type, levels]) => (
            <div key={type} className={classes.type}>
              <TypeHeader
                sets={sets}
                setSets={setSets}
                copy={copy}
                setCopy={setCopy}
                type={type as AuthTypeType}
              />
              <div className={classes.levels}>
                {/* note: using flexDirection row-reverse the levels are displayed in the correct order
                          only because they are alphabetically ordered by sheer coincidence */}
                {Object.entries(levels).map(([level, auths]) => {
                  const droppableId = `${type}-${level}`;

                  return (
                    <Level
                      key={droppableId}
                      droppableId={droppableId}
                      sets={sets}
                      setSets={setSets}
                      type={type as AuthTypeType}
                      level={level as AuthLevelType}
                      auths={auths}
                    />
                  );
                })}
              </div>
            </div>
          ))}
        </DragDropContext>
      </div>
    </div>
  );
};

export default AuthTab;
