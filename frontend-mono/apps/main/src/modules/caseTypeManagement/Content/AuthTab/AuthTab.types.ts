// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  CaseTypeResponseBodyType,
  CaseTypeRequestBodyType,
  CaseTypeType,
} from '../../CaseTypeManagement.types';

export type GetDefinitionType = (
  t: i18next.TFunction
) => AnyFormDefinitionField[];

export type AuthTypeType = 'public' | 'confidential';
export type AuthLevelType =
  | 'zaak_beheer'
  | 'zaak_edit'
  | 'zaak_read'
  | 'zaak_search';

export type AuthType = {
  uuid: string;
  type: AuthTypeType;
  department?: string;
  role?: string;
  level: AuthLevelType;
  order: number;
};

export type AuthSetType = {
  [key in AuthTypeType]: {
    [key in AuthLevelType]: AuthType[];
  };
};

export type SetSetsType = (sets: AuthSetType) => void;

export type AuthCopyType = AuthSetType[AuthTypeType] | null;

export type SetCopyType = (copy: AuthCopyType) => void;

export type GetAuthSetsType = (auths: AuthType[]) => AuthSetType;
export type GetAuthsValueType = (authSets: AuthSetType) => AuthType[];

type RawAuthType =
  CaseTypeResponseBodyType['data']['attributes']['authorization'][0];

export type GetAuthLevelType = (
  rights: CaseTypeResponseBodyType['data']['attributes']['authorization'][0]['rights']
) => AuthLevelType;

export type FormatAuthType = (auth: RawAuthType) => AuthType;

export type ConvertAuthsType = (auths: RawAuthType[]) => AuthType[];

export type RevertAuthsType = (
  auths: AuthType[]
) => CaseTypeRequestBodyType['authorization'];

export type FormatAuthsForExportType = (
  t: i18next.TFunction,
  auths: AuthType[]
) => Promise<{ [key: string]: string }[]>;

export type DownloadType = (
  t: i18next.TFunction,
  caseType: CaseTypeType
) => void;

type BranchType = DepartmentType | RoleType;
export type SortType = (itemA: BranchType, itemB: BranchType) => 1 | -1;

// DEPARTMENTS

export type DepartmentsResponseBodyType =
  APICaseManagement.GetDepartmentsResponseBody;

export type DepartmentType = {
  uuid: string;
  name?: string;
};

export type FetchDepartmentsType = () => Promise<DepartmentsResponseBodyType>;

export type GetDepartmentsType = () => Promise<DepartmentType[]>;

// ROLES

export type RolesResponseBodyType = APICaseManagement.GetRolesResponseBody;

export type RoleType = {
  uuid: string;
  name?: string;
};

export type FetchRolesType = () => Promise<RolesResponseBodyType>;

export type GetRolesType = () => Promise<RoleType[]>;

//
