// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchDepartmentsType,
  DepartmentsResponseBodyType,
  FetchRolesType,
  RolesResponseBodyType,
} from './AuthTab.types';

export const fetchDepartments: FetchDepartmentsType = async () => {
  const url = '/api/v2/cm/authorization/get_departments';

  const response = await request<DepartmentsResponseBodyType>('GET', url);

  return response;
};

export const fetchRoles: FetchRolesType = async () => {
  const url = '/api/v2/cm/authorization/get_roles';

  const response = await request<RolesResponseBodyType>('GET', url);

  return response;
};
