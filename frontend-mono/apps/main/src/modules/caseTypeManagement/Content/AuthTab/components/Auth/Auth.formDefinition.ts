// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hasValue,
  setDisabled,
  setEnabled,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { GetDefinitionType } from '../../AuthTab.types';

export const getDefinition: GetDefinitionType = t => {
  return [
    {
      name: 'department',
      type: fieldTypes.DEPARTMENT_FINDER,
      value: null,
      placeholder: t('auth.placeholders.department'),
      nestedValue: true,
    },
    {
      name: 'role',
      type: fieldTypes.ROLE_FINDER,
      value: null,
      placeholder: t('auth.placeholders.role'),
    },
  ];
};

export const getRules = () => [
  new Rule()
    .when(() => true)
    .then(transferDataAsConfig('department', 'role', 'parentRoleUuid')),
  new Rule()
    .when('department', field => hasValue(field))
    .then(setEnabled(['role']))
    .else(setDisabled(['role'])),
];
