// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import Typography from '@mui/material/Typography';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useUpdateCaseType } from '../../CaseTypeManagement.library';
import { useCaseTypeQuery } from '../../CaseTypeManagement.requests';
import { CaseTypeType } from '../../CaseTypeManagement.types';
import SaveChildrenDialog from './SaveDialog/SaveDialog';
import { useChildrenTabStyles } from './ChildrenTab.styles';
import { ChildType } from './ChildrenTab.types';
import Child from './Child';
import { getDefinition } from './Child.formDefinition';
import { getCanPublishAll } from './ChildrenTab.library';

const ChildrenTab: React.ComponentType = () => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useChildrenTabStyles();

  const [copyValue, setCopyValue] = useState<ChildType[]>([]);

  const { data: caseType } = useCaseTypeQuery() as { data: CaseTypeType };
  const children = caseType.children;

  const [childrenToPublish, setChildrenToPublish] = useState<ChildType[]>([]);
  const dialogOpen = childrenToPublish.length > 0;

  const updateCaseType = useUpdateCaseType();
  const updateChildren = (newChildren: ChildType[]) =>
    updateCaseType({ children: newChildren });

  const addChild = () => {
    const uuid = v4();

    const newchild = {
      uuid,
      baseSections: [],
      phaseSections: [],
      active: false,
    };

    updateChildren([...children, newchild]);

    setTimeout(() => {
      const newChildElement = document.getElementById(uuid);

      newChildElement?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
  };

  const updateChild = (newChild: ChildType) => {
    const newChildren = children.map(child =>
      child.uuid === newChild.uuid ? newChild : child
    );

    updateChildren(newChildren);
  };

  const removeChild = (uuid: string) => {
    const newChildren = children.filter(child => child.uuid !== uuid);

    updateChildren(newChildren);
  };

  const title = t('children.title');
  const description = t('children.description');

  const formDefinition = getDefinition(t as any, caseType.phases);

  const activeChildren = children.filter(child => child.active);
  const canPublishAll = getCanPublishAll(activeChildren);
  const publishAllTooltip = canPublishAll
    ? ''
    : t('children.save.button.noPublishableChildren');

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <SubHeader title={title} description={description} />
        <div className={classes.publish}>
          <Tooltip title={publishAllTooltip}>
            <Button
              variant="outlined"
              name="publish-children"
              icon="save"
              action={() => {
                setChildrenToPublish(activeChildren);
              }}
              disabled={!canPublishAll}
            >
              {t('children.save.button.publish')}
            </Button>
          </Tooltip>
        </div>
      </div>
      <div className={classes.content}>
        {children.map((child, index) => (
          <Child
            key={index}
            formDefinition={formDefinition}
            child={child}
            updateChild={updateChild}
            copyValue={copyValue}
            setCopyValue={setCopyValue}
            removeChild={removeChild}
            publish={() => setChildrenToPublish([child])}
          />
        ))}
        <Button name="add-child" action={addChild}>
          <Typography variant="button">{t('common:verbs.add')}</Typography>
        </Button>
      </div>

      {dialogOpen && (
        <SaveChildrenDialog
          open={dialogOpen}
          onClose={() => setChildrenToPublish([])}
          childrenToPublish={childrenToPublish}
          caseType={caseType}
        />
      )}
    </div>
  );
};

export default ChildrenTab;
