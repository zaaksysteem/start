// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSaveDailogStyles = makeStyles(
  ({ palette: { grass, error } }: Theme) => ({
    child: {
      display: 'flex',
      alignItems: 'center',
      height: 30,
      gap: 10,
    },
    icon: {
      display: 'flex',
      justifyContent: 'center',
      width: 30,
    },
    success: {
      color: grass.main,
    },
    error: {
      color: error.dark,
    },
  })
);
