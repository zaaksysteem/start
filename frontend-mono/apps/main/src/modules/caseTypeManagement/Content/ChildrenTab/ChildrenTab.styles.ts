// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { childWidth, gap } from './Child.styles';

export const useChildrenTabStyles = makeStyles(
  ({ palette: { common }, mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    header: {
      padding: 20,
      gap: 20,
      display: 'flex',
      '&>:first-child': {
        flexGrow: 1,
      },
    },
    publish: {
      minWidth: 260,
    },
    content: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      padding: 20,
      gap,
      overflowY: 'auto',
      flexWrap: 'wrap',
      '&>button': {
        border: `1px dashed ${greyscale.darker}`,
        backgroundColor: common.white,
        color: common.black,
        boxShadow: 'none',
        height: 100,
        width: childWidth,
        '&:hover': {
          border: `1px dashed ${greyscale.darkest}`,
          backgroundColor: greyscale.dark,
          boxShadow: 'none',
        },
      },
    },
  })
);
