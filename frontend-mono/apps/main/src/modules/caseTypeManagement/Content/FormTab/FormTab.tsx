// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { useDebouncedCallback } from 'use-debounce';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { CaseTypeType, FormTabType } from '../../CaseTypeManagement.types';
import { useUpdateCaseType, useUrl } from '../../CaseTypeManagement.library';
import {
  useCaseTypeQuery,
  useHtmlEmailTemplatesQuery,
} from '../../CaseTypeManagement.requests';
import { useFormTabStyles } from './FormTab.styles';
import { getDefinition, getRules } from './FormTab.library';

const FastField = ({ FieldComponent, ...rest }: any) =>
  useMemo(() => <FieldComponent {...rest} />, [rest.value, rest.disabled]);

const FormTab: React.ComponentType = () => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useFormTabStyles();

  const { tab } = useUrl() as { tab: FormTabType };
  const { data: caseType } = useCaseTypeQuery() as { data: CaseTypeType };

  const { data: htmlEmailTemplates = [] } = useHtmlEmailTemplatesQuery();

  const width = useWidth();
  const isCompact = ['xs', 'sm'].includes(width);

  const updateCaseType = useUpdateCaseType();
  const [updateCaseTypeDebounced] = useDebouncedCallback(updateCaseType, 300);
  const onChange = (values: any) => {
    const headerKeys = Object.keys(t('form.headers', { returnObjects: true }));
    const newValues = cloneWithout(values, ...headerKeys);

    return updateCaseTypeDebounced({ [tab]: newValues });
  };

  const formDefinition = getDefinition(
    tab,
    t as any,
    htmlEmailTemplates,
    caseType
  );
  const rules = getRules(tab, caseType, formDefinition);
  const validationMap = generateValidationMap(formDefinition);
  const form = useForm({ formDefinition, rules, validationMap, onChange });

  return (
    <div className={classes.wrapper}>
      {form.fields.map(
        (
          { FieldComponent, type, label, isLabel, title, description, ...rest },
          index
        ) => {
          if (isLabel) {
            const headerClass = classNames(classes.subHeader, {
              [classes.subHeaderTop]: index !== 0,
            });

            return (
              <div key={rest.name} className={headerClass}>
                <SubHeader title={title} description={description} />
              </div>
            );
          }

          return (
            <div key={rest.name} className={classes.fieldWrapper}>
              <FormControlWrapper compact={isCompact} label={label} {...rest}>
                <div
                  className={classNames({
                    [classes.richText]: type === fieldTypes.WYSISWYG,
                    [classes.noHelp]: Boolean(!rest.help),
                  })}
                >
                  <FastField FieldComponent={FieldComponent} {...rest} />
                </div>
              </FormControlWrapper>
            </div>
          );
        }
      )}
    </div>
  );
};

export default FormTab;
