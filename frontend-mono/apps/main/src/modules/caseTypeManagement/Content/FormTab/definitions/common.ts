// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  valueEquals,
} from '@zaaksysteem/common/src/components/form/rules';
import { getLeadTimeChoices, getLeadTimeTypeChoices } from '../FormTab.choices';
import { GetAnyFormDefinitionType } from '../FormTab.types';

export const getCommonFormDefinition: GetAnyFormDefinitionType = t => {
  const leadTimeTypeChoices = getLeadTimeTypeChoices(t);
  const leadTimeChoices = getLeadTimeChoices(t);

  return [
    {
      name: 'common',
      type: fieldTypes.TEXT,
      isLabel: true,
    },
    {
      name: 'name',
      type: fieldTypes.TEXT,
      required: true,
    },
    {
      name: 'identification',
      type: fieldTypes.TEXT,
    },
    {
      name: 'tags',
      type: fieldTypes.TEXT,
    },
    {
      name: 'description',
      type: fieldTypes.TEXT,
    },
    {
      name: 'summaryInternal',
      type: fieldTypes.TEXT,
    },
    {
      name: 'summaryExternal',
      type: fieldTypes.TEXT,
    },
    {
      name: 'leadTimeType',
      type: fieldTypes.RADIO_GROUP,
      choices: leadTimeTypeChoices,
      nestedValues: true,
    },
    {
      name: 'leadTimeLegal',
      type: fieldTypes.AMOUNT_AND_TYPE,
      choices: leadTimeChoices,
      required: true,
    },
    {
      name: 'leadTimeService',
      type: fieldTypes.AMOUNT_AND_TYPE,
      choices: leadTimeChoices,
      required: true,
    },
    {
      name: 'leadTimeLegalDate',
      type: fieldTypes.DATEPICKER,
      required: true,
    },
    {
      name: 'leadTimeServiceDate',
      type: fieldTypes.DATEPICKER,
      required: true,
    },
  ];
};

const leadTimeFields = ['leadTimeLegal', 'leadTimeService'];
const leadTimeDateFields = ['leadTimeLegalDate', 'leadTimeServiceDate'];

export const getCommonRules = () => [
  new Rule()
    .when('leadTimeType', valueEquals('term'))
    .then(showFields(leadTimeFields))
    .and(hideFields(leadTimeDateFields))
    .else(showFields(leadTimeDateFields))
    .and(hideFields(leadTimeFields)),
];
