// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useGroupsStyles } from '../Groups.styles';
import { AttributeType } from '../../../../PhasesTab.types';

type MagicStringPropsType = {
  field: AttributeType;
};

/* eslint complexity: [2, 100] */
const MagicString: React.ElementType<MagicStringPropsType> = ({ field }) => {
  const classes = useGroupsStyles();

  const { magicString } = field;

  if (!magicString) return null;

  const string = `[[${magicString}]]`;

  return (
    <button
      type="button"
      name="copy-magic-string"
      className={classes.magicStringButton}
      onClick={() => {
        navigator.clipboard.writeText(string || '');
      }}
    >
      <Tooltip
        title={<Icon size="extraSmall">{iconNames.content_copy}</Icon>}
        placement="right"
      >
        <div className={classes.magicString}>{string}</div>
      </Tooltip>
    </button>
  );
};

export default MagicString;
