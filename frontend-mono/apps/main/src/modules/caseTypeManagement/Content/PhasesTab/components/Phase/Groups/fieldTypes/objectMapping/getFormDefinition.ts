// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ObjectMappingType } from '../../../../../PhasesTab.types';

export const getFormDefinition = ({ label, value }: ObjectMappingType) => [
  {
    name: 'label',
    type: fieldTypes.TEXT,
    value: label,
    readOnly: true,
  },
  {
    name: 'value',
    type: fieldTypes.TEXT,
    value: value,
    placeholder: `[[${label}]]`,
  },
];
