// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const margin = '5px';
const taskHeight = 40;
const handleWidth = 38;

export const useTasksStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      margin: `${margin} 10px 10px 0`,
    },
    tasks: {
      display: 'flex',
      flexDirection: 'column',
    },
    task: {
      height: taskHeight,
      display: 'flex',
      alignItems: 'center',
      margin: `${margin} 0`,
      position: 'relative',
      '&:hover': {
        '& $remove': {
          visibility: 'visible',
        },
      },
    },
    text: {
      flexGrow: 1,
    },
    newTask: {
      marginTop: margin,
      marginLeft: handleWidth,
    },
    handle: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: taskHeight,
      minWidth: handleWidth,
      color: greyscale.darkest,
    },
    remove: {
      position: 'absolute',
      top: 3,
      right: margin,
      visibility: 'hidden',
      color: greyscale.darkest,
    },
  })
);
