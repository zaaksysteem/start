// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {
  FieldType,
  GroupType,
  PhaseType,
  UpdatePhaseType,
} from '../../../PhasesTab.types';
import { useGroupsStyles } from './Groups.styles';
import Group from './Group';
import {
  GROUP_ZONE,
  addAttribute,
  onDragEnd,
  removeField,
} from './Groups.library';
import { useDialogs } from './dialogs/useDialogs';
import { AttributeChoiceType } from './components/AttributeSearch';
import Placeholder from './components/Placeholder';

export type AddAttributeType = (
  group: GroupType,
  attribute: AttributeChoiceType
) => void;

export type RemoveFieldType = (group: GroupType, field: FieldType) => void;

type GroupsPropsType = {
  phase: PhaseType;
  updatePhase: UpdatePhaseType;
};

const Groups: React.ComponentType<GroupsPropsType> = ({
  phase,
  updatePhase,
}) => {
  const classes = useGroupsStyles();

  const {
    groupDialog,
    openGroupDialog,
    removeGroupDialog,
    openRemoveGroupDialog,
    textblockDialog,
    openTextblockDialog,
    attributeDialog,
    openAttributeDialog,
  } = useDialogs(phase, updatePhase);

  const addAttr: AddAttributeType = (group, attribute) =>
    addAttribute(phase, updatePhase, group, attribute);

  const removeFld: RemoveFieldType = (group, field) =>
    removeField(phase, updatePhase, group, field);

  return (
    <>
      {groupDialog}
      {removeGroupDialog}
      {textblockDialog}
      {attributeDialog}

      {phase.groups.length === 0 ? (
        <Placeholder type="group" openDialog={openGroupDialog} />
      ) : (
        // @ts-ignore
        <DragDropContext
          onDragEnd={(event: any) => onDragEnd(event, phase, updatePhase)}
        >
          {/* @ts-ignore */}
          <Droppable droppableId={GROUP_ZONE} type={GROUP_ZONE}>
            {provided => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className={classes.wrapper}
              >
                {phase.groups.map((group, index) => (
                  // @ts-ignore
                  <Draggable
                    key={group.id}
                    draggableId={group.id}
                    index={index}
                  >
                    {(draggableProvided, snapshot) => (
                      <Group
                        phase={phase}
                        group={group}
                        openGroupDialog={openGroupDialog}
                        openRemoveGroupDialog={openRemoveGroupDialog}
                        openTextblockDialog={openTextblockDialog}
                        openAttributeDialog={openAttributeDialog}
                        addAttribute={addAttr}
                        removeField={removeFld}
                        provided={draggableProvided}
                        snapshot={snapshot}
                      />
                    )}
                  </Draggable>
                ))}
                {provided.placeholder as any}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      )}
    </>
  );
};

export default Groups;
