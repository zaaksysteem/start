// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { AttributeAuthorizationsType as AuthType } from '../../../../../PhasesTab.types';
import { useAuthorizationListStyles } from './AuthorizationList.styles';
import AuthorizationForm from './Authorization';

export const AUTHORIZATION_LIST = 'AuthorizationList';

export const AuthorizationList: React.ComponentType<
  FormRendererFormField<any, any, AuthType[]>
> = props => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useAuthorizationListStyles();

  const { fields, add } = useMultiValueField<any, AuthType>(props);

  return (
    <div className={classes.wrapper}>
      {fields.map(fieldProps => (
        <AuthorizationForm {...fieldProps} key={fieldProps.name} />
      ))}

      <div className={classes.addButtonWrapper}>
        <button
          type="button"
          className={classes.addButton}
          onClick={(event: any) => add(event.target.value.value)}
          disabled={props.disabled}
        >
          {t('phases.attributes.attribute.auths.add')}
          <Icon>{iconNames.add_circle_outline}</Icon>
        </button>
      </div>
    </div>
  );
};
