// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDateLimitsStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
  },
  dateLimit: {
    display: 'flex',
    flexDirection: 'row',
    gap: 5,
    maxHeight: 45,
    '&>:nth-of-type(1)': {
      minWidth: 80,
      maxWidth: 80,
    },
    '&>:nth-of-type(2)': {
      minWidth: 55,
      maxWidth: 55,
      '& .MuiTextField-root': {
        height: 42,
      },
    },
    '&>:nth-of-type(3)': {
      minWidth: 125,
      maxWidth: 125,
    },
    '&>:nth-of-type(4)': {
      minWidth: 90,
      maxWidth: 90,
    },
    '&>:nth-of-type(5)': {
      flexGrow: 1,
    },
  },
}));
