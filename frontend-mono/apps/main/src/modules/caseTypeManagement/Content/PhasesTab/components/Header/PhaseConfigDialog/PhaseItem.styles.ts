// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePhaseItemStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { common } }: Theme) => ({
    content: {
      width: '100%',
      border: `1px solid ${greyscale.darker}`,
      backgroundColor: common.white,
      borderRadius: 8,
      display: 'flex',
      padding: '0px 10px 0px 0',
      marginBottom: 20,
      justifyContent: 'space-between',
      alignItems: 'center',
      position: 'relative',
    },
    handle: {
      height: '100%',
      minWidth: 40,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      color: greyscale.darker,
    },
    main: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'start',
      flexGrow: 1,
      height: '100%',
      marginRight: 10,
      overflow: 'hidden',
      '&:hover': {
        '& $plus': {
          visibility: 'visible',
        },
      },
    },
    title: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      height: 48,
      gap: 10,
      alignItems: 'center',
    },
    name: {
      flexGrow: 1,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    icons: {
      display: 'flex',
      minWidth: 56,
    },
    icon: {
      color: greyscale.darkest,
    },
    plusBackdrop: {
      position: 'absolute',
      left: 'calc(50% - 50px)',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: 100,
      height: 40,
      bottom: -20,
    },
    plus: {
      backgroundColor: 'white',
      border: `1px solid ${greyscale.darker}`,
      borderRadius: '50%',
      visibility: 'hidden',
    },
  })
);
