// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { v4 } from 'uuid';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { useTranslation } from 'react-i18next';
import {
  PhaseType,
  TaskType,
  UpdatePhaseType,
} from '../../../../PhasesTab.types';
import { useTasksStyles } from './Tasks.styles';
import Task from './Task';

type TasksPropsType = {
  phase: PhaseType;
  updatePhase: UpdatePhaseType;
};

const Tasks: React.ComponentType<TasksPropsType> = ({ phase, updatePhase }) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useTasksStyles();

  const inputRef = useRef<HTMLInputElement>(null);
  const scrollIntoView = () => inputRef.current?.scrollIntoView();

  const { tasks } = phase;

  const [newTaskValue, setNewTaskValue] = useState<string>('');
  const addTask = (newTask: TaskType) => {
    updatePhase({ ...phase, tasks: [...tasks, newTask] });
  };

  const onEdit = (newTask: TaskType) => {
    const newList = tasks.map(task =>
      task.id === newTask.id ? newTask : task
    );

    updatePhase({ ...phase, tasks: newList });
  };

  const onDelete = (oldTask: TaskType) => {
    const newList = tasks.filter(task => task.id !== oldTask.id);

    updatePhase({ ...phase, tasks: newList });
  };

  const onDragEnd = ({ destination, draggableId }: any) => {
    if (!destination) return;

    const task = tasks.find(({ id }) => id === draggableId) as TaskType;

    let newList = tasks.filter(task => task.id !== draggableId);
    newList.splice(destination.index, 0, task);

    updatePhase({ ...phase, tasks: newList });
  };

  return (
    <div className={classes.wrapper}>
      {/* @ts-ignore */}
      <DragDropContext onDragEnd={onDragEnd}>
        {/* @ts-ignore */}
        <Droppable droppableId={'tasks'}>
          {provided => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              className={classes.tasks}
            >
              {tasks.map((task, index) => {
                return (
                  // @ts-ignore
                  <Draggable key={task.id} draggableId={task.id} index={index}>
                    {draggableProvided => (
                      <Task
                        task={task}
                        onEdit={onEdit}
                        onDelete={onDelete}
                        provided={draggableProvided}
                      />
                    )}
                  </Draggable>
                );
              })}
              {provided.placeholder as any}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      <div className={classes.newTask}>
        <TextField
          value={newTaskValue}
          placeholder={t('phases.tasks.placeholder.new')}
          onChange={(event: any) => setNewTaskValue(event.target.value)}
          onKeyDown={(event: any) => {
            if (newTaskValue.length && event.key.toLowerCase() === 'enter') {
              addTask({ id: v4(), value: newTaskValue });
              scrollIntoView();
              setNewTaskValue('');
            }
          }}
        />
        <div ref={inputRef} />
      </div>
    </div>
  );
};

export default Tasks;
