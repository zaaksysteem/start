// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  CHECKBOX,
  DATEPICKER,
  SELECT,
  TEXT,
  WYSISWYG,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  isTruthy,
  setDisabled,
  setEnabled,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { capitalize } from '@mintlab/kitchen-sink/source';
import {
  PhaseType,
  GroupType,
  FieldType,
  TextblockType,
  AttributeType,
  ResultType,
} from '../../../../PhasesTab.types';
import {
  CaseTypeType,
  MapLayerType,
  SubjectRoleType,
} from '../../../../../../CaseTypeManagement.types';
import {
  OBJECT_MAPPING,
  ObjectMapping,
} from '../fieldTypes/objectMapping/ObjectMapping';
import {
  AUTHORIZATION_LIST,
  AuthorizationList,
} from '../fieldTypes/authorizationList/AuthorizationList';
import { DATE_LIMITS, DateLimits } from '../fieldTypes/dateLimits/dateLimits';
import { getShowExternalDescription } from '../Groups.library';

const defaultValues: { [key: string]: any } = {
  [TEXT]: '',
  [WYSISWYG]: '',
  [CHECKBOX]: false,
  [SELECT]: null,
};

type FormFieldType = { name: string; type: any; [key: string]: any };

export const mapField = (
  t: i18next.TFunction,
  field: FormFieldType,
  elementType: 'group' | 'textblock' | 'attribute',
  element?: any
) => {
  // @ts-ignore
  const value = element ? element[field.name] : defaultValues[field.type];
  const label = t(`phases.attributes.labels.${field.name}`);
  const help = t(`phases.attributes.help.${field.name}`, {
    elementType: t(`phases.attributes.${elementType}.title`).toLowerCase(),
  });
  const suppressLabel = field.type === CHECKBOX;

  return { label, help, value, suppressLabel, ...field };
};

export const mapResult = (
  t: i18next.TFunction,
  field: FormFieldType,
  result?: ResultType
) => {
  // @ts-ignore
  const value = result ? result[field.name] : defaultValues[field.type];
  const label = t(`phases.results.labels.${field.name}`);
  const suppressLabel = field.type === CHECKBOX;

  return { label, value, suppressLabel, ...field };
};

export type GroupValuesType = {
  title: string;
  description: string;
  publishOnPip: boolean;
};

export const getGroupFormDefinition = (
  t: i18next.TFunction,
  group?: GroupType
) =>
  [
    { type: TEXT, name: 'title', required: true },
    { type: WYSISWYG, name: 'description' },
    { type: CHECKBOX, name: 'publishOnPip' },
  ].map(field => mapField(t, field, 'group', group));

export type TextblockValuesType = {
  title: string;
  description: string;
  publishOnPip: boolean;
};

export const getTextblockFormDefinition = (
  t: i18next.TFunction,
  textblock?: TextblockType
) =>
  [
    { type: TEXT, name: 'title', required: true },
    { type: WYSISWYG, name: 'description' },
    { type: CHECKBOX, name: 'publishOnPip' },
  ].map(field => mapField(t, field, 'textblock', textblock));

const supportsCaseAddress = [
  'address_v2',
  'googlemaps',
  'bag_straat_adres',
  'bag_straat_adressen',
  'bag_adres',
  'bag_adressen',
  'bag_openbareruimte',
  'bag_openbareruimtes',
];
const supportsShowOnMap = ['address_v2', 'geojson', 'relationship'];
const supportMapLayer = ['address_v2', 'geolatlon'];
const supportMapLayerAttribute = ['select', 'option'];

/* eslint complexity: [2, 100] */
export const getAttributeFormDefinition = (
  t: i18next.TFunction,
  caseType: CaseTypeType,
  phase: PhaseType,
  subjectRoles: SubjectRoleType[] = [],
  mapLayers: MapLayerType[] = [],
  attribute?: AttributeType
) => {
  const fieldsOfPhase = phase.groups.reduce(
    (acc, group) => [...acc, ...group.fields],
    [] as FieldType[]
  );
  const attributesOfPhase = fieldsOfPhase.filter(
    field => field.type !== 'textblock'
  ) as AttributeType[];
  const attributeChoices = attributesOfPhase.map(({ name, uuid, type }) => ({
    label: name,
    value: uuid,
    type,
  }));
  const mapLayerAttributeChoices = attributeChoices.filter(({ type }) =>
    supportMapLayerAttribute.includes(type)
  );
  const dateAttributeChoices = attributeChoices.filter(
    ({ value, type }) => type === 'date' && value !== attribute?.uuid
  );

  const hidePip = caseType.case_dossier.disablePipForRequestor;
  const hideSkipQueue = !caseType.case_dossier.queueCoworkerChanges;
  const hideMultiple = !attribute?.isMultiple;
  const hideCaseAddress = !(
    attribute && supportsCaseAddress.includes(attribute.type)
  );
  const hideShowOnMap = !(
    attribute && supportsShowOnMap.includes(attribute.type)
  );
  const hideMapLayer = !(attribute && supportMapLayer.includes(attribute.type));
  const hideDateLimit = !(attribute?.type === 'date');
  const hideAuthorizations = attribute?.type === 'file';
  const hideRole = !(attribute?.relationshipType === 'subject');
  const hideObject = !(attribute?.relationshipType === 'custom_object');

  return [
    {
      type: CHECKBOX,
      name: 'permanentHidden',
    },
    {
      type: CHECKBOX,
      name: 'referential',
    },
    {
      type: TEXT,
      name: 'title',
    },
    {
      type: WYSISWYG,
      name: 'descriptionInternal',
    },
    {
      type: CHECKBOX,
      name: 'mandatory',
    },
    {
      type: CHECKBOX,
      name: 'publishOnPip',
      hidden: hidePip,
    },
    {
      type: CHECKBOX,
      name: 'editOnPip',
    },
    {
      type: WYSISWYG,
      name: 'descriptionExternal',
    },
    {
      type: CHECKBOX,
      name: 'skipQueue',
      hidden: hideSkipQueue,
    },
    {
      type: TEXT,
      name: 'labelMultiple',
      hidden: hideMultiple,
    },
    {
      type: CHECKBOX,
      name: 'caseAddress',
      hidden: hideCaseAddress,
    },
    {
      type: CHECKBOX,
      name: 'showOnMap',
      hidden: hideShowOnMap,
    },
    {
      name: 'mapLayer',
      type: SELECT,
      choices: mapLayers,
      nestedValue: true,
      hidden: hideMapLayer,
    },
    {
      name: 'mapLayerAttribute',
      type: SELECT,
      choices: mapLayerAttributeChoices,
      freeSolo: true,
      hidden: hideMapLayer,
    },
    {
      name: 'dateLimits',
      type: DATE_LIMITS,
      hidden: hideDateLimit,
      config: { dateAttributeChoices },
    },
    {
      name: 'authorizations',
      type: AUTHORIZATION_LIST,
      hidden: hideAuthorizations,
      multiValue: true,
    },
    {
      name: 'role',
      type: SELECT,
      required: true,
      choices: subjectRoles,
      value: subjectRoles[0],
      isClearable: false,
      hidden: hideRole,
    },
    {
      name: 'createObjectEnabled',
      type: CHECKBOX,
      hidden: hideObject,
    },
    {
      name: 'createObjectLabel',
      type: TEXT,
      hidden: hideObject,
    },
    {
      name: 'createObjectMapping',
      type: OBJECT_MAPPING,
      hidden: hideObject,
    },
  ]
    .filter(field => !field.hidden)
    .map(field => mapField(t, field, 'attribute', attribute));
};

const attributesToDisable1 = ['referential', 'title', 'descriptionInternal'];

const attributesToDisable2 = [
  'mandatory',
  'publishOnPip',
  'editOnPip',
  'descriptionExternal',
  'skipQueue',
  'labelMultiple',
  'caseAddress',
  'showOnMap',
  'mapLayer',
  'mapLayerAttribute',
  'dateLimits',
  'authorizations',
  'role',
  'createObjectEnabled',
  'createObjectLabel',
  'createObjectMapping',
];

export const getAttributeRules = (
  caseType: CaseTypeType,
  phaseNumber: string
) => [
  new Rule()
    .when('permanentHidden', isTruthy)
    .then(setDisabled(attributesToDisable1))
    .else(setEnabled(attributesToDisable1)),
  new Rule()
    .when(
      fields =>
        (fields.permanentHidden.value as any) ||
        (fields.referential.value as any)
    )
    .then(setDisabled(attributesToDisable2))
    .else(setEnabled(attributesToDisable2)),
  new Rule()
    .when(fields => Boolean(fields.publishOnPip?.value))
    .then(showFields(['editOnPip']))
    .else(hideFields(['editOnPip'])),
  new Rule()
    .when(fields => getShowExternalDescription(caseType, phaseNumber, fields))
    .then(showFields(['descriptionExternal']))
    .else(hideFields(['descriptionExternal'])),
  new Rule()
    .when('mapLayer', isTruthy)
    .then(showFields(['mapLayerAttribute']))
    .else(hideFields(['mapLayerAttribute'])),
  new Rule()
    .when('createObjectEnabled', isTruthy)
    .then(showFields(['createObjectLabel', 'createObjectMapping']))
    .else(hideFields(['createObjectLabel', 'createObjectMapping'])),
];

export const getFieldComponents = () => ({
  [AUTHORIZATION_LIST]: AuthorizationList,
  [DATE_LIMITS]: DateLimits,
  [OBJECT_MAPPING]: ObjectMapping,
});

export type ResultValuesType = Omit<ResultType, 'id' | 'type'>;

const mapChoice = (value: string) => ({ label: capitalize(value), value });

const resultTypeChoices = [
  'aangegaan',
  'aangehouden',
  'aangekocht',
  'aangesteld',
  'aanvaard',
  'afgeboekt',
  'afgebroken',
  'afgehandeld',
  'afgesloten',
  'afgewezen',
  'akkoord',
  'akkoord met wijzigingen',
  'behaald',
  'betaald',
  'beëindigd',
  'buiten behandeling gesteld',
  'definitief toegekend',
  'geannuleerd',
  'gedeeltelijk gegrond',
  'gedeeltelijk verleend',
  'gedoogd',
  'gegrond',
  'gegund',
  'geleverd',
  'geweigerd',
  'gewijzigd',
  'geïnd',
  'handhaving uitgevoerd',
  'ingericht',
  'ingeschreven',
  'ingesteld',
  'ingetrokken',
  'ingewilligd',
  'niet aangekocht',
  'niet aangesteld',
  'niet akkoord',
  'niet behaald',
  'niet betaald',
  'niet doorgegaan',
  'niet gegund',
  'niet geleverd',
  'niet gewijzigd',
  'niet geïnd',
  'niet ingesteld',
  'niet ingetrokken',
  'niet nodig',
  'niet ontvankelijk',
  'niet opgelegd',
  'niet opgeleverd',
  'niet toegekend',
  'niet uitgevoerd',
  'niet vastgesteld',
  'niet verkregen',
  'niet verleend',
  'niet verstrekt',
  'niet verwerkt',
  'ongegrond',
  'ontvankelijk',
  'opgeheven',
  'opgelegd',
  'opgeleverd',
  'opgelost',
  'opgezegd',
  'toegekend',
  'toezicht uitgevoerd',
  'uitgevoerd',
  'vastgesteld',
  'verhuurd',
  'verkocht',
  'verkregen',
  'verleend',
  'vernietigd',
  'verstrekt',
  'verwerkt',
  'voorlopig toegekend',
  'voorlopig verleend',
].map(mapChoice);

const archivalNominationChoices = [
  'Bewaren (B)',
  'Conversie',
  'Migratie',
  'Overbrengen (O)',
  'Overdracht',
  'Publicatie',
  'Vernietigen (V)',
  'Vernietigen',
  'Vervallen beperkingen openbaarheid',
].map(mapChoice);

export const retentionPeriodChoices = [
  { value: '28', label: '4 weken' },
  { value: '42', label: '6 weken' },
  { value: '93', label: '3 maanden' },
  { value: '186', label: '6 maanden' },
  { value: '279', label: '9 maanden' },
  { value: '365', label: '1 jaar' },
  { value: '548', label: '1,5 jaar' },
  { value: '730', label: '2 jaar' },
  { value: '1095', label: '3 jaar' },
  { value: '1460', label: '4 jaar' },
  { value: '1825', label: '5 jaar' },
  { value: '2190', label: '6 jaar' },
  { value: '2555', label: '7 jaar' },
  { value: '2920', label: '8 jaar' },
  { value: '3285', label: '9 jaar' },
  { value: '3650', label: '10 jaar' },
  { value: '4015', label: '11 jaar' },
  { value: '4380', label: '12 jaar' },
  { value: '4745', label: '13 jaar' },
  { value: '5110', label: '14 jaar' },
  { value: '5475', label: '15 jaar' },
  { value: '5840', label: '16 jaar' },
  { value: '6935', label: '19 jaar' },
  { value: '7300', label: '20 jaar' },
  { value: '7665', label: '21 jaar' },
  { value: '9125', label: '25 jaar' },
  { value: '10950', label: '30 jaar' },
  { value: '14600', label: '40 jaar' },
  { value: '18250', label: '50 jaar' },
  { value: '40150', label: '110 jaar' },
  { value: '99999', label: 'Bewaren' },
];

const archivalNominationValuationChoices = [
  'vervallen',
  'onherroepelijk',
  'afhandeling',
  'verwerking',
  'geweigerd',
  'verleend',
  'geboorte',
  'einde-dienstverband',
].map(mapChoice);

const processTypeGenericChoices = ['Generiek', 'Specifiek'].map(mapChoice);

const originChoices = [
  'Specifiek benoemde wet- en regelgeving',
  'Systeemanalyse',
  'Trendanalyse',
  'Risicoanalyse',
].map(mapChoice);

const processPeriodChoices = ['A', 'B', 'C', 'D', 'E'].map(mapChoice);

export const getResultFormDefinition = (
  t: i18next.TFunction,
  result?: ResultType
) => {
  return [
    { type: TEXT, name: 'name' },
    {
      type: SELECT,
      name: 'resultType',
      choices: resultTypeChoices,
      value: result?.resultType || resultTypeChoices[0].value,
      isClearable: false,
      nestedValue: true,
    },
    { type: CHECKBOX, name: 'isDefault' },
    { type: TEXT, name: 'selectionList' },
    { type: DATEPICKER, name: 'selectionListSourceDate' },
    { type: DATEPICKER, name: 'selectionListEndDate' },
    { type: TEXT, name: 'selectionListNumber' },
    { type: CHECKBOX, name: 'archivalTrigger' },
    {
      type: SELECT,
      name: 'archivalNomination',
      choices: archivalNominationChoices,
      value: result?.archivalNomination || archivalNominationChoices[0].value,
      isClearable: false,
      nestedValue: true,
    },
    {
      type: SELECT,
      name: 'retentionPeriod',
      choices: retentionPeriodChoices,
      value: result?.retentionPeriod || retentionPeriodChoices[0].value,
      isClearable: false,
      nestedValue: true,
    },
    {
      type: SELECT,
      name: 'archivalNominationValuation',
      choices: archivalNominationValuationChoices,
      value:
        result?.archivalNominationValuation ||
        archivalNominationValuationChoices[0].value,
      isClearable: false,
      nestedValue: true,
    },
    { type: TEXT, name: 'comments' },
    { type: TEXT, name: 'processtypeNumber' },
    { type: TEXT, name: 'processtypeName' },
    { type: TEXT, name: 'processtypeDescription' },
    { type: TEXT, name: 'processtypeExplanation' },
    { type: TEXT, name: 'processtypeObject' },
    {
      type: SELECT,
      name: 'processtypeGeneric',
      choices: processTypeGenericChoices,
      value: result?.processtypeGeneric || null,
      nestedValue: true,
    },
    {
      type: SELECT,
      name: 'origin',
      choices: originChoices,
      value: result?.origin || null,
      nestedValue: true,
    },
    {
      type: SELECT,
      name: 'processPeriod',
      choices: processPeriodChoices,
      value: result?.processPeriod || null,
      nestedValue: true,
    },
  ].map(field => mapResult(t, field, result));
};
