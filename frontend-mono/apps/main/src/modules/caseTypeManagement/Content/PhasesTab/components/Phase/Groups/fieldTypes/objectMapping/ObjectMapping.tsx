// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { useTranslation } from 'react-i18next';
import { ObjectMappingType } from '../../../../../PhasesTab.types';
import { useObjectMappingStyles } from './ObjectMapping.styles';
import Mapping from './Mapping';

export const OBJECT_MAPPING = 'ObjectMapping';

export const ObjectMapping: React.ComponentType<
  FormRendererFormField<any, any, ObjectMappingType[]>
> = props => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useObjectMappingStyles();

  const { fields } = useMultiValueField<any, ObjectMappingType>(props);

  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <div className={classes.headerItem}>
          {t('phases.attributes.attribute.objectMapping.attributeOfObject')}
        </div>
        <div className={classes.headerItem}>
          {t('phases.attributes.attribute.objectMapping.prefilledValue')}
        </div>
      </div>
      {fields.map(fieldProps => (
        <Mapping {...fieldProps} key={fieldProps.name} />
      ))}
    </div>
  );
};
