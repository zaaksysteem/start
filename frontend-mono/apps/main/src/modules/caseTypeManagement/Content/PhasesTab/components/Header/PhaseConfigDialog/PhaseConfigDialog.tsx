// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { PhaseType, UpdatePhasesType } from '../../../PhasesTab.types';
import PhaseEditDialog from '../PhaseEditDialog/PhaseEditDialog';
import { fixItems, fixNavigation } from '../Header.library';
import { usePhaseConfigDialogStyles } from './PhaseConfigDialog.styles';
import PhaseItem from './PhaseItem';

type PhaseConfigDialogPropsType = {
  phaseNumber: string;
  phases: PhaseType[];
  updatePhases: UpdatePhasesType;
  open: boolean;
  onClose: () => void;
};

const PhaseConfigDialog: React.ComponentType<PhaseConfigDialogPropsType> = ({
  phaseNumber,
  phases,
  updatePhases,
  open,
  onClose,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = usePhaseConfigDialogStyles();
  const dialogEl = useRef();
  const navigate = useNavigate();

  const title = t('phases.config.dialog.title');
  const scope = 'phase-config-dialog';
  const warnings = Object.values(
    t('phases.config.dialog.warnings', { returnObjects: true })
  );

  const [itemToAdd, setItemToAdd] = useState<number>(0);
  const [itemToEdit, setItemToEdit] = useState<PhaseType | null>(null);
  const dialogOpen = Boolean(itemToAdd) || Boolean(itemToEdit);

  const [items, setItems] = useState<PhaseType[]>(phases);
  const [firstItem, ...restItems] = items;

  const addItem = (itemToAdd: PhaseType) => {
    const addAsLast = itemToAdd.number === items.length + 1;
    let newItems = [...items];

    if (addAsLast) {
      newItems = [...newItems, itemToAdd];
    } else {
      newItems.splice(itemToAdd.number - 1, 0, itemToAdd);
    }

    newItems = fixItems(phases, newItems);

    setItems(newItems);
  };

  const moveItem = ({ source, destination }: any) => {
    if (!destination) return;

    const sourceIndex = source.index;
    const destinationIndex = destination.index;

    let rest = Array.from(restItems);
    const [reorderedItem] = rest.splice(sourceIndex, 1);
    rest.splice(destinationIndex, 0, reorderedItem);

    let newItems = [firstItem, ...rest];

    newItems = fixItems(items, newItems, destinationIndex + 1, sourceIndex + 1);

    setItems(newItems);
  };

  const editItem = (newItem: PhaseType) => {
    let newItems = items.map(item =>
      item.number === newItem.number ? newItem : item
    );

    setItems(newItems);
  };

  const removeItem = (itemToRemove: PhaseType) => {
    let newItems = items.filter(item => item.id !== itemToRemove.id);

    newItems = fixItems(phases, newItems);

    setItems(newItems);
  };

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope={scope}
      ref={dialogEl}
    >
      <DialogTitle
        elevated={true}
        icon="settings"
        title={title}
        onCloseClick={onClose}
      />
      <DialogContent>
        <div className={classes.wrapper}>
          <div className={classes.description}>
            <div>{t('phases.config.dialog.description')}</div>
            <div className={classes.icon}>
              <Tooltip
                title={
                  <div>
                    <span>{t('phases.config.dialog.warning')}</span>
                    <ul className={classes.warningList}>
                      {warnings.map((warning, index) => (
                        <li key={index}>{warning}</li>
                      ))}
                    </ul>
                  </div>
                }
              >
                <Icon>{iconNames.info_outlined}</Icon>
              </Tooltip>
            </div>
          </div>

          {/* @ts-ignore */}
          <DragDropContext onDragEnd={moveItem}>
            {/* @ts-ignore */}
            <Droppable droppableId="phase-config">
              {provided => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  className={classes.droppable}
                >
                  <PhaseItem phase={firstItem} setItemToAdd={setItemToAdd} />
                  {restItems.map((phase, index) => (
                    // @ts-ignore
                    <Draggable
                      key={phase.id}
                      draggableId={phase.id}
                      index={index}
                    >
                      {draggableProvided => (
                        <PhaseItem
                          phase={phase}
                          provided={draggableProvided}
                          setItemToAdd={setItemToAdd}
                          setItemToEdit={setItemToEdit}
                          removeItem={removeItem}
                        />
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder as any}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </DialogContent>
      <>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                text: t('common:verbs.confirm'),
                onClick: () => {
                  updatePhases(items);
                  onClose();

                  fixNavigation(navigate, phaseNumber, phases, items);
                },
              },
              {
                text: t('common:dialog.cancel'),
                onClick: onClose,
              },
            ],
            scope
          )}
        </DialogActions>
      </>

      {dialogOpen && (
        <PhaseEditDialog
          items={items}
          itemToAdd={itemToAdd}
          addItem={addItem}
          itemToEdit={itemToEdit}
          editItem={editItem}
          open={dialogOpen}
          onClose={() => {
            setItemToAdd(0);
            setItemToEdit(null);
          }}
        />
      )}
    </Dialog>
  );
};

export default PhaseConfigDialog;
