// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as i18next from 'i18next';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { TopMenuItemType } from '@mintlab/ui/App/Zaaksysteem/TopMenu';
import { PhaseType, SideBarType } from '../../../PhasesTab.types';
import { sidebarTypes } from '../Phase.library';
import { CaseTypeManagementContext } from '../../../../../CaseTypeManagement.context';

export const useNavigationItems = (
  t: i18next.TFunction,
  phase: PhaseType,
  sideBarType: SideBarType
) => {
  const navigate = useNavigate();
  const { setDefaultPhaseSidebar } = useContext(CaseTypeManagementContext);

  const { tasks, documents, messages, contacts, objectActions, caseTypes } =
    phase;
  const taskCount = tasks.length;
  const actionCount = [
    ...documents,
    ...messages,
    ...objectActions,
    ...caseTypes,
    ...contacts,
  ].length;

  const navigationItems = phase.number > 1 ? sidebarTypes : [sidebarTypes[0]];

  return navigationItems.map(type => {
    const onClick = () => {
      setDefaultPhaseSidebar(type);
      navigate(`../${type}`);
    };
    const count = type === 'actions' ? actionCount : taskCount;
    const label = `${t(`phases.sidebar.${type}.title`)} (${count})`;
    const selected = type === sideBarType;
    const icon =
      type === 'actions'
        ? iconNames.play_circle_filled
        : iconNames.library_add_check_icon;

    return { onClick, label, selected, icon } as TopMenuItemType;
  });
};
