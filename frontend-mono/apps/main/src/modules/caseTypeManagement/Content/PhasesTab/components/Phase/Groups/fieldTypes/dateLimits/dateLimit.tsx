// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { useSubForm } from '@zaaksysteem/common/src/components/form/hooks/useSubForm';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { getFormDefinition } from './getFormDefinition';
import { useDateLimitsStyles } from './dateLimits.styles';

type DateLimitPropsType = FormFieldPropsType;

const DateLimit: React.ComponentType<DateLimitPropsType> = ({
  namespace,
  value,
  ...props
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useDateLimitsStyles();

  const dateAttributeChoices = props.config.dateAttributeChoices;
  const formDefinition = getFormDefinition(
    t as any,
    namespace,
    value,
    dateAttributeChoices
  );
  const { fields } = useSubForm({
    formDefinition,
    namespace: `${props.name}[${namespace}]`,
    ...props,
  });

  return (
    <div className={classes.dateLimit}>
      {fields.map(({ FieldComponent, key, label, mode, ...fieldProps }) => (
        <FormControlWrapper
          {...fieldProps}
          key={`${fieldProps.name}-formcontrol-wrapper`}
        >
          <FieldComponent
            {...fieldProps}
            label={label}
            disabled={props.disabled}
          />
        </FormControlWrapper>
      ))}
    </div>
  );
};

export default DateLimit;
