// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import classNames from 'classnames';
import { IconButton } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useTranslation } from 'react-i18next';
import { PhaseType, GroupType, FieldType } from '../../../PhasesTab.types';
import Field from './Field';
import { useGroupsStyles } from './Groups.styles';
import AttributeSearch, {
  AttributeChoiceType,
} from './components/AttributeSearch';
import { OpenGroupDialogType } from './dialogs/useGroupDialog';
import { OpenTextblockDialogType } from './dialogs/useTextblockDialog';
import { OpenAttributeDialogType } from './dialogs/useAttributeDialog';
import { OpenRemoveGroupDialogType } from './dialogs/useRemoveGroupDialog';
import FieldInfo from './FieldInfo';

type GroupPropsType = {
  phase: PhaseType;
  group: GroupType;
  openGroupDialog: OpenGroupDialogType;
  openRemoveGroupDialog: OpenRemoveGroupDialogType;
  openTextblockDialog: OpenTextblockDialogType;
  openAttributeDialog: OpenAttributeDialogType;
  addAttribute: (
    group: GroupType,
    attributeChoice: AttributeChoiceType
  ) => void;
  removeField: (group: GroupType, field: FieldType) => void;
  provided: any;
  snapshot: any;
};

const Group: React.ComponentType<GroupPropsType> = ({
  phase,
  group,
  openGroupDialog,
  openRemoveGroupDialog,
  openTextblockDialog,
  openAttributeDialog,
  addAttribute,
  removeField,
  provided,
  snapshot,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const { id } = group;

  const isFirstGroup = phase.groups[0].id === id;
  const isOnlyGroup = phase.groups.length === 1;
  const canRemove = !isFirstGroup || isOnlyGroup;

  const edit = () => openGroupDialog({ type: 'edit', group });
  const remove = canRemove ? () => openRemoveGroupDialog(group) : undefined;

  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      className={classNames(classes.group, {
        [classes.dragging]: snapshot.isDragging,
      })}
    >
      <div className={classNames(classes.row, classes.groupRow)}>
        <FieldInfo
          field={group}
          edit={edit}
          remove={remove}
          provided={provided}
        />
      </div>
      {/* @ts-ignore */}
      <Droppable droppableId={id}>
        {groupProvided => (
          <div
            {...groupProvided.droppableProps}
            ref={groupProvided.innerRef}
            className={classes.fields}
          >
            {group.fields.map((field, index) => (
              // @ts-ignore
              <Draggable key={field.id} draggableId={field.id} index={index}>
                {(draggableProvided, draggableSnapshot) => (
                  <Field
                    group={group}
                    field={field}
                    openTextblockDialog={openTextblockDialog}
                    openAttributeDialog={openAttributeDialog}
                    removeField={removeField}
                    provided={draggableProvided}
                    isDragging={draggableSnapshot.isDragging}
                  />
                )}
              </Draggable>
            ))}
            {groupProvided.placeholder as any}
            <div className={classes.new}>
              <div className={classes.addButtons}>
                <IconButton
                  onClick={() => openTextblockDialog({ type: 'add', group })}
                  sx={{ padding: 0.95 }}
                >
                  <Tooltip title={t('phases.attributes.textblock.add')}>
                    <Icon size="extraSmall" color="inherit">
                      {iconNames.playlist_add}
                    </Icon>
                  </Tooltip>
                </IconButton>
                <IconButton
                  onClick={() => openGroupDialog({ type: 'add', group })}
                  sx={{ padding: 0.95 }}
                >
                  <Tooltip title={t('phases.attributes.group.add')}>
                    <Icon size="extraSmall" color="inherit">
                      {iconNames.add_card}
                    </Icon>
                  </Tooltip>
                </IconButton>
              </div>
              <div className={classes.searchWrapper}>
                <div className={classes.search}>
                  <AttributeSearch
                    phase={phase}
                    group={group}
                    addAttribute={addAttribute}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </Droppable>
    </div>
  );
};

export default Group;
