// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { IconButton } from '@mui/material';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTranslation } from 'react-i18next';
import { ResultType, UpdateResultsType } from '../../../PhasesTab.types';
import { useGroupsStyles } from './Groups.styles';
import { RESULTS_ZONE, onResultsDragEnd, removeResult } from './Groups.library';
import Result from './Result';
import { useResultDialog } from './dialogs/useResultDialog';
import Placeholder from './components/Placeholder';

export type AddResultType = (result: ResultType) => void;

export type RemoveResultType = (result: ResultType) => void;

type ResultsPropsType = {
  results: ResultType[];
  updateResults: UpdateResultsType;
};

const Results: React.ComponentType<ResultsPropsType> = ({
  results,
  updateResults,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const { openResultDialog, resultDialog } = useResultDialog(
    results,
    updateResults
  );

  const remove: RemoveResultType = result =>
    removeResult(results, updateResults, result);

  return (
    <div>
      {resultDialog}

      {results.length === 0 ? (
        <Placeholder type="result" openDialog={openResultDialog} />
      ) : (
        // @ts-ignore
        <DragDropContext
          onDragEnd={(event: any) =>
            onResultsDragEnd(event, results, updateResults)
          }
        >
          {/* @ts-ignore */}
          <Droppable droppableId={RESULTS_ZONE} type={RESULTS_ZONE}>
            {provided => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className={classes.wrapper}
              >
                {results.map((result, index) => (
                  // @ts-ignore
                  <Draggable
                    key={result.id}
                    draggableId={result.id}
                    index={index}
                  >
                    {(draggableProvided, draggableSnapshot) => (
                      <Result
                        result={result}
                        openResultDialog={openResultDialog}
                        removeResult={remove}
                        provided={draggableProvided}
                        isDragging={draggableSnapshot.isDragging}
                      />
                    )}
                  </Draggable>
                ))}
                {provided.placeholder as any}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      )}
      <div className={classes.new}>
        <div className={classes.addButtons}>
          <IconButton
            onClick={() => openResultDialog({ type: 'add' })}
            sx={{ padding: 0.95 }}
          >
            <Tooltip title={t('phases.attributes.textblock.add')}>
              <Icon size="extraSmall" color="inherit">
                {iconNames.add_task}
              </Icon>
            </Tooltip>
          </IconButton>
        </div>
      </div>
    </div>
  );
};

export default Results;
