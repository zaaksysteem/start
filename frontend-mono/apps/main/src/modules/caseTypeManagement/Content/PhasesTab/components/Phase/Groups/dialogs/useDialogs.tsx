// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { PhaseType, UpdatePhaseType } from '../../../../PhasesTab.types';
import { useAttributeDialog } from './useAttributeDialog';
import { useGroupDialog } from './useGroupDialog';
import { useRemoveGroupDialog } from './useRemoveGroupDialog';
import { useTextblockDialog } from './useTextblockDialog';

export const useDialogs = (phase: PhaseType, updatePhase: UpdatePhaseType) => {
  const { groupDialog, openGroupDialog } = useGroupDialog(phase, updatePhase);
  const { removeGroupDialog, openRemoveGroupDialog } = useRemoveGroupDialog(
    phase,
    updatePhase
  );
  const { textblockDialog, openTextblockDialog } = useTextblockDialog(
    phase,
    updatePhase
  );
  const { attributeDialog, openAttributeDialog } = useAttributeDialog(
    phase,
    updatePhase
  );

  return {
    groupDialog,
    openGroupDialog,
    removeGroupDialog,
    openRemoveGroupDialog,
    textblockDialog,
    openTextblockDialog,
    attributeDialog,
    openAttributeDialog,
  };
};
