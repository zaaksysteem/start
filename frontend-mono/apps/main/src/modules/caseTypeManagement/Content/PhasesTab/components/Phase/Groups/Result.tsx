// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import classNames from 'classnames';
import { capitalize } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTranslation } from 'react-i18next';
import { ResultType } from '../../../PhasesTab.types';
import { CaseTypeManagementContext } from '../../../../../CaseTypeManagement.context';
import { useGroupsStyles } from './Groups.styles';
import { OpenResultDialogType } from './dialogs/useResultDialog';
import DragHandle from './components/DragHandle';
import ActionButtons from './components/ActionButtons';

type ResultPropsType = {
  result: ResultType;
  openResultDialog: OpenResultDialogType;
  removeResult: (result: ResultType) => void;
  isDragging: boolean;
  provided: any;
};

const Result: React.ComponentType<ResultPropsType> = ({
  result,
  openResultDialog,
  removeResult,
  isDragging,
  provided,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useGroupsStyles();

  const { name, resultType, isDefault } = result;

  const { showGrower } = useContext(CaseTypeManagementContext);

  const edit = () => openResultDialog({ type: 'edit', result });
  const remove = () => removeResult(result);

  const showAttributeInfo = showGrower;
  const nameClass = classNames(classes.name, {
    [classes.nameLimit]: showAttributeInfo,
  });

  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      className={classNames(classes.row, classes.fieldRow, {
        [classes.dragging]: isDragging,
      })}
    >
      <DragHandle provided={provided} />
      <div className={nameClass}>{capitalize(resultType)}</div>
      {showGrower && (
        <div className={classes.grower}>
          <div className={classes.typeIcon}>
            {isDefault && (
              <Tooltip title={t(`phases.results.labels.isDefault`)}>
                <Icon size="extraSmall" color="inherit">
                  {iconNames.star_outline}
                </Icon>
              </Tooltip>
            )}
          </div>
          <div>{name}</div>
        </div>
      )}
      <ActionButtons edit={edit} remove={remove} />
    </div>
  );
};

export default Result;
