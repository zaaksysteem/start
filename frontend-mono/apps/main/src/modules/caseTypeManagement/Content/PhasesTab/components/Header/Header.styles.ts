// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useHeaderStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      gap: 10,
      paddingLeft: 20,
      borderBottom: `1px solid ${greyscale.darker}`,
    },
    button: {
      display: 'flex',
      height: '100%',
      alignItems: 'center',
    },
  })
);
