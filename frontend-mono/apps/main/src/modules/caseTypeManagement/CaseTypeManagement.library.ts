// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useParams } from 'react-router-dom';
import { queryClient } from '../../queryClient';
import { CASE_TYPE } from './CaseTypeManagement.requests';
import {
  CaseTypeType,
  GetBreadcrumbsType,
  ParamsType,
} from './CaseTypeManagement.types';

export const useUrl = () => useParams() as ParamsType;

const CATALOG_URL = '/main/catalog';

export const getBreadcrumbs: GetBreadcrumbsType = (t, caseType) => {
  const { uuid, name = '' } = caseType.catalogFolder;

  return [
    { label: t('catalog'), path: CATALOG_URL },
    { label: name, path: `${CATALOG_URL}/${uuid}` },
    { label: caseType.name || t('title') },
  ];
};

export const useUpdateCaseType = () => {
  const { uuid } = useUrl();
  const query = [CASE_TYPE, uuid];

  return (caseTypePartials: any) => {
    const caseType = queryClient.getQueryData(query) as CaseTypeType;
    const newCaseType = { ...caseType, ...caseTypePartials };

    queryClient.setQueryData(query, () => newCaseType);
  };
};
