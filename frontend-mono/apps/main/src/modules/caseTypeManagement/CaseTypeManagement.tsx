// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import useBlockNavigation from '@zaaksysteem/common/src/hooks/useBlockNavigation';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Menu from './Menu/Menu';
import { useUrl } from './CaseTypeManagement.library';
import NavigationDialog from './Dialogs/NavigationDialog/NavigationDialog';
import SaveDialog from './Dialogs/SaveDialog/SaveDialog';
import { useCaseTypeManagementStyles } from './CaseTypeManagement.styles';
import { getTabStates } from './Dialogs/SaveDialog/SaveDialog.library';
import ResetDialog from './Dialogs/ResetDialog/ResetDialog';
import Content from './Content';
import { CaseTypeType } from './CaseTypeManagement.types';

type CaseTypeManagementPropsType = {
  caseType: CaseTypeType;
};

const CaseTypeManagement: React.ComponentType<CaseTypeManagementPropsType> = ({
  caseType,
}) => {
  const [t] = useTranslation('caseTypeManagement');
  const classes = useCaseTypeManagementStyles();
  const ref = useRef(v4());

  const { tab } = useUrl();
  const width = useWidth();

  const menuWidth = ['xs', 'sm', 'md', ...(tab === 'phases' ? ['lg'] : [])];
  const isCompact = menuWidth.includes(width);

  const [saving, setSaving] = useState<boolean>(false);
  const [resetting, setResetting] = useState<boolean>(false);

  useEffect(() => {
    if (!resetting) ref.current = v4();
  }, [resetting]);

  const [initialCaseType, setInitialCaseType] =
    useState<CaseTypeType>(caseType);

  const tabStates = getTabStates(caseType, initialCaseType);
  const valid = Object.values(tabStates).every(tabState => tabState.valid);
  const hasUnsavedChanges = Object.values(tabStates).some(
    tabState => tabState.changed
  );

  const { blocked, resolvedBlock } = useBlockNavigation(
    hasUnsavedChanges,
    'case-type'
  );

  return (
    <PanelLayout>
      <Panel type="side" folded={isCompact}>
        <Menu tabStates={tabStates} isCompact={isCompact} />
      </Panel>
      <Panel>
        <div
          className={classNames(
            classes.wrapper,
            hasUnsavedChanges ? classes.saving : classes.notSaving
          )}
        >
          <div key={tab + ref.current} className={classes.content}>
            <Content />
          </div>
          <div className={classes.saveBar}>
            <Button
              variant="outlined"
              action={() => {
                setResetting(true);
              }}
              name="reset"
            >
              {t('reset.title')}
            </Button>
            <Button
              action={() => {
                setSaving(true);
              }}
              name="save"
              disabled={!valid}
            >
              {t('save.title')}
            </Button>
          </div>
        </div>
      </Panel>

      <NavigationDialog
        valid={valid}
        onConfirm={() => {
          setSaving(true);
          resolvedBlock();
        }}
        onClose={resolvedBlock}
        open={blocked}
      />
      {saving && (
        <SaveDialog
          caseType={caseType}
          setInitialCaseType={setInitialCaseType}
          tabStates={tabStates}
          onClose={() => setSaving(false)}
          open={saving}
        />
      )}
      {resetting && (
        <ResetDialog
          caseType={caseType}
          initialCaseType={initialCaseType}
          tabStates={tabStates}
          onClose={() => setResetting(false)}
          open={resetting}
        />
      )}
    </PanelLayout>
  );
};

export default CaseTypeManagement;
