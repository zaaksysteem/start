// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import auth from './auth.locale';
import form from './form.locale';
import phases from './phases.locale';
import children from './children.locale';

export default {
  nl: {
    ...form,
    ...phases,
    ...auth,
    ...children,

    title: 'Zaaktypebeheer',
    catalog: 'Catalogus',
    unsavedChangesWarning:
      'Let op: U heeft onopgeslagen wijzigingen. Weet u zeker dat u het zaaktypebeheer wilt verlaten?',
    tabs: {
      common: 'Algemeen',
      documentation: 'Documentatie',
      avg: 'AVG',
      relations: 'Relaties',
      registrationform: 'Registratieformulier',
      webform: 'Webformulier',
      case_dossier: 'Zaakdossier',
      api: 'API',
      phases: 'Fasen',
      authorization: 'Rechten',
      children: 'Kinderen',
    },
    navigation: {
      title: 'Onopgeslagen wijzigingen',
      content:
        'Er zijn onopgeslagen wijzigingen. Door het navigeren naar deze pagina zullen uw wijzigingen verloren gaan. Weet u zeker dat u door wilt gaan?',
      contentInvalid:
        'Er zijn onopgeslagen wijzigingen. De wijzigingen zijn niet valide en kunnen niet opgeslagen worden. Door het navigeren naar deze pagina zullen uw wijzigingen verloren gaan. Weet u zeker dat u door wilt gaan?',
      save: 'Wijzigingen publiceren',
      continue: 'OK',
    },
    save: {
      title: 'Publiceren',
      publish: 'Publiceren',
      description: 'Publiceer de wijzigingen in de volgende onderdelen.',
      supportWarning:
        'Let op: Deze versie van zaaktypebeheer wordt nog niet ondersteund. Publiceren kan ongewenste gevolgen hebben voor het zaaktype.',
      childrenWarning:
        'Let op: Dit zaaktype heeft kindzaaktypen ingesteld. Publiceer deze om de wijzigingen ook in die zaaktypen door te voeren.',
      fields: {
        updateDescription: 'Wijzigingsomschrijving',
      },
      snack: 'Zaaktype gepubliceerd.',
    },
    reset: {
      title: 'Wijzigingen terugdraaien',
      fields: {
        sections:
          'Selecteer de onderdelen waarvan de wijzigingen moeten worden teruggedraaid.',
      },
    },
  },
};
