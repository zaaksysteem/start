// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  auth: {
    title: '$t(tabs.authorization)',
    description:
      'De ingestelde rechten zijn van toepassing op zaken van alle versies van dit zaaktype.',
    type: {
      public: 'Openbaar / Intern',
      confidential: 'Vertrouwelijk',
    },
    level: {
      zaak_beheer: 'Beheren',
      zaak_edit: 'Behandelen',
      zaak_read: 'Raadplegen',
      zaak_search: 'Zoeken',
    },
    placeholders: {
      department: 'Selecteer een afdeling…',
      role: 'Selecteer een rol…',
    },
    action: {
      copy: 'Alles kopiëren',
      paste: 'Alles toevoegen',
      overwrite: 'Alles overschrijven',
      remove: 'Verwijderen',
      removeAll: 'Alles verwijderen',
    },
    csvHeader: {
      type: 'Type',
      department: 'Afdeling',
      role: 'Rol',
      level: 'Niveau',
    },
  },
};
