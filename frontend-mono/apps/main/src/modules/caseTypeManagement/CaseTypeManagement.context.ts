// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createContext, useState } from 'react';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { SideBarType } from './Content/PhasesTab/PhasesTab.types';
import { sidebarTypes } from './Content/PhasesTab/components/Phase/Phase.library';

type CaseTypeManagementContextType = {
  defaultPhase: string;
  setDefaultPhase: (phaseNumber: string) => void;
  defaultPhaseSideBar: SideBarType;
  setDefaultPhaseSidebar: (sideBar: SideBarType) => void;
  phasesStackedMode: boolean;
  setPhasesStackedMode: (stackedMode: boolean) => void;
  showTitles: boolean;
  setShowTitles: (show: boolean) => void;
  isCompact: boolean;
  allowSideBySide: boolean;
  showGrower: boolean;
  showIcons: boolean;
};

export const defaultContext = {
  defaultPhase: '1',
  setDefaultPhase: () => {},
  defaultPhaseSideBar: sidebarTypes[0],
  setDefaultPhaseSidebar: () => {},
  phasesStackedMode: true,
  setPhasesStackedMode: () => {},
  showTitles: false,
  setShowTitles: () => {},
  isCompact: false,
  allowSideBySide: false,
  showGrower: false,
  showIcons: false,
};

export const CaseTypeManagementContext =
  createContext<CaseTypeManagementContextType>(defaultContext);

/* eslint complexity: [2, 10] */
export const useContext = () => {
  const [defaultPhase, setDefaultPhase] = useState<string>('1');
  const [phasesStackedMode, setPhasesStackedMode] = useState<boolean>(true);
  const [showTitles, setShowTitles] = useState<boolean>(false);
  const [defaultPhaseSideBar, setDefaultPhaseSidebar] = useState<SideBarType>(
    sidebarTypes[0]
  );

  const width = useWidth();
  const isFullSized = ['xxl'].includes(width);
  const isSpacious = ['lg', 'xl', 'xxl'].includes(width);
  const isCompact = ['xs', 'sm', 'md'].includes(width);
  const isVeryCompact = ['xs', 'sm'].includes(width);

  const allowSideBySide = !isCompact;
  const isStacked = phasesStackedMode || !allowSideBySide;

  const showGrower = (isStacked && !isCompact) || (!isStacked && isFullSized);
  const showIcons = (isStacked && !isVeryCompact) || (!isStacked && isSpacious);

  return {
    defaultPhase,
    setDefaultPhase,

    defaultPhaseSideBar,
    setDefaultPhaseSidebar,

    phasesStackedMode,
    setPhasesStackedMode,

    showTitles,
    setShowTitles,

    isCompact,
    allowSideBySide,
    showGrower,
    showIcons,
  };
};
