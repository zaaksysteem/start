// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  allocationsSection,
  baseSectionsDict,
  firstPhaseSection,
  lastPhaseSection,
  resultSection,
} from '../Content/ChildrenTab/ChildrenTab.library';
import {
  GetBaseSectionsType,
  GetPhaseSectionsType,
  ConvertChildrenType,
  PhaseSectionType,
  RevertChildrenType,
} from '../Content/ChildrenTab/ChildrenTab.types';

const getBaseSections: GetBaseSectionsType = settings =>
  baseSectionsDict.filter(section => settings[section]);

const getPhaseSettings: GetPhaseSectionsType = (settings, phases) => {
  const otherPhaseNumbers = phases
    .map((phase, index) => index + 1)
    .slice(1, -1);

  const firstPart = [allocationsSection, firstPhaseSection].filter(
    section => settings[section]
  );

  const middlePart = otherPhaseNumbers
    .filter(phaseNumber => (settings.middle_phases || []).includes(phaseNumber))
    .map(phaseNumber => `${phaseNumber}`) as PhaseSectionType[];

  const lastPart = [lastPhaseSection, resultSection].filter(
    section => settings[section]
  );

  return [...firstPart, ...middlePart, ...lastPart];
};

export const convertChildren: ConvertChildrenType = (
  childCaseTypeSettings,
  phases
) =>
  childCaseTypeSettings.child_casetypes.map(
    ({ casetype, enabled, settings }) => ({
      uuid: v4(),
      active: enabled,
      caseType: {
        label: casetype.name,
        value: casetype.uuid,
      },
      baseSections: getBaseSections(settings),
      phaseSections: getPhaseSettings(settings, phases),
    })
  );

export const revertChildren: RevertChildrenType = children => ({
  enabled: Boolean(children.length),
  child_casetypes: children.map(
    ({ caseType, active, baseSections, phaseSections }) => ({
      casetype: {
        name: caseType?.label as string,
        uuid: caseType?.value as string,
      },
      enabled: active,
      settings: [...baseSections, ...phaseSections].reduce(
        (acc, item) => ({ ...acc, [item]: true }),
        {}
      ),
    })
  ),
});
