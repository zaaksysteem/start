// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  initiatorTypeValues,
  kindValues,
  processingLegalValues,
  processingTypeValues,
  sourceValues,
} from '../Content/FormTab/FormTab.choices';
import {
  ConvertCaseTypeFromApiType,
  RevertCaseTypeToApiType,
} from '../CaseTypeManagement.types';
import { convertPhases, revertPhases } from './phases.conversions';
import { convertChildren, revertChildren } from './children.conversions';
import { convertAuths, revertAuths } from './auth.conversions';
import {
  convertAllowedRequestorTypes,
  convertCheckboxList,
  convertLeadTime,
  revertCheckboxList,
  revertLeadTime,
  revertRequestorTypes,
  convertDefaultDocumentFolders,
  revertDefaultDocumentFolders,
  convertPresetRequestor,
  convertPresetAssignee,
  revertPresetRequestor,
  revertPresetAssignee,
} from './form.conversions';

/* eslint complexity: [2, 999] */
export const convertCaseTypeFromApi: ConvertCaseTypeFromApiType = ({
  data: {
    attributes: {
      casetype_uuid,
      active,
      catalog_folder,

      general_attributes,
      documentation,
      relations,
      case_dossier,
      registrationform,
      webform,
      api,
      api: { notifications },
      phases,
      results,
      authorization: authorizations,
      child_casetype_settings,
    },
  },
}) => {
  const leadTimeValues = convertLeadTime(
    general_attributes.legal_period,
    general_attributes.service_period
  );

  const source = convertCheckboxList(documentation.gdpr?.source);
  const kind = convertCheckboxList(documentation.gdpr?.kind);

  const allowedRequestorTypes = convertAllowedRequestorTypes(
    relations.allowed_requestor_types
  );
  const presetRequestor = convertPresetRequestor(relations.preset_requestor);
  const presetAssignee = convertPresetAssignee(relations.api_preset_assignee);

  const defaultDocumentFolders = convertDefaultDocumentFolders(
    case_dossier.default_document_folders
  );

  const convertedPhases = convertPhases(phases || [], results || []);
  const authorization = convertAuths(authorizations);
  const children = convertChildren(child_casetype_settings, convertedPhases);

  return {
    uuid: casetype_uuid,
    catalogFolder: catalog_folder,
    active,
    name: general_attributes.name || '',

    common: {
      name: general_attributes.name || '',
      identification: general_attributes.identification || '',
      tags: general_attributes.tags || '',
      description: general_attributes.description || '',
      summaryInternal: general_attributes.case_summary || '',
      summaryExternal: general_attributes.case_public_summary || '',
      ...leadTimeValues,
    },
    documentation: {
      processDescription: documentation.process_description || '',
      initiatorType: documentation.initiator_type || initiatorTypeValues[0],
      motivation: documentation.motivation || '',
      purpose: documentation.purpose || '',
      archiveClassificationCode:
        documentation.archive_classification_code || '',
      designationOfConfidentiality:
        documentation.designation_of_confidentiality || null,
      responsibleSubject: documentation.responsible_subject || '',
      responsibleRelationship: documentation.responsible_relationship || '',
      possibilityForObjectionAndAppeal:
        documentation.possibility_for_objection_and_appeal || false,
      publication: documentation.publication || false,
      publicationText: documentation.publication_text || '',
      bag: documentation.bag || false,
      lexSilencioPositivo: documentation.lex_silencio_positivo || false,
      mayPostpone: documentation.may_postpone || false,
      mayExtend: documentation.may_extend || false,
      extensionPeriod: documentation.extension_period
        ? `${documentation.extension_period}`
        : '',
      adjournPeriod: documentation.adjourn_period
        ? `${documentation.adjourn_period}`
        : '',
      penaltyLaw: documentation.penalty_law || false,
      wkpbApplies: documentation.wkpb_applies || false,
      eWebform: documentation.e_webform || '',
      legalBasis: documentation.legal_basis || '',
      localBasis: documentation.local_basis || '',

      gdprEnabled: documentation.gdpr?.enabled || false,
      source,
      kind,
      processingType:
        documentation.gdpr?.processing_type || processingTypeValues[0],
      processForeignCountry:
        documentation.gdpr?.process_foreign_country || false,
      processForeignCountryReason:
        documentation.gdpr?.process_foreign_country_reason || '',
      processingLegal:
        documentation.gdpr?.processing_legal || processingLegalValues[0],
      processingLegalReason: documentation.gdpr?.processing_legal_reason || '',
    },
    relations: {
      allowedRequestorTypes,
      presetRequestor,
      presetAssignee,
      requestorAddressCase:
        relations.address_requestor_use_as_case_address || false,
      requestorAddressCorrespondence:
        relations.address_requestor_use_as_correspondence || false,
      requestorAddressMap: relations.address_requestor_show_on_map || false,
    },
    registrationform: {
      allowAssigningToSelf: registrationform.allow_assigning_to_self || false,
      allowAssigning: registrationform.allow_assigning || false,
      showConfidentiality: registrationform.show_confidentionality || false,
      showContactDetails: registrationform.show_contact_details || false,
      allowAddRelations: registrationform.allow_add_relations || false,
    },
    webform: {
      publicConfirmationTitle: webform.public_confirmation_title || '',
      publicConfirmationMessage: webform.public_confirmation_message || '',
      caseLocationMessage: webform.case_location_message || '',
      pipViewMessage: webform.pip_view_message || '',
      enableWebform: webform.actions?.enable_webform || false,
      createDelayed: webform.actions?.create_delayed || false,
      addressCheck: webform.actions?.address_check || false,
      reUseCaseData: webform.actions?.reuse_casedata || false,
      enableOnlinePayment: webform.actions?.enable_online_payment || false,
      enableManualPayment: webform.actions?.enable_manual_payment || false,
      emailRequired: webform.actions?.email_required || false,
      phoneRequired: webform.actions?.phone_required || false,
      mobileRequired: webform.actions?.mobile_required || false,
      disableCaptcha: webform.actions?.disable_captcha || false,
      generatePdfEndWebform: webform.actions?.generate_pdf_end_webform || false,

      priceFrontdesk: `${webform.price?.frontdesk || ''}`,
      priceAssignee: `${webform.price?.assignee || ''}`,
      priceChat: `${webform.price?.chat || ''}`,
      priceEmail: `${webform.price?.email || ''}`,
      pricePost: `${webform.price?.post || ''}`,
      pricePhone: `${webform.price?.phone || ''}`,
      priceWebform: `${webform.price?.web || ''}`,
      priceOther: `${webform.price?.other || ''}`,
    },
    case_dossier: {
      disablePipForRequestor: case_dossier.disable_pip_for_requestor || false,
      lockRegistrationPhase: case_dossier.lock_registration_phase || false,
      queueCoworkerChanges: case_dossier.queue_coworker_changes || false,
      allowExternalTaskAssignement:
        case_dossier.allow_external_task_assignment || false,
      defaultDocumentFolders,
      htmlEmailTemplate: case_dossier.default_html_email_template || null,
    },
    api: {
      apiCanTransition: api.api_can_transition || false,

      notifyOnNewCase: notifications?.external_notify_on_new_case || false,
      notifyOnNewDocument:
        notifications?.external_notify_on_new_document || false,
      notifyOnNewMessage:
        notifications?.external_notify_on_new_message || false,
      notifyOnExceedTerm:
        notifications?.external_notify_on_exceed_term || false,
      notifyOnAllocate:
        notifications?.external_notify_on_allocate_case || false,
      notifyOnPhaseTransition:
        notifications?.external_notify_on_phase_transition || false,
      notifyOnTaskChange:
        notifications?.external_notify_on_task_change || false,
      notifyOnLabelChange:
        notifications?.external_notify_on_label_change || false,
      notifyOnSubjectChange:
        notifications?.external_notify_on_subject_change || false,
    },

    phases: convertedPhases,
    authorization,
    children,
  };
};

export const revertCaseTypeToApi: RevertCaseTypeToApiType = values => {
  const {
    uuid,
    active,
    common,
    documentation,
    relations,
    webform,
    registrationform,
    api,
  } = values;

  const { legal_period, service_period } = revertLeadTime(values);

  const source = revertCheckboxList(sourceValues, documentation.source) as any;
  const kind = revertCheckboxList(kindValues, documentation.kind) as any;

  const allowed_requestor_types = revertRequestorTypes(values);

  const preset_requestor = revertPresetRequestor(relations.presetRequestor);
  const api_preset_assignee = revertPresetAssignee(relations.presetAssignee);

  const default_document_folders = revertDefaultDocumentFolders(
    values.case_dossier.defaultDocumentFolders
  );

  const { phases, results } = revertPhases(values.phases);
  const authorization = revertAuths(values.authorization);
  const child_casetype_settings = revertChildren(values.children);

  const data = {
    casetype_uuid: uuid,
    active,
    general_attributes: {
      name: common.name,
      identification: common.identification,
      tags: common.tags,
      description: common.description,
      case_summary: common.summaryInternal,
      case_public_summary: common.summaryExternal,
      legal_period,
      service_period,
    },
    documentation: {
      process_description: documentation.processDescription,
      initiator_type: documentation.initiatorType,
      motivation: documentation.motivation,
      purpose: documentation.purpose,
      archive_classification_code: documentation.archiveClassificationCode,
      designation_of_confidentiality:
        documentation.designationOfConfidentiality || undefined,
      responsible_subject: documentation.responsibleSubject,
      responsible_relationship: documentation.responsibleRelationship,
      possibility_for_objection_and_appeal:
        documentation.possibilityForObjectionAndAppeal,
      publication: documentation.publication,
      publication_text: documentation.publicationText,
      bag: documentation.bag,
      lex_silencio_positivo: documentation.lexSilencioPositivo,
      may_postpone: documentation.mayPostpone,
      may_extend: documentation.mayExtend,
      extension_period: documentation.extensionPeriod
        ? Number(documentation.extensionPeriod)
        : undefined,
      adjourn_period: documentation.adjournPeriod
        ? Number(documentation.adjournPeriod)
        : undefined,
      penalty_law: documentation.penaltyLaw,
      wkpb_applies: documentation.wkpbApplies,
      e_webform: documentation.eWebform,
      legal_basis: documentation.legalBasis,
      local_basis: documentation.localBasis,
      gdpr: {
        enabled: documentation.gdprEnabled,
        source,
        kind,
        processing_type: documentation.processingType,
        process_foreign_country: documentation.processForeignCountry,
        process_foreign_country_reason:
          documentation.processForeignCountryReason,
        processing_legal: documentation.processingLegal,
        processing_legal_reason: documentation.processingLegalReason,
      },
    },
    relations: {
      allowed_requestor_types,
      preset_requestor,
      api_preset_assignee,
      address_requestor_use_as_case_address: relations.requestorAddressCase,
      address_requestor_use_as_correspondence:
        relations.requestorAddressCorrespondence,
      address_requestor_show_on_map: relations.requestorAddressMap,
    },
    registrationform: {
      allow_assigning_to_self: registrationform.allowAssigningToSelf,
      allow_assigning: registrationform.allowAssigning,
      show_confidentionality: registrationform.showConfidentiality,
      show_contact_details: registrationform.showContactDetails,
      allow_add_relations: registrationform.allowAddRelations,
    },
    webform: {
      public_confirmation_title: webform.publicConfirmationTitle,
      public_confirmation_message: webform.publicConfirmationMessage,
      case_location_message: webform.caseLocationMessage,
      pip_view_message: webform.pipViewMessage,
      actions: {
        enable_webform: webform.enableWebform,
        create_delayed: webform.createDelayed,
        address_check: webform.addressCheck,
        reuse_casedata: webform.reUseCaseData,
        enable_online_payment: webform.enableOnlinePayment,
        enable_manual_payment: webform.enableManualPayment,
        email_required: webform.emailRequired,
        phone_required: webform.phoneRequired,
        mobile_required: webform.mobileRequired,
        disable_captcha: webform.disableCaptcha,
        generate_pdf_end_webform: webform.generatePdfEndWebform,
      },
      price: {
        web: webform.priceWebform,
        frontdesk: webform.priceFrontdesk,
        phone: webform.pricePhone,
        email: webform.priceEmail,
        assignee: webform.priceAssignee,
        post: webform.pricePost,
      },
    },
    case_dossier: {
      disable_pip_for_requestor: values.case_dossier.disablePipForRequestor,
      lock_registration_phase: values.case_dossier.lockRegistrationPhase,
      queue_coworker_changes: values.case_dossier.queueCoworkerChanges,
      allow_external_task_assignment:
        values.case_dossier.allowExternalTaskAssignement,
      default_document_folders,
      default_html_email_template:
        values.case_dossier.htmlEmailTemplate || undefined,
    },
    api: {
      api_can_transition: api.apiCanTransition,
      notifications: {
        external_notify_on_new_case: api.notifyOnNewCase,
        external_notify_on_new_document: api.notifyOnNewDocument,
        external_notify_on_new_message: api.notifyOnNewMessage,
        external_notify_on_exceed_term: api.notifyOnExceedTerm,
        external_notify_on_allocate_case: api.notifyOnAllocate,
        external_notify_on_phase_transition: api.notifyOnPhaseTransition,
        external_notify_on_task_change: api.notifyOnTaskChange,
        external_notify_on_label_change: api.notifyOnLabelChange,
        external_notify_on_subject_change: api.notifyOnSubjectChange,
      },
    },

    phases,
    results,
    authorization,
    child_casetype_settings,
  };

  return data;
};
