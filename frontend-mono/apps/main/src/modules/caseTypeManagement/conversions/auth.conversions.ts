// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { asArray } from '@mintlab/kitchen-sink/source';
import { authLevels } from '../Content/AuthTab/AuthTab.library';
import {
  GetAuthLevelType,
  FormatAuthType,
  ConvertAuthsType,
  RevertAuthsType,
} from '../Content/AuthTab/AuthTab.types';

// find the highest auth level
const getAuthLevel: GetAuthLevelType = rights =>
  [...authLevels].reverse().find(level => rights.includes(level)) ||
  authLevels[0];

const formatAuth: FormatAuthType = ({
  department_uuid: department,
  role_uuid: role,
  confidential,
  rights,
}) => {
  const type = confidential ? 'confidential' : 'public';
  const level = getAuthLevel(rights);
  const order = authLevels.indexOf(level) + (type === 'confidential' ? 10 : 0);

  return { uuid: v4(), department, role, type, level, order };
};

export const convertAuths: ConvertAuthsType = auths =>
  auths.map(formatAuth).sort((authA, authB) => authA.order - authB.order);

export const revertAuths: RevertAuthsType = auths =>
  auths.map(({ department, role, type, level }) => ({
    department_uuid: department as string,
    // @ts-ignore
    role_uuid: typeof role === 'string' ? role : role?.value,
    confidential: type === 'confidential',
    rights: asArray(level as string),
  }));
