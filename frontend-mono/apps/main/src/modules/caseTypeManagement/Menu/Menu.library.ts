// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { iconNames, IconNameType } from '@mintlab/ui/App/Material/Icon';
import { TabType } from '../CaseTypeManagement.types';

type MenuItemsConfigType = {
  [key in TabType]: IconNameType;
};

export const menuItemsConfig: MenuItemsConfigType = {
  common: iconNames.ballot,
  documentation: iconNames.article,
  relations: iconNames.link,
  registrationform: iconNames.web,
  webform: iconNames.web,
  case_dossier: iconNames.folder,
  api: iconNames.quickreply,
  phases: iconNames.category,
  authorization: iconNames.admin_panel_settings,
  children: iconNames.lan,
};

export const tabs: TabType[] = Object.keys(menuItemsConfig) as TabType[];
