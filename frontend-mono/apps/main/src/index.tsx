// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/// <reference path='../../../packages/ui/types/Theme.d.ts'/>

import React from 'react';
import ReactDOM from 'react-dom';
import { setupI18n } from './i18n';
import App from './App';
import './index.css';
import { checkForExports } from './checkForZipExports';

window.onerror = null;

const oldFetch = window.fetch;

window.fetch = (resource, options) => {
  const jwtToken = localStorage.getItem('jwt_token');
  const headers = jwtToken
    ? { Authorization: 'Bearer ' + jwtToken }
    : ({} as {});

  if (
    resource.toString().startsWith(window.location.origin) ||
    resource.toString().startsWith('/')
  ) {
    if (options) {
      return oldFetch(resource, {
        ...options,
        headers: {
          ...headers,
          ...(options.headers || {}),
        },
      });
    }

    return oldFetch(resource, { headers });
  }

  return oldFetch(resource, options);
};

const render = () =>
  setupI18n().then(
    () => void ReactDOM.render(<App />, document.getElementById('root'))
  );

try {
  const iframed = window.self !== window.top;
  const inExternalComps = window.location.pathname.includes(
    'external-components'
  );

  if (iframed && !inExternalComps) {
    const slug = window.location.href.split('/catalog')[1];
    window.top?.postMessage({ type: 'IFRAME:RELOAD', slug }, '*');
  } else if (iframed && inExternalComps) {
    render();
  } else {
    render();
    checkForExports();
  }
} catch (err) {
  console.log(err);
  render();
}

if ('serviceWorker' in navigator && !process.env.SKIP_SW) {
  navigator.serviceWorker
    .register('/main/service-worker.js', {
      scope: '/main/',
    })
    .catch(err => console.error(err));
}
