// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import * as UiSnackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import { snackbarMessage } from '@zaaksysteem/common/src/signals';

const Snackbar = () => {
  const [t] = useTranslation();
  const text = t(
    //@ts-ignore
    snackbarMessage.value.message,
    snackbarMessage.value.interpolations || {}
  );

  return (
    <UiSnackbar.default
      autoHideDuration={snackbarMessage.value.autoHideDuration}
      sx={snackbarMessage.value.sx}
      scope={snackbarMessage.value.message}
      message={
        snackbarMessage.value.link ? (
          <>
            {text}
            <Button
              iconSize="small"
              sx={{
                color: 'white',
                marginLeft: '10px',
                '&:hover': {
                  background: '#FFFFFF33',
                },
              }}
              href={snackbarMessage.value.link}
              name="snackbarLink"
              target="_blank"
              icon={snackbarMessage.value.download ? 'download' : 'open_in_new'}
            />
          </>
        ) : (
          text
        )
      }
      open={Boolean(snackbarMessage.value.message)}
      handleClose={() => {
        snackbarMessage.value = { message: '' };
      }}
    />
  );
};

export default Snackbar;
