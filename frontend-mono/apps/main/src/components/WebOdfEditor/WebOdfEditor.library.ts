// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { v4 } from 'uuid';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { queryClient } from '../../queryClient';
import {
  GET_CASE_DOCUMENTS,
  GET_DOCUMENT_VERSIONS,
} from '../../sharedQueryKeys';

export const convertBlobToBase64 = (blob: Blob) =>
  new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      typeof reader.result === 'string' && resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

export const applyOdfCss = () => {
  const style = document.createElement('style');
  document.body.classList.add('root');

  style.innerText = `
    .webodf-virtualSelections {
      margin: 10px 15px;
      box-shadow: 0px 0px 10px 1px #b2b2b2;
    }
    body.root {
      height: auto!important;
      background-color: #eee!important;
      box-sizing: border-box!important;
      margin: 0!important;
      font-size: 15px!important;
    }
    div#root {
      height: auto
    }
  `;

  document.body.appendChild(style);

  return style;
};

export const startOdfSession = (
  odfCanvas: any,
  setParams: any,
  memberId = 'localuser'
) => {
  //@ts-ignore
  const gui = window.gui;
  //@ts-ignore
  const ops = window.ops;
  const session = new ops.Session(odfCanvas);
  const odfDocument = session.getOdtDocument();
  const cursor = new gui.ShadowCursor(odfDocument);
  const sessionController = new gui.SessionController(
    session,
    memberId,
    cursor,
    {
      directTextStylingEnabled: true,
      directParagraphStylingEnabled: true,
    }
  );
  const formattingController =
    sessionController.getDirectFormattingController();

  const viewOptions = {
    editInfoMarkersInitiallyVisible: false,
    caretAvatarsInitiallyVisible: false,
    caretBlinksOnRangeSelect: true,
  };
  const caretManager = new gui.CaretManager(
    sessionController,
    odfCanvas.getViewport()
  );
  const selectionViewManager = new gui.SelectionViewManager(
    gui.SvgSelectionView
  );
  const sessionConstraints = sessionController.getSessionConstraints();
  new gui.SessionView(
    viewOptions,
    memberId,
    session,
    sessionConstraints,
    caretManager,
    selectionViewManager
  );
  selectionViewManager.registerCursor(cursor, true);
  sessionController.setUndoManager(new gui.TrivialUndoManager());

  var op = new ops.OpAddMember();
  op.init({
    memberid: memberId,
    setProperties: {
      fullName: '',
      color: 'black',
      imageUrl: '',
    },
  });
  session.enqueue([op]);

  sessionController.insertLocalCursor();
  sessionController.startEditing();

  formattingController.subscribe(
    gui.DirectFormattingController.textStylingChanged,
    (params: any) => {
      setParams(params);
    }
  );

  formattingController.subscribe(
    gui.DirectFormattingController.paragraphStylingChanged,
    (params: any) => {
      setParams(params);
    }
  );

  return {
    formattingController,
    sessionController,
  };
};

export const closeEditor = () => {
  window.parent?.location.href.includes('documenten/v2')
    ? window.parent.location.reload()
    : window.parent?.postMessage('odfclose', '*');
};

export const saveFile = (
  id: string,
  uuid: string,
  caseUuid: string,
  title: string,
  blob: Blob
) => {
  if (window.parent?.location.href.includes('documenten/v2')) {
    const data = new FormData();

    data.append('document_file', new File([blob], title));
    data.append('file_id', id);
    data.append('duplicateAllowed', 'true');
    data.append('document_uuid', v4());
    data.append('case_uuid', caseUuid);
    data.append('replaces', uuid);
    data.append(
      'case_id',
      window.parent.location.href.split('/intern/zaak/')[1].split('/')[0]
    );

    request('POST', '/api/v2/document/create_document', data, {
      type: 'formdata',
      cached: false,
    });
  } else {
    window.parent?.postMessage({ type: 'save', id, blob }, '*');
  }
};

export const useWebodfLock = (fileId: string) => {
  const renewLock = () =>
    fetch('/file/' + fileId + '/lock/acquire', {
      method: 'GET',
    });

  React.useEffect(() => {
    let shouldRenew = true;

    if (window.location.href.includes('documents=v2')) {
      //@ts-ignore
      window.history.pushState({}, '', new URL(window.parent?.location));

      renewLock().then(() => {
        setTimeout(() => {
          shouldRenew && renewLock();
        }, 600000);
      });
    }

    return () => {
      queryClient.invalidateQueries([GET_CASE_DOCUMENTS]);
      queryClient.invalidateQueries([GET_DOCUMENT_VERSIONS]);
      shouldRenew = false;
    };
  });
};
