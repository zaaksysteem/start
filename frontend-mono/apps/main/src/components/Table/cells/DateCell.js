// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import fecha from 'fecha';
import { withStyles } from '@mui/styles';
import { cellStyleSheet } from './cells.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.formatDate
 * @param {string} props.value
 * @return {ReactElement}
 */
export const DateCell = ({ classes, value, t }) => {
  const date = new Date(value);

  return (
    <div className={classes.dateTime}>
      <span className={classNames(classes.dateTimeWrapper, classes.date)}>
        {fecha.format(date, t('common:dates:dateFormat'))}
      </span>
      <span className={classNames(classes.dateTimeWrapper, classes.time)}>
        {fecha.format(date, t('common:dates:timeFormatFull'))}
      </span>
    </div>
  );
};

export default withStyles(cellStyleSheet)(DateCell);
