// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import Render from '@mintlab/ui/App/Abstract/Render';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { cellStyleSheet } from './cells.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.handleNavigate
 * @param {string} props.value
 * @param {string} props.path
 * @param {string} props.scope
 * @return {ReactElement}
 */
export const LinkCell = ({ classes, handleNavigate, value, path, scope }) => (
  <Render condition={value}>
    <Tooltip title={value} noWrap={true} sx={{ width: 'unset' }}>
      <a
        href={path}
        className={classes.link}
        onClick={handleNavigate}
        {...addScopeAttribute(scope, 'link')}
      >
        {value}
      </a>
    </Tooltip>
  </Render>
);

export default withStyles(cellStyleSheet)(LinkCell);
