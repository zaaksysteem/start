// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Render from '@mintlab/ui/App/Abstract/Render';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';

/**
 * @param {Object} props
 * @param {string} props.value
 * @return {ReactElement}
 */
export const IconCell = ({ value }) => (
  <Render condition={value}>
    <ZsIcon style={{ fontSize: 24 }}>{value}</ZsIcon>
  </Render>
);

export default IconCell;
