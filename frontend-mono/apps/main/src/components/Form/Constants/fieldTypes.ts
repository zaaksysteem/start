// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const CHECKBOX = 'checkbox';
export const EMAIL = 'email';
export const LOCATION_FINDER = 'locationFinder';
export const MAGICSTRING = 'magicString';
export const PRODUCT_FINDER = 'productFinder';
export const SELECT = 'select';
export const TEXT = 'text';
export const TEXTAREA = 'textarea';
export const TYPES = 'types';
export const VERSIONS = 'versions';
export const UPLOAD = 'file';
export const FLATVALUE_SELECT = 'flatValueSelect';
export const ATTRIBUTE_LIST = 'attributeList';
export const CASETYPE_RELATION_LIST = 'caseTypeRelationList';
export const OBJECTTYPE_RELATION_LIST = 'objectTypeRelationList';
export const OBJECT_TYPE_FINDER = 'objectTypeFinder';
export const OPTIONS = 'options';
