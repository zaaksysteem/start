// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const IFRAME_OVERLAY_CLASS = 'iframe-overlay';

export const setLegacyBodyClass = (hasOverlay: boolean) => {
  hasOverlay
    ? document.body.classList.add(IFRAME_OVERLAY_CLASS)
    : document.body.classList.remove(IFRAME_OVERLAY_CLASS);
};
