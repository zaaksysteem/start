// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getUrl, navigate, parseUrl } from './url';

describe('The `url` module', () => {
  describe('exports an `getUrl` function for the window location object that', () => {
    test('returns the path component when a fragment identifier is present', () => {
      window.location.assign('/foo#bar');

      const actual = getUrl();
      const expected = '/foo';

      expect(actual).toBe(expected);
    });

    test('returns the path and query components', () => {
      window.location.assign('/foo?bar');

      const actual = getUrl();
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `navigate` function that', () => {
    test('cannot be tested with jest', () => {
      window.location.assign('/foo');

      navigate('/bar');

      const actual = getUrl();
      const expected = '/bar';

      expect(actual).toBe(expected);
    });
  });

  describe('exports an `parseUrl` function for a string that', () => {
    test('returns the path and query components', () => {
      const actual = parseUrl('https://example.org/foo?bar#quux');
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });

    test('strips a trailing slash', () => {
      const actual = parseUrl('https://example.org/foo/');
      const expected = '/foo';

      expect(actual).toBe(expected);
    });

    test('strips a trailing slash', () => {
      const actual = parseUrl('https://example.org/foo/?bar');
      const expected = '/foo?bar';

      expect(actual).toBe(expected);
    });
  });
});
