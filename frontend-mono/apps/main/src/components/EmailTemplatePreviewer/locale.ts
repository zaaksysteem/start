// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    to: 'Aan',
    from: 'Van',
    cc: 'Cc',
    bcc: 'Bcc',
  },
};

export default locale;
