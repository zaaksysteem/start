// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import PDFViewer from '@zaaksysteem/common/src/components/DocumentPreview/Viewer/PDFViewer';

export default () => {
  const url = window.location.href.split('?url=')[1];
  return (
    <PDFViewer
      pagination={true}
      url={url + '/pdf?inline=1'}
      downloadUrl={url}
    />
  );
};
