// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { useAppStyle } from './App.style';
import { views } from './Mor/Mor.constants';

const MorModule = React.lazy(
  () => import(/* webpackChunkName: "case" */ './Mor/Mor')
);

export interface RoutesPropsType {
  prefix: string;
}

const MorRoutes: React.ComponentType<RoutesPropsType> = ({ prefix }) => {
  const classes = useAppStyle();

  return (
    <div className={classes.app}>
      <ErrorBoundary>
        <Router>
          <Routes>
            <Route path={prefix}>
              <Route
                path=""
                element={<Navigate to={views[0]} replace={true} />}
              />
              <Route path=":view">
                <Route path="" element={<MorModule />} />
                <Route path=":caseNumber" element={<MorModule />} />
              </Route>
            </Route>
          </Routes>
        </Router>
      </ErrorBoundary>
    </div>
  );
};

export default MorRoutes;
