// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { CreatableSelect } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  IntegrationType,
  FilterType,
  SetFiltersType,
  ViewType,
} from '../../Mor.types';
import { views } from '../../Mor.constants';
import { useUrl } from '../../Mor.library';
import { logout } from '../../Mor.requests';
import { useHeaderStyles } from './Header.styles';

type HeaderPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  setFilters: SetFiltersType;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  integration,
  filters,
  setFilters,
}) => {
  const [t] = useTranslation('mor');
  const classes = useHeaderStyles();
  const { view, caseNumber } = useUrl();
  const navigate = useNavigate();
  const width = useWidth();
  const {
    // @ts-ignore
    palette: { common, elephant },
  } = useTheme();

  const [filtering, setFiltering] = useState<boolean>(false);
  const { title, headerImage, colorMain } = integration;

  const getNavigationButtonStyles = (navView: ViewType) => ({
    backgroundColor: 'unset',
    boxShadow: 'unset',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: 270,
    minWidth: 150,
    height: 50,
    marginLeft: width === 'xs' ? 0 : 4,
    flexGrow: width === 'xs' ? 1 : 0,
    borderBottom:
      navView === view ? `3px solid ${common.white}` : `3px solid ${colorMain}`,
    color: navView === view ? common.white : elephant.light,
    fontSize: 15,
    '&:hover': {
      backgroundColor: 'unset',
      boxShadow: 'unset',
    },
  });

  return (
    <div
      className={classes.wrapper}
      style={{
        backgroundColor: colorMain,
      }}
    >
      <div
        className={classes.image}
        style={{
          backgroundImage: `url(${headerImage})`,
          backgroundColor: colorMain,
        }}
      />
      <div className={classes.headerTop}>
        {filtering ? (
          <CreatableSelect
            // eslint-disable-next-line jsx-a11y/no-autofocus
            autoFocus={true}
            isMulti={true}
            value={filters}
            onChange={event => {
              const value = event?.target.value;

              if (value.length <= 3) {
                setFilters(value);
              }
            }}
            placeholder={t('filter.placeholder')}
          />
        ) : (
          <Typography
            variant="h3"
            sx={{
              color: common.white,
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              margin: 1,
            }}
          >
            {title}
          </Typography>
        )}
        <div className={classes.actions}>
          {filtering ? (
            <IconButton
              onClick={() => {
                setFilters([]);
                setFiltering(false);
              }}
              sx={{ color: common.white }}
            >
              <Icon size="small" color="inherit">
                {iconNames.close}
              </Icon>
            </IconButton>
          ) : (
            <IconButton
              onClick={() => {
                setFiltering(true);
              }}
              sx={{ color: common.white }}
            >
              <Icon size="small" color="inherit">
                {iconNames.search}
              </Icon>
            </IconButton>
          )}
          <IconButton onClick={logout} sx={{ color: common.white }}>
            <Icon size="small" color="inherit">
              {iconNames.power_settings_new}
            </Icon>
          </IconButton>
        </div>
      </div>
      <div className={classes.navigation}>
        {views.map(view => {
          const action = () => {
            navigate(caseNumber ? `../../${view}` : `../${view}`);
          };
          const sx = getNavigationButtonStyles(view);
          const name = `nav-${view}`;
          const text = t(`navigation.${view}`).toUpperCase();

          return (
            <Button key={view} name={name} action={action} sx={sx}>
              {text}
            </Button>
          );
        })}
      </div>
    </div>
  );
};

export default Header;
