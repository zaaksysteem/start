// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useFooterStyles = makeStyles(
  ({ palette: { elephant } }: Theme) => ({
    wrapper: {
      width: '100%',
      padding: 20,
      gap: 10,
      backgroundColor: elephant.lightest,
      '&>*': {
        color: elephant.dark,
        marginRight: 5,
      },
    },
  })
);
