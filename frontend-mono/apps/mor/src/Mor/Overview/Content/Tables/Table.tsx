// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { Box } from '@mui/material';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { openServerError } from '@zaaksysteem/common/src/signals';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import classNames from 'classnames';
import {
  CaseStatusType,
  FilterType,
  IntegrationType,
  TableDataType,
} from '../../../Mor.types';
import { getCases, useUrl } from '../../../Mor.library';
import { filterCases } from '../../Overview.library';
import { ZqlsType } from '../../Overview.types';
import { QUERY_STATUS } from '../../../Mor.constants';
import { useTableStyles } from './Table.styles';

type TablePropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  type: CaseStatusType;
  zqls: ZqlsType;
};

/* eslint complexity: [2, 12] */
const Table: React.ComponentType<TablePropsType> = ({
  integration,
  filters,
  type,
  zqls,
}) => {
  const [t] = useTranslation('mor');
  const navigate = useNavigate();
  const classes = useTableStyles();
  const { caseNumber } = useUrl();

  const { colorAccent } = integration;

  const zql = zqls[type];

  const { data, isLoading, isFetching } = useQuery<
    TableDataType,
    V2ServerErrorsType
  >([QUERY_STATUS[type]], () => getCases(integration, zql), {
    onError: openServerError,
  });

  const cases = data ? filterCases(data.cases, filters) : [];
  const count = data ? (data.count > 20 ? '20+' : data?.count) : 0;

  const title = t(`tables.${type}.title`);

  return (
    <div className={classes.wrapper}>
      <SubHeader title={`${title} (${count})`} />
      {isLoading && <Loader />}
      {!isLoading && Boolean(cases.length) && (
        <div
          style={{
            opacity: isFetching ? 0.5 : 1,
          }}
          className={classes.table}
        >
          {isFetching && (
            <div className={classes.loader}>
              <Loader />
            </div>
          )}
          {cases.map(({ number, subject, urgency, location }) => (
            <Box
              key={number}
              className={classes.row}
              onClick={() => {
                if (!caseNumber) {
                  navigate(number);
                }
              }}
            >
              <div className={classes.info}>
                <div
                  style={{
                    color: colorAccent,
                  }}
                  className={classes.subject}
                >{`${number} - ${subject}`}</div>
                {location && (
                  <div className={classes.location}>
                    <Icon size="small">{iconNames.place}</Icon>
                    <span>{location}</span>
                  </div>
                )}
              </div>
              <div
                className={classNames(
                  classes.urgency,
                  urgency >= 0 ? classes.onTime : classes.late
                )}
              >
                {urgency}
              </div>
            </Box>
          ))}
        </div>
      )}
      {!isLoading && !cases.length && (
        <div className={classes.placeholder}>
          {filters.length ? (
            <>
              <Icon size="huge">{iconNames.close}</Icon>
              <span>{t(`tables.filter.placeholder`)}</span>
            </>
          ) : (
            <>
              <Icon size="huge">{iconNames.done_all}</Icon>
              <span>{t(`tables.${type}.placeholder`)}</span>
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default React.memo(Table);
