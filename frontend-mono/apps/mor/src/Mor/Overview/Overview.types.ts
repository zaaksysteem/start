// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import {
  CaseStatusType,
  CaseType,
  FilterType,
  IntegrationType,
} from '../Mor.types';

export type ZqlsType = {
  [key in CaseStatusType]: string;
};

export type GetZqlsType = (
  integration: IntegrationType,
  session: SessionType
) => ZqlsType;

export type FilterCasesType = (
  cases: CaseType[],
  filters: FilterType[]
) => CaseType[];
