// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { formatCase } from '../Mor.library';
import {
  fetchCaseType,
  fetchCases,
  fetchDocuments,
  takeCase,
  transitionCase,
  updateCase,
} from '../Mor.requests';
import { AttributeTypeType } from '../Mor.types';
import {
  GetCaseType,
  FormatDocumentType,
  FormatSpecialAttributes,
  FormatAttributes,
  FormatCaseTypetype,
  PerformTakeCaseType,
  FieldType,
  ResolveCaseType,
  AttributeType,
} from './CaseView.types';

const formatDocument: FormatDocumentType = ({
  filestore_id: { uuid, original_name: name },
}) => ({
  uuid,
  name,
});

const formatCaseType: FormatCaseTypetype = ({
  result: {
    instance: { phases, results: rawResults },
  },
}) => {
  const fields = phases.reduce(
    (acc, { fields }: any) => [
      ...acc,
      ...fields.reduce(
        (
          acc: any,
          { is_group, magic_string, label, type, required, values }: any
        ) =>
          is_group
            ? acc
            : [
                ...acc,
                {
                  label,
                  magic_string,
                  type,
                  required,
                  choices: values
                    .filter(({ active }: any) => active)
                    .map(({ value }: any) => ({ label: value, value })),
                },
              ],
        []
      ),
    ],
    []
  );
  const results = rawResults.map(({ label, resultaat_id }) => ({
    label,
    id: resultaat_id,
  }));

  return { fields, results };
};

export const filterAttribute = ({ value }: any) =>
  Array.isArray(value) ? Boolean(value.length) : Boolean(value);

const formatSpecialAttributes: FormatSpecialAttributes = (
  t,
  { values, resultId, caseTypeSetting },
  { fields, results }
) => {
  const { specialAttrs } = caseTypeSetting;
  const attributes = specialAttrs.reduce((acc, { magic_string }) => {
    const field = fields.find(
      field => field.magic_string === magic_string
    ) as FieldType;
    const value = values[magic_string];

    if (!value) {
      return acc;
    } else {
      return [...acc, { ...field, value: value[0] }];
    }
  }, [] as AttributeType[]);
  const resultValue = resultId ? `${resultId}` : resultId;
  const choices = results.map(({ label, id }) => ({ label, value: `${id}` }));
  const activeChoice = choices.find(choice => choice.value === resultValue);
  const result = {
    label: t('case.result'),
    value: resultValue,
    resultLabel: activeChoice?.label,
    type: 'option' as AttributeTypeType,
    magic_string: 'case-result',
    required: true,
    choices,
  };

  return [...attributes, result];
};

const formatAddress = (value: any) => {
  if (!value) return null;

  const {
    address_data: {
      straat,
      huisnummer,
      huisletter,
      huisnummertoevoeging: suffix,
      postcode,
      woonplaats,
    },
  } = value;

  return `${straat} ${huisnummer}${huisletter ? huisletter : ''}${
    suffix ? '-' : ''
  }${suffix ? suffix : ''}, ${postcode} ${woonplaats}`;
};

const formatStreet = (value: any) => {
  if (!value) return null;

  const {
    address_data: { straat },
  } = value;

  return straat;
};

/* eslint complexity: [2, 11] */
const formatAttributes: FormatAttributes = ({ values }, { fields }) =>
  fields
    .map(field => {
      let value = values[field.magic_string];

      switch (field.type) {
        case 'bag_adres':
        case 'bag_straat_adres': {
          value = formatAddress(value[0]);
          break;
        }
        case 'bag_adressen':
        case 'bag_straat_adressen': {
          value = value.map(formatAddress).filter(Boolean);
          break;
        }
        case 'bag_openbareruimte': {
          value = formatStreet(value[0]);
          break;
        }
        case 'bag_openbareruimtes': {
          value = value.map(formatStreet).filter(Boolean);
          break;
        }
        case 'file': {
          value = null;
          break;
        }
        default: {
          const val = Array.isArray(value) ? value[0] : value;
          if (typeof val !== 'string' && !Array.isArray(val)) {
            value = null;
          } else {
            value = val;
          }
          break;
        }
      }

      return {
        ...field,
        value,
      };
    })
    .filter(filterAttribute);

export const getCase: GetCaseType = async (t, integration, caseNumber) => {
  const zql = `SELECT {} FROM case WHERE case.number = ${caseNumber}`;
  const caseResponse = await fetchCases(zql);
  const {
    result: {
      instance: { rows },
    },
  } = caseResponse;
  const caseRow = rows[0];
  const now = new Date().valueOf();
  const caseObj = formatCase(caseRow, now, integration);

  const {
    caseTypeSetting: { uuid },
    caseTypeVersion,
  } = caseObj;
  const caseTypeResponse = await fetchCaseType(uuid, caseTypeVersion);
  const caseType = formatCaseType(caseTypeResponse);
  const specialAttributes = formatSpecialAttributes(t, caseObj, caseType);
  const attributes = formatAttributes(caseObj, caseType);

  const documentsResponse = await fetchDocuments(caseNumber);
  const documents = documentsResponse.result[0].files.map(formatDocument);

  return {
    ...caseObj,
    specialAttributes,
    attributes,
    documents,
  };
};

export const performTakeCase: PerformTakeCaseType = async uuid => {
  await takeCase(uuid);
};

export const resolveCase: ResolveCaseType = async (uuid, fields) => {
  const attributes = fields.slice(0, -1);
  const resultField = fields.slice(-1)[0];

  const values = attributes.reduce(
    (acc: any, attr: any) =>
      attr.value ? { ...acc, [attr.name]: [attr.value] } : acc,
    {}
  );

  await updateCase(uuid, { values });

  await transitionCase(uuid, { result_id: resultField.value });
};
