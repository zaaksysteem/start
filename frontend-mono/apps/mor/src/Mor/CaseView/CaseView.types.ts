// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  IntegrationType,
  CaseType,
  CaseTypeResponseBodyType,
  AttributeTypeType,
  ValueType,
} from '../Mor.types';

export type AttributeType = {
  label: string;
  value: ValueType;
  resultLabel?: string;
  type: AttributeTypeType;
  magic_string: string;
  required: boolean;
  choices?: { label: string; value: string }[];
};

export type FieldType = {
  label: string;
  type: AttributeTypeType;
  required: boolean;
  magic_string: string;
};

type ResultType = {
  label: string;
  id: number;
};

type CaseTypeType = {
  fields: FieldType[];
  results: ResultType[];
};

type DocumentType = {
  uuid: string;
  name: string;
};

export type CaseObjType = {
  specialAttributes: AttributeType[];
  attributes: AttributeType[];
  documents: DocumentType[];
} & CaseType;

export type GetCaseType = (
  t: i18next.TFunction,
  integration: IntegrationType,
  caseNumber: string
) => Promise<CaseObjType>;

export type FormatDocumentType = (file: any) => DocumentType;

export type FormatSpecialAttributes = (
  t: i18next.TFunction,
  caseObj: CaseType,
  caseType: CaseTypeType
) => AttributeType[];

export type FormatAttributes = (
  caseObj: CaseType,
  caseType: CaseTypeType
) => AttributeType[];

export type FormatCaseTypetype = (
  caseTypeResponse: CaseTypeResponseBodyType
) => CaseTypeType;

export type PerformTakeCaseType = (caseNumber: string) => Promise<void>;

export type ResolveCaseType = (uuid: string, fields: any) => Promise<void>;
