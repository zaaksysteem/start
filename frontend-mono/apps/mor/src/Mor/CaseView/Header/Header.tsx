// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { IntegrationType } from '../../Mor.types';
import { useUrl } from '../../Mor.library';
import { CaseObjType } from '../CaseView.types';
import { useHeaderStyles } from './Header.styles';

type HeaderPropsType = {
  integration: IntegrationType;
  caseObj?: CaseObjType | null;
};

const Header: React.ComponentType<HeaderPropsType> = ({
  integration,
  caseObj,
}) => {
  const classes = useHeaderStyles();
  const { caseNumber } = useUrl();
  const navigate = useNavigate();
  const {
    // @ts-ignore
    palette: { common },
  } = useTheme();

  const title = caseObj?.subject
    ? `${caseNumber}: ${caseObj.subject}`
    : caseNumber;

  return (
    <div
      style={{ backgroundColor: integration.colorMain }}
      className={classes.wrapper}
    >
      <IconButton
        onClick={() => {
          navigate('..');
        }}
        sx={{ color: common.white }}
      >
        <Icon size="small">{iconNames.arrow_back}</Icon>
      </IconButton>
      <Typography
        variant="h3"
        sx={{
          color: common.white,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
        }}
      >
        {title}
      </Typography>
    </div>
  );
};

export default Header;
