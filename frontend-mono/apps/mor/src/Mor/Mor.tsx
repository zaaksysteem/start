// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { getIntegration } from './Mor.library';
import { IntegrationType } from './Mor.types';
import { useMorStyles } from './Mor.styles';
import CaseView from './CaseView/CaseView';
import OverView from './Overview/Overview';

const Mor: React.ComponentType = () => {
  const [t] = useTranslation('mor');
  const classes = useMorStyles();

  const { data: integration, error } = useQuery<
    IntegrationType,
    V2ServerErrorsType
  >(['integration'], () => getIntegration());

  document.title = integration?.title || t('title');

  return (
    <Sheet classes={{ sheet: classes.sheet }}>
      {!integration ? (
        <Loader />
      ) : (
        <>
          <OverView integration={integration} />
          <CaseView integration={integration} />
        </>
      )}
      {error && <ServerErrorDialog />}
    </Sheet>
  );
};

export default Mor;
