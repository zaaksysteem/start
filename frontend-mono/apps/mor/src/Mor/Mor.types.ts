// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type ViewType = 'open' | 'resolved';
export type CaseStatusType = 'new' | 'open' | 'resolved';

export type ParamsType = {
  view: ViewType;
  caseNumber: string;
};

export type UseUrlType = () => ParamsType;

export type FilterType = {
  label: string;
  value: string;
};

export type SetFiltersType = (filters: FilterType[]) => void;

// Integration

type AttributeSettingType = {
  magic_string: string;
  type: string;
};

export type CaseTypeSettingsType = {
  uuid: string;
  id: number;
  subjectAttr: AttributeSettingType;
  specialAttrs: AttributeSettingType[];
};

export type IntegrationType = {
  title: string;
  caseTypeSettings: CaseTypeSettingsType[];
  headerImage: string;
  colorMain: string;
  colorAccent: string;
  helpUrl: string | null;
};

export type AttributeTypeType =
  | 'text'
  | 'textarea'
  | 'option'
  | 'checkbox'
  | 'bag_adres'
  | 'bag_adressen'
  | 'bag_straat_adres'
  | 'bag_straat_adressen'
  | 'bag_openbareruimte'
  | 'bag_openbareruimtes'
  | 'file';

type RawAttributeType = {
  object: {
    column_name: string;
    value_type: AttributeTypeType;
  };
};

export type IntegrationResponseBody = {
  result: {
    instance: {
      interface_config: {
        title: string;
        casetypes: {
          casetype: {
            id: string;
            object_id: number;
          };
          casetype_case_attributes: RawAttributeType[];
          casetype_subject_attribute: RawAttributeType;
        }[];
        header_bgcolor: string;
        accent_color: string;
        header_bgimage: string;
        app_help_uri: string;
      };
    };
  };
};

export type fetchIntegrationType = () => Promise<IntegrationResponseBody>;

export type getIntegrationType = () => Promise<IntegrationType>;

export type FormatIntegrationType = (
  response: IntegrationResponseBody
) => IntegrationType;

export type FormatAttributeType = (
  attribute: RawAttributeType
) => AttributeSettingType;

export type FormatCaseTypeSettingType = (
  casetype: IntegrationResponseBody['result']['instance']['interface_config']['casetypes'][0]
) => CaseTypeSettingsType;

// Cases

export type ValueType = any;

export type CaseType = {
  // overview
  number: string;
  subject: string | null;
  location: string | null;
  urgency: number;
  filterText: string;
  // caseview
  uuid: string;
  status: CaseStatusType;
  caseTypeSetting: CaseTypeSettingsType;
  caseTypeVersion: string;
  values: { [key: string]: ValueType };
  resultId: number;
};

export type TableDataType = {
  cases: CaseType[];
  count: number;
};

export type CasesResponseBodyType = {
  result: {
    instance: {
      pager: { total_rows: number };
      rows: any;
    };
  };
};

export type FetchCasesType = (zql: string) => Promise<CasesResponseBodyType>;

export type GetCasesType = (
  integration: IntegrationType,
  zql: string
) => Promise<TableDataType>;

export type FormatCaseType = (
  row: any,
  now: number,
  integration: IntegrationType
) => CaseType;

export type CaseTypeResponseBodyType = {
  result: {
    instance: {
      phases: any[];
      results: { label: string; resultaat_id: number }[];
    };
  };
};

export type FetchCaseTypeType = (
  uuid: string,
  version: string
) => Promise<CaseTypeResponseBodyType>;

export type DocumentsResponseBodyType = {
  result: {
    files: any;
  }[];
};

export type FetchDocumentsType = (
  number: string
) => Promise<DocumentsResponseBodyType>;

export type TakeCaseType = (uuid: string) => Promise<void>;

export type UpdateCaseType = (
  uuid: string,
  data: {
    values: {
      [key: string]: string[];
    };
  }
) => Promise<void>;

export type TransitionCaseType = (
  uuid: string,
  data: {
    result_id: number;
  }
) => Promise<void>;
