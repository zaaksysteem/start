// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const mor = {
  nl: {
    mor: {
      title: 'MOR',
      navigation: {
        open: 'Open',
        resolved: 'Afgehandeld',
      },
      filter: {
        placeholder: 'Filter de meldingen… (3 maximum)',
      },
      dialog: {
        error: {
          title: 'Fout',
          details: 'Details',
          body: 'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
        },
        close: 'Sluiten',
      },
      tables: {
        open: {
          title: 'In behandeling',
          placeholder:
            'Je hebt geen zaken in behandeling. Open een zaak uit de werkvoorraad om te beginnen.',
        },
        new: {
          title: 'Werkvoorraad',
          placeholder: 'Er zijn geen zaken in de werkvoorraad.',
        },
        resolved: {
          title: 'Afgehandeld',
          placeholder: 'Er zijn geen afgehandelde zaken.',
        },
        filter: {
          placeholder:
            'Er zijn geen zaken die voldoen aan de ingestelde filters.',
        },
      },
      footer: {
        description: 'Probleem met de werking van de app?',
        helpUrl: 'Meld het hier',
        or: 'of',
      },
      case: {
        done: 'Melding is afgehandeld.',
        result: 'Resultaat',
        button: {
          take: 'In behandeling nemen',
          resolve: 'Afhandelen',
          cancel: 'Annuleren',
        },
        attachments: 'Bijlagen',
      },
    },
  },
};

export default mor;
