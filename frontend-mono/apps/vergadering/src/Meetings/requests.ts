// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl, capitalize } from '@mintlab/kitchen-sink/source';
import {
  FormatIntegrationType,
  FormatMeetingType,
  FormatAttributesType,
  FormatProposalType,
  SortMeetingsType,
  GetContentType,
  GetIntegrationsType,
  GetMagicStringType,
  IntegrationsResponseBodyType,
  CaseResponseBodyType,
  GetProposalsType,
  ProposalType,
  MapMappingType,
  DocumentResponseBodyType,
  GetDocumentsType,
  FormatDocumentType,
  FormatNoteType,
  GetNotesType,
  NoteResponseBodyType,
  GetCaseObjType,
  CreateNoteType,
  UpdateNoteType,
  DeleteNoteType,
  CaseTypeResponseBodyType,
  GetCaseTypeType,
  FormatCaseTypeType,
  FieldType,
  FormatVotingAttributesType,
  VotingAttributesType,
  PerformSaveType,
  CommentAttributeType,
  GetDateTimeType,
  GetTimeType,
} from './types';

const getMagicStringType: GetMagicStringType = attr =>
  attr?.object.column_name.replace('attribute.', '');

const isDocumentRegex = /^voorstelbijlage\d*$/;

const mapMapping: MapMappingType = ({
  checked,
  internal_name,
  external_name,
}) => ({
  checked: Boolean(checked),
  magicString: internal_name?.searchable_object_id,
  label: internal_name?.searchable_object_label_public,
  isSubject: external_name === 'voorstelonderwerp',
  isDocument: Boolean(external_name.match(isDocumentRegex)),
});

const formatIntegration: FormatIntegrationType = ({
  reference: uuid,
  instance: {
    interface_config: {
      access,
      defaultview,

      accent_color: colorAccent,
      header_bgcolor: colorHeader,
      header_loader_color: colorLoader,
      header_bgimage: image,
      links,

      title_normal: title,
      title_small: titleSmall,
      short_name: shortName,

      casetype_meeting,
      meeting_publication_filter_attribute,
      meeting_publication_filter_attribute_value,

      casetype_voorstel_item,
      proposal_publication_filter_attribute,
      proposal_publication_filter_attribute_value,
      magic_strings_accept,
      magic_strings_comment,

      attribute_mapping,
    },
  },
}) => {
  const mapping = attribute_mapping?.map(mapMapping) || [];

  const meetingMapping = mapping.slice(0, 5);
  const restMapping = mapping.slice(5).filter(map => map.magicString);

  const proposalMapping = restMapping
    .filter(map => !map.isDocument)
    .map(map => ({
      ...map,
      label: map.label || capitalize(map.magicString.replace('_', ' ')),
    }));

  const documentMapping = restMapping.filter(map => map.isDocument);

  return {
    uuid,
    canEdit: access === 'rw',
    defaultGrouping: defaultview === 'per_meeting' ? 'grouped' : 'ungrouped',

    colorAccent,
    colorHeader,
    colorLoader,
    image,
    links: links || [],

    title,
    titleSmall,
    shortName,

    meetingCaseTypeId: casetype_meeting[0],
    meetingFilterAttribute:
      meeting_publication_filter_attribute?.object.column_name,
    meetingFilterKeyword: meeting_publication_filter_attribute_value,

    proposalCaseTypeIds: casetype_voorstel_item,
    proposalFilterAttribute:
      proposal_publication_filter_attribute?.object.column_name,
    proposalFilterKeyword: proposal_publication_filter_attribute_value,

    proposalVoteAttributes: (magic_strings_accept || []).map(
      getMagicStringType
    ),
    proposalCommentAttributes: (magic_strings_comment || []).map(
      getMagicStringType
    ),

    meetingMapping,
    proposalMapping,
    documentMapping,
  };
};

export const getIntegrations: GetIntegrationsType = async () => {
  const url = '/api/v1/app/app_meeting';
  const response = await request<IntegrationsResponseBodyType>('GET', url);
  const integrations = response.result.instance.rows.map(formatIntegration);

  return integrations;
};

const formatMeeting: FormatMeetingType = (integration, meeting) => {
  const { meetingMapping } = integration;
  const [subject, date, time, location, chairperson] = meetingMapping;
  const {
    reference: uuid,
    instance: {
      number,
      attributes,
      case_relationships: {
        plain: {
          instance: { rows },
        },
      },
    },
  } = meeting;

  return {
    uuid,
    id: number,
    subject: attributes[subject.magicString]?.[0],
    date: attributes[date.magicString]?.[0],
    time: attributes[time.magicString]?.[0],
    location: attributes[location.magicString]?.[0],
    chairperson: attributes[chairperson.magicString]?.[0],
    relationUuids: rows.map(row => row.reference),
  };
};

const formatAttributes: FormatAttributesType = (
  rawProposal,
  proposalMapping,
  caseType
) => {
  const {
    instance: {
      number,
      casetype: { preview },
      requestor,
      confidentiality,
      result,
      attributes: rawAttributes,
    },
  } = rawProposal;

  const attributes = proposalMapping.map(
    ({ checked, magicString, label, isSubject }) => {
      let value: string | null = rawAttributes[magicString]?.[0];

      if (magicString === 'zaaknummer') {
        value = `${number}`;
      } else if (magicString === 'zaaktype') {
        value = preview;
      } else if (magicString === 'aanvrager_naam') {
        value = requestor.preview;
      } else if (magicString === 'vertrouwelijkheid') {
        value = confidentiality.mapped;
      } else if (magicString === 'resultaat' && result) {
        value = capitalize(result);
      }

      const field = caseType?.fields.find(
        field => field.magicString === magicString
      );
      const caseTypeLabel = field?.label;
      const type = field?.type;

      return {
        checked,
        label: caseTypeLabel || label,
        magicString,
        value,
        isSubject,
        type,
      };
    }
  );

  return attributes;
};

const formatVotingAttributes: FormatVotingAttributesType = (
  rawProposal,
  voteAttributes,
  commentAttributes,
  caseType
) => {
  const {
    instance: { attributes: rawAttributes },
  } = rawProposal;

  const commentAttrs = commentAttributes.reduce((acc, magicString) => {
    let field = caseType?.fields.find(
      field => field.magicString === magicString
    );

    if (!field) return acc;

    const value: string | null = rawAttributes[magicString]?.[0];
    const label = field.label;
    const type = field.type;
    const permissions = field.permissions;

    return [...acc, { label, type, value, magicString, permissions }];
  }, [] as CommentAttributeType[]);

  const votingAttributes = voteAttributes.reduce((acc, magicString, index) => {
    let field = caseType?.fields.find(
      field => field.magicString === magicString
    ) as FieldType;

    if (!field) return acc;

    const value: string | null = rawAttributes[magicString]?.[0];
    const label = field.label;
    const type = field.type;
    const permissions = field.permissions;
    const choices = field.choices;

    const comment = commentAttrs[index];

    if (!comment) return acc;

    const vote = { label, type, value, magicString, permissions, choices };

    return [...acc, { vote, comment }];
  }, [] as VotingAttributesType[]);

  return votingAttributes;
};

const formatProposal: FormatProposalType = (
  integration,
  rawProposal,
  caseType
) => {
  const {
    proposalMapping,
    documentMapping,
    proposalVoteAttributes,
    proposalCommentAttributes,
  } = integration;
  const {
    reference: uuid,
    instance: {
      number,
      attributes: rawAttributes,

      status,
    },
  } = rawProposal;

  const attributes = formatAttributes(rawProposal, proposalMapping, caseType);
  const votingAttributesList = caseType
    ? formatVotingAttributes(
        rawProposal,
        proposalVoteAttributes,
        proposalCommentAttributes,
        caseType
      )
    : [];

  const documentUuids = documentMapping.reduce(
    (acc, map) => [...acc, ...(rawAttributes[map.magicString] || [])],
    [] as string[]
  );
  const uniqueDocumentUuids = [...new Set(documentUuids)];

  const filterText = attributes
    .map(attr => attr.value)
    .join(' ')
    .toLowerCase();

  return {
    uuid,
    id: number,
    attributes,
    votingAttributesList,
    documentUuids: uniqueDocumentUuids,
    filterText,
    counter: 0,

    status,
  };
};

const timeRegex = /(\d{1,2})(:|\.)?(\d{2})/;

const getTime: GetTimeType = time => {
  if (!time) return 0;

  const match = time.match(timeRegex);
  const hours = match ? Number(match[1]) + Number(match[3]) / 60 : 0;

  return hours * 60 * 60 * 1000;
};

const getDateTime: GetDateTimeType = meeting => {
  const date = meeting.date ? new Date(meeting.date).valueOf() : 0;
  const time = getTime(meeting.time);

  return date + time;
};

const sortMeetings: SortMeetingsType = (integration, view, meetings) => {
  const [, dateAttribute] = integration.meetingMapping;

  if (!dateAttribute) return meetings;

  const sortedMeetings = meetings.sort((meeting1, meeting2) => {
    const dateTime1 = getDateTime(meeting1);
    const dateTime2 = getDateTime(meeting2);

    return dateTime1 > dateTime2 ? 1 : -1;
  });

  return view === 'open' ? sortedMeetings : sortedMeetings.reverse();
};

/* eslint complexity: [2, 10] */
export const getContent: GetContentType = async (
  integration,
  view,
  grouping
) => {
  const {
    meetingCaseTypeId,
    meetingFilterAttribute,
    meetingFilterKeyword,
    proposalCaseTypeIds,
    proposalFilterAttribute,
    proposalFilterKeyword,
  } = integration;

  const open = view === 'open';
  const status = open ? '"open","new"' : '"resolved"';
  const grouped = grouping === 'grouped';
  const order = !open && grouped ? 'ORDER BY case.date_of_completion DESC' : '';

  const caseTypeIds = grouped
    ? meetingCaseTypeId
    : proposalCaseTypeIds.join(',');
  const filterAttribute = grouped
    ? meetingFilterAttribute
    : proposalFilterAttribute;
  const filterKeyword = grouped ? meetingFilterKeyword : proposalFilterKeyword;

  const filters = [
    `(case.casetype.id IN (${caseTypeIds}))`,
    filterAttribute && filterKeyword
      ? `(${filterAttribute} = "${filterKeyword}")`
      : '',
    `(case.status IN (${status}) )`,
  ]
    .filter(Boolean)
    .join(' AND ');

  const zql = `SELECT {} FROM case WHERE ${filters} ${order}`;

  const params = { page: 1, rows_per_page: 100, zql };
  const url = buildUrl('/api/v1/case', params);
  const response = await request<CaseResponseBodyType>('GET', url);
  const rows = response.result.instance.rows;

  if (grouped) {
    const meetings = rows.map(row => formatMeeting(integration, row));

    return sortMeetings(integration, view, meetings);
  } else {
    const proposals = rows.map(row => formatProposal(integration, row));

    return proposals;
  }
};

export const getProposals: GetProposalsType = async (integration, meeting) => {
  const {
    proposalCaseTypeIds,
    proposalFilterAttribute,
    proposalFilterKeyword,
  } = integration;

  const filters = [
    `(case.casetype.id IN (${proposalCaseTypeIds}))`,
    proposalFilterAttribute && proposalFilterKeyword
      ? `(${proposalFilterAttribute} = "${proposalFilterKeyword}")`
      : '',
  ]
    .filter(Boolean)
    .join(' AND ');

  const zql = `SELECT {} FROM case WHERE ${filters}`;

  const params = { page: 1, rows_per_page: 100, zql };
  const url = buildUrl(`/api/v1/case/${meeting.uuid}/relation`, params);
  const response = await request<CaseResponseBodyType>('GET', url);
  const rows = response.result.instance.rows;

  const proposals = rows.map(row => formatProposal(integration, row));
  const orderedProposals = meeting.relationUuids
    .map(uuid => proposals.find(proposal => proposal.uuid === uuid))
    .filter(Boolean)
    .map((proposal, index) => ({
      ...proposal,
      counter: index + 1,
    })) as ProposalType[];

  return orderedProposals;
};

const formatDocument: FormatDocumentType = ({
  instance: { id, name, number },
}) => ({
  uuid: id,
  label: name,
  number,
});

const getDocuments: GetDocumentsType = async ({ uuid, documentUuids }) => {
  const params = { page: 1, rows_per_page: 100 };
  const url = buildUrl(`/api/v1/case/${uuid}/document`, params);
  const response = await request<DocumentResponseBodyType>('GET', url);
  const rows = response.result.instance.rows;
  const documents = rows
    .map(formatDocument)
    .filter(doc => documentUuids.includes(doc.uuid));

  return documents;
};

const formatNote: FormatNoteType = ({ reference, instance: { content } }) => ({
  uuid: reference,
  content,
});

export const getNotes: GetNotesType = async ({ uuid }) => {
  const params = { page: 1, rows_per_page: 100 };
  const url = buildUrl(`/api/v1/case/${uuid}/note`, params);
  const response = await request<NoteResponseBodyType>('GET', url);
  const rows = response.result.instance.rows;
  const notes = rows.map(formatNote);

  return notes;
};

const formatCaseType: FormatCaseTypeType = ({ phases }) => {
  const fields = phases.reduce((acc, { fields }) => {
    const formattedFields = fields
      .filter(({ type, is_group }) => !is_group && type !== 'text_block')
      .map(({ type, label, magic_string, permissions, values }) => ({
        type,
        label,
        magicString: magic_string,
        permissions: (permissions || []).map(
          ({
            group: {
              instance: { id: group },
            },
            role: {
              instance: { id: role },
            },
          }) => ({ group, role })
        ),
        choices: values
          .filter(val => Boolean(val.active))
          .sort((valA, valB) => (valA.sort_order > valB.sort_order ? 1 : -1))
          .map(({ value }) => ({ label: value, value })),
      }));

    return [...acc, ...formattedFields];
  }, [] as FieldType[]);

  return { fields };
};

const getCaseType: GetCaseTypeType = async rawCaseObj => {
  const {
    instance: {
      casetype: {
        reference: uuid,
        instance: { version },
      },
    },
  } = rawCaseObj;
  const params = { version };
  const url = buildUrl(`/api/v1/casetype/${uuid}`, params);
  const response = await request<CaseTypeResponseBodyType>('GET', url);
  const rawCaseType = response.result.instance;
  const caseType = formatCaseType(rawCaseType);

  return caseType;
};

export const getCaseObj: GetCaseObjType = async (integration, caseNumber) => {
  const filters = [`(case.number = (${caseNumber}))`];
  const zql = `SELECT {} FROM case WHERE ${filters}`;

  const params = { zql, page: 1, rows_per_page: 20 };
  const url = buildUrl('/api/v1/case', params);
  const response = await request<CaseResponseBodyType>('GET', url);
  const rawCaseObj = response.result.instance.rows[0];

  const caseType = await getCaseType(rawCaseObj);

  const proposal = formatProposal(integration, rawCaseObj, caseType);

  const documents = await getDocuments(proposal);
  const notes = await getNotes(proposal);

  return { ...proposal, documents, notes };
};

export const createNote: CreateNoteType = async (caseUuid, content) => {
  const body = { content };
  const url = `/api/v1/case/${caseUuid}/note/create`;

  await request('POST', url, body);
};

export const editNote: UpdateNoteType = async (caseUuid, note) => {
  const body = { content: note.content };
  const url = `/api/v1/case/${caseUuid}/note/${note.uuid}/update`;

  await request('POST', url, body);
};

export const deleteNote: DeleteNoteType = async (caseUuid, noteUuid) => {
  const url = `/api/v1/case/${caseUuid}/note/${noteUuid}/delete`;

  await request('POST', url);
};

export const performSave: PerformSaveType = async (
  caseObj,
  votingAttributes,
  decision,
  comment
) => {
  const zql = `SELECT+%7B%7D+FROM+case+WHERE+case.number+%3D+${caseObj.id}`;
  const params = { zql };
  const url = buildUrl(
    `/api/v1/app/app_meeting/case/${caseObj.uuid}/update_attributes`,
    params
  );

  const body = {
    values: {
      [votingAttributes.vote.magicString]: [decision],
      [votingAttributes.comment.magicString]: [comment],
    },
  };

  await request('POST', url, body);
};
