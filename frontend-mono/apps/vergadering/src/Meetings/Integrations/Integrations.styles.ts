// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { headerHeight } from '../Overview/Header/Header.styles';

export const useIntegrationsStyles = makeStyles(
  ({ palette: { common, primary } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      minHeight: headerHeight,
    },
    header: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: headerHeight,
      padding: 20,
      backgroundColor: primary.main,
      '&>H3': { color: common.white },
    },
    integrations: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      padding: 20,
      gap: 20,
    },
  })
);
