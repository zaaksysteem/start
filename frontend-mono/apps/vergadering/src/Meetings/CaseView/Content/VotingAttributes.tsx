// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { IconButton } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, PermissionType, VotingAttributesType } from '../../types';
import Attribute from './Attribute';
import { useContentStyles } from './Content.styles';
import VotingDialog from './VotingDialog';

type VotingAttributesPropsType = {
  caseObj: CaseObjType;
  votingAttributes: VotingAttributesType;
  index: number;
};

const VotingAttributes: React.ComponentType<VotingAttributesPropsType> = ({
  caseObj,
  votingAttributes,
  index,
}) => {
  const classes = useContentStyles();
  const session = useSession();
  const {
    logged_in_user: {
      legacy: { ou_id, role_ids },
    },
  } = session;
  const { vote, comment } = votingAttributes;

  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const toggleDialog = () => setDialogOpen(!dialogOpen);

  const isOpen = caseObj.status !== 'resolved';

  const permissionCheck = ({ group, role }: PermissionType) =>
    group === ou_id && role_ids.includes(role);
  const allowedToEdit =
    vote.permissions.some(permissionCheck) &&
    comment.permissions.some(permissionCheck);

  return (
    <div
      className={allowedToEdit ? classes.votingAttributesWrapper : undefined}
    >
      <div className={classes.votingAttributes}>
        <Attribute key={vote.magicString} attr={vote} />
        <Attribute key={comment.magicString} attr={comment} />
      </div>
      {allowedToEdit && isOpen && (
        <IconButton onClick={toggleDialog}>
          <Icon>{iconNames.edit}</Icon>
        </IconButton>
      )}
      {dialogOpen && (
        <VotingDialog
          caseObj={caseObj}
          votingAttributes={votingAttributes}
          index={index}
          close={toggleDialog}
        />
      )}
    </div>
  );
};

export default VotingAttributes;
