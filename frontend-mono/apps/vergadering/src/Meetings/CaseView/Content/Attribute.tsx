// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import Wysiwyg from '@mintlab/ui/App/Zaaksysteem/Wysiwyg';
import { asArray } from '@mintlab/kitchen-sink/source';
import { AttributeType } from '../../types';
import { dateFormatText } from '../../library';
import { useContentStyles } from './Content.styles';

type AttributePropsType = {
  attr: AttributeType;
};

/* eslint complexity: [2, 8] */
const getAttributeValue = (attr: AttributeType) => {
  const { type, value, magicString } = attr;
  if (!value) return <span>-</span>;

  switch (type) {
    case 'richtext': {
      // @ts-ignore
      return <Wysiwyg value={value} readOnly={true} />;
    }
    case 'date': {
      return <span>{fecha.format(new Date(value), dateFormatText)}</span>;
    }
    case 'select':
    case 'checkbox': {
      return Array.isArray(value) && value?.length === 1 ? (
        <span>{value[0]}</span>
      ) : (
        <>
          {asArray(value).map((val, index) => (
            <li key={`${magicString}-value-${index}`}>{val}</li>
          ))}
        </>
      );
    }

    default: {
      return (
        <>
          {(value as string).split('\n').map((val, index) => (
            <span key={`${magicString}-value-${index}`}>{val}</span>
          ))}
        </>
      );
    }
  }
};

const Attribute: React.ComponentType<AttributePropsType> = ({ attr }) => {
  const classes = useContentStyles();

  return (
    <div className={classes.attribute}>
      <div className={classes.attributeLabel}>{attr.label}</div>
      {getAttributeValue(attr)}
    </div>
  );
};

export default Attribute;
