// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTheme } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { IconButton } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import { CaseObjType, DocumentType } from '../../types';
import { useContentStyles } from './Content.styles';

type DocumentPropsType = {
  caseObj: CaseObjType;
  document: DocumentType;
};

const Document: React.ComponentType<DocumentPropsType> = ({
  caseObj,
  document: { uuid, label, number },
}) => {
  const classes = useContentStyles();
  const {
    palette: { common },
  } = useTheme<Theme>();
  const sx = {
    flexGrow: 1,
    backgroundColor: 'unset',
    color: common.black,
    boxShadow: 'unset',
    padding: '15px 5px 15px 20px',
    '&:hover': {
      backgroundColor: 'unset',
      boxShadow: 'unset',
    },
  };

  const [previewOpen, setPreviewOpen] = useState<boolean>(false);
  const togglePreview = () => setPreviewOpen(!previewOpen);

  const downloadUrl = `/api/v1/case/${caseObj.uuid}/document/${uuid}/download`;
  const previewUrl = `/zaak/${caseObj.id}/document/${number}/download/pdf?inline=1`;

  return (
    <div className={classes.document}>
      <Button sx={sx} name="document" onClick={togglePreview}>
        <div className={classes.documentLabel}>{label}</div>
      </Button>

      <IconButton
        onClick={() => {
          window.open(downloadUrl, '_blank');
        }}
      >
        <Icon size="small">{iconNames.download}</Icon>
      </IconButton>
      {previewUrl && (
        <DocumentPreviewModal
          open={previewOpen}
          title={label}
          url={previewUrl}
          contentType="application/pdf"
          downloadUrl={downloadUrl}
          onClose={togglePreview}
        />
      )}
    </div>
  );
};

export default Document;
