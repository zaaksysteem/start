// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Table from '@mui/material/Table/Table';
import TableRow from '@mui/material/TableRow/TableRow';
import TableCell from '@mui/material/TableCell/TableCell';
import TableHead from '@mui/material/TableHead/TableHead';
import TableBody from '@mui/material/TableBody/TableBody';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { FilterType, IntegrationType, ProposalType } from '../../../types';
import { filterProposals, formatValue } from '../../Overview.library';
import { useUrl } from '../../../library';
import { useProposalListStyles } from './ProposalList.styles';
import HeaderCell from './HeaderCell';

type ProposalListPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  proposals: ProposalType[];
};

const ProposalList: React.ComponentType<ProposalListPropsType> = ({
  integration,
  filters,
  proposals,
}) => {
  const [t] = useTranslation('meetings');
  const classes = useProposalListStyles();
  const navigate = useNavigate();
  const { caseNumber } = useUrl();

  const hasCounterColumn = Boolean(proposals[0]?.counter);

  const [[sortedColumn, ascending], setSorting] = useState<[string, boolean]>([
    hasCounterColumn ? 'counter' : 'zaaknummer',
    true,
  ]);

  const toggleSort = (magicString: string) =>
    magicString === sortedColumn
      ? setSorting([magicString, !ascending])
      : setSorting([magicString, true]);

  const columns = integration.proposalMapping.filter(map => map.checked);

  const filteredProposals = filterProposals(proposals, filters);

  /* eslint complexity: [2, 11] */
  const sortedProposals = filteredProposals.sort((prop1, prop2) => {
    const direction = ascending ? 1 : -1;

    if (sortedColumn === 'counter') {
      return (prop1.counter > prop2.counter ? 1 : -1) * direction;
    } else if (sortedColumn === 'documents') {
      return (
        (prop1.documentUuids.length > prop2.documentUuids.length ? 1 : -1) *
        direction
      );
    } else if (sortedColumn === 'zaaknummer') {
      return (prop1.id - prop2.id) * direction;
    } else {
      const first =
        prop1.attributes.find(attr => attr.magicString === sortedColumn)
          ?.value || '';
      const second =
        prop2.attributes.find(attr => attr.magicString === sortedColumn)
          ?.value || '';

      return (first > second ? 1 : -1) * direction;
    }
  });

  return sortedProposals.length ? (
    <Table>
      <TableHead>
        <TableRow>
          {hasCounterColumn && (
            <HeaderCell
              toggleSort={toggleSort}
              magicString={'counter'}
              label={'#'}
              sortedColumn={sortedColumn}
              ascending={ascending}
            />
          )}
          {columns.map(({ label, magicString }) => (
            <HeaderCell
              key={magicString}
              toggleSort={toggleSort}
              magicString={magicString}
              label={label}
              sortedColumn={sortedColumn}
              ascending={ascending}
            />
          ))}
          <HeaderCell
            toggleSort={toggleSort}
            magicString={'documents'}
            label={<Icon size="extraSmall">{iconNames.insert_drive_file}</Icon>}
            sortedColumn={sortedColumn}
            ascending={ascending}
          />
        </TableRow>
      </TableHead>
      <TableBody>
        {sortedProposals.map(
          ({ uuid, id, counter, attributes, documentUuids }) => {
            const cells = attributes.filter(map => map.checked);

            return (
              <TableRow
                key={uuid}
                className={classes.row}
                onClick={() => {
                  if (!caseNumber) {
                    navigate(`${id}`);
                  }
                }}
              >
                {hasCounterColumn && <TableCell>{counter}</TableCell>}
                {cells.map(attr => {
                  return (
                    <TableCell key={`${uuid}-${attr.magicString}`}>
                      {formatValue(attr.value)}
                    </TableCell>
                  );
                })}
                <TableCell>{documentUuids.length}</TableCell>
              </TableRow>
            );
          }
        )}
      </TableBody>
    </Table>
  ) : (
    <div className={classes.placeholder}>{t('proposals.placeholder')}</div>
  );
};

export default ProposalList;
