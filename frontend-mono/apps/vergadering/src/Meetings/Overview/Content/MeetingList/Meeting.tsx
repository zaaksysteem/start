// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import fecha from 'fecha';
import { Box } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTranslation } from 'react-i18next';
import {
  FilterType,
  IntegrationType,
  MeetingType,
  ProposalType,
} from '../../../types';
import { QUERY_PROPOSALS } from '../../../constants';
import { getProposals } from '../../../requests';
import ProposalList from '../ProposalList/ProposalList';
import { dateFormatText } from '../../../library';
import { useMeetingListStyles } from './MeetingList.styles';

type MeetingPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  meeting: MeetingType;
};

/* eslint complexity: [2, 8] */
const Meeting: React.ComponentType<MeetingPropsType> = ({
  integration,
  filters,
  meeting,
}) => {
  const classes = useMeetingListStyles();
  const [t] = useTranslation('meetings');
  const { colorAccent } = integration;

  const [open, setOpen] = useState<boolean>(false);
  const toggle = ({ type, code }: any) => {
    if (type === 'click' || code === 'Space') {
      setOpen(!open);
    }
  };

  const {
    data: proposals,
    error,
    isFetching,
  } = useQuery<ProposalType[], V2ServerErrorsType>(
    [QUERY_PROPOSALS, meeting.id, open],
    () => getProposals(integration, meeting),
    { enabled: open }
  );

  const { subject, date, time, location, chairperson } = meeting;
  const formattedDate = date
    ? fecha.format(new Date(date), dateFormatText)
    : null;

  const title =
    formattedDate || subject
      ? [formattedDate, subject].filter(Boolean).join(' - ')
      : t('meetings.meeting.placeholder');

  const props = [
    { icon: iconNames.access_time, value: time },
    { icon: iconNames.place, value: location },
    { icon: iconNames.gavel, value: chairperson },
  ].filter(prop => Boolean(prop.value));

  return (
    <div className={classes.wrapper}>
      <div
        className={classes.summary}
        onClick={toggle}
        onKeyDown={toggle}
        role="button"
        tabIndex={0}
        data-case-id={meeting.id}
      >
        <Box sx={{ color: colorAccent }} className={classes.title}>
          {title}
        </Box>
        <div className={classes.props}>
          {props.map(({ icon, value }) => (
            <div key={icon} className={classes.prop}>
              <Icon size="extraSmall">{icon}</Icon>
              <span>{value}</span>
            </div>
          ))}
        </div>
      </div>
      {isFetching && !proposals && (
        <div>
          <Loader />
        </div>
      )}
      {proposals && (
        <ProposalList
          integration={integration}
          filters={filters}
          proposals={proposals}
        />
      )}
      {error && <ServerErrorDialog />}
    </div>
  );
};

export default Meeting;
