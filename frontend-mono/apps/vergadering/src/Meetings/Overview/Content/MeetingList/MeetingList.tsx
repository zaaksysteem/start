// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { FilterType, IntegrationType, MeetingType } from '../../../types';
import Meeting from './Meeting';
import { useMeetingListStyles } from './MeetingList.styles';

type MeetingListPropsType = {
  integration: IntegrationType;
  filters: FilterType[];
  meetings: MeetingType[];
};

const MeetingList: React.ComponentType<MeetingListPropsType> = ({
  integration,
  filters,
  meetings,
}) => {
  const [t] = useTranslation('meetings');
  const classes = useMeetingListStyles();

  return meetings.length ? (
    <>
      {meetings.map(meeting => (
        <Meeting
          key={meeting.uuid}
          integration={integration}
          filters={filters}
          meeting={meeting}
        />
      ))}
    </>
  ) : (
    <div className={classes.placeholder}>{t('meetings.placeholder')}</div>
  );
};

export default MeetingList;
