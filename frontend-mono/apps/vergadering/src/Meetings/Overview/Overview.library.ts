// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { dateFormatText } from '../library';
import { FilterProposalsType } from '../types';

/*
  In the case view we know the types of the attributes,
  because we have access to the casetype.

  In the overview we do not fetch the casetypes,
  because the latest version would not be sufficient.

  Some cases may be of older versions, and could lead to
  a lot of different casetype versions to be fetched,
  which would just be overkill and bad for performance.

  Of the types that require value formatting (textarea, wysiwyg, date)
  only date is supported in the overview. So a regex approach is sufficient.
*/
const zuluRegex = /\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z\b/;

export const SCROLL_TARGET = 'scroll-target';

export const scrollToTop = () => {
  document.getElementById(SCROLL_TARGET)?.scrollIntoView();
};

export const filterProposals: FilterProposalsType = (proposals, filters) => {
  return proposals.filter(({ filterText }) =>
    filters.every(({ value }) => filterText.includes(value.toLowerCase()))
  );
};

export const formatValue = (value?: string | null) => {
  if (!value) return '-';

  if (zuluRegex.test(value)) {
    return fecha.format(new Date(value), dateFormatText);
  }

  return value;
};
