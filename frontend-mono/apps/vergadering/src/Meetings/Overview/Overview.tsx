// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { FilterType, GroupingType, IntegrationType } from '../types';
import Header from './Header/Header';
import { useOverviewStyles } from './Overview.styles';
import Content from './Content/Content';

type OverviewPropsType = {
  integration: IntegrationType;
};

const Overview: React.ComponentType<OverviewPropsType> = ({ integration }) => {
  const classes = useOverviewStyles();

  const { defaultGrouping } = integration;

  const [grouping, setGrouping] = useState<GroupingType>(defaultGrouping);
  const [filters, setFilters] = useState<FilterType[]>([]);

  return (
    <div className={classes.wrapper}>
      <Header
        integration={integration}
        filters={filters}
        setFilters={setFilters}
        grouping={grouping}
        setGrouping={setGrouping}
      />
      <Content
        integration={integration}
        grouping={grouping}
        filters={filters}
      />
    </div>
  );
};

export default Overview;
