// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import LoginWrapper from '@zaaksysteem/common/src/components/LoginWrapper';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import ThemeProvider from '@mintlab/ui/App/Material/ThemeProvider/ThemeProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';

import Routes from './Routes';
import { prefix } from './Meetings/constants';

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: 0,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider>
        <LoginWrapper>
          <React.Suspense fallback={<Loader />}>
            <Routes prefix={process.env.APP_CONTEXT_ROOT || prefix} />
            <ServerErrorDialog />
          </React.Suspense>
        </LoginWrapper>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
