// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const meetings = {
  nl: {
    meetings: {
      title: 'Vergaderingen',
      navigation: {
        open: 'Open',
        resolved: 'Afgehandeld',
      },
      filter: {
        placeholder: 'Filter de voorstellen… (3 maximum)',
      },
      buttonBar: {
        close: 'Filters sluiten',
        filter: 'Filteren',
        group: 'Groepeer voorstellen per vergadering',
        ungroup: 'Bekijk enkel de voorstellen',
        logout: 'Uitloggen',
      },
      meetings: {
        meeting: {
          placeholder: 'Vergadering',
        },
        placeholder:
          'Er zijn geen vergaderingen die voldoen aan de voorwaarden.',
      },
      proposals: {
        header: 'Alle voorstellen',
        placeholder: 'Er zijn geen voorstellen die voldoen aan de voorwaarden.',
      },
      case: {
        votingAttributes: {
          title: 'Besluitkenmerken',
        },
        vote: {
          title: 'Besluit invoeren',
        },
        attributes: {
          title: 'Zaakkenmerken',
        },
        documents: {
          title: 'Bijlagen',
          placeholder: 'Er zijn geen bijlagen voor dit voorstel.',
        },
        notes: {
          placeholder: 'Deze notitie is alleen voor u zichtbaar',
          deleteTitle: 'Notitie verwijderen',
          deleteBody: 'Weet u zeker dat u deze notitie wilt verwijderen?',
        },
        resolvedWarning:
          'Dit voorstel is afgehandeld. U kunt geen wijzigingen meer aanbrengen.',
      },
    },
  },
};

export default meetings;
