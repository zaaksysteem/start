// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { WebPartContext } from '@microsoft/sp-webpart-base';
import { spfi, SPFI, SPFx as spSPFx } from '@pnp/sp/presets/all';
import { graphfi, GraphFI, SPFx as graphSPFx } from '@pnp/graph';
import '@pnp/graph/teams';
import '@pnp/sp/webs';
import '@pnp/sp/lists';
import '@pnp/sp/items';
import '@pnp/sp/batching';
import '@pnp/sp/files';
import '@pnp/sp/folders';
import '@pnp/sp/context-info';
import '@pnp/graph/users';

let _sp: SPFI = spfi(); // Initialize _sp with an empty SPFI object
let _graph: GraphFI = graphfi();

export const getSP = (context?: WebPartContext): SPFI => {
  if (context) {
    // eslint-disable-line eqeqeq
    _sp = spfi().using(spSPFx(context));
  }
  return _sp;
};

export const getGraph = (context?: WebPartContext): GraphFI => {
  if (context) {
    //You must add the @pnp/logging package to include the PnPLogging behavior it is no longer a peer dependency
    // The LogLevel set's at what level a message will be written to the console
    _graph = graphfi().using(graphSPFx(context));
  }
  return _graph;
};
