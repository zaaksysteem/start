// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import {
  DefaultButton,
  IconButton,
} from '@fluentui/react/lib/components/Button';
import { Modal } from '@fluentui/react/lib/Modal';
import { Checkbox } from '@fluentui/react/lib/components/Checkbox';
import { sendFilesToZs } from './getTeamFiles';

export const TransferFilesToZsDialog = ({
  close,
  opened,
  files,
  relativeUrl,
  zaaksysteemId,
  zaaksysteemUrl,
}: {
  close: () => void;
  opened: boolean;
  files: any[];
  relativeUrl: string;
  zaaksysteemId: string;
  zaaksysteemUrl: string;
}) => {
  const [selected, setSelected] = React.useState([] as any[]);

  return (
    <Modal
      closeButtonAriaLabel="close"
      isOpen={opened}
      onDismiss={close}
      isBlocking={false}
    >
      <div
        style={{
          minWidth: '600px',
          minHeight: '300px',
          padding: '0 20px 20px 20px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
      >
        {files.length ? (
          <>
            <div>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <h2>Selecteer bestanden om over te brengen</h2>
                <IconButton
                  style={{ marginTop: '10px' }}
                  iconProps={{ iconName: 'Cancel' }}
                  onClick={close}
                />
              </div>
              {files.map((file, ix) => {
                return (
                  <Checkbox
                    styles={{ root: { margin: '8px 0 8px 0' } }}
                    label={file.Name}
                    key={ix}
                    onChange={(ev, val) => {
                      setSelected(
                        val
                          ? [...selected, file]
                          : selected.filter(sel => sel !== file)
                      );
                    }}
                  />
                );
              })}
            </div>
            <DefaultButton
              style={{ maxWidth: '120px', marginLeft: 'auto' }}
              onClick={() =>
                sendFilesToZs(
                  relativeUrl,
                  selected,
                  zaaksysteemId,
                  zaaksysteemUrl
                ).then(close)
              }
              text="Uploaden"
            />
          </>
        ) : (
          <div
            style={{
              paddingTop: '20px',
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <p>
              Er zijn geen bestanden in dit team die kunnen worden overgedragen
            </p>
            <IconButton
              style={{ marginTop: '10px' }}
              iconProps={{ iconName: 'Cancel' }}
              onClick={close}
            />
          </div>
        )}
      </div>
    </Modal>
  );
};
