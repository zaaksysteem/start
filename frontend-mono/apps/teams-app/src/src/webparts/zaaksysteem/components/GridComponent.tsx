// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import { Grid } from 'gridjs-react';
import axios from 'axios';
import 'gridjs/dist/theme/mermaid.css';

interface IZaaksysteemWebPartProps {
  zaaksysteemUrl: string;
  zaaksysteemId: string;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const GridComponent: React.FunctionComponent<
  IZaaksysteemWebPartProps
> = props => {
  const [data, setData] = React.useState([]);

  React.useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const fetchData = async () => {
      try {
        //url for data https://supzsnl.sharepoint.com/sites/TEAMS/Shared%20Documents/data.json
        const result = await axios.get(props.zaaksysteemUrl);
        setData(result.data);
      } catch (error) {
        console.error(error);
      }
    };
    // eslint-disable-next-line no-void
    void fetchData();
  }, [props.zaaksysteemUrl]);

  return (
    <div>
      <h1>Zaaknummer: {props.zaaksysteemId}</h1>
      <Grid
        data={data}
        columns={['Document', 'Nr', 'Gemaakt op', 'Gewijzigd op', 'Kopieer']}
      />
    </div>
  );
};

export default GridComponent;
