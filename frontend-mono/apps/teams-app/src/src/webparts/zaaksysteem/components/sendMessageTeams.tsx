// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { graphPost, GraphQueryable } from '@pnp/graph';
import { body } from '@pnp/queryable';
import { escape } from '@microsoft/sp-lodash-subset';
import { getGraph } from '../pnpjsConfig';

type SendMessageTeamsReturn = {
  sendMessage: (
    bestandURL: string | undefined,
    teamId: string,
    channelId: string,
    messageTeams: string
  ) => Promise<void>;
};

export const sendMessageTeams = (): SendMessageTeamsReturn => {
  const graphContext = getGraph();

  const sendMessage = async (
    bestandURL: string | undefined,
    teamId: string,
    channelId: string,
    messageTeams: string
  ): Promise<void> => {
    const messages = await graphContext.teams
      .getById(teamId ?? '')
      .channels.getById(channelId ?? '').messages;

    await graphPost(
      GraphQueryable(messages),
      body({
        body: {
          contentType: 'html',
          content: `<p>${escape(messageTeams ?? '')}</p><p><a href="${
            bestandURL ?? ''
          }">Klik hier om naar het bestand te gaan.</a></p>`,
        },
      })
    );
  };

  return { sendMessage };
};
