// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

require('./Zaaksysteem.module.css');
const styles = {
  zaakSysteem: 'zaakSysteem_5bc328e1',
  teams: 'teams_5bc328e1',
  header: 'header_5bc328e1',
  zaaknummer: 'zaaknummer_5bc328e1',
  logo: 'logo_5bc328e1',
  zaakbutton: 'zaakbutton_5bc328e1',
  kopieerbutton: 'kopieerbutton_5bc328e1',
  links: 'links_5bc328e1',
};

export default styles;
/* tslint:enable */
