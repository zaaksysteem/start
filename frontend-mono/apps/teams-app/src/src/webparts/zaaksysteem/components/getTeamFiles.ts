// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import axios from 'axios';
import { getSP } from '../pnpjsConfig';

export const getTeamFiles = async (relativeUrl: string) => {
  const spU = getSP();

  const res = await spU.web.getFolderByServerRelativePath(relativeUrl).files();

  return res;
};

export const sendFilesToZs = async (
  relativePath: string,
  files: any[],
  zaaksysteemId: string,
  zaaksysteemUrl: string
) => {
  const spU = getSP();

  for (const file of files) {
    const res = await spU.web
      .getFolderByServerRelativePath(relativePath)
      .files.getByUrl(file.Name)
      .getBlob();

    const renamedBlob = new File([res], file.Name);

    const fd = new FormData();
    fd.append('file', renamedBlob);
    fd.append('case_id', zaaksysteemId);

    await axios.post(`${zaaksysteemUrl}file/create`, fd);
  }
};
