// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as React from 'react';
import { Grid } from 'gridjs-react';
import { h } from 'gridjs';
import axios from 'axios';
import 'gridjs/dist/theme/mermaid.css';
//import "./gridjsWithDarkmode.css";
import type { TextareaProps } from '@fluentui/react-components';
import { MessageBar, MessageBarType } from '@fluentui/react';
import { Dialog, DialogType, DialogFooter } from '@fluentui/react/lib/Dialog';
import { PrimaryButton, DefaultButton } from '@fluentui/react/lib/Button';
import { useBoolean } from '@fluentui/react-hooks';
import { Stack, IStackTokens } from '@fluentui/react/lib/Stack';
import { Toggle } from '@fluentui/react/lib/Toggle';
import {
  FluentProvider,
  teamsLightTheme,
  Toaster,
  Textarea,
} from '@fluentui/react-components';
import { getSP } from '../pnpjsConfig';
import { useToastNotifications } from './useToastNotifications';
import { sendMessageTeams } from './sendMessageTeams';
import styles from './Zaaksysteem.module.scss';

interface FileData {
  modifiedDate: string;
  documentId: string;
  filename: string;
  number: number;
  createdDate: string;
}

interface IZaaksysteemWebPartProps {
  zaaksysteemUrl: string;
  zaaksysteemId: string;
  relativeUrl: string | undefined;
  teamId: string | undefined;
  channelId: string | undefined;
  domainUrl: string | undefined;
}

const stackTokens: IStackTokens = { childrenGap: 10 };

let zaakGuid: string;
let zsFilename: string;
let zsFileGuid: string;
let messageTeams: string;

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const ZaaksysteemTable: React.FunctionComponent<
  IZaaksysteemWebPartProps
> = props => {
  const spU = getSP();

  const [data, setData] = React.useState<FileData[]>([]);

  const [alertBericht, setAlertBericht] = React.useState<string>(
    'Oeps! Er is iets misgegaan.'
  );

  const [hideDialog, { toggle: toggleHideDialog }] = useBoolean(true);

  const [hideAlert, { toggle: toggleHideAlert }] = useBoolean(false);

  const { notify, notifyDownload, notifyDownloadFailed, toasterId } =
    useToastNotifications();

  const { sendMessage } = sendMessageTeams();

  const [bestandOverschrijven, { toggle: toggleBestandOverschrijven }] =
    useBoolean(false);

  const [berichtPlaatsen, { toggle: toggleBerichtPlaatsen }] =
    useBoolean(false);

  const _onChangeSaveMessage: TextareaProps['onChange'] = (ev, data) => {
    messageTeams = data.value ?? '';
  };

  function _onChangeBericht(
    ev: React.MouseEvent<HTMLElement>,
    checked?: boolean
  ): void {
    toggleBerichtPlaatsen();
  }

  function _onChangeOverschrijven(
    _ev: React.MouseEvent<HTMLElement>,
    checked?: boolean
  ): void {
    toggleBestandOverschrijven();
  }

  const modelProps = {
    isBlocking: false,
    styles: { main: { maxWidth: 450 } },
  };
  const dialogContentProps = {
    type: DialogType.largeHeader,
    title: `${zsFilename} kopieren naar Teams`,
    subText:
      'Kies of u het bestand wilt overschrijven en of u een bericht in het kanaal wilt plaatsen. Er wordt automatisch een link naar het bestand geplaatst onder het bericht.',
  };

  // Grid Table columns
  const columns = [
    {
      id: 'filename',
      name: 'Bestandsnaam',
    },
    {
      id: 'number',
      name: 'Nummer',
    },
    {
      id: 'modifiedDate',
      name: 'Gewijzigd',
    },
    {
      id: 'createdDate',
      name: 'Aangemaakt',
    },
    {
      id: 'documentId',
      name: 'Document ID',
      hidden: true,
    },
    {
      name: 'Actions',
      formatter: (cell: h.JSX.Element, row: { cells: { data: string }[] }) => {
        return h(
          'button',
          {
            className: styles.kopieerbutton,
            onClick: async () => {
              zsFilename = row.cells[0].data;
              zsFileGuid = row.cells[4].data;
              messageTeams = '';
              toggleHideDialog();
            },
          },
          ' '
        );
      },
    },
  ];

  async function _kopieerBestand(): Promise<void> {
    notifyDownload(zsFilename);

    await axios
      .get(
        `${props.zaaksysteemUrl}api/v1/case/${zaakGuid}/document/${zsFileGuid}/download`,
        {
          responseType: 'arraybuffer',
          withCredentials: true,
        }
      )
      .then(async response => {
        if (response.status === 200) {
          const blob = response.data;

          let randomString = '';
          const characters =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
          const charactersLength = characters.length;
          for (let ix = 0; ix < 5; ix++) {
            randomString += characters.charAt(
              Math.floor(Math.random() * charactersLength)
            );
          }

          if (!bestandOverschrijven) {
            zsFilename = `${randomString}-${zsFilename}`;
          }

          // Pass the blob and filename to the upload function
          if (props.relativeUrl) {
            await spU.web
              .getFolderByServerRelativePath(props.relativeUrl)
              .files.addUsingPath(zsFilename, blob, {
                Overwrite: bestandOverschrijven,
              })
              .then(async result => {
                if (berichtPlaatsen) {
                  await sendMessage(
                    `https://${props.domainUrl}${result.data.ServerRelativeUrl}`,
                    props.teamId ?? '',
                    props.channelId ?? '',
                    messageTeams
                  );
                }
                notify(zsFilename);
              });
          } else {
            notifyDownloadFailed(
              zsFilename,
              'Relatieve URL van SharePoint niet gevonden'
            );
          }
        } else {
          // Handle errors
          notifyDownloadFailed(zsFilename, 'Bestand niet kunnen downloaden');
        }
      })
      .catch(() => {
        // Handle network errors or other issues
        notifyDownloadFailed(
          zsFilename,
          'Er is iets misgegaan. Probeer het later opnieuw.'
        );
      });
  }

  // table data

  React.useEffect(() => {
    /* eslint complexity: [2, 10] */
    const fetchData = async (): Promise<void> => {
      try {
        //url for data https://supzsnl.sharepoint.com/sites/TEAMS/Shared%20Documents/data.json

        if (!props.zaaksysteemUrl || !props.zaaksysteemId) {
          setAlertBericht(
            'Er is nog geen URL en ID ingevuld, klik op de tab instellingen om ze in te vullen.'
          );
          if (!hideAlert) {
            toggleHideAlert();
          }
          return;
        }

        const urlPattern =
          /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,})\/$/i; // domain name and extension

        if (urlPattern.test(props.zaaksysteemUrl)) {
          // URL is valid
        } else {
          setAlertBericht(
            'De URL is niet geldig, klik op de tab instellingen om de URL te wijzigen.'
          );
          if (!hideAlert) {
            toggleHideAlert();
          }
          return;
        }

        const getZaakguid = await axios.get(
          `${props.zaaksysteemUrl}api/v1/case/get_by_number/${props.zaaksysteemId}`
        );

        zaakGuid = getZaakguid.data.result.reference;

        const dataUrl = `${props.zaaksysteemUrl}api/v1/case/${zaakGuid}/document/all`;

        const result = await axios.get(dataUrl);

        const fileArray: FileData[] = [];

        for (let ix = 0; ix < result.data.result.instance.rows.length; ix++) {
          const obj = result.data.result.instance.rows[ix];

          const createdDate = new Date(obj.instance.file.instance.date_created);
          const modifiedDate = new Date(
            obj.instance.file.instance.date_modified
          );
          const fileData = {
            documentId: obj.instance.file.reference,
            filename: obj.instance.filename,
            number: obj.instance.number,
            createdDate: createdDate.toLocaleString('nl-NL'),
            modifiedDate: modifiedDate.toLocaleString('nl-NL'),
          };
          fileArray.push(fileData);
        }
        if (hideAlert) {
          toggleHideAlert();
        }

        setData(fileArray as FileData[]);
      } catch (error) {
        console.log(error);
        setAlertBericht(
          `Er is iets misgegaan. Controleer of de url (${props.zaaksysteemUrl}) of het Zaak Id (${props.zaaksysteemId}) correct is ingevuld.`
        );
        if (!hideAlert) {
          toggleHideAlert();
        }
        setData([] as FileData[]);
      }
    };
    fetchData().catch(error => console.error(error));
  }, [props.zaaksysteemUrl, props.zaaksysteemId]);

  return (
    <>
      <FluentProvider theme={teamsLightTheme}>
        <Toaster toasterId={toasterId} />
        <Dialog
          hidden={hideDialog}
          onDismiss={toggleHideDialog}
          dialogContentProps={dialogContentProps}
          modalProps={modelProps}
        >
          <Stack tokens={stackTokens}>
            <Toggle
              label="Bestaand bestand overschrijven"
              onText="On"
              offText="Off"
              onChange={_onChangeOverschrijven}
            />
            <Toggle
              label="Bericht in kanaal plaatsen"
              onText="On"
              offText="Off"
              onChange={_onChangeBericht}
            />
            {berichtPlaatsen && (
              <Textarea
                onChange={_onChangeSaveMessage}
                placeholder="Type bericht voor teams"
              />
            )}
          </Stack>
          <DialogFooter>
            <PrimaryButton
              onClick={async () => {
                toggleHideDialog();
                await _kopieerBestand();
              }}
              text="Kopieer"
            />
            <DefaultButton onClick={toggleHideDialog} text="Annuleren" />
          </DialogFooter>
        </Dialog>
      </FluentProvider>
      {hideAlert && (
        <MessageBar
          messageBarType={MessageBarType.error}
          isMultiline={true}
          dismissButtonAriaLabel="Close"
          onDismiss={() => {
            toggleHideAlert();
          }}
        >
          {alertBericht}
        </MessageBar>
      )}
      {!hideAlert && (
        <Grid
          data={data}
          columns={columns}
          sort={{
            enabled: true,
          }}
          language={{
            pagination: {
              previous: '⬅️',
              next: '➡️',
              showing: '😃 aantal',
              results: () => 'Resultaten',
            },
            noRecordsFound: 'Geen documenten gevonden',
          }}
        />
      )}
    </>
  );
};

export default ZaaksysteemTable;
