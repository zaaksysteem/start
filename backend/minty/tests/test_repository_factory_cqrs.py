# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import statsd
from minty.cqrs import (
    CQRS,
    CommandBase,
    CommandWrapper,
    MiddlewareBase,
    QueryBase,
    QueryMiddleware,
    QueryWrapper,
    UserInfo,
)
from minty.repository import RepositoryBase, RepositoryFactory
from unittest import mock
from uuid import uuid4

statsd.Connection.set_defaults(disabled=True)


class DummyInfrastructure:
    pass


class DummyInfrastructureRO(DummyInfrastructure):
    pass


class DummyInfrastructureRW(DummyInfrastructure):
    pass


class DummyRepository(RepositoryBase):
    REQUIRED_INFRASTRUCTURE = {"DummyInfraStructure": DummyInfrastructure}
    REQUIRED_INFRASTRUCTURE_RO = {
        "RODummyInfraStructure": DummyInfrastructureRO
    }
    REQUIRED_INFRASTRUCTURE_RW = {
        "RWDummyInfraStructure": DummyInfrastructureRW
    }


class DummyQuery(QueryBase):
    class_attribute = 42

    def some_query(self, arg):
        return f"Arg was: '{arg}'"


class DummyCommand(CommandBase):
    not_callable = 3

    def some_command(self, arg):
        return f"arg: {arg}"


class DummyDomain:
    REQUIRED_REPOSITORIES = {"DummyRepository": DummyRepository}

    @staticmethod
    def get_query_instance(repo_factory, context, user_uuid):
        return DummyQuery(repo_factory, context, user_uuid)

    @staticmethod
    def get_command_instance(repo_factory, context, user_uuid, event_service):
        return DummyCommand(repo_factory, context, user_uuid, event_service)


class TestRepositoryFactory:
    """Test the repository factory"""

    def test_repository_factory(self):
        infra_factory = {"dummy": "dummy"}
        infra_factory_ro = {"dummy": "dummy2"}
        repo_factory = RepositoryFactory(
            infrastructure_factory=infra_factory,  # type: ignore
            infrastructure_factory_ro=infra_factory_ro,  # type: ignore
        )

        assert repo_factory.infrastructure_factory == infra_factory
        assert repo_factory.infrastructure_factory_ro == infra_factory_ro
        assert repo_factory.repositories == {}

        repo_factory.register_repository(
            name="dummyrepo",
            repository=DummyRepository,  # type: ignore
        )

        assert repo_factory.repositories == {"dummyrepo": DummyRepository}

        repo = repo_factory.get_repository(
            "dummyrepo", read_only=True, context="no context"
        )

        assert isinstance(repo, DummyRepository)
        assert repo.read_only is True

        assert repo_factory.infrastructure_factory == infra_factory


class TestCQRS:
    """Test the CQRS class"""

    def test_cqrs(self):
        class InfraFactory:
            def __init__(self):
                self.infra = {}

            def __copy__(self):
                cls = self.__class__
                copy = cls.__new__(cls)
                copy.infra = {}
                return copy

            def register_infrastructure(self, name, infrastructure):
                self.infra[name] = infrastructure

            def set_local_storage(self, event):
                pass

            def flush_local_storage(self):
                pass

        infra_factory = InfraFactory()

        cqrs = CQRS([DummyDomain], infra_factory)
        assert cqrs.domains["DummyDomain"]["module"] == DummyDomain

        assert (
            cqrs.domains["DummyDomain"][
                "repository_factory"
            ].infrastructure_factory
            == infra_factory
        )

        infra_factory_rw = cqrs.domains["DummyDomain"][
            "repository_factory"
        ].infrastructure_factory
        assert sorted(infra_factory_rw.infra.keys()) == [
            "DummyInfraStructure",
            "RWDummyInfraStructure",
        ]

        infra_factory_ro = cqrs.domains["DummyDomain"][
            "repository_factory"
        ].infrastructure_factory_ro
        assert sorted(infra_factory_ro.infra.keys()) == [
            "DummyInfraStructure",
            "RODummyInfraStructure",
        ]

        correlation_id = uuid4()
        user_uuid = uuid4()

        qi = cqrs.get_query_instance(
            correlation_id=correlation_id,
            domain="DummyDomain",
            context="foobar",
            user_uuid=user_uuid,
            user_info=UserInfo(user_uuid=user_uuid, permissions={}),
        )

        assert isinstance(qi, QueryWrapper)

        assert qi.user_uuid == user_uuid
        assert qi.context == "foobar"
        assert isinstance(qi.query_instance, DummyQuery)

        repo_factory = qi.query_instance.repository_factory
        assert (
            repo_factory == cqrs.domains["DummyDomain"]["repository_factory"]
        )
        assert qi.query_instance.context == "foobar"
        assert qi.query_instance.user_uuid == user_uuid

    def test_cqrs_command(self):
        class InfraFactory:
            def __init__(self):
                self.infra = []

            def register_infrastructure(self, name, infrastructure):
                self.infra.append(infrastructure)

            def set_local_storage(self):
                pass

            def flush_local_storage(self):
                pass

        infra_factory = InfraFactory()

        cqrs = CQRS([DummyDomain], infra_factory)
        assert cqrs.domains["DummyDomain"]["module"] == DummyDomain

        assert (
            cqrs.domains["DummyDomain"][
                "repository_factory"
            ].infrastructure_factory
            == infra_factory
        )

        correlation_id = uuid4()
        user_uuid = uuid4()
        ci = cqrs.get_command_instance(
            correlation_id=correlation_id,
            domain="DummyDomain",
            context="foobar",
            user_uuid=user_uuid,
            user_info=UserInfo(user_uuid=user_uuid, permissions={}),
        )
        assert isinstance(ci, CommandWrapper)
        assert isinstance(ci.command_instance, DummyCommand)

        rv = ci.some_command(arg="value")
        assert rv is None
        assert ci.not_callable == 3
        assert ci.user_uuid == user_uuid

    def test_middleware_registration(self):
        class InfraFactory:
            def __init__(self):
                self.infra = []

            def register_infrastructure(self, name, infrastructure):
                self.infra.append(infrastructure)

            def set_local_storage(self):
                pass

            def flush_local_storage(self):
                pass

        middleware_check = mock.MagicMock()

        class MockMiddleWare(MiddlewareBase):
            def __call__(self, func):
                middleware_check.before("check1")
                func()
                middleware_check.after("check2")

        class MockQueryMiddleware(QueryMiddleware):
            def __call__(self, func):
                return func()

        infra_factory = InfraFactory()
        command_wrapper_middleware = [MockMiddleWare]
        query_middleware = [MockQueryMiddleware]
        cqrs = CQRS(
            [DummyDomain],
            infra_factory,
            command_wrapper_middleware=command_wrapper_middleware,
            query_middleware=query_middleware,
        )

        correlation_id = uuid4()
        user_uuid = uuid4()
        ci = cqrs.get_command_instance(
            correlation_id=correlation_id,
            domain="DummyDomain",
            context="foobar",
            user_uuid=user_uuid,
            # No optional user_info yet, add when that becomes mandatory
        )
        ci.some_command(arg="value")
        middleware_check.before.assert_called_once_with("check1")
        middleware_check.after.assert_called_once_with("check2")

        qi = cqrs.get_query_instance(
            correlation_id=correlation_id,
            domain="DummyDomain",
            context="foobar",
            user_uuid=user_uuid,
            user_info=UserInfo(user_uuid=user_uuid, permissions={}),
        )

        rv = qi.some_query("foo")
        assert qi.class_attribute == 42

        assert rv == "Arg was: 'foo'"
