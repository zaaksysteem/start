# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import unittest
from unittest import mock
from uuid import uuid4


class DemoCommand(minty.cqrs.SplitCommandBase):
    name = "demo"

    def __call__(self, x: str | None = None):
        self.cmd.test_cmd(x)


class DemoCommand2(minty.cqrs.SplitCommandBase):
    name = "demo"

    def __call__(self, x: str | None = None):
        self.cmd.test_cmd(x)


class DemoCommandWithRepo(minty.cqrs.SplitCommandBase):
    name = "demo_with_repo"

    def __call__(self, x: str | None = None):
        self.get_repository("repo_name_here")


class DemoCommandWithHasPermission(minty.cqrs.SplitCommandBase):
    name = "demo_with_permission"

    def __call__(self, x: str | None = None):
        if self._user_has_permission("admin"):
            return

        raise PermissionError


class DemoQuery(minty.cqrs.SplitQueryBase):
    name = "demo"

    def __call__(self, x: str | None = None):
        self.qry.test_qry(x)


class DemoQuery2(minty.cqrs.SplitQueryBase):
    name = "demo"

    def __call__(self, x: str | None = None):
        self.qry.test_qry(x)


class DemoQueryWithRepo(minty.cqrs.SplitQueryBase):
    name = "demo_with_repo"

    def __call__(self, x: str | None = None):
        self.get_repository("repo_name_here")


class TestSplitCommandBase(unittest.TestCase):
    def test_cmd_accessor(self):
        mock_cmd = mock.Mock()

        command = DemoCommand(
            repository_factory=mock.Mock(),
            context="mock",
            user_uuid=uuid4(),
            event_service=mock.Mock(),
            command_instance=mock_cmd,
        )

        command_return = command("test")

        self.assertEqual(command_return, None)
        mock_cmd.test_cmd.assert_called_once_with("test")

    def test_get_repository(self):
        mock_cmd = mock.Mock()
        mock_event_service = mock.Mock()
        mock_repo_factory = mock.Mock()

        command = DemoCommandWithRepo(
            repository_factory=mock_repo_factory,
            context="context",
            user_uuid=uuid4(),
            event_service=mock_event_service,
            command_instance=mock_cmd,
        )

        command_return = command("test")

        self.assertEqual(command_return, None)
        mock_repo_factory.get_repository.assert_called_once_with(
            name="repo_name_here",
            context="context",
            event_service=mock_event_service,
            read_only=False,
        )

    def test_user_has_permission(self):
        mock_cmd = mock.Mock()
        mock_event_service = mock.Mock()
        mock_repo_factory = mock.Mock()

        command = DemoCommandWithHasPermission(
            repository_factory=mock_repo_factory,
            context="context",
            user_uuid=uuid4(),
            event_service=mock_event_service,
            command_instance=mock_cmd,
        )

        delattr(command.cmd, "user_info")
        command_return = command("test")
        self.assertEqual(
            command_return,
            None,
            "No user_info: always allow",
        )

        command.cmd.user_info = minty.cqrs.UserInfo(uuid4(), {"admin": True})

        command_return = command("test")
        self.assertEqual(command_return, None, "User info present: permission")

        command.cmd.user_info = minty.cqrs.UserInfo(uuid4(), {"admin": False})

        with self.assertRaises(
            PermissionError, msg="User info present: no permission"
        ):
            command_return = command("test")


class TestSplitQueryBase(unittest.TestCase):
    def test_qry_accessor(self):
        mock_qry = mock.Mock()

        query = DemoQuery(
            repository_factory=mock.Mock(),
            context="mock",
            user_uuid=uuid4(),
            query_instance=mock_qry,
        )

        query_return = query("test")

        self.assertEqual(query_return, None)
        mock_qry.test_qry.assert_called_once_with("test")

    def test_get_repository(self):
        mock_qry = mock.Mock()
        mock_repo_factory = mock.Mock()

        query = DemoQueryWithRepo(
            repository_factory=mock_repo_factory,
            context="context",
            user_uuid=uuid4(),
            query_instance=mock_qry,
        )

        query_return = query("test")

        self.assertEqual(query_return, None)
        mock_repo_factory.get_repository.assert_called_once_with(
            name="repo_name_here",
            context="context",
            event_service=None,
            read_only=True,
        )


class Test_build_command_lookup_table(unittest.TestCase):
    def test_build_command_lookup_table_success(self):
        rv = minty.cqrs.build_command_lookup_table(
            {DemoCommand, DemoCommandWithRepo}
        )

        self.assertEqual(
            rv,
            {"demo": DemoCommand, "demo_with_repo": DemoCommandWithRepo},
        )

    def test_build_command_lookup_table_failure(self):
        with self.assertRaises(AssertionError):
            minty.cqrs.build_command_lookup_table({DemoCommand, DemoCommand2})


class Test_build_query_lookup_table(unittest.TestCase):
    def test_build_query_lookup_table_success(self):
        rv = minty.cqrs.build_query_lookup_table(
            {DemoQuery, DemoQueryWithRepo}
        )

        self.assertEqual(
            rv,
            {"demo": DemoQuery, "demo_with_repo": DemoQueryWithRepo},
        )

    def test_build_query_lookup_table_failure(self):
        with self.assertRaises(AssertionError):
            minty.cqrs.build_query_lookup_table({DemoQuery, DemoQuery2})


class TestDomainCommandContainer(unittest.TestCase):
    def test_wrapping_behavior(self):
        mock_event_service = mock.Mock()
        mock_repo_factory = mock.Mock()

        clt = minty.cqrs.build_command_lookup_table({DemoCommand})

        dc = minty.cqrs.DomainCommandContainer(
            repository_factory=mock_repo_factory,
            context="context",
            user_uuid=uuid4(),
            event_service=mock_event_service,
            command_lookup_table=clt,
        )

        self.assertIsInstance(dc.demo, DemoCommand)
        with self.assertRaises(AttributeError):
            _ = dc.no_such_attribute


class TestDomainQueryContainer(unittest.TestCase):
    def test_wrapping_behavior(self):
        mock_repo_factory = mock.Mock()

        qlt = minty.cqrs.build_query_lookup_table({DemoQuery})

        dc = minty.cqrs.DomainQueryContainer(
            repository_factory=mock_repo_factory,
            context="context",
            user_uuid=uuid4(),
            query_lookup_table=qlt,
        )

        self.assertIsInstance(dc.demo, DemoQuery)
        with self.assertRaises(AttributeError):
            _ = dc.no_such_attribute
