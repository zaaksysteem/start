pytz>=2021.3

## Framework
amqpstorm~=2.10
apacheconfig~=0.3
file-magic
holidays<1.0
jsonpointer
jsonschema
python-statsd
redis
pydantic<3.0,>=2.0
