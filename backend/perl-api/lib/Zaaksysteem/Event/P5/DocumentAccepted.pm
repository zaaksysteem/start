package Zaaksysteem::Event::P5::DocumentAccepted;
use Moose;

with 'Zaaksysteem::Event::EventInterface';

=head1 NAME

Zaaksysteem::Event::P5::DocumentAccepted - Event handler for DocumentCreated

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

sub event_type { 'DocumentAccepted' }

sub emit_event {
    my $self = shift;

    my $ok = $self->queue->emit_case_event(
        {
            case       => $self->case,
            event_name => 'DocumentAddedToCase',
            description => 'Document geaccepteerd door de behandelaar',
            changes    => $self->event->{ changes },
            user_uuid  => $self->event->{ user_uuid },
        }
    );
    return $self->new_ack_message('Event processed') if $ok;
    return $self->new_ack_message('Event processed: nothing send');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

