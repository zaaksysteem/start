package Zaaksysteem::Object::SecurityIdentity::Role;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object::SecurityIdentity';

=head1 NAME

Zaaksysteem::Object::SecurityIdentity::Role - Role specific security
identity data-wrapper

=head1 DESCRIPTION

This class extends L<Zaaksysteem::Object::SecurityIdentity>, and adds behavior
specific to the conceptual 'role' of a user within the DB.

=head1 ATTRIBUTES

=head2 role_id

This attribute holds an integer ID which references an role.

=cut

has role_id => (
    is => 'rw',
    isa => 'Int',
    required => 1
);

=head2 entity_id

This attribute is inherited from L<Zaaksysteem::Object::SecurityIdentity>. We
extend it to add a default value hook based on the L</ou_id> and L</role_id>
attributes, effectively making the attribute optional.

=cut

has '+entity_id' => (
    lazy => 1,
    default => sub {
        return shift->role_id;
    }
);

=head2 entity_type

This attribute is inherited from L<Zaaksysteem::Object::SecurityIdentity>. We
extend it to add a default value of 'role', effectively making it
optional.

=cut

has '+entity_type' => (
    default => 'role'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

