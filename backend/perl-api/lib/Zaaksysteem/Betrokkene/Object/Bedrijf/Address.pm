package Zaaksysteem::Betrokkene::Object::Bedrijf::Address;
use v5.26;
use Object::Pad;

=head1 NAME

Zaaksysteem::Betrokkene::Object::Bedrijf::Address - An address object for bedrijf

=head1 DESCRIPTION

This is just a simple data container to sort of reflect the API of
ZS::Betrokkene::Object::NatuurlijkPersoon where we can call
$object->correspondentieadres and have an address object. It just mimics the
same logic without the ability to save it in the DB.

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head2 straatnaam

=head2 postcode

=head2 woonplaats

=head2 huisletter

=head2 huisnummer

=head2 huisnummertoevoeging

=head2 adres_buitenland1

=head2 adres_buitenland2

=head2 adres_buitenland3

=head1 METHODS

=head2 straatnaam

=head2 postcode

=head2 woonplaats

=head2 huisletter

=head2 huisnummer

=head2 huisnummertoevoeging

=head2 adres_buitenland1

=head2 adres_buitenland2

=head2 adres_buitenland3

=cut

class Zaaksysteem::Betrokkene::Object::Bedrijf::Address;

field $straatnaam :reader :param = undef;
field $postcode :reader :param = undef;
field $woonplaats :reader :param = undef;
field $huisletter :reader :param = undef;
field $huisnummer :reader :param = undef;
field $huisnummertoevoeging :reader :param = undef;

field $adres_buitenland1 :reader :param =undef;
field $adres_buitenland2 :reader :param =undef;
field $adres_buitenland3 :reader :param =undef;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
