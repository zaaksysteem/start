package Zaaksysteem::Controller::Form::Employee;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller' };

=head1 NAME

Zaaksysteem::Controller::Form::Employee - A internal case registration form for employee

=head1 DESCRIPTION

An internal case creation endpoint for employees.

=head1 ACTIONS

=head2 internal_form_base

=cut


sub internal_form_base : Chained('/') : PathPart('form/employee') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

=head2 create_case_by_casetype

=head3 URI

    C</form/employee/ID/NAME>

=head3 DESCRIPTION

Allows direction creation of a case via a clickable link for logged in users of the organisation.
It redirects to the case creation form with the logged in user as case requestor, the casetype etc.
If the casetype is not I<intern> or I<internextern> you cannot create a case via this endpoint.

=cut

sub create_case_by_casetype : Chained('internal_form_base'): PathPart('') : Args(2) {
    my ($self, $c, $id, $name) = @_;

    if ($id !~ /^\d+$/) {
        $c->log_detach(
            error          => "Casetype ID is not a number",
            human_readable => "Ongeldige zaaktype ID opgegeven",
        );
    }

    my $casetype = $c->model('DB::Zaaktype')->search(
        {
            'me.id'                    => $id,
            'me.deleted'               => undef,
            'zaaktype_node_id.trigger' => ['intern', 'internextern'],
            'me.active'                => 1,
        },
        { 'prefetch' => 'zaaktype_node_id', }
    )->first;

    if (!$casetype) {
        $c->log_detach(
            error          => "Casetype with id $id is not a internal casetype",
            human_readable => "Ongeldige zaaktype ID opgegeven",
        );
    }

    $c->res->redirect(
        $c->uri_for(sprintf('/intern/aanvragen/%s/', $casetype->uuid), {
            aanvrager => $c->user->uuid,
            contactkanaal => 'behandelaar'
        })
    );

    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
