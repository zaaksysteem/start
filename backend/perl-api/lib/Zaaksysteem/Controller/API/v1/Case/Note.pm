package Zaaksysteem::Controller::API::v1::Case::Note;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Note - APIv1 controller for case note objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case/[CASE_UUID]/note>. Extensive
documentation about this API can be found in:

L<Zaaksysteem::Manual::API::V1::Case::Note>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case::Note>

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::Case::Note;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/note> routing namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('note') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{notes_rs} = $c->stash->{zaak}->case_notes->search_rs(
        { subject_id => $c->user->id },
        { order_by => { -asc => 'id' }}
    );
}

=head2 instance_base

Reserves the C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]> routing namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $note_uuid) = @_;

    $c->stash->{note}
        = $c->stash->{notes_rs}->search_rs({ uuid => $note_uuid })->first;

    if (!$c->stash->{note}) {
        throw("api/v1/case/note/id/not_found",
            "Unable to find note with id $note_uuid");
    }
}

=head2 create

Create a new case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->stash->{note} = $c->model('DB::CaseNotes')->create(
        {
            value      => $c->req->params->{content},
            subject_id => $c->user->id,
            case_id    => $c->stash->{zaak}->id,
        }
    );
    $c->forward('get');
}

=head2 get

Retrieve a single note by its identifier (UUID).

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ note };
}

=head2 update

Update a case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]/update>

=cut

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    try {
        $c->model('DB')->txn_do(
            sub {
                $c->stash->{note}->update(
                    {
                        value         => $c->req->params->{content},
                        date_modified => \'NOW()',
                    }
                )->discard_changes;
            }
        );
    }
    catch {
        throw('api/v1/case/note/update/fault',
            sprintf('Error updating note "%s": %s', $c->stash->{note}->id, $_)
        );
    };

    $c->forward('get');
}

=head2 delete

Remove a case note.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note/[NOTE_UUID]/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->stash->{note}->delete;
    $c->forward('list');
}

=head2 list

Retrieve a list of all case note objects for a specific case.

=head3 URL path

C</api/v1/case/[CASE_UUID]/note>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;


    $c->stash->{set} = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{notes_rs}
    );
    $self->list_set($c);
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
