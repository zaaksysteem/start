package Zaaksysteem::Controller::API::File;

use Moose;

use BTTW::Tools;
use List::Util qw(notall);
use Zaaksysteem::Types qw(UUID);

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub base : Chained('/') : PathPart('api/file') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    throw ('file/no_file_id_given', 'No file id present in request')
        unless $file_id;

    # current purpose is only files that are not attached to a case.
    $c->stash->{file} = $c->model('DB::File')->search({
        id => $file_id,
        case_id => undef
    })->active->first or throw (
        'file/not_found',
        "Could not find unattached file with id $file_id"
    );
}

=head2 get

=head3 URL

C</api/file/[FILE_ID]>

=cut

sub get: Chained('base') : PathPart('') : Args(0): ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi} = [ $c->stash->{file} ];
}

=head2 get_ids

=head3 URL

C</api/file/get_ids>

=cut

sub get_ids : Chained('') : PathPart('api/file/get_ids') : Args(0) : DB('RO') : ZAPI {
    my ($self, $c) = @_;
    $c->assert_post();

    my $uuids = $c->req->json_parameters->{"file_uuids"};
    if (!ref($uuids) or ref($uuids) ne 'ARRAY' or notall { UUID->check($_) } @$uuids ) {
        throw(
            'file/get_ids/invalid_input',
            'Invalid input. This API call requires a list of UUIDs in `file_uuids`'
        );
    }

    my @file_ids = map {
        $_->uuid => $_->id
    } $c->model('DB::File')->search(
        {
            'me.uuid' => $uuids,

            'me.destroyed'      => 0,
            'me.active_version' => 1,
            '-or' => [
                {
                    'me.accepted'       => 1,
                },
                {
                    'me.accepted'       => 0,
                    'me.date_deleted'   => undef
                }
            ]
        },
    )->all;

    $c->stash->{zapi} = [{@file_ids}];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

