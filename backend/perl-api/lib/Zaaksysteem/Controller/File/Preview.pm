package Zaaksysteem::Controller::File::Preview;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub preview : Path : Args() {
    my ($self, $c, $file_id) = @_;

    throw ('file/no_file_id_given', 'No file id present in request')
        unless $file_id;

    # current purpose is only files that are not attached to a case.
    $c->stash->{file} = $c->model('DB::File')->search({
        id => $file_id,
        case_id => undef
    })->active->first or throw (
        'file/not_found',
        "Could not find unattached file with id $file_id"
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 preview

TODO: Fix the POD

=cut

