package Zaaksysteem::XML::Generator::Wordaddin;
use Moose;

with 'Zaaksysteem::XML::Generator';

=head1 NAME

Zaaksysteem::XML::Generator::Wordaddin - Definitions for Wordaddin XML templates

=head1 SYNOPSIS

    use Zaaksysteem::XML::Generator::Wordaddin;

    my $generator = Zaaksysteem::XML::Generator::Wordaddin->new(...);

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->wordaddin(
        writer => { hostname => 'foo.bar.nl', }
    );

=head1 METHODS

=head2 name

Short name for this module, for use as the accessor name in
L<Zaaksysteem::XML::Compile::Backend>.

=cut

sub name { return 'wordaddin' }

=head2 path_prefix

Path (under share/xml-templates) to search for . "stuf0310".

=cut

sub path_prefix {
    return 'wordaddin';
}

__PACKAGE__->build_generator_methods();

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
