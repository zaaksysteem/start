package Zaaksysteem::DB::Component::BibiotheekKenmerken::SuperSaaS;
use Moose::Role;

with 'Zaaksysteem::DB::Component::BibliotheekKenmerken::Calendar';

use DateTime::Format::Strptime;
use DateTime::Format::DateParse;

=head2 format_as_string

A case field is updated, and this must be recorded in the logs and displayed as a file.
A QMatic value looks like "day;time;id", where day and time are ISO8601 datetimes.

=cut

sub format_as_string {
    my ($self, $values) = @_;

    return map { $self->format_calendar($_) } @$values;
}

=head2 format_calendar

Re-format a single calendar value, so it's fit for humans.

=cut

sub format_calendar {
    my ($self, $value) = @_;

    my $calendar_value = $self->parse_calendar_value($value);

    return "Afspraak geboekt op: ". $self->format_date($calendar_value->{appointment_start}) .
        ' om ' . $self->format_time($calendar_value->{appointment_start});
}


=head2 format_calendar

Parse a single calendar value, so it's fit for computers.

=cut

sub parse_calendar_value {
    my ($self, $value) = @_;

    my ($app_start, $app_end, $appointmentId) = split /;/, $value;

    return {
        appointment_start => $app_start,
        appointment_end   => $app_end,
        appointment_id    => $appointmentId,
    };
}

# TODO: Implement reject_pip_change_request

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

