package Zaaksysteem::DB::Component::Logging::Case::Update::Zorginstituut;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use JSON;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Update::Zorginstituut - Logging for Zorginstituut JW/WMO messages

=head2 onderwerp

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $type = uc($data->{type});

    if ($data->{send}) {
        return
            "Het $type bericht is succesvol afgeleverd bij de externe partij";
    }

    if (!defined $self->data->{errors}) {
        return
            "Het $type bericht is succesvol verwerkt door de externe partij";
    }
    else {
        return "Het $type bericht bevat fouten";
    }
}

sub _print_error_codes {
    my @codes = @_;
    return join(', ', map { sprintf("%03d", $_) } @codes);
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $type = uc($data->{type});

    my $version = $data->{version} // undef;

    if ($data->{send}) {
        $data->{content}
            = "Het $type bericht is succesvol afgeleverd bij de externe partij";
    }
    elsif (!defined $self->data->{errors}) {
        $data->{content}
            = "Het $type bericht is succesvol verwerkt door de externe partij";
    }
    else {
        my @msg    = ("Het $type bericht had de volgende fouten:");
        my %errors = %{ $self->data->{errors} };
        foreach my $e (keys %errors) {
            my @e;
            # WMO2.0
            if (ref $errors{$e} eq "HASH") {
                $version //= '2.0';
                foreach (sort keys %{ $errors{$e} }) {
                    next unless (defined $errors{$e}->{$_});
                    if (ref $errors{$e}->{$_}) {
                        push(@e, "$_: " . join(", ", grep { defined $_ } @{$errors{$e}->{$_}}));
                    }
                    else {
                        push(@e, "$_: $errors{$e}->{$_}");
                    }
                }
                push(@msg, "Foutcodes in $e: " . join(", ", @e));
            }
            # WMO/JW 2.1
            else {
                $version //= '2.1';
                push(@msg, "Foutcodes in $e: " . _print_error_codes(@{$errors{$e}}));
            }
        }

        my $url_type;
        my $url_version = $version;
        $url_version =~ s/\./_/;

        if ($type =~ /^WMO/) {
            $url_type = "wmo";
        }
        else {
            $url_type = 'jw';
        }

        my $url = sprintf("https://modellen.istandaarden.nl/%s/i%s%s/index.php/WJ001", $url_type, $url_type, $url_version);
        push(@msg, "Foutcodes opvragen bij $url");
        $data->{content} = join("\n", @msg);
    }

    $data->{expanded} = JSON::false;
    return $data;
};

=head2 event_category

Type type of event category for this logging item.

=cut

sub event_category { 'case-mutation'; }

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
