package Zaaksysteem::DB::Component::Logging::Case::Update::Price;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my $old = $self->data->{old};
    $old =~ s/\./,/;
    my $new = $self->data->{new};
    $new =~ s/\./,/;

    return "Zaakprijs gewijzigd van $old naar $new" if $old;
    return "Zaakprijs ingesteld op $new";

}

sub event_category { 'case-mutation'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
