package Zaaksysteem::DB::Component::Logging::Case::Merge::File::Error;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use JSON;

=head2 onderwerp

    Documenten konden niet worden samengevoegd

=cut

sub onderwerp {
    my $self = shift;
    return "Documenten konden niet worden samengevoegd";
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{content} = $data->{error};
    $data->{expanded} = JSON::false;
    return $data;
};

=head2 event_category

Type type of event category for this logging item.

=cut

sub event_category { 'case-mutation'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020,  Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
