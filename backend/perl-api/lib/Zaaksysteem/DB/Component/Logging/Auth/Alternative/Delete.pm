package Zaaksysteem::DB::Component::Logging::Auth::Alternative::Token;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

sub onderwerp {
    my $self = shift;
    return "Alternatieve authenticatie verwijderd voor " . $self->data->{username};
}

sub event_category { 'system'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
