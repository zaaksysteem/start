package Zaaksysteem::DB::Component::Logging::Folder::Create;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


has folder => (
    is => 'ro',
    lazy => 1,
    builder => '_add_folder_builder'
);

sub _python_subject {
    my $self = shift;
    if ($self->data->{ name }) {
            return sprintf(
                'Folder "%s" toegevoegd',
                $self->data->{ name }
            );
    }
    return;
}

sub onderwerp {
    my $self = shift;

    if (my $subject = $self->_python_subject) {
        return $subject;
    }

    return $self->get_column('onderwerp') unless $self->folder;

    sprintf(
        "Folder '%s' toegevoegd",
        $self->folder->naam,
    );
}

sub _add_magic_attributes {
    return;
}

sub _add_folder_builder {
    my $self = shift;
    return $self->result_source->schema->resultset('BibliotheekCategorie')->find($self->data->{ folder_id });
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
