package Zaaksysteem::DB::Component::ZaaktypeRelatie;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub added_columns {
    return [qw/
        relatie_naam
    /];
}

sub relatie_naam {
    my $self    = shift;

    if (
        $self->relatie_zaaktype_id &&
        $self->relatie_zaaktype_id->zaaktype_node_id &&
        $self->relatie_zaaktype_id->zaaktype_node_id->titel
    ) {
        return $self->relatie_zaaktype_id->zaaktype_node_id->titel;
    }
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 added_columns

TODO: Fix the POD

=cut

=head2 relatie_naam

TODO: Fix the POD

=cut

