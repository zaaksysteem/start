package Zaaksysteem::DB::ResultSet::ZaaktypeArchiveSettings;
use Zaaksysteem::Moose;

use v5.12;

extends qw(
    DBIx::Class::ResultSet
    Zaaksysteem::Zaaktypen::BaseResultSet
);

# We just adhere the interface, but we accept everything here
sub _validate_session {
    my $self = shift;
    return $_[0];
}

around _retrieve_as_session => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);
    return $rv->{1};
};

sub _commit_session {
    my $self = shift;

    my ($node, $element_session_data, $options) = @_;

    delete($element_session_data->{$_}) for (qw(id uuid casetype_node_id));
    for my $multi_field(qw(valuation_glossary subject_glossary retention_period_glossary restriction_use_term_label restriction_use_documents_name restriction_use_description coverage_in_place coverage_in_geo_tmlo classification_source_tmlo description_mdto coverage_in_geo_mdto classification_source_mdto aggregation_glossary name classification_code classification_description classification_source_tmlo classification_date description_tmlo location coverage_in_time_tmlo coverage_in_geo_tmlo user_rights user_rights_description user_rights_date_period confidentiality confidentiality_date_period form_genre  form_publication structure generic_metadata_tmlo additional_metadata archive_maker confidentiality_description coverage_in_time_begin_date coverage_in_time_end_date coverage_in_time_mdto event_actor_name event_glossary event_in_time event_result information_category_glossary related_information_object_glossary restriction_use_code restriction_use_glossary restriction_use_label restriction_use_term_code restriction_use_term_glossary)) {        if (defined($element_session_data->{$multi_field}) && !ref($element_session_data->{$multi_field})) {
            $element_session_data->{$multi_field} = [ $element_session_data->{$multi_field} ];
        }
    }
    my %create_args = map { $_ => $element_session_data->{$_} }
        grep {
            defined $element_session_data->{$_}
                && length $element_session_data->{$_}
        } $self->result_source->columns;


    $create_args{casetype_node_id} = $node->id;

    return $self->create(\%create_args);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
