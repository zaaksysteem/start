package Zaaksysteem::SAML2::RequestedAttribute;
use Moose;

extends 'Net::SAML2::RequestedAttribute';

sub _build_attributes {
  my $self = shift;

  my %attrs = (
    $self->required ? (isRequired => 'true') : (),
    Name => $self->name,
    $self->_has_friendly ? (FriendlyName => $self->friendly_name) : (),
  );
  return %attrs;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
