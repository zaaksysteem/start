package Zaaksysteem::SAML2::Binding::SOAP;

use Zaaksysteem::Moose;

extends 'Net::SAML2::Binding::SOAP';

use BTTW::Tools::UA qw(new_user_agent);

sub build_user_agent {
    my $self = shift;
    return new_user_agent(
        $self->has_cacert ? (ca_cert => $self->cacert ): (),
        client_cert => $self->cert,
        client_key  => $self->key,
    );
}

__PACKAGE__->meta->make_immutable();


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
