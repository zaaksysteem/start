=head1 SAML Manual

This document describes how to setup Zaaksysteem for a SAML supplier.

=head1 Interface configuration

Every SAML systems integration requires two seperate interefaces to be
configured. The Service Provider and Identity Provider interfaces.

=head2 Service Provider Interface

The L<SP|Zaaksysteem::SAML2::SP> definition interface, which describes our
side of the SAML Protocol Exchange.

=head3 Create

Navigate to the interface management interface, and click the '+' to add the
interface. Use a name that would identify us to an outside actor.

=for html <img src="/images/manual/saml/create-sp-interface-example.png" />

=head3 Configure

The interface requires two certificate files, a base URL and an application
name. Optionally you can set a contactperson to by hydrated in our metadata.

=for html <img src="/images/manual/saml/sp-interface-example.png" />

=head3 Certificates

The two required certificate files have some funky requirements, because of
a misbehaving L<Net::SAML2> code. The C<public> key certificate must be a
combined C<public> and C<private> C<PEM> file. The C<private> certificate
must be only the C<private> key in ASCII (Base64) armored C<PEM> hydration.

The certificates can be found in the NTN's C<config> repository. Access to this
repository is restricted to sys-operators due to the sensitive nature of the
stored information. Ask the current sysop for the files if you need them.

=head4 Certificates for SAML Spoofmode

The current implementation of the SAML Spoofmode requires valid certificates,
this limitation should be lifted in the future, but for now, ask for the
C<test.zaaksysteem.nl> certificates.

=head2 Identity Provider Interface

See the manual for the specific implementation you are trying to setup.

=head1 Implementations

Because every supplier of a SAML infrastructure has their own quirks and
peculiarities the major implementers have their individual manual pages
describing the specifics.

=head2 Logius

L<Logius|http://www.logius.nl/producten/toegang/digid/> is our main provider
of DigiD authentication.

See L<Zaaksysteem::SAML2::Manual::Logius> for the specific manual.

=head2 Gemnet

L<Gemnet|http://www.eherkenning.nl/overheden/aansluiten/eherkenningsmakelaars-info/gemnet/>
is our main provider of eHerkenning authentication.

See L<Zaaksysteem::SAML2::Manual::Gemnet> for the specific manual.

=head2 AD FS / Windows

L<AD FS|http://technet.microsoft.com/en-us/library/hh831502.aspx>
is Microsoft's federated authentication/single sign-on service, that links to
the Active Directory. More information about it is available through the above
link.

See L<Zaaksysteem::SAML2::Manual::ADFS> for the specific manual on how to set
up a (lab) server and get it to talk to Zaaksysteem.

=head2 Mintlab spoofmode

Zaaksysteem also implements a way to spoof an authentication with any
previously mentioned Identity Provider through the use of a specific spoof-mode
configuration. See L<Zaaksysteem::SAML2::Manual::Spoofmode> for the specifics.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
