package Zaaksysteem::Search::Object;
use Zaaksysteem::Moose;

with qw (
    Zaaksysteem::Moose::Role::Schema
    Zaaksysteem::Search::Role::Base
);

=head1 NAME

Zaaksysteem::Search::Object - A search model for objects with ZQL

=head1 SYNOPSIS

    use Zaaksysteem::Search::Object;

    my $search = Zaaksysteem::Search::Object->new(
        user => $c->user,
        schema => $c->model('DB')->schema,
        object_model => $c->model('Object'),
        query => 'SELECT {}  FROM case',
    );

    # Be aware that results can either be:
    # 1) An arrayref
    # 2) A DBIx::Class resultset
    my $results = $search->search;

    # Your code here

=head1 DESCRIPTION

A model that was build to move code from fat controllers and making them a
little bit slimmer. Its main aim was to have code that lived in
L<Zaaksysteem::Controller::API::Object> to be reused for using with the
L<Zaaksysteem::Export::Model>.

Perhaps more work is needed to make it really spiffy, abeit the first iteration
seems to do its job.

=cut

use Carp qw(croak confess);
use List::Util qw(any none);

has object_model => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_object_model',
);

sub search_documents {
    my $self = shift;

    my $object_rs = $self->object_model->zql_search($self->query)->rs;

    $object_rs = $object_rs->search_rs(undef,
        { select => 'object_id', group_by => 'object_id' });

    return $self->case_rs->search_rs(
        {
            'me.id' =>
                { -in => $object_rs->get_column('object_id')->as_query },
        },
    );
}

sub search_intake {
    my $self      = shift;
    my $resultset = shift // $self->resultset;

    return $resultset unless $self->check_zql_command_for('case');

    return $resultset unless $self->is_intake;

    my @seeers = ();
    my $mine   = { 'me.status' => ['new'] };
    $mine->{'me.behandelaar_gm_id'} = $self->user->uidnumber;
    push @seeers, $mine;

    my $ou_id = $self->user->primary_group->id;

    my @role_ids = map { $_->id } @{ $self->user->roles };

    push @seeers,
        {
        '-and' => [
            {
                '-and' => [
                    { '-or' => { route_role => \@role_ids } },
                    {
                        '-or' => [
                            { 'route_ou' => $ou_id },
                            { 'route_ou' => undef },
                        ],
                    }
                ],
            },
            { 'behandelaar' => undef },
        ],
        };

    # Special case: divver, they can see all zaken without a complete role.
    if ($self->user->has_legacy_permission('zaak_route_default')) {
        push @seeers, { 'me.route_role' => undef };
    }

    my %where = (
        '-or'                 => \@seeers,
        'me.deleted'          => undef,
        'me.registratiedatum' => { '<' => \'NOW()' },
        'me.status'           => 'new',
    );

    my $in_query = $self->case_rs->search(\%where, { column => 'me.id' })
        ->get_column('id')->as_query;

    return $resultset->search_rs(
        {
            'me.object_class' => 'case',
            'me.object_id'    => { -in => $in_query }
        }
    );
}


sub search_distinct {
    my ($self, $resultset) = @_;
    $resultset //= $self->resultset;

    if (!$self->is_search_distinct) {
        croak("Cannot search distinct on something that isn't distinct");
    }

    my $opts = $self->zql->cmd->dbixify_opts;
    my @results;
    my $tr = sub { my $val = shift; $val =~ s/\$/./g; return $val };
    while (my $object = $resultset->next) {
        push @results,
            {
            count => int($object->get_column('count')),
            map { $tr->($_) => $object->get_column($_) } @{ $opts->{as} }
            };
    }
    return \@results;
}

sub search_select {
    my $self = shift;

    # Make sure intake is special
    my $zql = $self->zql;

    my $rs = $self->search_intake;

    if ($self->check_zql_command_for('case')) {
        $self->blacklisting;

        if (    not $self->is_search_distinct
            and not $self->is_intake)
        {
            $rs = $self->optimize_resultset($rs);
        }

        return $rs unless $self->is_search_distinct;
        return $self->search_distinct;
    }
    else {
        return Zaaksysteem::Object::Iterator->new(
            rs       => $rs,
            inflator => sub { $self->object_model->inflate_from_row(shift) }
        );
    }

}

sub _extract_columns {
    my $self       = shift;
    my $expression = shift;

    my @columns;

    my $expr = $expression;

    my @stack;
    while ($expr) {
        if ($expr->isa('Zaaksysteem::Search::ZQL::Expression::Infix')) {
            $self->log->trace("Walking infix expression: " . $expr);
            if (
                $expr->lterm->isa(
                    'Zaaksysteem::Search::ZQL::Expression::Infix')
                )
            {
                push @stack, $expr->rterm;
                $expr = $expr->lterm;
                next;
            }
            elsif (
                $expr->lterm->isa('Zaaksysteem::Search::ZQL::Literal::Column'))
            {
                push @columns, $expr->lterm->value;
            }
            $expr = $expr->rterm;
        }
        elsif ($expr->isa('Zaaksysteem::Search::ZQL::Expression::Prefix')) {
            $self->log->trace("Walking prefix expression: " . $expr);
            $expr = $expr->term;
        }
        else {
            $self->log->trace("Pop stack: Not an expression: " . $expr);
            $expr = pop @stack;
        }
    }

    return \@columns;
}

sub optimize_resultset {
    my $self = shift;
    my $rs   = shift;

    if (!$self->zql->cmd->where) {
        $self->log->trace("No WHERE clause, nothing to optimize.");
        return $rs;
    }

    if (!$self->check_zql_command_for('case')) {
        $self->log->trace(
            "Not searching for `case` objects, nothing to optimize.");
        return $rs;
    }

    # Ensure index is used if case type id is specified:
    my $columns = $self->_extract_columns($self->zql->cmd->where);

    # Check if "case.casetype.id" is in "where" somewhere.
    # If so, check if "case.status" is in there.
    # If not, add an "AND case.status IN ('all possible statuses')"
    $self->log->trace(dump_terse($columns)) if $self->log->is_trace;

    if (    (any { $_ eq 'case.casetype.id' } @$columns)
        and (none { $_ eq 'case.status' } @$columns))
    {
        my $column = $rs->map_hstore_key('case.status');

        $rs = $rs->search_rs(
            { $column => { -in => ['new', 'open', 'stalled', 'resolved',] } });
    }

    return $rs;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
