package Zaaksysteem::Search::Acl;
use Moose::Role;
use namespace::autoclean;

use BTTW::Tools qw(dump_terse throw sig);
use List::MoreUtils qw[uniq];
use Time::HiRes qw/tv_interval gettimeofday/;
use Zaaksysteem::Types qw(ACLCapability);

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Search::Acl - ACL Functions for Object::Data::ResultSet

=head1 SYNOPSIS

=head1 DESCRIPTION

This Role defines a route from resultset to component, containing the ACL capabilities of an object
without doing an additional query every time we hit a row.

Route:

    ResultSet->search: no changes
      ->_construct_row:
        * Trigger $resultset->_acl_table to get all ACL entries for this user
        * Collect the capabilities belonging to this $row from $resultset->_acl_table
          and insert this in Component->acl_capabilities

=head1 ATTRIBUTES

=head2 _acl_table

The table, hashref, containing all the acl entries for the currently logged in user.

Example:

=cut

has '_acl_table' => (
    is  => 'rw',
    lazy => 1,
    builder => '_load_acl_table'
);

=head2 _load_acl_table

    $self->_load_acl_table

Queries the ObjectAclEntry table, and collects every uuid for the currently loggedin user

=cut

sub _load_acl_table {
    my $self            = shift;

    my $acl_scope_where = $self->_get_acl_query;

    my @tests = map {
        { scope => $_, -or => $acl_scope_where->{ $_ } }
    } keys %{ $acl_scope_where };

    my $acl_table_query = $self->result_source->schema->resultset('ObjectAclEntry')->search_rs(
        { -or           => \@tests },
        {
            select      => [qw/groupname object_uuid scope/, { array_agg => 'distinct capability'}],
            as          => [qw/groupname object_uuid scope capabilities/],
            group_by    => [qw/groupname object_uuid scope/],
        }
    );

    my $acls_for_query = {};

    while (my $acls = $acl_table_query->next) {
        my ($scope, $uuid, $capabilities, $groupname) = map {
            $acls->get_column($_)
        } qw[scope object_uuid capabilities groupname];

        $acls_for_query->{ $scope }{ $uuid }{ $groupname || 'undefined' } = $capabilities;
    }

    return $acls_for_query;
}

=head2 _load_acl_capabilitis_on_row

Loads the acl capabilities from _acl_table on the given row

=cut

sub _load_acl_capabilities_on_row {
    my $self    = shift;
    my $row     = shift;

    if ($self->result_class eq 'DBIx::Class::ResultClass::HashRefInflator') {
        return;
    }

    my $table = $self->_acl_table;
    my $class_uuid = $row->get_column('class_uuid');

    my @capabilities;

    # The loops below could be generalised one more step, combining type and instance
    # deref strategies, but the map/grep/map code becomes unreadable in the process.

    # The inclusion of the 'undefined' group in every loop is by design, as it
    # provides for the meta-group of ACLs that are *always* in effect,
    # ignoring the acl_groupname column of rows. This is used for conceptual
    # 'shared set permissions' that should change for every group.

    if ($class_uuid && $table->{ type }{ $class_uuid }) {
        # Unwrap all matching capabilities in type scope
        push @capabilities, map  { @{ $_ } } # Unwrap aggregate arrayref of capabilities
                            grep { defined } # Ignore undefined groups
                            map  { $table->{ type }{ $class_uuid }{ $_ // '' } } # Deref possible groups
                                 ( 'undefined', $row->acl_groupname );
    }

    my $uuid = $row->get_column('uuid');

    if ($uuid && $table->{ instance }{ $uuid }) {
        # Unwrap all matching capabilities in instance scope
        push @capabilities, map  { @{ $_ } }
                            grep { defined }
                            map  { $table->{ instance }{ $uuid }{ $_ // '' } }
                                 ( 'undefined', $row->acl_groupname );
    }

    $row->acl_capabilities([ uniq @capabilities ]);
}

sub _get_object_matcher {
    my ($self, $scope, $surrounding_query_alias) = @_;

    if ($scope eq 'instance') {
        return ('oae_instance.object_uuid' => { '=' => \"${surrounding_query_alias}.uuid" });
    }
    elsif ($scope eq 'type') {
        return ('oae_type.object_uuid' => { '=' => \"${surrounding_query_alias}.class_uuid" });
    }
    else {
        throw(
            'search/acl/unknown_scope',
            "Unknown scope in ACL expansion: '$scope'"
        );
    }
}

=head2 acl_items

This method returns a HASH reference that L<SQL::Abstract> will interpret as
an 'or' disjunction. It builds this OR query by looking at the current user,
his/her group (=ou) and roles. Each returned item in the OR is a normal
L<SQL::Abstract> condition.

=cut

sub acl_items {
    my ($self, $capability, $surrounding_query_alias) = @_;

    $surrounding_query_alias //= 'me';
    $capability //= 'search';
    ACLCapability->check($capability) or throw(
        'search/acl_items/invalid_capability',
        "Invalid capability: '$capability'",
    );

    my $acl_scope_where = $self->_get_acl_query;

    my $capabilities = $self->_expand_capabilities($capability);

    # Map acl scope names to object_data columns
    my %col_map = (
        instance => 'uuid',
        type     => 'class_uuid',
    );

    my @acl_subqueries;

    for my $scope (sort keys %{ $acl_scope_where }) {
        my %matcher = $self->_get_object_matcher($scope, $surrounding_query_alias);
        my $acl_query = $self->result_source->schema->resultset('ObjectAclEntry')->search_rs(
            {
                scope       => $scope,
                groupname   => [ { '=' => \"${surrounding_query_alias}.acl_groupname" }, undef ],
                capability  => $capabilities,
                %matcher,
                -or         => $acl_scope_where->{ $scope }
            },
            {
                alias => "oae_$scope"
            }
        );

        # This is the meat of our production, this builds a WHERE clause
        # where either uuid or class_uuid is checked to be IN the
        # object_uuid column of a scoped and grouped ACL subquery result.
        my $acl_subquery = {
            -exists => $acl_query->get_column('object_uuid')->as_query
        };

        push @acl_subqueries, $acl_subquery;
    }

    return @acl_subqueries;
}

=head2 _expand_capabilities

Expand a capability to include all more permissive ones.

=cut

sub _expand_capabilities {
    my $self = shift;
    my $capability = shift;

    if ($capability eq 'search') {
        return [qw(search read write manage)];
    }
    elsif($capability eq 'read') {
        return [qw(read write manage)];
    }
    elsif($capability eq 'write') {
        return [qw(write manage)];
    }
    elsif($capability eq 'manage') {
        return [qw(manage)];
    }

    throw(
        'search/acl/unknown_capability',
        "Unknown capability in ACL expansion: '$capability'"
    );
}

=head2 _get_acl_query

Gets the ACL query for the user

=cut

sub _get_acl_query {
    my $self = shift;

    my $user = $self->{attrs}{zaaksysteem}{user};

    if (!$user) {
        $self->log->trace("Implied admin on _get_acl_query");
        return {},
    }

    my @entities;

    my @roles = map { $_->id } @{ $user->roles };
    my @groups = map { $_->id } @{ $user->groups };

    if (!@roles) {
        $self->log->warn("User with ID %d has no roles", $user->id);
    }
    if (!@groups) {
        $self->log->warn("User with ID %d has no groups", $user->id);
    }

    # All possible positions for the current user, which is the cartesian product of
    # groups over roles, a rule may exist for any item in that product.
    for my $group (@groups) {
        for my $role (@roles) {
            push @entities, sprintf('%s|%s', $group, $role);
        }
    }

    $self->log->trace(sprintf("User with ID %d has is in the following entities: %s", $user->id, dump_terse(\@entities)));

    ### Define the two different queries. One for "instance" and one for "type"
    return {
        instance => [
            { entity_type => 'tag',      entity_id => 'all_users' },

            # This unit checks for object-level rules based on the current user's name
            { entity_type => 'user',     entity_id => $user->username },

            # This unit checks for object and object_class level rules based on the current user's group/role combinations
            { entity_type => 'position', entity_id => { -in => \@entities } },

            # This unit checks for object and object_class level rules based on the current user's roles
            { entity_type => 'role',     entity_id => { -in => \@roles } },

            # This unit checks for object and object_class level rules based on the current user's group lineage
            { entity_type => 'group',    entity_id => { -in => \@groups } }
        ],

        type => [
            # Match globally readable objects
            { entity_type => 'tag',      entity_id => 'all_users' },

            # This unit checks for object and object_class level rules based on the current user's group/role combinations
            { entity_type => 'position', entity_id => { -in => \@entities } },

            # This unit checks for object and object_class level rules based on the current user's roles
            { entity_type => 'role',     entity_id => { -in => \@roles } },

            # This unit checks for object and object_class level rules based on the current user's group lineage
            { entity_type => 'group',    entity_id => { -in => \@groups } }
        ]
    };
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
