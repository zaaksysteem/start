package Zaaksysteem::Types::Datastore;
use warnings;
use strict;

use MooseX::Types -declare => [qw(
    DatastoreType
)];

use Zaaksysteem::Constants::Datastore qw(DATASTORE_CLASSES);

=head1 NAME

Zaaksysteem::Types::Datastore - Custom types for Zaaksysteem datastore

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types::Datastore qw(DatastoreType);

    has attr => (
        isa => DatastoreType,
        is => 'ro',
    );

=head1 AVAILABLE TYPES

=head2 DatastoreTypes

=cut

=head2 CustomerType

A type (subtype of Str) that only allows syntactically valid Customer Types

=over

=item NatuurlijkPersoon

=item Bedrijf

=item BagLigplaats

=item BagNummeraanduiding

=item BagOpenbareruimte

=item BagPand

=item BagStandplaats

=item BagVerblijfsobject

=item BagWoonplaats

=back

=cut

subtype DatastoreType, as enum([ @{DATASTORE_CLASSES()} ]),
    message { "'$_' is not a valid datastore type" };

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

