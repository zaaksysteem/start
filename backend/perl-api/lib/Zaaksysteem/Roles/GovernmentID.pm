package Zaaksysteem::Roles::GovernmentID;
use Moose::Role;

#requires qw(schema);
use Carp qw(croak);
use BTTW::Tools qw(elfproef);

=head2 format_government_id

Format a government ID to the correct specs

=cut

sub format_government_id {
    my $self = shift;
    my $id   = shift;
    my $schema = shift // $self->schema;

    if ($schema->country_code eq '5107') {
        # Persoonsnummers are YYYYMMDDXX. Which is 10 long. But applications
        # who did a quick hack store it as a BSN which has nine digits. So they
        # cut of the century. Now 2000 becomes 000 the first digit must be a 9
        # for the century to be 1, otherwise it is 2. We support everyone from
        # the 1700 and upward :D
        my $l = length($id);
        croak "Government ID is not the correct length" if $l <9;
        return $id if $l == 10;
        my $prefix = substr($id, 0, 1) > 6 ? 1 : 2;
        return $prefix . $id;
        return $id;
    }
    else {
        $id = sprintf("%09d", int($id));
        elfproef($id, 1);
        return $id;
    }
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
