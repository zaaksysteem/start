package Zaaksysteem::SBUS::Types::StUF::GenericAdapter;

use Moose;
use Data::Dumper;

use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Types::StUF::XML;

has [qw/app/] => (
    'is'    => 'rw',
);

extends 'Zaaksysteem::SBUS::Types::StUF';

sub prepare_response_parameters {
    my $self                = shift;
    my ($params, $options)  = @_;

    my $stufxml             = $params->{input};

    my $entiteit            = uc(
        $stufxml
        ->{stuurgegevens}
        ->{entiteittype}
    );

    my $xml                 = $stufxml->{body};

    my $stuf_options        = {
        'mutatie_type'          => $stufxml
                                ->{stuurgegevens}
                                ->{kennisgeving}
                                ->{mutatiesoort},
        'entiteit_type'         => $entiteit,
        'traffic_object'        => $options->{traffic_object}
    };

    die('Need at least the stuf_options entiteit_type + mutatie_type')
        unless (
            $stuf_options->{entiteit_type} &&
            $stuf_options->{mutatie_type}
        );

    die('Invalid object') unless (
        $xml->{$entiteit} &&
        UNIVERSAL::isa($xml->{$entiteit}, 'ARRAY') &&
        scalar(@{ $xml->{$entiteit} })
    );

    my ($prs_xml);

    if (
        uc($stuf_options->{mutatie_type}) eq 'W' ||
        uc($stuf_options->{mutatie_type}) eq 'C'
    ) {
        $prs_xml    = $xml->{$entiteit}->[1];
    } else {
        $prs_xml    = $xml->{$entiteit}->[0];
    }

    my $create_params = $self->_stuf_to_params( $prs_xml, $stuf_options );

    $self->_stuf_relaties($prs_xml,$stuf_options,$create_params);

    ### Change options
    $create_params->{mutatie_type}    = $stuf_options->{mutatie_type};

    ### Hand it to database
    #$self->_stuf_to_database($create_params, $stuf_options);
    return $create_params;
}

sub generate_response_return {
    my ($self, $params, $prepared_params, $options, $result) = @_;

    my $stufxml             = $params->{input};

    my $stuurgegevens       = $stufxml->{stuurgegevens};


    $stuurgegevens->{referentienummer}    = $options->{traffic_object}->id;
    $stuurgegevens->{tijdstipBericht}     = $options->{traffic_object}
            ->created->strftime('%Y%m%d%H%M%S00');

    $stuurgegevens->{berichtsoort}        = 'Bv01';

    return {
        'bevestiging'   => {
            stuurgegevens   => $stuurgegevens,
        }
    };
}

sub _convert_stuf_to_hash {
    my ($self, $xml, $mapping) = @_;

    return {
        map { $mapping->{ $_ } => $self->_parse_value($xml->{ $_ }) }
            grep {
                defined($xml->{ $_ }) &&
                $self->_is_stuf_value($xml->{$_})
            } keys %{ $mapping }
    };
}

sub _is_stuf_value {
    my ($self, $value) = @_;

    $value  = $self->_parse_value($value);

    ### NIL value meanse waardeOnbekend
    if ($value && $value eq 'NIL') {
        return undef;
    }

    return 1;
}

sub _parse_value {
    my ($self, $value) = @_;

    ### When hash, try to find out data
    if (
        UNIVERSAL::isa($value, 'HASH') &&
        exists($value->{'_'})
    ) {
        $value = $value->{'_'};
    }

    ### First entry from array
    if (UNIVERSAL::isa($value, 'ARRAY')) {
        $value = shift @{ $value }
    }

    if (blessed($value) && $value->can('bstr')) {
        $value = $value->bstr;
    }

    if ($value && $value eq 'NIL:geenWaarde') {
        return '';
    }


    return $value;
}

sub _convert_stuf_extra_elementen_to_hash {
    my ($self, $xml) = @_;

    return unless UNIVERSAL::isa($xml, 'HASH');
    return unless UNIVERSAL::isa($xml->{seq_extraElement}, 'ARRAY');

    my $rv = {};
    for my $rawelement (@{ $xml->{seq_extraElement} }) {
        my $element = $rawelement->{extraElement};

        my $naam;
        if (
            UNIVERSAL::isa($element, 'HASH') &&
            exists($element->{'_'})
        ) {
            $naam   = $element->{naam};
        }

        next unless $naam;

        my $value   = $self->_parse_value($element);

        $rv->{ $naam } = $value;
    }

    return $rv;

}


sub prepare_request_parameters {
    my $self                = shift;
    my ($params, $options)  = @_;
    my ($request_params);

    if (my $coderef = $self->can($params->{operation})) {
        $request_params = $coderef->(
            $self,
            $params->{input},
            {
                sortering       => 'adr',
                rows            => '10',
            },
            $options
        );

        $options->{dispatch_type} = 'soap';
    }

    ### Hand it to database
    #$self->_stuf_to_database($create_params, $stuf_options);
    return $request_params;
}

sub handle_adr_relations {
    my $self        = shift;
    my $xml         = shift;
    my $prefix      = shift || 'PRS';

    my ($prsadr, $verblijf, $address_type);

    if (ref($prefix)) {
        $prsadr = $prefix;
    } elsif (
        $xml->{$prefix . 'ADRVBL'} &&
        ref($xml->{$prefix . 'ADRVBL'}) &&
        ($verblijf = $xml->{$prefix . 'ADRVBL'}->{ADR})
    ) {
        $prsadr = $verblijf;
        $address_type = 'W';
    } elsif (
        $xml->{$prefix . 'ADRCOR'} &&
        ref($xml->{$prefix . 'ADRCOR'}) &&
        ($verblijf = $xml->{$prefix . 'ADRCOR'}->{ADR})
    ) {
        $prsadr = $verblijf;
        $address_type = 'B';
    } elsif (
        $xml->{$prefix . 'ADR'} &&
        ref($xml->{$prefix . 'ADR'}) &&
        ($verblijf = $xml->{$prefix . 'ADR'}->{ADR})
    ) {
        $prsadr = $verblijf;
    }

    return {} unless $prsadr;

    my $adres_data      = $self->_stuf_prs_adres(
        $prsadr
    );

    $adres_data->{functie_adres} = $address_type if $address_type;

    return $adres_data;
}

sub _stuf_prs_adres {
    my ($self, $adresxml) = @_;

    my $ADRES_MAPPING = STUF_PRS_ADRES_MAPPING;

    unless (ref($adresxml)) {
        return {};
    }

    my $adres_data = $self->_convert_stuf_to_hash(
        $adresxml,
        $ADRES_MAPPING
    );

    my $extra_elementen = $self->_convert_stuf_extra_elementen_to_hash(
        $adresxml->{extraElementen}
    );

    my $mapping         = STUF_PRS_ADRES_MAPPING_EXTRA;

    my $extra   = $self->_convert_stuf_to_hash(
        $extra_elementen,
        $mapping,
    );

    $adres_data->{ $_ }     = $extra->{ $_ }
        for keys %{ $extra };

    ### Special: use officieleStraatnaam as straatnaam, because some systems
    ### will shorten the straatnaam to something like Damstr instead of
    ### Damstraat
    $adres_data->{straatnaam}   = $extra_elementen->{officieleStraatnaam}
        if $extra_elementen->{officieleStraatnaam};

    return $adres_data;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 STUF_PRS_ADRES_MAPPING

TODO: Fix the POD

=cut

=head2 STUF_PRS_ADRES_MAPPING_EXTRA

TODO: Fix the POD

=cut

=head2 generate_response_return

TODO: Fix the POD

=cut

=head2 handle_adr_relations

TODO: Fix the POD

=cut

=head2 prepare_request_parameters

TODO: Fix the POD

=cut

=head2 prepare_response_parameters

TODO: Fix the POD

=cut

