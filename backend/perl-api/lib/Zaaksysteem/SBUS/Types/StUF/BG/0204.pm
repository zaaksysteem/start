

package Zaaksysteem::SBUS::StUF::BG::0204;

use strict;
use warnings;

use Zaaksysteem::Constants;

use Params::Profile;
use Data::Dumper;

use Moose;
use namespace::autoclean;

use constant VERSIE_STUF => '0204';

### Wat is het idee achter deze module? Rare naamgeving en
### de module die het extend bestaat niet. Code uitgecomment
### tot dit opgelost/weggehaald wordt.

# extends 'Zaaksysteem::SBUS::StUF::BG';

# around 'search' => sub {
#     my $orig    = shift;
#     my $self    = shift;
#     my $search  = shift;
#     my $opt     = shift;

#     $opt->{versieStUF}  =
#         $opt->{versieSectormodel} = VERSIE_STUF;

#     $self->$orig($search, $opt, @_);
# };

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
