package Zaaksysteem::Backend::Directory::Component;

use Moose;

use Params::Profile;

use Zaaksysteem::Constants;
use BTTW::Tools;

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Directory::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Directory::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Directory::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::Directory::Component::Exception',
        description => 'Logic error',
        alias       => 'throw_logic_exception',
    },
);

=head2 update_properties

Updates the properties of a Directory entry.

=head3 Arguments

=over

=item name [optional]

The new name of the directory.

=item case_id [optional]

The case this directory belongs to.

=back

=head3 Returns

The updated Directory object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
        /],
        optional => [qw/
            name
            case_id
        /],
        constraint_methods => {
            name => qr/\w+/,
            case_id => qr/^\d+$/,
        },
    }
);

sub update_properties {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(params  => $opts);

    my $valid = $dv->valid;

    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/directory/update_properties/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }

    my $rs = $self->result_source->schema->resultset('Directory');

    my $new = {
        name    => $valid->{name}    || $self->name,
        case_id => $valid->{case_id} || $self->case->id
    };

    # check if name is valid
    die "invalid name given" unless $rs->validate_name($new);


    # Check if name + case are already taken
    if ($rs->find($new)) {
        throw_logic_exception(
            code  => '/directory/update_properties/directory_exists',
            error => sprintf("Directory with name %s and case %d already exists",
                (
                    $valid->{name} || $self->name,
                    $valid->{case_id} || $self->case->id,
                )
            ),
        );
    }

    return $self->update($valid);
}

=head2 delete

Extends the default delete method for Directory to check if there are still
files using this directory. Without this an ugly DBIx::Class exception would get
thrown.

=cut

sub delete {
    my $self = shift;
    my $skip_check = shift;

    return $self->next::method(@_) if $skip_check;

    my @files = $self->files->all;

    if (scalar @files) {
        throw_general_exception(
            code  => '/directory/delete/not_empty',
            error => 'There are still files referencing this directory',
        );
    }

    return $self->next::method(@_);
}

=head2 delete_tree

Delete the directory, and possible child elements.

=cut

# We could also check for the base subject object class, but this interface
# should only be used internally, limit it to employee/user entities for now.
sig delete_tree => 'Zaaksysteem::Object::Types::Subject';

sub delete_tree {
    my $self = shift;
    my $subject = shift;

    my @subdirs = $self->result_source->resultset->search(
        { path => { '@>' => sprintf('{%d}', $self->id) } },
        { prefetch => 'files' }
    )->all;

    my @files = ($self->files, map { $_->files } @subdirs);
    my @directories = ($self, @subdirs);

    $self->result_source->schema->txn_do(sub {
        for my $file (@files) {
            $file->update_properties({
                subject => $subject,
                deleted => 1
            });
        }

        $_->delete(1) for @directories;
    });

    return;
}

define_profile copy_to_case => (
    required => {
        case => 'Zaaksysteem::Model::DB::Zaak',
    },
    optional => {
        parent => 'Zaaksysteem::Backend::Directory::Component',
    }
);

sub copy_to_case {
    my ($self) = shift;
    my $opts = assert_profile({@_})->valid;

    my $case_id = $opts->{case}->id;
    my $path = [];

    if (my $parent = $opts->{parent}) {
        $parent->discard_changes;
        $path = $parent->path;
        push(@$path, $parent->id);
    }

    my $new;
    try {
        $new = $self->copy({
            case_id => $case_id,
            path    => $path,
        });
    }
    catch {
        if ($_ !~ /duplicate key value violates unique constraint "name_case_id"/) {
            die $_;
        }
        $new = $self->search_rs(
            {
                name    => $self->name,
                case_id => $case_id
            }
        )->first;
    };

    return $new;

}

=head2 TO_JSON

Implements the JSON serialization interface for L<JSON/encode_json>.

=cut

sub TO_JSON {
    my $self = shift;

    return { $self->get_columns };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
