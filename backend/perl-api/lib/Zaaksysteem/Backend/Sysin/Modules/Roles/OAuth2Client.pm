package Zaaksysteem::Backend::Sysin::Modules::Roles::OAuth2Client;
use Moose::Role;

use BTTW::Tools;
use BTTW::Tools::UA qw(new_user_agent);
use JSON::XS qw(decode_json);

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::OAuth2Client

=head1 SYNOPSIS

Shared code for all integrations that have to do OAuth2 client work.

=head1 DEFINITIONS

=head2 refresh_access_token

Refresh the stored auth token if it is no longer valid.

=cut

sub _access_token_valid {
    my $self = shift;
    my $access_token = shift;
    my $access_token_expiration = shift;

    # Consider a token that has more than a minute of validity left to be
    # valid (not in need of refresh).
    my $max_expiration = time + 60;

    return if not defined $access_token;
    return if not defined $access_token_expiration;
    return if $access_token_expiration < $max_expiration;

    $self->log->debug(
        sprintf("Token is still valid. %d > %d",
            $access_token_expiration,
            $max_expiration,
        )
    );

    return 1;
}

sub refresh_access_token {
    my $self = shift;
    my $interface = shift;

    $self->log->debug("Request to refresh OAuth2 token for interface " . $interface->uuid);

    my $internal_config = $interface->internal_config;
    my $access_token = $internal_config->{access_token};
    my $access_token_expiration = $internal_config->{access_token_expiration};
    my $refresh_token = $internal_config->{refresh_token};

    my $config = $interface->get_interface_config;
    my $tenant_id = $config->{ms_tenant_id};
    my $client_id = $config->{ms_client_id};
    my $client_secret = $config->{ms_client_secret};

    # Only refresh the token if it does not exist, or expires in under 60 seconds.
    if ( $self->_access_token_valid($access_token, $access_token_expiration) ) {
        return $access_token;
    }

    my $token_url = "https://login.microsoft.com/${tenant_id}/oauth2/v2.0/token";

    $self->log->trace("Refreshing token using '$token_url'.");

    my $ua = new_user_agent();
    my $res = $ua->post(
        $token_url,
        {
            client_id => $client_id,
            client_secret => $client_secret,
            grant_type => "refresh_token",
            refresh_token => $refresh_token,
        }
    );

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf("Response: status=%s - content=%s",
                $res->code,
                $res->content,
            )
        );
    }

    if (!$res->is_success || ($res->content_type ne 'application/json')) {
        $self->log->warn("Error retrieving new auth token: " . $res->content);

        $interface->internal_config({
            %$internal_config,
            access_token_failed => 1,
        });
        $interface->update();
        throw("oauth2client", "Unable to get new auth token");
    }

    my $token_data = decode_json($res->content);

    my $new_access_token = $token_data->{access_token};
    my $new_access_token_expiration = time + $token_data->{expires_in};
    my $new_refresh_token = $token_data->{refresh_token};

    $interface->internal_config({
            %$internal_config,
            access_token => $new_access_token,
            access_token_expiration => $new_access_token_expiration,
            refresh_token => $new_refresh_token,
        }
    );
    $interface->update();

    return $new_access_token;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.