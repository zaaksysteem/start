package Zaaksysteem::Backend::Sysin::Modules::Roles::XMLDocuments;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::XMLDocuments - A role for XML
documents handling

=head1 SYNOPSIS

    package Foo;
    use Zaaksysteem::Moose;
    with 'Zaaksysteem::Backend::Sysin::Modules::Roles::XMLDocuments';

    sub foo {
        my $self = shift;
        my $xml = shift;

        ...;

        # strip the document contents from XML
        $self->replace_documents_contents_from_xml($xml);

        ...;
    }

=cut

use XML::LibXML;
use XML::LibXML::XPathContext;

=head1 METHODS

=head2 replace_documents_contents_from_xml

Replace documents from C<//ZKN:inhoud> with a custom text. The replacement text is
optional and defaults to C<Document contents has been removed>

    $self->replace_documents_contents_from_xml($xml, $replacement_text);

=cut

sub replace_documents_contents_from_xml {
    my $self = shift;
    my $xml  = shift;
    my $replacement = shift // 'Document contents has been removed';

    my $dom = XML::LibXML->load_xml(string => $xml);
    my $xpc = XML::LibXML::XPathContext->new($dom);

    $xpc->registerNs('ZKN', 'http://www.egem.nl/StUF/sector/zkn/0310');
    my $nodes = $xpc->findnodes('//ZKN:inhoud');
    $nodes->foreach(
        sub {
            $_->removeChildNodes;
            $_->appendText($replacement);
        }
    );
    return $dom->toString;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
