package Zaaksysteem::Backend::Email::Case;
use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Email';

use List::Util qw(all first);
use Encode qw(decode);
use POSIX qw(strftime);
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use BTTW::Tools;
use List::Util qw(any);

=head2 case

The case from which the email is sent, used for retrieval of magic string
properties.

=cut

has case => (
    is       => 'ro',
    required => 1,
    isa      => 'Zaaksysteem::Schema::Zaak',
);

has '+schema' => (
    required => 0,
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return $self->case->result_source->schema;
    }
);

=head2 additional_ztt_context

Used to provide an extra context for magic string retrieval. ZTT will
also source magic strings from this context if present.

=cut

has additional_ztt_context => (is => 'rw',);

=head2 send_case_notification

Extract emails parameters from notifications and defer to send_from_case

=cut

define_profile send_case_notification => (
    required => [qw/notification recipient/],
    optional => [qw/attachments cc bcc additional_ztt_context/],
    typed    => {
        recipient    => 'Str',
        notification => 'Zaaksysteem::Model::DB::BibliotheekNotificaties'
    }
);

sub send_case_notification {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $notification = $params->{notification};

    return $self->send_from_case(
        {
            recipient      => $params->{recipient},
            subject        => $notification->subject,
            body           => $notification->message,
            attachments    => $params->{attachments},
            sender_address => $notification->sender_address,
            sender         => $notification->sender,
            cc             => $params->{cc},
            bcc            => $params->{bcc},
            log_error      => 1,
            $params->{additional_ztt_context}
            ? (additional_ztt_context => $params->{additional_ztt_context},)
            : (),
        }
    );
}

around sender => sub {
    my $orig = shift;
    my $self = shift;

    my $params = shift;

    $params->{sender_address}
        = $self->replace_magic_strings($params->{sender_address})
        if defined $params->{sender_address};
    $params->{sender} = $self->replace_magic_strings($params->{sender})
        if defined $params->{sender};

    return $self->$orig($params);
};

=head2 send_from_case

Send email based on a slew of case settings.

=cut

define_profile send_from_case => (
    required => [qw/body subject recipient/],
    optional => [
        qw/
            additional_ztt_context
            attachments
            bcc
            cc
            contactmoment
            file_attachments
            sender
            sender_address
            log_error
            sender_subject
            request_id
            recipient_type
            /
    ],
    typed => {
        body           => 'Str',
        subject        => 'Str',
        sender_subject => 'Zaaksysteem::Schema::Subject',
    },
    defaults           => { contactmoment => 'balie', log_error => 1 },
    constraint_methods => {
        recipient => sub {
            my ($dfv, $value) = @_;

            my @emails = split /[;,]/, $value;
            return all { Email::Valid->address($_) } @emails;
        },
    },
);

sub send_from_case {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    my $case   = $self->case;
    my $schema = $self->schema;

    $self->additional_ztt_context($params->{additional_ztt_context});

    my $body    = $self->replace_magic_strings($params->{body});
    my $subject = $self->replace_magic_strings($params->{subject});

    my $interface = $schema->resultset('Interface')
        ->search_active({ module => 'emailconfiguration' })->first;

    my ($id, $regexp, $html_template, $html_images);
    if ($interface) {
        $id     = join("-", $case->id, substr($case->get_column('uuid'), -6));
        $regexp = qr/(\d+-[a-z0-9]{6})/;

        my $config = $interface->get_interface_config;
        if ($config->{rich_email}) {
            my $template_name = $case->html_email_template // '';
            my @templates     = @{ $config->{rich_email_templates} };

            my ($template_to_use)
                = first { $_->{label} eq $template_name } @templates;

            # Fallback: HTML mail enabled and at least one template configured
            # but unknown template found in case (type).
            if (@templates and not defined $template_to_use) {
                $template_to_use = $templates[0];
            }

            if ($template_to_use) {
                $html_template = $template_to_use->{template};
                $html_images   = $template_to_use->{image};
            }
        }
    }


    # Case type documents
    my @attached_files
        = $params->{attachments}
        ? $self->_get_files($params->{attachments})
        : ();

    if ($params->{file_attachments}) {
        my $files = $schema->resultset('File')
            ->search_rs({ id => $params->{file_attachments} });

        # When emailing, always use the latest versions of documents
        for my $file ($files->all) {
            push @attached_files, $file->get_last_version;
        }
    }

    my %args;
    foreach (qw(cc bcc recipient sender sender_address)) {
      $args{$_} = $params->{$_} if exists $params->{$_};
    }

    my $msg = $self->send_email(
        body        => $body,
        subject     => $subject,
        attachments => \@attached_files,
        %args,
        $interface
        ? (
            interface         => $interface,
            identifier        => $id,
            identifier_regex => $regexp,
            html_template     => $html_template,
            html_image        => $html_images,
            )
        : (),
    );

    unless ($msg) {
      $self->log->info("We didn't send an email, this is unusual");
      return;
    }

    my $msg_subject = _get_mime_header($msg, 'subject');
    my $from = _get_mime_header($msg, 'from');

    ($from) = Email::Address->parse($from);

    my @mail_addresses = map { lc($_->address) } Email::Address->parse($from),
        Email::Address->parse($params->{recipient}),
        Email::Address->parse($params->{cc}),
        Email::Address->parse($params->{bcc});
    my $logmsg = "All e-mail addresses: " . join(", ", @mail_addresses);

    my @mail_recipient_address = map { lc($_->address) } Email::Address->parse($params->{recipient});
    my $recipient_address = "" . join(", ", @mail_recipient_address);

    my @mail_cc_addresses = map { lc($_->address) } Email::Address->parse($params->{cc});
    my $cc_addresses = "" . join(", ", @mail_cc_addresses);

    my @mail_bcc_addresses = map { lc($_->address) } Email::Address->parse($params->{bcc});
    my $bcc_addresses = "" . join(", ", @mail_bcc_addresses);

    my $requestor = $self->case->aanvrager_object;

    my $args = {
        subject_display_name => $from->phrase || $from->address,
        case_id              => $self->case->id,
        email                => {
            body        => $body,
            subject     => $msg_subject,
            recipient   => [Email::Address->parse($params->{recipient})],
            cc          => [Email::Address->parse($params->{cc})],
            bcc         => [Email::Address->parse($params->{bcc})],
            from        => [$from],
            attachments => [$self->_log_attachments(@attached_files)],
        },
        # The full message source is saved in order to allow the creation of
        # the .eml file (just in case).
        message_source => $msg->stringify,
    };

    $self->schema->txn_do(sub {
        $self->_create_message_thread($args);
    });

    $self->schema->resultset('Logging')->trigger(
        'case/email/created',
        {
            component    => 'case',
            component_id => $self->case->id,
            zaak_id      => $self->case->id,
            data         => {
                case_id      => $self->case->id,
                content      => "",
                message_type => "E-mail",
                subject      => "$msg_subject",
                from         => "$from",
                recipient    => $recipient_address,
                cc           => $cc_addresses,
                bcc          => $bcc_addresses,
                attachments  => undef,
                message_date => strftime('%Y-%m-%dT%H:%M:%SZ', gmtime)
,
            }
        }
    );

    return;
}

sub _log_or_throw {
    my $self   = shift;
    my $params = shift;

    $self->log->error("Error while sending mail: $params->{message}");
    if ($params->{log_error}) {
        my $logging = $self->schema->resultset('Logging');
        $logging->trigger(
            'case/email',
            {
                component    => 'case',
                component_id => $self->case->id,
                zaak_id      => $self->case->id,
                data         => {
                    destination => "mail",
                    subject     => $params->{subject},
                    message     => $params->{message},
                    error       => 1,
                }
            }
        );
        return;
    }
    throw("case/email/$params->{type}", $params->{message});
}

=head2 replace_magic_strings

Interpolate magic strings like [[this_is_a_magic_string]]
in the given string, using information from the case.

=cut

sub replace_magic_strings {
    my ($self, $body) = @_;

    throw 'email/replace_magic_strings/empty_body', 'Geen bericht aangeboden',
        unless defined $body;

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context($self->case);

    $ztt->add_context($self->additional_ztt_context)
        if $self->additional_ztt_context;

    return $ztt->process_template($body)->string;
}

=head2 _log_attachments

Transform a list of attached files into a format that will fit into
what contactmoment_create expects.

=cut

sub _log_attachments {
    my ($self, @attached_files) = @_;

    return map {
        {
            filename       => $_->filename,
            file_id        => $_->id,
            filestore_id   => $_->get_column('filestore_id'),
            email_filename => $_->filename,
        }
    } @attached_files;
}

=head2 _get_files

Retrieve file objects given a list of zaaktype_kenmerken_ids.

=cut

sub _get_files {
    my ($self, $zaaktype_kenmerken_ids) = @_;

    throw('email/get_files',
        'Systeemfout: lijst met zaaktype kenmerk ids verwacht')
        unless ref $zaaktype_kenmerken_ids eq 'ARRAY';

    my $schema = $self->schema;
    my $rs     = $schema->resultset('ZaaktypeKenmerken')->search_rs(
        { 'id' => $zaaktype_kenmerken_ids, },
        {
            columns  => ['bibliotheek_kenmerken_id'],
            distinct => 1,
        },
    );

    my $rv = $schema->resultset('File')->search(
        {
            'case_documents.bibliotheek_kenmerken_id' =>
                { -in => $rs->as_query },
            'me.date_deleted'        => undef,
            'me.accepted'            => 1,
            'me.active_version'      => 1,
            'case_documents.case_id' => $self->case->id,
        },
        { join => { case_documents => 'file_id' }, }
    );

    if ($self->log->is_debug) {
        my $case_id = $self->case->id;
        my @found;
        while (my $file = $rv->next) {
            push(@found, $file);
            $self->log->debug(
                sprintf(
                    "Filename %s with ID %d found for mail attachement in case %d",
                    $file->filename, $file->id, $case_id
                )
            );
        }
        return @found;
    }
    return $rv->all;
}

sub _create_message_thread {
    my $self = shift;
    my $args = shift;

    if (not $args->{case_id}) {
        $self->log->warn("No case ID found. Not saving email in thread list.");
        return;
    }

    my $message_slug = substr($args->{email}{body}, 0, 180);

    my $attachment_count = scalar @{ $args->{email}{attachments} // [] };
    my $thread           = $self->schema->resultset('Thread')->create(
        {
            contact_uuid        => undef,
            contact_displayname => undef,
            case_id             => $args->{case_id},
            thread_type         => 'external',
            last_message_cache  => {
                message_type => 'email',
                slug         => $message_slug,
                subject      => $args->{email}{subject},
                created_name => $args->{subject_display_name},
                created      => strftime('%Y-%m-%dT%H:%M:%SZ', gmtime)
            },
            unread_pip_count      => 0,
            unread_employee_count => 0,
            attachment_count      => $attachment_count,
        }
    );

    my $participants = [];
    for my $role (qw(recipient cc bcc from)) {
        for my $participant (grep { defined } @{ $args->{email}{$role} }) {
            my $participant_role;
            if ($role eq 'recipient') {
                $participant_role = 'to';
            }
            else {
                $participant_role = $role;
            }

            push @$participants,
                {
                role         => $participant_role,
                display_name => $participant->phrase,
                address      => $participant->address,
                };
        }
    }

    $self->log->trace("Participants: " . dump_terse($participants));

    my $message_uuid = generate_uuid_v4();
    my $filestore    = $self->_upload_message_source(
        message_source => $args->{message_source},
        message_uuid   => $message_uuid,
    );

    my $message_ext
        = $self->schema->resultset('ThreadMessageExternal')->create(
        {
            type             => 'email',
            content          => $args->{email}{body},
            subject          => $args->{email}{subject},
            direction        => 'outgoing',
            participants     => $participants,
            source_file_id   => $filestore->id,
            read_pip         => DateTime->now(),
            read_employee    => DateTime->now(),
            attachment_count => $attachment_count,
        }
        );

    my $message = $self->schema->resultset('ThreadMessage')->create(
        {
            thread_id                  => $thread->id,
            thread_message_external_id => $message_ext->id,
            type                       => 'external',
            message_slug               => $message_slug,
            created_by_uuid            => $args->{subject_uuid},
            created_by_displayname     => $args->{subject_display_name},
            uuid                       => $message_uuid,
        }
    );

    my $message_id = $message->id;

    for my $attachment (@{ $args->{email}{attachments} }) {
        $self->schema->resultset('ThreadMessageAttachment')->create(
            {
                filestore_id      => $attachment->{filestore_id},
                filename          => $attachment->{filename},
                thread_message_id => $message_id,
            }
        );
    }

    return;
}

sub _upload_message_source {
    my $self = shift;
    my %args = @_;

    my $temp_file = File::Temp->new();
    print $temp_file $args{message_source};
    $temp_file->close();

    return $self->schema->resultset('Filestore')->filestore_create(
        {
            original_name    => sprintf("message-%s.eml", $args{message_uuid}),
            file_path        => "$temp_file",
            ignore_extension => 1,
        }
    );
}

sub _get_mime_header {
  my $msg   = shift;
  my $thing = shift;
  my $found = decode('MIME-Header', $msg->head->get($thing, 0) // '');
  $found =~ s/\s+$//;
  return $found;
  }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
