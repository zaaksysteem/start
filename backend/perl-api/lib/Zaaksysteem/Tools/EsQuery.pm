package Zaaksysteem::Tools::EsQuery;
use warnings;
use strict;
use autodie;

use Exporter qw(import);

our @EXPORT_OK = qw(
    parse_es_query
);

use BTTW::Tools qw(throw);
use Hash::Fold qw(unflatten);

=head1 NAME

Zaaksysteem::Tools::EsQuery - Custom parser for ES query syntax

=head1 DESCRIPTION

Small helper to parse Es like queries


=head2 parse_es_query

Parse ES Query parameters to a query data structure.

C<query:match:foo=bar> will be parsed to an datastrucure like
C<{query => { match => { foo => bar }}}>

=cut

sub parse_es_query {
    my $params = shift;
    my %query = map { $_ => $params->{ $_ } } grep {
        $_ =~ m[^query\:]
    } keys %{ $params };

    unless (keys %query) {
        throw('general/es_query_fault',
            'Please specify a Elasticsearch query using URL mapped JSON with the "query:" root'
        );
    }
    return unflatten(\%query, { delimiter => ':' });
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
