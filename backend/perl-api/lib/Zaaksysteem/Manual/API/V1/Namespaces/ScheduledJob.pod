=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::ScheduledJob - ScheduledJob API
reference documentation

=head1 NAMESPACE URL

    /api/v1/scheduled_job

=head1 DESCRIPTION

This page documents the endpoints within the C<app> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob> instances.

=head3 C<GET /[scheduled_job:id]>

Returns a L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob>
instance.

=head3 C<POST /create>

Returns a L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob>
instance.

=head3 C<POST /[scheduled_job:id]/update>

Returns a L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob>
instance.

=head3 C<POST /[scheduled_job:id]/delete>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob> instances.

=head2 Legacy endpoints

=head3 C<GET /legacy>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob> instances.

=head3 C<GET /legacy/[scheduled_job:id]>

Returns a L<scheduled_job|Zaaksysteem::Manual::API::V1::Types::ScheduledJob>
instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
