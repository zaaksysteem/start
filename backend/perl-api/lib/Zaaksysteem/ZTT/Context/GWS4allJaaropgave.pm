package Zaaksysteem::ZTT::Context::GWS4allJaaropgave;
use Moose;

extends 'Zaaksysteem::ZTT::Context::GWS4all';

use String::CamelCase qw(decamelize camelize);

=head1 ATTRIBUTES

=head2 jaaropgave

The specific row from the "Jaaropgave" list in the GWS4all data structure.

(There can be more than one in there, but this context only supports one at a
time for the moment).

=cut

has jaaropgave => (
    is  => 'ro',
    isa => 'HashRef',
);

=head1 METHODS

=head2 get_string_fetchers

Return an array of subroutine (references) that return a
L<Zaaksysteem::ZTT::Element> if they are given a recognised tag.

=cut

around get_string_fetchers => sub {
    my $orig = shift;
    my $self = shift;

    my @fetchers = $self->$orig(@_);
    push @fetchers, sub {
        my $tagname = shift->name;
        return if $tagname !~ /^specificatie_(?<tag>.*)$/;

        my $tag = $+{tag};

        my $specificatie = $self->jaaropgave->{SpecificatieJaarOpgave}[0];

        my $value;

        for my $key (qw(
            Regeling
            IndicatieZVW
            CdPremieVolksverzekering
            Dienstjaar
        )) {
            if ($tag eq decamelize($key)) {
                return Zaaksysteem::ZTT::Element->new(value => $specificatie->{ $key });
            }
        }

        for my $key (qw(
            Fiscaalloon
            IngehoudenPremieZVW
            Loonheffing
            OntvangstenFiscaalloon
            OntvangstenIngehoudenPremieZVW
            OntvangstenPremieloon
            OntvangstenVergoedingPremieZVW
            OntvangstenWerkgeversheffingPremieZVW
            VergoedingPremieZVW
            WerkgeversheffingPremieZVW
        )) {
            if ($tag eq decamelize($key)) {
                return Zaaksysteem::ZTT::Element->new(
                    value => $self->_flatten_bedrag( $specificatie->{ $key } )
                );
            }
        }

        if ($tag =~ /(aangifte_periode_(?:van|tot))_(jaar|maand|dag)/) {
            my $source = $1;
            my $part = $2;

            my $date = $specificatie->{ camelize($source) };
            $date =~ /^(?<jaar>[0-9]{4})(?<maand>[0-9]{2})(?<dag>[0-9]{2})$/;

            return Zaaksysteem::ZTT::Element->new(value => $+{$part});
        }

        return;
    };
    push @fetchers, sub {
        my $tagname = shift->name;
        return if $tagname !~ /^inhoudingsplichtige_(?<tag>.*)/;

        my $tag = $+{tag};

        if (exists $self->jaaropgave->{Inhoudingsplichtige}{ camelize($tag) }) {
            return Zaaksysteem::ZTT::Element->new(
                value => $self->jaaropgave->{Inhoudingsplichtige}{ camelize($tag) },
            );
        }

        return;
    };

    return @fetchers;
};

=head2 get_context_iterators

Returns a hashref containing context generators for iterating blocks.

=cut

sub get_context_iterators {
    my $self = shift;

    return {
        loonheffingskorting => sub {
            my @rv;

            my $loonheffingskorting = $self->jaaropgave->{SpecificatieJaarOpgave}[0]{Loonheffingskorting} ;

            for my $korting (@$loonheffingskorting) {
                $korting->{Ingangsdatum} =~ /^(?<jaar>[0-9]{4})(?<maand>[0-9]{2})(?<dag>[0-9]{2})$/;

                push @rv, {
                    code_loonheffingskorting => $korting->{CdLoonheffingskorting},
                    ingangsdatum_jaar  => $+{jaar},
                    ingangsdatum_maand => $+{maand},
                    ingangsdatum_dag   => $+{dag},
                };
            }

            return \@rv;
        },
    };
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
