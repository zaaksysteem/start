package Zaaksysteem::View::CSV;
use warnings;
use strict;

use base 'Catalyst::View::Download::CSV';

sub render {
    my $self    = shift;
    my $c = shift;
    my $args = shift;

    # jw@mintlab, Januari 2014
    # At the time of writing, on the testserver version 0.03 was installed. On my vagrant box
    # there was 0.05. The author has changed the interface (doesn't want the data hashkey
    # anymore). Because it may be difficult to update all the environments I have chosen
    # to defensively try make the code work in all environments.
    #
    # To get rid of this, simply update all the live environments at the same time as
    # releasing a new code version.
    my $version = $Catalyst::View::Download::CSV::VERSION;

    if ($version >= 0.05) {
        $c->stash->{csv} = $c->stash->{csv}->{data};
    }

    return $self->NEXT::render($c, 'template', $args);
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 render

TODO: Fix the POD

=cut
