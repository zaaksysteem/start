package Zaaksysteem::BR::Subject::ResultSet::Company;

use Moose::Role;
use BTTW::Tools;

with qw/Zaaksysteem::BR::Subject::Utils/;

=head1 NAME

Zaaksysteem::BR::Subject::ResultSet::Company - This is a specific bridge role for L<DBIx::Class::ResultSet> classes.

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge within L<DBIx::Class::ResultSet> classes

=head1 METHODS

=head2 search_from_bridge

=cut

sub search_from_bridge {
    my $self            = shift;
    my $params          = shift;
    my $options         = shift || {};

    my $search_params   = $self->map_search_params($params, 'company')->{subject};

    ### First search into addresses
    my $primary_params  = { map { $_ => $self->_bridge_format_for_dbix('company', "subject.$_", $search_params->{$_}) } grep({ ! ref $search_params->{$_} } keys %$search_params) };

    for my $address_key (qw/address_correspondence address_residence/) {
        next unless $search_params->{$address_key};

        my %address_params;
        if ($address_key eq 'address_correspondence') {
            %address_params = map {
                'correspondentie_' . $_ => $self->_bridge_format_for_dbix('company', "subject.$address_key" . '.' . $_, $search_params->{$address_key}->{ $_ })
            } keys %{ $search_params->{$address_key} };
        } elsif ($address_key eq 'address_residence') {
            %address_params = map {
                'vestiging_' . $_ => $self->_bridge_format_for_dbix('company', "subject.$address_key" . '.' . $_, $search_params->{$address_key}->{ $_ })
            } keys %{ $search_params->{$address_key} };
        }

        $primary_params = {
            %$primary_params,
            %address_params
        };
    }

    $primary_params->{'bedrijf.deleted_on'} = undef;

    # Ensure zero-prefixing doesn't break the dossiernummer, vestigingsnummer search
    # but keep "null/undef" passthrough

    if (exists $primary_params->{dossiernummer}) {
        $primary_params->{"NULLIF(bedrijf.dossiernummer,'')::bigint"} =
            defined $primary_params->{dossiernummer}
                ? int(delete $primary_params->{dossiernummer})
                : undef;
    }

    if (exists $primary_params->{vestigingsnummer}) {
        $primary_params->{"bedrijf.vestigingsnummer"} =
            defined $primary_params->{vestigingsnummer}
                ? int($primary_params->{vestigingsnummer})
                : undef;
        delete $primary_params->{vestigingsnummer};
    }

    return $self->search($primary_params, { %$options, alias => 'bedrijf' });
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
