package Zaaksysteem::Auth::JWT;
use Zaaksysteem::Moose;

with qw(Zaaksysteem::Roles::Session);
use BTTW::Tools::UA qw(new_user_agent);
use Crypt::JWT qw(decode_jwt);

=head1 NAME

Zaaksysteem::Auth::JWT - A JWT model for Zaaksysteem

=cut

# 3 minute expire time
has '+expire' => (default => 180);

sub type { return 'jwt'; }

sub _get_kid_keys {
    my $self = shift;
    my $ua   = shift;
    my $uri  = shift;
    my $res = $ua->get($uri);
    if (!$res->is_success) {
        $self->log->info("Unable to get well known from $uri");
        return;
    }
    my $wellknown = JSON::XS::decode_json($res->decoded_content);
    $res = $ua->get($wellknown->{jwks_uri});
    return JSON::XS::decode_json($res->decoded_content) if $res->is_success;
    $self->log->info("Unable to get kid keys from $uri");
}

sub get_keys {
    my $self = shift;
    my @uris = @_;

    my $ua = new_user_agent();
    my @keys;

    foreach my $uri (@uris) {
        my $key = $self->_generate_redis_id($uri);
        my $k = $self->redis->get_or_set_json(
            "kid_keys:$key",
            sub {
                return $self->_get_kid_keys($ua, $uri);
            },
            { expire => 14400 },
        );
        push(@keys, @{$k->{keys}});
    }

    return { keys => \@keys };
}

sub decode_token {
    my $self  = shift;
    my $token = shift;
    my @uris  = @_;

    my $keys = $self->get_keys(@uris);

    my $data;
    try {
        $data = decode_jwt(token => $token, kid_keys => $keys);
    }
    catch {
        $self->log->info("Unable to validate token: $_");
    };

    return unless $data;

    $self->log->trace("Token decrypted: " . dump_terse($data))
        if $self->log->is_trace;

    return $data if $data;
    return;
}

around start_session => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $self->$orig(@_);

    my $key = $self->_generate_redis_id($_[0]);
    my $exp_key = $key =~ s/jwt:/jwt:expire:/r;

    my $expire = $_[2] if $_[2];

    # The logic here is as follows:
    # Create a seperate expire key which equals the expire date of the token
    # based on this all subsequent calls will set the correct expire time and
    # we can never expose a session that has expired

    if ($expire) {
        $self->redis->set($exp_key, $expire);
        $self->redis->expire_at($exp_key, $expire);
    }
    else {
        $expire = $self->redis->get($exp_key);
        $self->redis->expire_at($exp_key, $expire);
    }
    $self->redis->expire_at($key, $expire);

    # TODO: Figure out how to hook into catalyst' session without having
    # catalyst sessions. Setting the expire_at or the expire value of the
    # cookiejar doesn't have the effect we want.

    return $rv;
};


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
