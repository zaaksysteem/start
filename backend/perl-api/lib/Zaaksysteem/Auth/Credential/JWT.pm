package Zaaksysteem::Auth::Credential::JWT;

use Zaaksysteem::Moose;
use JSON::XS;
use List::Util qw(none);

=head1 NAME

Zaaksysteem::Auth::Credential::JWT - Authenticate users using an
OTP/single-use token

=head1 DESCRIPTION

=cut

# Constuctor args list->map transformer
around BUILDARGS => sub {
    my ($orig, $class, $config, $app, $realm) = @_;

    return $class->$orig(
        app    => $app,
        config => $config,
        realm  => $realm
    );
};

=head1 METHODS

=head2 authenticate

Implements L<Catalyst::Plugin::Authentication> interface for authenticating
a user.

=cut

sub authenticate {
    my ($self, $c, $realm, $authinfo) = @_;

    my $token = $c->req->header('Authorization');

    return unless $token =~ /^Bearer \S+/;
    $token =~ s/^Bearer //;

    # If we have validated the token already, reuse the session data
    my $session_exists = $c->model('Auth::JWT')->end_session(
        $token,
        sub {
            my $sid = shift;
            my $old_session_data = $c->get_session_data($sid);
            $c->session($old_session_data);
        }
    );

    if ($session_exists) {
        $c->log->trace("Session exists.. reloading user");
        $c->model("Auth::JWT")->start_session($token, $c->sessionid);

        my $user = $realm->from_session($c, $c->session->{__user});

        return $user;
    }

    my $config = assert_profile($realm->config,
        profile => { required => { interface => 'Str' } })->valid;

    my $rs = $c->model('DB::Interface')
        ->search_active({ module => $config->{interface} });

    while (my $interface = $rs->next) {
        my $user = $self->_validate_token($c, $realm, $interface, $token);
        return $user if $user;
    }
    return;
}

sub _set_user {
    my $self   = shift;
    my $c      = shift;
    my $user   = shift;
    my $schema = $c->model('DB')->schema;

    $schema->default_resultset_attributes->{current_user}
        = $schema->current_user($user);

    $user->log_login_attempt(
        ip      => $c->get_client_ip,
        success => 1,
        method  => 'jwt',
    );

    return;
}

sub _validate_token {
    my $self      = shift;
    my $c         = shift;
    my $realm     = shift;
    my $interface = shift;
    my $token     = shift;

    my $config = $interface->get_interface_config;
    my @uris = (
        sprintf('https://login.microsoftonline.com/%s/.well-known/openid-configuration/',
            $config->{tenant_id}),
        sprintf('https://login.microsoftonline.com/%s/.well-known/openid-configuration/?appid=%s',
            $config->{tenant_id}, $config->{application_id}),
    );
    if ($config->{resource_id}) {
        push @uris, sprintf(
            'https://login.microsoftonline.com/%s/.well-known/openid-configuration/?appid=%s',
            $config->{tenant_id}, $config->{resource_id}
        );
    }

    # If signature validation fails, this returns undef and falls through to the next interface
    # (if any)
    my $data = $c->model("Auth::JWT")->decode_token($token, @uris);
    return unless $data;

    $data->{aud} //= '';
    my @allowed = grep {$_} (
        "api://$config->{application_id}",
        $config->{application_id},
        $config->{resource_id}
    );
    return if none { $data->{aud} eq $_ } @allowed;

    return if exists $data->{scp} && $data->{scp} eq 'user_impersonation'
        && $data->{appid} ne $config->{application_id};

    my $user = $realm->find_user(
        $c,
        {
            source => $interface->id,
            username => $data->{upn}
        }
    );

    if (not $user) {
        my $subject = $c->model('DB::Subject')->search_active({
            username => lc($data->{upn}),
            subject_type => 'employee',
        })->first;

        return if not defined $subject;

        $c->log->debug(sprintf(
            "User entity not found for user %s; creating one",
            $data->{upn},
        ));

        my $user_entity = $subject->create_related(
            'user_entities',
            {
                source_interface_id => $interface->id,
                source_identifier   => lc($data->{upn}),
                active              => 1,
            }
        );

        $user = $realm->find_user(
            $c,
            {
                source => $interface->id,
                username => $data->{upn}
            }
        );
    }

    return unless $user;
    $self->_set_user($c, $user);

    $c->model("Auth::JWT")->start_session($token, $c->sessionid, $data->{exp});

    return $user;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
