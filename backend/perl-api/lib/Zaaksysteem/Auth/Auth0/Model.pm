package Zaaksysteem::Auth::Auth0::Model;

use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Auth::Auth0::Model - Auth0 abstractions

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Crypt::OpenSSL::Random qw(random_bytes);
use Zaaksysteem::Types qw[Store];

=head1 ATTRIBUTES

=head2 redis

A reference to a Zaaksyteem::Redis like Object

=cut

has redis => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Store::Redis',
    required => 1,
);

=head1 METHODS

=head2 create

Creates a new auth0 state value, store it in Redis and returns the state
value.

    my $auth0_authorize_state = $c->model('Auth::Auth0')->create({
        date_expires => DateTime->now->add(minutes => 4),
        session_id => $c->sessionid,
    });

=cut

define_profile create => (
    required => {
        date_expires => 'DateTime',
        session_id => 'Any',
    }
);

sig create => 'HashRef';

sub create {
    my $self = shift;

    my $params = assert_profile(shift)->valid;
    my $session_id = $params->{session_id};

    my $token = unpack('H*', random_bytes(16));

    my $redis_key = "auth0:state:$session_id";
    $self->redis->set($redis_key, $token);
    $self->redis->expire_at($redis_key, $params->{date_expires}->epoch);

    return $token;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017-2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
