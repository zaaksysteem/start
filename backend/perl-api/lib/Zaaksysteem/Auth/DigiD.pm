package Zaaksysteem::Auth::DigiD;
use Zaaksysteem::Moose;
use XML::LibXML::XPathContext;
use XML::LibXML;

with qw(Zaaksysteem::Roles::Session);

=head1 NAME

Zaaksysteem::Auth::DigiD - A DigiD model for Zaaksysteem

=cut

sub type {
    return 'digid';
}

sub _get_xpath {
    my ($self, $xml) = @_;

    my $xp  = XML::LibXML::XPathContext->new(
        XML::LibXML->load_xml(string => $xml)
    );

    $xp->registerNs('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');
    $xp->registerNs('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');

    return $xp;
}

sub _get_nameid {
    my ($self, $xml) = @_;

    my $xp = $self->_get_xpath($xml);
    my @nodes = $xp->findnodes('//samlp:LogoutRequest/saml:NameID');
    return $nodes[0]->textContent if @nodes;
    return;
}

sub start_federated_session {
    my ($self, $id, $session_id) = @_;
    return $self->start_session($id, $session_id);
}

sub end_federated_session {
    my ($self, $xml) = @_;

    my $name_id = $self->_get_nameid($xml);

    return 0 unless defined $name_id;

    return $self->end_session($name_id);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
