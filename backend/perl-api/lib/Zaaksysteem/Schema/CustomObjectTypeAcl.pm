use utf8;
package Zaaksysteem::Schema::CustomObjectTypeAcl;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CustomObjectTypeAcl

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<custom_object_type_acl>

=cut

__PACKAGE__->table("custom_object_type_acl");
__PACKAGE__->result_source_instance->view_definition(" SELECT authorization_subquery.custom_object_type_id,\n    roles.id AS role_id,\n    groups.id AS group_id,\n    authorization_subquery.\"authorization\"\n   FROM ((( SELECT custom_object_type.id AS custom_object_type_id,\n            (t.role ->> 'uuid'::text) AS role_uuid,\n            (t.department ->> 'uuid'::text) AS group_uuid,\n            t.\"authorization\"\n           FROM (custom_object_type\n             CROSS JOIN LATERAL jsonb_to_recordset((custom_object_type.authorization_definition -> 'authorizations'::text)) t(\"authorization\" text, role json, department json))) authorization_subquery\n     JOIN roles ON (((roles.uuid)::text = authorization_subquery.role_uuid)))\n     JOIN groups ON (((groups.uuid)::text = authorization_subquery.group_uuid)))");

=head1 ACCESSORS

=head2 custom_object_type_id

  data_type: 'integer'
  is_nullable: 1

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 group_id

  data_type: 'integer'
  is_nullable: 1

=head2 authorization

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "custom_object_type_id",
  { data_type => "integer", is_nullable => 1 },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "group_id",
  { data_type => "integer", is_nullable => 1 },
  "authorization",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-12-08 12:57:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:o269GxjrHST0P1d3zR5IbA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
