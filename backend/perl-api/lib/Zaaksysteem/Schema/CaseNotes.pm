use utf8;
package Zaaksysteem::Schema::CaseNotes;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseNotes

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_notes>

=cut

__PACKAGE__->table("case_notes");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'case_notes_id_seq'

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 subject_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_modified

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 value

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "case_notes_id_seq",
  },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "subject_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_modified",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "value",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<case_notes_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("case_notes_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { id => "subject_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2024-03-28 14:10:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Gqt0UpX7wuf+o0BwNkKYbQ

#__PACKAGE__->resultset_class('Zaaksysteem::Backend::Case::Note::Resultset');

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::Case::Note::Component",
    __PACKAGE__->load_components()
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
