use utf8;
package Zaaksysteem::Schema::SavedSearchLabelMapping;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SavedSearchLabelMapping

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<saved_search_label_mapping>

=cut

__PACKAGE__->table("saved_search_label_mapping");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'saved_search_label_mapping_id_seq'

=head2 label_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 saved_search_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "saved_search_label_mapping_id_seq",
  },
  "label_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "saved_search_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 label_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::SavedSearchLabels>

=cut

__PACKAGE__->belongs_to(
  "label_id",
  "Zaaksysteem::Schema::SavedSearchLabels",
  { id => "label_id" },
);

=head2 saved_search_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::SavedSearch>

=cut

__PACKAGE__->belongs_to(
  "saved_search_id",
  "Zaaksysteem::Schema::SavedSearch",
  { id => "saved_search_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-01-09 09:53:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:OTflHyZE4C/l5YmELfVdxw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
