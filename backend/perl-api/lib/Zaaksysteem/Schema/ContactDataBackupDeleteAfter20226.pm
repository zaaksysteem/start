use utf8;
package Zaaksysteem::Schema::ContactDataBackupDeleteAfter20226;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ContactDataBackupDeleteAfter20226

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<contact_data_backup_delete_after_2022_6>

=cut

__PACKAGE__->table("contact_data_backup_delete_after_2022_6");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene_type

  data_type: 'integer'
  is_nullable: 1

=head2 mobiel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 telefoonnummer

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 note

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene_type",
  { data_type => "integer", is_nullable => 1 },
  "mobiel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "telefoonnummer",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "note",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-05-17 11:27:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a/l4XfG3k0x/9jbEK2z8BQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
