package Zaaksysteem::Schema::CaseCustomObjectView;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table_class('DBIx::Class::ResultSource::View');
__PACKAGE__->table('case_custom_object');

__PACKAGE__->result_source_instance->deploy_depends_on(
    [
        qw(
            Zaaksysteem::Schema::Zaak
            Zaaksysteem::Schema::ZaaktypeKenmerken
            Zaaksysteem::Schema::CustomObject
            Zaaksysteem::Schema::CustomObjectVersion
            Zaaksysteem::Schema::CustomObjectTypeVersion
            Zaaksysteem::Schema::customObjectType
            Zaaksysteem::Schema::customObjectRelationship
            )
    ]
);

__PACKAGE__->result_source_instance->is_virtual(1);

__PACKAGE__->result_source_instance->view_definition(
    qq{

  SELECT
    cot.uuid type_uuid,
    covt.name type_name,
    z.id case_id,
    ztk.value_mandatory mandatory,
    ztk.zaak_status_id casetype_status_id

  FROM custom_object_version cov

  JOIN custom_object co
  ON co.id = cov.custom_object_id

  JOIN custom_object_relationship cor
  ON cor.custom_object_id = co.id

  JOIN custom_object_type cot
  ON cot.custom_object_type_version_id = cov.custom_object_type_version_id

  JOIN custom_object_type_version covt
  ON cot.id = covt.custom_object_type_id

  JOIN zaaktype_kenmerken ztk
  ON cot.uuid = ztk.custom_object_uuid

  JOIN zaak z
  ON (
    ztk.zaaktype_node_id = z.zaaktype_node_id
    AND
    z.id = cor.related_case_id
  )

}
);

__PACKAGE__->add_columns(
    'type_uuid'          => { data_type => 'UUID' },
    'type_name'          => { data_type => 'text' },
    'case_id'            => { data_type => 'integer' },
    'mandatory'          => { data_type => 'boolean' },
    'casetype_status_id' => { data_type => 'integer' },
);

1;

__END__

=head1 NAME

Zaaksysteem::Schema::CaseObjectView - A view for custom object types in cases

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
