package Zaaksysteem::Moose::Role::LoggingSubject;
use Moose::Role;

requires qw(onderwerp);

# Make onderwerp speedy

around onderwerp => sub {
    my $orig = shift;
    my $self = shift;
    my $data = shift;

    return $self->$orig($data) if defined $data;

    if (defined(my $onderwerp = $self->get_column('onderwerp'))) {
        return $onderwerp;
    }
    return $self->$orig;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
