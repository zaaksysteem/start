package Zaaksysteem::Moose::Role::LoggingPostex;
use Moose::Role;

use DateTime::Format::ISO8601;

sub format_postex_time {

    my $self = shift;
    my $time = shift;

    return '' unless $time;

    my $dt = DateTime::Format::ISO8601->parse_datetime($time);
    $dt->set_time_zone('Europe/Amsterdam');
    return $dt->strftime("%d-%m-%Y %H:%M:%S");

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
