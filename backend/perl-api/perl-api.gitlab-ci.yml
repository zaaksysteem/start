stages:
  - Tag
  - QA
  - Build
  - BuildSecondPhase
  - Verify
  - Deploy
  - Release

.perl_base_variables:
  variables:
    ADD_JOB_ON_CHANGES_OF_1: "backend/perl-api/cpanfile"
    ADD_JOB_ON_CHANGES_OF_2: "backend/perl-api/docker/Dockerfile.perl"
    BACKEND_PATH: "backend/perl-base"
    DOCKERFILE: "backend/perl-api/docker/Dockerfile.perl"

.perl_api_variables:
  variables:
    BACKEND_PATH: "backend/perl-api"
    DOCKERFILE: "backend/perl-api/docker/Dockerfile.backend"
    IMAGE: "${CI_REGISTRY_IMAGE}/backend/perl-base:${VERSION}"

###########
# Tag
perl-api:Base layer:Tag latest when there are no changes:
  extends:
    - .tag_latest_backend_container_template
  variables:
    BACKEND_PATH: "backend/perl-base"
  only:
    refs:
      - tags
      - merge_requests
      - /^(master|production|preprod|development)$/
    variables:
      - $CI_COMMIT_TAG =~ /^release\//
      - $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production|preprod)$/
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/perl-api/cpanfile"
      - "backend/perl-api/docker/Dockerfile.perl"

perl-api:Tag latest container when there are no changes:
  extends:
    - .perl_api_variables
    - .tag_latest_backend_container_template
  only:
    variables:
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/perl-api/**/*"

###########
# Lint
perl-api:OpenAPI Lint:
  extends:
    - .perl_api_variables
    - .qa_rules
  stage: QA
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  image:
    name: wework/speccy
    entrypoint: [""]
  needs: ["wait-for-other-pipelines-to-finish"]
  script:
    - cd ${BACKEND_PATH}
    - speccy lint share/apidocs/v1/swagger/index.yaml

###########
# Build
.build_perl_base_layer:
  extends:
    - .perl_base_variables
    - .build_and_push_container_image_template
    - .qa_rules

perl-api:Base layer:Build (x86_64):
  extends: .build_perl_base_layer
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

perl-api:Base layer:Build (arm64):
  extends: .build_perl_base_layer
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

perl-api:Base layer:Make multiarch manifest:
  extends:
    - .perl_base_variables
    - .make_multiarch_manifest_template
    - .qa_rules
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "perl-api:Base layer:Build (x86_64)"
    - "perl-api:Base layer:Build (arm64)"

.build_perl_api:
  extends:
    - .perl_api_variables
    - .build_and_push_container_image_template
    - .qa_rules
  stage: BuildSecondPhase
  script:
    - !reference [.build_and_push_container_image_template, script]
    - docker run --rm "${CI_REGISTRY_IMAGE}/${BACKEND_PATH}:${CI_COMMIT_REF_SLUG}-ci-${CI_PIPELINE_IID}-${BUILD_ARCH}" dev-bin/run-ci-tests

perl-api:Build API and test (x86_64):
  extends: .build_perl_api
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

perl-api:Build API and test (arm64):
  extends: .build_perl_api
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

perl-api:Make multiarch manifest:
  extends:
    - .perl_api_variables
    - .make_multiarch_manifest_template
    - .qa_rules
  stage: BuildSecondPhase
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "perl-api:Build API and test (x86_64)"
    - "perl-api:Build API and test (arm64)"

perl-api:Check licenses:
  stage: BuildSecondPhase
  extends:
    - .perl_api_variables
    - .perl_license_check_template
    - .qa_rules
  variables:
    CONTAINER_IMAGE: "${CI_REGISTRY_IMAGE}/${BACKEND_PATH}:${VERSION}"
  needs:
    - "set-version-number-and-tag-commit"
    - "perl-api:Make multiarch manifest"

#
# Release targets
#
perl-api:Tag xcp release container:
  extends:
    - .perl_api_variables    
    - .release_on_xcp
