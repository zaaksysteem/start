#! /bin/bash

if [ ! -e "/opt/filestore/zaaksysteem" ]; then
    echo "First run. Creating filestore for 'zaaksysteem' instance."
    mkdir -p /opt/filestore/zaaksysteem/storage
    chown -R zaaksysteem:zaaksysteem /opt/filestore/zaaksysteem
fi

exec su -c "starman --preload-app --host=0.0.0.0 --port=9083 --workers=${ZS_NUM_THREADS:-3} --workers=5 /opt/zaaksysteem/script/zaaksysteem.psgi"
