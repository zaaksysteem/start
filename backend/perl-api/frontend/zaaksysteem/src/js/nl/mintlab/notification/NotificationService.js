// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.notification').service('notificationService', [
    '$http',
    '$rootScope',
    function ($http, $rootScope) {
      var notificationService = {},
        notifications = [],
        uiEventToCaseTab = {
          'xential/sjabloon': 'docs',
        },
        more;

      function appendNotification(n) {
        notifications.unshift(n);
        $rootScope.$broadcast('notification.add', n);
      }

      function prependNotification(n) {
        notifications.push(n);
        $rootScope.$broadcast('notification.add', n);
      }

      function parseMessage(m) {
        var notification = {
          data: {
            logging_id: m.logging_id,
            message: m.message,
            subject_id: m.subject_id,
            aanvrager: m.aanvrager,
            case_type: m.case_type,
            created_by: m.created_by,
          },
          content: m.message,
          id: m.id,
          is_read: m.is_read,
          time: m.logging_id.timestamp,
          case_tab_destination: notificationService.getCaseTab(m),
        };

        return notification;
      }

      notificationService.getCaseTab = function (m) {
        return uiEventToCaseTab[m.logging_id.event_type] || 'timeline';
      };

      notificationService.getNotifications = function () {
        return notifications;
      };

      notificationService.update = function () {
        $http({
          method: 'GET',
          url: '/api/message/get_for_user',
        }).success(function (response) {
          var messages = response.result,
            notifs = [],
            target;

          more = response.next;

          _.each(messages, function (m) {
            notifs.push(parseMessage(m));
          });

          _.each(notifs, function (n) {
            target = _.find(notifications, function (t) {
              return t.id === n.id;
            });

            if (target) {
              for (var key in n) {
                target[key] = n[key];
              }
            } else {
              prependNotification(n);
            }
          });
        });
      };

      notificationService.getMore = function () {
        if (!more) {
          return null;
        }

        return $http({
          method: 'GET',
          url: more,
        }).success(function (response) {
          var messages = response.result,
            notifs = [],
            target;

          more = response.next;

          _.each(messages, function (m) {
            notifs.push(parseMessage(m));
          });

          _.each(notifs, function (n) {
            target = _.find(notifications, function (t) {
              return t.id === n.id;
            });

            if (target) {
              for (var key in n) {
                target[key] = n[key];
              }
            } else {
              appendNotification(n);
            }
          });
        });
      };

      notificationService.markAsRead = function (notifs) {
        _.each(notifs, function (n) {
          n.is_read = true;

          $rootScope.$broadcast('notification.read', n);

          $http({
            method: 'POST',
            url: '/api/message/mark_as_read/' + n.id,
          }).success(function (response) {
            var data = response.result[0];

            for (var key in data) {
              n[key] = data[key];
            }
          });
        });
      };

      notificationService.markAllAsRead = function () {
        notificationService.markAsRead(
          _.filter(notifications, function (n) {
            return !n.is_read;
          })
        );
      };

      notificationService.getNumUnread = function () {
        var count = 0;
        _.each(notifications, function (n) {
          if (!n.is_read) {
            count++;
          }
        });
        return count;
      };

      notificationService.update();

      return notificationService;
    },
  ]);
})();
