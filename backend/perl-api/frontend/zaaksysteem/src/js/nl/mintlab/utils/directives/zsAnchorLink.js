// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsAnchorLink', [
    '$window',
    function ($window) {
      return {
        link: function (scope, element, attrs) {
          var href = attrs.href,
            anchor = $window.location.pathname + $window.location.search + href;

          element.attr('href', anchor);
        },
      };
    },
  ]);
})();
