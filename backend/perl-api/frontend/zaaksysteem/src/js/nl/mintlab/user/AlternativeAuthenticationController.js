// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.user')
    .controller('nl.mintlab.user.AlternativeAuthenticationController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        $scope.$on('form.submit.success', function (/*event, name, data*/) {
          $scope.$emit('systemMessage', {
            type: 'info',
            content: translationService.get(
              'Uw wijziging zijn succesvol opgeslagen.'
            ),
          });
        });

        $scope.$on('form.submit.error', function (event, name, data) {
          var error = data.result ? data.result[0] : null,
            type = error ? error.type : '',
            errorObj = {
              type: 'error',
            };

          switch (type) {
            case 'auth/alternative/username/exists':
              errorObj.content = translationService.get(
                'De ingevulde gebruikersnaam bestaat al'
              );
              break;

            case 'auth/alternative/invalid/bsn':
              errorObj.content = translationService.get(
                'De betrokkene heeft geen geldige BSN'
              );
              break;

            case 'auth/alternative/no_account':
              errorObj.content = translationService.get(
                'De betrokkene heeft geen alternatieve authenticatie account: Sla de gegevens eerst op.'
              );
              break;

            case 'params/profile':
              errorObj.content = translationService.get(
                'Het ingevulde telefoonnummer of email adres is incorrect invuld'
              );
              break;

            default:
              errorObj.content = translationService.get(
                'Er ging iets fout bij het wijzigen van de instellingen. Probeer het later opnieuw.'
              );
              break;
          }

          $scope.$emit('systemMessage', errorObj);
        });
      },
    ]);
})();
