// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.core.crud.CrudItemActionMenuController', [
      '$scope',
      function ($scope) {
        $scope.behaviour = 'hover';

        $scope.handleOpenMenuClick = function ($event) {
          $scope.setSelection([$scope.item]);
          $event.stopPropagation();
        };

        $scope.$on('zs.ezra.dialog.open', function () {
          $scope.closePopupMenu();
        });
      },
    ]);
})();
