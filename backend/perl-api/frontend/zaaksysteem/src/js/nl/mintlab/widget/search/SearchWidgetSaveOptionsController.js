// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.SearchWidgetSaveOptionsController', [
      '$scope',
      '$http',
      'searchService',
      'formService',
      function ($scope, $http, searchService, formService) {
        $scope.isEditable = function () {
          return searchService.isEditable($scope.activeSearch);
        };

        $scope.saveChanges = function () {
          var search = $scope.activeSearch,
            form = formService.get('searchWidgetForm');

          search.values.values = angular.copy(form.getValues(false));
          search.values.columns = angular.copy($scope.getActiveColumns());
          search.values.zql = $scope.getZql();
          search.values.options.sort = angular.copy($scope.getActiveSort());
          search.values.options.showLocation = $scope.isLocationVisible();
          search.values.objectType = $scope.options.objectType;

          $scope.search.saveFilter($scope.activeSearch);
        };

        $scope.saveAsCopy = function () {
          var from = $scope.getActiveSearchClone(),
            copy = $scope.search.getCopy(from);

          $scope.search.addFilter(copy);
          $scope.setSearch(copy);
        };

        $scope.handleTitleClick = function () {
          $scope.refresh();
        };
      },
    ]);
})();
