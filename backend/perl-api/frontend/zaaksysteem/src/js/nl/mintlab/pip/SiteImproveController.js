// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.SiteImproveController', [
      '$scope',
      function ($scope) {
        $scope.siteImprove = function (id) {
          var sz = document.createElement('script');
          sz.type = 'text/javascript';
          sz.async = true;
          sz.src = '//siteimproveanalytics.com/js/siteanalyze_' + id + '.js';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(sz, s);
        };
      },
    ]);
})();
