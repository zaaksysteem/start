// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.dom.contains', function () {
    return function (parent, element) {
      while (element) {
        element = element.parentNode;
        if (element === parent) {
          return true;
        }
      }
      return false;
    };
  });
})();
