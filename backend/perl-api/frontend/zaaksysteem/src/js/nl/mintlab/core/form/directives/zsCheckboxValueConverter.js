// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('input', [
    function () {
      return {
        restrict: 'E',
        require: ['?ngModel'],
        priority: 1,
        link: function (scope, element, attrs, controllers) {
          var ngModel = controllers[0];

          if (!ngModel || attrs.type !== 'checkbox') {
            return;
          }

          ngModel.$formatters.push(function (val) {
            return val === true || val === '1' || val === 1;
          });
        },
      };
    },
  ]);
})();
