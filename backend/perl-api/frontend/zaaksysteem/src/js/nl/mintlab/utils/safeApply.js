// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.safeApply', function () {
    var noop = angular.noop;

    return function (scope, func) {
      if (!func) {
        func = noop;
      }

      if (!scope.$$phase && !scope.$root.$$phase) {
        return scope.$apply(func);
      } else {
        return func();
      }
    };
  });
})();
