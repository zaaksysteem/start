// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.SearchCaseController', [
      '$scope',
      function ($scope) {
        var inp = document.getElementById('caseSearcher');
        var currentFocus = -1;
        var arr;
        var value;

        $scope.init = function (zaaknummers = '', zaaktypen = '') {
          var caseNumbers = zaaknummers.split(',').filter(Boolean);
          var caseTypes = zaaktypen.split(',').filter(Boolean);
          var cases = [];

          for (i = 0; i < caseNumbers.length; i++) {
            cases.push({
              number: caseNumbers[i],
              type: caseTypes[i],
            });
          }

          cases = cases.sort(function (a, b) {
            if (Number(a.number) > Number(b.number)) {
              return -1;
            } else {
              return 1;
            }
          });

          arr = cases.map(function (caseObj) {
            return caseObj.number + ': ' + caseObj.type;
          });

          inp.addEventListener('keyup', function (e) {
            var itemWrapper;
            var itemContent;
            var i;
            var val = this.value;

            if (val !== value) {
              currentFocus = -1;
              value = val;
            }

            closeAllLists();

            if (!val || val.length < 3) {
              return false;
            }

            itemWrapper = document.createElement('DIV');
            itemWrapper.setAttribute('id', this.id + 'autocomplete-list');
            itemWrapper.setAttribute('class', 'autocomplete-items');

            this.parentNode.appendChild(itemWrapper);

            for (i = 0; i < arr.length; i++) {
              var label = arr[i];
              if (label.toLowerCase().includes(val.toLowerCase())) {
                var caseNumber = label.split(':')[0];
                var href = window.location.origin + '/pip/zaak/' + caseNumber;

                itemContent = document.createElement('a');
                itemContent.href = href;
                itemContent.innerHTML = label;

                itemWrapper.appendChild(itemContent);
              }
            }
          });

          inp.addEventListener('keyup', function (e) {
            var list = document.getElementById(this.id + 'autocomplete-list');

            if (list) items = list.getElementsByTagName('a');

            if (e.key === 'ArrowDown') {
              currentFocus++;
              addActive(items);
            } else if (e.key === 'ArrowUp') {
              currentFocus = currentFocus - 1;
              addActive(items);
            } else if (e.key === 'Enter') {
              e.preventDefault();

              if (currentFocus > -1) {
                if (list) items[currentFocus].click();
              }
            }
          });
        };

        function addActive(items) {
          if (!items) return false;

          removeActive(items);

          if (currentFocus >= items.length) currentFocus = 0;
          if (currentFocus < 0) currentFocus = items.length - 1;

          items[currentFocus].classList.add('autocomplete-active');
        }

        function removeActive(items) {
          for (var i = 0; i < items.length; i++) {
            items[i].classList.remove('autocomplete-active');
          }
        }

        function closeAllLists(element) {
          var items = document.getElementsByClassName('autocomplete-items');

          for (var i = 0; i < items.length; i++) {
            if (element != items[i] && element != inp) {
              items[i].parentNode.removeChild(items[i]);
            }
          }
        }

        document.addEventListener('click', function (e) {
          closeAllLists(e.target);
        });
      },
    ]);
})();
