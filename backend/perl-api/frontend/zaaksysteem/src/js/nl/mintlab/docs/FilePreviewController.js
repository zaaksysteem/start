// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FilePreviewController', [
      '$scope',
      '$window',
      'smartHttp',
      'snackbarService',
      function ($scope, $window, smartHttp, snackbarService) {
        $scope.$on('popupclose', function (/*event, key, value*/) {
          $scope.reloadData();
        });

        $scope.onNameClick = function (event) {
          if (event.type !== 'click' && event.key !== 'Enter') {
            return;
          }

          if ($scope.entity.getEntityType() === 'folder') {
            $scope.onFolderNameClick(event);
          } else {
            $scope.deselectAll();
            $scope.selectEntity($scope.entity);
            let done = false;

            const doPreview = () =>
              fetch(
                '/api/v2/document/preview_document?id=' + $scope.entity.uuid
              )
                .then((res) => {
                  if (res.status === 200) {
                    done = true;
                    $scope.openPopup();
                  } else {
                    return res.json();
                  }
                })
                .then((res) => {
                  if (res && res.errors && res.errors[0]) {
                    if (res.errors[0].code === 'document/preview_not_found') {
                      done = true;
                      snackbarService.error(
                        'Dit document heeft geen pdf voorbeeld.'
                      );
                    } else if (
                      res.errors[0].code === 'document/preview_creating'
                    ) {
                      snackbarService.info(
                        'Voorbeeld wordt gegenereerd, probeer het over enkele seconden nog een keer…'
                      );
                    } else {
                      done = true;
                    }
                  }
                });

            doPreview();
          }

          event.stopPropagation();
        };
      },
    ]);
})();
