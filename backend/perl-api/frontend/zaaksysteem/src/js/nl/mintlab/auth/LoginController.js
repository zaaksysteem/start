// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const extensions = {
  '155': 'png',
  a2: 'svg',
  aaenhunze: 'png',
  amersfoort: 'svg',
  asten: 'png',
  alkmaar: 'jpg',
  avres: 'svg',
  baarn: 'png',
  barneveld: 'svg',
  bbsuo: 'svg',
  beuningen: 'jpg',
  bommelerwaard: 'jpg',
  brabantsedelta: 'jpg',
  bwb: 'png',
  cak: 'svg',
  curacao: 'png',
  deurne: 'svg',
  debilt: 'gif',
  'den-helder': 'svg',
  'de-ronde-venen': 'svg',
  dinkelland: 'svg',
  dijkenwaard: 'svg',
  dongen: 'svg',
  dronten: 'png',
  ecal: 'png',
  ede: 'png',
  epe: 'svg',
  escaperoom: 'jpg',
  'etten-leur': 'png',
  gemeenteberkelland: 'jpg',
  gemeentelangedijk: 'png',
  geschillencommissie: 'jpg',
  'geschillencommissie-consumentenzaken': 'jpg',
  'geschillencommissie-kinderopvang': 'jpg',
  'geschillencommissie-klachtenloket': 'jpg',
  'geschillencommissie-klachtenloket-vastgoedprofessionals': 'jpg',
  'geschillencommissie-klachtenloket-openbaar-vervoer': 'jpg',
  'geschillencommissie-klachtenloket-nivre': 'jpg',
  'geschillencommissie-klachtenloket-ondernemers': 'jpg',
  'geschillencommissie-klachtenloket-zorg': 'jpg',
  'geschillencommissie-zorg': 'jpg',
  gooisemeren: 'svg',
  gouda: 'svg',
  groningenseaports: 'png',
  halderberge: 'png',
  'halte-werk': 'png',
  hardenberg: 'svg',
  hdsr: 'svg',
  heerhugowaard: 'png',
  hellendoorn: 'png',
  heumen: 'jpg',
  heusden: 'jpg',
  hilversum: 'png',
  hofvantwente: 'jpg',
  'hoogheemraadschap-van-rijnland': 'svg',
  'hoogheemraadschap-van-schieland': 'png',
  'huis-voor-klokkenluiders': 'png',
  hulst: 'svg',
  huurcomissie: 'jpg',
  'inspectie-justitie-en-veiligheid': 'svg',
  'kempen-plus': 'png',
  tilburg: 'svg',
  imk: 'png',
  ijsselstein: 'png',
  krimpenerwaard: 'jpg',
  'leidschendam-voorburg': 'svg',
  landsmeer: 'svg',
  langedijkenheerhugowaard: 'png',
  lansingerland: 'png',
  leerdam: 'png',
  lexces: 'svg',
  lingewaard: 'png',
  'lingewaard-oud': 'jpg',
  lochem: 'svg',
  maasdriel: 'jpg',
  maassluis: 'jpg',
  meppel: 'jpeg',
  metropoolregioeindhoven: 'png',
  'midden-delfland': 'png',
  'midden-groningen': 'png',
  'midden-holland': 'png',
  mintlab: 'svg',
  moerdijk: 'svg',
  montfoort: 'svg',
  munitax: 'jpg',
  nieuwkoop: 'png',
  nissewaard: 'svg',
  noaberkracht: 'png',
  noordenveld: 'svg',
  noorderzijlvest: 'svg',
  noordwijk: 'jpg',
  ommen: 'svg',
  'ommen-hardenberg': 'jpg',
  onderwijsgeschillen: 'png',
  'orionis-walcheren': 'svg',
  oss: 'svg',
  'oude-ijsselstreek': 'png',
  overbetuwe: 'svg',
  purmerend: 'svg',
  raadvanstate: 'jpg',
  rivierenland: 'svg',
  rijkswaterstaat: 'svg',
  roosendaal: 'png',
  rozgroep: 'svg',
  'schouwen-duiveland': 'jpg',
  schiedam: 'svg',
  's-bb': 'svg',
  'sed-organisatie': 'svg',
  simpelveld: 'svg',
  smallingerland: 'gif',
  soest: 'png',
  solo: 'jpg',
  'solo-altena': 'png',
  'solo-baanbrekers': 'png',
  'solo-berg-en-dal': 'png',
  'solo-breda': 'png',
  'solo-buren': 'jpg',
  'solo-emmen': 'jpg',
  'solo-geertruidenberg': 'jpg',
  'solo-hulst': 'png',
  'solo-isdbrabantsewal': 'png',
  'solo-katwijk': 'png',
  'solo-kerkrade': 'png',
  'solo-meierijstad': 'jpg',
  'solo-mijngemeentedichtbij': 'png',
  'solo-montferland': 'png',
  'solo-nunspeet': 'png',
  'solo-ooststellingwerf': 'png',
  'solo-optimisd': 'gif',
  'solo-scmer': 'png',
  'solo-senzer': 'png',
  'solo-veenendaal': 'png',
  'solo-weert': 'png',
  steenbergen: 'png',
  stichtsevecht: 'svg',
  stroomopwaarts: 'jpg',
  'sudwest-fryslan': 'png',
  terneuzen: 'svg',
  tholen: 'png',
  tubbergen: 'svg',
  'vallei-veluwe': 'svg',
  vianen: 'png',
  vechtstromen: 'svg',
  velsen: 'svg',
  venlo: 'svg',
  vijfheerenlanden: 'jpg',
  vlaardingen: 'svg',
  vlissingen: 'png',
  voerendaal: 'png',
  voorst: 'svg',
  vrbn: 'svg',
  waalre: 'jpg',
  waalwijk: 'svg',
  waterland: 'svg',
  werkplein: 'svg',
  westland: 'png',
  wetterskipfryslan: 'png',
  wierden: 'svg',
  wijkbijduurstede: 'png',
  winterswijk: 'png',
  waddinxveen: 'svg',
  xxllnc: 'svg',
  zaakstad: 'svg',
  zaaksysteem: 'svg',
  zaffier: 'svg',
  zaltbommel: 'jpg',
  zederik: 'png',
  zeeland: 'svg',
  zeist: 'png',
  zoetermeer: 'png',
  zutphen: 'png',
  'zuid-drecht': 'svg',
  'zuid-holland': 'svg',
};

(function () {
  angular
    .module('Zaaksysteem.auth')
    .controller('nl.mintlab.auth.LoginController', [
      '$scope',
      function ($scope) {
        var passwordVisible = false;
        $scope.passwordVisible = () => passwordVisible;
        $scope.togglePassword = () => (passwordVisible = !passwordVisible);

        var IE = navigator.userAgent.indexOf('MSIE') >= 0;
        $scope.loginAllowed = !IE;

        if (window.top !== window) {
          window.top.location.href = window.location.href;
        }

        if (window.location.origin.includes('.vth')) {
          const footerLink = document.querySelector(
            '.xx-login-footer-appname>a'
          );
          const appTitle = document.querySelector('.xx-app-logo-appname');
          const appLogo = document.querySelector('.xx-app-logo-vignet');
          appTitle.innerText = 'VTH';
          appLogo.innerText = 'Vg';
          appLogo.style.color = 'white';
          appLogo.style.background = '#006450';
          footerLink.innerText = 'VTH is een applicatie van xxllnc';
        }

        // An attempt was made to figure out the extension dynamically,
        // by fetching for each possible extension and seeing which one
        // didn't return an error, then using that one to set the image src.
        // This however causes you to redirect to the image upon logging in.
        //   (the referer known by the backend can't be overwritten by frontend)
        $scope.getImgUrl = (gemeente_id = 'xxllnc') => {
          const extension = extensions[gemeente_id];

          if (gemeente_id === 'custom-styles')
            return '/api/v2/style/get_content?name=logo-login';

          if (!extension) return;

          return `../../form/${gemeente_id}/img/logo.${extension}`;
        };
      },
    ]);
})();
