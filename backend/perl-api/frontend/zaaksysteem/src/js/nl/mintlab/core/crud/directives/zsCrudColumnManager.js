// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsCrudColumnManager', [
    '$parse',
    function ($parse) {
      return {
        require: 'zsCrudTemplateParser',
        link: function (scope, element, attrs, zsCrudTemplateParser) {
          zsCrudTemplateParser.enableColumnManagement();

          scope.getColumns = function (query) {
            return $parse(attrs.zsCrudColumnManager)(scope, {
              $query: query,
            });
          };
        },
      };
    },
  ]);
})();
