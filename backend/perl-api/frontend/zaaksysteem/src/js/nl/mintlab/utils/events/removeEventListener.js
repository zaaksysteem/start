// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  var win = window;

  window.zsDefine('nl.mintlab.utils.events.removeEventListener', function () {
    if (win.removeEventListener) {
      return function (dispatcher, type, listener, useCapture) {
        dispatcher.removeEventListener(type, listener, useCapture);
      };
    } else if (win.attachEvent) {
      return function (dispatcher, type, listener, useCapture) {
        dispatcher.detachEvent(type, listener);
      };
    }

    throw new Error('Events not supported in this browser');
  });
})();
