// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  // Holy Grail inherit function, via Stoyan Stefanov's JavaScript Patterns

  window.zsDefine('nl.mintlab.utils.object.inherit', function () {
    return function (C, P) {
      var F = function () {};
      F.prototype = P.prototype;
      C.prototype = new F();
      C.uber = P.prototype;
      C.prototype.constructor = C;
    };
  });
})();
