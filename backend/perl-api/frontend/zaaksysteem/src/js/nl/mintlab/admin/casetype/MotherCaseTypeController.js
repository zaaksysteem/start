// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.casetype')
    .controller('nl.mintlab.admin.casetype.MotherCaseTypeController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        $scope.toggleCaseType = function () {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/casetype/' + $scope.caseTypeId + '/is_casetype_mother',
              data: {
                is_casetype_mother: $scope.isCaseTypeMother,
              },
            })
            .error(function (response) {
              $scope.$emit('systemMessage', {
                type: 'error',
              });
            });
        };
      },
    ]);
})();
