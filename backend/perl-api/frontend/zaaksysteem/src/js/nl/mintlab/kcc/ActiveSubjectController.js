// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.kcc')
    .controller('nl.mintlab.kcc.ActiveSubjectController', [
      '$scope',
      'subjectService',
      function ($scope, subjectService) {
        $scope.enableSession = function () {
          subjectService.enableSession($scope.identifier);
        };

        $scope.disableSession = function () {
          subjectService.disableSession();
        };
      },
    ]);
})();
