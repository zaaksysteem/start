// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem').directive('zsAccordion', [
    '$window',
    function ($window) {
      var addEventListener = window.zsFetch(
          'nl.mintlab.utils.events.addEventListener'
        ),
        removeEventListener = window.zsFetch(
          'nl.mintlab.utils.events.removeEventListener'
        );

      return {
        compile: function (/*tELement, tAttrs, transclude*/) {
          return function link(scope, element, attrs) {
            var initialized,
              initUnwatch,
              refreshUnwatch,
              destroyUnwatch,
              intervalId;

            function initWidget() {
              var widget = $(element[0]);
              widget.accordion({
                active: 0,
                autoHeight: false,
                fillSpace: true,
              });
              initialized = true;
              setActive();
            }

            function onDocResize() {
              resize();
            }

            function resize() {
              var widget = $(element[0]);
              widget.accordion('resize');
              widget.accordion('refresh');
            }

            function setActive() {
              var widget = $(element[0]),
                active =
                  parseInt(scope.$eval(attrs.zsAccordionActive), 10) || 0;

              widget.accordion({ active: active });
            }

            initUnwatch = scope.$watch(function () {
              if (element.children().length) {
                initUnwatch();
                initUnwatch = null;

                initWidget();
                addListeners();

                intervalId = setInterval(function () {
                  resize();
                }, 1000);
              }
            });

            function addListeners() {
              refreshUnwatch = scope.$watch(function () {
                if (initialized) {
                  resize();
                }
              });

              addEventListener($window, 'resize', onDocResize);
            }

            destroyUnwatch = scope.$on('$destroy', function () {
              if (initUnwatch) {
                initUnwatch();
              }
              if (refreshUnwatch) {
                refreshUnwatch();
              }

              if (intervalId) {
                clearInterval(intervalId);
              }

              removeEventListener($window, 'resize', onDocResize);

              destroyUnwatch();
            });

            if (attrs.zsAccordionActive !== undefined) {
              attrs.$observe('zsAccordionActive', function () {
                if (initialized) {
                  setActive();
                }
              });
            }
          };
        },
      };
    },
  ]);
})();
