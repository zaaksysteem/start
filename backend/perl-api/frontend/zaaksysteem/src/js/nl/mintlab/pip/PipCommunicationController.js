// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.PipCommunicationController', [
      '$scope',
      '$element',
      function ($scope, $element) {
        var fullscreen = false;

        document.body.className =
          (document.body.className || '') + ' communication';

        $scope.isFullscreen = function () {
          return fullscreen;
        };

        $scope.toggleFullscreen = function () {
          fullscreen = !fullscreen;
        };
      },
    ]);
})();
