import { mouseOver } from './mouse';

export const selectByText = ( elementToSearchIn, searchValue, resultElements, expectedResult ) => {
    elementToSearchIn.sendKeys(searchValue);

    resultElements
        .filter(result =>
            result
                .getText()
                .then(text =>
                    text === expectedResult
                )
        )
        .click();
};

export const selectFirst = ( elementToSearchIn, searchValue, resultElements ) => {
    const resultToClick = resultElements.get(0);

    elementToSearchIn.sendKeys(searchValue);

    // dropdowns at the bottom of the page are flipped up when hovered over
    // without this pre-hover the e2e will try to click the unflipped dropdown
    // causing it to flip right before it can click it, resulting in a misclick
    // with this pre-hover the dropdown is already flipped up before e2e clicks
    mouseOver(resultToClick);

    resultToClick.click();
};

export const selectFirstSuggestion = ( elementToSearchIn, searchValue ) => {
    const resultElements = $$('.suggestion-list-item');

    selectFirst(elementToSearchIn, searchValue, resultElements);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
