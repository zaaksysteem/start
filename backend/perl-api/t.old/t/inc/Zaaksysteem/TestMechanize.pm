package Zaaksysteem::TestMechanize;
use Moose;

use Test::More;

extends 'Test::WWW::Mechanize::Catalyst';

with qw/MooseX::Log::Log4perl/;

=head1 NAME

Zaaksysteem::TestMechanize - Test::WWW::Mechanize::Catalyst subclass with some Zaaksysteem sugar

=head1 DESCRIPTION

This class adds a few methods to L<Test::WWW::Mechanize::Catalyst>, for doing
common Zaaksysteem actions like logging in.

=head1 SYNOPSIS

    use Zaaksysteem::TestMechanize;

    my $mech = Zaaksysteem::TestMechanize->new();

    $mech->zs_login();
    $mech->get_ok($mech->zs_url_base . "/my_controller");

=head1 ATTRIBUTES

=head2 logged_in_as

=cut

has 'logged_in_as' => (is => 'rw');

=head1 METHODS

=head2 import

This class wraps "import", so the "Zaaksysteem" application is always selected.

=cut

around 'import' => sub {
    my $orig = shift;
    my $self = shift;
    
    return $self->$orig("Zaaksysteem");
};

=head2 $mech->zs_url_base

Returns the base URL that should be prepended to requested URLs.

=cut

sub zs_url_base {
    return "https://testsuite";
}

=head2 $mech->zs_login(%args)

Logs in on the Zaaksysteem instance.

Accepts C<username> and C<password> arguments, which both default to "admin".

=cut

sub zs_login {
    my $self = shift;
    my %args = @_;

    my $username    = $args{username} // 'admin';

    if ($self->logged_in_as) {
        if ($self->logged_in_as eq $username) {
            return $username;
        }

        $self->get_ok(
            $self->zs_url_base . '/auth/logout',
            'Already logged in as: ' . $self->logged_in_as
            . ': logging out'
        );
    }

    no warnings qw(redefine once);
    $self->get_ok($self->zs_url_base . '/auth/page', "GET / works");

    my $r = $self->submit_form_ok({
            with_fields => {
                username => $username,
                password => $args{password} // 'admin',
                referer  => $self->zs_url_base . '/search',
            },
        },
        'Sent login parameters for username: ' . $username,
    );

    return $self->logged_in_as($username);
}

sub post_json {
    my $self        = shift;
    my $url         = shift;
    my $perldata    = shift;
    my $desc        = shift;

    my $jsono       = JSON->new->utf8->pretty->canonical;

    my $json        = $jsono->encode($perldata);


    $self->post($url, Content => $json, Content_Type => 'application/json');

    ### Keep here for easy copy-paste-manual purposes
    $self->log->trace(
        "Request:\n" . $json . "\n" .
        "Response:\n" . $self->content . "\n"
    );
}

sub post_json_ok {
    my $self                    = shift;
    my ($url,$perldata,$desc)   = @_;

    $self->post_json(@_);

    my $ok          = $self->success;
    $ok             = $self->_maybe_lint( $ok, $desc );

    return $self->_get_json_as_perl($self->content);
}


=head2 get_json

Executes the GET call against mechanize, and returns the json hash as a perl struct

=cut

sub get_json {
    my $self                    = shift;
    my ($url,$desc)   = @_;

    $self->get(@_);

    ### Until we found a way to only print JSON, below is commented
    $self->log->trace(
        "Response:\n" . $self->content . "\n"
    );

    return $self->_get_json_as_perl($self->content);
}

=head2 get_json_ok

Executes the GET call against mechanize, and returns the json hash as a perl struct while checking return value

=cut

sub get_json_ok {
    my $self                    = shift;
    my ($url,$desc) = @_;

    my $perlstruct  = $self->get_json(@_);

    my $ok          = $self->success;
    $ok             = $self->_maybe_lint( $ok, $desc );

    return $perlstruct;
}

=head2 postget_json_ok

Executes the GET call against mechanize, with given post parameters

=cut

sub postget_json {
    my $self                    = shift;
    my ($url,$perldata,$desc)   = @_;

    my $jsono       = JSON->new->utf8->pretty->canonical;

    my $json        = $jsono->encode($perldata);

    $self->get($url, Content => $json, Content_Type => 'application/json');

    $self->log->trace(
        "Request:\n" . $json . "\n" .
        "Response:\n" . $self->content . "\n"
    );
}

sub postget_json_ok {
    my $self                    = shift;
    my ($url,$perldata,$desc)   = @_;

    $self->postget_json(@_);

    my $ok          = $self->success;
    $ok             = $self->_maybe_lint( $ok, $desc );

    return $self->_get_json_as_perl($self->content);
}

sub post_file {
    my $self        = shift;
    my $url         = shift;
    my $file        = shift;
    my $desc        = shift;


    $self->post(
        $url,
        Content => [ upload => [$file] ],
        Content_Type => 'form-data'
    );

    ### Keep here for easy copy-paste-manual purposes
    $self->log->trace(
        "Response:\n" . $self->content . "\n"
    );

    my $ok = $self->success;
    $ok = $self->_maybe_lint( $ok, $desc );
}

=head2 _get_json_as_perl

=over 4

=item Arguments: $STRING_JSON

=item Return Value: $PERL_STRUCTURE (HashRef or ArrayRef)

=back

Returns the given JSON blob as a perl HashRef/ArrayRef

=cut

sub _get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}

sub validate_api_v1_error {
    my ($self, %opts) = @_;

    my $json = $self->content;
    my $data = $self->_get_json_as_perl($json);

    is($self->status,   '500',       'Got valid exception error');
    is($data->{status_code},  '500',       'Got valid status_code');
    is($data->{result}{type}, 'exception', 'Got exception result');

    my $msg = $data->{result}{instance}{message};

    like($msg, $opts{regexp},
        $opts{message} // "validate_api_v1_error found an error: $msg");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
