package TestFor::General::Testsuite::XML;
use base qw(ZSTest);

use TestSetup;

sub xml_compile_classes : Tests {
    # These tests will slow down the testsuite
    # If we do it in test::class the penalty will be less
    my $backend = $zs->xmlcompile;
    isa_ok($backend, "Zaaksysteem::XML::Compile::Backend");

    foreach (qw(0204 0301 0310 0312)) {
        my $method = "stuf_$_";
        if ($zs->can($method)) {
            my $stuf = $zs->$method();
            isa_ok($stuf, "Zaaksysteem::StUF::${_}::Instance");
        }
        else {
            diag "$method is not supported"
        }
    }

    isa_ok($zs->mijnoverheid, "Zaaksysteem::XML::MijnOverheid::Instance");

    isa_ok($zs->xential, "Zaaksysteem::XML::Xential::Instance");
    isa_ok($zs->zaaksysteem, "Zaaksysteem::XML::Zaaksysteem::Instance");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
