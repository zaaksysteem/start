package TestFor::General::Object::Types::Product;

use base 'ZSTest';

use TestSetup;

use JSON;
#use Zaaksysteem::Object::Types::Product;
use Zaaksysteem::Object::Model;

sub setup : Test(startup) {
    my $self = shift;

    $self->{model} = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );
}

sub test_product : Tests {
    my $self = shift;
    my $model = $self->{model};
    if (!$ENV{ZS_DEVELOPER_PRODUCT}) {
        $self->builder->skip("Zaaksysteem::Object::Types::Product are deprecated?");
    }
    return;

    $zs->zs_transaction_ok(sub {
        with_stopped_clock {
            my ($obj, $obj2, $now);
            my $bibcat = $zs->create_bibliotheek_categorie_ok();
            my $casetype = $zs->create_zaaktype_ok();

            $now = DateTime->now();

            $obj = Zaaksysteem::Object::Types::Product->new(
                {
                    name                 => 'foobar',
                    description          => 'quux',
                    approach             => 'now approaching',
                    terms                => 'Termses',
                    price                => 42,
                    internal_explanation => 'EXPLAIN! EXPLAIN!',
                    search_terms         => 'foo bar baz',
                    external_id          => 123,
                    category_id          => $bibcat->id,
                    target               => ['internal'],
                    update_on_import     => 1,
                    related_casetypes    => [
                        {
                            related_object_id    => $casetype->id,
                            related_object_title => $casetype->title,
                            related_object_type  => 'casetype',
                            related_object       => undef,
                        }
                    ],
                }
            );

            $obj2 = Zaaksysteem::Object::Types::Product->new(
                {
                    name                 => 'foobar2',
                    description          => 'quux2',
                    approach             => 'now approaching2',
                    terms                => 'Termses2',
                    price                => 422,
                    internal_explanation => 'EXPLAIN! EXPLAIN!2',
                    search_terms         => 'foo bar baz2',
                    external_id          => 124,
                    category_id          => $bibcat->id,
                    target               => ['internal'],
                    update_on_import     => 1,
                }
            );

            isa_ok(
                $obj,
                'Zaaksysteem::Object::Types::Product',
                'New object instantiated correctly'
            );
            my $json_representation = {
                actions       => [],
                date_created  => $now,
                date_modified => $now,
                id            => undef,
                type          => 'product',
                values        => {
                    approach             => 'now approaching',
                    category_id          => $bibcat->id,
                    description          => 'quux',
                    internal_explanation => 'EXPLAIN! EXPLAIN!',
                    name                 => 'foobar',
                    price                => 42,
                    search_terms         => 'foo bar baz',
                    external_id          => 123,
                    target               => ['internal'],
                    terms                => 'Termses',
                    update_on_import     => \1,
                    related_casetypes    => [
                        {
                            related_object_id    => $casetype->id,
                            related_object_title => $casetype->title,
                            related_object_type  => 'casetype',
                            related_object       => undef,
                        }
                    ],
                },
                security_rules  => [],
                related_objects => [],
                relatable_types => ['product', 'faq'],
            };

            is_deeply $obj->TO_JSON, $json_representation,
                'JSON version of almost-empty object';

            my $saved_obj = $model->save(object => $obj);
            my $object_row = $model->rs->find($saved_obj->id);

            $json_representation->{id} = $saved_obj->id;
            isnt($saved_obj->id, undef, "object_id got defined on save");

            # Actions mutate all over the place depending on the current user
            # and model configuration, chuck it regardless of completeness.
            $saved_obj->actions([]);

            is($object_row->uuid, $saved_obj->id,
                'Object retrieved from the database');

            is_deeply($saved_obj->TO_JSON, $json_representation,
                "JSON representation of saved object didn't change except for the ID."
            );

            $obj2->add_relation(Zaaksysteem::Object::Relation->new(
                related_object_id   => $saved_obj->id,
                related_object_type => $saved_obj->type,
                relationship_name_a => 'mother',
                relationship_name_b => 'child',
            ));

            my $saved_obj2 = $model->save(object => $obj2);
            my $object_row2 = $model->rs->find($saved_obj2->id);

            my $inflated = $model->inflate_from_row($object_row2);

            my $decoded;
            {
                no warnings 'redefine';
                local *DateTime::TO_JSON = sub { $_[0]->iso8601 };

                my $json = to_json($inflated,
                    { allow_blessed => 1, convert_blessed => 1, pretty => 0 });
                $decoded = decode_json($json);
            }

            my $expected_decoded = {
                'actions'         => [],
                'date_created'    => $now,
                'date_modified'   => $now,
                'id'              => $saved_obj2->id,
                'related_objects' => [
                    {
                        'related_object'      => undef,
                        'related_object_id'   => $saved_obj->id,
                        'related_object_type' => 'product',
                        'related_object'      => undef,
                        'relationship_name_a' => 'mother',
                        'relationship_name_b' => 'child'
                    }
                ],
                'relatable_types' => ['product', 'faq'],
                'security_rules' => [],
                'type'   => 'product',
                'values' => {
                    'approach'             => 'now approaching2',
                    'category_id'          => $bibcat->id,
                    'description'          => 'quux2',
                    'internal_explanation' => 'EXPLAIN! EXPLAIN!2',
                    'name'                 => 'foobar2',
                    'price'                => 422,
                    'related_casetypes'    => [],
                    'search_terms'         => 'foo bar baz2',
                    'external_id'          => 124,
                    'target'               => ['internal'],
                    'terms'                => 'Termses2',
                    'update_on_import' =>
                        bless(do { \(my $o = 1) }, 'JSON::XS::Boolean')
                }
            };

            # Fix up composit actions field, content depends on user and model.
            $decoded->{ actions } = [];

            is_deeply($decoded, $expected_decoded,
                'Product with a related object JSONifies correctly.');
        };
    }, 'Saving a product to the database')
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

