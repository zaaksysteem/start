package TestFor::General::Backend::Sysin::Modules::WOZ;

use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::WOZ;

sub zs_b_s_m_woz : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $config = {
                password   => "meuk",
                username   => "meuk",
                key        => "fsdfs",
                account_id => undef,
            };
            my $interface = $zs->create_interface_ok(
                module           => 'woz',
                name             => 'WOZ objecten',
                interface_config => $config,
            );
            is_deeply($interface->get_interface_config, $config, "Config is correct");

        }, "Interface config is correct");
}

1;

__END__

=head1 NAME

TestFor::General::Backend::Sysin::Modules::WOZ - A WOZ interface tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
