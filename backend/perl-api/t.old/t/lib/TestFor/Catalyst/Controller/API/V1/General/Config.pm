package TestFor::Catalyst::Controller::API::V1::General::Config;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::General::Config - Test file for General::Config in our v1 API

=head1 SYNOPSIS

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/General/Config.pm

=head1 DESCRIPTION

This module tests the C</api/v1/general/config> namespace.

=cut

sub api_v1_general_config : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(
        sub {
            my $mech = $zs->mech;
            $mech->zs_login;

            my $item = $schema->resultset('Config')->search({'parameter' => 'allocation_notification_template_id'})->first;
            if (!$item) {
                $item = $schema->resultset('Config')->create({
                    parameter => 'allocation_notification_template_id',
                    value => 31337,
                });
            }

            $item->update({ value => '31337' });

            my $base_url = $mech->zs_url_base . '/api/v1/general/config';

            {
                my $data = $mech->get_json_ok($base_url);
                my $type = {
                    type      => 'config',
                    reference => 'allocation_notification_template_id',
                    instance => {
                        value     => 31337,
                        parameter => 'allocation_notification_template_id',
                    },
                };
                cmp_deeply($data->{result}{instance}{rows}[0], $type, "Configuration is correct");
            }

            {
                $mech->get("$base_url/10001");
                $mech->validate_api_v1_error(
                    regexp => qr/Unable to find configuration item '10001'/,
                );
            }
        },
        'Get all the configuration items',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
