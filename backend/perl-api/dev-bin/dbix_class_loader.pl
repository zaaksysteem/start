#!/usr/bin/env perl
use warnings;
use strict;

use DBIx::Class::Schema::Loader qw(make_schema_at);
use lib qw(lib);

make_schema_at(
    'Zaaksysteem::Schema',
    {
        result_base_class => 'Zaaksysteem::Result',
        debug             => 0,
        use_namespaces    => 0,

        # Enabling this requires some rewrites in Zaaksysteem, stripping most _id calls.
        #naming                  => 'v4',
        overwrite_modifications => 0,
        dump_directory          => './lib',
        components              => [

            #"InflateColumn::DateTime",
            #"TimeStamp"
        ],
    },
    [
        'dbi:Pg:dbname="zs_test"',
        'vagrant',
        '',
    ],
);


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

