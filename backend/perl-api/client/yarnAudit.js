// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const exec = require('child_process').exec;
const proc = exec('yarn audit --json');

const pathToRegexpIsUsedStatically =
  "This vulnerability is caused by 'path-to-regexp' package imported by 'sw-precache'. In certain cases, path-to-regexp will output a regular expression that can be exploited to cause poor performance. This cant happen with how we use it, because we pass a static string to the service worker's write function";

const buildReason =
  'This only affects part of the build script and we dont ship this to the client';

// This is caused by angular, which we are in long process of replacing. Refer to MINTY-57 and MINTY-13187
const angular = {
  prototypePollution:
    'Applied the security fix of AngularJS ourselves, so this security risk no longer applies.',
  // Prototype Pollution fixed as found here: https://github.com/angular/angular.js/commit/add78e62004e80bb1e16ab2dfe224afa8e513bc3
  // Applied by making use of patch-package library
  redos:
    'A regular expression used to split the value of the ng-srcset directive is vulnerable to super-linear runtime due to backtracking. We do not currently use ng-srcset, and will not use it in the future because we are migrating away from AngularJS.',
};

const ignoredCves = [
  { id: 'GHSA-grv7-fg5c-xmjg', reason: buildReason },
  { id: 'GHSA-2p57-rm9w-gvfp', reason: buildReason },
  { id: 'GHSA-cf66-xwfp-gvc4', reason: buildReason },
  { id: 'GHSA-c9f4-xj24-8jqx', reason: buildReason },
  { id: 'GHSA-c7qv-q95q-8v27', reason: buildReason },
  { id: 'GHSA-89mq-4x47-5v83', reason: angular.prototypePollution },
  { id: 'GHSA-4w4v-5hc9-xrr2', reason: angular.redos },
  { id: 'GHSA-2rq5-699j-x7p6', reason: buildReason },
  { id: 'GHSA-67hx-6x53-jw92', reason: buildReason },
  { id: 'GHSA-35jh-r3h4-6jhm', reason: buildReason },
  { id: 'GHSA-pfq8-rq6v-vf5m', reason: buildReason },
  { id: 'GHSA-9wv6-86v2-598j', reason: pathToRegexpIsUsedStatically },
  { id: 'GHSA-rhx6-c78j-4q9w', reason: pathToRegexpIsUsedStatically },
];

let fullMsg = '';
proc.stdout.on('data', (msg) => {
  fullMsg += msg.toString();
});

proc.addListener('close', () => {
  let shouldFail = false;

  fullMsg.split('\n').forEach((msg) => {
    if (msg && msg.trim()) {
      let adv;
      try {
        adv = JSON.parse(msg.trim());
      } catch (err) {
        console.log(err);
        shouldFail = true;
      }
      const id =
        adv &&
        adv.data &&
        adv.data.advisory &&
        adv.data.advisory.github_advisory_id;
      const severity =
        adv && adv.data && adv.data.advisory && adv.data.advisory.severity;
      const cve = ignoredCves.find((cve) => cve.id === id);

      if (cve) {
        console.log('Ignoring ' + cve.id, cve.reason);
        cve.logged = true;
      } else if (
        adv.type !== 'auditSummary' &&
        ['high', 'critical'].includes(severity)
      ) {
        console.log(adv.data.advisory.github_advisory_id);
        shouldFail = true;
      }
    }
  });

  ignoredCves.forEach((cve) => {
    !cve.logged && console.log('No longer flagged ' + cve.id);
  });

  if (shouldFail) {
    throw new Error(
      'New high/critical vulnerabilites were introduced, run `yarn audit` manually for more info'
    );
  }
});
