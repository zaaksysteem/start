// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import controller from './AppointmentControlController';
import template from './template.html';

controller.$inject = [
  '$scope',
  '$http',
  'resource',
  'composedReducer',
  'dateFilter',
  'snackbarService',
];

const appointmentControlDirective = () => ({
  restrict: 'E',
  require: ['vormAppointmentControl', 'ngModel'],
  scope: {
    templateData: '&',
    provider: '&',
  },
  bindToController: true,
  controllerAs: 'vm',
  controller,
  template,
  link: (scope, element, attrs, controllers) => {
    controllers.shift().link(...controllers);
  },
});

export default appointmentControlDirective;
