// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import vormTemplateServiceModule from './../../vormTemplateService';
import vormInvokeModule from './../../vormInvoke';
import assign from 'lodash/assign';

module.exports = angular
  .module('vorm.types.form', [vormTemplateServiceModule, vormInvokeModule])
  .directive('vormFormField', [
    'vormInvoke',
    (vormInvoke) => {
      return {
        restrict: 'E',
        scope: {
          delegate: '&',
          templateData: '&',
          compiler: '&',
        },
        bindToController: true,
        template,
        require: ['vormFormField', 'ngModel', '^ngModelOptions'],
        controller: [
          function () {
            let ctrl = this,
              ngModel,
              ngModelOptions;

            ctrl.link = (controllers) => {
              [ngModel, ngModelOptions] = controllers;
            };

            ctrl.handleChange = (name, value) => {
              ngModel.$setViewValue(
                assign({}, ngModel.$modelValue, { [name]: value }),
                'click'
              );
            };

            ctrl.getFields = () => vormInvoke(ctrl.templateData().fields);

            ctrl.getModelOptions = () => ngModelOptions;
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ])
  .run([
    'vormTemplateService',
    function (vormTemplateService) {
      const el = angular.element(
        '<vorm-form-field data-compiler="vm.compiler()" ng-model></vorm-form-field>'
      );

      vormTemplateService.registerType('form', {
        control: el,
      });
    },
  ]).name;
