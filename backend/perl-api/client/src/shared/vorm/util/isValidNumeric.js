// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const numericExpression = /^-?\d+$/;

/**
 * @param {string} value
 * @return {boolean}
 */
export const isValidNumeric = (value) =>
  typeof value === 'string' &&
  numericExpression.test(value) &&
  value.length <= 18;
