// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default class ZaaksysteemException {
  constructor(type, message) {
    this.type = type;
    this.message = message;
    this.toString = function () {
      return `${this.type}: ${this.message}`;
    };
  }
}
