// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import includes from 'lodash/includes';

/**
 * Determine if a target array contains at
 * least one element of an array of values.
 *
 * @param {Array<string>} haystack
 *   The target array to search in.
 * @param {Array<string>} values
 *   An array of values to search for.
 * @return {boolean}
 */
export const containsOneOf = (haystack, values) =>
  values.some((item) => haystack.indexOf(item) >= 0);

export default ({ user, $state, auxiliaryRouteService }) => {
  const aboutStateName = auxiliaryRouteService.append($state.current, 'about');
  const versionStateName = auxiliaryRouteService.append(
    $state.current,
    'version'
  );
  const isUser = includes(user.capabilities, 'gebruiker');
  const isBehandelaar = isUser && includes(user.capabilities, 'dashboard');

  return [
    {
      name: 'user',
      children: [
        {
          name: 'dashboard',
          label: 'Dashboard',
          href: $state.href('home') || '/intern',
          icon: 'home',
          when: isBehandelaar,
        },
        {
          name: 'communication',
          label: 'Communicatie',
          href: '/main/communication',
          icon: 'message',
          when: isBehandelaar && includes(user.capabilities, 'message_intake'),
        },
        {
          name: 'intake',
          label: 'Documentintake',
          href: '/main/document-intake',
          icon: 'file',
          class: '{ "has-unread": vm.hasNoticesForType(\'document/assign\') } ',
          when:
            isBehandelaar &&
            includes(user.capabilities, 'documenten_intake_subject'),
        },
        {
          name: 'search',
          label: 'Uitgebreid zoeken',
          href: '/search',
          icon: 'magnify',
          when: isBehandelaar && includes(user.capabilities, 'search'),
        },
        {
          name: 'search_contact',
          label: 'Contact zoeken',
          href: '/main/contact-search',
          icon: 'account-search',
          when: isBehandelaar && includes(user.capabilities, 'contact_search'),
        },
        {
          name: 'export',
          label: 'Exportbestanden',
          href: '/main/export-files',
          icon: 'download',
          when: isBehandelaar,
        },
      ],
    },
    {
      name: 'admin',
      children: [
        {
          name: 'library',
          label: 'Catalogus',
          href: '/main/catalog',
          icon: 'puzzle',
          when: includes(user.capabilities, 'beheer_zaaktype_admin'),
        },
        {
          name: 'users',
          label: 'Gebruikers',
          href: '/main/users',
          icon: 'account-multiple',
          when: includes(user.capabilities, 'useradmin'),
        },
        {
          name: 'log',
          label: 'Logboek',
          href: '/main/log',
          icon: 'console',
          when: includes(user.capabilities, 'admin'),
        },
        {
          name: 'transactions',
          label: 'Transactieoverzicht',
          href: '/main/transactions',
          icon: 'swap-vertical',
          when: includes(user.capabilities, 'admin'),
        },
        {
          name: 'interfaces',
          label: 'Koppelingconfiguratie',
          href: '/main/integrations',
          icon: 'link',
          when: includes(user.capabilities, 'admin'),
        },
        {
          name: 'datastore',
          label: 'Gegevensmagazijn',
          href: '/main/datastore',
          icon: 'share-variant',
          when: includes(user.capabilities, 'admin'),
        },
        {
          name: 'config',
          label: 'Configuratie',
          href: '/main/configuration',
          icon: 'settings',
          when: includes(user.capabilities, 'admin'),
        },
      ],
    },
    {
      name: 'info',
      children: [
        {
          name: 'help',
          label: 'Help',
          href: 'https://help.zaaksysteem.nl',
          icon: 'help-circle',
          external: true,
          when: isBehandelaar,
        },
        {
          name: 'info',
          label: 'Over xxllnc Zaken',
          href: $state.href(aboutStateName) || '/intern/!over',
          icon: 'information',
          when: isBehandelaar,
        },
        {
          name: 'version',
          label: 'Release notes',
          href: $state.href(versionStateName) || '/intern/!version',
          icon: 'lightbulb',
          when: isBehandelaar,
        },
      ],
    },
  ];
};
