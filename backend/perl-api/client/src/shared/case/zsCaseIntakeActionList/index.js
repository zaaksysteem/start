// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsTooltipModule from './../../ui/zsTooltip';
import template from './template.html';
import get from 'lodash/get';
import './styles.scss';

module.exports = angular
  .module('zsCaseIntakeActionList', [zsTooltipModule])
  .directive('zsCaseIntakeActionList', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          case: '&',
          user: '&',
          onSelfAssign: '&',
          onReject: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              isAssignedToUser =
                get(ctrl.case(), 'instance.assignee.instance.id') ===
                get(ctrl.user(), 'instance.logged_in_user.id'),
              caseNumber = get(ctrl.case(), 'instance.number'),
              icon = isAssignedToUser ? 'account' : 'account-multiple',
              description = isAssignedToUser
                ? 'Deze zaak is alleen aan u toegewezen'
                : `Deze zaak is toegewezen aan ${get(
                    ctrl.case(),
                    'instance.casetype.instance.department'
                  )}`;

            ctrl.getIcon = () => icon;

            ctrl.getCaseNumber = () => caseNumber;

            ctrl.getDescription = () => description;

            ctrl.handleSelfAssign = (event) => {
              event.stopPropagation();
              event.preventDefault();
              ctrl.onSelfAssign();
            };

            ctrl.handleReject = (event) => {
              event.stopPropagation();
              event.preventDefault();
              ctrl.onReject();
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
