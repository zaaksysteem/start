// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';

const getAssignee = (allocation, user) => {
  return !allocation ||
    allocation.type === 'org-unit' ||
    !get(allocation, 'data.me', true)
    ? null
    : {
        type: 'assignee',
        subject: {
          type: 'subject',
          reference:
            allocation.type === 'me' ? user.uuid : allocation.data.uuid,
        },
        role: 'Behandelaar',
        magic_string_prefix: 'behandelaar',
        send_assignment_confirmation: get(
          allocation,
          'data.informAssignee',
          false
        ),
      };
};

export default getAssignee;
