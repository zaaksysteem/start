// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import map from 'lodash/map';
import identity from 'lodash/identity';
import getRoleValue from '../../../../../intern/views/case/zsCaseAddSubject/getRoleValue';

const typeDictionaryV0 = {
  natuurlijk_persoon: 'person',
  bedrijf: 'company',
  medewerker: 'employee',
};

const typeDictionaryV2 = {
  person: 'person',
  organization: 'company',
  employee: 'employee',
};

const getMagicStringPrefix = (role) => {
  return role.replaceAll(' ', '').toLowerCase();
};

const getRelatedSubjects = (subjectsFromForm, subjectsFromParent) => {
  const formSubjects = map(subjectsFromForm, (subject) => {
    const {
      related_subject,
      pip_authorized,
      notify_subject,
      magic_string_prefix,
      employee_authorisation,
    } = subject;

    return {
      subject: {
        type: typeDictionaryV0[related_subject.type],
        reference: related_subject.data.uuid,
      },
      role: getRoleValue(subject),
      magic_string_prefix,
      pip_authorized,
      send_auth_notification: notify_subject,
      employee_authorisation,
    };
  }).filter(identity);

  const parentSubjects = map(subjectsFromParent, (subject) => {
    const {
      attributes: {
        role,
        authorized,
        permission,
        // magic_string_prefix

        /*
          we explicitly do not use the magic_string_prefix that is returned from the API

          example:
            For a role 'Book author', the magic_string_prefix is 'bookauthor',
            while the magic_string can be 'bookauthor', 'bookauthor1', 'bookauthor2', etc

          the API returns the magic_string of the subject, instead of the magic_string_prefix
          therefore, we convert the role into the magic_string_prefix

          (
            as a sidenote:
            
            we do this because the API requires a magic_string_prefix be given,
            but the backend doesn't use this value either,
            and instead converts the role into the magic_string_prefix
          )
        */
      },
      relationships: {
        subject: {
          data: { id, type },
        },
      },
    } = subject;

    return {
      subject: {
        type: typeDictionaryV2[type],
        reference: id,
      },
      role,
      magic_string_prefix: getMagicStringPrefix(role),
      pip_authorized: authorized,
      send_auth_notification: false, // not returned by API
      employee_authorisation: permission,
    };
  }).filter(identity);

  return [...formSubjects, ...parentSubjects];
};

export default getRelatedSubjects;
