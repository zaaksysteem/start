// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import flatten from 'lodash/flatten';
import parseAttributeValues from '../parseAttributeValues';
import isEmpty from '../../../vorm/util/isEmpty';
import toIsoCalendarDate from '../../../util/date/toIsoCalendarDate';
import { stringToNumber } from '../../../util/number';

const getApiValues = (values, fieldsByName) => {
  return mapValues(parseAttributeValues(fieldsByName, values), (value, key) => {
    let field = fieldsByName[key],
      val = value;

    if (!isArray(val)) {
      val = [val];
    }

    val = flatten(val).filter((v) => !isEmpty(v));

    if (field) {
      const type = field.type || field.$attribute.type;

      val = val.map((v) => {
        switch (type) {
          case 'date':
            return toIsoCalendarDate(v);

          case 'file':
            return v.reference;

          case 'numeric':
          case 'valuta':
          case 'valutaex':
          case 'valutaex21':
          case 'valutaex6':
          case 'valutain':
          case 'valutain21':
          case 'valutain6':
            return stringToNumber(v);

          case 'bag_adres':
          case 'bag_adressen':
          case 'bag_straat_adres':
          case 'bag_straat_adressen':
          case 'bag_openbareruimte':
          case 'bag_openbareruimtes':
            return v.bag_id;

          case 'relationship': {
            return {
              type: 'relationship',
              value: v.id,
              specifics: {
                metadata: {
                  summary: v.label,
                  description: v.description,
                },
                relationship_type: v.type,
              },
            };
          }

          default:
            return v;
        }
      });

      if (field.$attribute && field.$attribute.limit_values > 1) {
        val = [val];
      }

      // Checkboxes need to wrapped in a second array
      // in order to be accepted by API.
      if (type === 'checkbox') {
        val = [val];
      }

      // Limitless relationships need to be wrapped into this husk
      // in order to conform to the v2 standard
      if (type === 'relationship' && field.limit === -1 && val && val[0]) {
        val = [
          {
            type: 'relationship',
            value: val,
            specifics: null,
          },
        ];
      }
    }

    return val;
  });
};

export default getApiValues;
