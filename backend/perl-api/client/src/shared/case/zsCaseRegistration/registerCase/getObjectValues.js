// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import get from 'lodash/get';
import merge from 'lodash/merge';

const getObjectValues = (fieldsByName, customObject, customObjectTypeUuid) => {
  if (!customObject || !customObjectTypeUuid) return;

  const {
    meta: { summary },
    attributes: { subtitle, version_independent_uuid },
  } = customObject;

  const fields = Object.entries(fieldsByName).map(([, entry]) => entry);
  const objectFields = fields.filter(
    (field) =>
      get(field, 'data.queryOptions.objectTypeUuid') === customObjectTypeUuid
  );
  const objectFieldsByName = objectFields.reduce((acc, field) => {
    const name = field.name;
    let value = {
      specifics: {
        metadata: {
          description: subtitle,
          summary,
        },
        relationship_type: 'custom_object',
      },
      type: 'relationship',
      value: version_independent_uuid,
    };

    if (field.limit === -1) {
      value = { specifics: null, type: 'relationship', value: [value] };
    }

    return merge(acc, { [name]: [value] });
  }, {});

  return objectFieldsByName;
};

export default getObjectValues;
