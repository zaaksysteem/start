// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

module.exports = angular
  .module('zsClickOutside', [])
  .directive('zsClickOutside', [
    '$parse',
    '$document',
    ($parse, $document) => {
      return {
        restrict: 'A',
        link: (scope, element, attrs) => {
          let onClickOutside = $parse(attrs.zsClickOutside),
            onClickOutsideEvent = attrs.onClickOutsideEvent || 'mousedown';

          $document.bind(onClickOutsideEvent, (event) => {
            if (
              !(
                element[0] === event.target || element[0].contains(event.target)
              )
            ) {
              let shouldApply = onClickOutside(scope, { $event: event });

              // let the click handler decide if $apply should be called
              if (shouldApply !== false) {
                scope.$apply();
              }
            }
          });
        },
      };
    },
  ]).name;
