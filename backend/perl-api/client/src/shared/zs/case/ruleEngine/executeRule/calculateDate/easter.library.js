// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import easterDefinitions from './easter.definitions';

const easterOffsets = {
  goodfriday: -2,
  easter: 0,
  easter2: 1,
  ascension: 40,
  pentecost: 49,
  pentecost2: 50,
};

const getEaster = (year) => {
  const f = Math.floor;
  // Golden Number - 1
  const G = year % 19;
  const C = f(year / 100);
  // related to Epact
  const H = (C - f(C / 4) - f((8 * C + 13) / 25) + 19 * G + 15) % 30;
  // number of days from 21 March to the Paschal full moon
  const I = H - f(H / 28) * (1 - f(29 / (H + 1)) * f((21 - G) / 11));
  // weekday for the Paschal full moon
  const J = (year + f(year / 4) + I + 2 - C + f(C / 4)) % 7;
  // number of days from 21 March to the Sunday on or before the Paschal full moon
  const L = I - J;
  const month = 3 + f((L + 40) / 44);
  const day = L + 28 - 31 * f(month / 4);

  return new Date(year, month - 1, day);
};

export const getEasterDefinitions = (baseYear, country) => {
  const easter = getEaster(baseYear);

  return easterDefinitions[country].map((definition) => {
    let newDate = new Date(easter);
    const offset = easterOffsets[definition.holiday];

    newDate.setDate(easter.getDate() + offset);

    return {
      month: newDate.getMonth() + 1,
      day: newDate.getDate(),
      institutionTypes: definition.institutionTypes,
    };
  });
};
