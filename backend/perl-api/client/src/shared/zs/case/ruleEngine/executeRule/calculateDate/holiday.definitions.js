// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getEasterDefinitions } from './easter.library';

export default {
  nl: {
    weekend: [6, 7],
    specialHolidays: [(year) => getEasterDefinitions(year, 'nl')],
    holidays: [
      {
        // new year's day
        day: 1,
        month: 1,
      },
      // princess's day (wilhelmina)
      {
        day: 31,
        month: 8,
        yearStart: 1885,
        yearEnd: 1890,
      },
      // queen's day (wilhelmina)
      {
        day: 31,
        month: 8,
        yearStart: 1891,
        yearEnd: 1948,
      },
      // queen's day (juliana)
      {
        day: 31,
        month: 8,
        yearStart: 1949,
        yearEnd: 1979,
      },
      // queen's day (beatrix)
      {
        day: 31,
        month: 8,
        yearStart: 1980,
        yearEnd: 2013,
      },
      // kingsday (wimlex)
      {
        day: 27,
        month: 4,
        yearStart: 2014,
      },
      // liberation day (government)
      {
        institutionTypes: ['government'],
        day: 5,
        month: 5,
        yearStart: 1945, // could officially be different
      },
      // liberation day (public)
      {
        institutionTypes: ['public'],
        day: 5,
        month: 5,
        yearStart: 1945, // could officially be different
        interval: 5,
        offset: 0,
      },
      // christmas
      {
        day: 25,
        month: 12,
      },
      // christmas 2
      {
        day: 26,
        month: 12,
      },
    ],
  },
};
