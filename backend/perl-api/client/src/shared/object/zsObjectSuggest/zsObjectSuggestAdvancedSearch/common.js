// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';

/*
 * Flip the required bit if certain fields are set for remote
 * searches
 *
 * @lookup   - hash/dictionary of fields
 * @required - array with fields that lift the required bit
 */

export default function isRequired(lookup, required) {
  const max = required.length;

  let v;
  for (let k = 0; k < max; k++) {
    v = get(lookup, required[k]);
    if (v !== undefined && v !== null && (v.length || typeof v === 'object')) {
      return false;
    }
  }
  return true;
}
