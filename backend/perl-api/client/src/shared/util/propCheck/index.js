// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import check from 'api-check';

export default check({
  verbose: false,
  disabled: !ENV.IS_DEV,
});
