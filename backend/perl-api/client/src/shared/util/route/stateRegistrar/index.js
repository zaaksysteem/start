// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

module.exports = angular
  .module('stateRegistrar', [])
  .provider('stateRegistrar', [
    '$stateProvider',
    ($stateProvider) => {
      let states = [],
        statesByName = {};

      $stateProvider.decorator('data', (state, parent) => {
        if (!statesByName[state.self.name]) {
          states = states.concat(state);

          statesByName[state.self.name] = state;
        }

        return parent(state.self);
      });

      return {
        $get: [
          () => {
            return {
              getState: (name) => statesByName[name],
              getStates: () => states,
            };
          },
        ],
      };
    },
  ]).name;
