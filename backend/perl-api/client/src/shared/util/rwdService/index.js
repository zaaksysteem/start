// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import camelCase from 'lodash/camelCase';
import each from 'lodash/each';
import includes from 'lodash/includes';
import 'match-media/matchMedia';
import breakpoints from '!raw-loader!!./../../styles/_breakpoints.scss';
import appUnloadModule from './../appUnload';
import parseSassVariables from './../parseSassVariables';

module.exports = angular
  .module('rwdService', [appUnloadModule])
  .provider('rwdService', () => {
    const magicNumberExpression = /^(\d+)px$/;
    let breakpointVars = {};
    let mediaQueries = {};

    const parse = (sass) => {
      const sassVariables = parseSassVariables(sass);

      breakpointVars = {};
      mediaQueries = {};

      Object.keys(sassVariables).forEach((key) => {
        const cssLengthMatch = magicNumberExpression.exec(sassVariables[key]);

        if (cssLengthMatch) {
          breakpointVars[camelCase(key)] = Number(cssLengthMatch[1]);
        } else {
          mediaQueries[key] = sassVariables[key];
        }
      });
    };

    parse(breakpoints);

    return {
      parse,
      $get: [
        '$rootScope',
        '$window',
        '$document',
        'appUnload',
        ($rootScope, $window, $document, appUnload) => {
          let activeViews = [];

          const determineActiveViews = () => {
            let views = [];

            each(mediaQueries, (variable, key) => {
              let isActive = $window.matchMedia(variable).matches;

              if (isActive) {
                views = views.concat(key);
              }
            });

            if (!angular.equals(views, activeViews)) {
              $rootScope.$evalAsync(() => {
                activeViews = views;
              });
            }
          };

          const getActiveViews = () => activeViews;

          const getMagicNumbers = () => breakpointVars;

          const isActive = (type) => includes(activeViews, type);

          const handleResize = () => {
            determineActiveViews();
          };

          $window.addEventListener('resize', handleResize);

          appUnload.onUnload(() => {
            $window.removeEventListener('resize', handleResize);
          });

          determineActiveViews();

          return {
            isActive,
            getActiveViews,
            getMagicNumbers,
          };
        },
      ],
    };
  }).name;
