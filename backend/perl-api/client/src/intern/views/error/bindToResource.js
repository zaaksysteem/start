// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import getParamsFromResourceError from './getParamsFromResourceError';
import propCheck from './../../../shared/util/propCheck';

export default (resource, $state, description) => {
  propCheck.throw(
    propCheck.shape({
      resource: propCheck.object,
      $state: propCheck.object,
      description: propCheck.string.optional,
    }),
    {
      resource,
      $state,
      description,
    }
  );

  resource.onError((err) => {
    $state.go('error', getParamsFromResourceError(err, description), {
      location: false,
    });
  });

  return resource;
};
