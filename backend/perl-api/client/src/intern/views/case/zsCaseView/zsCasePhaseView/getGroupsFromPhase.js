// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import first from 'lodash/first';
import filter from 'lodash/filter';

export default (phase) => {
  let fields = get(phase, 'fields', []),
    groups;

  if (!get(first(fields), 'is_group')) {
    fields = [
      {
        is_group: true,
        label: 'Benodigde gegevens',
        help: '',
      },
    ].concat(fields);
  }

  groups = filter(fields, { is_group: true });

  return groups.map((group, index) => {
    let isFirst = index === 0,
      isLast = index === groups.length - 1,
      from = isFirst ? 1 : fields.indexOf(group) + 1,
      to = isLast ? fields.length : fields.indexOf(groups[index + 1]);

    return {
      id: group.id,
      label: group.label,
      description: group.help,
      fields: fields.slice(from, to),
    };
  });
};
