// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (option, value) => {
  return (
    option.active ||
    value === option.value ||
    (value && typeof value === 'object' && option.value in value)
  );
};
