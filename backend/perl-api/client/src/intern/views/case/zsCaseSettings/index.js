// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import caseActions from './../../../../shared/case/caseActions';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import auxiliaryRouteModule from './../../../../shared/util/route/auxiliaryRoute';
import zsConfirmModule from './../../../../shared/ui/zsConfirm';
import assign from 'lodash/assign';
import partition from 'lodash/partition';
import template from './template.html';
import './../../../../shared/styles/_contextual-settings.scss';

module.exports = angular
  .module('zsCaseSettings', [
    composedReducerModule,
    angularUiRouter,
    auxiliaryRouteModule,
    zsConfirmModule,
  ])
  .directive('zsCaseSettings', [
    '$state',
    'zsConfirm',
    'auxiliaryRouteService',
    'composedReducer',
    'dateFilter',
    ($state, zsConfirm, auxiliaryRouteService, composedReducer, dateFilter) => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseResource: '&',
          casetypeResource: '&',
          user: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          function (scope) {
            let ctrl = this,
              expanded,
              optionReducer = composedReducer(
                { scope },
                ctrl.caseResource(),
                ctrl.casetypeResource(),
                () => auxiliaryRouteService.getCurrentBase(),
                ctrl.user
              ).reduce((caseObj, casetype, currentState, user) => {
                let actions = caseActions(
                    {
                      caseObj,
                      casetype,
                      $state,
                      user,
                      acls: [],
                    },
                    dateFilter
                  ),
                  menuOptions;

                menuOptions = actions.map((action) => {
                  let state = auxiliaryRouteService.append(
                      currentState,
                      'admin'
                    ),
                    type = action.type || 'link',
                    button = {
                      name: action.name,
                      label: action.label,
                      type,
                      context: action.context,
                    };

                  if (type === 'confirm') {
                    button = assign(button, {
                      type: 'button',
                      click: () => {
                        zsConfirm(
                          action.confirm.label,
                          action.confirm.verb
                        ).then(() => {
                          let mutation = action.mutate();

                          ctrl
                            .caseResource()
                            .mutate(mutation.type, mutation.data)
                            .asPromise();
                        });
                      },
                    });
                  } else {
                    assign(button, {
                      link: $state.href(state, { action: action.name }),
                    });
                  }

                  assign(button, {
                    classes: {
                      admin: action.context === 'admin',
                    },
                  });

                  return button;
                });

                if (
                  actions.filter((action) => action.context === 'admin')
                    .length > 0
                ) {
                  let split = partition(menuOptions, { context: 'admin' });

                  menuOptions = split[1]
                    .concat({
                      name: 'expand',
                      type: 'text',
                      label: 'Beheeracties:',
                      click: () => {
                        expanded = !expanded;
                      },
                    })
                    .concat(split[0]);
                }

                return menuOptions;
              });

            ctrl.getOptions = optionReducer.data;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
