// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.css';

module.exports = angular
  .module('zsCaseDocumentViewV2', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseDocumentViewV2', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          caseData: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            const uiView = document.querySelector('ui-view.case-inner-view');
            const zsTopBar = document.querySelector('zs-top-bar');
            const caseSidebar = document.querySelector('.case-sidebar');
            const messageList = document.querySelector('.case-message-list');

            window.addEventListener('message', (ev) => {
              if (ev.data && ev.data.type === 'goFullscreen') {
                uiView && (uiView.style.padding = '0');
                zsTopBar && (zsTopBar.style.display = 'none');
                caseSidebar && (caseSidebar.style.display = 'none');
                messageList && (messageList.style.display = 'none');
              } else if (ev.data && ev.data.type === 'exitFullscreen') {
                uiView && (uiView.style = {});
                zsTopBar && (zsTopBar.style = {});
                caseSidebar && (caseSidebar.style = {});
                messageList && (messageList.style = {});
              }
            });

            ctrl.getStartUrl = () => {
              return `/external-components/exposed/case/${ctrl.caseUuid()}/documents`;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
