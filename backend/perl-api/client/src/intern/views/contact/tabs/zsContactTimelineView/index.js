// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsTimelineModule from '../../../../../shared/ui/zsTimeline';
import './styles.scss';

module.exports = angular
  .module('zsContactTimelineView', [zsTimelineModule])
  .component('zsContactTimelineView', {
    bindings: {
      subject: '&',
    },
    controller: [
      function () {
        let ctrl = this;

        ctrl.getTimelineReference = () => ctrl.subject().data().reference;
      },
    ],
    template,
  }).name;
