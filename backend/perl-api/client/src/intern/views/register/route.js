// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import observableStateParamsModule from './../../../shared/util/route/observableStateParams';
import resourceModule from './../../../shared/api/resource';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import bindToResource from './../error/bindToResource';
import first from 'lodash/first';
import get from 'lodash/get';
import assign from 'lodash/assign';
import template from './index.html';
import {
  getAction,
  getValuesFromCustomObject,
  getValuesFromParent,
} from './library';

// v1 to v2
const subjectTypeDict = {
  person: 'person',
  company: 'organization',
  employee: 'employee',
};

module.exports = angular
  .module('Zaaksysteem.intern.register.route', [
    angularUiRouterModule,
    observableStateParamsModule,
    ocLazyLoadModule,
    resourceModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider.state('register', {
        url:
          '/aanvragen/:casetypeId/:step/?aanvrager&contactkanaal&ontvanger&document&origin&origin-relation-type&custom_object&custom_object_type&copy-subject-roles',
        title: [
          'casetype',
          (casetype) => `${get(casetype.data(), 'instance.title')}`,
        ],
        template,
        params: {
          casetypeId: { value: null },
          step: { value: null, squash: true },
          aanvrager: null,
          contactkanaal: null,
          ontvanger: null,
          document: null,
          origin: null,
          'origin-relation-type': null,
          'copy-subject-roles': null,
          values: null,
          custom_object: null,
          custom_object_type: null,
        },
        shouldReload: (current, currentParams, to, toParams) => {
          return (
            current.name !== to.name ||
            currentParams.casetypeId !== toParams.casetypeId
          );
        },
        resolve: {
          casetype: [
            '$rootScope',
            '$state',
            'resource',
            '$stateParams',
            ($rootScope, $state, resource, $stateParams) => {
              let res;

              res = resource(`/api/v1/casetype/${$stateParams.casetypeId}`, {
                scope: $rootScope,
              }).reduce((requestOptions, data) => {
                return first(data);
              });

              bindToResource(res, $state, 'het zaaktype');

              return res.asPromise().then(() => res);
            },
          ],
          casetypeV2: [
            '$rootScope',
            '$state',
            'resource',
            '$stateParams',
            (/* $rootScope, $state, resource, $stateParams */) => {
              // disabled as part of the hotfix (MINTY-2906)
              // remove when v2 is supported again (when MINTY-2901 is fixed)
              return {};

              // let res;

              // res = resource(
              // 	{
              // 		url: '/api/v2/cm/case_type/get_active_version',
              // 		params: {
              // 			case_type_uuid: $stateParams.casetypeId
              // 		}
              // 	},
              // 	{ scope: $rootScope }
              // )
              // 	.reduce(( requestOptions, data ) => {

              // 		return get(data, 'data');

              // 	});

              // bindToResource(res, $state, 'het zaaktype');

              // return res.asPromise().then(( ) => res);
            },
          ],
          parentCase: [
            '$rootScope',
            'resource',
            '$stateParams',
            (scope, resource, $stateParams) => {
              const parentCaseId = $stateParams.origin;

              if (!parentCaseId) return null;

              const url = `/api/v0/case/${parentCaseId}`;
              const parentResource = resource(url, { scope });
              const res = parentResource.reduce((req, data) => first(data));

              return res.asPromise().then(() => res);
            },
          ],
          parentAction: [
            '$rootScope',
            'resource',
            '$stateParams',
            'casetype',
            (scope, resource, $stateParams, casetypeResource) => {
              const casetype = casetypeResource.data();
              const parentCaseId = $stateParams.origin;

              if (!parentCaseId) return null;

              const url = `/api/v0/case/${parentCaseId}/actions`;
              const actionResource = resource(url, { scope });
              const res = actionResource.reduce((req, data) =>
                getAction(data, casetype)
              );

              return res.asPromise().then(() => res);
            },
          ],
          valuesFromParent: [
            'casetype',
            'parentCase',
            'parentAction',
            (casetypeResource, parentCaseResource, parentActionResource) => {
              if (!parentCaseResource) return null;

              const casetype = casetypeResource.data();
              const parentCase = parentCaseResource.data();
              const parentAction = parentActionResource.data();

              return getValuesFromParent(parentCase, parentAction, casetype);
            },
          ],
          subjectsFromParent: [
            '$rootScope',
            'resource',
            'parentCase',
            'parentAction',
            (scope, resource, parentCaseResource, parentActionResource) => {
              if (!parentCaseResource) return null;

              const parentCase = parentCaseResource.data();
              const parentUuid = get(parentCase, 'id');

              const parentAction = parentActionResource.data();
              const rolesToCopy = get(parentAction, 'data.subject_role');

              if (!rolesToCopy) return;

              const url = `/api/v2/cm/case/get_subject_relations?case_uuid=${parentUuid}`;
              const subjectsResource = resource(url, { scope });
              const promise = subjectsResource.reduce((req, subjects) => {
                return subjects.filter((subject) =>
                  rolesToCopy.includes(get(subject, 'attributes.role'))
                );
              });

              return promise.asPromise().then(() => promise);
            },
          ],
          valuesFromCustomObject: [
            'custom_object',
            (customObjectResource) => {
              const customObject = customObjectResource.data();

              // without a custom object, no attributes need to be copied
              if (!customObject) {
                return null;
              }

              return getValuesFromCustomObject(customObject);
            },
          ],
          requestor: [
            '$rootScope',
            'resource',
            '$stateParams',
            '$state',
            'user',
            ($rootScope, resource, $stateParams, $state, userResource) => {
              let res,
                requestorId =
                  $stateParams.aanvrager ||
                  get(userResource.data(), 'instance.logged_in_user.uuid');

              res = resource(`/api/v1/subject/${requestorId}`, {
                scope: $rootScope,
              }).reduce((requestOptions, data) => first(data));

              bindToResource(res, $state, 'de aanvrager');

              return res.asPromise().then(() => res);
            },
          ],
          requestorV2: [
            '$rootScope',
            'resource',
            'requestor',
            ($rootScope, resource, requestorResource) => {
              let res;
              const requestorId = requestorResource.data().reference;
              const requestorTypeV1 = requestorResource.data().instance.subject
                .type;
              const requestorType = subjectTypeDict[requestorTypeV1];

              res = resource(
                `/api/v2/cm/contact/get_contact?uuid=${requestorId}&type=${requestorType}`,
                {
                  scope: $rootScope,
                }
              ).reduce((requestOptions, data) => data);

              return res.asPromise().then(() => res);
            },
          ],
          recipient: [
            '$rootScope',
            'resource',
            '$stateParams',
            '$state',
            ($rootScope, resource, $stateParams, $state) => {
              let res;

              res = resource(
                $stateParams.ontvanger
                  ? `/api/v1/subject/${$stateParams.ontvanger}`
                  : null,
                { scope: $rootScope }
              ).reduce((requestOptions, data) => first(data));

              bindToResource(res, $state, 'de ontvanger');

              return res.asPromise().then(() => res);
            },
          ],
          rules: [
            '$rootScope',
            '$state',
            'resource',
            '$stateParams',
            'casetype',
            'requestor',
            (
              $rootScope,
              $state,
              resource,
              $stateParams,
              casetypeResource,
              requestorResource
            ) => {
              let res;

              res = resource(
                () => {
                  // We need to send requestor zipcode to obtain a correct rules response from backend related to zipcode.
                  // Correspondence zipcode is leading over residence zipcode.

                  let params = assign(
                    {},
                    {
                      'case.casetype.node.id': casetypeResource.data().instance
                        .legacy.zaaktype_node_id,
                      'case.requestor.subject_type': requestorResource.data()
                        .instance.subject_type,
                      'case.channel_of_contact': $stateParams.contactkanaal,
                      'case.requestor.zipcode':
                        get(
                          requestorResource.data(),
                          'instance.subject.instance.address_correspondence.instance.zipcode'
                        ) ||
                        get(
                          requestorResource.data(),
                          'instance.subject.instance.address_residence.instance.zipcode'
                        ),
                      zapi_no_pager: 1,
                    }
                  );

                  return {
                    url: '/api/rules/',
                    params,
                  };
                },
                { scope: $rootScope }
              ).reduce((requestOptions, data) => {
                return data ? data.asMutable({ deep: true }) : [];
              });

              bindToResource(res, $state, 'de regels van het zaaktype');

              return res.asPromise().then(() => res);
            },
          ],
          document: [
            '$rootScope',
            '$stateParams',
            'resource',
            ($rootScope, $stateParams, resource) => {
              let res = resource(
                $stateParams.document
                  ? `/api/file/${$stateParams.document}`
                  : null,
                {
                  scope: $rootScope,
                }
              );

              return res.asPromise().then(() => res);
            },
          ],
          custom_object: [
            '$rootScope',
            '$stateParams',
            'resource',
            ($rootScope, $stateParams, resource) => {
              let res = resource(
                $stateParams.custom_object
                  ? `/api/v2/cm/custom_object/get_custom_object?uuid=${$stateParams.custom_object}`
                  : null,
                {
                  scope: $rootScope,
                }
              );

              return res.asPromise().then(() => res);
            },
          ],
          mapSettings: [
            '$rootScope',
            '$stateParams',
            'resource',
            ($rootScope, $stateParams, resource) => {
              let res = resource(
                () => {
                  return '/api/v1/map/ol_settings';
                },
                {
                  scope: $rootScope,
                }
              );

              return res.asPromise().then(() => res);
            },
          ],
          module: [
            '$rootScope',
            '$ocLazyLoad',
            '$q',
            ($rootScope, $ocLazyLoad, $q) => {
              return $q((resolve /*, reject*/) => {
                require(['./'], () => {
                  let load = () => {
                    $ocLazyLoad.load({
                      name: 'Zaaksysteem.intern.register',
                    });

                    resolve();
                  };

                  if (!$rootScope.$$phase) {
                    $rootScope.$apply(load);
                  } else {
                    load();
                  }
                });
              });
            },
          ],
        },
        controller: [
          'observableStateParams',
          '$stateParams',
          'casetype',
          'casetypeV2',
          'valuesFromParent',
          'subjectsFromParent',
          'valuesFromCustomObject',
          'requestor',
          'requestorV2',
          'recipient',
          'rules',
          'user',
          'document',
          'custom_object',
          'mapSettings',
          function (
            observableStateParams,
            $stateParams,
            casetypeResource,
            casetypeV2Resource,
            valuesFromParent,
            subjectsFromParent,
            valuesFromCustomObject,
            requestorResource,
            requestorV2Resource,
            recipientResource,
            rulesResource,
            userResource,
            documentResource,
            customObjectResource,
            mapSettingsResource
          ) {
            let ctrl = this;

            ctrl.getCasetype = casetypeResource.data;

            ctrl.getCasetypeV2 = casetypeV2Resource.data;

            ctrl.getParentCaseId = () => $stateParams.origin;

            ctrl.getParentCaseRelationType = () =>
              $stateParams['origin-relation-type'];

            ctrl.getValuesFromParent = () => valuesFromParent;

            ctrl.getSubjectsFromParent = () => subjectsFromParent;

            ctrl.getValuesFromCustomObject = () => valuesFromCustomObject;

            ctrl.getRequestor = requestorResource.data;

            ctrl.getRequestorV2 = requestorV2Resource.data;

            ctrl.getRecipient = recipientResource.data;

            ctrl.getRules = rulesResource.data;

            ctrl.getDocuments = documentResource.data;

            ctrl.getCustomObject = customObjectResource.data;

            ctrl.getCustomObjectTypeUuid = () =>
              $stateParams.custom_object_type;

            ctrl.getUser = () =>
              get(userResource.data(), 'instance.logged_in_user');

            ctrl.getStep = () => observableStateParams.get('step');

            ctrl.getChannelOfContact = () =>
              observableStateParams.get('contactkanaal') || 'behandelaar';

            ctrl.getValues = () => $stateParams.values;

            ctrl.getMapSettings = () =>
              get(mapSettingsResource.data(), '[0].instance');
          },
        ],
        controllerAs: '$ctrl',
      });
    },
  ]).name;
