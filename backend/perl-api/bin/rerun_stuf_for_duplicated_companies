#!/usr/bin/perl -w
use strict;

use Moose;
use Data::Dumper;
use JSON;
use Time::HiRes qw(gettimeofday tv_interval);

use File::Copy;
use File::Temp qw/mktemp/;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catalyst qw[ConfigLoader];

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Model::DB;
use Zaaksysteem::Model::LDAP;

use Zaaksysteem::Geo;
use Text::CSV;

#use Geo::Proj4;
use LWP::UserAgent;



use Config::Any;

my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [optional: dbname]") unless @ARGV && scalar(@ARGV) >= 4;
my ($hostname, $config_file, $config_dir, $dbname)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};

                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};
                    $mig_config->{ldap_config_basedn}   = $config_data->{LDAP}->{basedn};
                    $mig_config->{customer_info}        = { gemeente => $config_data->{customer_info} };

                    if ($dbname) {
                        $mig_config->{database_dsn} =~ s/dbname=([0-9a-zA-Z_-]+)/dbname=$dbname/;
                    }
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';
    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
    $mig_config->{ldap_config}                  = $config->{LDAP};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

load_customer_d();
load_zaaksysteem_conf();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn}
    );

my $dbic = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});

$dbic->txn_do(sub {
    remove_duplicates($dbic);
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Zaaksysteem::Model::DB->new->schema;

    $schema->log($log);

    $schema->betrokkene_model->log($log);
    $schema->betrokkene_model->config($mig_config->{customer_info});

    info('Connected to: ' . $dsn);

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

# SELECT id,local_id FROM object_subscription WHERE local_id in (
#     select local_id from object_subscription where local_table='Bedrijf' group by local_id having count(local_id) > 1
# ) and local_table='Bedrijf'

sub remove_duplicates {
    my $dbic = shift;

    my @duplicate_object_subscriptions = $dbic->resultset('ObjectSubscription')->search(
        {
            local_id => { in => \'(select local_id from object_subscription where local_table=\'Bedrijf\' group by local_id having count(local_id) > 1)' }
        },
        {
            order_by => { -asc => 'me.local_id' }
        }
    );

    my $total_count = scalar(@duplicate_object_subscriptions);

    my ($last_subscription, $current_bedrijf);
    my $count = 0;
    my %prevent_dupes;
    for my $row (@duplicate_object_subscriptions) {
        ++$count;

        if ($prevent_dupes{$row->external_id}) {
            next;
        }

        $prevent_dupes{$row->external_id} = 1;

        if (!$last_subscription || $last_subscription != $row->local_id) {
            $last_subscription  = $row->local_id;
            $current_bedrijf    = $dbic->resultset('Bedrijf')->find($row->local_id);
            info('Patching: ' . $row->local_id . '(' . $count . ' of ' . $total_count . ')');

            load_from_stuf_history($dbic, $row->external_id);

            info('  ' . $row->local_id . ': Loaded from StUF: ' . $row->external_id);
            next;
        }

        my $copy                = $current_bedrijf->copy;

        $copy->update->discard_changes();
        info('  ' . $row->local_id . ': Cloning: ' . $row->local_id . ' into ' . $copy->id);
        $row->local_id($copy->id);
        $row->update;

        load_from_stuf_history($dbic, $row->external_id);

        info('    ' . $row->local_id . ': Loaded from StUF: ' . $row->external_id);

    }
}

sub load_from_stuf_history {
    my $dbic        = shift;
    my $external_id = shift;

    my $transactions = $dbic->resultset('Transaction')->search(
        {
            -and    => [
                { 'me.input_data' => { like => '%entiteittype>NNP%'}},
                { 'me.input_data' => { like => '%sleutelVerzendend="' . $external_id . '"%'}},
                { 'success_count' => { '>' => 0 }},
            ]
        },
        {
            order_by => 'me.id',
            prefetch => 'interface_id',
        }
    );

    while (my $transaction = $transactions->next) {
        my $new_transaction = $transaction->interface_id->process({
            input_data                  => $transaction->input_data,
            external_id                 => $external_id,
        });

        info('      LOADED: ' . $new_transaction->id . ' (from: ' . $transaction->id . ')');

        #die('Failed running transaction for: ' . $external_id) unless $new_transaction->success_count > 0;
    }

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
