CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

BEGIN;

DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS subject CASCADE;

CREATE TABLE subject (
    id 			            SERIAL PRIMARY KEY,
    uuid                    UUID UNIQUE DEFAULT uuid_generate_v4(),
    subject_type            TEXT NOT NULL CHECK (
        subject_type ~ '(employee)'
    ),
    properties              TEXT DEFAULT '{}' NOT NULL,
    settings                TEXT DEFAULT '{}' NOT NULL
);

DROP TABLE IF EXISTS user_entity CASCADE;

CREATE TABLE user_entity (
    id                      SERIAL PRIMARY KEY,
    uuid 	                UUID DEFAULT uuid_generate_v4(),
    source_interface_id     INTEGER REFERENCES interface(id),
    source_identifier       TEXT NOT NULL,
    active                  BOOLEAN DEFAULT 'f' NOT NULL,
    subject_id              INTEGER REFERENCES subject(id),
    date_created            timestamp without time zone,
    date_deleted            timestamp without time zone
);

COMMIT;
