DROP INDEX IF EXISTS thread_last_message_cache_message_type_idx;
    
CREATE INDEX CONCURRENTLY thread_last_message_cache_message_type_idx ON thread(
    (CAST(last_message_cache AS json)->>'message_type')
);
