BEGIN;
  ALTER TABLE thread_message_external ADD COLUMN subtype TEXT;
  ALTER TABLE thread_message ADD COLUMN external_reference TEXT;
COMMIT;

