BEGIN;

    CREATE OR REPLACE FUNCTION attribute_richtext_strip_html_tags(html_text text[])
    RETURNS text
    IMMUTABLE
    LANGUAGE plpgsql
    AS $$
    declare
    BEGIN
        RETURN regexp_replace(
            regexp_replace(html_text[1], '<[^/][^>]*>', ' ', 'g'),
            '</[^>]+>', '',
            'g'
        );
    END;
    $$;

COMMIT;