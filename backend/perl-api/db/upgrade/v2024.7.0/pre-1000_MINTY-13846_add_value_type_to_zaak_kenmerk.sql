BEGIN;

    ALTER TABLE zaak_kenmerk ADD COLUMN value_type TEXT;

    UPDATE zaak_kenmerk SET value_type = bk.value_type
    FROM bibliotheek_kenmerken bk
    WHERE bibliotheek_kenmerken_id = bk.id;

    ALTER TABLE zaak_kenmerk ALTER COLUMN value_type SET NOT NULL;

    CREATE OR REPLACE FUNCTION set_value_type() RETURNS TRIGGER AS $$
    BEGIN
        -- Fetch the value_type from the bibliotheek_kenmerk table
        SELECT value_type
        INTO NEW.value_type
        FROM bibliotheek_kenmerken
        WHERE id = NEW.bibliotheek_kenmerken_id;

        RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;

    CREATE TRIGGER before_insert_zaak_kenmerk BEFORE INSERT ON zaak_kenmerk FOR EACH ROW EXECUTE FUNCTION set_value_type();

COMMIT;