BEGIN;

ALTER TABLE file_metadata ADD COLUMN document_source VARCHAR;

COMMIT;