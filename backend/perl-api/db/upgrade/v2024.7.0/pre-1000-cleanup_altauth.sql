CREATE OR REPLACE FUNCTION cleanup_altauth(IN dry_run boolean DEFAULT FALSE)
    RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    subject_ids integer[];
BEGIN
    WITH subject_query AS (
        SELECT
            s.id,
            s.last_modified
        FROM
            subject s
        WHERE
            subject_type = 'person'
            AND NOT EXISTS (
                SELECT
                    1
                FROM
                    natuurlijk_persoon
                WHERE
                    uuid = s.uuid)
                AND (
                    SELECT
                        COUNT(*)
                    FROM
                        user_entity ue
                        JOIN interface i ON ue.source_interface_id = i.id
                    WHERE
                        ue.subject_id = s.id
                        AND i.module = 'auth_twofactor') = 1
                    AND NOT EXISTS (
                        SELECT
                            1
                        FROM
                            user_entity ue
                            JOIN interface i ON ue.source_interface_id = i.id
                        WHERE
                            ue.subject_id = s.id
                            AND i.module != 'auth_twofactor')
                    UNION
                    SELECT
                        s.id,
                        s.last_modified
                    FROM
                        subject s
                    WHERE
                        subject_type = 'company'
                        AND (
                            SELECT
                                COUNT(*)
                            FROM
                                user_entity ue
                                JOIN interface i ON ue.source_interface_id = i.id
                            WHERE
                                ue.subject_id = s.id
                                AND i.module = 'auth_twofactor') = 1
                            AND NOT EXISTS (
                                SELECT
                                    1
                                FROM
                                    user_entity ue
                                    JOIN interface i ON ue.source_interface_id = i.id
                                WHERE
                                    ue.subject_id = s.id
                                    AND i.module != 'auth_twofactor')
                                AND NOT EXISTS (
                                    SELECT
                                        1
                                    FROM
                                        gegevensmagazijn_subjecten
                                    WHERE
                                        subject_uuid = s.uuid)
                                    AND NOT EXISTS (
                                        SELECT
                                            1
                                        FROM
                                            bedrijf
                                        WHERE
                                            dossiernummer::varchar =((s.properties::jsonb) ->> 'kvknummer')
                                            AND vestigingsnummer::varchar IS NOT DISTINCT FROM ((s.properties::jsonb) ->> 'vestigingsnummer')))
                            SELECT
                                array_agg(id) INTO subject_ids
                        FROM
                            subject_query
                        WHERE
                            last_modified < NOW() AT TIME ZONE 'UTC' - '2 hours'::interval;
    RAISE NOTICE 'Removing subjects: %', subject_ids;
    IF dry_run IS TRUE THEN
        RAISE NOTICE 'Dry run, not actually deleting';
    ELSE
        DELETE FROM "user_entity"
        WHERE subject_id IN (
                SELECT
                    unnest(subject_ids));
        DELETE FROM "subject"
        WHERE id IN (
                SELECT
                    unnest(subject_ids));
    END IF;
END;
$$;

