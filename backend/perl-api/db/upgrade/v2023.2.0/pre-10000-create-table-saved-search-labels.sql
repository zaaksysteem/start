BEGIN;

DROP TABLE IF EXISTS saved_search_label_mapping;
DROP TABLE IF EXISTS saved_search_labels;

CREATE TABLE saved_search_labels (
    id SERIAL PRIMARY KEY,
	uuid UUID UNIQUE DEFAULT uuid_generate_v4(),
	label VARCHAR UNIQUE NOT NULL
);


CREATE TABLE saved_search_label_mapping (
	id SERIAL PRIMARY KEY,
	label_id integer NOT NULL,
	saved_search_id integer NOT NULL,
    CONSTRAINT label_id_fkey FOREIGN KEY (label_id) REFERENCES saved_search_labels (id) ON DELETE CASCADE,
    CONSTRAINT saved_search_id_fkey FOREIGN KEY (saved_search_id) REFERENCES saved_search (id) ON DELETE CASCADE
);
CREATE INDEX saved_search_label_idx ON saved_search_label_mapping(label_id, saved_search_id);
CREATE INDEX saved_search_id_idx ON saved_search_label_mapping(saved_search_id, label_id);

COMMIT;