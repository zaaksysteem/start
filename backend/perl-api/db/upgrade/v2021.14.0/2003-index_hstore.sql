BEGIN;

  CREATE INDEX zaak_meta_text_vector_idx
    ON zaak_meta
    USING GIN (text_vector) WITH (gin_pending_list_limit=128);

  CREATE INDEX zaak_meta_index_hstore_idx
    ON zaak_meta
    USING GIN (index_hstore) WITH (gin_pending_list_limit=128);

COMMIT;
