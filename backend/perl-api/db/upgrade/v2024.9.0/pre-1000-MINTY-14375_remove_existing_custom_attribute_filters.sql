BEGIN;

    DELETE FROM saved_search WHERE kind='custom_object' AND filters::jsonb @> '{"filters": [{"type": "attributes.value"}]}';

COMMIT;