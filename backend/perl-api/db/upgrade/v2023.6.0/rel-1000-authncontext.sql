
BEGIN;

  UPDATE interface SET
    interface_config = interface_config::jsonb || '{"authentication_level": "" }' WHERE
      interface_config::jsonb->>'saml_type' = 'adfs'
      AND
      interface_config::jsonb->>'disable_requested_authncontext' = '1'
      AND 
      interface_config::jsonb->>'login_type_user' = '1'
      AND
      interface_config::jsonb->>'authentication_level' != ''
      ;

  UPDATE interface set interface_config = interface_config::jsonb || '{"binding":"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"}'
    WHERE
    interface_config::jsonb->>'saml_type' IN ('adfs', 'minimal') AND
    interface_config::jsonb->>'binding' NOT IN ('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact', 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST');

COMMIT;
