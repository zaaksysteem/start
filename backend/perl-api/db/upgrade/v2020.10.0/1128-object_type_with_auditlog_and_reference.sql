BEGIN;
ALTER TABLE custom_object_type_version
ADD
  COLUMN audit_log JSONB NOT NULL DEFAULT '{}';
ALTER TABLE custom_object_type_version
ADD
  COLUMN external_reference text;
COMMIT;