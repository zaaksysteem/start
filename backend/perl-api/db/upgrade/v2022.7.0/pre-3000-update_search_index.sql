INSERT INTO queue (type, label, priority, metadata, data)
SELECT
    'touch_case',
    'release: Fix missing search index for cases',
    950,
    -- metadata
    json_build_object('require_object_model', 1, 'disable_acl', 1, 'target', 'backend'),
    -- data
    json_build_object('case_number', z.id)
FROM
    zaak z
    JOIN zaak_meta zm ON z.id = zm.zaak_id
WHERE
    zm.index_hstore IS NULL
    AND z.status != 'deleted';

