
BEGIN;
  WITH cte AS
  (
    SELECT zts.* FROM zaaktype_sjablonen zts
    JOIN bibliotheek_sjablonen bs
      ON bs.id = zts.bibliotheek_sjablonen_id AND bs.interface_id IS NOT NULL
    JOIN interface i on bs.interface_id = i.id
    WHERE
    i.module = 'stuf_dcr'
  )
  UPDATE zaaktype_sjablonen zts
  SET allow_edit = true
  FROM cte
  WHERE zts.id = cte.id
  ;

COMMIT;
