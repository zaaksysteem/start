BEGIN;

    ALTER TABLE zaak_betrokkenen
       ADD COLUMN bibliotheek_kenmerken_id INTEGER REFERENCES bibliotheek_kenmerken(id) ON DELETE CASCADE;

COMMIT;
