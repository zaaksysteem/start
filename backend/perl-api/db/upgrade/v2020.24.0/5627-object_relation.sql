BEGIN;

ALTER TABLE custom_object_relationship ADD COLUMN source_custom_field_type_id integer REFERENCES bibliotheek_kenmerken(id);
ALTER TABLE custom_object_relationship ADD COLUMN uuid UUID NOT NULL UNIQUE DEFAULT uuid_generate_v4();

ALTER TABLE custom_object_relationship
ADD CONSTRAINT source_custom_field_reference CHECK 
 (((source_custom_field_type_id IS NOT NULL) AND (related_case_id IS NOT NULL)) OR
 (source_custom_field_type_id IS NULL));

COMMIT;