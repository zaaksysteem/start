BEGIN;

    DROP VIEW case_acl;
    DROP MATERIALIZED VIEW subject_position_matrix;

    CREATE MATERIALIZED VIEW subject_position_matrix AS
    SELECT
        s.id AS "subject_id",
        unrolled_groups.gid AS "group_id",
        unrolled_roles.rid AS "role_id",
        unrolled_groups.gid::VARCHAR || '|' || unrolled_roles.rid::VARCHAR AS "position"
    FROM
        subject s,
        (
            SELECT
            s.id AS subject_id,
            unnest(s.role_ids) AS rid
            FROM
            subject s) unrolled_roles,
        (
            SELECT
            s.id AS subject_id,
            unnest(g.path) AS gid
            FROM
            subject s
            JOIN GROUPS g ON s.group_ids[1] = g.id) AS unrolled_groups,
        "roles" ro,
        "groups" gr
    WHERE
        s.id = unrolled_roles.subject_id
        AND s.id = unrolled_groups.subject_id
        AND ro.id = unrolled_roles.rid
        AND gr.id = unrolled_groups.gid
        GROUP BY s.id, unrolled_groups.gid, unrolled_roles.rid;

    CREATE UNIQUE INDEX subject_position_matrix_group_role_subject_idx ON subject_position_matrix (subject_id, role_id, group_id);
    CREATE INDEX subject_position_matrix_position_subject_idx ON subject_position_matrix (subject_id, position);

    CREATE OR REPLACE FUNCTION refresh_subject_position_matrix ()
        RETURNS TRIGGER
        LANGUAGE plpgsql
    AS $$
    BEGIN
        REFRESH MATERIALIZED VIEW CONCURRENTLY subject_position_matrix;
        RETURN NULL;
    END
    $$;

    CREATE VIEW case_acl AS
        SELECT
            z.id AS case_id,
            z.uuid AS case_uuid,
            cam.key AS permission,
            s.id AS subject_id,
            s.uuid AS subject_uuid,
            z.zaaktype_id AS casetype_id
        FROM
            zaak z
            JOIN zaaktype_authorisation za ON za.zaaktype_id = z.zaaktype_id
                AND za.confidential = z.confidential
            JOIN (subject_position_matrix spm
                JOIN subject s ON spm.subject_id = s.id) ON spm.role_id = za.role_id
                AND spm.group_id = za.ou_id
            JOIN case_authorisation_map cam ON za.recht = cam.legacy_key
        UNION ALL
        SELECT
            z.id AS case_id,
            z.uuid AS case_uuid,
            za.capability AS permission,
            s.id AS subject_id,
            s.uuid AS subject_uuid,
            z.zaaktype_id AS casetype_id
        FROM
            zaak z
            JOIN zaak_authorisation za ON za.zaak_id = z.id
                AND za.entity_type = 'position'::text
            JOIN (subject_position_matrix spm
                JOIN subject s ON spm.subject_id = s.id) ON spm."position" = za.entity_id
        UNION ALL
        SELECT
            z.id AS case_id,
            z.uuid AS case_uuid,
            za.capability AS permission,
            s.id AS subject_id,
            s.uuid AS subject_uuid,
            z.zaaktype_id AS casetype_id
        FROM
            zaak z
            JOIN zaak_authorisation za ON za.zaak_id = z.id
                AND za.entity_type = 'user'::text
            JOIN subject s ON s.username::text = za.entity_id;
COMMIT;
