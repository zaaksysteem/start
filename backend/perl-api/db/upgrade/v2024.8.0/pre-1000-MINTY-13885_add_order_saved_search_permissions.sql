BEGIN;

    ALTER TABLE saved_search_permission ADD COLUMN sort_order integer;

COMMIT;