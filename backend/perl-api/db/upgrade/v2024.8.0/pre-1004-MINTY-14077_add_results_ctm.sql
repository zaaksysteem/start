
BEGIN;

CREATE OR REPLACE FUNCTION get_results_as_json(zaaktype_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(row_to_json(results)) results
      from (
        select label as name,
               resultaat as result_type,
               standaard_keuze as is_default,
               selectielijst as selection_list,
               properties::jsonb->>'selectielijst_nummer' as selection_list_number,
               selectielijst_brondatum as selection_list_source_date,
               selectielijst_einddatum  as selection_list_end_date,
               trigger_archival as archival_trigger,
               archiefnominatie as archival_nomination,
               bewaartermijn as retention_period,
               ingang as archival_nomination_valuation,
               "comments" as comments,
               properties::jsonb->>'procestype_nummer' as processtype_number,
               properties::jsonb->>'procestype_naam' as processtype_name,
               properties::jsonb->>'procestype_omschrijving' as processtype_description,
               properties::jsonb->>'procestype_toelichting' as processtype_explanation,
               properties::jsonb->>'procestype_object' as processtype_object,
               NULLIF(properties::jsonb->>'procestype_generiek', '') as procestype_generic, 
               NULLIF(properties::jsonb->>'herkomst', '') as origin,
               NULLIF(properties::jsonb->>'procestermijn', '') as process_period
            from zaaktype_resultaten zr
            where zr.zaaktype_status_id = zaaktype_statusid
        ) results;
END;
$$;

CREATE OR REPLACE FUNCTION get_phases_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(phases) from (select zts.status as phase_id,
        zts.naam as milestone_name,
        zts.fase as phase_name,
        zts.created,
        zts.last_modified,
        zts.termijn as term_in_days,
        case when g.uuid is null or r.uuid is null then null else
        json_build_object(
            'enabled', case when zts.status = 1 then true else text_to_bool(zts.role_set::text) end,
            'department_uuid', g.uuid,
            'department_name', g.name,
            'role_uuid', r.uuid,
            'role_name', r.name
        ) end as assignment,
        get_kenmerken_for_phase_as_json(zts.id) as custom_fields,
        get_results_as_json(zts.id) as results
        from zaaktype_status zts
        left join roles r on r.id = zts.role_id
        left join groups g on g.id = zts.ou_id
        where zts.zaaktype_node_id = zaaktype_nodeid
        order by zts.status) phases;
END;
$$;

COMMIT;