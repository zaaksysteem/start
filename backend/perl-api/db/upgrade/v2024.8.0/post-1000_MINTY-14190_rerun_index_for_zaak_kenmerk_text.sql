BEGIN;

    CREATE INDEX IF NOT EXISTS zaak_kenmerk_text_idx  ON zaak_kenmerk USING GIN (immutable_array_to_string(value) gin_trgm_ops) WHERE value_type IN ('text', 'textarea');

COMMIT;