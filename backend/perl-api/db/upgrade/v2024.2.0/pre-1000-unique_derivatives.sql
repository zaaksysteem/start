BEGIN;
    CREATE TABLE filestore_derivatives_dupes_202309 AS (
        SELECT * FROM file_derivative WHERE id IN (
            SELECT id from (
                SELECT id, row_number() OVER (
                    PARTITION BY file_id, max_width, max_height, type ORDER BY date_generated
                ) AS row_num FROM file_derivative
            ) fd WHERE fd.row_num > 1
        )
    );
    DELETE FROM file_derivative WHERE id IN (SELECT id FROM filestore_derivatives_dupes_202309);

    CREATE UNIQUE INDEX file_derivative_unique ON file_derivative(file_id, type, max_width, max_height);

COMMIT;
