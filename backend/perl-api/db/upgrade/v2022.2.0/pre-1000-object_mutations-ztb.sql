BEGIN;

DROP TABLE IF EXISTS zaaktype_object_mutation;

CREATE TABLE zaaktype_object_mutation (
  id serial,
  zaaktype_node_id integer REFERENCES zaaktype_node (id) NOT NULL,
  zaaktype_status_id integer REFERENCES zaaktype_status (id) NOT NULL,
  attribute_uuid uuid REFERENCES bibliotheek_kenmerken (uuid) NOT NULL,
  mutation_type text NOT NULL,
  object_title text,
  phase_transition boolean DEFAULT FALSE NOT NULL,
  system_attribute_changes jsonb DEFAULT '{}' NOT NULL,
  attribute_mapping jsonb DEFAULT '{}' NOT NULL
);

COMMIT;

