-- These may fail if they already exist
CREATE INDEX zaak_pid_idx ON zaak(pid);
CREATE INDEX zaak_betrokkenen_zaak_id_idx ON zaak_betrokkenen(zaak_id);
CREATE INDEX zaak_betrokkenen_betrokkene_type_betrokkene_id_idx ON zaak_betrokkenen(betrokkene_type, betrokkene_id);
