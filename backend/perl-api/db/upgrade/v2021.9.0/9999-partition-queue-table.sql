
BEGIN;

  DROP TABLE IF EXISTS queue_evict;

  CREATE TABLE public.queue_part ( like queue INCLUDING DEFAULTS)
    PARTITION BY RANGE (date_created);

  CREATE OR REPLACE FUNCTION check_queue_object_id_constraint() RETURNS TRIGGER
  LANGUAGE 'plpgsql' AS $$
  BEGIN

    IF NEW.object_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT uuid FROM object_data WHERE uuid = NEW.object_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.object_id
          USING HINT = 'Please check your object_id';
      END IF;
    END IF;

    return NEW;

  END $$;

  CREATE OR REPLACE FUNCTION check_queue_parent_id_constraint() RETURNS TRIGGER
  LANGUAGE 'plpgsql' AS $$
  BEGIN

    IF NEW.parent_id IS NOT NULL
    THEN
      IF NOT EXISTS (SELECT id FROM queue WHERE id = NEW.parent_id)
      THEN
        RAISE EXCEPTION 'Nonexistent UUID --> %', NEW.parent_id
          USING HINT = 'Please check your parent_id';
      END IF;
    END IF;

    return NEW;

  END $$;

  CREATE OR REPLACE FUNCTION create_queue_partition(
    t_stamp date
  )
  RETURNS VOID
  LANGUAGE plpgsql AS $$
  DECLARE
    t_name text;
    t_end date;
    t_start date;
  BEGIN

    t_start := t_stamp;
    t_end := t_stamp + interval '1 month';

    SELECT INTO t_name REGEXP_REPLACE('queue_' || t_stamp::text, '-', '', 'g');

    EXECUTE format('
       CREATE TABLE %I PARTITION OF queue_part
         FOR VALUES FROM (%L) TO (%L)
    ', t_name, t_start, t_end);

    EXECUTE format('
      CREATE TRIGGER insert_leading_qitem
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE set_leading_qitem();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_object_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_object_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE TRIGGER check_queue_parent_id_constraint
      BEFORE INSERT ON %I
      FOR EACH ROW
      EXECUTE PROCEDURE check_queue_parent_id_constraint();
    ' , t_name);

    EXECUTE format('
      CREATE INDEX ON %I((data::jsonb->%L->>%L)) WHERE type = %L
    ' , t_name, 'create-args', 'duplicate_prevention_token', 'create_case_form');

  END $$;

  -- Save all the old data in a partition and don't worry about it
  -- anymore
  CREATE TABLE IF NOT EXISTS queue_20xx PARTITION OF queue_part
    FOR VALUES FROM ('1970-01-01') TO ('2021-01-01');

  SELECT create_queue_partition('2021-01-01');
  SELECT create_queue_partition('2021-02-01');
  SELECT create_queue_partition('2021-03-01');
  SELECT create_queue_partition('2021-04-01');
  SELECT create_queue_partition('2021-05-01');
  SELECT create_queue_partition('2021-06-01');
  SELECT create_queue_partition('2021-07-01');
  SELECT create_queue_partition('2021-08-01');
  SELECT create_queue_partition('2021-09-01');
  SELECT create_queue_partition('2021-10-01');
  SELECT create_queue_partition('2021-11-01');
  SELECT create_queue_partition('2021-12-01');

  ALTER TABLE queue RENAME TO queue_no_partition;
  ALTER TABLE queue_part RENAME TO queue;

  ALTER TABLE queue_20210101 DISABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210101 DISABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210101 DISABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210201 DISABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210201 DISABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210201 DISABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210301 DISABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210301 DISABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210301 DISABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210401 DISABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210401 DISABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210401 DISABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210501 DISABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210501 DISABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210501 DISABLE trigger check_queue_parent_id_constraint ;

  INSERT INTO public.queue
    SELECT * FROM queue_no_partition;

  ALTER TABLE queue_20210101 ENABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210101 ENABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210101 ENABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210201 ENABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210201 ENABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210201 ENABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210301 ENABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210301 ENABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210301 ENABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210401 ENABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210401 ENABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210401 ENABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_20210501 ENABLE trigger insert_leading_qitem ;
  ALTER TABLE queue_20210501 ENABLE trigger check_queue_object_id_constraint ;
  ALTER TABLE queue_20210501 ENABLE trigger check_queue_parent_id_constraint ;

  ALTER TABLE queue_no_partition DROP CONSTRAINT "queue_status_check";
  ALTER TABLE queue ADD CHECK (status IN ('pending', 'running', 'finished', 'failed', 'waiting', 'cancelled', 'postponed'));

COMMIT;
