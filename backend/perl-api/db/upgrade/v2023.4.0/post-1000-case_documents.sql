
BEGIN;

  DELETE FROM file_case_document where file_id IN (select id from file where destroyed = true);
  DELETE FROM file_case_document where file_id IN (select id from file where accepted = true and active_version = false);

COMMIT;
