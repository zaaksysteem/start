BEGIN;

-- Direction: incoming/outgoing.
ALTER TABLE transaction
    ADD COLUMN direction
        CHARACTER VARYING(255)
        NOT NULL
        DEFAULT 'incoming'
        CHECK(direction = 'incoming' OR direction = 'outgoing');


-- last_error: errormessage describing the problem for this row.
ALTER TABLE transaction_record ADD COLUMN last_error TEXT;

COMMIT;