BEGIN;

  CREATE TABLE case_notes (
    id SERIAL PRIMARY KEY NOT NULL,
    uuid UUID UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    case_id INT NOT NULL REFERENCES zaak(id),
    subject_id INT NOT NULL REFERENCES subject(id),
    date_created timestamp not null default now(),
    date_modified timestamp not null default now(),
    value TEXT NOT NULL
  );

COMMIT;
