
BEGIN;

  INSERT INTO public.case_notes (uuid, case_id, subject_id, value, date_created, date_modified)
    SELECT od.uuid, z.id, s.id, od.properties::jsonb->'values'->'content'->>'value', od.date_created, od.date_modified
    FROM object_data od
    JOIN
      object_relation rel
    ON
      rel.object_id = od.uuid
    JOIN
      zaak z
      ON
      z.uuid = rel.object_uuid
    JOIN
      subject s
      ON
      s.uuid = (od.properties::jsonb->'values'->'owner_id'->>'value')::uuid
      ;

COMMIT;
