DROP INDEX IF EXISTS logging_secondary_objects_idx;
CREATE INDEX logging_secondary_objects_idx ON logging USING gin ((event_data::jsonb->'related_uuids')) WHERE component = 'custom_object';

