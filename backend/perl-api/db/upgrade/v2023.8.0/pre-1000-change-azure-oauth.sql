
BEGIN;

  UPDATE interface SET module = 'authjwt' WHERE module = 'azure_oauth2';

COMMIT;
