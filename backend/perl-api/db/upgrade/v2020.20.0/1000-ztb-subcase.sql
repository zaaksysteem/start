BEGIN;

  ALTER TABLE zaaktype_relatie add column copy_related_cases boolean default false not null;
  ALTER TABLE zaaktype_relatie add column copy_related_objects boolean default false not null;

COMMIT;
