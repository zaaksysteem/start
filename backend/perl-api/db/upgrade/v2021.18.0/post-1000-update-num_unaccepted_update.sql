BEGIN;

  WITH cte AS
  ( select case_id as case_id, (value_v0->0->'value')::int as value from case_property where name = 'unaccepted_update_count' and (value_v0->0->'value')::int > 0)
  UPDATE
    zaak_meta zm
  SET
    unaccepted_attribute_update_count = cte.value
  FROM cte
  WHERE
    zm.zaak_id = cte.case_id;

  WITH cte AS
  ( select case_id as case_id, (value_v0->0->'value')::int as value from case_property where name = 'unaccepted_document_count' and (value_v0->0->'value')::int > 0)
  UPDATE
    zaak_meta zm
  SET
    unaccepted_files_count = cte.value
  FROM cte
  WHERE
    zm.zaak_id = cte.case_id;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'update_pending_changes_for_migration',
    CONCAT('api-v0: Populate pending_changes case ', zaak_id),
    950,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', zaak_id
    )
  FROM
    zaak_meta
  WHERE
    unaccepted_attribute_update_count > 0;
  ;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'update_zaak_bag_cache_for_migration',
    CONCAT('api-v0: Populate zaak bag for case ', id),
    800,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', id
    )
  FROM
    zaak
  WHERE
    locatie_zaak IS NOT NULL
  ;

COMMIT;
