CREATE INDEX CONCURRENTLY thread_search_idx ON thread USING gin(
   (LOWER(CAST(last_message_cache AS json)->>'slug')) gin_trgm_ops,
   (LOWER(CAST(last_message_cache AS json)->>'subject')) gin_trgm_ops,
   (LOWER(CAST(last_message_cache AS json)->>'created_name')) gin_trgm_ops,
   (LOWER(CAST(last_message_cache AS json)->>'recipient_name')) gin_trgm_ops
);
