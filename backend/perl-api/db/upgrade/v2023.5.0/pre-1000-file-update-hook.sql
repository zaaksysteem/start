
BEGIN;

  CREATE OR REPLACE FUNCTION update_case_document_cache_for_file()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE file_case_document SET file_id = NEW.id WHERE file_id = NEW.id;
    RETURN NULL;
  END
  $$;


  DROP TRIGGER IF EXISTS update_case_document_cache_for_file ON "file";
  CREATE TRIGGER update_case_document_cache_for_file
     AFTER UPDATE
     OF confidential, name, extension
     ON file
     FOR EACH ROW
     EXECUTE PROCEDURE update_case_document_cache_for_file();

COMMIT;
