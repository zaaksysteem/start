
BEGIN;

  ALTER TABLE saved_search ALTER COLUMN sort_order DROP NOT NULL;
  ALTER TABLE saved_search ALTER COLUMN sort_column DROP NOT NULL;

COMMIT;
