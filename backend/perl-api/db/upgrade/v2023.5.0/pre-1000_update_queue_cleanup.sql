-- delete items from queue table older than 120 days and status != pending or running
-- this script will run using a cronjob. to limit the number of records to delete / io, a limit of 10000 is used
BEGIN;

    CREATE OR REPLACE FUNCTION cleanup_queue() RETURNS void LANGUAGE plpgsql AS $$    
    DECLARE
    BEGIN
        ALTER TABLE queue DROP CONSTRAINT queue_parent_id_fkey;

        UPDATE queue set parent_id = null WHERE status not in ('pending', 'running') and date_created < CURRENT_DATE - '120 days'::INTERVAL and parent_id is not null;

        update queue set parent_id = null WHERE parent_id in (select id from queue where status not in ('pending', 'running') and date_created < CURRENT_DATE - '120 days'::INTERVAL);
        delete from queue where id in (select id from queue where status not in ('pending', 'running') and date_created < CURRENT_DATE - '120 days'::INTERVAL);

        ALTER TABLE queue ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.queue(id) ON DELETE SET NULL DEFERRABLE INITIALLY IMMEDIATE;
    END;
    $$;

COMMIT;
