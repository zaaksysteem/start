BEGIN;

CREATE OR REPLACE FUNCTION casetype_v1_attribute_to_json(
    IN r hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    h hstore;
    p jsonb;
    _key text;
    _value text;
    opt jsonb;
    bkv record;
  BEGIN

    IF (r->'is_group')::boolean = true
    THEN
      json := jsonb_build_object(
          'required', false,
          'label_multiple', null,
          'is_group', true,
          'help', r->'help',
          'values', null,
          'multiple_values', null,
          'type', null,
          'id', (r->'id')::int,
          'label', r->'label'
        );
        RETURN;
    ELSE

      IF r->'object_id' IS NOT NULL
      THEN
        SELECT INTO bkv * FROM object_data where uuid = (r->'object_id')::uuid
          AND object_class = 'type';

        p := (r->'object_metadata')::jsonb;

        json := jsonb_build_object(
          'is_group', false,
          'values', null,
          'type', 'object',
          'catalogue_id', (r->'bibliotheek_kenmerken_id')::int,
          'required', (r->'value_mandatory')::boolean,
          'help', r->'help',
          'id', (r->'id')::int,
          'label', r->'label',
          'object_type_prefix', (bkv.properties)::jsonb->'values'->'prefix'->>'value',
          'object_metadata', jsonb_build_object(
            'create_object_action_label', NULLIF(p->>'create_object_action_label', ''),
            'update_object_action_label', NULLIF(p->>'update_object_action_label', ''),
            'relate_object_action_label', NULLIF(p->>'relate_object_action_label', ''),
            'delete_object_action_label', NULLIF(p->>'delete_object_action_label', ''),
            'create_object', text_to_bool(p->>'create_object'),
            'update_object', text_to_bool(p->>'text_to_bool'),
            'relate_object', text_to_bool(p->>'relate_object'),
            'delete_object', text_to_bool(p->>'delete_object')
          ),
          'label_multiple', r->'label_multiple',
          'multiple_values', (r->'type_multiple')::boolean
        );

      ELSIF r->'custom_object_uuid' IS NOT NULL
      THEN
        -- This isn't a valid representation anymore. Custom objects have
        -- become a relationship type. Casetypes which display this are broken.
        json := jsonb_build_object(
          'is_group', false,
          'values', null,
          'type', 'custom_object',
          'catalogue_id', (r->'bibliotheek_kenmerken_id')::int,
          'required', (r->'value_mandatory')::boolean,
          'help', r->'help',
          'id', (r->'id')::int,
          'label', r->'label',
          'custom_object_uuid', r->'custom_object_uuid',
          'authorisation', null,
          'create_object_action_label', (r->'object_metadata')::jsonb->'create_object_action_label'
        );

      ELSE

        p := (r->'properties')::jsonb;
        p := p || jsonb_build_object('show_on_map', text_to_bool(p->>'show_on_map'::text));

        IF p->'map_case_location' IS NOT NULL
        THEN
          p := p || jsonb_build_object('map_case_location', text_to_bool(p->>'map_case_location'::text));
        END IF;

        IF p->>'map_wms_feature_attribute_id' IS NOT NULL AND p->>'map_wms_feature_attribute_id' ~ E'^\\d+$'
        THEN
          p := p || jsonb_build_object('map_wms_feature_attribute_id', (p->>'map_wms_feature_attribute_id')::int);
        END IF;

        FOR _key, _value IN SELECT * FROM jsonb_each_text((r->'orig_properties')::jsonb)
        LOOP
            p :=jsonb_set(p, ARRAY[_key], to_jsonb(_value));
        END LOOP;

        opt := '[]'::jsonb;
        IF r->'value_type' = ANY ( ARRAY['option', 'select', 'checkbox'] )
        THEN
          FOR bkv IN
            SELECT * FROM bibliotheek_kenmerken_values WHERE bibliotheek_kenmerken_id = (r->'bibliotheek_kenmerken_id')::int
            ORDER BY sort_order
          LOOP
            opt := opt || jsonb_build_object(
              'id', bkv.id,
              'value', bkv.value,
              'active', CASE WHEN bkv.active = true THEN 1 ELSE 0 END,
              'sort_order', bkv.sort_order
            );
          END LOOP;
        END IF;

        IF r->'value_type' = 'relationship'
        THEN
          p := p || jsonb_build_object(
            'relationship_type', r->'relationship_type',
            'relationship_name', r->'relationship_name',
            'relationship_uuid', r->'relationship_uuid'
          );
        END IF;

        IF p->'date_limit' IS NOT NULL
        THEN

          IF p->'date_limit'->'start'->>'reference' = 'current'
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'reference'], to_jsonb('now'::text));
          END IF;

          IF p->'date_limit'->'start'->>'num' = ''
          THEN
            p := jsonb_set(p, array['date_limit', 'start', 'num'], 'null');
          END IF;
          IF p->'date_limit'->'end'->>'num' = ''
          THEN
            p := jsonb_set(p, array['date_limit', 'end', 'num'], 'null');
          END IF;

          IF p->'date_limit'->'start'->>'num' IS NOT NULL AND p->'date_limit'->'start'->>'num' != ''
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'num'], to_jsonb(trunc((p->'date_limit'->'start'->>'num')::decimal,0)::int));
          END IF;

          IF p->'date_limit'->'start'->>'active' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'start', 'active'], to_jsonb((p->'date_limit'->'start'->>'active')::boolean));
          END IF;

          IF p->'date_limit'->'end'->>'reference' = 'current'
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'reference'], to_jsonb('now'::text));
          END IF;

          IF p->'date_limit'->'end'->>'num' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'num'], to_jsonb(trunc((p->'date_limit'->'end'->>'num')::decimal,0)::int));
          END IF;

          IF p->'date_limit'->'start'->>'active' IS NULL
          THEN
            p := jsonb_set(p, array['date_limit', 'start', 'active'], to_jsonb(false));
          END IF;
          IF p->'date_limit'->'end'->>'active' IS NULL
          THEN
            p := jsonb_set(p, array['date_limit', 'end', 'active'], to_jsonb(false));
          END IF;

          IF p->'date_limit'->'end'->>'active' IS NOT NULL
          THEN
            p := jsonb_set(p, ARRAY['date_limit', 'end', 'active'], to_jsonb((p->'date_limit'->'end'->>'active')::boolean));
          END IF;

        END IF;

        IF r->'label' = ''
        THEN
          r := r || 'label=>null'::hstore;
        END IF;

        IF p->>'text_content' IS NOT NULL
        THEN
          json := jsonb_build_object(
            'help', r->'help',
            'id', (r->'id')::int,
            'is_group', false,
            'properties', p,
            'label', COALESCE(r->'label', r->'original_label'),
            'label_multiple', r->'label_multiple',
            'multiple_values', null,
            'required', (r->'value_mandatory')::boolean,
            'type', 'text_block',
            'values', null
          );
          ELSE
            json := jsonb_build_object(
                'catalogue_id', CASE WHEN r->'bibliotheek_kenmerken_id' IS NOT NULL THEN
                  (r->'bibliotheek_kenmerken_id')::int
                ELSE null END,
                'default_value', r->'value_default',
                'help', r->'help',
                'id', (r->'id')::int,
                'is_group', false,
                'is_system', (r->'is_systeemkenmerk')::boolean,
                'label', COALESCE(r->'label', r->'original_label'),
                'label_multiple', r->'label_multiple',
                'limit_values', CASE WHEN (r->'type_multiple')::boolean = true THEN -1 ELSE 1 END,
                'magic_string', r->'magic_string',
                'multiple_values', (r->'type_multiple')::boolean,
                'original_label', r->'original_label',
                'permissions', casetype_v1_permissions(r->'required_permissions'),
                'pip', COALESCE((r->'pip')::boolean, false),
                'properties', p,
                'publish_public', COALESCE((r->'publish_public')::boolean, false),
                'referential', (r->'referential')::boolean,
                'required', (r->'value_mandatory')::boolean,
                'values', opt,
                'type', r->'value_type'
              );
        END IF;
      END IF;


    END IF;

    RETURN;

  END;
  $$;

COMMIT;