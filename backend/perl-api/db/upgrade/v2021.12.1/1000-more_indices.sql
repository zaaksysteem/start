BEGIN;

DROP INDEX IF EXISTS object_data_regdate_idx;

DROP INDEX IF EXISTS object_data_regdate_idx2;

DROP INDEX IF EXISTS object_data_target_date_idx;

DROP INDEX IF EXISTS object_data_completion_idx;

CREATE INDEX object_data_regdate_idx ON object_data ((CAST(hstore_to_timestamp (index_hstore -> 'case.date_of_registration') AS date)))
WHERE
    object_class = 'case'
    AND invalid IS FALSE;

CREATE INDEX object_data_target_date_idx ON object_data ((CAST(hstore_to_timestamp (index_hstore -> 'case.case.date_target') AS date)))
WHERE
    object_class = 'case'
    AND invalid IS FALSE;

CREATE INDEX object_data_completion_idx ON object_data ((CAST(hstore_to_timestamp (index_hstore -> 'case.date_of_completion') AS date)))
WHERE
    object_class = 'case'
    AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_status_id_idx;

CREATE INDEX object_data_case_status_id_idx ON object_data ((index_hstore -> 'case.status'))
WHERE
    object_class = 'case' AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_type_status_idx;

CREATE INDEX object_data_case_type_status_idx ON object_data ((index_hstore -> 'case.status'), ((index_hstore -> 'case.casetype.id')::numeric))
WHERE
    object_class = 'case' AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_casetype_id_idx;

CREATE INDEX object_data_case_casetype_id_idx ON object_data (((index_hstore -> 'case.casetype.id')::numeric))
WHERE
    object_class = 'case' AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_assignee_id_idx;

CREATE INDEX object_data_case_assignee_id_idx ON object_data (((index_hstore -> 'case.assignee.id')::numeric))
WHERE
    object_class = 'case' AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_requestor_id_idx;

CREATE INDEX object_data_case_requestor_id_idx ON object_data ((index_hstore -> 'case.requestor.id'))
WHERE
    object_class = 'case' AND invalid IS FALSE;

DROP INDEX IF EXISTS object_data_case_route_role_idx;

CREATE INDEX object_data_case_route_role_idx ON object_data (((index_hstore -> 'case.route_ou')::numeric), ((index_hstore -> 'case.route_ou')::numeric))
WHERE
    object_class = 'case' AND invalid IS FALSE;

COMMIT;

