BEGIN;

    ALTER TABLE filestore ADD COLUMN virus_scan_status TEXT;

    -- assume whatever we already have has been scanned
    UPDATE filestore SET virus_scan_status = 'ok';

    ALTER TABLE filestore ADD CONSTRAINT filestore_virus_scan_status_check
        CHECK (virus_scan_status ~ '^(pending|ok|found(:.*)?)$' );
        -- pending    - initial state
        -- ok         - scanned and no viruses found
        -- found: ... - optionally followed by the name of the virus(es)
    
    ALTER TABLE filestore ALTER COLUMN virus_scan_status SET DEFAULT 'pending';
    ALTER TABLE filestore ALTER COLUMN virus_scan_status SET NOT NULL;

COMMIT;
