BEGIN;

INSERT INTO roles (parent_group_id, name, description, system_role, date_created)
       SELECT id, 'App gebruiker', 'Systeemrol: Verleent enkel toegang tot apps', true, NOW() FROM groups WHERE path = array[groups.id];

COMMIT;
