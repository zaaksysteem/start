BEGIN;
  ALTER TABLE natuurlijk_persoon ADD COLUMN persoonsnummer varchar(10);
  ALTER TABLE gm_natuurlijk_persoon ADD COLUMN persoonsnummer varchar(10);
COMMIT;
