BEGIN;

  DROP VIEW IF EXISTS case_v0;

  CREATE VIEW case_v0 AS
  SELECT

    -- header info
    z.uuid AS id,
    z.id AS object_id,

    -- expensive bits
    build_case_v0_values(
      hstore(z),
      hstore(zm),
      hstore(zt),
      hstore(ztr),
      hstore(zts),
      hstore(zts_next),
      hstore(rpt),
      hstore(ztd),
      hstore(gr),
      hstore(ro),
      hstore(case_location),
      hstore(parent),
      hstore(requestor),
      hstore(recipient),
      bc.naam,
      gassign.name,
      ztn.v0_json
    ) as values,

    build_case_v0_case(hstore(zt), hstore(zm)) as case,

    -- static values
    '{}'::text[] as related_objects,
    'case' as object_type


  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  JOIN
    zaaktype zt
  ON
    z.zaaktype_id = zt.id
  INNER JOIN
    zaaktype_node ztn
  ON
    z.zaaktype_node_id = ztn.id
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  LEFT JOIN
    zaaktype_status zts
  ON
    ( z.zaaktype_node_id = zts.zaaktype_node_id
      AND zts.status = z.milestone)
  LEFT JOIN
    zaaktype_status zts_next
  ON
    ( z.zaaktype_node_id = zts_next.zaaktype_node_id
      AND zts_next.status = z.milestone + 1)
  LEFT JOIN
    result_preservation_terms rpt
  ON
    ztr.bewaartermijn = rpt.code
  LEFT JOIN
    bibliotheek_categorie bc
  ON
    zt.bibliotheek_categorie_id = bc.id
  LEFT JOIN
    zaaktype_definitie ztd
  ON
    ztn.zaaktype_definitie_id = ztd.id
  LEFT JOIN
    groups gr
  ON
    z.route_ou = gr.id
  LEFT JOIN
    roles ro
  ON
    z.route_role = ro.id
  LEFT JOIN
    zaak_bag case_location
  ON
    (case_location.id = z.locatie_zaak and z.id = case_location.zaak_id)
  LEFT JOIN
    subject assignee
  ON
    z.behandelaar_gm_id = assignee.id
  LEFT JOIN groups gassign
  ON
    assignee.group_ids[1] = gassign.id
  LEFT JOIN
    zaak parent
  ON
    z.pid = parent.id
  LEFT JOIN
    zaak_betrokkenen requestor
  ON
    (z.aanvrager = requestor.id and z.id = requestor.zaak_id and requestor.deleted IS NULL)
  LEFT JOIN zaak_betrokkenen recipient ON recipient.id = (
      select id from zaak_betrokkenen zb
      where z.id = zb.zaak_id AND zb.rol = 'Ontvanger' AND zb.deleted IS NULL
      order by id asc limit 1
  )

  WHERE z.deleted IS NULL
  ;

COMMIT;
