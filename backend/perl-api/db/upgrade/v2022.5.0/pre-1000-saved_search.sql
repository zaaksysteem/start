BEGIN;

    ALTER TABLE saved_search DROP columns;
    ALTER TABLE saved_search ADD COLUMN columns JSONB NOT NULL DEFAULT '[]'::jsonb CHECK (jsonb_typeof(columns) = 'array');

COMMIT;
