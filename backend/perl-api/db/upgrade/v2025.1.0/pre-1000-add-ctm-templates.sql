BEGIN;

CREATE OR REPLACE FUNCTION get_templates_as_json(zaak_statusid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(templates) from (
        select bs.uuid as catalog_template_uuid,
               bs.naam as catalog_template_name,
               zs.label as label,
               text_to_bool(zs.automatisch_genereren::text) as auto_generate,
               zs.help as description,
               zs.custom_filename as custom_filename,
               zs.allow_edit as allow_edit,
               coalesce(zs.target_format, 'odt') as target_format,
               bk.uuid as document_attribute_uuid,
               bk.naam as document_attribute_name
        from zaaktype_sjablonen zs
        join bibliotheek_sjablonen bs on zs.bibliotheek_sjablonen_id = bs.id 
        left join bibliotheek_kenmerken bk on bk.id = zs.bibliotheek_kenmerken_id
        where zaak_status_id = zaak_statusid
        order by zs.id) templates;
END;
$$;

CREATE OR REPLACE FUNCTION get_phases_as_json(zaaktype_nodeid int)
RETURNS table(j json)
IMMUTABLE
LANGUAGE plpgsql
AS $$
declare
begin
    return QUERY select json_agg(phases) from (select zts.status as phase_id,
        zts.naam as milestone_name,
        zts.fase as phase_name,
        zts.created,
        zts.last_modified,
        zts.termijn as term_in_days,
        case when g.uuid is null or r.uuid is null then null else
        json_build_object(
            'enabled', case when zts.status = 1 then true else text_to_bool(zts.role_set::text) end,
            'department_uuid', g.uuid,
            'department_name', g.name,
            'role_uuid', r.uuid,
            'role_name', r.name
        ) end as assignment,
        get_kenmerken_for_phase_as_json(zts.id) as custom_fields,
        get_results_as_json(zts.id) as results,
        get_tasks_as_json(zts.id) as tasks,
        get_templates_as_json(zts.id) as templates
        from zaaktype_status zts
        left join roles r on r.id = zts.role_id
        left join groups g on g.id = zts.ou_id
        where zts.zaaktype_node_id = zaaktype_nodeid
        order by zts.status) phases;
END;
$$;

COMMIT;