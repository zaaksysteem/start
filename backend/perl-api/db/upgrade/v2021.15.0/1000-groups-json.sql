BEGIN;

  ALTER TABLE groups ADD COLUMN v1_json JSONB DEFAULT '{}' NOT NULL;

  CREATE OR REPLACE FUNCTION update_group_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    DECLARE
      j_group JSONB;
    BEGIN

      SELECT INTO j_group org_unit(hstore(NEW), 'group');
      NEW.v1_json := j_group;
      RETURN NEW;
    END;
  $$;

  CREATE TRIGGER update_v1_json
    BEFORE INSERT
    OR UPDATE
    ON groups
    FOR EACH ROW
    EXECUTE PROCEDURE update_group_json();

  UPDATE groups SET uuid = uuid;

  ALTER TABLE roles ADD COLUMN v1_json JSONB DEFAULT '{}' NOT NULL;

  CREATE OR REPLACE FUNCTION update_role_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    DECLARE
      j_role JSONB;
    BEGIN

      SELECT INTO j_role org_unit(hstore(NEW), 'role');
      NEW.v1_json := j_role;
      RETURN NEW;
    END;
  $$;

  CREATE TRIGGER update_v1_json
    BEFORE INSERT
    OR UPDATE
    ON roles
    FOR EACH ROW
    EXECUTE PROCEDURE update_role_json();

  UPDATE roles SET uuid = uuid;

COMMIT;
