BEGIN;

  CREATE OR REPLACE FUNCTION subject_company_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address_cor json;
    address_res json;

    latitude float;
    longitude float;

  BEGIN

    SELECT INTO json subject_json(subject, 'company');

    latitude := ((subject->'vestiging_latlong')::point)[0];
    longitude := ((subject->'vestiging_latlong')::point)[1];

    IF (
        subject->'vestiging_adres_buitenland1' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland2' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'vestiging_straatnaam' IS NOT NULL
          AND subject->'vestiging_postcode' IS NOT NULL
      )
    THEN
      address_res := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'vestiging_bag_id')::bigint,
          'city', subject->'vestiging_woonplaats',
          'street', subject->'vestiging_straatnaam',
          'street_number', (subject->'vestiging_huisnummer')::int,
          'street_number_suffix', subject->'vestiging_huisnummertoevoeging',
          'street_number_letter', subject->'vestiging_huisletter',
          'foreign_address_line1', subject->'vestiging_adres_buitenland1',
          'foreign_address_line2', subject->'vestiging_adres_buitenland2',
          'foreign_address_line3', subject->'vestiging_adres_buitenland3',
          'country', (subject->'vestiging_country')::jsonb,
          'zipcode', subject->'vestiging_postcode',
          'latitude', latitude,
          'longitude', longitude,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF (
        subject->'correspondentie_adres_buitenland1' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland2' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'correspondentie_straatnaam' IS NOT NULL
          AND subject->'correspondentie_postcode' IS NOT NULL
      )
    THEN
      address_cor := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'correspondentie_bag_id')::bigint,
          'city', subject->'correspondentie_woonplaats',
          'street', subject->'correspondentie_straatnaam',
          'street_number', (subject->'correspondentie_huisnummer')::int,
          'street_number_suffix', subject->'correspondentie_huisnummertoevoeging',
          'street_number_letter', subject->'correspondentie_huisletter',
          'foreign_address_line1', subject->'correspondentie_adres_buitenland1',
          'foreign_address_line2', subject->'correspondentie_adres_buitenland2',
          'foreign_address_line3', subject->'correspondentie_adres_buitenland3',
          'zipcode', subject->'correspondentie_postcode',
          'country', (subject->'correspondentie_country')::jsonb,
          'latitude', null,
          'longitude', null,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF subject->'main_activity' = '{}'
    THEN
      SELECT INTO subject subject || hstore('main_activity', null);
    END IF;

    IF subject->'secondairy_activities' = '[]'
    THEN
      SELECT INTO subject subject || hstore('secondairy_activities', null);
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email_address',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'coc_number', lpad(subject->'dossiernummer', 8, '0'),
        'coc_location_number', lpad(subject->'vestigingsnummer', 12, '0'),
        'date_ceased', (subject->'date_ceased')::date,
        'date_founded', (subject->'date_founded')::date,
        'date_registration', (subject->'date_registration')::date,
        'main_activity', (subject->'main_activity')::jsonb,
        'secondairy_activities', (subject->'secondairy_activities')::jsonb,
        'company_type', (subject->'company_type')::jsonb,
        'oin', (subject->'oin')::bigint,
        'rsin', (subject->'rsin')::bigint,
        'company', (subject->'handelsnaam')::text,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION case_subject_json(
    IN zb hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;

  BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id := (zb->'gegevens_magazijn_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_number_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
      FROM
        natuurlijk_persoon np
      LEFT JOIN
        adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = 1)

      WHERE
        np.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      cd.mobiel as mobile,
      cd.telefoonnummer as phone_number,
      cd.email as email_address,
      cd.email as wtf,
      cd.id as huh,
      gm_id as huhu,
      let.json as company_type
      FROM
        bedrijf b
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = b.id and cd.betrokkene_type = 2)
      WHERE
        b.id = gm_id
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW()),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;

COMMIT;
