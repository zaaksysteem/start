=head1 NAME

ezra_maps - Maps plugin designed to work with openlayers and custom maps

=head1 SYNOPSIS

    <html>
    <h1>Select a adress by clicking</h1>
    <div>
        <div class="ezra_map ezra_map-small">
            <div class="ezra_map-viewport"></div>
            <input type="text" class="ezra_map-autocomplete" name="address_search" placeholder="Zoek op adres (e.g. Amsterdam, Damrak 5)">
        </div>
    </div>
    </html>

    <script>
    $(document).ready(function() {
        // Initialize ezra_map somewhere in doucment.ready
        $('.ezra_map').ezra_map({
            layer_type  : 'pdoc',
            theme       : '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/style.css',
            marker_icon : '/images/marker.png',
            viewport    : {
                small:  {
                    css: {
                        width: '512px',
                        height: '350px',
                    },
                },
                medium:  {
                    css: {
                        width: '1024px',
                        height: '768px',
                    },
                },
                large:  {
                    css: {
                        width: '512px',
                        height: '256px',
                    },
                },
            },
        });

        $('.ezra_map').each(function() {
            // Add a marker
            $(this).ezra_map('setAddress', 'Amsterdam, Marnixstraat 32');
        });
    });
    </script>

=head1 DESCRIPTION

Using this module you will be able to generate a openlayers map in your
application with features like: place multiple markers, autocomplete search on
addresses, place markers by clicking on the map, etc. Follow above synopsis
for a quickstart how to implement it on your website.

=head2 REQUIREMENTS

The following modules are required to get the full potential of this plugin

=over 4

=item jQuery v1.8 or later

=item jQueryUI (including Autocomplete)

=item Backend server providing geocoding possibilities, see below

=back

=head1 HTML

A standard snippet of html is below. Additionally, you can define classes on
the div.ezra_map html code to change certain funcionality, see below.


    <div>
        <div class="ezra_map ezra_map-small">
            <div class="ezra_map-viewport"></div>
        </div>
    </div>

=over 4

=item readonly

You can make the map readonly, which disables the functionality to add markers
from the client.

    <div>
        <div class="ezra_map ezra_map-readonly ezra_map-small">
            <div class="ezra_map-viewport"></div>
        </div>
    </div>

=item multiple markers

Add default markers to the map by adding input fields without using the
L<setMarker> method. . It supports text or hidden input fields. Make sure the
B<ezra_map-address> class is set on the input tag.

    <div>
        <div class="ezra_map ezra_map-readonly ezra_map-small">
            <input type="hidden" class="ezra_map-address" value="Amsterdam, Donker Curtiusstraat 7" />
            <input type="hidden" class="ezra_map-address" value="Amsterdam, Damrak 6" />
            <div class="ezra_map-viewport"></div>
        </div>
    </div>

=item autocomplete

Add an input type B<text> to the html, which transforms into an autocomplete
search for an address box. Spiffy eh? As above, make sure the class
B<ezra_map-autocomplete> is added.

    <div>
        <div class="ezra_map ezra_map-readonly ezra_map-small">
            <div class="ezra_map-viewport"></div>
            <input type="text" class="ezra_map-autocomplete" name="address_search" placeholder="Zoek op adres (e.g. Amsterdam, Damrak 5)">
        </div>
    </div>

=item center location

Make sure the map is centered on a certain position by adding a hidden input
field with class B<ezra_map-center>

    <div>
        <div class="ezra_map ezra_map-readonly ezra_map-small">
            <input type="hidden" class="ezra_map-center" value="4.8958764 52.3752124" />
            <div class="ezra_map-viewport"></div>
        </div>
    </div>

=back

=head1 OPTIONS

=over 4

=item layer_type (REQUIRED)

Now only 'pdoc' is supported, and generates a standard dutch map

=item theme (REQUIRED)

Define the CSS location for the used theme, or leave empty

=item viewport (REQUIRED)

Using a named hash, you can define profiles for displaying the map, like size.

=item marker_icon (OPTIONAL)

Optionally, specify an icon to use when showing a marker.

=item geocode_url (OPTIONAL)

When a B<geocode_url> is provided, the app will use this url for geocoding
information.

=item center (OPTIONAL)

When given a string containing google coordinates seperated by a space, it
will center the map on this point.

=back

=head1 METHODS

=head2 clearMarkers()

Clear all markers from the map.

  $('.ezra_map').ezra_map('clearMarkers');

=head2 setCenter(point_x, point_y)

Center the map on the given x and y coordinates

  $('.ezra_map').ezra_map('setCenter', '119875.800858', '487916.40');

B<Options>

=over 4

=item point_x

Longtitude or X coordinate

=item point_y

Longtitude or Y coordinate

=back

=head2 setMarker(point_x, point_y, popup_content, [center_to, no_popup])

Put a marker on the map;

  $('.ezra_map').ezra_map(
    'setMarker',
    '119875.800858',
    '487916.40',
    '<h1>Adres</h1>Damrak 5'
  );

B<Options>

=over 4

=item point_x

Longtitude or X coordinate

=item point_y

Longtitude or Y coordinate

=item popup_content

The HTML content for the popup to show

=item center_to

Type: boolean

Defining if we center the map on this position after placing the marker

=item no_popup

Type: boolean

Prevents the popup from showing after the marker is set.

=back

=head2 setAddress(address [, only_center])

Ask your application for an address. This function will ignite a JSON call,
providing a parameter B<term> containing an address which could also be found
with L<maps.google.com>.

  $('.ezra_map').ezra_map('setAddress', 'Amsterdam, Damrak 5');

B<Options>

=over 4

=item address

Given address, e.g. B<Damrak 5>

=item only_center

Don't place a marker on this address, just center the map on this location

=back

B<CALL TO BACKEND>

Will make a call to url B<geocode_url> with parameter B<term>, e.g.
L<http://www.example.com/geocode?term=Damrak>

It expects a JSON hash containing an object B<json> containing the following
structure

  json: {
      success: 1,
      addresses: [
        {
            search: Damrak,
            coordinates: ["4.8958764", "52.3752124", 0],
            address: "The Netherlands, North Holland, Amsterdam",
            epsg28992: [121517.059364829 , 487483.051691071 ]
        }
      ]
  }


=over 4

=item success

Type: boolean

Defines if the call was a success or not

=item addresses

Type: anonymous array

Array containing hashes with the B<search> as original search query, B<coordinates> for the original Google coordinates, B<address> for the address returned by google and most importantly B<epsg28992> containing the point_x and point_y coordinates.

=back

=head2 destroy()

Destroys the ezra_map

  $('.ezra_map').ezra_map('destroy');

=cut

=head1 SUPPORT

Web:

    http://zaaksysteem.nl/

GitHub (Issues and source)

    https://github.com/mintlab/Zaaksysteem

=head1 SEE ALSO

Zaaksysteem.nl main application using this plugin extensively

    http://zaaksysteem.nl/

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut
