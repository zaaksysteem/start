fetch("/api/v2/style/get_content?name=config")
  .then((r) => r.json())
  .then((content) => {
    const id = Number(content.siteImprovId);
    const documentTitle = content.documentTitle;
    const mainAddress = content.mainAddress;
    const logoLinkEl = document.querySelector("#logo>a");

    if (id) {
      const sz = document.createElement("script");
      sz.type = "text/javascript";
      sz.async = true;
      sz.src = "//siteimproveanalytics.com/js/siteanalyze_" + id + ".js";
      const s = document.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(sz, s);
    }

    if (documentTitle) {
      document.title = `${document.title} - ${documentTitle}`;
    }

    if (logoLinkEl && mainAddress) {
      const url = new URL(mainAddress);
      const weblink = url.protocol === "http:" || url.protocol === "https:";
      const normal = !mainAddress.includes("?");
      weblink && normal && (logoLinkEl.href = mainAddress);
    }
  });
