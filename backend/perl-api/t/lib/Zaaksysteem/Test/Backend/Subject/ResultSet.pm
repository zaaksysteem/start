package Zaaksysteem::Test::Backend::Subject::ResultSet;
use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::Backend::Subject::ResultSet - Test the subject component class

=head1 DESCRIPTION

Tests for the subject component class

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Subject::ResultSet

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Subject::ResultSet;

sub test_create_user {
    my $last_create;

    my $mock_user_entity_rs = mock_one(
        search => sub {
            return mock_one(
                first => sub { return; }
            );
        },
    );

    my @mock_subjects = (
        mock_one(
            user_entities => sub {
                return mock_one(
                    create => sub {
                        cmp_deeply(
                            $_[0],
                            {
                                source_identifier => 'pietje',
                                source_interface_id => 42,
                                subject_id => 9,
                                active => 1,
                            },
                            'user_entity created correctly',
                        );
                        return "user_entity";
                    },
                )
            },
            id => 9,
        ),
        mock_one(
            id => 9,
            user_entities => sub {
                return mock_one(
                    create => sub {
                        cmp_deeply(
                            $_[0],
                            {
                                source_identifier   => 'pietje',
                                source_interface_id => 42,
                                subject_id => 9,
                                password   => 'generated',
                                active => 1,
                            },
                            'user_entity created correctly (with password)',
                        );
                        return "user_entity";
                    },
                )
            },
        ),
    );

    my $mock_schema = mock_one(
        resultset => sub {
            if ($_[0] eq 'UserEntity') {
                return $mock_user_entity_rs;
            }
            else {
                fail("Unknown resultset requested: $_[0]");
            }
        },
    );
    my $mock_source = mock_one(
        schema => sub { return $mock_schema; }
    );

    my $mock_self = mock_one(
        default_create_user_profile => sub {
            return {
                required    => [qw/
                    username
                    interface
                /],
                optional    => [qw/
                    role_ids

                    set_behandelaar
                    group_id

                    empty_user
                    password
                /],
            };
        },
        result_source => sub { return $mock_source },
        search => sub {
            return mock_one(
                count => sub { 0; },
            );
        },
        create => sub {
            $last_create = [@_];

            return shift @mock_subjects;
        },
    );

    my $mock_interface = mock_one(
        module_object => sub {
            return mock_one(
                subject_type => sub { 'employee' },
            );
        },
        id => sub { 42; },
    );

    is(
        Zaaksysteem::Backend::Subject::ResultSet::create_user(
            $mock_self,
            {
                username   => 'pietje',
                interface  => $mock_interface,
                empty_user => 1,
            }
        ),
        'user_entity',
        'User created successfully',
    );

    my $override = override('Crypt::SaltedHash::generate' => sub { return 'generated' });

    is(
        Zaaksysteem::Backend::Subject::ResultSet::create_user(
            $mock_self,
            {
                username   => 'pietje',
                interface  => $mock_interface,
                password   => 'supergeheim',
                empty_user => 1,
            }
        ),
        'user_entity',
        'User created successfully',
    );
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
