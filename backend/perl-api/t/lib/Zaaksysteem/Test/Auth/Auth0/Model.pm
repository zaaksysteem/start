package Zaaksysteem::Test::Auth::Auth0::Model;

use Zaaksysteem::Test;

use Zaaksysteem::Auth::Auth0::Model;
use BTTW::Tools::RandomData qw(generate_uuid_v4);

use DateTime;

=head1 NAME

Zaaksysteem::Test::Auth::Auth0::Model - Test Auth0 abstractions model

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Auth::Auth0::Model

=cut

sub test_create_auth_state {

    my $store = {};
    my $expire = {};
    my $redis = mock_one(
        'X-Mock-SelfArg' => 1,
        set => sub {
            my $self = shift;
            my $key = shift;
            my $value = shift;
            $store->{$key} = $value;
        },
        expire_at => sub {
            my $self = shift;
            my $key  = shift;
            my $time = shift;
            $expire->{$key} = $time;
        }
    );

    my $model = Zaaksysteem::Auth::Auth0::Model->new(
        redis => $redis
    );

    my $date_expires = DateTime->now->add(minutes => 4);
    my $session_id = generate_uuid_v4;

    my $token = $model->create(
        {
            date_expires => $date_expires,
            session_id   => $session_id,
        }
    );

    is(keys %$store, 1, "Got one token stored in Redis");
    my ($key) = keys %$store;
    is(
        $key,
        "auth0:state:" . $session_id,
        "... and is the token we have stored"
    );
    cmp_deeply(
        $store->{$key},
        $token,
        "... and correct information is stored"
    );

    cmp_deeply(
        $expire,
        { $key => $date_expires->epoch },
        "... with the correct expiration date"
    );    
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017-2024, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
