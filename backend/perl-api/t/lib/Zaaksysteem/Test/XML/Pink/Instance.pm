package Zaaksysteem::Test::XML::Pink::Instance;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::XML::Pink::Instance;
use Data::Random::NL qw(generate_bsn);

sub test_pink_voa {

    my $pink = Zaaksysteem::XML::Pink::Instance->new(
        home => '.'
    );

    isa_ok($pink, "Zaaksysteem::XML::Pink::Instance");

    throws_ok(
        sub {
            $pink->set_pink_voa();
        },
        qr#missing: bsn_or_anummer#,
        "profile checks"
    );


    my $bsn = int(generate_bsn(0));
    my $anummer = sprintf("1%09d", $bsn);

    my $xml;
    lives_ok(
        sub {
           $xml = $pink->set_pink_voa({burgerservicenummer => $bsn});
        },
        "8 digit BSN ($bsn) works",
    );
    like($xml, qr#<CML:BSN>0$bsn</CML:BSN>#, ".. and BSN 0$bsn  found in XML");

    $bsn = generate_bsn(9);
    lives_ok(
        sub {
           $xml = $pink->set_pink_voa({burgerservicenummer => $bsn});
        },
        "9 digit BSN ($bsn) works too",
    );
    like($xml, qr#<CML:BSN>$bsn</CML:BSN>#, ".. and BSN $bsn found in XML");

    lives_ok(
        sub {
            $xml = $pink->set_pink_voa({a_nummer => $anummer});
        },
        ".. and A-number also works"
    );
    like($xml, qr#<CML:Anummer>$anummer</CML:Anummer>#, ".. And A-number found in XML");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::XML::Pink::Instance - Test ZS::Test::XML::Pink::Instance

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::XML::Pink::Instance

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
