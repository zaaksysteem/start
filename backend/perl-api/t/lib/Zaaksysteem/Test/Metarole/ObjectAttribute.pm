package Zaaksysteem::Test::Metarole::ObjectAttribute;

use Zaaksysteem::Test;

use Zaaksysteem::Object::Value;

package MetaroleAttributeTestObject {
    use Moose;

    extends 'Zaaksysteem::Object';

    has foo => (
        is => 'rw',
        isa => 'Str',
        type => 'string',
        label => 'foo attr',
        traits => [qw[OA]],
    );
};

sub test_object_attribute_zss_value {
    my $object = MetaroleAttributeTestObject->new(foo => 'abc');

    my $value = $object->_zss_get('foo');

    isa_ok $value, 'Zaaksysteem::Object::Value', '_zss_get dereferences value instance';

    is $value->type_name, 'string', '_zss_get type name';

    my $new_value = Zaaksysteem::Object::Value->new(
        type => $value->type,
        value => 'xyz'
    );

    lives_ok {
        $object->_zss_set('foo', $new_value);
    } 'object accepts new value';

    is $object->foo, 'xyz', 'value was set';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
