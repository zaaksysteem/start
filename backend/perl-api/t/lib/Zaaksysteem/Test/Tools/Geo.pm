package Zaaksysteem::Test::Tools::Geo;

use Zaaksysteem::Test;

use Zaaksysteem::Tools::Geo qw(
    point_to_geo_json
);

sub test_point_to_geo_json {

    is(point_to_geo_json(), undef, "No value, not point, no geo");

    cmp_deeply(
        point_to_geo_json('(1,0)'),
        {
            type     => 'Feature',
            geometry => {
                type        => 'Point',
                coordinates => [0, 1]
            },
        },
        "Point to GeoJSON conversion done correctly"
    );

    cmp_deeply(
        point_to_geo_json('1,0'),
        {
            type     => 'Feature',
            geometry => {
                type        => 'Point',
                coordinates => [0, 1]
            },
        },
        "... and point-y-ish is also done correctly",
    );


}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
