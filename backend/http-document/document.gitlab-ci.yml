# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

stages:
    - Tag
    - QA
    - Build
    - Verify
    - Release

.http_document_job_variables:
  variables:
    BACKEND_PATH: "backend/http-document"
    ADD_JOB_ON_CHANGES_OF_1: "backend/domains/**/*"
    ADD_JOB_ON_CHANGES_OF_2: "backend/migration_libraries/**/*"
    ADD_JOB_ON_CHANGES_OF_3: "backend/zsnl-pyramid/**/*"
    ADD_JOB_ON_CHANGES_OF_4: "backend/minty/**/*"
    ADD_JOB_ON_CHANGES_OF_5: "backend/minty-infra-*/**/*"
    ADD_JOB_ON_CHANGES_OF_6: "backend/minty-pyramid/**/*"

http-document:Tag latest container when there are no changes:
  extends: 
    - .http_document_job_variables
    - .tag_latest_backend_container_template
  # only is used to add this job only when the app or ci template is NOT changed
  # This is not supported yet by rules
  only:
    variables:
      - $CI_COMMIT_TAG =~ /^release\//
      - $CI_COMMIT_BRANCH =~ /^(master|production|preprod|development)$/
  except:
    refs:
      - schedule
      - web
    changes:
      - "backend/http-document/**/*"
      - "template.gitlab-ci.yml"
      - "backend/domains/**/*"
      - "backend/migration_libraries/**/*"
      - "backend/zsnl-pyramid/**/*"
      - "backend/minty/**/*"
      - "backend/minty-infra-*/**/*"
      - "backend/minty-pyramid/**/*"

http-document:REUSE Compliance:
  extends: 
    - .http_document_job_variables
    - .reuse_compliance_template

http-document:OpenAPI Lint:
  extends: 
    - .http_document_job_variables
    - .openapi_lint_template

http-document:Run Python tests:
  extends: 
    - .http_document_job_variables
    - .python_tests_template

.build_container_image_http_document:
    extends:
      - .http_document_job_variables
      - .build_and_push_container_image_template
    needs:
      - "set-version-number-and-tag-commit"
      - "http-document:REUSE Compliance"
      - "http-document:OpenAPI Lint"
      - "http-document:Run Python tests"
      - job: "minty-pyramid:REUSE Compliance"
        optional: true
      - job: "minty-pyramid:Run Python tests"
        optional: true
      - job: "domains:REUSE Compliance"
        optional: true
      - job: "domains:Run Python tests"
        optional: true
      - job: "migration_libraries:REUSE Compliance"
        optional: true
      - job: "migration_libraries:Run Python tests"
        optional: true
      - job: "pyramid:REUSE Compliance"
        optional: true
      - job: "pyramid:Run Python tests"
        optional: true


http-document:Build container image (x86_64):
  extends: .build_container_image_http_document
  tags: [ "xxllnc-shared", "arch:x86_64" ]
  variables:
    BUILD_ARCH: "amd64"

http-document:Build container image (arm64):
  extends: .build_container_image_http_document
  tags: [ "xxllnc-shared", "arch:arm64" ]
  variables:
    BUILD_ARCH: "arm64"

http-document:Make multiarch manifest:
  extends:
    - .http_document_job_variables
    - .make_multiarch_manifest_template
  variables:
    BUILD_ARCHS: "amd64 arm64"
  needs:
    - "set-version-number-and-tag-commit"
    - "http-document:Build container image (x86_64)"
    - "http-document:Build container image (arm64)"

http-document:Create SBOM:
    extends:
      - .http_document_job_variables
      - .create_sbom_template
    needs: ["http-document:Make multiarch manifest"]

#
# Release targets
#
http-document:Tag xcp release container:
  extends:
    - .http_document_job_variables    
    - .release_on_xcp
