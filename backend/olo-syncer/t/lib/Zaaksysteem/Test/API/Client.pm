package Zaaksysteem::Test::API::Client;

use Test::Class::Moose;
use Test::Fatal;
use Test::Mock::One;
use Test::More;

use HTTP::Response;
use Zaaksysteem::API::Client;

sub test_client_api {
    my $request;

    my $mock_ua = Test::Mock::One->new(
        ISA => 'LWP::UserAgent',
        request => sub {
            $request = shift;
            return Test::Mock::One->new(
                decoded_content => '{"request_id":"abc","status_code":200,"result":{"foo":"bar"}}'
            );
        }
    );

    my $client = Zaaksysteem::API::Client->new(
        user_agent => $mock_ua,
        hostname => 'localhost',
        platform_key => 'abc'
    );

    isa_ok $client, 'Zaaksysteem::API::Client',
        'api client intantiation';

    my $response = $client->get('foo/123');

    isa_ok $request, 'HTTP::Request',
        'api client request generator';

    is $request->header('ZS-Platform-Key'), 'abc',
        'api client request platform key';

    is $request->header('Accept'), 'application/json',
        'api client request accept content type';

    is $request->method, 'GET',
        'api client request method';

    is $request->uri, 'https://localhost/api/v1/foo/123',
        'api client request uri';

    isa_ok $response, 'Zaaksysteem::API::Response',
        'api client response';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
