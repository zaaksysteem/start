# SPDX-FileCopyrightText: xxllnc Zaakgericht
#
# SPDX-License-Identifier: EUPL-1.2


import copy
import fnmatch
import io
import zipfile
from typing import BinaryIO, Generator, Literal
from typing_extensions import TypedDict
from unittest import mock


def _to_str(s: str | bytes) -> str:
    if isinstance(s, bytes):
        return s.decode()
    elif isinstance(s, str):
        return s

    raise ValueError("Expected str or bytes")


def _to_bytes(s: str | bytes | int) -> bytes:
    if isinstance(s, str):
        return s.encode()
    elif isinstance(s, int):
        return str(s).encode()
    elif isinstance(s, bytes):
        return s

    raise ValueError("Expected str or bytes")


class MockRedis:
    data: dict[str, tuple[bytes | dict[str, bytes] | list[bytes], int | None]]
    zdata: dict[str, tuple[dict[str, int | float], int | None]]
    _pipeline: mock.Mock

    def __init__(self):
        self.reset()

    def reset(self):
        self.data = {}
        self.zdata = {}
        self._pipeline = mock.Mock()
        self._pipeline.get.side_effect = lambda *k, **kw: self.get(*k, **kw)
        self._pipeline.set.side_effect = lambda *k, **kw: self.set(*k, **kw)
        self._pipeline.hget.side_effect = lambda *k, **kw: self.hget(*k, **kw)
        self._pipeline.hset.side_effect = lambda *k, **kw: self.hset(*k, **kw)
        self._pipeline.rpush.side_effect = lambda *k, **kw: self.rpush(
            *k, **kw
        )
        self._pipeline.llen.side_effect = lambda *k, **kw: self.llen(*k, **kw)
        self._pipeline.copy.side_effect = lambda *k, **kw: self.copy(*k, **kw)

        self._pipeline.zadd.side_effect = lambda *k, **kw: self.zadd(*k, **kw)
        self._pipeline.zrangebyscore.side_effect = (
            lambda *k, **kw: self.zrangebyscore(*k, **kw)
        )
        self._pipeline.zremrangebyscore.side_effect = (
            lambda *k, **kw: self.zremrangebyscore(*k, **kw)
        )

    def set(
        self,
        key: str | bytes,
        value: str | bytes,
        ex: int | None = None,
    ):
        str_key = _to_str(key)
        self.data[str_key] = (_to_bytes(value), ex)
        return

    def hset(
        self,
        name: str | bytes,
        key: str | None = None,
        value: str | bytes | None = None,
        mapping: dict[str, str | bytes] | None = None,
    ):
        str_name = _to_str(name)

        if str_name not in self.data:
            self.data[str_name] = ({}, None)

        hash = self.data[str_name][0]
        assert isinstance(hash, dict)

        if mapping:
            for k, v in mapping.items():
                hash[k] = _to_bytes(v)
        elif key and value:
            str_key = _to_str(key)
            hash[str_key] = _to_bytes(value)

    def get(self, key: str | bytes) -> bytes | None:
        str_key = _to_str(key)

        if str_key not in self.data:
            return

        data = self.data[str_key]

        assert isinstance(data[0], bytes)
        return data[0]

    def hget(self, name: str | bytes, key: str) -> bytes | None:
        str_name = _to_str(name)
        str_key = _to_str(key)
        data = self.data.get(str_name, (None, 60))

        if data[0] is None:
            return None

        assert isinstance(data[0], dict)

        return data[0].get(str_key, None)

    def expireat(self, name, when):
        return

    def scan_iter(
        self, match: str, count: int
    ) -> Generator[str | bytes, None, None]:
        yield from [
            k.encode() if isinstance(k, str) else k
            for k in self.data.keys()
            if fnmatch.fnmatchcase(_to_str(k), match)
        ]

    def delete(self, key: str | bytes):
        str_key = _to_str(key)
        del self.data[str_key]
        return

    def rpush(self, key, *items):
        str_name = _to_str(key)

        if str_name not in self.data:
            self.data[str_name] = ([], None)

        data = self.data[str_name]
        assert isinstance(data[0], list)

        data[0].extend(items)

        return len(items)

    def lrange(
        self, key: str | bytes, start: int, end: int
    ) -> list[bytes] | None:
        str_key = _to_str(key)

        if str_key not in self.data:
            return None

        rv = []

        first = start

        if end == -1:
            last = None
        else:
            last = end + 1

        data = self.data[str_key]
        assert isinstance(data[0], list)

        for item in data[0][first:last]:
            rv.append(item)

        return rv

    def lmove(self, first_list, second_list, src, dest):
        first_list_str = _to_str(first_list)
        second_list_str = _to_str(second_list)

        assert src == "LEFT"
        assert dest == "RIGHT"

        if first_list_str not in self.data:
            self.data[first_list_str] = ([], 60)
        if second_list_str not in self.data:
            self.data[second_list_str] = ([], 60)

        source = self.data[first_list_str][0]
        destination = self.data[second_list_str][0]

        assert isinstance(source, list)
        assert isinstance(destination, list)

        try:
            item = source.pop(0)
            destination.append(item)
            return item
        except IndexError:
            return None

    def llen(self, name) -> int:
        name_str = _to_str(name)

        return len(self.data.get(name_str, ([], 60))[0])

    def pipeline(self):
        return self._pipeline

    def unlink(self, key: str | bytes):
        str_key = _to_str(key)

        del self.data[str_key]
        return

    def copy(self, source, dest):
        self.data[dest] = copy.deepcopy(self.data[source])

    def zadd(self, key, mapping: dict):
        if key not in self.zdata:
            self.zdata[key] = ({}, 60)

        data = self.zdata[key][0]

        for k in mapping:
            data[k] = mapping[k]

    def zremrangebyscore(self, key, min, max):
        # noop in this mock
        pass

    def zrangebyscore(
        self,
        key,
        min: int | float | Literal["inf", "-inf"],
        max: int | float | Literal["inf", "-inf"],
    ):
        if key not in self.zdata:
            return []

        data = self.zdata[key][0]
        assert isinstance(data, dict)

        res = []
        for k, v in data.items():
            if v >= float(min) and v <= float(max):
                res.append(_to_bytes(k))

        return res


class MockS3:
    class MockS3File(TypedDict):
        name: str
        data: bytes
        tags: dict[str, str]

    uploads: dict[str, MockS3File]

    def __init__(self):
        self.uploads = {}

    def upload(self, file_handle: BinaryIO, uuid: str, tags: dict[str, str]):
        file_handle.seek(0)

        s3_file = self.MockS3File(
            name=uuid,
            data=file_handle.read(),
            tags=tags,
        )

        self.uploads[uuid] = s3_file

        return {
            "uuid": uuid,
            "md5": "x",
            "size": len(s3_file["data"]),
            "mime_type": "application/octet-stream",
            "storage_location": "far away",
        }

    def get_download_url(
        self,
        uuid: str,
        storage_location: str,
        filename: str,
        mime_type: str,
        download: bool,
    ) -> str:
        return f"https://{storage_location}/{uuid}?mime={mime_type}&filename={filename}&download={download}"

    def download_file(self, destination, file_uuid, storage_location="minio"):
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, "w") as zip_file:
            zip_file.writestr("folder1/", "", zipfile.ZIP_STORED)
            zip_file.writestr("folder2/subfolder/", "", zipfile.ZIP_STORED)
            zip_file.writestr(
                "folder1/data.xml",
                "content for folder1",
                zipfile.ZIP_DEFLATED,
            )
            zip_file.writestr(
                "folder2/subfolder/data.xml",
                "content for folder2/subfolder",
                zipfile.ZIP_DEFLATED,
            )

        zip_buffer.seek(0)
        destination.write(zip_buffer.read())
        destination.flush()

        return
