# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from ..mocks import MockRedis
from ._shared import make_job_json
from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains.jobs.entities.job import Job
from zsnl_domains.jobs.repositories.job import JobRepository

# Some deeper tests for the "jobs" repository:
# Taking job items and marking them as finished


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_config(self, context: str):
        return {
            "instance_uuid": "test",
        }

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


class TestItemProcessing(TestBase):
    def setup_method(self):
        mock_event_service = mock.Mock()
        self.redis = MockRedis()
        self.infrastructure_factory = MockInfrastructureFactory(
            mock_infra={
                "redis": self.redis,
            }
        )

        self.repo = JobRepository(
            infrastructure_factory=self.infrastructure_factory,
            infrastructure_factory_ro=self.infrastructure_factory,
            context="test",
            read_only=False,
            event_service=mock_event_service,
        )

    def test_get_item_from_batch(self):
        """
        Test 'get_item_from_batch' in the common case
        """
        job_uuid = uuid4()
        item_uuid = uuid4()
        self.redis.data[f"job_batch:test:{job_uuid}:processing"] = ([], 60)
        self.redis.data[f"job_batch:test:{job_uuid}:waiting"] = (
            [
                str(item_uuid).encode(),
            ],
            60,
        )

        job_json = make_job_json(job_uuid)
        job = Job.parse_raw(job_json)
        job.entity_id = job_uuid

        item = self.repo.get_item_from_batch(job=job)

        assert item == str(item_uuid)
        assert self.redis.data[f"job_batch:test:{job_uuid}:processing"] == (
            [
                str(item_uuid).encode(),
            ],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:waiting"] == (
            [],
            60,
        )

    def test_get_item_from_batch_last(self):
        """
        Test 'get_item_from_batch' when the all items are done/the waiting
        list is empty
        """

        job_uuid = uuid4()
        self.redis.data[f"job_batch:test:{job_uuid}:processing"] = ([], 60)
        self.redis.data[f"job_batch:test:{job_uuid}:waiting"] = ([], 60)

        job_json = make_job_json(job_uuid)
        job = Job.parse_raw(job_json)
        job.entity_id = job_uuid

        item = self.repo.get_item_from_batch(job=job)

        assert item is None
        assert self.redis.data[f"job_batch:test:{job_uuid}:processing"] == (
            [],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:waiting"] == (
            [],
            60,
        )

    def test_get_item_from_batch_restart(self):
        """
        Test 'get_item_from_batch' if an item is already in "processing" state

        The "processing" item should get priority
        """
        job_uuid = uuid4()
        item_uuid = uuid4()
        item2_uuid = uuid4()
        self.redis.data[f"job_batch:test:{job_uuid}:processing"] = (
            [
                str(item_uuid).encode(),
            ],
            60,
        )
        self.redis.data[f"job_batch:test:{job_uuid}:waiting"] = (
            [str(item2_uuid).encode()],
            60,
        )

        job_json = make_job_json(job_uuid)
        job = Job.parse_raw(job_json)
        job.entity_id = job_uuid

        item = self.repo.get_item_from_batch(job=job)

        assert item == str(object=item_uuid)
        assert self.redis.data[f"job_batch:test:{job_uuid}:processing"] == (
            [
                str(item_uuid).encode(),
            ],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:waiting"] == (
            [str(item2_uuid).encode()],
            60,
        )

    def test_finish_item_success(self):
        job_uuid = uuid4()
        item_uuid = uuid4()
        self.redis.data[f"job_batch:test:{job_uuid}:processing"] = (
            [
                str(item_uuid).encode(),
            ],
            60,
        )

        job_json = make_job_json(job_uuid)
        job = Job.parse_raw(job_json)
        job.entity_id = job_uuid

        self.repo.finish_item(
            job=job, batch_item=str(item_uuid), result=("good", None)
        )

        assert self.redis.data[f"job_batch:test:{job_uuid}:processing"] == (
            [],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:done"] == (
            [str(item_uuid).encode()],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:result"] == (
            {
                str(item_uuid): json.dumps(
                    {"type": "success", "value": "good"}
                ).encode()
            },
            None,
        )

    def test_finish_item_error(self):
        job_uuid = uuid4()
        item_uuid = uuid4()
        self.redis.data[f"job_batch:test:{job_uuid}:processing"] = (
            [
                str(item_uuid).encode(),
            ],
            60,
        )

        job_json = make_job_json(job_uuid)
        job = Job.parse_raw(job_json)
        job.entity_id = job_uuid

        self.repo.finish_item(
            job=job, batch_item=str(item_uuid), result=(None, "error message")
        )

        assert self.redis.data[f"job_batch:test:{job_uuid}:processing"] == (
            [],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:done"] == (
            [str(item_uuid).encode()],
            60,
        )
        assert self.redis.data[f"job_batch:test:{job_uuid}:result"] == (
            {
                str(item_uuid): json.dumps(
                    {"type": "error", "value": "error message"}
                ).encode()
            },
            None,
        )
