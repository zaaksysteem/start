# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
import pytest
import time
from ..mocks import MockRedis, MockS3
from ._shared import make_job_json
from minty.cqrs.test import TestBase
from minty.entity import RedirectResponse
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import jobs as jobs_domain
from zsnl_domains.jobs.entities.job import Job, JobStatus


class TestJobsBase(TestBase):
    def setup_method(self):
        self.redis = MockRedis()
        self.s3 = MockS3()
        self.amqp = mock.Mock()

        self.load_query_instance(
            jobs_domain,
            inframocks={"redis": self.redis, "s3": self.s3, "amqp": self.amqp},
        )
        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "test"}},
            }
        )


class TestGetJob(TestJobsBase):
    def test_get_job(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60)
        }

        job = self.qry.get_job(job_uuid)

        assert isinstance(job, Job)
        assert job.entity_id == job_uuid
        assert job.status == JobStatus.pending
        assert job.friendly_name == "get job"
        assert job.progress_percent == 0.0
        assert job.total_item_count == 0

    def test_get_job_running(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
            status="running",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60),
            f"job_batch:test:{job_uuid}:done": ([b"1", b"2", b"3", b"4"], 60),
        }

        job = self.qry.get_job(job_uuid)

        assert isinstance(job, Job)
        assert job.entity_id == job_uuid
        assert job.status == JobStatus.running
        assert job.friendly_name == "get job"
        assert job.progress_percent == 100.0
        assert job.total_item_count == 4

    def test_get_job_percentage_25(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        job_data = make_job_json(job_uuid, friendly_name="get job")

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60),
            f"job_batch:test:{job_uuid}:processing": ([b"1"], 60),
            f"job_batch:test:{job_uuid}:waiting": ([b"1", b"2"], 60),
            f"job_batch:test:{job_uuid}:done": ([b"1"], 60),
        }

        job = self.qry.get_job(job_uuid)

        assert isinstance(job, Job)
        assert job.entity_id == job_uuid
        assert job.status == JobStatus.pending
        assert job.friendly_name == "get job"
        assert job.progress_percent == 25.0

    def test_get_job_percentage_finished_noitems(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        job_data = make_job_json(
            job_uuid, friendly_name="get job", status="finished"
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60),
            f"job_batch:test:{job_uuid}:processing": ([], 60),
            f"job_batch:test:{job_uuid}:waiting": ([], 60),
            f"job_batch:test:{job_uuid}:done": ([], 60),
        }

        job = self.qry.get_job(job_uuid)

        assert isinstance(job, Job)
        assert job.entity_id == job_uuid
        assert job.status == JobStatus.finished
        assert job.friendly_name == "get job"
        assert job.progress_percent == 100.0

    def test_get_job_percentage_running_noitems(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        job_data = make_job_json(
            job_uuid, friendly_name="get job", status="running"
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60),
            f"job_batch:test:{job_uuid}:processing": ([], 60),
            f"job_batch:test:{job_uuid}:waiting": ([], 60),
            f"job_batch:test:{job_uuid}:done": ([], 60),
        }

        job = self.qry.get_job(job_uuid)

        assert isinstance(job, Job)
        assert job.entity_id == job_uuid
        assert job.status == JobStatus.running
        assert job.friendly_name == "get job"
        assert job.progress_percent == 0.0

    def test_get_job_notfound(self):
        job_uuid: UUID = uuid4()

        self.redis.data = {}

        with pytest.raises(minty.exceptions.NotFound):
            _ = self.qry.get_job(job_uuid)


class TestGetJobs(TestJobsBase):
    def test_get_jobs(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        other_user_uuid: UUID = uuid4()
        other_instance_id = "second_instance"
        job_uuid1: UUID = uuid4()
        job_uuid2: UUID = uuid4()
        job_uuid3: UUID = uuid4()

        job_data1 = make_job_json(job_uuid1, friendly_name="job 1")
        job_data2 = make_job_json(job_uuid2, friendly_name="job 2")
        job_data3 = make_job_json(job_uuid3, friendly_name="job 3")

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid1}": (
                {"entity": job_data1},
                60,
            ),
            f"jobs:{other_instance_id}:{user_uuid}:{job_uuid2}": (
                {"entity": job_data2},
                60,
            ),
            f"test:{other_user_uuid}:{job_uuid3}": (
                {"entity": job_data3},
                60,
            ),
        }
        self.redis.zdata = {
            f"jobs:test:{user_uuid}:all": (
                {
                    str(job_uuid1): time.time() + 3600,
                },
                600,
            )
        }

        jobs = self.qry.get_jobs()

        assert len(jobs) == 1
        assert isinstance(jobs[0], Job)

        assert jobs[0].entity_id == job_uuid1
        assert jobs[0].status == "pending"
        assert jobs[0].friendly_name == "job 1"


class TestDownloadJobResult(TestJobsBase):
    def test_download_job_result(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        results_file = str(uuid4())
        results_file_location = "somewhere"
        errors_file = str(uuid4())
        errors_file_location = "somewhere else"
        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
            results_file=results_file,
            results_file_location=results_file_location,
            results_file_name="results.csv",
            results_file_type="text/csv",
            errors_file=errors_file,
            errors_file_location=errors_file_location,
            errors_file_name="errors.csv",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60)
        }

        res = self.qry.download_result(job_uuid=job_uuid)

        assert isinstance(res, RedirectResponse)

        assert (
            res.location
            == f"https://somewhere/{results_file}?mime=text/csv&filename=results.csv&download=True"
        )

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_called_once()

    def test_download_job_result_no_file(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        errors_file = str(uuid4())
        errors_file_location = "somewhere else"
        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
            results_file=None,
            results_file_location=None,
            results_file_name=None,
            results_file_type=None,
            errors_file=errors_file,
            errors_file_location=errors_file_location,
            errors_file_name="errors.csv",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60)
        }

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.download_result(job_uuid=job_uuid)

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_not_called()


class TestDownloadJobErrors(TestJobsBase):
    def test_download_job_errors(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        results_file = str(uuid4())
        results_file_location = "somewhere"
        errors_file = str(uuid4())
        errors_file_location = "somewhere else"
        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
            results_file=results_file,
            results_file_location=results_file_location,
            results_file_name="result.csv",
            results_file_type="text/csv",
            errors_file=errors_file,
            errors_file_location=errors_file_location,
            errors_file_name="errors.csv",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60)
        }

        res = self.qry.download_errors(job_uuid=job_uuid)

        assert isinstance(res, RedirectResponse)

        assert (
            res.location
            == f"https://somewhere else/{errors_file}?mime=text/csv&filename=errors.csv&download=True"
        )

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_called_once()

    def test_download_job_errors_no_file(self):
        user_uuid: UUID = self.qry.user_info.user_uuid
        job_uuid: UUID = uuid4()

        results_file = str(uuid4())
        results_file_location = "somewhere"
        errors_file = None
        errors_file_location = None
        job_data = make_job_json(
            job_uuid,
            friendly_name="get job",
            results_file=results_file,
            results_file_location=results_file_location,
            results_file_name="result.csv",
            results_file_type="text/csv",
            errors_file=errors_file,
            errors_file_location=errors_file_location,
            errors_file_name="errors.csv",
        )

        self.redis.data = {
            f"jobs:test:{user_uuid}:{job_uuid}": ({"entity": job_data}, 60)
        }

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.download_errors(job_uuid=job_uuid)

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_not_called()
