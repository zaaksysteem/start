# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import freezegun
import json
import minty.exceptions
import pytest
import random
from ..mocks import MockRedis, MockS3
from ._shared import make_job_json
from minty.cqrs.test import TestBase
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import jobs as jobs_domain
from zsnl_domains.jobs.entities.job import JobCancelReason, JobStatus
from zsnl_domains.jobs.repositories.job import (
    JOB_EXPIRATION_SECONDS,
    MAX_INSTANCE_JOBS,
    MAX_USER_JOBS,
)


class JobsTestBase(TestBase):
    def setup_method(self):
        self.redis = MockRedis()
        self.s3 = MockS3()

        self.load_command_instance(
            jobs_domain,
            inframocks={
                "redis": self.redis,
                "s3": self.s3,
            },
        )
        self.cmd.repository_factory.infrastructure_factory.get_config = (
            lambda context: {"instance_uuid": "test"}
        )


class TestCreateJob(JobsTestBase):
    @freezegun.freeze_time()
    def test_create_job(self):
        user_uuid: UUID = self.cmd.user_info.user_uuid
        job_uuid: UUID = uuid4()

        self.cmd.create_job(
            job_uuid=job_uuid,
            job_description={
                "job_type": "delete_case",
                "selection": {"type": "filter", "filters": {}},
            },
            friendly_name="[no name]",
        )

        redis_key = f"jobs:test:{user_uuid}:{job_uuid}"
        assert redis_key in self.redis.data

        redis_data = self.redis.data[redis_key]
        assert isinstance(redis_data[0], dict)

        job_data = json.loads(redis_data[0]["entity"])

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        expires = now + datetime.timedelta(seconds=JOB_EXPIRATION_SECONDS)

        assert job_data == {
            "uuid": str(job_uuid),
            "status": "pending",
            "results_file": None,
            "errors_file": None,
            "job_description": {
                "job_type": "delete_case",
                "selection": {"type": "filter", "filters": {}},
            },
            "friendly_name": "[no name]",
            "started_at": now.isoformat(),
            "expires_at": expires.isoformat(),
        }

        user_info = redis_data[0]["user_info"]
        assert user_info.decode() == self.cmd.user_info.json()

        # Defined in minty.cqrs.test.TestBase
        assert redis_data[0]["context"] == b"context"

    def test_create_job_user_too_many(self):
        user_uuid = self.cmd.user_info.user_uuid

        self.redis.data = {}
        for _ in range(MAX_USER_JOBS):
            self.redis.data[f"jobs:test:{user_uuid}:{uuid4()}"] = (
                {
                    "entity": json.dumps(
                        {
                            "status": "pending",
                            "uuid": str(uuid4()),
                        }
                    ).encode()
                },
                60,
            )

        self.redis.data[f"jobs:test:{user_uuid}:all"] = (
            {},
            60,
        )

        with pytest.raises(minty.exceptions.Conflict):
            job_uuid: UUID = uuid4()
            self.cmd.create_job(
                job_uuid=job_uuid,
                job_description={
                    "job_type": "delete_case",
                    "selection": {"type": "filter", "filters": {}},
                },
                friendly_name="[no name]",
            )

    def test_create_job_instance_too_many(self):
        self.redis.data = {}
        for _ in range(MAX_INSTANCE_JOBS):
            self.redis.data[f"jobs:test:{uuid4()}:{uuid4()}"] = (
                {
                    "entity": json.dumps(
                        {
                            "status": "pending",
                            "uuid": str(uuid4()),
                        }
                    ).encode()
                },
                60,
            )

        with pytest.raises(minty.exceptions.Conflict):
            job_uuid: UUID = uuid4()
            self.cmd.create_job(
                job_uuid=job_uuid,
                job_description={
                    "job_type": "delete_case",
                    "selection": {"type": "filter", "filters": {}},
                },
                friendly_name="[no name]",
            )


class TestDeleteJob(JobsTestBase):
    def test_delete_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="finished",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {job_key: ({"entity": job_data}, 60)}

        self.cmd.delete_job(job_uuid)

        assert job_key not in self.redis.data

    def test_delete_job_wrong_status(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="running",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {job_key: ({"entity": job_data}, 60)}

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.delete_job(job_uuid)

        assert job_key in self.redis.data


class TestCancelJob(JobsTestBase):
    def test_cancel_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="running",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {job_key: ({"entity": job_data}, 60)}

        self.cmd.cancel_job(job_uuid)

        assert job_key in self.redis.data

        redis_data = self.redis.data[job_key]
        assert isinstance(redis_data[0], dict)

        json_data = json.loads(redis_data[0]["entity"])
        assert json_data["status"] == JobStatus.cancelled

        pipeline = self.redis.pipeline()

        calls = pipeline.method_calls

        assert calls[0] == mock.call.watch(job_key)
        assert calls[1] == mock.call.hget(job_key, "entity")
        assert calls[2] == mock.call.llen(f"job_batch:test:{job_uuid}:waiting")
        assert calls[3] == mock.call.llen(
            f"job_batch:test:{job_uuid}:processing"
        )
        assert calls[4] == mock.call.llen(f"job_batch:test:{job_uuid}:done")
        assert calls[5] == mock.call.multi()
        assert calls[6] == mock.call.hset(
            job_key,
            "entity",
            make_job_json(
                job_uuid=job_uuid,
                friendly_name="a job to delete",
                status="cancelled",
                cancel_reason=JobCancelReason.user_action,
            ).decode(),
        )
        assert calls[7] == mock.call.execute()

    def test_cancel_job_wrongstate(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="finished",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {job_key: ({"entity": job_data}, 60)}

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.cancel_job(job_uuid)

        assert job_key in self.redis.data

        redis_data = self.redis.data[job_key][0]
        assert isinstance(redis_data, dict)

        json_data = json.loads(redis_data["entity"])
        assert json_data["status"] == "finished"

    @staticmethod
    def _redis_trigger(redis, value):
        def _really(key):
            redis.data[key][0]["entity"] = value

        return _really

    def test_cancel_job_parallel_action(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="running",
        )

        # Job finished while we're cancelling it
        job_data_finished = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to delete",
            status="finished",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {job_key: ({"entity": job_data}, 60)}
        self.redis.pipeline().watch.side_effect = self._redis_trigger(
            self.redis, job_data_finished
        )

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.cancel_job(job_uuid)
        assert job_key in self.redis.data

        redis_data = self.redis.data[job_key][0]
        assert isinstance(redis_data, dict)

        json_data = json.loads(redis_data["entity"])
        assert json_data["status"] == "finished"


class TestPrepareJob(JobsTestBase):
    def test_prepare_job_filters(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="pending",
            selection={"type": "filter", "filters": []},
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        batch_key = f"job_batch:test:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        case_1 = (str(uuid4()), random.randint(1, 100))
        case_2 = (str(uuid4()), random.randint(1, 100))
        case_3 = (str(uuid4()), random.randint(1, 100))

        self.session.execute().tuples().all.side_effect = [
            # First page
            [
                case_1,
                case_2,
            ],
            # Second page
            [
                case_3,
            ],
            # Third (last) page
            [],
        ]

        self.cmd.prepare_job(job_uuid=job_uuid)

        redis_data = self.redis.data[f"{batch_key}:waiting"]
        assert isinstance(redis_data[0], list)

        assert redis_data[0] == [
            json.dumps(case_1),
            json.dumps(case_2),
            json.dumps(case_3),
        ]

    def test_prepare_job_source_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        source_job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="pending",
            selection={
                "type": "source_job",
                "source_job": str(source_job_uuid),
            },
        )
        source_job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="an older job",
            status="finished",
            selection={"type": "filter", "filters": {}},
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        source_job_key = f"jobs:test:{user_uuid}:{source_job_uuid}"
        batch_key = f"job_batch:test:{job_uuid}"
        source_batch_key = f"job_batch:test:{source_job_uuid}"

        case_uuid1 = str(uuid4())
        case_uuid2 = str(uuid4())

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)
        self.redis.data[source_job_key] = ({"entity": source_job_data}, 60)
        self.redis.data[f"{source_batch_key}:done"] = (
            [case_uuid1.encode(), case_uuid2.encode()],
            60,
        )

        self.cmd.prepare_job(job_uuid=job_uuid)

        redis_data = self.redis.data[f"{batch_key}:waiting"]
        source_redis_data = self.redis.data[f"{source_batch_key}:done"]
        assert redis_data[0] == source_redis_data[0]

    def test_prepare_job_source_job_not_finished(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        source_job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="pending",
            selection={
                "type": "source_job",
                "source_job": str(source_job_uuid),
            },
        )
        source_job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="an older job",
            status="running",
            selection={"type": "filter", "filters": {}},
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        source_job_key = f"jobs:test:{user_uuid}:{source_job_uuid}"
        batch_key = f"job_batch:test:{job_uuid}"
        source_batch_key = f"job_batch:test:{source_job_uuid}"

        case_uuid1 = str(uuid4())
        case_uuid2 = str(uuid4())

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)
        self.redis.data[source_job_key] = ({"entity": source_job_data}, 60)
        self.redis.data[f"{source_batch_key}:done"] = (
            [case_uuid1.encode(), case_uuid2.encode()],
            60,
        )

        self.cmd.prepare_job(job_uuid=job_uuid)

        assert f"{batch_key}:waiting" not in self.redis.data
        redis_data = self.redis.data[job_key][0]
        assert isinstance(redis_data, dict)

        json_data = json.loads(redis_data["entity"])
        assert json_data["status"] == "cancelled"

    def test_prepare_job_source_job_not_found(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        source_job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="pending",
            selection={
                "type": "source_job",
                "source_job": str(source_job_uuid),
            },
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        batch_key = f"job_batch:test:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        self.cmd.prepare_job(job_uuid=job_uuid)

        assert f"{batch_key}:waiting" not in self.redis.data

        assert job_key in self.redis.data

        redis_data = self.redis.data[job_key][0]
        assert isinstance(redis_data, dict)

        json_data = json.loads(redis_data["entity"])
        assert json_data["status"] == "cancelled"

    def test_prepare_job_wrong_status(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="cancelled",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.prepare_job(job_uuid=job_uuid)

    def test_prepare_job_id_list(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()

        case_uuid1 = str(uuid4())
        case_id1 = random.randint(1, 100)
        case_uuid2 = str(uuid4())

        self.session.execute().tuples().all.side_effect = [
            [
                (case_uuid1, case_id1),
            ]
        ]

        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="pending",
            selection={"type": "id_list", "id_list": [case_uuid1, case_uuid2]},
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        batch_key = f"job_batch:test:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        self.cmd.prepare_job(job_uuid=job_uuid)

        redis_data = self.redis.data[f"{batch_key}:waiting"]
        assert redis_data[0] == [
            json.dumps([str(case_uuid1), case_id1]),
            json.dumps(
                [str(case_uuid2), -1]
            ),  # Fallback when case is not found
        ]


class TestProgressJob(JobsTestBase):
    def test_progress_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress",
            status="running",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        self.cmd.progress_job(job_uuid=job_uuid)

        redis_data = self.redis.data[job_key]
        assert isinstance(redis_data[0], dict)

        json_data = json.loads(redis_data[0]["entity"])
        assert json_data["status"] == "running"

    def test_progress_job_not_running(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to progress?",
            status="finished",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.progress_job(job_uuid=job_uuid)


class TestJobProcessed(JobsTestBase):
    def test_job_processed(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed",
            status="running",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        self.cmd.job_processed(job_uuid=job_uuid)

        redis_data = self.redis.data[job_key]
        assert isinstance(redis_data[0], dict)

        json_data = json.loads(redis_data[0]["entity"])
        assert json_data["status"] == "processed"

    def test_job_processed_not_running(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed?",
            status="finished",
        )

        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.job_processed(job_uuid=job_uuid)


class TestFinishJob(JobsTestBase):
    def test_finish_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed?",
            status="processed",
        )
        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        done_key = f"job_batch:test:{job_uuid}:done"
        result_key = f"job_batch:test:{job_uuid}:result"

        case_uuids = [str(uuid4()), str(uuid4())]
        case_ids = [random.randint(1, 100), random.randint(1, 100)]

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)
        self.redis.data[done_key] = (
            [
                json.dumps([cu, ci]).encode()
                for cu, ci in zip(case_uuids, case_ids, strict=True)
            ],
            60,
        )
        self.redis.data[result_key] = (
            {
                json.dumps([case_uuids[0], case_ids[0]]): json.dumps(
                    {"type": "success", "value": ["it", "worked"]}
                ).encode(),
                json.dumps([case_uuids[1], case_ids[1]]): json.dumps(
                    {"type": "error", "value": "it broked"}
                ).encode(),
            },
            60,
        )

        self.cmd.finish_job(job_uuid=job_uuid)

        redis_data = self.redis.data[job_key]
        assert isinstance(redis_data[0], dict)

        json_data = json.loads(redis_data[0]["entity"])
        assert json_data["status"] == "finished"

        assert json_data["results_file_location"] == "far away"
        assert json_data["errors_file_location"] == "far away"

        # Check mock S3
        assert json_data["results_file"] in self.s3.uploads
        results_file = self.s3.uploads[json_data["results_file"]]
        assert json_data["results_file_size"] == len(results_file["data"])

        assert results_file[
            "data"
        ] == f"{case_uuids[0]},{case_ids[0]},it,worked\r\n".encode("utf-8")
        assert results_file["tags"] == {"type": "job_result"}

        assert json_data["errors_file"] in self.s3.uploads
        errors_file = self.s3.uploads[json_data["errors_file"]]
        assert json_data["errors_file_size"] == len(errors_file["data"])

        assert errors_file[
            "data"
        ] == f"{case_uuids[1]},{case_ids[1]},it broked\r\n".encode("utf-8")
        assert errors_file["tags"] == {"type": "job_result"}

        # Ensure the intermediate results are deleted
        assert result_key not in self.redis.data

    def test_finish_job_wrong_status(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed?",
            status="running",
        )
        job_key = f"jobs:test:{user_uuid}:{job_uuid}"

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)

        with pytest.raises(expected_exception=minty.exceptions.Conflict):
            self.cmd.finish_job(job_uuid=job_uuid)

    def test_finish_job_item_not_in_results(self):
        "Result found in 'done' list but not in result hash"

        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed?",
            status="processed",
        )
        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        done_key = f"job_batch:test:{job_uuid}:done"
        result_key = f"job_batch:test:{job_uuid}:result"

        case_uuids = [str(uuid4()), str(uuid4())]
        case_ids = [random.randint(1, 100), random.randint(1, 100)]

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)
        self.redis.data[done_key] = (
            [
                json.dumps([cu, ci]).encode()
                for cu, ci in zip(case_uuids, case_ids, strict=True)
            ],
            60,
        )
        self.redis.data[result_key] = (
            {
                json.dumps([case_uuids[0], case_ids[0]]): json.dumps(
                    {"type": "success", "value": "it worked"}
                ).encode(),
            },
            60,
        )

        with pytest.raises(AssertionError):
            self.cmd.finish_job(job_uuid=job_uuid)

    def test_finish_archive_export_job(self):
        user_uuid = self.cmd.user_info.user_uuid
        job_uuid = uuid4()
        job_data = make_job_json(
            job_uuid=job_uuid,
            friendly_name="a job to job_processed?",
            status="processed",
            job_type="run_archive_export",
        )
        job_key = f"jobs:test:{user_uuid}:{job_uuid}"
        done_key = f"job_batch:test:{job_uuid}:done"
        result_key = f"job_batch:test:{job_uuid}:result"

        case_uuids = [str(uuid4()), str(uuid4())]
        case_ids = [random.randint(1, 100), random.randint(1, 100)]

        self.redis.data = {}
        self.redis.data[job_key] = ({"entity": job_data}, 60)
        self.redis.data[done_key] = (
            [
                json.dumps([cu, ci]).encode()
                for cu, ci in zip(case_uuids, case_ids, strict=True)
            ],
            60,
        )
        self.redis.data[result_key] = (
            {
                json.dumps([case_uuids[0], case_ids[0]]): json.dumps(
                    {
                        "type": "success",
                        "value": [
                            {
                                "file_storage_location": "minio",
                                "file_uuid": "ea02625b-930c-4c26-ab7a-6f0d1fda5fd4",
                            }
                        ],
                    }
                ).encode(),
                json.dumps([case_uuids[1], case_ids[1]]): json.dumps(
                    {"type": "error", "value": "it broked"}
                ).encode(),
            },
            60,
        )

        self.cmd.finish_job(job_uuid=job_uuid)

        redis_data = self.redis.data[job_key]
        assert isinstance(redis_data[0], dict)

        json_data = json.loads(redis_data[0]["entity"])
        assert json_data["status"] == "finished"

        assert json_data["results_file_location"] == "far away"
        assert json_data["errors_file_location"] == "far away"

        # Check mock S3
        assert json_data["results_file"] in self.s3.uploads
        results_file = self.s3.uploads[json_data["results_file"]]
        assert json_data["results_file_size"] == len(results_file["data"])

        assert results_file["tags"] == {"type": "job_result"}

        assert json_data["errors_file"] in self.s3.uploads
        errors_file = self.s3.uploads[json_data["errors_file"]]
        assert json_data["errors_file_size"] == len(errors_file["data"])

        assert errors_file[
            "data"
        ] == f"{case_uuids[1]},{case_ids[1]},it broked\r\n".encode("utf-8")
        assert errors_file["tags"] == {"type": "job_result"}

        # Ensure the intermediate results are deleted
        assert result_key not in self.redis.data
