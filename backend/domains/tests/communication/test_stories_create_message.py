# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty
import pytest
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import communication


class TestCreateExternalMessage(TestBase):
    def setup_method(self):
        self.load_command_instance(communication)
        self.template_uuid = uuid4()
        self.mock_template_row = mock.Mock(
            id=1,
            uuid=self.template_uuid,
            sender_address="address_from_template@foo.com",
            sender="template bro",
        )
        self.mock_case_row = mock.Mock(
            id=12345,
            uuid=uuid4(),
            status="open",
            description="dikke test case",
            description_public="vrij forse test case",
            case_type_name="testerino capucino",
        )
        self.mock_requestor_contact = mock.Mock(
            id=1,
            uuid=uuid4(),
            type="person",
            name="tester",
        )
        self.mock_created_by_contact = mock.Mock(
            id=2,
            uuid=uuid4(),
            type="employee",
            name="koning eindbaas",
        )
        self.mock_thread_row = mock.Mock(
            uuid=uuid4(),
            id=1,
            last_message_cache={"message_type": "email"},
            unread_employee_count=0,
            unread_pip_count=0,
        )
        self.mock_email_config_row = mock.Mock(
            uuid=uuid4(),
            interface_config={"api_user": "kevin"},
            internal_config={"refresh_token": "something"},
        )

    def test_create_external_message_with_template_uuid(self):
        participants = [
            {
                "role": "to",
                "display_name": "eindbaas",
                "address": "test@foo.com",
            }
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # is admin
            [self.mock_case_row],  # get case by uuid
            [],  # get_subject_relation_contacts_from_case
        ]
        self.session.execute().fetchone.side_effect = [
            self.mock_requestor_contact,
            self.mock_created_by_contact,
            self.mock_thread_row,
            self.mock_template_row,
            self.mock_email_config_row,
            self.mock_thread_row,  # loops over changes in event apparently 3
            self.mock_thread_row,
            self.mock_thread_row,
            None,  # Nothing to skip having to mock a very large query spanning multiple tables
        ]
        self.cmd.create_external_message(
            external_message_uuid=uuid4(),
            thread_uuid=uuid4(),
            subject="test subject",
            content="test content",
            message_type="email",
            direction="outgoing",
            case_uuid=uuid4(),
            attachments=[],
            participants=participants,
            template_uuid=self.template_uuid,
        )

        call_list = self.session.execute.call_args_list
        assert (len(call_list)) == 20

        insert_query = call_list[15][0][0]
        compiled_query = insert_query.compile(dialect=postgresql.dialect())

        assert str(compiled_query) == (
            "INSERT INTO thread_message_external (content, subject, participants, type, direction, source_file_id, attachment_count) VALUES (%(content)s, %(subject)s, %(participants)s, %(type)s, %(direction)s, (SELECT filestore.id \n"
            "FROM filestore \n"
            "WHERE filestore.uuid IS NULL), %(attachment_count)s) RETURNING thread_message_external.id"
        )

        assert compiled_query.params["participants"] == [
            {
                "role": "to",
                "display_name": "eindbaas",
                "address": "test@foo.com",
            },
            {
                "address": "address_from_template@foo.com",
                "role": "from",
                "display_name": "template bro",
            },
        ]

    def test_create_external_message_with_template_id_template_id_not_present(
        self,
    ):
        participants = [
            {
                "role": "to",
                "display_name": "eindbaas",
                "address": "test@foo.com",
            }
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # is admin
            [self.mock_case_row],  # get case by uuid
            [],  # get_subject_relation_contacts_from_case
        ]
        self.session.execute().fetchone.side_effect = [
            self.mock_requestor_contact,
            self.mock_created_by_contact,
            self.mock_thread_row,
            None,  # simulate template not existing in database
            self.mock_email_config_row,
            self.mock_thread_row,  # loops over changes in event apparently 3
            self.mock_thread_row,
            self.mock_thread_row,
            None,  # Nothing to skip having to mock a very large query spanning multiple tables
        ]
        with pytest.raises(minty.exceptions.NotFound):
            self.cmd.create_external_message(
                external_message_uuid=uuid4(),
                thread_uuid=uuid4(),
                subject="test subject",
                content="test content",
                message_type="email",
                direction="outgoing",
                case_uuid=uuid4(),
                attachments=[],
                participants=participants,
                template_uuid=uuid4(),
            )

        call_list = self.session.execute.call_args_list
        assert (len(call_list)) == 9

    def test_create_external_message(self):
        participants = [
            {
                "role": "to",
                "display_name": "eindbaas",
                "address": "test@foo.com",
            }
        ]
        self.cmd.event_service.event_list = []  # reset event list
        self.session.execute().fetchall.side_effect = [
            [True],  # is admin
            [self.mock_case_row],  # get case by uuid
            [],  # get_subject_relation_contacts_from_case
        ]
        self.session.execute().fetchone.side_effect = [
            self.mock_requestor_contact,
            self.mock_created_by_contact,
            self.mock_thread_row,
            self.mock_email_config_row,
            self.mock_thread_row,  # 3 times mock thread row for every ThreadUnReadCount event
            self.mock_thread_row,
            self.mock_thread_row,
            None,  # Nothing to skip having to mock a very large query spanning multiple tables
        ]
        self.cmd.create_external_message(
            external_message_uuid=str(uuid4()),
            thread_uuid=str(uuid4()),
            subject="test subject",
            content="test content",
            message_type="email",
            direction="outgoing",
            case_uuid=str(uuid4()),
            attachments=[],
            participants=participants,
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 19

        insert_query = call_list[14][0][0]
        compiled_query = insert_query.compile(dialect=postgresql.dialect())

        assert str(compiled_query) == (
            "INSERT INTO thread_message_external (content, subject, participants, type, direction, source_file_id, attachment_count) VALUES (%(content)s, %(subject)s, %(participants)s, %(type)s, %(direction)s, (SELECT filestore.id \n"
            "FROM filestore \n"
            "WHERE filestore.uuid IS NULL), %(attachment_count)s) RETURNING thread_message_external.id"
        )
        assert compiled_query.params["participants"] == [
            {
                "role": "to",
                "display_name": "eindbaas",
                "address": "test@foo.com",
            }
        ]

    def test_create_external_message_pip_as_employee(self):
        self.cmd.event_service.event_list = []  # reset event list
        self.session.execute().fetchall.side_effect = [
            [True],  # is admin
            [self.mock_case_row],  # get case by uuid
            [],  # get_subject_relation_contacts_from_case
        ]
        organisation_name = "municipality of test"
        self.session.execute().fetchone.side_effect = [
            self.mock_requestor_contact,
            self.mock_created_by_contact,
            self.mock_thread_row,
            self.mock_thread_row,  # 3 times mock thread row for every ThreadUnReadCount event
            self.mock_thread_row,
            self.mock_thread_row,
            None,  # Nothing to skip having to mock a very large query spanning multiple tables
        ]
        self.session.execute().scalar_one.side_effect = [organisation_name]

        self.cmd.create_external_message(
            external_message_uuid=str(uuid4()),
            thread_uuid=str(uuid4()),
            subject="test subject",
            content="test content",
            message_type="pip",
            direction="unspecified",
            case_uuid=str(uuid4()),
            attachments=[],
            participants=[],
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 19

        insert_query = call_list[15][0][0]
        compiled_query = insert_query.compile(dialect=postgresql.dialect())

        assert str(compiled_query) == (
            "INSERT INTO thread_message (uuid, thread_id, message_slug, type, created_by_uuid, created_by_displayname, created, last_modified, message_date, thread_message_external_id) VALUES (%(uuid)s::UUID, (SELECT thread.id \nFROM thread \nWHERE thread.uuid = %(uuid_1)s::UUID), %(message_slug)s, %(type)s, %(created_by_uuid)s::UUID, %(created_by_displayname)s, %(created)s, %(last_modified)s, %(message_date)s, %(thread_message_external_id)s) RETURNING thread_message.id"
        )

        assert (
            compiled_query.params["created_by_displayname"]
            == organisation_name
        )
