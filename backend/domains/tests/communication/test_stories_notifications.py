# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import random
from datetime import datetime, timezone
from freezegun import freeze_time
from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains import communication


class TestSendNotification(TestBase):
    def setup_method(self):
        self.email_infra = mock.MagicMock()
        self.storage_infra = mock.MagicMock()

        self.load_command_instance(
            domain=communication,
            inframocks={"email": self.email_infra, "s3": self.storage_infra},
        )

    @freeze_time()
    def test_send_notification_no_existing_thread(self):
        case_uuid = uuid4()

        now = datetime.now(tz=timezone.utc)

        mock_case = mock.Mock()
        mock_case.configure_mock(
            case_id=random.randint(1, 1000),
            case_uuid=case_uuid,
            case_status="open",
            case_description="X",
            case_description_public="Y",
            case_type_name="Test",
        )

        mock_last_message_row = mock.Mock()
        mock_last_message_row.configure_mock(
            thread_uuid=uuid4(),
            is_notification=True,
            case_uuid=case_uuid,
            case_html_email_template=None,
            uuid=uuid4(),
            type="external",
            message_slug="slug",
            message_date=now,
            created_by_uuid=uuid4(),
            created_by_displayhname="Displayname",
            created=now,
            last_modified=now,
            thread_id=random.randint(2000, 3000),
            contactmoment_channel=None,
            contactmoment_direction=None,
            contactmoment_content=None,
            note_content=None,
            external_message_content="Message content",
            external_message_subject="Message subject",
            direction="outgoing",
            participants=[],
            read_employee=False,
            read_pip=False,
            attachment_count=0,
            external_message_type="email",
            external_message_failure_reason=None,
            is_imported=False,
            recipient_uuid=None,
            recipient_displayname=None,
            attachments=[],
        )

        self.session.execute().fetchone.side_effect = [
            # Get thread
            None,
            # Validate unread message count (employee)
            None,
            # Validate unread message count (pip)
            None,
            mock_last_message_row,
        ]
        self.session.execute().fetchall.side_effect = [
            # unsafe_find_case_by_uuid
            [mock_case],
        ]

        self.cmd.send_notification(
            case_uuid=case_uuid,
            sender_name="Sender Name",
            sender_address="sender@example.com",
            subject="Notification subject",
            body="Notification body",
            recipient_address="recipient@example.com",
        )

        events = self.get_events_by_name("ThreadCreated")
        assert len(events) == 1
        assert events[0].changes == [
            {
                "key": "thread_type",
                "new_value": "external",
                "old_value": None,
            },
            {
                "key": "case",
                "new_value": {
                    "entity_id": str(case_uuid),
                    "type": "Case",
                },
                "old_value": None,
            },
            {
                "key": "last_message_cache",
                "new_value": {},
                "old_value": {},
            },
            {
                "key": "created",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "last_modified",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "unread_pip_count",
                "new_value": 0,
                "old_value": None,
            },
            {
                "key": "unread_employee_count",
                "new_value": 0,
                "old_value": None,
            },
            {
                "key": "attachment_count",
                "new_value": 0,
                "old_value": None,
            },
            {
                "key": "is_notification",
                "new_value": True,
                "old_value": False,
            },
        ]

        events = self.get_events_by_name("ExternalMessageCreated")

        assert len(events) == 1
        assert events[0].changes == [
            {
                "key": "thread_uuid",
                "new_value": mock.ANY,
                "old_value": None,
            },
            {
                "key": "message_slug",
                "new_value": "Notification body",
                "old_value": None,
            },
            {
                "key": "message_type",
                "new_value": "external",
                "old_value": "external",
            },
            {
                "key": "created_by",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "created_by_displayname",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "content",
                "new_value": "Notification body",
                "old_value": None,
            },
            {
                "key": "subject",
                "new_value": "Notification subject",
                "old_value": None,
            },
            {
                "key": "external_message_type",
                "new_value": "email",
                "old_value": None,
            },
            {
                "key": "created_date",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "last_modified",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "message_date",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "attachments",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "participants",
                "new_value": [
                    {
                        "address": "sender@example.com",
                        "display_name": "Sender Name",
                        "role": "from",
                    },
                    {
                        "address": "recipient@example.com",
                        "display_name": "",
                        "role": "to",
                    },
                ],
                "old_value": [],
            },
            {
                "key": "direction",
                "new_value": "outgoing",
                "old_value": None,
            },
            {
                "key": "original_message_file",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "attachment_count",
                "new_value": 0,
                "old_value": None,
            },
            {
                "key": "is_imported",
                "new_value": False,
                "old_value": None,
            },
            {
                "key": "creator_type",
                "new_value": "employee",
                "old_value": None,
            },
        ]

    @freeze_time()
    def test_send_notification_existing_thread(
        self,
    ):
        case_uuid = uuid4()

        now = datetime.now(tz=timezone.utc)

        mock_thread_row = mock.Mock()
        mock_thread_row.configure_mock(
            case_id=random.randint(1, 1000),
            case_uuid=case_uuid,
            status="",
            description="",
            description_public="",
            case_type_name="",
            # If this is falsey, the contact_uuid and contact_displayname are not read
            contact_uuid=None,
            # Thread data:
            uuid=uuid4(),
            id=random.randint(1000, 2000),
            thread_type="external",
            created=now,
            last_modified=now,
            last_message_cache={"message_type": "email"},
            message_count=0,
            unread_pip_count=0,
            unread_employee_count=0,
            attachment_count=0,
        )
        mock_last_message_row = mock.Mock()
        mock_last_message_row.configure_mock(
            thread_uuid=uuid4(),
            is_notification=True,
            case_uuid=case_uuid,
            case_html_email_template=None,
            uuid=uuid4(),
            type="external",
            message_slug="slug",
            message_date=now,
            created_by_uuid=uuid4(),
            created_by_displayhname="Displayname",
            created=now,
            last_modified=now,
            thread_id=random.randint(2000, 3000),
            contactmoment_channel=None,
            contactmoment_direction=None,
            contactmoment_content=None,
            note_content=None,
            external_message_content="Message content",
            external_message_subject="Message subject",
            direction="outgoing",
            participants=[],
            read_employee=False,
            read_pip=False,
            attachment_count=0,
            external_message_type="email",
            external_message_failure_reason=None,
            is_imported=False,
            recipient_uuid=None,
            recipient_displayname=None,
            attachments=[],
        )

        self.session.execute().fetchone.side_effect = [
            mock_thread_row,
            mock_last_message_row,
        ]

        self.cmd.send_notification(
            case_uuid=case_uuid,
            sender_name="Sender Name",
            sender_address="sender@example.com",
            subject="Notification subject",
            body="Notification body",
            recipient_address="recipient@example.com",
        )

        events = self.get_events_by_name("ThreadCreated")
        assert len(events) == 0

        events = self.get_events_by_name("ExternalMessageCreated")

        assert len(events) == 1
        assert events[0].changes == [
            {
                "key": "thread_uuid",
                "new_value": str(mock_thread_row.uuid),
                "old_value": None,
            },
            {
                "key": "message_slug",
                "new_value": "Notification body",
                "old_value": None,
            },
            {
                "key": "message_type",
                "new_value": "external",
                "old_value": "external",
            },
            {
                "key": "created_by",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "created_by_displayname",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "content",
                "new_value": "Notification body",
                "old_value": None,
            },
            {
                "key": "subject",
                "new_value": "Notification subject",
                "old_value": None,
            },
            {
                "key": "external_message_type",
                "new_value": "email",
                "old_value": None,
            },
            {
                "key": "created_date",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "last_modified",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "message_date",
                "new_value": now.isoformat(),
                "old_value": None,
            },
            {
                "key": "attachments",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "participants",
                "new_value": [
                    {
                        "address": "sender@example.com",
                        "display_name": "Sender Name",
                        "role": "from",
                    },
                    {
                        "address": "recipient@example.com",
                        "display_name": "",
                        "role": "to",
                    },
                ],
                "old_value": [],
            },
            {
                "key": "direction",
                "new_value": "outgoing",
                "old_value": None,
            },
            {
                "key": "original_message_file",
                "new_value": None,
                "old_value": None,
            },
            {
                "key": "attachment_count",
                "new_value": 0,
                "old_value": None,
            },
            {
                "key": "is_imported",
                "new_value": False,
                "old_value": None,
            },
            {
                "key": "creator_type",
                "new_value": "employee",
                "old_value": None,
            },
        ]
