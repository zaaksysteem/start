# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import requests
from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains.document.infrastructures import WopiInfrastructure


class Test_WOPIfrastructure(TestBase):
    def setup_method(self):
        config = {
            "wopi_base_url": "http://wopi.development.zaaksyteem.nl",
            "wopi_api_key": "samplekey",
        }
        self.wopi = WopiInfrastructure()(config=config)

    @mock.patch("zsnl_domains.document.infrastructures.wopi_editor.requests")
    def test_edit_document(self, requests_mock):
        result = mock.MagicMock()
        result.configure_mock(status_code=200)
        result.json.return_value = {
            "favIconUrl": "test_fav_icon_url",
            "urlsrc": "test_urlsrc",
            "access_token": "123",
            "access_token_ttl": "3000",
        }

        requests_mock.post.return_value = result
        document_uuid = str(uuid4())
        directory_uuid = str(uuid4())
        document_info = {
            "extension": "docx",
            "filename": "test_doc.docx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": document_uuid,
                "authentication_token": "urlsafe_token",
                "editor": "msonline",
                "directory_uuid": directory_uuid,
            },
        }

        options = {
            "document_uuid": document_uuid,
            "context": "test",
            "app": "Word",
            "action": "edit",
            "user_uuid": str(uuid4()),
            "user_display_name": "Admin",
            "owner_uuid": str(uuid4()),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        result = self.wopi.edit_document(options)

        assert result["favIconUrl"] == "test_fav_icon_url"

    @mock.patch("zsnl_domains.document.infrastructures.wopi_editor.requests")
    def test_edit_document_with_error(self, requests_mock):
        def http_connection_pool_error():
            raise requests.exceptions.HTTPError()

        result = mock.MagicMock()
        result.configure_mock(status_code=500)
        result.raise_for_status = http_connection_pool_error

        requests_mock.post.return_value = result

        with pytest.raises(requests.exceptions.HTTPError):
            self.wopi.edit_document({})
