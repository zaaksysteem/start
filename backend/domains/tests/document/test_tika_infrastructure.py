# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import io
import zsnl_domains.document.infrastructures.tika as tika
from minty.cqrs.test import TestBase
from unittest import mock


class Test_Tika_Infrastructure(TestBase):
    def setup_method(self):
        self.config = {
            "tika_url": "http://tika",
        }
        self.tika = tika.TikaInfrastructure()(config=self.config)

    @mock.patch("zsnl_domains.document.infrastructures.tika.requests.put")
    def test_get_text(self, requests_mock):
        # MIME type of the file being sent to Tika
        mime_type = "application/vnd.oasis.opendocument.text"

        # Text response
        response = "this text was extracted from the document"

        requests_mock.return_value.text = response
        requests_mock.return_value.status_code = 200

        handle = io.BytesIO(b"ODT file contents would be here")
        result = self.tika.get_text(mime_type=mime_type, handle=handle)

        requests_mock.assert_called_once_with(
            url=self.config["tika_url"] + "/tika",
            headers={
                "Accept": "text/plain",
                "Content-Type": mime_type,
                "X-Tika-OCRskipOcr": "true",
            },
            data=handle,
        )
        assert result == response

    @mock.patch("zsnl_domains.document.infrastructures.tika.requests.put")
    def test_get_text_pdf_with_ocr(self, requests_mock):
        # MIME type of the file being sent to Tika
        mime_type = "application/pdf"

        # Text response
        response = "this text was extracted from the document"

        requests_mock.return_value.text = response
        requests_mock.return_value.status_code = 200

        handle = io.BytesIO(b"ODT file contents would be here")
        result = self.tika.get_text(mime_type=mime_type, handle=handle)

        requests_mock.assert_called_once_with(
            url=self.config["tika_url"] + "/tika",
            headers={
                "Accept": "text/plain",
                "Content-Type": mime_type,
                "X-Tika-OCRskipOcr": "false",
            },
            data=handle,
        )
        assert result == response

    @mock.patch("zsnl_domains.document.infrastructures.tika.requests.put")
    def test_get_text_image_with_ocr(self, requests_mock):
        # MIME type of the file being sent to Tika
        mime_type = "image/jpeg"

        # Text response
        response = "this text was extracted from the document"

        requests_mock.return_value.text = response
        requests_mock.return_value.status_code = 200

        handle = io.BytesIO(b"ODT file contents would be here")
        result = self.tika.get_text(mime_type=mime_type, handle=handle)

        requests_mock.assert_called_once_with(
            url=self.config["tika_url"] + "/tika",
            headers={
                "Accept": "text/plain",
                "Content-Type": mime_type,
                "X-Tika-OCRskipOcr": "false",
            },
            data=handle,
        )
        assert result == response
