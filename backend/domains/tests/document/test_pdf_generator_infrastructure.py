# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import zsnl_domains.document.infrastructures.pdf_generator as pdf_generator
from minty.cqrs.test import TestBase
from requests.exceptions import HTTPError
from secrets import token_hex
from unittest import mock
from zsnl_domains.document.infrastructures.pdf_generator import (
    PDFTemplateValues,
)


class Test_PDF_Generator_Infrastructure(TestBase):
    def setup_method(self):
        self.config = {
            "pdf_host": "https://zaaksysteem-dev.bridge-to-knowledge.nl/open?",
            "pdf_user": "user",
            "pdf_password": "password",
            "pdf_modelid": 1,
        }
        self.pdf_generator = pdf_generator.PDFGeneratorInfrastructure()(
            config=self.config
        )
        self.token = token_hex()
        self.hostname = "mijn.testwappies.nl"
        self.template_values: PDFTemplateValues = {
            "attributes": [
                {
                    "name": "attribute_name",
                    "value": "testerino pemperino",
                    "address": [{}],
                    "type": "text",
                    "sequence": 1,
                }
            ],
            "meta": {
                "customer": self.hostname,
                "case_number": 1,
                "casetype_name": "holymoley wat een gedoe",
                "date": "24-07-2020",
            },
            "prepared_request": {
                "token": self.token,
                "upload_url": f"https://{self.hostname}/api/v2/document/create_document_with_token",
            },
        }

    @mock.patch("requests.post")
    def test_generate_pdf(self, requests_mock):
        self.pdf_generator.generate_pdf(self.template_values)

        requests_mock.assert_called_once_with(
            self.pdf_generator.request_url, json=self.template_values
        )

    @mock.patch("requests.post")
    def test_generate_pdf_http_error(self, post_mock):
        mock_response = mock.Mock(name="MockResponse")
        mock_response.raise_for_status.side_effect = HTTPError
        post_mock.side_effect = [mock_response]

        with pytest.raises(HTTPError):
            self.pdf_generator.generate_pdf(self.template_values)
