# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.exceptions
import pytest
import random
from ..mocks import MockRedis
from freezegun import freeze_time
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.entity import RedirectResponse
from minty.exceptions import Forbidden, NotFound
from sqlalchemy.dialects import postgresql
from typing import cast
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import document
from zsnl_domains.document.entities.case_registration_form import (
    CaseRegistrationForm,
    CaseRegistrationFormDownload,
)
from zsnl_domains.document.infrastructures.pdf_generator import (
    PDFTemplateValues,
)
from zsnl_domains.document.repositories.case import (
    _format_value,
)
from zsnl_domains.document.repositories.case_registration_form import (
    CaseRegistrationFormRepository,
)


class MockPDFGenerator:
    def reset(self):
        self.last_template_values = None

    def generate_pdf(self, template_values: PDFTemplateValues):
        self.last_template_values = template_values


class TestCreateRegistrationPDF(TestBase):
    def setup_method(self):
        self.mock_pdf_generator = MockPDFGenerator()
        self.redis = MockRedis()

        self.load_command_instance(
            document,
            inframocks={
                "redis": self.redis,
                "pdf_generator": self.mock_pdf_generator,
            },
        )

        # Remove admin role from standard mockend command instance
        self.cmd.user_info.permissions.pop("admin")

        # Inject pip_user role for testing
        self.cmd.user_info.permissions["pip_user"] = True

    @freeze_time("2024-02-15")
    def test_create_pdf_for_case_registration_np(self):
        case_uuid = uuid4()
        case_id = random.randint(1, 1000)

        mock_case = mock.Mock()
        mock_case.configure_mock(
            id=case_id,
            uuid=case_uuid,
            case_type={
                "label": "Case type name here",
                "needs_registration_form": True,
                "custom_fields": [
                    {
                        "phase_number": 1,
                        "magic_string": "alakazam",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "An internal name",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "alakazam2",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "An internal name not in form",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "abracadabra",
                        "value_type": "text",
                        "is_internal": True,
                        "label": "Internal field, not shown on form",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus",
                        "value_type": "bag_adres",
                        "is_internal": False,
                        "label": "Address field 1",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus2",
                        "value_type": "address_v2",
                        "is_internal": False,
                        "label": "Address field 2",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus3",
                        "value_type": "googlemaps",
                        "is_internal": False,
                        "label": "Address field 3",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus4",
                        "value_type": "address_v2",
                        "is_internal": False,
                        "label": "Address field 4",
                    },
                    {
                        "phase_number": 2,
                        "magic_string": "shazam",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "Phase 2 should not be included",
                    },
                ],
            },
        )
        casetype_rows = [
            mock.Mock(
                id=1, label="Groupname 1", is_group=True, magic_string=""
            ),
            mock.Mock(
                id=2, label="alakazam", is_group=False, magic_string="alakazam"
            ),
            mock.Mock(
                id=3,
                label="abracadabra",
                is_group=False,
                magic_string="abracadabra",
            ),
            mock.Mock(
                id=4,
                label="hocuspocus",
                is_group=False,
                magic_string="hocuspocus",
            ),
            mock.Mock(
                id=5,
                label="hocuspocus2",
                is_group=False,
                magic_string="hocuspocus2",
            ),
            mock.Mock(
                id=6, label="Groupname 2", is_group=True, magic_string=""
            ),
            mock.Mock(
                id=7,
                label="hocuspocus3",
                is_group=False,
                magic_string="hocuspocus3",
            ),
            mock.Mock(
                id=8,
                label="hocuspocus4",
                is_group=False,
                magic_string="hocuspocus4",
            ),
        ]

        self.session.execute().fetchone.side_effect = [
            mock_case,
            mock_case,
            mock_case,
        ]
        self.session.execute().fetchall.side_effect = [
            casetype_rows,
            [],
            casetype_rows,
            casetype_rows,
        ]
        self.session.reset_mock()

        fields = {
            "attributes": {
                "alakazam": "some text",
                # "alakazam2": <not here>
                "abracadabra": "internal",
                "hocuspocus": {
                    "address_data": {
                        "gps_lat_lon": "1.53,3.92",
                        "huisletter": "A",
                        "huisnummer": 2,
                        "huisnummertoevoeging": None,
                        "identification": "9999999999999999",
                        "postcode": "0000AA",
                        "straat": "Test",
                        "woonplaats": "Testing",
                    },
                    "bag_id": "nummeraanduiding-9999999999999999",
                    "human_identifier": "Test 2A, 0000AA Testing",
                },
                "hocuspocus2": {
                    "address": {"full": "Test 2A, 0000AA Testing"},
                    "bag": {
                        "id": "nummeraanduiding-9999999999999999",
                        "type": "nummeraanduiding",
                    },
                    "geojson": {
                        "features": [
                            {
                                "geometry": {
                                    "coordinates": [
                                        3.92,
                                        1.53,
                                    ],
                                    "type": "Point",
                                },
                                "properties": {},
                                "type": "Feature",
                            }
                        ],
                        "type": "FeatureCollection",
                    },
                },
                "hocuspocus3": "Test 2A, 0000AA Testing",
                "hocuspocus4": {},  # invalid address v2
            },
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "name": "A Aaa",
                "country": "Nederland",
                "street": "Teststraat",
                "streetnumber": "1337",
                "streetletter": "A",
                "streetnumberaddition": "ZWAG",
                "zipcode": "1337ZZ",
                "city": "Utopia",
                "type": "natuurlijk_persoon",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        self.cmd.create_pdf_for_case_registration(
            case_uuid=case_uuid, fields=fields
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 2

        select_statement_1 = calls[0][0][0]
        compiled = select_statement_1.compile(dialect=postgresql.dialect())

        assert str(compiled) == (
            "SELECT zaak.uuid, zaak.id, (SELECT json_build_object(%(json_build_object_2)s, zaaktype_node.titel, %(json_build_object_3)s, CASE WHEN ((CAST(zaaktype_node.properties AS JSON) ->> %(param_1)s) = %(param_2)s) THEN %(param_3)s ELSE %(param_4)s END, %(json_build_object_4)s, (SELECT json_agg(json_build_object(%(json_build_object_5)s, zaaktype_status.status, %(json_build_object_6)s, bibliotheek_kenmerken.magic_string, %(json_build_object_7)s, bibliotheek_kenmerken.value_type, %(json_build_object_8)s, zaaktype_kenmerken.is_systeemkenmerk, %(json_build_object_9)s, coalesce(nullif(zaaktype_kenmerken.label, %(nullif_1)s), nullif(bibliotheek_kenmerken.naam_public, %(nullif_2)s), bibliotheek_kenmerken.naam)) ORDER BY zaaktype_status.status, zaaktype_kenmerken.id) AS json_agg_1 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaaktype_status ON zaaktype_kenmerken.zaak_status_id = zaaktype_status.id \n"
            "WHERE zaaktype_kenmerken.zaaktype_node_id = zaaktype_node.id)) AS json_build_object_1 \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.id = zaak.zaaktype_node_id) AS case_type \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert compiled.params == {
            "json_build_object_2": "label",
            "json_build_object_3": "needs_registration_form",
            "json_build_object_4": "custom_fields",
            "json_build_object_5": "phase_number",
            "json_build_object_6": "magic_string",
            "json_build_object_7": "value_type",
            "json_build_object_8": "is_internal",
            "json_build_object_9": "label",
            "nullif_1": "",
            "nullif_2": "",
            "param_1": "pdf_registration_form",
            "param_2": "1",
            "param_3": True,
            "param_4": False,
            "uuid_1": case_uuid,
        }

        select_statement_2 = calls[1][0][0]
        compiled_2 = select_statement_2.compile(dialect=postgresql.dialect())

        assert str(compiled_2) == (
            "SELECT zaaktype_kenmerken.id, zaaktype_kenmerken.label, zaaktype_kenmerken.is_group, bibliotheek_kenmerken.magic_string \n"
            "FROM zaaktype_kenmerken JOIN zaaktype_node ON zaaktype_kenmerken.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaaktype_status ON zaaktype_status.id = zaaktype_kenmerken.zaak_status_id \n"
            "WHERE zaaktype_node.uuid = %(uuid_1)s::UUID AND zaaktype_status.status = %(status_1)s ORDER BY zaaktype_kenmerken.id ASC"
        )

        assert self.mock_pdf_generator.last_template_values == {
            "attributes": [
                {
                    "name": "Naam",
                    "sequence": 1,
                    "type": "text",
                    "address": [{}],
                    "value": "A Aaa",
                },
                {
                    "name": "Straat",
                    "sequence": 2,
                    "type": "text",
                    "address": [{}],
                    "value": "Teststraat",
                },
                {
                    "name": "Huisnummer",
                    "sequence": 3,
                    "type": "text",
                    "address": [{}],
                    "value": "1337",
                },
                {
                    "name": "Huisletter",
                    "sequence": 4,
                    "type": "text",
                    "address": [{}],
                    "value": "A",
                },
                {
                    "name": "Huisnummertoevoeging",
                    "sequence": 5,
                    "type": "text",
                    "address": [{}],
                    "value": "ZWAG",
                },
                {
                    "name": "Postcode",
                    "sequence": 6,
                    "type": "text",
                    "address": [{}],
                    "value": "1337ZZ",
                },
                {
                    "name": "Plaats",
                    "sequence": 7,
                    "type": "text",
                    "address": [{}],
                    "value": "Utopia",
                },
                {
                    "name": "Land",
                    "sequence": 8,
                    "type": "text",
                    "address": [{}],
                    "value": "Nederland",
                },
                {
                    "name": "Groupname 1",
                    "sequence": 9,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "An internal name",
                    "sequence": 10,
                    "type": "text",
                    "address": [{}],
                    "value": "some text",
                },
                {
                    "name": "Address field 1",
                    "sequence": 11,
                    "type": "address",
                    "address": [
                        {
                            "human_value": "Test 2A, 0000AA Testing",
                            "bag_id": "nummeraanduiding-9999999999999999",
                            "coordinates": "1.53,3.92",
                        }
                    ],
                    "value": "",
                },
                {
                    "name": "Address field 2",
                    "sequence": 12,
                    "type": "address",
                    "address": [
                        {
                            "human_value": "Test 2A, 0000AA Testing",
                            "bag_id": "nummeraanduiding-9999999999999999",
                            "coordinates": "1.53,3.92",
                        }
                    ],
                    "value": "",
                },
                {
                    "name": "Groupname 2",
                    "sequence": 13,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "Address field 3",
                    "sequence": 14,
                    "type": "googlemaps",
                    "address": [{}],
                    "value": "Test 2A, 0000AA Testing",
                },
                {
                    "name": "Address field 4",
                    "sequence": 15,
                    "type": "address",
                    "address": [
                        {"human_value": "", "bag_id": "", "coordinates": ""}
                    ],
                    "value": "",
                },
            ],
            "meta": {
                "case_number": case_id,
                "casetype_name": "Case type name here",
                "customer": "dev.zaaksysteem.nl",
                "date": "2024-02-15T00:00:00.000+00:00",
            },
            "prepared_request": {
                "upload_url": "https://dev.zaaksysteem.nl/api/v2/document/create_document_with_token",
                "token": mock.ANY,
            },
        }

        token = self.mock_pdf_generator.last_template_values[
            "prepared_request"
        ]["token"]

        assert self.redis.data == {
            f"test:tokens:{token}": (
                json.dumps(
                    {
                        "case_uuid": str(case_uuid),
                        "user_info": {
                            "type": "UserInfo",
                            "user_uuid": str(self.cmd.user_info.user_uuid),
                            "permissions": {"pip_user": True, "system": True},
                        },
                        "type": "registration_form",
                        "magic_string": "pdf_webformulier",
                        "filename": f"{case_id}-2024-02-15-Case type name here.pdf",
                    }
                ).encode(),
                300,
            ),
            f"test:tokens:pdf_form:{case_uuid}": (
                str(case_uuid).encode(),
                300,
            ),
        }

        with pytest.raises(NotFound):
            self.cmd.create_pdf_for_case_registration(
                case_uuid=case_uuid, fields=fields
            )

        fields_foreign_address = {
            "attributes": {
                "alakazam": "some text",
            },
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "name": "A Aaa",
                "country": "Nederland",
                "address": "Testweg 5 A, 1234AA Test",
                "type": "natuurlijk_persoon",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        self.cmd.create_pdf_for_case_registration(
            case_uuid=case_uuid, fields=fields_foreign_address
        )

        assert self.mock_pdf_generator.last_template_values == {
            "attributes": [
                {
                    "name": "Naam",
                    "sequence": 1,
                    "type": "text",
                    "address": [{}],
                    "value": "A Aaa",
                },
                {
                    "name": "Land",
                    "sequence": 2,
                    "type": "text",
                    "address": [{}],
                    "value": "Nederland",
                },
                {
                    "name": "Adres",
                    "sequence": 3,
                    "type": "text",
                    "address": [{}],
                    "value": "Testweg 5 A, 1234AA Test",
                },
                {
                    "name": "Groupname 1",
                    "sequence": 4,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "An internal name",
                    "sequence": 5,
                    "type": "text",
                    "address": [{}],
                    "value": "some text",
                },
            ],
            "meta": {
                "case_number": case_id,
                "casetype_name": "Case type name here",
                "customer": "dev.zaaksysteem.nl",
                "date": "2024-02-15T00:00:00.000+00:00",
            },
            "prepared_request": {
                "upload_url": "https://dev.zaaksysteem.nl/api/v2/document/create_document_with_token",
                "token": mock.ANY,
            },
        }

    @freeze_time("2024-02-15")
    def test_create_pdf_for_case_registration_nnp(self):
        case_uuid = uuid4()
        case_id = random.randint(1, 1000)

        mock_case = mock.Mock()
        mock_case.configure_mock(
            id=case_id,
            uuid=case_uuid,
            case_type={
                "label": "Case type name here",
                "needs_registration_form": True,
                "custom_fields": [
                    {
                        "phase_number": 1,
                        "magic_string": "alakazam",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "An internal name",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "alakazam2",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "An internal name not in form",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "abracadabra",
                        "value_type": "text",
                        "is_internal": True,
                        "label": "Internal field, not shown on form",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus",
                        "value_type": "bag_adres",
                        "is_internal": False,
                        "label": "Address field 1",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus2",
                        "value_type": "address_v2",
                        "is_internal": False,
                        "label": "Address field 2",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus3",
                        "value_type": "googlemaps",
                        "is_internal": False,
                        "label": "Address field 3",
                    },
                    {
                        "phase_number": 1,
                        "magic_string": "hocuspocus4",
                        "value_type": "address_v2",
                        "is_internal": False,
                        "label": "Address field 4",
                    },
                    {
                        "phase_number": 2,
                        "magic_string": "shazam",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "Phase 2 should not be included",
                    },
                ],
            },
        )

        casetype_rows = [
            mock.Mock(
                id=1, label="Groupname 1", is_group=True, magic_string=""
            ),
            mock.Mock(
                id=2, label="alakazam", is_group=False, magic_string="alakazam"
            ),
            mock.Mock(
                id=3,
                label="abracadabra",
                is_group=False,
                magic_string="abracadabra",
            ),
            mock.Mock(
                id=4,
                label="hocuspocus",
                is_group=False,
                magic_string="hocuspocus",
            ),
            mock.Mock(
                id=5,
                label="hocuspocus2",
                is_group=False,
                magic_string="hocuspocus2",
            ),
            mock.Mock(
                id=6, label="Groupname 2", is_group=True, magic_string=""
            ),
            mock.Mock(
                id=7,
                label="hocuspocus3",
                is_group=False,
                magic_string="hocuspocus3",
            ),
            mock.Mock(
                id=8,
                label="hocuspocus4",
                is_group=False,
                magic_string="hocuspocus4",
            ),
        ]

        self.session.execute().fetchone.side_effect = [mock_case, mock_case]
        self.session.execute().fetchall.side_effect = [
            casetype_rows,
            casetype_rows,
        ]
        self.session.reset_mock()

        fields = {
            "attributes": {
                "alakazam": "some text",
                # "alakazam2": <not here>
                "abracadabra": "internal",
                "hocuspocus": {
                    "address_data": {
                        "gps_lat_lon": "1.53,3.92",
                        "huisletter": "A",
                        "huisnummer": 2,
                        "huisnummertoevoeging": None,
                        "identification": "9999999999999999",
                        "postcode": "0000AA",
                        "straat": "Test",
                        "woonplaats": "Testing",
                    },
                    "bag_id": "nummeraanduiding-9999999999999999",
                    "human_identifier": "Test 2A, 0000AA Testing",
                },
                "hocuspocus2": {
                    "address": {"full": "Test 2A, 0000AA Testing"},
                    "bag": {
                        "id": "nummeraanduiding-9999999999999999",
                        "type": "nummeraanduiding",
                    },
                    "geojson": {
                        "features": [
                            {
                                "geometry": {
                                    "coordinates": [
                                        3.92,
                                        1.53,
                                    ],
                                    "type": "Point",
                                },
                                "properties": {},
                                "type": "Feature",
                            }
                        ],
                        "type": "FeatureCollection",
                    },
                },
                "hocuspocus3": "Test 2A, 0000AA Testing",
                "hocuspocus4": {},  # invalid address v2
            },
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "name": "some random bedrijf",
                "country": "Nederland",
                "street": "Teststraat",
                "streetnumber": "1337",
                "streetletter": "A",
                "streetnumberaddition": "ZWAG",
                "zipcode": "1337ZZ",
                "city": "Utopia",
                "coc_number": 12345678,
                "type": "bedrijf",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        self.cmd.create_pdf_for_case_registration(
            case_uuid=case_uuid, fields=fields
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 2

        select_statement = calls[0][0][0]
        compiled = select_statement.compile(dialect=postgresql.dialect())

        assert str(compiled) == (
            "SELECT zaak.uuid, zaak.id, (SELECT json_build_object(%(json_build_object_2)s, zaaktype_node.titel, %(json_build_object_3)s, CASE WHEN ((CAST(zaaktype_node.properties AS JSON) ->> %(param_1)s) = %(param_2)s) THEN %(param_3)s ELSE %(param_4)s END, %(json_build_object_4)s, (SELECT json_agg(json_build_object(%(json_build_object_5)s, zaaktype_status.status, %(json_build_object_6)s, bibliotheek_kenmerken.magic_string, %(json_build_object_7)s, bibliotheek_kenmerken.value_type, %(json_build_object_8)s, zaaktype_kenmerken.is_systeemkenmerk, %(json_build_object_9)s, coalesce(nullif(zaaktype_kenmerken.label, %(nullif_1)s), nullif(bibliotheek_kenmerken.naam_public, %(nullif_2)s), bibliotheek_kenmerken.naam)) ORDER BY zaaktype_status.status, zaaktype_kenmerken.id) AS json_agg_1 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaaktype_status ON zaaktype_kenmerken.zaak_status_id = zaaktype_status.id \n"
            "WHERE zaaktype_kenmerken.zaaktype_node_id = zaaktype_node.id)) AS json_build_object_1 \n"
            "FROM zaaktype_node \n"
            "WHERE zaaktype_node.id = zaak.zaaktype_node_id) AS case_type \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID"
        )
        assert compiled.params == {
            "json_build_object_2": "label",
            "json_build_object_3": "needs_registration_form",
            "json_build_object_4": "custom_fields",
            "json_build_object_5": "phase_number",
            "json_build_object_6": "magic_string",
            "json_build_object_7": "value_type",
            "json_build_object_8": "is_internal",
            "json_build_object_9": "label",
            "nullif_1": "",
            "nullif_2": "",
            "param_1": "pdf_registration_form",
            "param_2": "1",
            "param_3": True,
            "param_4": False,
            "uuid_1": case_uuid,
        }

        assert self.mock_pdf_generator.last_template_values == {
            "attributes": [
                {
                    "name": "Naam",
                    "sequence": 1,
                    "type": "text",
                    "address": [{}],
                    "value": "some random bedrijf",
                },
                {
                    "name": "KvK nummer",
                    "sequence": 2,
                    "type": "text",
                    "address": [{}],
                    "value": 12345678,
                },
                {
                    "name": "Straat",
                    "sequence": 3,
                    "type": "text",
                    "address": [{}],
                    "value": "Teststraat",
                },
                {
                    "name": "Huisnummer",
                    "sequence": 4,
                    "type": "text",
                    "address": [{}],
                    "value": "1337",
                },
                {
                    "name": "Huisletter",
                    "sequence": 5,
                    "type": "text",
                    "address": [{}],
                    "value": "A",
                },
                {
                    "name": "Huisnummertoevoeging",
                    "sequence": 6,
                    "type": "text",
                    "address": [{}],
                    "value": "ZWAG",
                },
                {
                    "name": "Postcode",
                    "sequence": 7,
                    "type": "text",
                    "address": [{}],
                    "value": "1337ZZ",
                },
                {
                    "name": "Plaats",
                    "sequence": 8,
                    "type": "text",
                    "address": [{}],
                    "value": "Utopia",
                },
                {
                    "name": "Land",
                    "sequence": 9,
                    "type": "text",
                    "address": [{}],
                    "value": "Nederland",
                },
                {
                    "name": "Groupname 1",
                    "sequence": 10,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "An internal name",
                    "sequence": 11,
                    "type": "text",
                    "address": [{}],
                    "value": "some text",
                },
                {
                    "name": "Address field 1",
                    "sequence": 12,
                    "type": "address",
                    "address": [
                        {
                            "human_value": "Test 2A, 0000AA Testing",
                            "bag_id": "nummeraanduiding-9999999999999999",
                            "coordinates": "1.53,3.92",
                        }
                    ],
                    "value": "",
                },
                {
                    "name": "Address field 2",
                    "sequence": 13,
                    "type": "address",
                    "address": [
                        {
                            "human_value": "Test 2A, 0000AA Testing",
                            "bag_id": "nummeraanduiding-9999999999999999",
                            "coordinates": "1.53,3.92",
                        }
                    ],
                    "value": "",
                },
                {
                    "name": "Groupname 2",
                    "sequence": 14,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "Address field 3",
                    "sequence": 15,
                    "type": "googlemaps",
                    "address": [{}],
                    "value": "Test 2A, 0000AA Testing",
                },
                {
                    "name": "Address field 4",
                    "sequence": 16,
                    "type": "address",
                    "address": [
                        {"human_value": "", "bag_id": "", "coordinates": ""}
                    ],
                    "value": "",
                },
            ],
            "meta": {
                "case_number": case_id,
                "casetype_name": "Case type name here",
                "customer": "dev.zaaksysteem.nl",
                "date": "2024-02-15T00:00:00.000+00:00",
            },
            "prepared_request": {
                "upload_url": "https://dev.zaaksysteem.nl/api/v2/document/create_document_with_token",
                "token": mock.ANY,
            },
        }

        token = self.mock_pdf_generator.last_template_values[
            "prepared_request"
        ]["token"]

        assert self.redis.data == {
            f"test:tokens:{token}": (
                json.dumps(
                    {
                        "case_uuid": str(case_uuid),
                        "user_info": {
                            "type": "UserInfo",
                            "user_uuid": str(self.cmd.user_info.user_uuid),
                            "permissions": {"pip_user": True, "system": True},
                        },
                        "type": "registration_form",
                        "magic_string": "pdf_webformulier",
                        "filename": f"{case_id}-2024-02-15-Case type name here.pdf",
                    }
                ).encode(),
                300,
            ),
            f"test:tokens:pdf_form:{case_uuid!s}": (
                str(case_uuid).encode(),
                300,
            ),
        }

        fields_foreign_address = {
            "attributes": {
                "alakazam": "some text",
            },
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "name": "some random bedrijf",
                "country": "Nederland",
                "address": "Teststraat 1337 A ZWAG, 1337ZZ Utopia",
                "coc_number": 12345678,
                "type": "bedrijf",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        self.cmd.create_pdf_for_case_registration(
            case_uuid=case_uuid, fields=fields_foreign_address
        )

        assert self.mock_pdf_generator.last_template_values == {
            "attributes": [
                {
                    "name": "Naam",
                    "sequence": 1,
                    "type": "text",
                    "address": [{}],
                    "value": "some random bedrijf",
                },
                {
                    "name": "Land",
                    "sequence": 2,
                    "type": "text",
                    "address": [{}],
                    "value": "Nederland",
                },
                {
                    "name": "Adres",
                    "sequence": 3,
                    "type": "text",
                    "address": [{}],
                    "value": "Teststraat 1337 A ZWAG, 1337ZZ Utopia",
                },
                {
                    "name": "KvK nummer",
                    "sequence": 4,
                    "type": "text",
                    "address": [{}],
                    "value": 12345678,
                },
                {
                    "name": "Groupname 1",
                    "sequence": 5,
                    "type": "header",
                    "address": [{}],
                    "value": "",
                },
                {
                    "name": "An internal name",
                    "sequence": 6,
                    "type": "text",
                    "address": [{}],
                    "value": "some text",
                },
            ],
            "meta": {
                "case_number": case_id,
                "casetype_name": "Case type name here",
                "customer": "dev.zaaksysteem.nl",
                "date": "2024-02-15T00:00:00.000+00:00",
            },
            "prepared_request": {
                "upload_url": "https://dev.zaaksysteem.nl/api/v2/document/create_document_with_token",
                "token": mock.ANY,
            },
        }

    def test_create_pdf_for_case_registration_case_not_found(self):
        case_uuid = uuid4()
        case_id = random.randint(1, 1000)

        self.session.execute().fetchone.side_effect = [None]
        self.session.reset_mock()
        fields = {
            "attributes": {},
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "gender": "Male",
                "name": "A Aaa",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        with pytest.raises(minty.exceptions.NotFound):
            self.cmd.create_pdf_for_case_registration(
                case_uuid=case_uuid, fields=fields
            )

    def test_create_pdf_for_case_registration_casetype_not_configured(self):
        case_uuid = uuid4()
        case_id = random.randint(1, 1000)

        mock_case = mock.Mock()
        mock_case.configure_mock(
            id=case_id,
            uuid=case_uuid,
            case_type={
                "label": "Case type name here",
                "needs_registration_form": False,
                "custom_fields": [
                    {
                        "phase_number": 1,
                        "magic_string": "alakazam",
                        "value_type": "text",
                        "is_internal": False,
                        "label": "An internal name",
                    }
                ],
            },
        )

        self.session.execute().fetchone.side_effect = [mock_case]
        self.session.reset_mock()
        self.mock_pdf_generator.reset()
        self.redis.reset()

        fields = {
            "attributes": {
                "alakazam": "some text",
                # "alakazam2": <not here>
            },
            "case_number": case_id,
            "case_uuid": str(case_uuid),
            "casetype_version_uuid": "5d51238e-f326-4530-b0b0-d69690718e68",
            "hostname": "dev.zaaksysteem.nl",
            "origin": "pip",
            "requestor": {
                "gender": "Male",
                "name": "A Aaa",
                "uuid": "e8cbe22f-252a-4a22-b93f-43264b6f423b",
            },
        }

        self.cmd.create_pdf_for_case_registration(
            case_uuid=case_uuid, fields=fields
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 1

        # Ensure the PDF generator was not called, and no auth token was
        # generated
        assert self.mock_pdf_generator.last_template_values is None
        assert self.redis.data == {}


class TestGetRegistrationForm(TestBase):
    def setup_method(self):
        self.redis = MockRedis()
        self.load_query_instance(document, inframocks={"redis": self.redis})
        self.repo = cast(
            CaseRegistrationFormRepository,
            self.qry.get_repository("registration_form"),
        )

        self.case_uuid = uuid4()
        self.document_uuid = uuid4()
        self.document_name = "pdf_webformulier"
        self.registration_form_row = mock.Mock()

        self.registration_form_row.configure_mock(
            uuid=self.document_uuid,
            id=1337,
            entity_id=self.document_uuid,
            mimetype="pdf",
            original_name=self.document_name,
            storage_location=["S3"],
        )

        self.anonymous_user_uuid = UUID("2221a63c-777c-45a6-b7c7-b23fe81df620")

    def test_get_registration_form(self):
        self.session.execute().fetchone.side_effect = (
            self.registration_form_row,
            self.registration_form_row,
        )

        registration_form = self.qry.get_registration_form(self.case_uuid)
        case_registration_form = self.repo.get_case_by_uuid(
            case_uuid=self.case_uuid,
            user_info=self.qry.user_info,
        )

        assert isinstance(case_registration_form, CaseRegistrationForm)
        assert isinstance(registration_form, CaseRegistrationForm)
        assert registration_form.uuid == self.case_uuid
        assert registration_form.document_uuid == self.document_uuid
        assert registration_form.id == 1337
        assert registration_form.entity_id == self.document_uuid
        assert registration_form.mimetype == "pdf"
        assert registration_form.document_name == self.document_name
        assert registration_form.storage_location == "S3"
        assert isinstance(
            registration_form.download_object, CaseRegistrationFormDownload
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 3
        query = calls[1][0][0]
        compiled_statement = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        query_string = str(compiled_statement)

        assert_query = (
            "SELECT zaak.id, filestore.uuid, filestore.original_name, filestore.mimetype, filestore.storage_location \n"
            "FROM zaak JOIN file_case_document ON file_case_document.case_id = zaak.id JOIN file ON file.id = file_case_document.file_id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.magic_string = %(magic_string_1)s AND zaak.uuid = %(uuid_1)s::UUID"
        )

        assert query_string == assert_query
        assert compiled_statement.params == {
            "magic_string_1": "pdf_webformulier",
            "uuid_1": self.case_uuid,
        }

    def test_get_registration_form_as_anonymous_user(self):
        self.redis.data = {
            f"test:tokens:pdf_form:{self.case_uuid!s}": (
                str(self.case_uuid).encode(),
                1,
            )
        }

        def redis_side_effect(name: str):
            if name == "redis":
                return self.redis
            else:
                return self.session

        self.repo._get_infrastructure = mock.Mock(
            side_effect=redis_side_effect
        )

        self.load_command_instance(
            document,
            inframocks={
                "redis": self.redis,
            },
        )
        self.session.execute().fetchone.side_effect = (
            self.registration_form_row,
            self.registration_form_row,
        )

        registration_form = self.qry.get_registration_form(self.case_uuid)
        case_registration_form = self.repo.get_case_by_uuid(
            case_uuid=self.case_uuid,
            user_info=UserInfo(
                self.anonymous_user_uuid,
                permissions={"anonymous": True, "pip_user": True},
            ),
        )

        assert isinstance(case_registration_form, CaseRegistrationForm)
        assert isinstance(registration_form, CaseRegistrationForm)
        assert isinstance(
            registration_form.download_object, CaseRegistrationFormDownload
        )

        calls = self.session.execute.call_args_list
        assert len(calls) == 3
        query = calls[1][0][0]
        compiled_statement = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        query_string = str(compiled_statement)

        assert_query = (
            "SELECT zaak.id, filestore.uuid, filestore.original_name, filestore.mimetype, filestore.storage_location \n"
            "FROM zaak JOIN file_case_document ON file_case_document.case_id = zaak.id JOIN file ON file.id = file_case_document.file_id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.magic_string = %(magic_string_1)s AND zaak.uuid = %(uuid_1)s::UUID"
        )

        assert query_string == assert_query
        assert compiled_statement.params == {
            "magic_string_1": "pdf_webformulier",
            "uuid_1": self.case_uuid,
        }

        self.redis.reset()
        case_registration_form = self.repo.get_case_by_uuid(
            case_uuid=self.case_uuid,
            user_info=UserInfo(
                self.anonymous_user_uuid,
                permissions={"anonymous": True, "pip_user": True},
            ),
        )
        assert case_registration_form == "Token expired or not present"

        self.qry.user_info = UserInfo(
            self.anonymous_user_uuid,
            permissions={"anonymous": True, "pip_user": True},
        )
        with pytest.raises(Forbidden):
            self.qry.get_registration_form(uuid4())

    def test_get_registration_form_as_pip_user(self):
        user_uuid = uuid4()
        self.load_command_instance(
            document,
        )
        self.session.execute().fetchone.side_effect = (
            self.registration_form_row,
            self.registration_form_row,
        )

        case_registration_form = self.repo.get_case_by_uuid(
            case_uuid=self.case_uuid,
            user_info=UserInfo(
                user_uuid,
                permissions={"pip_user": True},
            ),
        )

        assert isinstance(case_registration_form, CaseRegistrationForm)

        calls = self.session.execute.call_args_list
        assert len(calls) == 2
        query = calls[1][0][0]
        compiled_statement = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        query_string = str(compiled_statement)

        assert_query = (
            "SELECT zaak.id, filestore.uuid, filestore.original_name, filestore.mimetype, filestore.storage_location \n"
            "FROM zaak JOIN file_case_document ON file_case_document.case_id = zaak.id JOIN file ON file.id = file_case_document.file_id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.magic_string = %(magic_string_1)s AND zaak.uuid = %(uuid_1)s::UUID AND (EXISTS (SELECT 1 \n"
            "FROM zaak \n"
            "WHERE (SELECT zaak_betrokkenen.subject_id \n"
            "FROM zaak_betrokkenen \n"
            "WHERE zaak.uuid = %(uuid_2)s::UUID AND zaak_betrokkenen.id = zaak.aanvrager) = %(param_1)s::UUID))"
        )

        assert query_string == assert_query

    def test_get_registration_form_as_gebruiker(self):
        user_uuid = uuid4()
        self.load_command_instance(
            document,
        )
        self.session.execute().fetchone.side_effect = (
            self.registration_form_row,
            self.registration_form_row,
        )

        case_registration_form = self.repo.get_case_by_uuid(
            case_uuid=self.case_uuid,
            user_info=UserInfo(
                user_uuid,
                permissions={"gebruiker": True},
            ),
        )

        assert isinstance(case_registration_form, CaseRegistrationForm)

        calls = self.session.execute.call_args_list
        assert len(calls) == 2
        query = calls[1][0][0]
        compiled_statement = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        query_string = str(compiled_statement)

        assert_query = (
            "SELECT zaak.id, filestore.uuid, filestore.original_name, filestore.mimetype, filestore.storage_location \n"
            "FROM zaak JOIN file_case_document ON file_case_document.case_id = zaak.id JOIN file ON file.id = file_case_document.file_id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.magic_string = %(magic_string_1)s AND zaak.uuid = %(uuid_1)s::UUID AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID)))"
        )

        assert query_string == assert_query

    def test_get_registration_form_not_existing(self):
        case_uuid = uuid4()

        self.session.execute().fetchone.side_effect = ((), ())
        result = self.repo.get_case_by_uuid(case_uuid, self.qry.user_info)

        assert result is None

        with pytest.raises(NotFound):
            self.qry.get_registration_form(case_uuid)

    def test_download_registration_form(self):
        self.session.execute().fetchone.side_effect = (
            self.registration_form_row,
        )
        response = self.qry.download_registration_form(self.case_uuid)
        assert isinstance(response, RedirectResponse)

    def test_download_registration_form_not_existing(self):
        self.session.execute().fetchone.side_effect = ((),)
        with pytest.raises(NotFound):
            self.qry.download_registration_form(self.case_uuid)


class TestFormatAttributeValues(TestBase):
    def test_html_tag_stripper(self):
        # <p><strong>dik</strong></p>
        # <p><em>schuin</em></p>
        # <p>onderstreept</p>
        # <p><strong><em>dik schuin onderstreept</em></strong></p>
        # <p><strong><em>dik schuin</em></strong></p>
        # <p><strong>dik onderstreept</strong></p>
        # <p>twee<br>regels</p>
        # <p></p>
        # <ul>
        #     <li>unordered list 1</li>
        #     <li>2</li>
        #     <li>3</li>
        # </ul>
        # <p></p>
        # <ol>
        #     <li>ordered list 1</li>
        #     <li>2</li>
        #     <li>3</li>
        # </ol>
        # <p></p>
        # <p><a href="https://google.nl">hyperlink</a></p>
        attribute_value = '<p><strong>dik</strong></p><p><em>schuin</em></p><p>onderstreept</p><p><strong><em>dik schuin onderstreept</em></strong></p><p><strong><em>dik schuin</em></strong></p><p><strong>dik onderstreept</strong></p><p>twee<br>regels</p><p></p><ul><li>unordered list 1</li><li>2</li><li>3</li></ul><p></p><ol><li>ordered list 1</li><li>2</li><li>3</li></ol><p></p><p><a href="https://google.nl">hyperlink</a></p>'
        parsed_string = _format_value(attribute_value, "richtext")

        assert (
            parsed_string
            == """dik
schuin
onderstreept
dik schuin onderstreept
dik schuin
dik onderstreept
twee
regels


- unordered list 1
- 2
- 3


1. ordered list 1
2. 2
3. 3

https://google.nl -> hyperlink"""
        )

    def test_file_formatter(self):
        attribute_value = [
            {
                "filename": "165950-2024-04-05-Barnie Simpel.pdf",
                "size": 58045,
                "uuid": "5e1c23a5-4f23-4812-89b7-357239beebae",
                "accepted": 0,
                "is_archivable": 1,
                "confidential": False,
                "md5": "aebb8da06276a985a536a3b37d1250ba",
                "thumbnail_uuid": None,
                "original_name": "165950-2024-04-05-Barnie Simpel.pdf",
                "file_id": 45,
                "mimetype": "application/pdf",
            }
        ]
        parsed_string = _format_value(attribute_value, "file")

        assert parsed_string == "165950-2024-04-05-Barnie Simpel.pdf"

    def test_date_formatter(self):
        attribute_value = "2024-04-17T00:00:00"
        parser_string = _format_value(attribute_value, "date")

        assert parser_string == "17-04-2024"
