# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import UTC, datetime, timedelta
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import document

mock_document_uuid = uuid4()
mock_case_uuid = uuid4()
mock_user_uuid = uuid4()
mock_user_display_name = "admin"
mock_case_id = uuid4()
mock_timestamp = datetime.now(tz=UTC) + timedelta(minutes=15)


class Test_Document_Locking(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        self.user_uuid = mock_user_uuid
        self.mock_document = mock.Mock()
        self.mock_document.configure_mock(
            document_uuid=mock_document_uuid,
            case_uuid=mock_case_uuid,
            filename="doc_1",
            labels=[],
            lock_user_display_name=mock_user_display_name,
            lock_user_uuid=mock_user_uuid,
            case_display_number=mock_case_id,
            lock_timestamp=mock_timestamp,
            shared=False,
            document_status="original",
        )

    def test_document_lock_release(self):
        mock_document = self.mock_document
        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()
        self.cmd.release_document_lock_for_wopi(
            document_uuid=mock_document_uuid, user_uuid=mock_user_uuid
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        update_statement = call_list[1][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE file SET lock_timestamp=%(lock_timestamp)s, lock_subject_id=%(lock_subject_id)s::UUID, lock_subject_name=%(lock_subject_name)s, shared=%(shared)s WHERE file.active_version IS true AND file.uuid = %(uuid_1)s::UUID"
        )

    def test_document_lock_extend(self):
        mock_document = self.mock_document
        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()

        self.cmd.extend_document_lock_for_wopi(
            document_uuid=mock_document_uuid, user_uuid=mock_user_uuid
        )
        call_list = self.session.execute.call_args_list

        assert len(call_list) == 2

        update_statement = call_list[1][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE file SET lock_timestamp=%(lock_timestamp)s, shared=%(shared)s WHERE file.active_version IS true AND file.uuid = %(uuid_1)s::UUID"
        )

    def test_document_lock_extend_no_case_id(self):
        mock_timestamp = datetime.now(tz=UTC) + timedelta(minutes=-15)
        mock_document = self.mock_document
        mock_document.lock_timestamp = mock_timestamp
        mock_document.case_display_number = None

        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.extend_document_lock_for_wopi(
                document_uuid=mock_document_uuid, user_uuid=mock_user_uuid
            )
        assert excinfo.value.args == (
            "File is not locked.",
            "file/lock/not_locked",
        )

    def test_document_lock_extend_timestamp_expired(self):
        mock_timestamp = datetime.now(tz=UTC) - timedelta(minutes=30)
        mock_document = self.mock_document
        mock_document.lock_timestamp = mock_timestamp
        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.extend_document_lock_for_wopi(
                document_uuid=mock_document_uuid, user_uuid=mock_user_uuid
            )
        assert excinfo.value.args == (
            "File is not locked.",
            "file/lock/not_locked",
        )

    def test_document_lock_extend_for_logged_in_user(self):
        mock_timestamp = None
        mock_document = self.mock_document
        mock_document.lock_timestamp = mock_timestamp

        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.extend_document_lock_for_wopi(
                document_uuid=mock_document_uuid, user_uuid=mock_user_uuid
            )
        assert excinfo.value.args == (
            "File is not locked.",
            "file/lock/not_locked",
        )

    def test_document_lock_acquired(self):
        mock_document = self.mock_document

        self.session.execute().fetchone.return_value = mock_document
        self.session.execute.reset_mock()

        self.cmd.acquire_document_lock_for_wopi(
            document_uuid=mock_document_uuid,
            user_uuid=mock_user_uuid,
            user_display_name=mock_user_display_name,
        )
        update = str(self.session.execute.call_args_list[1][0][0])

        assert update == (
            "UPDATE file SET lock_timestamp=:lock_timestamp, lock_subject_id=:lock_subject_id, lock_subject_name=:lock_subject_name, shared=:shared WHERE file.active_version IS true AND file.uuid = :uuid_1"
        )
