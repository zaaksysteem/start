# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
import random
import time
from collections import namedtuple
from datetime import UTC, datetime, timedelta, timezone
from freezegun import freeze_time
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains import document

DocumentRow = namedtuple(
    "DocumentRow",
    [
        "document_uuid",
        "store_uuid",
        "mimetype",
        "size",
        "storage_location",
        "md5",
        "is_archivable",
        "virus_scan_status",
        "directory_uuid",
        "case_uuid",
        "case_display_number",
        "filename",
        "extension",
        "accepted",
        "creator_uuid",
        "creator_displayname",
        "date_modified",
        "id",
        "preview_uuid",
        "preview_storage_location",
        "preview_mimetype",
        "description",
        "origin",
        "origin_date",
        "thumbnail_uuid",
        "thumbnail_storage_location",
        "thumbnail_mimetype",
        "intake_owner_uuid",
        "intake_group_uuid",
        "intake_role_uuid",
        "has_search_index",
        "version",
        "confidentiality",
        "document_category",
        "labels",
        "lock_user_uuid",
        "lock_user_display_name",
        "lock_timestamp",
        "lock_shared",
        "publish_pip",
        "publish_website",
        "document_source",
        "document_status",
        "pronom_format",
        "appearance",
        "structure",
    ],
)


class Test_as_an_employee_I_want_to_add_document_to_a_case(TestBase):
    """As an employee I want to add document to a case."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())
        self.case_uuid = str(uuid4())
        self.document_label_uuid = str(uuid4())

        document_file = DocumentRow(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            confidentiality="confidential",
            document_category=None,
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        document_label = namedtuple(
            "DocumentLabel",
            ["uuid", "name", "public_name", "magic_string", "attribute_id"],
        )(
            uuid=self.document_label_uuid,
            name="doc_att",
            public_name="doc",
            magic_string="magic",
            attribute_id=uuid4(),
        )

        file_metadata = namedtuple("FileMetaData", ["id"])(id=26)
        case = namedtuple("Case", ["id", "status"])(id=9, status="open")

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = document_file

        self.mock_get_document_labels = mock.MagicMock()
        self.mock_get_document_labels.fetchall.return_value = [document_label]

        self.mock_get_case = mock.MagicMock()
        self.mock_get_case.fetchone.return_value = case

        self.mock_get_file_metadata = mock.MagicMock()
        self.mock_get_file_metadata.fetchone.return_value = file_metadata

        self.mock_get_case_unaccepted_files = mock.MagicMock()
        self.mock_get_case_unaccepted_files.first.return_value = namedtuple(
            "CountUnacceptedFiles", "count"
        )(count=1)

        self.mock_get_case_property_document_label = mock.Mock()
        self.mock_get_case_property_document_label.fetchone.return_value = (
            namedtuple("CaseProperty", "value_v0")(value_v0=[{"md5": 123}])
        )

        case_uuid = uuid4()
        self.mock_get_case_by_document = mock.Mock()
        self.mock_get_case_by_document.scalar.return_value = case_uuid

        self.mock_get_case_uuids = mock.Mock()
        self.mock_get_case_uuids.fetchall.return_value = []

        self.mock_get_label_defaults = mock.Mock()
        self.mock_get_label_defaults.fetchone.return_value = mock.Mock(
            show_on_pip=True,
            show_on_website=False,
            trust_level="Openbaar",
            document_category="Aangifte",
            origin="Inkomend",
        )

        self.mock_update_file_metadata = mock.Mock()
        self.mock_insert_file_metadata = mock.Mock(inserted_primary_key=[123])
        self.mock_update_file = mock.Mock()
        self.mock_update_case_property_case_documents = mock.Mock()
        self.mock_update_file_case_document = mock.Mock()
        self.mock_update_case_property_document_label = mock.Mock()
        self.mock_update_case_property_unaccepted_files = mock.Mock()
        self.mock_update_object_data = mock.Mock()
        self.mock_update_case_meta = mock.Mock()

    def test_add_document_to_case(self):
        """Test add document to a case without any error"""
        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

        self.assert_has_event_name("DocumentAddedToCase")

    def test_add_document_to_case_when_case_property_with_document_label_doesnot_exists(
        self,
    ):
        """Test add document to a case when the case_property for document_label doesnot exists"""
        self.mock_get_label_defaults.fetchone.return_value = None
        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
        ]
        queries = []

        def mock_execute(query):
            queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

        update_file = queries[3].compile(dialect=postgresql.dialect())

        assert str(update_file) == (
            "UPDATE file SET case_id=(SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s::UUID) WHERE file.uuid = %(uuid_2)s::UUID"
        )

        get_label_defaults = queries[8].compile(dialect=postgresql.dialect())
        assert str(get_label_defaults) == (
            "SELECT zaaktype_document_kenmerken_map.show_on_pip, zaaktype_document_kenmerken_map.show_on_website, file_metadata.trust_level, file_metadata.document_category, file_metadata.origin \n"
            "FROM zaaktype_document_kenmerken_map JOIN bibliotheek_kenmerken ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN file_metadata ON file_metadata.id = bibliotheek_kenmerken.file_metadata_id \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_1)s::UUID"
        )

    def test_add_document_to_case_when_document_has_no_metadata(self):
        """Test add document to a case when the document has no meta data"""
        self.mock_get_file_metadata.fetchone.return_value = None
        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_insert_file_metadata,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

    def test_add_document_to_resolved_case(self):
        """Test add document to resolved case"""
        self.mock_get_case.fetchone.return_value = namedtuple(
            "Case", ["id", "status"]
        )(id=10, status="resolved")

        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
            self.mock_update_case_property_case_documents,
            self.mock_update_file_case_document,
            self.mock_get_case_property_document_label,
            self.mock_update_case_property_document_label,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_property_unaccepted_files,
            self.mock_update_object_data,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"Document cannot be added to resolved case with uuid '{self.case_uuid}'",
            "document/cannot_add_to_resolved_case",
        )

    def test_add_document_to_case_but_document_is_already_linked_to_a_case(
        self,
    ):
        """Test add document to a case when already a case was linked to the document"""
        case_uuid = str(uuid4())
        document = DocumentRow(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=case_uuid,
            case_display_number=9,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            confidentiality="confidential",
            document_category=None,
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )
        self.mock_get_document.fetchone.return_value = document

        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"Document with uuid '{self.document_uuid}' is already linked to a case with uuid '{case_uuid}'.",
            "document/already_linked_to_a_case",
        )

    def test_add_document_to_case_but_document_cannot_be_found(self):
        """Test add document to a case but document with given uuid is not accessable to the user/document doesn't exists"""

        self.mock_get_document.fetchone.return_value = None

        mock_rows = [
            self.mock_get_document,
            self.mock_get_document_labels,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"No document found with uuid={self.document_uuid}",
            "document/not_found",
        )


class Test_as_an_employee_I_want_to_update_a_document(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        document_file = DocumentRow(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        file_metadata = namedtuple("FileMetaData", ["id"])(id=26)

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = document_file

        self.mock_get_file_metadata = mock.MagicMock()
        self.mock_get_file_metadata.fetchone.return_value = file_metadata

        self.mock_update_file_metadata = mock.MagicMock()
        self.mock_update_file = mock.MagicMock()

        self.mock_get_contact = mock.Mock()
        mock_contact = mock.Mock()
        mock_contact.id = random.randint(1, 1000)
        mock_contact.subject_type = "medewerker"
        self.mock_get_contact.fetchone.return_value = mock_contact

    def test_update_document(self):
        """Test update document"""
        mock_rows = [
            self.mock_get_document,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_get_contact,
            self.mock_update_file,
        ]

        queries = []

        def mock_execute(query):
            queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.update_document(
            document_uuid=self.document_uuid,
            origin="Intern",
            origin_date="2020-05-13",
            description="description",
        )

        self.assert_has_event_name("DocumentUpdated")

        update_metadata_query = queries[2].compile(
            dialect=postgresql.dialect()
        )
        assert (
            (str(update_metadata_query))
            == "UPDATE file_metadata SET description=%(description)s, trust_level=%(trust_level)s, origin=%(origin)s, document_category=%(document_category)s, origin_date=%(origin_date)s, pronom_format=%(pronom_format)s, appearance=%(appearance)s, structure=%(structure)s, document_source=%(document_source)s WHERE file_metadata.id = %(id_1)s"
        )

        get_contact_query = queries[3].compile(dialect=postgresql.dialect())
        assert str(get_contact_query) == (
            "SELECT subject.id, %(param_1)s AS subject_type \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s::UUID AND subject.subject_type = %(subject_type_1)s UNION ALL SELECT natuurlijk_persoon.id, %(param_2)s AS subject_type \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_2)s::UUID UNION ALL SELECT bedrijf.id, %(param_3)s AS subject_type \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_3)s::UUID"
        )
        assert get_contact_query.params == {
            "param_1": "medewerker",
            "param_2": "natuurlijk_persoon",
            "param_3": "bedrijf",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_uuid,
            "uuid_2": self.cmd.user_uuid,
            "uuid_3": self.cmd.user_uuid,
        }

        update_file_query = queries[4].compile(dialect=postgresql.dialect())
        assert str(update_file_query) == (
            "UPDATE file SET name=%(name)s, metadata_id=%(metadata_id)s, publish_pip=%(publish_pip)s, publish_website=%(publish_website)s, date_modified=(CURRENT_TIMESTAMP AT TIME ZONE %(current_timestamp_1)s), modified_by=%(modified_by)s, document_status=%(document_status)s, search_term=concat_ws(%(concat_ws_1)s, file.name, (SELECT file_metadata.description \n"
            "FROM file_metadata \n"
            "WHERE file.metadata_id = file_metadata.id)) WHERE file.uuid = %(uuid_1)s::UUID"
        )
        assert update_file_query.params == {
            "concat_ws_1": " ",
            "current_timestamp_1": "UTC",
            "metadata_id": 26,
            "name": "word_document",
            "uuid_1": self.document_uuid,
            "modified_by": "betrokkene-medewerker-"
            + str(self.mock_get_contact.fetchone().id),
            "document_status": "original",
            "publish_pip": True,
            "publish_website": False,
        }

    def test_update_document_with_no_origin_date(self):
        """Test update document when origin date is null"""
        mock_rows = [
            self.mock_get_document,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_get_contact,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.update_document(
            document_uuid=self.document_uuid,
            origin="Intern",
            origin_date=None,
        )

        self.assert_has_event_name("DocumentUpdated")


class Test_as_an_employee_I_want_to_assign_document_to_user(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        documen_file = DocumentRow(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        subject = namedtuple("Subject", ["id"])(id=3)

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = documen_file

        self.mock_get_subject = mock.MagicMock()
        self.mock_get_subject.fetchone.return_value = subject

        self.mock_update_file = mock.MagicMock()

    def test_assign_document_to_user(self):
        """Test assign document to user"""
        user_uuid = str(uuid4())
        mock_rows = [
            self.mock_get_document,
            self.mock_get_subject,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.assign_document_to_user(
            document_uuid=self.document_uuid, user_uuid=user_uuid
        )

        self.assert_has_event_name("DocumentAssignedToUser")

    def test_assign_document_to_user_subject_not_found(self):
        """Test assign document to user"""
        user_uuid = str(uuid4())
        mock_rows = [
            self.mock_get_document,
            self.mock_get_subject,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.mock_get_subject.fetchone.return_value = None
            self.cmd.assign_document_to_user(
                document_uuid=self.document_uuid, user_uuid=user_uuid
            )

        assert excinfo.value.args == (
            f"Subject with uuid '{user_uuid}' not found",
            "subject/not_found",
        )

    def test_reject_assigned_document(self):
        "Test rejecting a document's intake assignment"

        document_file = mock.Mock()
        document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=uuid4(),
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        mock_get_document = mock.Mock()
        mock_get_document.fetchone.return_value = document_file

        mock_rows = [
            mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute.side_effect = mock_execute
        self.session.reset_mock()

        self.cmd.reject_assigned_document(
            document_uuid=self.document_uuid,
            rejection_reason="I don't like it.",
        )

        self.assert_has_event_name("DocumentAssignmentRejected")

    def test_reject_assigned_document_failure(self):
        "Test rejecting a document's intake assignment"

        document_file = mock.Mock()
        document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
            has_search_index=False,
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        mock_get_document = mock.Mock()
        mock_get_document.fetchone.return_value = document_file

        mock_rows = [
            mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute.side_effect = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict):
            self.cmd.reject_assigned_document(
                document_uuid=self.document_uuid,
                rejection_reason="I don't like it.",
            )

        document_file.intake_role_uuid = uuid4()
        document_file.case_uuid = uuid4()

        mock_rows = [
            mock_get_document,
            self.mock_update_file,
        ]

        with pytest.raises(Conflict):
            self.cmd.reject_assigned_document(
                document_uuid=self.document_uuid,
                rejection_reason="I don't like it.",
            )


class Test_as_an_employee_I_want_to_assign_document_to_role(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        documen_file = DocumentRow(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            intake_role_uuid=None,
            intake_group_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            has_search_index=False,
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = documen_file

        self.mock_update_file = mock.MagicMock()

    def test_assign_document_to_role(self):
        """Test assign document to role"""
        role_uuid = str(uuid4())
        group_uuid = str(uuid4())

        mock_rows = [
            self.mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.assign_document_to_role(
            document_uuid=self.document_uuid,
            group_uuid=group_uuid,
            role_uuid=role_uuid,
        )

        self.assert_has_event_name("DocumentAssignedToRole")


class Test_as_an_employee_I_want_to_get_thumbnail_for_a_document(TestBase):
    """As an employee I want to get thumbnail for document."""

    def setup_method(self):
        self.load_query_instance(
            document,
            inframocks={
                "amqp": mock.Mock(),
                "redis": mock.Mock(),
            },
        )

        user_info = UserInfo(
            user_uuid=uuid4(), permissions={"pip_user": False}
        )
        self.qry.user_info = user_info

        self.config = {
            "instance_uuid": "test",
            "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
            "rate_limits": {
                "query_event:zsnl_domains_document:Document:PreviewRequested": 60,
                "query_event:zsnl_domains_document:Document:ThumbnailRequested": 60,
            },
        }

        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: self.config
        )

        self.document_uuid = str(uuid4())
        self.thumbnail_uuid = uuid4()

        self.document_file = mock.Mock()
        self.document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="image/png",
            size=123,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="an_image",
            extension=".png",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            intake_role_uuid=None,
            intake_group_uuid=None,
            thumbnail_uuid=self.thumbnail_uuid,
            thumbnail_mimetype="image/png",
            thumbnail_storage_location=["minio"],
            version=random.randint(1, 10),
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            document_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.mock_get_document = mock.Mock()
        self.mock_get_document.fetchone.return_value = self.document_file

    def test_get_document_thumbnail(self):
        mock_rows = [self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.qry.get_document_thumbnail_link(document_uuid=self.document_uuid)

        self.qry.get_repository("document")._get_infrastructure(
            "s3"
        ).get_download_url.assert_called_once_with(
            uuid=self.thumbnail_uuid,
            storage_location="minio",
            mime_type="image/png",
            filename=None,
            download=False,
        )

    def test_get_document_thumbnail_disabled_ratelimist(self):
        # Disable rate-limiting
        self.config["rate_limits"][
            "query_event:zsnl_domains_document:Document:ThumbnailRequested"
        ] = -1

        self.document_file.thumbnail_uuid = None
        self.mock_get_document.fetchone.return_value = self.document_file

        mock_rows = [self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_document_thumbnail_link(
                document_uuid=self.document_uuid
            )

        assert excinfo.value.args[1] == "document/thumbnail_creating"

        self.infrastructure_factory.infra["redis"].pipeline.assert_not_called()

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_called_once()

    @freeze_time()
    def test_get_document_thumbnail_where_thumbnail_not_found_but_can_create(
        self,
    ):
        self.infrastructure_factory_ro.infra["redis"].get.return_value = None

        self.document_file.thumbnail_uuid = None
        self.mock_get_document.fetchone.return_value = self.document_file

        mock_rows = [self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_document_thumbnail_link(
                document_uuid=self.document_uuid
            )

        assert excinfo.value.args[1] == "document/thumbnail_creating"

        self.infrastructure_factory.infra[
            "redis"
        ].pipeline.assert_called_once_with()

        pl: mock.Mock = self.infrastructure_factory_ro.infra[
            "redis"
        ].pipeline()

        minute: int = time.gmtime().tm_min

        pl.incr.assert_any_call(
            f"test:rate_limit:query_event:zsnl_domains_document:Document:ThumbnailRequested:{self.document_uuid}:{minute}",
            1,
        )
        pl.expire.assert_any_call(
            f"test:rate_limit:query_event:zsnl_domains_document:Document:ThumbnailRequested:{self.document_uuid}:{minute}",
            60,
        )
        pl.execute.assert_any_call()

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_called_once()

    def test_get_document_thumbnail_where_thumbnail_not_found_but_can_create_rate_limited(
        self,
    ):
        self.infrastructure_factory.infra["redis"].get.return_value = 100

        self.document_file.thumbnail_uuid = None
        self.mock_get_document.fetchone.return_value = self.document_file

        mock_rows = [self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_document_thumbnail_link(
                document_uuid=self.document_uuid
            )

        assert excinfo.value.args[1] == "document/thumbnail_creating"

        self.infrastructure_factory.infra["redis"].pipeline.assert_not_called()

        self.infrastructure_factory.infra[
            "amqp"
        ].basic.publish.assert_not_called()

    def test_get_document_thumbnail_where_thumbnail_not_found(self):
        self.document_file.thumbnail_uuid = None
        self.document_file.extension = ".doc"
        self.document_file.mimetype = "application/msword"

        self.mock_get_document.fetchone.return_value = self.document_file

        mock_rows = [self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_document_thumbnail_link(
                document_uuid=self.document_uuid
            )

        assert excinfo.value.args == (
            f"No thumbnail found for document with uuid {self.document_uuid}",
            "document/thumbnail_not_found",
        )


class Test_as_an_employee_I_want_to_edit_document_with_ZOHO(TestBase):
    """Test document edit with ZOHO"""

    def setup_method(self):
        self.load_query_instance(
            document, {"zoho": mock.MagicMock(), "redis": mock.MagicMock()}
        )

        self.document_uuid = str(uuid4())
        self.directory_uuid = uuid4()
        self.document = mock.MagicMock()
        self.document.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=self.directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            labels=[],
            lock_user_uuid=None,
            lock_user_display_name=None,
            lock_timestamp=None,
            lock_shared=None,
            publish_pip=True,
            publish_website=False,
            docuemnt_source="test_source",
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.mock_get_user_name = mock.MagicMock()
        self.mock_get_user_name.fetchone.return_value = ["Admin"]

        self.mock_is_doc_edit_enabled = mock.MagicMock()
        self.mock_is_doc_edit_enabled.fetchone.return_value = [1]

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = self.document

        mock_rows = [
            self.mock_get_user_name,
            self.mock_is_doc_edit_enabled,
            self.mock_get_document,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.user_info = mock.MagicMock()
        self.user_info.configure_mock(user_uuid=uuid4())

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "s3"
        ).get_download_url.return_value = "document_download_url"

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "zoho"
        ).edit_document.return_value = "zoho-edit-url"

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_for_zoho(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        res = self.qry.get_edit_document_url(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
        )

        assert res == "zoho-edit-url"
        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).set.assert_called_once_with(
            f"zoho_authentication_token:edit_document_online:test:{self.document_uuid}",
            json.dumps(
                {
                    "token": "urlsafe_token",
                    "user_uuid": str(self.user_info.user_uuid),
                    "case_uuid": str(self.document.case_uuid),
                }
            ),
            3600,
        )

        document_info = {
            "document_name": "test_doc.docx",
            "document_id": f"{self.document_uuid}-test",
        }

        callback_settings = {
            "save_format": "docx",
            "save_url": "save_url",
            "context_info": "Edit document",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "zoho",
            },
        }

        user_info = {
            "user_id": self.user_info.user_uuid,
            "display_name": "Admin",
        }

        options = {
            "editor_settings": '{ "unit": "in", "language": "nl", "view": "pageview" }',
            "permissions": '{ "document.export": true, "document.print": true, "document.edit": true, "review.changes.resolve": false, "review.comment": true, "collab.chat": true }',
            "callback_settings": str(callback_settings),
            "document_info": str(document_info),
            "user_info": str(user_info),
            "document_defaults": '{ "orientation": "portrait", "paper_size": "A4", "font_name": "Lato", "font_size": 12, "track_changes": "disabled" }',
            "url": "document_download_url",
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "zoho"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_failed_when_document_has_no_case(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.case_uuid = None
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Conflict) as excinfo:
            self.qry.get_edit_document_url(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
            )

        assert excinfo.value.args == (
            "Document is not linked to any case",
            "document/case/not_exists",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_failed_with_payment_required_error(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.mock_is_doc_edit_enabled.fetchone.return_value = [0]
        with pytest.raises(Forbidden) as excinfo:
            self.qry.get_edit_document_url(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
            )

        assert excinfo.value.args == (
            "Payment required to use document editor",
            "document/edit/not_enabled",
        )


class Test_as_an_employee_I_want_to_get_wopi_configuration_for_document(
    TestBase
):
    """As an employee I want to get edit_document with Ms Online"""

    def setup_method(self):
        self.load_query_instance(
            document, {"wopi": mock.MagicMock(), "redis": mock.MagicMock()}
        )
        self.document_uuid = str(uuid4())
        self.directory_uuid = uuid4()
        self.user_uuid = uuid4()
        self.document = mock.MagicMock()
        self.document.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=self.directory_uuid,
            case_uuid=uuid4(),
            case_display_number=30,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            lock_user_display_name="Admin",
            lock_user_uuid=self.user_uuid,
            lock_timestamp=datetime.now(tz=UTC) - timedelta(minutes=10),
            lock_shared=True,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )
        self.mock_get_user_name = mock.MagicMock()
        self.mock_get_user_name.fetchone.return_value = ["Admin"]
        self.mock_user_display_name = "Admin"
        self.mock_subject_uuid = self.user_uuid
        self.mock_case_id = 20
        self.mock_timestamp = datetime.now(tz=UTC) - timedelta(minutes=30)
        document_lock = namedtuple(
            "DocumentLock",
            [
                "document_uuid",
                "lock_subject_name",
                "lock_subject_id",
                "case_id",
                "lock_timestamp",
            ],
        )(
            document_uuid=self.document_uuid,
            lock_subject_name=self.mock_user_display_name,
            lock_subject_id=self.mock_subject_uuid,
            case_id=self.mock_case_id,
            lock_timestamp=self.mock_timestamp,
        )
        self.document.lock = document_lock
        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = self.document

        mock_rows = [self.mock_get_document, self.mock_get_user_name]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.user_info = UserInfo(
            user_uuid=self.user_uuid, permissions={"pip_user": False}
        )

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "s3"
        ).get_download_url.return_value = "document_download_url"

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.return_value = {
            "favIconUrl": "favIconUrl",
            "urlsrc": "urlsrc",
            "access_token": "access_token",
            "access_token_ttl": "access_token_ttl",
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).get.return_value = json.dumps(
            {
                "token": "urlsafe_token",
                "user_uuid": str(self.user_info.user_uuid),
                "case_uuid": str(self.document.case_uuid),
                "user_permissions": json.dumps(self.user_info.permissions),
            }
        ).encode("utf-8")

    def test_get_wopi_configuration_for_document(self):
        res = self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
            lock_acquire_url="lock_acquire_url",
            lock_release_url="lock_release_url",
            lock_extend_url="lock_extend_url",
        )

        assert res == {
            "fav_icon_url": "favIconUrl",
            "action_url": "urlsrc",
            "access_token": "access_token",
            "access_token_ttl": "access_token_ttl",
        }

        document_info = {
            "extension": "docx",
            "filename": "test_doc.docx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
            "lock_acquire_url": "lock_acquire_url",
            "lock_release_url": "lock_release_url",
            "lock_extend_url": "lock_extend_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "Word",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_document_with_no_token_data(
        self, mock_secrets
    ):
        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).get.return_value = None

        mock_secrets.token_urlsafe.return_value = "urlsafe_token"
        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
            lock_acquire_url="lock_acquire_url",
            lock_release_url="lock_release_url",
            lock_extend_url="lock_extend_url",
        )

        assert (
            len(
                self.qry.repository_factory.infrastructure_factory.get_infrastructure(
                    None, "redis"
                ).set.call_args_list
            )
            == 1
        )

        set_args = self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).set.call_args[0]

        assert (
            set_args[0]
            == f"msonline:edit_document_online:test:{self.document_uuid}:{self.user_info.user_uuid}"
        )
        assert json.loads(set_args[1]) == {
            "token": "urlsafe_token",
            "user_info": {
                "type": "UserInfo",
                "user_uuid": str(self.user_info.user_uuid),
                "permissions": self.user_info.permissions,
            },
            "case_uuid": str(self.document.case_uuid),
            "user_uuid": str(self.user_info.user_uuid),
            "user_permissions": self.user_info.permissions,
        }
        assert set_args[2] == 3600

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_wopitest_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".wopitestx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
            lock_acquire_url="lock_acquire_url",
            lock_release_url="lock_release_url",
            lock_extend_url="lock_extend_url",
        )

        document_info = {
            "extension": "wopitestx",
            "filename": "test_doc.wopitestx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
            "lock_acquire_url": "lock_acquire_url",
            "lock_release_url": "lock_release_url",
            "lock_extend_url": "lock_extend_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "WopiTest",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_ppt_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".pptx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
            lock_acquire_url="lock_acquire_url",
            lock_release_url="lock_release_url",
            lock_extend_url="lock_extend_url",
        )

        document_info = {
            "extension": "pptx",
            "filename": "test_doc.pptx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
            "lock_acquire_url": "lock_acquire_url",
            "lock_release_url": "lock_release_url",
            "lock_extend_url": "lock_extend_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "PowerPoint",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_excel_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".xlsx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
            lock_acquire_url="lock_acquire_url",
            lock_release_url="lock_release_url",
            lock_extend_url="lock_extend_url",
        )

        document_info = {
            "extension": "xlsx",
            "filename": "test_doc.xlsx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
            "lock_acquire_url": "lock_acquire_url",
            "lock_release_url": "lock_release_url",
            "lock_extend_url": "lock_extend_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "Excel",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_document_is_not_supported_by_wopi(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".png"
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Forbidden) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
                lock_acquire_url="lock_acquire_url",
                lock_release_url="lock_release_url",
                lock_extend_url="lock_extend_url",
            )

        assert excinfo.value.args == (
            "Document with extension '.png' cannot be edited with Microsoft Online.",
            "document/cannot_be_edited",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_user_cannot_be_found(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.mock_get_user_name.fetchone.return_value = []

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
                lock_acquire_url="lock_acquire_url",
                lock_release_url="lock_release_url",
                lock_extend_url="lock_extend_url",
            )

        assert excinfo.value.args == (
            f"No user found with uuid '{self.user_info.user_uuid}'",
            "user/not_found",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_document_has_no_case(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"
        self.document.case_uuid = None
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Conflict) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
                lock_acquire_url="lock_acquire_url",
                lock_release_url="lock_release_url",
                lock_extend_url="lock_extend_url",
            )

        assert excinfo.value.args == (
            "Document is not linked to any case",
            "document/case/not_exists",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_document_is_locked(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"
        self.document.lock_timestamp = datetime.now(tz=UTC) + timedelta(
            minutes=10
        )

        self.document.lock_shared = None
        self.document.lock = namedtuple(
            "DocumentLock",
            [
                "document_uuid",
                "subject_name",
                "subject_id",
                "case_id",
                "timestamp",
            ],
        )(
            document_uuid=self.document_uuid,
            subject_name=self.mock_user_display_name,
            subject_id=self.mock_subject_uuid,
            case_id=20,
            timestamp=self.mock_timestamp,
        )
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Conflict) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
                lock_acquire_url="lock_acquire_url",
                lock_release_url="lock_release_url",
                lock_extend_url="lock_extend_url",
            )

        assert excinfo.value.args == (
            "Document is locked",
            "document/locked/non_editable",
        )


class Test_Bugfix_External_Document_Upload_Removes_Document_Label(TestBase):
    def setup_method(self):
        self.load_command_instance(document)

    def test_document_new_by_assignee(self):
        """ "
        Documents that are uploaded by the assignee should be accepted if
        they have a unique name
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        mock_subject = mock.Mock(uuid=self.cmd.user_uuid)

        self.session.get.side_effect = [
            mock_subject,
            mock_subject,
        ]
        self.session.execute().scalar_one.side_effect = [
            random.randint(1, 10),  # unaccepted docs (create_document)
            1,  # docs_with_same_name,
            0,  # num_unaccepted_documents
        ]
        self.session.execute().one.side_effect = [
            (random.randint(100, 199), mock_case.id),
            (case_uuid, "open"),
        ]
        self.session.execute().one_or_none.side_effect = [
            None,  # Get root file id
            None,  # Get root file id
        ]

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            mock_user_result,  # User to use for "created by" _get_legacy_contact_identifier
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=False,
            editor_update=False,
        )

        # update = str(self.session.execute.call_args_list[23][0][0])
        # assert update == (
        #     "UPDATE file_case_document SET file_id=(SELECT file.id \n"
        #     "FROM file \n"
        #     "WHERE file.uuid = :uuid_1 AND file.active_version IS true) WHERE file_case_document.file_id IN (SELECT file.id \n"
        #     "FROM file \n"
        #     "WHERE file.uuid = :uuid_2)"
        # )

    def test_document_autoaccept(self):
        """ "
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().scalar_one.side_effect = [
            random.randint(500, 599),  # sync_metadata.file_metadata_id
            random.randint(1, 10),  # unaccepted docs (create_document)
            1,  # "number of documents with the same name"
            random.randint(1, 10),  # unaccepted docs (accept_document)
        ]
        self.session.execute().one.side_effect = [
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by" _get_legacy_contact_identifier
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            editor_update=False,
        )

        update = str(self.session.execute.call_args_list[22][0][0])
        assert update == (
            "UPDATE file_case_document SET file_id=(SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = :uuid_1 AND file.active_version IS true) WHERE file_case_document.file_id IN (SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = :uuid_2)"
        )

    def test_create_document_second_version(self):
        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)

        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        # get_root_file_id
        self.session.execute().one_or_none.side_effect = [None, None]

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            None,  # get_filestore_by_uuid
            mock_directory,
            mock_case,  # _get_case_id_assignee_uuid
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=False,
            editor_update=False,
        )

        insert = str(self.session.execute.call_args_list[12][0][0])

        assert insert == (
            "INSERT INTO file (filestore_id, uuid, root_file_id, name, extension, version, case_id, directory_id, creation_reason, accepted, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, destroyed, active_version, is_duplicate_of, queue, document_status, confidential, search_term, skip_intake) VALUES (:filestore_id, :uuid, :root_file_id, :name, :extension, :version, :case_id, :directory_id, :creation_reason, :accepted, :reject_to_queue, :is_duplicate_name, :publish_pip, :publish_website, :date_created, :created_by, :date_modified, :modified_by, :destroyed, :active_version, :is_duplicate_of, :queue, :document_status, :confidential, :search_term, :skip_intake)"
        )

    def test_document_autoaccept_new_document(self):
        """
        Some new documents (mostly during migrations) are created with auto_accept = true
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_directory = mock.Mock(id=123)
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        self.session.execute().one_or_none.side_effect = [None, None]
        self.session.execute().scalar_one.side_effect = [
            random.randint(1, 10),  # unaccepted docs (create_document)
            1,  # "number of documents with the same name"
            random.randint(1, 10),  # unaccepted docs (accept_document)
        ]
        self.session.execute().one.side_effect = [
            (random.randint(0, 99), random.randint(100, 199)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            None,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
        )
        insert = self.session.execute.call_args_list[12][0][0]
        insert_compiled = insert.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(insert_compiled) == (
            "INSERT INTO file (filestore_id, uuid, root_file_id, name, extension, version, case_id, directory_id, creation_reason, accepted, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, destroyed, active_version, is_duplicate_of, queue, document_status, confidential, search_term, skip_intake) VALUES (%(filestore_id)s, %(uuid)s::UUID, %(root_file_id)s, %(name)s, %(extension)s, %(version)s, %(case_id)s, %(directory_id)s, %(creation_reason)s, %(accepted)s, %(reject_to_queue)s, %(is_duplicate_name)s, %(publish_pip)s, %(publish_website)s, %(date_created)s, %(created_by)s, %(date_modified)s, %(modified_by)s, %(destroyed)s, %(active_version)s, %(is_duplicate_of)s, %(queue)s, %(document_status)s, %(confidential)s, %(search_term)s, %(skip_intake)s) RETURNING file.id"
        )
        assert insert_compiled.params["version"] == 1


class Test_as_an_employee_I_want_to_replace_a_document(TestBase):
    def setup_method(self):
        self.load_command_instance(document)

    def test_document_replace(self):
        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()
        replaces = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )
        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        mock_deactivate_result = mock.Mock()
        mock_deactivate_result.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(300, 399),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=False,
            publish_pip=False,
            publish_website=False,
            metadata_id=random.randint(400, 499),
        )

        self.session.execute().scalar_one.side_effect = [
            random.randint(500, 599),  # Sync metadata
            random.randint(1, 10),  # unaccepted docs (create_document)
            1,  # "number of documents with the same name"
            random.randint(1, 10),  # unaccepted docs (accept_document)
        ]
        self.session.execute().one.side_effect = [
            mock_deactivate_result,
            (random.randint(0, 99), random.randint(100, 199)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            replaces=str(replaces),
            editor_update=False,
        )
        call_list = self.session.execute.call_args_list

        # UPDATE file_case_document after "DocumentAccepted"
        update_statement = call_list[23][0][0]

        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE file_case_document SET file_id=(SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true) WHERE file_case_document.file_id IN (SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_2)s::UUID)"
        )
        assert update_stmt.params["uuid_1"] == str(replaces)


class Test_as_an_employee_I_want_to_move_documents(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

    def test_move_directory_to_another_directory(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [str(uuid4()), str(uuid4())]
        sub_directory_uuid = str(uuid4())
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        sub_directory_1 = mock.Mock()
        sub_directory_1.configure_mock(
            id=3,
            name="Another Name3",
            case_id=case_id,
            original_name="Another Name3",
            path=[2],
            uuid=sub_directory_uuid,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
            source_directory_2,
            namedtuple("directory", "id")(id=2),
        ]
        self.session.execute().fetchall.side_effect = [[], [sub_directory_1]]
        self.session.reset_mock()
        self.cmd.move_to_directory(
            destination_directory_uuid=desstination_directory_uuid,
            source_directories=source_directories,
        )

        call_list = self.session.execute.call_args_list

        update_statement = call_list[8][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE directory SET path=ARRAY[%(param_1)s] WHERE directory.uuid = %(uuid_1)s::UUID"
        )

        select_statement = call_list[9][0][0]
        select_stmt = select_statement.compile(dialect=postgresql.dialect())

        assert str(select_stmt) == (
            "SELECT directory.name AS name, directory.case_id AS case_id, directory.path AS path, directory.uuid AS uuid, %(param_1)s AS parent \n"
            "FROM directory \n"
            "WHERE (SELECT directory.id \n"
            "FROM directory \n"
            "WHERE directory.uuid = %(uuid_1)s::UUID) = any(directory.path)"
        )
        update_statement = call_list[10][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE directory SET path=ARRAY[%(param_1)s, %(param_2)s] WHERE directory.uuid = %(uuid_1)s::UUID"
        )

    def test_move_directory_to_root(self):
        source_directory_uuid_1 = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        self.session.reset_mock()
        self.cmd.move_to_directory(source_directories=source_directories)

        call_list = self.session.execute.call_args_list

        select_statement = call_list[3][0][0]
        select_stmt = select_statement.compile(dialect=postgresql.dialect())

        assert str(select_stmt) == (
            "SELECT directory.name AS name, directory.case_id AS case_id, directory.path AS path, directory.uuid AS uuid, %(param_1)s AS parent \n"
            "FROM directory \n"
            "WHERE (SELECT directory.id \n"
            "FROM directory \n"
            "WHERE directory.uuid = %(uuid_1)s::UUID) = any(directory.path)"
        )

    def test_move_directory_to_different_case(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=200,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_directories=source_directories,
            )
        assert excinfo.value.args == (
            f"The destination directories case_id does not match the given directories case_id ({destination_directory.case_id} != {source_directory_1.case_id})",
            "domains/document/move_to_directory/directory_case_id_mismatch",
        )

    def test_move_directory_to_child_fails(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[1],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_directories=source_directories,
            )
        assert excinfo.value.args == (
            "Can not move directory to itself or its child directories",
            "domains/document/move_to_directory/moving_to_self",
        )

    def test_move_document_to_directory(self):
        source_doc_uuid_1 = uuid4()
        desstination_directory_uuid = uuid4()
        source_documents = [source_doc_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_doc_1 = mock.Mock()
        source_doc_1.configure_mock(
            id=source_doc_uuid_1,
            document_uuid=source_doc_uuid_1,
            filename="fake_filename",
            extension="fake_extension",
            store_uuid="fake_store_uuid",
            directory_uuid=None,
            case_uuid="fake_case_uuid",
            case_display_number=case_id,
            mimetype="fake_mimetype",
            size=1,
            storage_location="fake_storage_location",
            md5="fake_md5",
            is_archivable="fake_is_archivable",
            virus_scan_status="fake_virus_scan_status",
            accepted="fake_accepted",
            date_modified="fake_date_modified",
            thumbnail="fake_thumbnail",
            creator_uuid="fake_creator_uuid",
            creator_displayname="fake_creator_displayname",
            type="fake_type",
            properties='{"displayname": "fake_displayname"}',
            preview_uuid=uuid4(),
            preview_storage_location=None,
            preview_mimetype=None,
            description="desc",
            origin="Intern",
            origin_date="2015-05-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_doc_1,
            namedtuple("directory_result", "id")(id=10),
        ]
        self.session.reset_mock()
        self.cmd.move_to_directory(
            destination_directory_uuid=desstination_directory_uuid,
            source_documents=source_documents,
        )

        call_list = self.session.execute.call_args_list

        update_statement = call_list[4][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE file SET directory_id=%(directory_id)s WHERE file.date_deleted IS NULL AND file.uuid = %(uuid_1)s::UUID"
        )

    def test_move_document_to_different_case(self):
        source_doc_uuid_1 = uuid4()
        desstination_directory_uuid = uuid4()
        source_documents = [source_doc_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=20,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_doc_1 = mock.Mock()
        source_doc_1.configure_mock(
            id=source_doc_uuid_1,
            document_uuid=source_doc_uuid_1,
            filename="fake_filename",
            extension="fake_extension",
            store_uuid="fake_store_uuid",
            directory_uuid=None,
            case_uuid="fake_case_uuid",
            case_display_number=case_id,
            mimetype="fake_mimetype",
            size=1,
            storage_location="fake_storage_location",
            md5="fake_md5",
            is_archivable="fake_is_archivable",
            virus_scan_status="fake_virus_scan_status",
            accepted="fake_accepted",
            date_modified="fake_date_modified",
            thumbnail="fake_thumbnail",
            creator_uuid="fake_creator_uuid",
            creator_displayname="fake_creator_displayname",
            type="fake_type",
            properties='{"displayname": "fake_displayname"}',
            preview_uuid=uuid4(),
            preview_storage_location=None,
            preview_mimetype=None,
            description="desc",
            origin="Intern",
            origin_date="2015-05-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=20),
            source_doc_1,
            namedtuple("directory_result", "id")(id=20),
        ]
        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_documents=source_documents,
            )
        assert excinfo.value.args == (
            f"The destination directories case_id does not match the given documents case_id ({destination_directory.case_id} != {source_doc_1.case_display_number})",
            "domains/document/move_to_directory/document_case_id_mismatch",
        )


class Test_update_a_document_from_editor(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        self.mock_repo_factory = mock.MagicMock()
        store_uuid = uuid4()
        self.mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository._get_filestore_by_uuid",
        return_value=mock.Mock(
            id=123,
            uuid=uuid4(),
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        ),
    )
    def test_document_from_editor(self, file_data):
        """ "
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = self.mock_filestore
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc) - timedelta(minutes=15),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().scalar_one.side_effect = [
            random.randint(500, 599),  # sync_metadata.file_metadata_id
            random.randint(1, 10),  # unaccepted docs (create_document)
            1,  # "number of documents with the same name"
            random.randint(1, 10),  # unaccepted docs (accept_document)
        ]
        self.session.execute().one.side_effect = [
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            editor_update=True,
        )

        set_active_version_qry = self.session.execute.call_args_list[9][0][0]
        set_active_version = set_active_version_qry.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(set_active_version)
            == "UPDATE file SET active_version=%(active_version)s WHERE file.id = %(id_1)s"
        )
        assert set_active_version.params == {
            "active_version": False,
            "id_1": 122,
        }

        insert_new_file_query = self.session.execute.call_args_list[13][0][0]
        insert_new_file = insert_new_file_query.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(insert_new_file)
            == "INSERT INTO file (filestore_id, uuid, root_file_id, name, extension, version, case_id, directory_id, creation_reason, accepted, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, destroyed, active_version, is_duplicate_of, queue, document_status, lock_timestamp, lock_subject_id, lock_subject_name, shared, confidential, search_term, skip_intake) VALUES (%(filestore_id)s, %(uuid)s::UUID, %(root_file_id)s, %(name)s, %(extension)s, %(version)s, %(case_id)s, %(directory_id)s, %(creation_reason)s, %(accepted)s, %(reject_to_queue)s, %(is_duplicate_name)s, %(publish_pip)s, %(publish_website)s, %(date_created)s, %(created_by)s, %(date_modified)s, %(modified_by)s, %(destroyed)s, %(active_version)s, %(is_duplicate_of)s, %(queue)s, %(document_status)s, %(lock_timestamp)s, %(lock_subject_id)s::UUID, %(lock_subject_name)s, %(shared)s, %(confidential)s, %(search_term)s, %(skip_intake)s) RETURNING file.id"
        )
        assert insert_new_file.params["shared"] == mock_old_active_file.shared
        assert (
            insert_new_file.params["lock_subject_id"]
            == mock_old_active_file.lock_subject_id
        )
        assert (
            insert_new_file.params["lock_subject_name"]
            == mock_old_active_file.lock_subject_name
        )
        assert (
            insert_new_file.params["lock_timestamp"]
            == mock_old_active_file.lock_timestamp
        )


class Test_update_a_document_from_editor_no_version_update(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        self.mock_repo_factory = mock.MagicMock()
        self.filestore_uuid = uuid4()
        self.filestore_id = random.randint(1000, 2000)

        self.mock_filestore = mock.Mock(
            name="filestore_from_setup",
            id=self.filestore_id,
            uuid=self.filestore_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
            date_created=datetime.now(tz=timezone.utc),
        )

    @freeze_time()
    def test_document_from_editor_no_version(self):
        """
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        file_id = random.randint(1, 100)

        mock_case = mock.Mock(
            name="case", id=123, status="open", behandelaar_gm_id=42
        )
        mock_old_active_file = mock.Mock(
            name="old_active_file",
            id=file_id,
            uuid=document_uuid,
            version=6,
            root_file_id=None,
            # One minute old, so "replace" should trigger
            date_created=datetime.now(tz=UTC) - timedelta(minutes=1),
        )

        mock_deactivate_returning = mock.Mock()
        mock_deactivate_returning.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(1, 10),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=None,
            publish_pip=True,
            publish_website=False,
            metadata_id=random.randint(300, 399),
        )

        self.session.execute().one_or_none.side_effect = [
            mock_old_active_file,
        ]

        self.session.execute().one.side_effect = [
            mock_deactivate_returning,
            (None, random.randint(1, 100)),
        ]
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock_old_active_file,  # _get_file_row_by_document_uuid
            self.mock_filestore,  # get_filestore_by_uuid
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            editor_update=True,
        )

        update_file_query = self.session.execute.call_args_list[5][0][0]
        compiled_update_file = update_file_query.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_update_file)
            == "UPDATE file SET filestore_id=%(filestore_id)s, date_modified=%(date_modified)s WHERE file.id = %(id_1)s"
        )
        assert compiled_update_file.params == {
            "filestore_id": self.mock_filestore.id,
            "date_modified": datetime.now(tz=UTC),
            "id_1": file_id,
        }

        delete_derivative_query = self.session.execute.call_args_list[6][0][0]
        compiled_delete_derivative = delete_derivative_query.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_delete_derivative)
            == "DELETE FROM file_derivative WHERE file_derivative.file_id = %(file_id_1)s"
        )
        assert compiled_delete_derivative.params == {
            "file_id_1": file_id,
        }

        # No new version, document is already accepted etc.


class Test_as_an_employee_I_want_to_accept_document(TestBase):
    """As an employee I want to accept a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = uuid4()

    def test_accept_document(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        previous_version_uuid = str(uuid4())

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        mock_deactivate_returning = mock.Mock()
        mock_deactivate_returning.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(1, 10),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=None,
            publish_pip=True,
            publish_website=False,
            metadata_id=random.randint(300, 399),
        )

        case_uuid = uuid4()
        num_unaccepted_files = random.randint(100, 199)

        self.session.execute().scalar_one.side_effect = [
            1,  # "number of documents with the same name"
            num_unaccepted_files,
        ]

        self.session.execute().one.side_effect = [
            mock_deactivate_returning,
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(
            document_uuid=document_uuid,
            as_version_of=previous_version_uuid,
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 8

        # Deactivate old version
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())

        assert str(update_query) == (
            "UPDATE file SET active_version=%(active_version)s WHERE file.uuid = %(uuid_1)s::UUID AND file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id = (SELECT file.case_id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_2)s::UUID \n"
            " LIMIT %(param_1)s) RETURNING file.root_file_id, file.id, file.version, file.lock_timestamp, file.lock_subject_id, file.lock_subject_name, file.shared, file.publish_pip, file.publish_website, file.metadata_id"
        )
        assert update_query.params == {
            "active_version": False,
            "param_1": 1,
            "uuid_1": str(previous_version_uuid),
            "uuid_2": str(document_uuid),
        }

        # Activate new version
        update_query = call_list[2][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET uuid=%(uuid)s::UUID, root_file_id=%(root_file_id)s, version=%(version)s, metadata_id=%(metadata_id)s, accepted=%(accepted)s, rejection_reason=%(rejection_reason)s, is_duplicate_name=%(is_duplicate_name)s, publish_pip=%(publish_pip)s, publish_website=%(publish_website)s, active_version=%(active_version)s, is_duplicate_of=%(is_duplicate_of)s, lock_timestamp=%(lock_timestamp)s, lock_subject_id=%(lock_subject_id)s::UUID, lock_subject_name=%(lock_subject_name)s, shared=%(shared)s WHERE file.accepted IS false AND file.uuid = %(uuid_1)s::UUID RETURNING file.id, file.case_id"
        )
        assert update_query.params == {
            "accepted": True,
            "active_version": True,
            "is_duplicate_name": False,
            "is_duplicate_of": None,
            "lock_subject_id": None,
            "lock_subject_name": None,
            "lock_timestamp": None,
            "metadata_id": mock_deactivate_returning.metadata_id,
            "publish_pip": True,
            "publish_website": False,
            "rejection_reason": None,
            "root_file_id": mock_deactivate_returning.root_file_id,
            "shared": None,
            "uuid": str(previous_version_uuid),
            "uuid_1": str(document_uuid),
            "version": mock_deactivate_returning.version + 1,
        }

        # 4 = SELECT count to check duplicate names
        count_query = call_list[4][0][0].compile(dialect=postgresql.dialect())
        assert str(count_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM file \n"
            "WHERE file.name = %(name_1)s AND file.extension = %(extension_1)s AND file.active_version IS true AND file.destroyed IS false AND file.date_deleted IS NULL AND file.case_id = %(case_id_1)s AND file.accepted IS true"
        )

        # 6 = SELECT count for "sync"
        count_query = call_list[6][0][0].compile(dialect=postgresql.dialect())
        assert str(count_query) == (
            "SELECT count(%(count_1)s) AS count \n"
            "FROM file JOIN zaak ON file.case_id = zaak.id \n"
            "WHERE file.accepted IS false AND file.destroyed IS false AND file.date_deleted IS NULL AND zaak.uuid = %(uuid_1)s::UUID"
        )

        update_query = call_list[7][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params == {
            "unaccepted_files_count": num_unaccepted_files,
            "uuid_1": case_uuid,
        }

    def test_accept_document_migration(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        # During the migration the document is accepted in the create call
        # Resulting in the previous being the same as the actual.
        previous_version_uuid = document_uuid

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        case_uuid = uuid4()
        num_unaccepted_files = random.randint(100, 199)

        self.session.execute().scalar_one.side_effect = [
            1,  # "number of documents with the same name"
            num_unaccepted_files,
        ]

        self.session.execute().one.side_effect = [
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(
            document_uuid=document_uuid,
            as_version_of=previous_version_uuid,
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 7

        # Deactivate old version
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())

        # Activate new version
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET accepted=%(accepted)s, rejection_reason=%(rejection_reason)s, is_duplicate_name=%(is_duplicate_name)s, active_version=%(active_version)s, is_duplicate_of=%(is_duplicate_of)s WHERE file.accepted IS false AND file.uuid = %(uuid_1)s::UUID RETURNING file.id, file.case_id"
        )
        assert update_query.params == {
            "accepted": True,
            "active_version": True,
            "is_duplicate_name": False,
            "is_duplicate_of": None,
            "rejection_reason": None,
            "uuid_1": str(document_uuid),
        }

        # 4 = SELECT count to check duplicate names
        count_query = call_list[3][0][0].compile(dialect=postgresql.dialect())
        assert str(count_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM file \n"
            "WHERE file.name = %(name_1)s AND file.extension = %(extension_1)s AND file.active_version IS true AND file.destroyed IS false AND file.date_deleted IS NULL AND file.case_id = %(case_id_1)s AND file.accepted IS true"
        )

        # 6 = SELECT count for "sync"
        count_query = call_list[5][0][0].compile(dialect=postgresql.dialect())
        assert str(count_query) == (
            "SELECT count(%(count_1)s) AS count \n"
            "FROM file JOIN zaak ON file.case_id = zaak.id \n"
            "WHERE file.accepted IS false AND file.destroyed IS false AND file.date_deleted IS NULL AND zaak.uuid = %(uuid_1)s::UUID"
        )

        update_query = call_list[6][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s::UUID"
        )
        assert update_query.params == {
            "unaccepted_files_count": num_unaccepted_files,
            "uuid_1": case_uuid,
        }

    def test_accept_document_no_previous(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        # During the migration the document is accepted in the create call
        # Resulting in the previous being the same as the actual.
        previous_version_uuid = str(uuid4())

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        case_uuid = uuid4()
        num_unaccepted_files = random.randint(100, 199)

        self.session.execute().scalar_one.side_effect = [
            1,  # "number of documents with the same name"
            num_unaccepted_files,
        ]

        self.session.execute().one.side_effect = [
            NoResultFound,
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict) as exception_info:
            self.cmd.accept_document(
                document_uuid=document_uuid,
                as_version_of=previous_version_uuid,
            )
        assert exception_info.value.args == (
            "Previous document version not found (in the same case, or not at all)",
            "document/accept/not_found",
        )

    def test_accept_document_accept_failed(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        previous_version_uuid = str(uuid4())

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        mock_deactivate_returning = mock.Mock()
        mock_deactivate_returning.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(1, 10),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=None,
            publish_pip=True,
            publish_website=False,
            metadata_id=random.randint(300, 399),
        )

        num_unaccepted_files = random.randint(100, 199)

        self.session.execute().scalar_one.side_effect = [
            1,  # "number of documents with the same name"
            num_unaccepted_files,
        ]

        self.session.execute().one.side_effect = [
            mock_deactivate_returning,
            (None, random.randint(1, 100)),
            NoResultFound,
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict) as exception_info:
            self.cmd.accept_document(
                document_uuid=document_uuid,
                as_version_of=previous_version_uuid,
            )
        assert exception_info.value.args == (
            "Could not accept document.",
            "document/accept_failed",
        )

    def test_accept_document_docs_with_same_name_failed(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        previous_version_uuid = str(uuid4())

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        mock_deactivate_returning = mock.Mock()
        mock_deactivate_returning.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(1, 10),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=None,
            publish_pip=True,
            publish_website=False,
            metadata_id=random.randint(300, 399),
        )

        case_uuid = uuid4()
        num_unaccepted_files = random.randint(100, 199)

        self.session.execute().scalar_one.side_effect = [
            2,  # "number of documents with the same name"
            num_unaccepted_files,
        ]

        self.session.execute().one.side_effect = [
            mock_deactivate_returning,
            (None, random.randint(1, 100)),
            (case_uuid, "open"),
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict) as exception_info:
            self.cmd.accept_document(
                document_uuid=document_uuid,
                as_version_of=previous_version_uuid,
            )
        assert exception_info.value.args == (
            "Cannot accept document: 2 document(s) with the same name already exists",
            "document/duplicate_name",
        )

    def test_reaccept_document(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.accept_document(document_uuid=document_uuid)

        assert excinfo.value.args == (
            "Cannot accept an already accepted document",
            "document/already_accepted",
        )

    def test_accept_document_without_case(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=None,
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )

        self.session.execute().fetchone.side_effect = [document]
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.accept_document(document_uuid=document_uuid)

        assert excinfo.value.args == (
            "Cannot accept a document that is not in a case",
            "document/not_in_case",
        )

    def test_accept_document_for_resolved_case(self):
        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        previous_version_uuid = str(uuid4())

        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
            document_status="original",
            pronom_format=None,
            appearance=None,
            structure=None,
        )
        mock_deactivate_returning = mock.Mock()
        mock_deactivate_returning.configure_mock(
            root_file_id=random.randint(100, 199),
            id=random.randint(200, 299),
            version=random.randint(1, 10),
            lock_timestamp=None,
            lock_subject_id=None,
            lock_subject_name=None,
            shared=None,
            publish_pip=True,
            publish_website=False,
            metadata_id=random.randint(300, 399),
        )

        case_uuid = uuid4()

        self.session.execute().one.side_effect = [
            mock_deactivate_returning,
            (None, random.randint(1, 100)),
            (case_uuid, "resolved"),
        ]
        self.session.execute().fetchone.side_effect = [
            document,
        ]
        self.session.reset_mock()

        with pytest.raises(expected_exception=Conflict) as excinfo:
            self.cmd.accept_document(
                document_uuid=document_uuid,
                as_version_of=previous_version_uuid,
            )

        assert excinfo.value.args == (
            "Cannot accept document in case with status 'resolved'",
            "document/cannot_accept_in_resolved_case",
        )
