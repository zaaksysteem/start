# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import minty.cqrs
import random
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import case_management


def make_contact(
    role: str,
    magic_string_prefix: str,
    uuid: UUID | None = None,
    email: str | None = None,
    notification_settings: dict[str, bool] | None = None,
):
    uuid = uuid or uuid4()
    return {
        "type": "medewerker",
        "uuid": uuid,
        "role": role,
        "magic_string_prefix": magic_string_prefix,
        "contact_details": {
            "id": random.randint(1, 100),
            "uuid": uuid,
            "subject_type": "employee",
            "properties": json.dumps({"mail": email}),
            "settings": json.dumps(
                {
                    "notifications": notification_settings
                    or {"new_document": True}
                }
            ),
            "username": "piet",
            "role_ids": [],
            "group_ids": [],
            "nobody": False,
            "system": False,
            "related_custom_object_id": None,
            "employee_address": [],
        },
    }


def make_contact_empty_settings(
    role: str,
    magic_string_prefix: str,
    uuid: UUID | None = None,
    email: str | None = None,
    notification_settings: dict[str, bool] | None = None,
):
    uuid = uuid or uuid4()
    return {
        "type": "medewerker",
        "uuid": uuid,
        "role": role,
        "magic_string_prefix": magic_string_prefix,
        "contact_details": {
            "id": random.randint(1, 100),
            "uuid": uuid,
            "subject_type": "employee",
            "properties": json.dumps({"mail": email}),
            "settings": json.dumps(None),
            "username": "piet",
            "role_ids": [],
            "group_ids": [],
            "nobody": False,
            "system": False,
            "related_custom_object_id": None,
            "employee_address": [],
        },
    }


class TestNotifyAssignee(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_notify_no_assignee(self, caplog, case_db_rows):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                )
            ],
        )

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert f"Case {case_uuid} has no assignee" in caplog.records[0].message

    def test_notify_assignee_no_email(self, caplog, case_db_rows):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email=None,
                ),
            ],
        )

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert (
            f"Case {case_uuid} assignee has no email: cannot notify"
            in caplog.records[0].message
        )

    def test_notify_assignee_no_settings(self, caplog, case_db_rows):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact_empty_settings(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email="behandelaar@example.com",
                ),
            ],
        )

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert (
            f"Case {case_uuid} assignee has no settings: cannot notify"
            in caplog.records[0].message
        )

    def test_notify_assignee_preference_off(self, caplog, case_db_rows):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email="behandelaar@example.com",
                    notification_settings={"new_document": False},
                ),
            ],
        )

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert (
            "User disabled email notifications." in caplog.records[0].message
        )

    def test_notify_assignee_self_action(self, caplog, case_db_rows):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=event_actor_uuid,
                    email="behandelaar@example.com",
                ),
            ],
        )

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert (
            f"Action on case {case_uuid} performed by assignee. Will not self-notify"
            in caplog.records[0].message
        )

    def test_notify_assignee_no_template_configured(
        self, caplog, case_db_rows
    ):
        assert self.cmd

        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email="behandelaar@example.com",
                ),
            ],
        )

        self.session.execute().one_or_none.side_effect = [None]

        with caplog.at_level(logging.DEBUG):
            self.cmd.notify_case_assignee(
                case_uuid=case_uuid,
                event_actor=event_actor_uuid,
                notification_type="new_document",
            )
        assert (
            "Template for 'new_document' not configured, not notifying"
            in caplog.records[0].message
        )

    def test_notify_assignee(self, caplog, case_db_rows):
        assert self.cmd

        case_id = random.randint(1, 1000)
        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_id=case_id,
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email="behandelaar@example.com",
                ),
            ],
        )

        mock_template = mock.Mock(
            uuid=uuid4(),
            label="Notification Template",
            subject="Subject [[zaaknummer]]",
            message="Message [[zaaknummer]]",
            sender="Sender Name",
            sender_address="sender@example.com",
        )
        self.session.execute().one_or_none.side_effect = [mock_template]
        self.session.execute.reset_mock()

        self.cmd.notify_case_assignee(
            case_uuid=case_uuid,
            event_actor=event_actor_uuid,
            notification_type="new_document",
        )

        events = self.get_events_by_name("AssigneeNotified")

        assert len(events) == 1
        event = events[0]

        assert event.changes == [
            {
                "key": "notification",
                "old_value": None,
                "new_value": {
                    "sender_name": "Sender Name",
                    "sender_address": "sender@example.com",
                    "recipient_address": "behandelaar@example.com",
                    # Check that template processing happened for subject and body:
                    "subject": f"Subject {case_id}",
                    "body": f"Message {case_id}",
                },
            }
        ]

        template_query = self.session.execute.call_args_list[13][0][0]
        compiled_query = template_query.compile(dialect=postgresql.dialect())

        assert str(compiled_query) == (
            "SELECT bibliotheek_notificaties.uuid, bibliotheek_notificaties.label, bibliotheek_notificaties.subject, bibliotheek_notificaties.message, coalesce(bibliotheek_notificaties.sender, %(coalesce_1)s) AS sender, coalesce(bibliotheek_notificaties.sender_address, (SELECT CAST(interface.interface_config AS JSONB) ->> %(param_1)s AS anon_1 \n"
            "FROM interface \n"
            "WHERE interface.module = %(module_1)s AND interface.date_deleted IS NULL AND interface.active IS true \n"
            " LIMIT %(param_2)s)) AS sender_address \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE CAST(bibliotheek_notificaties.id AS TEXT) = (SELECT config.value \n"
            "FROM config \n"
            "WHERE config.parameter = %(parameter_1)s)"
        )
        assert compiled_query.params == {
            "coalesce_1": "",
            "module_1": "emailconfiguration",
            "param_1": "api_user",
            "param_2": 1,
            "parameter_1": "new_document_notification_template_id",
        }

    def test_notify_assignee_case_assigned(self, caplog, case_db_rows):
        assert self.cmd

        case_id = random.randint(1, 1000)
        case_uuid = uuid4()
        event_actor_uuid = uuid4()

        # Set up database:
        self.session.execute().fetchone.side_effect = case_db_rows(
            case_id=case_id,
            case_uuid=case_uuid,
            contacts=[
                make_contact(
                    role="Aanvrager",
                    magic_string_prefix="aanvrager",
                    uuid=uuid4(),
                    email=None,
                ),
                make_contact(
                    role="Behandelaar",
                    magic_string_prefix="behandelaar",
                    uuid=uuid4(),
                    email="behandelaar@example.com",
                ),
            ],
        )

        mock_template = mock.Mock(
            uuid=uuid4(),
            label="Notification Template",
            subject="Subject [[zaaknummer]]",
            message="Message [[zaaknummer]]",
            sender="Sender Name",
            sender_address="sender@example.com",
        )
        self.session.execute().one_or_none.side_effect = [mock_template]
        self.session.execute.reset_mock()

        self.cmd.notify_case_assignee(
            case_uuid=case_uuid,
            event_actor=event_actor_uuid,
            notification_type="send_case_assignee_email",
        )
