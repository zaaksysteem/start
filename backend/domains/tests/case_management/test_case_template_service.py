# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from zsnl_domains.case_management.services.case_template import (
    CaseTemplateService,
)


class Test_CaseTemplateService(TestBase):
    def test_case_location(self, test_case):
        case = test_case()
        case_location = "Ellermanstraat 23, 1114AK Amsterdam-Duivendrecht"
        expected_results = {
            "[[zaaklocatie_ligplaats]]": case_location,
            "[[zaaklocatie_nummeraanduiding]]": case_location,
            "[[zaaklocatie_openbareruimte]]": case_location,
            "[[zaaklocatie_pand]]": case_location,
            "[[zaaklocatie_standplaats]]": case_location,
            "[[zaaklocatie_verblijfsobject]]": case_location,
            "[[zaaklocatie_woonplaats]]": case_location,
            "[[case.case_location.ligplaats]]": case_location,
            "[[case.case_location.nummeraanduiding]]": case_location,
            "[[case.case_location.openbareruimte]]": case_location,
            "[[case.case_location.pand]]": case_location,
            "[[case.case_location.standplaats]]": case_location,
            "[[case.case_location.verblijfsobject]]": case_location,
            "[[case.case_location.woonplaats]]": case_location,
        }
        for key, value in expected_results.items():
            self._assert_template_value(
                case=case,
                magic_string=key,
                expected_value=value,
            )

    def test_system_attributes(self, test_case):
        case = test_case()
        expected_results = {
            "[[case.number_relations]]": "",
            "[[relates_to]]": "",
            "[[case.department]]": "Backoffice",
            "[[afdeling]]": "Backoffice",
        }
        for key, value in expected_results.items():
            self._assert_template_value(
                case=case,
                magic_string=key,
                expected_value=value,
            )

    def test_registration_date(self, test_case):
        # error
        caseTemplateService = CaseTemplateService(user_name_retriever=None)
        case = test_case()
        case.case_type_version.case_summary = "Registered on '[[case.date_of_registration]]' or in isodate format '[[case.date_of_registration | isodate]]' or in human readable format '[[case.date_of_registration | date]]'"
        result = caseTemplateService.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert (
            result
            == "Registered on '21-12-2022' or in isodate format '2022-12-21' or in human readable format '21 december 2022'"
        )

    def _assert_template_value(self, case, magic_string, expected_value):
        caseTemplateService = CaseTemplateService(user_name_retriever=None)
        case.case_type_version.case_summary = magic_string
        result = caseTemplateService.render_value(
            case=case, template_attribute="case_summary"
        )
        assert result == expected_value

    def test_join_custom_field_multivalue(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()

        case.case_type_version.case_summary = (
            '[[ join(kenmerk_tekstveld_multivalue, " + ", " en ") ]]'
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == "text one + text two en text three"

    def test_join_custom_field_multivalue_with_one_value(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()
        case.custom_fields["kenmerk_tekstveld_multivalue"] = {
            "type": "text",
            "value": ["text one"],
        }

        case.case_type_version.case_summary = (
            '[[ join(kenmerk_tekstveld_multivalue, " + ", " en ") ]]'
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == "text one"

    def test_join_custom_field_multivalue_only_separator(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()

        case.case_type_version.case_summary = (
            '[[ join(kenmerk_tekstveld_multivalue, " + ") ]]'
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == "text one + text two + text three"

    def test_join_custom_field_multivalue_bad(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()

        case.case_type_version.case_summary = (
            '[[ join(kenmerk_tekstveld_multivalue_bad, " + ") ]]'
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == ""

    def test_custom_field_multivalue(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()

        case.case_type_version.case_summary = (
            "[[ kenmerk_tekstveld_multivalue ]]"
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == "text one, text two, text three"

    def test_custom_field_radiobutton(self, test_case):
        template_service = CaseTemplateService(user_name_retriever=None)

        case = test_case()

        case.case_type_version.case_summary = (
            "[[ join(kenmerk_enkelvoudigekeuze) ]]"
        )
        result = template_service.render_value(
            case=case,
            template_attribute="case_summary",
        )
        assert result == "option 1"
