# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import pytest
from collections import defaultdict, namedtuple
from datetime import date, datetime, timezone
from minty.cqrs.events import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, ValidationError
from sqlalchemy.dialects import postgresql
from unittest import mock
from unittest.mock import patch
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management import get_query_instance
from zsnl_domains.case_management.entities import _shared
from zsnl_domains.case_management.entities.case_type_version import (
    CaseTypePayment,
    CaseTypePaymentDict,
)
from zsnl_domains.case_management.repositories.case import CaseRepository


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context, event_service, read_only):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCaseDomainQueries:
    def setup_method(self):
        repo_factory = MockRepositoryFactory(infra_factory={})
        repo_factory.repositories["case"] = mock.MagicMock()
        repo_factory.repositories["case_basic"] = mock.MagicMock()
        repo_factory.repositories["case_type"] = mock.MagicMock()
        repo_factory.repositories["organization"] = mock.MagicMock()
        repo_factory.repositories["employee"] = mock.MagicMock()
        repo_factory.repositories["person"] = mock.MagicMock()
        repo_factory.repositories["subject_relation"] = mock.MagicMock()
        repo_factory.repositories["case_relation"] = mock.MagicMock()
        repo_factory.repositories["department"] = mock.MagicMock()
        repo_factory.repositories["country"] = mock.MagicMock()

        self.user_uuid = uuid4()
        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid, permissions={"admin": True}
        )
        self.querymock = get_query_instance(
            repository_factory=repo_factory,
            context=None,
            user_uuid=self.user_uuid,
        )
        self.querymock.user_info = self.user_info

        self.case_entity = mock.MagicMock()
        self.case_type_version_entity = mock.MagicMock()
        self.organization_entity = mock.MagicMock()
        self.employee_entity = mock.MagicMock()
        self.person_entity = mock.MagicMock()
        self.subject_relation_entity = mock.MagicMock()

        repo_factory.repositories[
            "case"
        ].find_case_by_uuid.return_value = self.case_entity
        repo_factory.repositories[
            "case_basic"
        ].find_case_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "case_type"
        ].find_case_type_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "organization"
        ].find_organization_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "employee"
        ].find_employee_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "person"
        ].find_person_by_uuid.return_value = mock.MagicMock()
        repo_factory.repositories[
            "case_relation"
        ].get_case_relations.return_value = None

    def test_get_case_type_version_by_uuid(self):
        version_uuid = uuid4()
        case_type_repo = self.querymock.repository_factory.repositories[
            "case_type"
        ]
        case_type_repo.find_case_type_version_by_uuid.return_value = (
            self.case_type_version_entity
        )

        res = self.querymock.get_case_type_version_by_uuid(
            version_uuid=str(version_uuid)
        )
        case_type_repo.find_case_type_version_by_uuid.assert_called_once_with(
            version_uuid=version_uuid
        )
        assert isinstance(res, dict)
        assert res == {"result": self.case_type_version_entity}

    def test_get_case_type_active_version(self):
        case_type_uuid = uuid4()
        case_type_repo = self.querymock.repository_factory.repositories[
            "case_type"
        ]
        case_type_repo.find_active_version_by_uuid.return_value = (
            self.case_type_version_entity
        )

        res = self.querymock.get_case_type_active_version(
            case_type_uuid=str(case_type_uuid)
        )
        case_type_repo.find_active_version_by_uuid.assert_called_once_with(
            case_type_uuid=case_type_uuid
        )
        assert isinstance(res, dict)
        assert res == {"result": self.case_type_version_entity}

    def test_get_subject_relations_for_case(self):
        case_uuid = uuid4()
        repo = self.querymock.repository_factory.repositories[
            "subject_relation"
        ]
        user_info = mock.MagicMock()

        user_info = minty.cqrs.UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": True},
        )

        repo.find_subject_relations_for_case.return_value = [
            self.subject_relation_entity
        ]
        self.querymock.user_info = user_info
        res = self.querymock.get_subject_relations_for_case(
            case_uuid=str(case_uuid)
        )
        repo.find_subject_relations_for_case.assert_called_once_with(
            case_uuid=case_uuid, user_info=user_info
        )
        assert res == {"result": [self.subject_relation_entity]}

    def test_get_case_relations(self):
        case_uuid = uuid4()
        repo = self.querymock.repository_factory.repositories["case_relation"]
        repo.get_case_relations.return_value = ["fake_value"]
        user_info = mock.MagicMock()

        user_info = minty.cqrs.UserInfo(
            user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            permissions={"admin": True},
        )
        self.querymock.user_info = user_info
        res = self.querymock.get_case_relations(case_uuid=case_uuid)
        repo.get_case_relations.assert_called_once_with(
            authorization=_shared.CaseAuthorizationLevel.search,
            case_uuid=case_uuid,
            user_info=user_info,
        )
        assert res == {"result": ["fake_value"]}

    def test_get_countries_list(self):
        country_repo = self.querymock.repository_factory.repositories[
            "country"
        ]

        country_repo.get_countries_list.return_value = [
            "countries_1",
            "countries_2",
        ]

        res = self.querymock.get_countries_list()
        country_repo.get_countries_list.assert_called_once()
        assert res == ["countries_1", "countries_2"]

    def test_get_countries_list_active(self):
        country_repo = self.querymock.repository_factory.repositories[
            "country"
        ]
        self.querymock.get_countries_list(active=True)
        country_repo.get_countries_list.assert_called_once_with(active=True)


class TestCaseManagementGeneral:
    def test_get_query_instance(self):
        repo = {}
        context = None
        qry = case_management.get_query_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
        )
        assert isinstance(qry, minty.cqrs.DomainQueryContainer)

    def test_get_command_instance(self):
        repo = {}
        context = None
        cmd = case_management.get_command_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
            event_service="event_service",
        )
        assert isinstance(cmd, minty.cqrs.DomainCommandContainer)


class Test_Case_Type_Result(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_get_case_available_result_types(self):
        case_uuid = str(uuid4())

        mock_db_row = [
            mock.Mock(),
            mock.Mock(),
        ]
        mock_db_row[0].configure_mock(
            uuid=uuid4(),
            name="naam1",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="4 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=4,
        )
        mock_db_row[1].configure_mock(
            uuid=uuid4(),
            name="naam2",
            result="aangegaan",
            trigger_archival=False,
            preservation_term_label="6 weken",
            preservation_term_unit="week",
            preservation_term_unit_amount=6,
        )

        self.session.execute().fetchall.return_value = mock_db_row
        self.session.reset_mock()

        result_types = self.qry.get_case_available_result_types(
            case_uuid=case_uuid
        )

        assert len(result_types.entities) == 2
        assert result_types.entities[0].name == "naam1"
        assert result_types.entities[0].result == "aangegaan"
        assert not result_types.entities[0].trigger_archival
        assert result_types.entities[0].preservation_term_label == "4 weken"
        assert result_types.entities[0].preservation_term_unit == "week"
        assert result_types.entities[0].preservation_term_unit_amount == 4

        assert result_types.entities[1].name == "naam2"
        assert not result_types.entities[1].trigger_archival
        assert result_types.entities[1].preservation_term_label == "6 weken"
        assert result_types.entities[1].preservation_term_unit == "week"
        assert result_types.entities[1].preservation_term_unit_amount == 6

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaaktype_resultaten_1.uuid AS uuid, zaaktype_resultaten_1.label AS name, zaaktype_resultaten_1.resultaat AS result, zaaktype_resultaten_1.trigger_archival AS trigger_archival, result_preservation_terms.label AS preservation_term_label, result_preservation_terms.unit AS preservation_term_unit, result_preservation_terms.unit_amount AS preservation_term_unit_amount \n"
            "FROM zaaktype_resultaten AS zaaktype_resultaten_1 JOIN zaak AS zaak_1 ON zaaktype_resultaten_1.zaaktype_node_id = zaak_1.zaaktype_node_id JOIN result_preservation_terms ON result_preservation_terms.code = zaaktype_resultaten_1.bewaartermijn \n"
            "WHERE zaak_1.uuid = %(uuid_1)s::UUID AND zaak_1.status != %(status_1)s AND zaak_1.deleted IS NULL AND zaak_1.status != %(status_2)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s::UUID) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s::UUID)))"
        )


class Test_Case_Allocation_Check_Result(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()

        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_case_allocation_check_query_allowed(self, _):
        check = self.qry.case_allocation_check(
            case_uuid=str(uuid4()), department_uuid=uuid4(), role_uuid=uuid4()
        )
        assert check.check_result is True
        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "SELECT true AS anon_1 \n"
            "FROM zaaktype_authorisation JOIN zaak ON zaaktype_authorisation.zaaktype_id = zaak.zaaktype_id \n"
            "WHERE zaak.uuid = :uuid_1 AND zaaktype_authorisation.role_id = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2) AND zaaktype_authorisation.ou_id IN (SELECT unnest(groups.path) AS id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_3) AND zaaktype_authorisation.confidential = zaak.confidential UNION SELECT true AS anon_1 \n"
            "FROM zaak JOIN zaak_authorisation ON zaak.id = zaak_authorisation.zaak_id \n"
            "WHERE zaak.uuid = :uuid_4 AND zaak_authorisation.entity_type = :entity_type_1 AND zaak_authorisation.entity_id IN (SELECT CAST(anon_3.id AS TEXT) || :param_1 || CAST((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_5) AS TEXT) AS anon_2 \n"
            "FROM (SELECT unnest(groups.path) AS id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_3) AS anon_3)"
        )

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_case_allocation_check_query_not_allowed(self, _):
        self.session.execute().fetchone.return_value = None

        check = self.qry.case_allocation_check(
            case_uuid=str(uuid4()), department_uuid=uuid4(), role_uuid=uuid4()
        )
        assert check.check_result is False

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "SELECT true AS anon_1 \n"
            "FROM zaaktype_authorisation JOIN zaak ON zaaktype_authorisation.zaaktype_id = zaak.zaaktype_id \n"
            "WHERE zaak.uuid = :uuid_1 AND zaaktype_authorisation.role_id = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2) AND zaaktype_authorisation.ou_id IN (SELECT unnest(groups.path) AS id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_3) AND zaaktype_authorisation.confidential = zaak.confidential UNION SELECT true AS anon_1 \n"
            "FROM zaak JOIN zaak_authorisation ON zaak.id = zaak_authorisation.zaak_id \n"
            "WHERE zaak.uuid = :uuid_4 AND zaak_authorisation.entity_type = :entity_type_1 AND zaak_authorisation.entity_id IN (SELECT CAST(anon_3.id AS TEXT) || :param_1 || CAST((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_5) AS TEXT) AS anon_2 \n"
            "FROM (SELECT unnest(groups.path) AS id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_3) AS anon_3)"
        )

    def test_case_allocation_check_wrong_data(self):
        self.session.execute().fetchone.return_value = None

        with pytest.raises(ValidationError):
            self.qry.case_allocation_check(
                department_uuid=uuid4(), role_uuid=uuid4()
            )

        with pytest.raises(ValidationError):
            self.qry.case_allocation_check(
                case_uuid=uuid4(),
                casetype_uuid=uuid4(),
                department_uuid=uuid4(),
                role_uuid=uuid4(),
            )

    def test_case_allocation_check_casetype(self):
        check = self.qry.case_allocation_check(
            casetype_uuid=uuid4(),
            department_uuid=uuid4(),
            role_uuid=uuid4(),
        )
        assert check.check_result is True
        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()
        assert str(query) == (
            "SELECT true AS anon_1 \n"
            "FROM zaaktype_authorisation JOIN zaaktype ON zaaktype_authorisation.zaaktype_id = zaaktype.id \n"
            "WHERE zaaktype.uuid = :uuid_1 AND zaaktype_authorisation.role_id = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = :uuid_2) AND zaaktype_authorisation.ou_id IN (SELECT unnest(groups.path) AS id \n"
            "FROM groups \n"
            "WHERE groups.uuid = :uuid_3)"
        )


class Test_Case_Delete_Check_Result(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()

        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, rules=None):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["name"] = "test_case_type "
        mock_case_type["id"] = "1"
        mock_case_type["description"] = "test case type"
        mock_case_type["identification"] = "Testid"
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = ""
        mock_case_type["case_public_summary"] = ""
        mock_case_type["catalog_folder"] = {"uuid": uuid4(), "name": "folder"}
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["is_eligible_for_case_creation"] = True
        mock_case_type["initiator_source"] = "internextern"
        mock_case_type["initiator_type"] = "aangaan"
        mock_case_type["created"] = datetime(2019, 1, 1)
        mock_case_type["deleted"] = None
        mock_case_type["last_modified"] = datetime(2019, 1, 1)
        mock_case_type["case_type_uuid"] = case_type_uuid
        mock_case_type["uuid"] = str(uuid4())
        mock_case_type["phases"] = [
            {
                "name": "Geregistreerd",
                "milestone": 1,
                "phase": "Registreren",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "custom_fields": [
                    {
                        "uuid": uuid4(),
                        "id": 123,
                        "name": "custom_field_1",
                        "field_magic_string": "test_subject",
                        "field_type": "text",
                        "is_hidden_field": False,
                        "is_multiple": False,
                    }
                ],
                "checklist_items": [],
                "rules": rules or [],
            },
            {
                "name": "Toetsen",
                "milestone": 2,
                "phase": "Toetsen",
                "allocation": {
                    "department": {
                        "uuid": "95523653-b46e-47eb-9c18-caf0ff242420",
                        "name": "Development",
                    },
                    "role": {
                        "uuid": "d2708267-88f7-443a-9653-5b9d87932e53",
                        "name": "Administrator",
                    },
                    "role_set": None,
                },
                "cases": [],
                "custom_fields": [],
                "documents": [],
                "emails": [],
                "subjects": [],
                "checklist_items": [],
                "rules": [],
            },
        ]
        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "1"},
            "lead_time_service": {"type": "kalenderdagen", "value": "1"},
        }

        mock_case_type["properties"] = {}
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True
        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }

        mock_case_type["process_description"] = "process_description"
        mock_case_type["initiator_source"] = "email"
        mock_case_type["initiator_type"] = "assignee"
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]
        mock_case_type["version"] = mock.MagicMock()
        mock_case_type["requestor"] = mock.MagicMock()
        mock_case_type["case_type_results"] = []
        mock_case_type["metadata"] = {
            "process_description": "",
            "legal_basis": "",
        }
        mock_case_type["settings"] = {
            "allow_reuse_casedata": True,
            "enable_webform": True,
            "enable_online_payment": True,
            "enable_manual_payment": None,
            "require_email_on_webform": False,
            "require_phonenumber_on_webform": False,
            "require_mobilenumber_on_webform": False,
            "disable_captcha_for_predefined_requestor": None,
            "show_confidentiality": True,
            "show_contact_info": True,
            "enable_subject_relations_on_form": True,
            "open_case_on_create": True,
            "enable_allocation_on_form": True,
            "disable_pip_for_requestor": False,
            "list_of_default_folders": [],
            "lock_registration_phase": None,
            "enable_queue_for_changes_from_other_subjects": None,
            "check_acl_on_allocation": None,
            "api_can_transition": None,
            "is_public": True,
            "text_confirmation_message_title": 'Uw"zaak"is"geregistreerd',
            "text_public_confirmation_message": 'Bedankt"voor"het"[[case.casetype.initiator_type]]"van"een"<strong>[[case.casetype.name]]</strong>."Uw"registratie"is"bij"ons"bekend"onder"<strong>zaaknummer"[[case.number]]</strong>."Wij"verzoeken"u"om"bij"verdere"communicatie"dit"zaaknummer"te"gebruiken."De"behandeling"van"deze"zaak"zal"spoedig"plaatsvinden.',
            "payment": CaseTypePayment(
                assignee=CaseTypePaymentDict(amount="500"),
                frontdesk=CaseTypePaymentDict(amount="200"),
                phone=CaseTypePaymentDict(amount="300"),
                mail=CaseTypePaymentDict(amount="400"),
                webform=CaseTypePaymentDict(amount="150"),
                post=CaseTypePaymentDict(amount="600"),
            ),
        }

        return mock_case_type

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.get_status_destruction_date_by_case_uuids"
    )
    def test_case_delete_check_query(
        self,
        mock_get_status_destruction_date_by_case_uuids,
        mock_find_case,
        test_case,
    ):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.qry.repository_factory
        )
        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)
        case_type_uuid = str(uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)

        case.destruction_date = "2020-01-01"
        case.status = "resolved"
        case.archival_state = "vernietigen"
        case.related_uuids = str(uuid4())
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type

        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case

        RelatedCase = namedtuple(
            "RelatedCase", ["uuid", "status", "destruction_date"]
        )

        mock_result_related_uuids = [
            RelatedCase(
                UUID(case.related_uuids),
                "new",
                datetime(3020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
            RelatedCase(
                UUID(case.related_uuids),
                "resolved",
                datetime(3020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        mock_get_status_destruction_date_by_case_uuids.return_value = (
            mock_result_related_uuids
        )
        self.session.reset_mock()
        check = self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert check.check_result is True
        assert (
            check.warnings
            == "Case has object relations\nStatus of related case not resolved\nDestruction date of related case has not passed."
        )

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.get_status_destruction_date_by_case_uuids"
    )
    def test_case_delete_check_query_child_can__not_delete(
        self,
        mock_get_status_destruction_date_by_case_uuids,
        mock_find_case,
        test_case,
    ):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.qry.repository_factory
        )
        case_type_uuid = str(uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)
        case.destruction_date = "2020-01-01"
        case.status = "resolved"
        case.archival_state = "vernietigen"
        case.related_uuids = str(uuid4())
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type
        case.child_uuids = str(uuid4())

        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case

        RelatedCase = namedtuple(
            "RelatedCase", ["uuid", "status", "destruction_date"]
        )

        mock_result_related_uuids = [
            RelatedCase(
                UUID(case.related_uuids),
                "new",
                datetime(3020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        mock_get_status_destruction_date_by_case_uuids.return_value = (
            mock_result_related_uuids
        )

        with pytest.raises(Conflict) as excinfo:
            self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert excinfo.value.args == (
            "Subcase not resolved or subcase destruction date has not passed.",
            "case/delete_case/subcase_not_allowed",
        )

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.get_status_destruction_date_by_case_uuids"
    )
    def test_case_delete_check_query_child_can_delete(
        self,
        mock_get_status_destruction_date_by_case_uuids,
        mock_find_case,
        test_case,
    ):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.qry.repository_factory
        )
        case_type_uuid = str(uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)
        case.destruction_date = "2020-01-01"
        case.status = "resolved"
        case.archival_state = "vernietigen"
        case.related_uuids = str(uuid4())
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type
        case.child_uuids = str(uuid4())

        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case

        RelatedCase = namedtuple(
            "RelatedCase", ["uuid", "status", "destruction_date"]
        )

        mock_result_related_uuids = [
            RelatedCase(
                UUID(case.related_uuids),
                "resolved",
                datetime(2020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        mock_get_status_destruction_date_by_case_uuids.return_value = (
            mock_result_related_uuids
        )

        check = self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert check.check_result is True

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.get_status_destruction_date_by_case_uuids"
    )
    def test_case_delete_check_query_parent(
        self,
        mock_get_status_destruction_date_by_case_uuids,
        mock_find_case,
        test_case,
    ):
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.qry.repository_factory
        )
        case_type_uuid = str(uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)
        case.destruction_date = "2020-01-01"
        case.status = "resolved"
        case.archival_state = "vernietigen"
        case.related_uuids = str(uuid4())
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type
        case.parent_uuid = None

        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case

        mock_get_status_destruction_date_by_case_uuids.return_value = []

        check = self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert check.check_result is True

        case.parent_uuid = str(uuid4())
        RelatedCase = namedtuple(
            "RelatedCase", ["uuid", "status", "destruction_date"]
        )

        mock_result_related_uuids = [
            RelatedCase(
                case.parent_uuid,
                "new",
                datetime(2020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        mock_get_status_destruction_date_by_case_uuids.return_value = (
            mock_result_related_uuids
        )

        with pytest.raises(Conflict) as excinfo:
            self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert excinfo.value.args == (
            "Parent case not resolved. Case can not be deleted.",
            "case/delete_case/parent_case_not_resolved",
        )

        mock_result_related_uuids = [
            RelatedCase(
                case.parent_uuid,
                "resolved",
                datetime(3020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        mock_get_status_destruction_date_by_case_uuids.return_value = (
            mock_result_related_uuids
        )

        check = self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert (
            "Destruction date of main case has not passed." in check.warnings
        )

    @patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_case_delete_check_query_get_status_destruction_date_by_case_uuids(
        self,
        mock_find_case,
        test_case,
    ):
        with mock.patch("sqlalchemy.orm.Session"):
            self.mock_infra = mock.MagicMock()
            self.mock_infra_ro = mock.MagicMock()
            event_service = EventService(
                correlation_id=uuid4(),
                domain="domain",
                context="context",
                user_uuid=uuid4(),
            )
            case_repo = CaseRepository(
                infrastructure_factory=self.mock_infra,
                infrastructure_factory_ro=self.mock_infra_ro,
                context=None,
                event_service=event_service,
                read_only=False,
            )
        case_uuid = str(uuid4())
        case = test_case(
            case_uuid=case_uuid, event_service=self.qry.repository_factory
        )
        case_type_uuid = str(uuid4())
        mock_case_type = self.__mock_case_type(case_type_uuid)
        case.destruction_date = "2020-01-01"
        case.status = "resolved"
        case.archival_state = "vernietigen"
        case.related_uuids = str(uuid4())
        case.completion_date = date(2022, 3, 1)
        case.case_type_version = mock_case_type
        case.custom_fields = {
            "custom_fields": {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            }
        }

        mock_find_case.return_value = case

        case.parent_uuid = str(uuid4())
        RelatedCase = namedtuple(
            "RelatedCase", ["uuid", "status", "destruction_date"]
        )

        mock_result_related_uuids = [
            RelatedCase(
                case.parent_uuid,
                "new",
                datetime(2020, 1, 10, 9, 26, 33, tzinfo=timezone.utc),
            ),
        ]

        case_repo.session.execute().fetchall.return_value = (
            mock_result_related_uuids
        )

        with pytest.raises(Conflict) as excinfo:
            self.qry.delete_case_check(case_uuid=str(uuid4()))
        assert excinfo.value.args == (
            "Parent case not resolved. Case can not be deleted.",
            "case/delete_case/parent_case_not_resolved",
        )
