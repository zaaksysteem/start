# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import datetime
import json
import minty.exceptions
import pytest
import random
import sqlalchemy.exc
from minty.cqrs.test import TestBase
from minty.entity import EntityCollection, EntityResponse
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.admin import integrations
from zsnl_domains.admin.integrations.entities import (
    Integration,
    Transaction,
    TransactionData,
    TransactionRecord,
)
from zsnl_domains.admin.integrations.repositories.integration import (
    INTEGRATION_QUERY,
)
from zsnl_domains.admin.integrations.repositories.transaction import (
    COUNT_QUERY,
    TRANSACTION_QUERY,
    TransactionSortOrder,
)
from zsnl_domains.admin.integrations.repositories.transaction_data import (
    TRANSACTION_DATA_QUERY,
)
from zsnl_domains.admin.integrations.repositories.transaction_record import (
    RECORDS_QUERY,
)
from zsnl_domains.database import schema


def mock_transaction_row(
    transaction_uuid: UUID,
    integration_uuid: UUID,
    preview_data=None,
    transaction_id: int | None = None,
    transaction_date_created: datetime.datetime | None = None,
):
    preview_data = preview_data or dict()

    id: int = transaction_id if transaction_id else random.randint(1, 100)
    date_created: datetime.datetime = (
        transaction_date_created
        if transaction_date_created
        else datetime.datetime(
            year=2024,
            month=8,
            day=20,
            hour=10,
            minute=16,
            second=0,
            tzinfo=datetime.timezone.utc,
        )
    )

    row = mock.Mock(name="transaction")
    row.configure_mock(
        uuid=transaction_uuid,
        id=id,
        direction="outgoing",
        date_created=date_created,
        date_last_retry=datetime.datetime(
            year=2024,
            month=8,
            day=20,
            hour=10,
            minute=17,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
        date_next_retry=None,
        external_transaction_id="external_id",
        automated_retry_count=10,
        error_count=5,
        success_count=1,
        record_count=10,
        error_fatal=False,
        processed=True,
        error_message="Something broke",
        preview_data=preview_data,
        integration_uuid=integration_uuid,
    )
    return row


def mock_transaction_data_row(transaction_uuid: UUID, integration_uuid: UUID):
    row = mock.Mock(name="transaction")
    row.configure_mock(uuid=transaction_uuid, input_data="Input data here")
    return row


def mock_integration_row(integration_uuid: UUID):
    row = mock.Mock(id="transaction")
    row.configure_mock(
        uuid=integration_uuid,
        name="Some Integration",
        active=True,
        module="example",
        id=random.randint(1, 100),
    )
    return row


def mock_transaction_record_row(record_uuid: UUID, transaction_uuid: UUID):
    row = mock.Mock(id="transaction_record")
    row.configure_mock(
        uuid=record_uuid,
        input="what goes in",
        output="must come out",
        is_error=False,
        date_executed=datetime.datetime(
            year=2024,
            month=8,
            day=20,
            hour=10,
            minute=18,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
        preview_string="What goes in must come out",
        last_error="it didn't come out",
        transaction_uuid=transaction_uuid,
    )
    return row


class TestTransactionQueries(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

    def test_get_transaction(self):
        transaction_uuid = uuid4()
        integration_uuid = uuid4()

        transaction_row = mock_transaction_row(
            transaction_uuid, integration_uuid
        )
        integration_row = mock_integration_row(integration_uuid)

        self.session.execute().one.return_value = transaction_row
        self.session.execute().fetchall.return_value = [integration_row]
        self.session.execute.reset_mock()
        response = self.qry.get_transaction(transaction_uuid)

        assert isinstance(response, EntityResponse)

        transaction = response.entity
        assert isinstance(transaction, Transaction)

        assert transaction.uuid == transaction_uuid

        assert (
            response.included_entities and len(response.included_entities) == 1
        )
        integration = response.included_entities[0]

        assert isinstance(integration, Integration)
        assert integration.uuid == integration_uuid

        expected_transaction_query = TRANSACTION_QUERY.where(
            schema.Transaction.uuid == transaction_uuid
        )
        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transaction_list_previewdata(self):
        transaction_uuid = uuid4()
        integration_uuid = uuid4()

        transaction_row = mock_transaction_row(
            transaction_uuid,
            integration_uuid,
            preview_data=[{"preview_string": "Preview goes here"}],
        )
        integration_row = mock_integration_row(integration_uuid)

        self.session.execute().one.return_value = transaction_row
        self.session.execute().fetchall.return_value = [integration_row]
        self.session.execute.reset_mock()
        response = self.qry.get_transaction(transaction_uuid)

        assert isinstance(response, EntityResponse)

        transaction = response.entity
        assert isinstance(transaction, Transaction)

        assert transaction.uuid == transaction_uuid
        assert transaction.entity_meta_summary == "Preview goes here"

        assert (
            response.included_entities and len(response.included_entities) == 1
        )
        integration = response.included_entities[0]

        assert isinstance(integration, Integration)
        assert integration.uuid == integration_uuid

        expected_transaction_query = TRANSACTION_QUERY.where(
            schema.Transaction.uuid == transaction_uuid
        )
        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transaction_notfound(self):
        transaction_uuid = uuid4()
        self.session.execute().one.side_effect = sqlalchemy.exc.NoResultFound

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_transaction(transaction_uuid)

    def test_get_transaction_data(self):
        transaction_uuid = uuid4()
        integration_uuid = uuid4()

        transaction_row = mock_transaction_data_row(
            transaction_uuid, integration_uuid
        )

        self.session.execute().one.return_value = transaction_row
        self.session.execute.reset_mock()
        response = self.qry.get_transaction_data(transaction_uuid)

        assert isinstance(response, TransactionData)

        assert response.uuid == transaction_uuid
        assert response.input_data == transaction_row.input_data

        expected_query = TRANSACTION_DATA_QUERY.where(
            schema.Transaction.uuid == transaction_uuid
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_query.compile(dialect=postgresql.dialect()))

    def test_get_transaction_data_notfound(self):
        transaction_uuid = uuid4()
        self.session.execute().one.side_effect = sqlalchemy.exc.NoResultFound

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_transaction_data(transaction_uuid)


class TestMultipleTransactionsQueries(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

        self.transaction1_id: int = 1
        self.transaction1_uuid = uuid4()
        self.transaction1_date_created = datetime.datetime(
            year=2024,
            month=1,
            day=1,
            hour=10,
            minute=11,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.transaction2_id: int = 2
        self.transaction2_uuid = uuid4()
        self.transaction2_date_created = datetime.datetime(
            year=2024,
            month=2,
            day=2,
            hour=10,
            minute=12,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.transaction3_id: int = 3
        self.transaction3_uuid = uuid4()
        self.transaction3_date_created = datetime.datetime(
            year=2024,
            month=3,
            day=3,
            hour=10,
            minute=13,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.transaction4_id: int = 4
        self.transaction4_uuid = uuid4()
        self.transaction4_date_created = datetime.datetime(
            year=2024,
            month=4,
            day=4,
            hour=10,
            minute=14,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.transaction5_id: int = 5
        self.transaction5_uuid = uuid4()
        self.transaction5_date_created = datetime.datetime(
            year=2024,
            month=5,
            day=5,
            hour=10,
            minute=15,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.transaction6_id: int = 6
        self.transaction6_uuid = uuid4()
        self.transaction6_date_created = datetime.datetime(
            year=2024,
            month=6,
            day=6,
            hour=10,
            minute=16,
            second=0,
            tzinfo=datetime.timezone.utc,
        )

        self.integration_uuid = uuid4()
        self.result_count: int = random.randint(100, 1000)

        self.transaction_row_1 = mock_transaction_row(
            transaction_uuid=self.transaction1_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction1_id,
            transaction_date_created=self.transaction1_date_created,
        )
        self.transaction_row_2 = mock_transaction_row(
            transaction_uuid=self.transaction2_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction2_id,
            transaction_date_created=self.transaction2_date_created,
        )
        self.transaction_row_3 = mock_transaction_row(
            transaction_uuid=self.transaction3_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction3_id,
            transaction_date_created=self.transaction3_date_created,
        )
        self.transaction_row_4 = mock_transaction_row(
            transaction_uuid=self.transaction4_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction4_id,
            transaction_date_created=self.transaction4_date_created,
        )
        self.transaction_row_5 = mock_transaction_row(
            transaction_uuid=self.transaction5_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction5_id,
            transaction_date_created=self.transaction5_date_created,
        )
        self.transaction_row_6 = mock_transaction_row(
            transaction_uuid=self.transaction6_uuid,
            integration_uuid=self.integration_uuid,
            preview_data=None,
            transaction_id=self.transaction6_id,
            transaction_date_created=self.transaction6_date_created,
        )

        self.integration_row = mock_integration_row(self.integration_uuid)

        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_1,
                self.transaction_row_2,
                self.transaction_row_3,
                self.transaction_row_4,
                self.transaction_row_5,
                self.transaction_row_6,
            ],
            [self.integration_row],
        ]
        self.session.execute().scalar.side_effect = [self.result_count]

        self.session.reset_mock()

    def test_get_transactions_nofilter(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters=None,
            cursor=None,
        )

        assert isinstance(response, EntityCollection)
        transactions = response.entities

        assert len(transactions) == 6
        assert isinstance(transactions[0], Transaction)
        assert isinstance(transactions[1], Transaction)
        assert isinstance(transactions[2], Transaction)
        assert isinstance(transactions[3], Transaction)
        assert isinstance(transactions[4], Transaction)
        assert isinstance(transactions[5], Transaction)

        assert response.total_results == self.result_count

        assert response.included_entities
        assert len(response.included_entities) == 1
        assert isinstance(response.included_entities[0], Integration)
        assert response.included_entities[0].uuid == self.integration_uuid

        expected_transaction_query = (
            TRANSACTION_QUERY.order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_integration_filter(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={
                "relationship.integration.id": str(self.integration_uuid)
            },
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.interface_id.in_(
                    other=sql.select(schema.Interface.id).where(
                        schema.Interface.uuid.in_([self.integration_uuid])
                    )
                )
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.interface_id.in_(
                other=sql.select(schema.Interface.id).where(
                    schema.Interface.uuid.in_([self.integration_uuid])
                )
            )
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_integration_filter_as_list(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={
                "relationship.integration.id": [str(self.integration_uuid)],
            },
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.interface_id.in_(
                    other=sql.select(schema.Interface.id).where(
                        schema.Interface.uuid.in_([self.integration_uuid])
                    )
                )
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.interface_id.in_(
                other=sql.select(schema.Interface.id).where(
                    schema.Interface.uuid.in_([self.integration_uuid])
                )
            )
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_error_filter_only_errors(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={"has_errors": "only_errors"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(schema.Transaction.error_fatal.is_(True))
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.error_fatal.is_(True)
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_error_filter_has_errors(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={"has_errors": "has_errors"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(schema.Transaction.error_count > 0)
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.error_count > 0
        )
        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_error_filter_no_errors(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={"has_errors": "no_errors"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                sql.and_(
                    sql.or_(
                        schema.Transaction.error_count == 0,
                        schema.Transaction.error_count.is_(None),
                    ),
                    schema.Transaction.error_fatal.is_(False),
                )
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            sql.and_(
                sql.or_(
                    schema.Transaction.error_count == 0,
                    schema.Transaction.error_count.is_(None),
                ),
                schema.Transaction.error_fatal.is_(False),
            )
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_keyword_filter(self):
        response = self.qry.get_transactions(
            page=2,
            page_size=10,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1

        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.text_vector.match("sleutel")
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )
        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

    def test_get_transactions_empty(self):
        # Overwrite default configuration
        integration_row = mock_integration_row(self.integration_uuid)
        self.session.execute().fetchall.side_effect = [
            [None],
            [integration_row],
        ]

        response = self.qry.get_transactions(
            page=2,
            page_size=4,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=None,
        )

        assert response.entities == [None]
        assert len(response.included_entities) == 0
        assert response.cursor is None

        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.text_vector.match("sleutel")
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(10)
            .offset(10)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

    def test_get_transactions_no_cursor_order_by_created_asc(self):
        # number of records to be retrieved
        page_size: int = 10

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current is None
        # - next last item points to last item (item 6)
        # - previous last item points to first item (item 1)
        expected_cursor_current_encoded: None = None
        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction6_id,
            "last_item": str(self.transaction6_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction1_id,
            "last_item": str(self.transaction1_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") is expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        assert response.entities[0].id == self.transaction1_id
        assert response.entities[1].id == self.transaction2_id
        assert response.entities[2].id == self.transaction3_id
        assert response.entities[3].id == self.transaction4_id
        assert response.entities[4].id == self.transaction5_id
        assert response.entities[5].id == self.transaction6_id

        # because we do not have a current cursor, the expected query is
        # with limit and offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.text_vector.match("sleutel")
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(page_size)
            .offset(0)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        transaction_query = execute_calls[0][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[1][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_asc(self):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the first result set (items 1 and 2).
        # So the query should return items 3 and 4
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_3,
                self.transaction_row_4,
            ],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 2.
        expected_last_item_identifier: int = self.transaction2_id
        expected_last_item: datetime.datetime = self.transaction2_date_created

        cursor_current: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }
        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == page_size
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current unchanged
        # - next last item points to last item (item 4)
        # - previous last item points to first item (item 3)
        expected_cursor_current_encoded: str = cursor_current_encoded

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction4_id,
            "last_item": str(self.transaction4_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction3_id,
            "last_item": str(self.transaction3_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") == expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                sql.and_(
                    schema.Transaction.text_vector.match("sleutel"),
                    sql.or_(
                        sql.and_(
                            schema.Transaction.date_created
                            == expected_last_item,
                            schema.Transaction.id
                            < expected_last_item_identifier,
                        ).self_group(),
                        schema.Transaction.date_created > expected_last_item,
                    ),
                )
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(page_size)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_asc_no_result(self):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the last result set (items 5 and 6).
        # So the query should return no items
        self.session.execute().fetchall.side_effect = [
            [None],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 6.
        expected_last_item_identifier: int = self.transaction6_id
        expected_last_item: datetime.datetime = self.transaction6_date_created

        # The expected date used in the query is date item 6.
        cursor_current: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }
        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=3,
            page_size=page_size,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == 1
        assert len(response.included_entities) == 0
        assert response.cursor is None

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                sql.and_(
                    schema.Transaction.text_vector.match("sleutel"),
                    sql.or_(
                        sql.and_(
                            schema.Transaction.date_created
                            == expected_last_item,
                            schema.Transaction.id
                            < expected_last_item_identifier,
                        ).self_group(),
                        schema.Transaction.date_created > expected_last_item,
                    ),
                )
            )
            .order_by(
                sql.asc(schema.Transaction.date_created),
                sql.asc(schema.Transaction.id),
            )
            .limit(page_size)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_asc_previous(self):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the second result set (items 3 and 4).
        # So the query should return items 1 and 2
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_1,
                self.transaction_row_2,
            ],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 3.
        expected_last_item_identifier: int = self.transaction3_id
        expected_last_item: datetime.datetime = self.transaction3_date_created

        cursor_current: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }

        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_asc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == page_size
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current unchanged
        # - next last item points to last item (item 2)
        # - previous last item points to first item (item 1)
        expected_cursor_current_encoded: str = cursor_current_encoded

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction2_id,
            "last_item": str(self.transaction2_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction1_id,
            "last_item": str(self.transaction1_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") == expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = ()

        expected_transaction_query = sql.select(
            (
                TRANSACTION_QUERY.where(
                    sql.and_(
                        schema.Transaction.text_vector.match("sleutel"),
                        sql.or_(
                            sql.and_(
                                schema.Transaction.date_created
                                == expected_last_item,
                                schema.Transaction.id
                                > expected_last_item_identifier,
                            ).self_group(),
                            schema.Transaction.date_created
                            < expected_last_item,
                        ),
                    )
                )
                .order_by(
                    sql.desc(schema.Transaction.date_created),
                    sql.desc(schema.Transaction.id),
                )
                .limit(page_size)
            ).subquery()
        ).order_by(
            sql.asc("date_created"),
            sql.asc("id"),
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created < %(date_created_2)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created ASC, anon_1.id ASC"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created < %(date_created_2)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created ASC, anon_1.id ASC"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_no_cursor_order_by_created_desc(self):
        # number of records to be retrieved
        page_size: int = 10

        # Sort mock data descending
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_6,
                self.transaction_row_5,
                self.transaction_row_4,
                self.transaction_row_3,
                self.transaction_row_2,
                self.transaction_row_1,
            ],
            [self.integration_row],
        ]

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_desc,
            filters={"keyword": "sleutel"},
            cursor=None,
        )
        assert len(response.entities) == 6
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current is None
        # - next last item points to last item (item 1)
        # - previous last item points to first item (item 6)
        expected_cursor_current_encoded: None = None

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction1_id,
            "last_item": str(self.transaction1_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction6_id,
            "last_item": str(self.transaction6_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") is expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        assert response.entities[0].id == self.transaction6_id
        assert response.entities[1].id == self.transaction5_id
        assert response.entities[2].id == self.transaction4_id
        assert response.entities[3].id == self.transaction3_id
        assert response.entities[4].id == self.transaction2_id
        assert response.entities[5].id == self.transaction1_id

        # because we do not have a current cursor, the expected query is
        # with limit and offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.text_vector.match("sleutel")
            )
            .order_by(
                sql.desc(schema.Transaction.date_created),
                sql.desc(schema.Transaction.id),
            )
            .limit(page_size)
            .offset(0)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_no_cursor_order_by_created_desc_2(self):
        # number of records to be retrieved
        page_size: int = 2

        # Sort mock data descending
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_6,
                self.transaction_row_5,
            ],
            [self.integration_row],
        ]

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_desc,
            filters={"keyword": "sleutel"},
            cursor=None,
        )
        assert len(response.entities) == page_size
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current is None
        # - next last item points to last item (item 5)
        # - previous last item points to first item (item 6)
        expected_cursor_current_encoded: None = None

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction5_id,
            "last_item": str(self.transaction5_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction6_id,
            "last_item": str(self.transaction6_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") is expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        assert response.entities[0].id == self.transaction6_id
        assert response.entities[1].id == self.transaction5_id

        # because we do not have a current cursor, the expected query is
        # with limit and offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                schema.Transaction.text_vector.match("sleutel")
            )
            .order_by(
                sql.desc(schema.Transaction.date_created),
                sql.desc(schema.Transaction.id),
            )
            .limit(page_size)
            .offset(0)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "param_1": page_size,  # limit
            "param_2": 0,  # offset
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_desc(self):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the first result set (items 6 and 5).
        # So the query should return items 4 and 3
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_4,
                self.transaction_row_3,
            ],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 5.
        expected_last_item_identifier: int = self.transaction5_id
        expected_last_item: datetime.datetime = self.transaction5_date_created

        cursor_current: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }
        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_desc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == page_size
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current unchanged
        # - next last item points to last item (item 3)
        # - previous last item points to first item (item 4)
        expected_cursor_current_encoded: str = cursor_current_encoded

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction3_id,
            "last_item": str(self.transaction3_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction4_id,
            "last_item": str(self.transaction4_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") == expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = (
            TRANSACTION_QUERY.where(
                sql.and_(
                    schema.Transaction.text_vector.match("sleutel"),
                    sql.or_(
                        sql.and_(
                            schema.Transaction.date_created
                            == expected_last_item,
                            schema.Transaction.id
                            < expected_last_item_identifier,
                        ).self_group(),
                        schema.Transaction.date_created < expected_last_item,
                    ),
                )
            )
            .order_by(
                sql.desc(schema.Transaction.date_created),
                sql.desc(schema.Transaction.id),
            )
            .limit(page_size)
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created < %(date_created_2)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )
        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT transaction.uuid, transaction.id, transaction.direction, transaction.date_created, transaction.date_last_retry, transaction.date_next_retry, transaction.external_transaction_id, transaction.automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal, transaction.processed, transaction.error_message, transaction.preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id < %(id_1)s) OR transaction.date_created < %(date_created_2)s) ORDER BY transaction.date_created DESC, transaction.id DESC \n"
            " LIMIT %(param_1)s"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_desc_previous(self):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the second result set (items 4 and 3).
        # So the query should return items 6 and 5
        self.session.execute().fetchall.side_effect = [
            [
                self.transaction_row_6,
                self.transaction_row_5,
            ],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 4.
        expected_last_item_identifier: int = self.transaction4_id
        expected_last_item: datetime.datetime = self.transaction4_date_created
        cursor_current: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }
        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=1,
            page_size=page_size,
            sort=TransactionSortOrder.created_desc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == page_size
        assert len(response.included_entities) == 1
        assert len(response.cursor) == 3

        # cursor
        # - current unchanged
        # - next last item points to last item (item 5)
        # - previous last item points to first item (item 6)
        expected_cursor_current_encoded: str = cursor_current_encoded

        expected_cursor_next: dict = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": self.transaction5_id,
            "last_item": str(self.transaction5_date_created),
        }
        expected_cursor_next_str: str = json.dumps(expected_cursor_next)
        expected_cursor_next_encoded: str = base64.b64encode(
            expected_cursor_next_str.encode("utf-8")
        ).decode("utf-8")

        expected_cursor_previous: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": self.transaction6_id,
            "last_item": str(self.transaction6_date_created),
        }
        expected_cursor_previous_str: str = json.dumps(
            expected_cursor_previous
        )
        expected_cursor_previous_encoded: str = base64.b64encode(
            expected_cursor_previous_str.encode("utf-8")
        ).decode("utf-8")

        assert (
            response.cursor.get("current") == expected_cursor_current_encoded
        )
        assert response.cursor.get("next") == expected_cursor_next_encoded
        assert (
            response.cursor.get("previous") == expected_cursor_previous_encoded
        )

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = sql.select(
            (
                TRANSACTION_QUERY.where(
                    sql.and_(
                        schema.Transaction.text_vector.match("sleutel"),
                        sql.or_(
                            sql.and_(
                                schema.Transaction.date_created
                                == expected_last_item,
                                schema.Transaction.id
                                > expected_last_item_identifier,
                            ).self_group(),
                            schema.Transaction.date_created
                            > expected_last_item,
                        ),
                    )
                )
                .order_by(
                    sql.asc(schema.Transaction.date_created),
                    sql.asc(schema.Transaction.id),
                )
                .limit(page_size)
            ).subquery()
        ).order_by(
            sql.desc("date_created"),
            sql.desc("id"),
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        expected_integration_query = INTEGRATION_QUERY.where(
            schema.Interface.uuid.in_([self.integration_uuid])
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 4

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created DESC, anon_1.id DESC"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))
        assert str(
            execute_calls[3][0][0].compile(dialect=postgresql.dialect())
        ) == str(
            expected_integration_query.compile(dialect=postgresql.dialect())
        )

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created DESC, anon_1.id DESC"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }

    def test_get_transactions_cursor_order_by_created_desc_previous_no_result(
        self,
    ):
        # number of records to be retrieved
        page_size: int = 2

        # Set the current cursor to the first result set (items 6 and 5).
        # So the query should return no items
        self.session.execute().fetchall.side_effect = [
            [None],
            [self.integration_row],
        ]

        # The expected date used in the query is date_created item 6.
        expected_last_item_identifier: int = self.transaction6_id
        expected_last_item: datetime.datetime = self.transaction6_date_created
        cursor_current: dict = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": expected_last_item_identifier,
            "last_item": str(expected_last_item),
        }
        cursor_current_str: str = json.dumps(cursor_current)
        cursor_current_encoded: str = base64.b64encode(
            cursor_current_str.encode("utf-8")
        ).decode("utf-8")

        response = self.qry.get_transactions(
            page=3,
            page_size=page_size,
            sort=TransactionSortOrder.created_desc,
            filters={"keyword": "sleutel"},
            cursor=cursor_current_encoded,
        )
        assert len(response.entities) == 1
        assert len(response.included_entities) == 0
        assert response.cursor is None

        # because we do have a current cursor, the expected query is
        # with limit and without offset
        expected_transaction_query = sql.select(
            (
                TRANSACTION_QUERY.where(
                    sql.and_(
                        schema.Transaction.text_vector.match("sleutel"),
                        sql.or_(
                            sql.and_(
                                schema.Transaction.date_created
                                == expected_last_item,
                                schema.Transaction.id
                                > expected_last_item_identifier,
                            ).self_group(),
                            schema.Transaction.date_created
                            > expected_last_item,
                        ),
                    )
                )
                .order_by(
                    sql.asc(schema.Transaction.date_created),
                    sql.asc(schema.Transaction.id),
                )
                .limit(page_size)
            ).subquery()
        ).order_by(
            sql.desc("date_created"),
            sql.desc("id"),
        )

        expected_count_query = COUNT_QUERY.where(
            schema.Transaction.text_vector.match("sleutel")
        )

        execute_calls = self.session.execute.call_args_list
        assert len(execute_calls) == 3

        # Ignore call[0] because this is reordering mock data.
        transaction_query = execute_calls[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(transaction_query) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created DESC, anon_1.id DESC"
        )

        assert str(transaction_query) == str(
            expected_transaction_query.compile(dialect=postgresql.dialect())
        )

        assert transaction_query.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }
        assert str(
            execute_calls[2][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_count_query.compile(dialect=postgresql.dialect()))

        select_statement = execute_calls[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT anon_1.uuid, anon_1.id, anon_1.direction, anon_1.date_created, anon_1.date_last_retry, anon_1.date_next_retry, anon_1.external_transaction_id, anon_1.automated_retry_count, anon_1.error_count, anon_1.success_count, anon_1.error_fatal, anon_1.processed, anon_1.error_message, anon_1.preview_data, anon_1.integration_uuid, anon_1.record_count \nFROM (SELECT transaction.uuid AS uuid, transaction.id AS id, transaction.direction AS direction, transaction.date_created AS date_created, transaction.date_last_retry AS date_last_retry, transaction.date_next_retry AS date_next_retry, transaction.external_transaction_id AS external_transaction_id, transaction.automated_retry_count AS automated_retry_count, coalesce(transaction.error_count, %(coalesce_1)s) AS error_count, coalesce(transaction.success_count, %(coalesce_2)s) AS success_count, transaction.error_fatal AS error_fatal, transaction.processed AS processed, transaction.error_message AS error_message, transaction.preview_data AS preview_data, interface.uuid AS integration_uuid, (SELECT count(%(count_2)s) AS count_1 \n"
            "FROM transaction_record \n"
            "WHERE transaction_record.transaction_id = transaction.id) AS record_count \n"
            "FROM transaction JOIN interface ON transaction.interface_id = interface.id AND interface.date_deleted IS NULL \n"
            "WHERE transaction.date_deleted IS NULL AND transaction.text_vector @@ plainto_tsquery(%(text_vector_1)s) AND ((transaction.date_created = %(date_created_1)s AND transaction.id > %(id_1)s) OR transaction.date_created > %(date_created_2)s) ORDER BY transaction.date_created ASC, transaction.id ASC \n"
            " LIMIT %(param_1)s) AS anon_1 ORDER BY anon_1.date_created DESC, anon_1.id DESC"
        )
        assert compiled_select.params == {
            "coalesce_1": 0,
            "coalesce_2": 0,
            "count_2": "*",
            "date_created_1": str(expected_last_item),
            "date_created_2": str(expected_last_item),
            "id_1": expected_last_item_identifier,
            "param_1": page_size,  # limit
            "text_vector_1": "sleutel",
        }


class TestTransactionRecordQueries(TestBase):
    def setup_method(self):
        self.load_query_instance(integrations)

        self.record_uuid = uuid4()
        self.transaction_uuid = uuid4()

        transaction_record_row = mock_transaction_record_row(
            self.record_uuid, self.transaction_uuid
        )

        self.session.execute().fetchall.side_effect = [
            [transaction_record_row],
        ]
        self.session.reset_mock()

    def test_get_transaction_records(self):
        records = self.qry.get_transaction_records(self.transaction_uuid)

        assert len(records) == 1
        assert isinstance(records[0], TransactionRecord)
        assert records[0].uuid == self.record_uuid
        assert records[0].input == "what goes in"
        assert records[0].output == "must come out"

        expected_query = RECORDS_QUERY.where(
            schema.TransactionRecord.transaction_id
            == sql.select(schema.Transaction.id)
            .where(schema.Transaction.uuid == self.transaction_uuid)
            .scalar_subquery()
        ).order_by(sql.desc(schema.TransactionRecord.date_executed))
        execute_calls = self.session.execute.call_args_list

        assert str(
            execute_calls[0][0][0].compile(dialect=postgresql.dialect())
        ) == str(expected_query.compile(dialect=postgresql.dialect()))
