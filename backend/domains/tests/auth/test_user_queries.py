# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import re
from datetime import datetime
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import auth
from zsnl_domains.auth.entities import User
from zsnl_domains.auth.exceptions import (
    FindActiveInterfaceError,
    FindUserByNameError,
    InterfaceNotActiveWarning,
    NewUserNotFoundError,
    UserMarkedAsDeletedWarning,
    UserNotActiveWarning,
)


class Test_User_queries(TestBase):
    def setup_method(self) -> None:
        self.load_query_instance(domain=auth)

    def test_find_user_by_name(self) -> None:
        uuid: UUID = uuid4()

        subject_username: str = "subject_user_name"

        mock_user_row: mock.Mock = mock.Mock()
        mock_user_row.configure_mock(
            subject_id=1,
            uuid=uuid,
            name=subject_username,
            user_id="2",
            role_ids=[],
            active=True,
            date_deleted=None,
        )

        self.session.execute().fetchone.return_value = mock_user_row
        self.session.reset_mock()

        result = self.qry.find_user_by_name(name=subject_username)

        assert isinstance(result, User)

        user: User = result
        assert user.entity_type == "user"
        assert user.name == subject_username
        assert user.id == 1
        assert user.login_entity_id == "2"
        assert user.active
        assert user.date_deleted is None

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT user_entity.id AS user_id, subject.id AS subject_id, "
            "subject.uuid, subject.role_ids, subject.username AS name, "
            "user_entity.active, user_entity.date_deleted \n"
            "FROM subject JOIN user_entity ON "
            "user_entity.subject_id = subject.id \n"
            "WHERE subject.username ILIKE '"
            + subject_username
            + "' AND subject.subject_type = 'employee' "
            "AND user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' "
            "AND interface.active = 'true' "
            "AND interface.date_deleted IS NULL)"
        )

    def test_find_user_by_name_inactive_user(self) -> None:
        uuid: UUID = uuid4()

        subject_username: str = "subject_user_name"

        mock_user_row: mock.Mock = mock.Mock()
        mock_user_row.configure_mock(
            subject_id=1,
            uuid=uuid,
            name=subject_username,
            user_id="2",
            role_ids=[],
            active=False,
            date_deleted=None,
        )

        self.session.execute().fetchone.return_value = mock_user_row
        self.session.reset_mock()

        result = self.qry.find_user_by_name(name=subject_username)

        assert isinstance(result, User)

        user: User = result
        assert user.entity_type == "user"
        assert user.name == subject_username
        assert user.id == 1
        assert user.login_entity_id == "2"
        assert not user.active
        assert user.date_deleted is None

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT user_entity.id AS user_id, subject.id AS subject_id, "
            "subject.uuid, subject.role_ids, subject.username AS name, "
            "user_entity.active, user_entity.date_deleted \n"
            "FROM subject JOIN user_entity ON "
            "user_entity.subject_id = subject.id \n"
            "WHERE subject.username ILIKE '"
            + subject_username
            + "' AND subject.subject_type = 'employee' "
            "AND user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' "
            "AND interface.active = 'true' "
            "AND interface.date_deleted IS NULL)"
        )

    def test_find_user_by_name_deleted_user(self) -> None:
        uuid: UUID = uuid4()

        subject_username: str = "subject_user_name"

        mock_user_row: mock.Mock = mock.Mock()
        mock_user_row.configure_mock(
            subject_id=1,
            uuid=uuid,
            name=subject_username,
            user_id="2",
            role_ids=[],
            active=True,
            date_deleted=datetime.now(),
        )

        self.session.execute().fetchone.return_value = mock_user_row
        self.session.reset_mock()

        result = self.qry.find_user_by_name(name=subject_username)

        assert isinstance(result, User)

        user: User = result
        assert user.entity_type == "user"
        assert user.name == subject_username
        assert user.id == 1
        assert user.login_entity_id == "2"
        assert user.active
        assert user.date_deleted is not None

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT user_entity.id AS user_id, subject.id AS subject_id, "
            "subject.uuid, subject.role_ids, subject.username AS name, "
            "user_entity.active, user_entity.date_deleted \n"
            "FROM subject JOIN user_entity ON "
            "user_entity.subject_id = subject.id \n"
            "WHERE subject.username ILIKE '"
            + subject_username
            + "' AND subject.subject_type = 'employee' "
            "AND user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' "
            "AND interface.active = 'true' "
            "AND interface.date_deleted IS NULL)"
        )

    def test_find_user_by_name_inactive_deleted_user(self) -> None:
        uuid: UUID = uuid4()

        subject_username: str = "subject_user_name"

        mock_user_row: mock.Mock = mock.Mock()
        mock_user_row.configure_mock(
            subject_id=1,
            uuid=uuid,
            name=subject_username,
            user_id="2",
            role_ids=[],
            active=False,
            date_deleted=datetime.now(),
        )

        self.session.execute().fetchone.return_value = mock_user_row
        self.session.reset_mock()

        result = self.qry.find_user_by_name(name=subject_username)

        assert isinstance(result, User)

        user: User = result
        assert user.entity_type == "user"
        assert user.name == subject_username
        assert user.id == 1
        assert user.login_entity_id == "2"
        assert not user.active
        assert user.date_deleted is not None

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT user_entity.id AS user_id, subject.id AS subject_id, "
            "subject.uuid, subject.role_ids, subject.username AS name, "
            "user_entity.active, user_entity.date_deleted \n"
            "FROM subject JOIN user_entity ON "
            "user_entity.subject_id = subject.id \n"
            "WHERE subject.username ILIKE '"
            + subject_username
            + "' AND subject.subject_type = 'employee' "
            "AND user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' "
            "AND interface.active = 'true' "
            "AND interface.date_deleted IS NULL)"
        )

    def test_find_user_by_name_not_found(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.return_value = None
        self.session.reset_mock()

        result = self.qry.find_user_by_name(name=subject_username)

        assert isinstance(result, type(None))

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT user_entity.id AS user_id, subject.id AS subject_id, "
            "subject.uuid, subject.role_ids, subject.username AS name, "
            "user_entity.active, user_entity.date_deleted \n"
            "FROM subject JOIN user_entity ON "
            "user_entity.subject_id = subject.id \n"
            "WHERE subject.username ILIKE '"
            + subject_username
            + "' AND subject.subject_type = 'employee' "
            "AND user_entity.source_interface_id = (SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' "
            "AND interface.active = 'true' "
            "AND interface.date_deleted IS NULL)"
        )

    def test_find_user_by_name_find_user_inactive(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.side_effect = UserNotActiveWarning(
            subject_username
        )
        self.session.reset_mock()

        with pytest.raises(
            UserNotActiveWarning,
            match=f"User with name '{subject_username}' is not active.",
        ):
            self.qry.find_user_by_name(name=subject_username)

    def test_find_user_by_name_find_user_marked_as_deleted(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.side_effect = (
            UserMarkedAsDeletedWarning(subject_username)
        )
        self.session.reset_mock()

        with pytest.raises(
            UserMarkedAsDeletedWarning,
            match=f"User with name '{subject_username}' is marked as deleted.",
        ):
            self.qry.find_user_by_name(name=subject_username)

    def test_find_user_by_name_new_user_not_found(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.side_effect = NewUserNotFoundError(
            subject_username
        )
        self.session.reset_mock()

        with pytest.raises(
            NewUserNotFoundError,
            match=f"Cannot find new created user with name "
            f"'{subject_username}'.",
        ):
            self.qry.find_user_by_name(name=subject_username)

    def test_find_user_by_name_find_user_error_sqlalchemy(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.side_effect = SQLAlchemyError()
        self.session.reset_mock()

        with pytest.raises(
            FindUserByNameError,
            match=f"Find user by name '{subject_username}' failed.",
        ):
            self.qry.find_user_by_name(name=subject_username)

    def test_find_user_by_name_find_user_error_dbapi(self) -> None:
        subject_username: str = "subject_user_name"

        self.session.execute().fetchone.side_effect = DBAPIError("", "", "")
        self.session.reset_mock()

        with pytest.raises(
            FindUserByNameError,
            match=f"Find user by name '{subject_username}' failed.",
        ):
            self.qry.find_user_by_name(name=subject_username)

    def test_find_active_interface(self) -> None:
        mock_interface_row: mock.Mock = mock.Mock()
        self.session.execute().fetchone.return_value = mock_interface_row
        self.session.reset_mock()

        self.qry.find_active_interface()

        (args, kwargs) = self.session.execute.call_args_list[0]
        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"literal_binds": True},
        )

        assert str(query) == (
            "SELECT interface.id \n"
            "FROM interface \n"
            "WHERE interface.module = 'authldap' AND interface.active = 'true'"
            " AND interface.date_deleted IS NULL"
        )

    def test_find_active_interface_error_sqlalchemy(self) -> None:
        name: str = "authldap"
        self.session.execute().fetchone.side_effect = SQLAlchemyError()
        self.session.reset_mock()

        with pytest.raises(
            FindActiveInterfaceError,
            match=re.escape(
                f"Find active interface with name '{name}' failed."
            ),
        ):
            self.qry.find_active_interface()

    def test_find_active_interface_error_dbapi(self) -> None:
        name: str = "authldap"
        self.session.execute().fetchone.side_effect = DBAPIError("", "", "")
        self.session.reset_mock()

        with pytest.raises(
            FindActiveInterfaceError,
            match=re.escape(
                f"Find active interface with name '{name}' failed."
            ),
        ):
            self.qry.find_active_interface()

    def test_find_active_interface_warning_not_active(self) -> None:
        name: str = "authldap"
        self.session.execute().fetchone.return_value = None
        self.session.reset_mock()

        with pytest.raises(
            InterfaceNotActiveWarning,
            match=re.escape(
                f"Interface with name '{name}' does not exist or is not"
                " active."
            ),
        ):
            self.qry.find_active_interface()
