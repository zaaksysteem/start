# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from minty.entity import Entity
from zsnl_domains.auth.entities import TokenResponse


class Test_entity_token_response(TestBase):
    def setup_method(self) -> None:
        self.token_response: TokenResponse = TokenResponse(
            entity_type="token_response",
            id_token="the_id_token",
            access_token="the_access_token",
            scope="",
            expires_in=1000,
            token_type="bearer",
        )

    def test_create_token_response(self) -> None:
        new_id_token: str = "new_id_token"

        new_token_response: Entity = self.token_response.create(
            id_token=new_id_token,
            access_token="the_access_token",
            scope="",
            expires_in=1000,
            token_type="bearer",
        )

        assert isinstance(new_token_response, TokenResponse)
        assert new_token_response.entity_type == "token_response"
        assert new_token_response.id_token == new_id_token
