# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.cqrs.test import TestBase
from zsnl_domains import auth


class Test_Event_commands(TestBase):
    def setup_method(self):
        self.load_command_instance(auth)
        self.username: str = "username"
        self.interface_name: str = "authldap"

    def test_add_event_auth0_interface_not_active(self):
        self.cmd.add_event_auth0_interface_not_active(
            username=self.username, interface_name=self.interface_name
        )
        self.assert_has_event_name("Auth0InterfaceNotActive")

    def test_add_event_auth0_user_marked_as_deleted(self):
        self.cmd.add_event_auth0_user_marked_as_deleted(
            username=self.username, interface_name=self.interface_name
        )
        self.assert_has_event_name("Auth0UserMarkedAsDeleted")

    def test_add_event_auth0_user_not_active(self):
        self.cmd.add_event_auth0_user_not_active(
            username=self.username, interface_name=self.interface_name
        )
        self.assert_has_event_name("Auth0UserNotActive")
