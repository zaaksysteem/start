# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime, timezone
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from uuid import UUID, uuid4
from zsnl_domains import auth
from zsnl_domains.auth.exceptions import CreateSubjectLoginHistoryError


class Test_SubjectLoginHistory_commands(TestBase):
    def setup_method(self):
        self.load_command_instance(auth)
        self.subject_id: int = 1
        self.subject_username: str = "test user"
        self.subject_uuid: UUID = uuid4()
        self.date_attempt: datetime = datetime.now(timezone.utc)
        self.ip: str = "127.0.0.1"
        self.success: bool = True
        self.method: str = "AUTH0"

    def test_create_subject_login_history(self):
        self.cmd.create_subject_login_history(
            ip=self.ip,
            subject_username=self.subject_username,
            subject_id=self.subject_id,
            subject_uuid=self.subject_uuid,
            success=True,
        )
        self.assert_has_event_name("Auth0SubjectLoginHistoryCreated")

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        insert_statement = call_list[0][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO subject_login_history (ip, subject_id, subject_uuid, "
            "success, method, date_attempt) "
            "VALUES (%(ip)s, %(subject_id)s, %(subject_uuid)s::UUID, "
            "%(success)s, %(method)s, %(date_attempt)s) "
            "RETURNING subject_login_history.id"
        )

        assert len(compiled_insert.params) == 6
        assert compiled_insert.params["ip"] == self.ip
        assert compiled_insert.params["method"] == self.method
        assert compiled_insert.params["subject_id"] == self.subject_id
        assert compiled_insert.params["subject_uuid"] == str(self.subject_uuid)
        assert compiled_insert.params["success"]
        assert compiled_insert.params["date_attempt"] is not None

    def test_create_subject_login_history_unsuccessful(self):
        self.cmd.create_subject_login_history(
            ip=self.ip,
            subject_username=self.subject_username,
            subject_id=self.subject_id,
            subject_uuid=self.subject_uuid,
            success=False,
        )
        self.assert_has_event_name("Auth0SubjectLoginHistoryCreated")

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        insert_statement = call_list[0][0][0]
        compiled_insert = insert_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert) == (
            "INSERT INTO subject_login_history (ip, subject_id, subject_uuid, "
            "success, method, date_attempt) "
            "VALUES (%(ip)s, %(subject_id)s, %(subject_uuid)s::UUID, "
            "%(success)s, %(method)s, %(date_attempt)s) "
            "RETURNING subject_login_history.id"
        )

        assert len(compiled_insert.params) == 6
        assert compiled_insert.params["ip"] == self.ip
        assert compiled_insert.params["method"] == self.method
        assert compiled_insert.params["subject_id"] == self.subject_id
        assert compiled_insert.params["subject_uuid"] == str(self.subject_uuid)
        assert not compiled_insert.params["success"]
        assert compiled_insert.params["date_attempt"] is not None

    def test_create_subject_login_history_error_sqlalchemy(self):
        self.session.execute.side_effect = SQLAlchemyError()
        self.session.reset_mock()

        with pytest.raises(
            CreateSubjectLoginHistoryError,
            match="Create subject login history for user 'test user' failed.",
        ):
            self.cmd.create_subject_login_history(
                ip=self.ip,
                subject_username=self.subject_username,
                subject_id=self.subject_id,
                subject_uuid=self.subject_uuid,
                success=self.success,
            )

    def test_create_subject_login_history_error_dbapi(self):
        self.session.execute.side_effect = DBAPIError("", "", "")
        self.session.reset_mock()

        with pytest.raises(
            CreateSubjectLoginHistoryError,
            match="Create subject login history for user 'test user' failed.",
        ):
            self.cmd.create_subject_login_history(
                ip=self.ip,
                subject_username=self.subject_username,
                subject_id=self.subject_id,
                subject_uuid=self.subject_uuid,
                success=self.success,
            )
