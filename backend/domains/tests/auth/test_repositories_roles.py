# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import EventService
from unittest import mock
from uuid import uuid4
from zsnl_domains.auth.repositories import RoleRepository


class TestRoleRepository:
    def setup_method(self) -> None:
        self.mock_infra = mock.MagicMock()
        self.mock_infra_ro = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.object_type_repo = RoleRepository(
            infrastructure_factory=self.mock_infra,
            infrastructure_factory_ro=self.mock_infra_ro,
            context="devtest",
            event_service=event_service,
            read_only=False,
        )

    def test_get_permissions_for_roles_no_role_ids(self) -> None:
        res = self.object_type_repo.get_permissions_for_roles(role_ids=None)
        assert res is None
