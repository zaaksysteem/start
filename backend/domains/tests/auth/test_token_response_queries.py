# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import re
from minty.cqrs.test import TestBase
from requests import Response
from requests.exceptions import RequestException
from unittest import mock
from zsnl_domains import auth
from zsnl_domains.auth.entities import TokenResponse
from zsnl_domains.auth.exceptions import (
    GetTokenError,
    GetTokenResponseError,
    GetTokenResponseJsonError,
    GetTokenResponseStatusError,
    IdTokenNotFoundError,
    TokenTypeNotBearerError,
    TokenTypeNotSetError,
)


class Test_Token_response_queries(TestBase):
    def setup_method(self) -> None:
        self.load_query_instance(auth)

    def test_get_token(self) -> None:
        token_response = mock.MagicMock()
        token_response.configure_mock(
            id_token="id_token",
            access_token="access_token",
            scope="openid email",
            expires_in=86400,
            token_type="Bearer",
        )

        self.session.execute().fetchone.return_value = token_response

        with mock.patch("requests.post") as requests_post:
            requests_post().json.return_value = {
                "id_token": "id_token",
                "access_token": "access_token",
                "scope": "openid email",
                "expires_in": 86400,
                "token_type": "Bearer",
            }
            requests_post().status_code = 200
            response = self.qry.get_token(
                code="test_code",
                hostname="hostname",
                client_id="client_1234567",
                client_secret="client_secret",
                domain="domain",
            )

        assert isinstance(response, TokenResponse)
        assert response.access_token is not None
        assert response.id_token is not None

    def test_get_token_request_error(self) -> None:
        url: str = "https://domain/oauth/token"
        data: dict = {
            "grant_type": "authorization_code",
            "code": "test_code",
            "client_id": "client_1234567",
            "client_secret": "client_secret",
            "redirect_uri": "https://hostname/",
        }

        with mock.patch("requests.post") as requests_post:
            with pytest.raises(
                GetTokenError,
                match=re.escape(
                    f"Exchange an authorization code for a token failed. Post "
                    f"token url '{url}' with request parameters '{data!s}'."
                ),
            ):
                requests_post.side_effect = RequestException()
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_response_not_json(self) -> None:
        url: str = "https://domain/oauth/token"
        data: dict = {
            "grant_type": "authorization_code",
            "code": "test_code",
            "client_id": "client_1234567",
            "client_secret": "client_secret",
            "redirect_uri": "https://hostname/",
        }

        with mock.patch("requests.post") as requests_post:
            with pytest.raises(
                GetTokenResponseJsonError,
                match=re.escape(
                    f"Response body does not contain valid json content. Post "
                    f"token url '{url}' with request parameters '{data!s}'."
                ),
            ):
                mocked_response = Response()
                mocked_response.status_code = 200
                mocked_response.data = "<!doctype html>"
                requests_post.return_value = mocked_response
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_response_status_not_200(self) -> None:
        with mock.patch("requests.post") as requests_post:
            requests_post.json.return_value = {
                "id_token": "id_token",
                "access_token": "access_token",
                "scope": "openid email",
                "expires_in": 86400,
                "token_type": "invalid",
            }
            requests_post.status_code = 400

            url: str = "https://domain/oauth/token"
            data: dict = {
                "grant_type": "authorization_code",
                "code": "test_code",
                "client_id": "client_1234567",
                "client_secret": "client_secret",
                "redirect_uri": "https://hostname/",
            }

            with pytest.raises(
                GetTokenResponseStatusError,
                match=re.escape(
                    f"Response status code is not 200 (OK). Post token url "
                    f"'{url}' with request parameters '{data!s}'."
                ),
            ):
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_response_contains_error(self) -> None:
        payload: dict = {
            "error": "Unknown error",
            "description": "Error description",
        }

        with mock.patch("requests.post") as requests_post:
            requests_post().json.return_value = payload
            requests_post().status_code = 200

            url: str = "https://domain/oauth/token"
            data: dict = {
                "grant_type": "authorization_code",
                "code": "test_code",
                "client_id": "client_1234567",
                "client_secret": "client_secret",
                "redirect_uri": "https://hostname/",
            }

            with pytest.raises(
                GetTokenResponseError,
                match=re.escape(
                    f"Response body contains error '{payload}'. Post "
                    f"token url '{url}' with request parameters '{data!s}'."
                ),
            ):
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_token_type_no_set(self) -> None:
        with mock.patch("requests.post") as requests_post:
            payload: dict = {
                "id_token": "id_token",
                "access_token": "access_token",
                "scope": "openid email",
                "expires_in": 86400,
                "token_type": None,
            }
            requests_post().json.return_value = payload
            requests_post().status_code = 200

            url: str = "https://domain/oauth/token"
            data: dict = {
                "grant_type": "authorization_code",
                "code": "test_code",
                "client_id": "client_1234567",
                "client_secret": "client_secret",
                "redirect_uri": "https://hostname/",
            }

            with pytest.raises(
                TokenTypeNotSetError,
                match=re.escape(
                    f"Token type is not set '{payload!s}'. Post token url "
                    f"'{url}' with request parameters '{data!s}'."
                ),
            ):
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_token_type_no_bearer(self) -> None:
        with mock.patch("requests.post") as requests_post:
            payload: dict = {
                "id_token": "id_token",
                "access_token": "access_token",
                "scope": "openid email",
                "expires_in": 86400,
                "token_type": "invalid",
            }
            requests_post().json.return_value = payload
            requests_post().status_code = 200

            url: str = "https://domain/oauth/token"
            data: dict = {
                "grant_type": "authorization_code",
                "code": "test_code",
                "client_id": "client_1234567",
                "client_secret": "client_secret",
                "redirect_uri": "https://hostname/",
            }

            with pytest.raises(
                TokenTypeNotBearerError,
                match=re.escape(
                    f"Token type is not bearer '{payload!s}'. Post token url "
                    f"'{url}' with request parameters '{data!s}'."
                ),
            ):
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )

    def test_get_token_id_token_not_set(self) -> None:
        with mock.patch("requests.post") as requests_post:
            payload: dict = {
                "id_token": None,
                "access_token": "access_token",
                "scope": "openid email",
                "expires_in": 86400,
                "token_type": "Bearer",
            }
            requests_post().json.return_value = payload
            requests_post().status_code = 200

            url: str = "https://domain/oauth/token"
            data: dict = {
                "grant_type": "authorization_code",
                "code": "test_code",
                "client_id": "client_1234567",
                "client_secret": "client_secret",
                "redirect_uri": "https://hostname/",
            }

            with pytest.raises(
                IdTokenNotFoundError,
                match=re.escape(
                    f"No ID token found '{payload!s}'. Post token url "
                    f"'{url}' with request parameters '{data!s}'."
                ),
            ):
                self.qry.get_token(
                    code="test_code",
                    hostname="hostname",
                    client_id="client_1234567",
                    client_secret="client_secret",
                    domain="domain",
                )
