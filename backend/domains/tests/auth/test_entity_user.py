# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from minty.entity import Entity
from uuid import UUID, uuid4
from zsnl_domains.auth.entities import User


class Test_entity_user(TestBase):
    def setup_method(self) -> None:
        uuid: UUID = uuid4()

        self.user: User = User(
            entity_type="user",
            id=1,
            uuid=uuid,
            name="username",
            login_entity_id="id",
            role_ids=[1],
            active=True,
            date_deleted=None,
        )

    def test_create_user(self) -> None:
        new_user_name: str = "new_user_name"
        new_uuid: UUID = uuid4()

        new_user: Entity = self.user.create(
            id=2,
            uuid=new_uuid,
            name=new_user_name,
            login_entity_id="id2",
            role_ids=[2],
            active=True,
        )

        assert isinstance(new_user, User)
        assert new_user.entity_type == "user"
        assert new_user.id == 2
        assert new_user.uuid == new_uuid
        assert new_user.name == new_user_name
        assert new_user.login_entity_id == "id2"
        assert new_user.role_ids == [2]
        assert new_user.active
