# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from ... import ZaaksysteemRepositoryBase
from ...database import schema
from ..entities import EmailTemplate
from sqlalchemy import sql, types
from sqlalchemy.dialects import postgresql
from typing import Final, Literal

email_template_query = sql.select(
    schema.BibliotheekNotificatie.uuid,
    schema.BibliotheekNotificatie.label,
    schema.BibliotheekNotificatie.subject,
    schema.BibliotheekNotificatie.message,
    sql.func.coalesce(schema.BibliotheekNotificatie.sender, "").label(
        "sender"
    ),
    sql.func.coalesce(
        schema.BibliotheekNotificatie.sender_address,
        sql.select(
            sql.cast(schema.Interface.interface_config, postgresql.JSONB)[
                "api_user"
            ].astext
        )
        .where(
            sql.and_(
                schema.Interface.module == "emailconfiguration",
                schema.Interface.date_deleted.is_(None),
                schema.Interface.active.is_(True),
            )
        )
        .limit(1)
        .scalar_subquery(),
    ).label("sender_address"),
)

NOTIFICATION_TYPE_CONFIG_MAP: Final = {
    "new_document": "new_document_notification_template_id",
    "send_case_assignee_email": "allocation_notification_template_id",
}


class EmailTemplateRepository(ZaaksysteemRepositoryBase):
    def get_notification_template(
        self,
        notification_type: Literal["new_document", "send_case_assignee_email"],
    ) -> None | EmailTemplate:
        query = email_template_query.where(
            sql.cast(schema.BibliotheekNotificatie.id, types.Text)
            == sql.select(schema.Config.value)
            .where(
                schema.Config.parameter
                == NOTIFICATION_TYPE_CONFIG_MAP[notification_type]
            )
            .scalar_subquery()
        )
        row = self.session.execute(query).one_or_none()

        if not row:
            return

        return self._inflate_row_to_entity(row)

    def _inflate_row_to_entity(
        self,
        row,
    ):
        return EmailTemplate(
            entity_id=row.uuid,
            uuid=row.uuid,
            label=row.label,
            subject=row.subject,
            body=row.message,
            sender=row.sender,
            sender_address=row.sender_address,
        )
