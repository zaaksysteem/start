# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from ...shared.services.template import TemplateService
from ..entities import Case


class ArchiveTemplateService(TemplateService):
    def _prepare_entity(self, case: Case):
        attributes = case.entity_dict()

        template_variables = {
            field: self._format_custom_field_value(value)
            for (field, value) in attributes["custom_fields"].items()
        }
        template_variables["attributes"] = attributes
        return template_variables

    def render_value(self, case: Case, template_attribute: str):
        template_attribute = getattr(
            case.archive_settings, str(template_attribute)
        )

        if template_attribute is None:
            template_attribute = ""

        template_variables = self._prepare_entity(case)
        return self.render(str(template_attribute), template_variables)

    def _format_custom_field_value(self, value):
        if "value" in value and isinstance(value["value"], str):
            value["value"] = self._process_string(value["value"])

        return value

    def _format_date(self, date_value):
        if isinstance(date_value, (datetime.datetime)):
            date_value = date_value.date()
        val = date_value.strftime("%d-%m-%Y")
        return val

    def _process_string(self, template_value):
        try:
            date_value = datetime.datetime.strptime(template_value, "%Y-%m-%d")
            if isinstance(date_value, datetime.datetime):
                return self._format_date(date_value)
        except ValueError:
            return template_value
