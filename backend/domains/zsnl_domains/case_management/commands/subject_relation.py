# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .._shared import get_subject
from ..repositories import SubjectRelationRepository
from minty.exceptions import Conflict
from minty.validation import validate_with
from pkgutil import get_data
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import UUID


class CreateSubjectRelation(minty.cqrs.SplitCommandBase):
    name = "create_subject_relation"

    @validate_arguments
    def __call__(
        self,
        case_uuid: UUID,
        subject: dict,
        role: str,
        magic_string_prefix: str | None,
        authorized: bool | None,
        send_confirmation_email: bool | None,
        permission: str | None,
    ):
        """
        Create relationship between case and subject.

        :param case_uuid: UUID of the case.
        :param subject: subject dict
        :param role: Role of related subject.
        :param magic_string_prefix: Magic string prefix for the related subject. If not supplied, a magic_string will be generated
        :param authorized: Flag to set pip authorization for the subject.
        :param send_confirmation_email: Flag to send email confirmation to the subject.
        """
        subject_type = subject["type"]
        subject_uuid = subject["id"]

        subject_entity = get_subject(
            get_repository=self.get_repository,
            subject_type=subject_type,
            subject_uuid=subject_uuid,
        )

        subject_relation_repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )

        if not magic_string_prefix:
            subject_relations_for_case = (
                subject_relation_repo.find_subject_relations_for_case(
                    case_uuid=case_uuid,
                    user_info=self.cmd.user_info,
                )
            )
            magic_string_prefix = self._generate_unique_magic_string(
                magic_strings=[
                    rel.magic_string_prefix
                    for rel in subject_relations_for_case
                ],
                role=role,
            )

        case = subject_relation_repo.get_case_for_subject_relation(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )
        subject_relation_repo.create_subject_relation(
            case,
            subject_entity,
            role,
            magic_string_prefix,
            authorized,
            send_confirmation_email,
            permission,
            source_custom_field_type_id=None,
        )
        subject_relation_repo.save()

    def _generate_unique_magic_string(
        self, magic_strings: list[str], role: str
    ) -> str:
        role_low = role.lower()
        highest_num = 0 if role_low in magic_strings else -1
        for magic_string in magic_strings:
            if (
                magic_string
                and magic_string.startswith(role_low)
                and magic_string.removeprefix(role_low).isnumeric()
            ):
                highest_num = max(
                    int(magic_string.removeprefix(role_low)), highest_num
                )

        return (
            role_low + f"{highest_num + 1}" if highest_num > -1 else role_low
        )


class UpdateSubjectRelation(minty.cqrs.SplitCommandBase):
    name = "update_subject_relation"

    @validate_with(
        get_data(__name__, "validation/update_subject_relation.json")
    )
    def __call__(
        self,
        relation_uuid: UUID,
        role: str,
        magic_string_prefix: str,
        authorized: bool | None,
        permission: str | None,
    ):
        """
        Update subject relation by uuid.

        :param relation_uuid: UUID of the subject_relation
        :param role: role of the subject_relation to be updated
        :param magic_string_prefix: magic_string_prefix of the subject_relation to be updated
        :param authorized: pip_authorization of the subject_relation
        :param permission: permitted permission for the employee subject_relation
        """

        subject_relation_repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )
        subject_relation = subject_relation_repo.find_subject_relation_by_uuid(
            uuid=relation_uuid, user_info=self.cmd.user_info
        )

        subject_relation.update(
            role=role,
            magic_string_prefix=magic_string_prefix,
            authorized=authorized,
            permission=permission,
        )
        subject_relation_repo.save()


class DeleteSubjectRelation(minty.cqrs.SplitCommandBase):
    name = "delete_subject_relation"

    @validate_with(
        get_data(__name__, "validation/delete_subject_relation.json")
    )
    def __call__(self, relation_uuid: str):
        """
        Deletion subject relation by uuid.

        :param relation_uuid: UUID of the subject_relation
        """
        subject_relation_repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )
        subject_relation = subject_relation_repo.find_subject_relation_by_uuid(
            uuid=UUID(relation_uuid), user_info=self.cmd.user_info
        )

        if subject_relation.source_custom_field_type_id:
            raise Conflict(
                f"Cannot delete subject relation. It is in use in relation attribute '{subject_relation.source_custom_field_type_name}' and must be deleted from this attribute."
            )

        subject_relation.delete()
        subject_relation_repo.save()


class EnqueueSubjectRelationEmail(minty.cqrs.SplitCommandBase):
    name = "enqueue_subject_relation_email"

    def __call__(self, subject_relation_uuid: str):
        """
        Enqueue subject relation email notification.

        :param subject_relation_uuid: UUID of subject relation
        """
        # This code will disappear once the creation of emails
        # has been moved to Python [20191012]
        repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )

        subject_relation = repo.find_subject_relation_by_uuid(
            uuid=UUID(subject_relation_uuid), user_info=self.cmd.user_info
        )
        subject_relation.enqueue_email()
        repo.save()


class SendSubjectRelationEmail(minty.cqrs.SplitCommandBase):
    name = "send_subject_relation_email"

    def __call__(self, subject_relation_uuid: str):
        """Send email from enqueued subject relation email.

        :param subject_relation_uuid: UUID of subject relation
        """
        repo = cast(
            SubjectRelationRepository, self.get_repository("subject_relation")
        )

        subject_relation = repo.find_subject_relation_by_uuid(
            UUID(subject_relation_uuid), user_info=self.cmd.user_info
        )
        subject_relation.send_email()
        repo.save()
