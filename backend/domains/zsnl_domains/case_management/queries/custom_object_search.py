# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import minty.cqrs
from ...shared.types import (
    ComparisonFilterConditionStr,
    FilterMultipleValuesWithOperator,
)
from ..entities import custom_object as object_entities
from ..entities import custom_object_search_result as search_entities
from ..repositories import CustomObjectSearchResultRepository
from minty.entity import EntityCollection
from pydantic.v1 import BaseModel, Field, validate_arguments
from typing import cast
from uuid import UUID


class CustomObjectSearchResultFilter(BaseModel):
    custom_object_type_uuid: UUID = Field(
        ..., alias="relationship.custom_object_type.id"
    )

    filter_status: set[object_entities.ValidObjectStatus] | None = Field(
        None, alias="attributes.status"
    )

    filter_archive_status: None | (set[object_entities.ValidArchiveStatus]) = (
        Field(None, alias="attributes.archive_status")
    )

    filter_last_modified: list[ComparisonFilterConditionStr] | None = Field(
        None, alias="attributes.last_modified"
    )

    filter_keyword: FilterMultipleValuesWithOperator[str] | None = Field(
        None, alias="keyword"
    )

    filter_title: str | None = Field(None, alias="attributes.title")

    filter_subtitle: str | None = Field(None, alias="attributes.subtitle")

    filter_external_reference: str | None = Field(
        None, alias="attributes.external_reference"
    )

    filter_attributes_value: FilterMultipleValuesWithOperator | None = Field(
        None, alias="attributes.value"
    )

    operator: str | None = Field(None, alias="operator")

    class Config:
        validate_all = True


class CustomObjectSearchResult(minty.cqrs.SplitQueryBase):
    name = "search_custom_objects"

    @validate_arguments
    def __call__(
        self,
        filters: CustomObjectSearchResultFilter,
        page: int = 1,
        page_size: int = 25,
        cursor: str | None = None,
        sort: None
        | (
            search_entities.CustomObjectSearchOrder
        ) = search_entities.CustomObjectSearchOrder.title_asc,
    ) -> EntityCollection[search_entities.CustomObjectSearchResult]:
        """
        Get a list of filtered custom object search results.
        """

        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        # decode cursor
        decoded_cursor = None
        if cursor:
            decoded_bytes = base64.b64decode(cursor)
            decoded_json = decoded_bytes.decode("utf-8")
            decoded_cursor = json.loads(decoded_json)
            page_size = decoded_cursor.get("page_size")

        result = custom_object_search_repo.search(
            page=page,
            page_size=page_size,
            sort=sort,
            user_info=self.qry.user_info,
            filters=filters.dict(),
            cursor=decoded_cursor,
        )

        cursor = self._calculate_cursor(cursor, result, sort, page_size)

        result.cursor = cursor

        return result

    def _calculate_cursor(self, cursor, result, sort, page_size):
        if not result.entities:
            return None
        last_item_value = self._get_last_item_value(sort, result)
        cursor_next = {
            "page": "next",
            "page_size": page_size,
            "last_item_identifier": str(result.entities[-1].last_modified),
            "last_item": str(last_item_value.get("next")),
        }
        cursor_previous = {
            "page": "previous",
            "page_size": page_size,
            "last_item_identifier": str(result.entities[0].last_modified),
            "last_item": str(last_item_value.get("previous")),
        }

        # Encode to Base64

        json_string_next = json.dumps(cursor_next)
        json_string_previous = json.dumps(cursor_previous)

        base64_bytes_next = base64.b64encode(json_string_next.encode("utf-8"))
        base64_string_next = base64_bytes_next.decode("utf-8")
        base64_bytes_previous = base64.b64encode(
            json_string_previous.encode("utf-8")
        )
        base64_string_previous = base64_bytes_previous.decode("utf-8")
        cursor = {
            "next": base64_string_next,
            "previous": base64_string_previous,
            "current": cursor,
        }

        return cursor

    def _get_last_item_value(self, sort, result):
        sort = sort.replace("-", "")
        last_item_value_based_on_sort = {
            search_entities.CustomObjectSearchOrder.title_asc: {
                "next": result.entities[-1].title,
                "previous": result.entities[0].title,
            },
            search_entities.CustomObjectSearchOrder.subtitle_asc: {
                "next": result.entities[-1].subtitle,
                "previous": result.entities[0].subtitle,
            },
            search_entities.CustomObjectSearchOrder.external_reference_asc: {
                "next": result.entities[-1].external_reference,
                "previous": result.entities[0].external_reference,
            },
            search_entities.CustomObjectSearchOrder.date_created_asc: {
                "next": result.entities[-1].date_created,
                "previous": result.entities[0].date_created,
            },
            search_entities.CustomObjectSearchOrder.last_modified_asc: {
                "next": result.entities[-1].last_modified,
                "previous": result.entities[0].last_modified,
            },
        }
        return last_item_value_based_on_sort[sort]


class CustomObjectSearchTotalResults(minty.cqrs.SplitQueryBase):
    name = "search_custom_object_total_results"

    @validate_arguments
    def __call__(self, filters: CustomObjectSearchResultFilter):
        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        total_result = custom_object_search_repo.search_total_results_count(
            user_info=self.qry.user_info,
            filters=filters.dict(),
        )

        return total_result
