# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseTypeRepository
from typing import cast
from uuid import UUID


class GetVersionByUUID(minty.cqrs.SplitQueryBase):
    name = "get_case_type_version_by_uuid"

    def __call__(self, version_uuid: str) -> dict:
        """
        Get CaseTypeVersion entity as python dictionary by given `version_uuid`.

        :param version_uuid: identifier of casetype version
        :return: case_type dictionary
        """

        repo = cast(CaseTypeRepository, self.get_repository("case_type"))

        case_type = repo.find_case_type_version_by_uuid(
            version_uuid=UUID(version_uuid)
        )
        return {"result": case_type}


class GetActiveVersion(minty.cqrs.SplitQueryBase):
    name = "get_case_type_active_version"

    def __call__(self, case_type_uuid: str) -> dict:
        """
        Get latest CaseTypeVersion entity as python dictionary by given
        `case_type_uuid`.

        :param case_type_uuid: identifier of casetype version
        :return: case_type dictionary
        """

        repo = cast(CaseTypeRepository, self.get_repository("case_type"))
        case_type = repo.find_active_version_by_uuid(
            case_type_uuid=UUID(case_type_uuid)
        )
        return {"result": case_type}
