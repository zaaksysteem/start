# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from typing import Literal
from uuid import UUID


class RelatedObjectObjectType(Entity):
    """custom_object_type for the related_custom_object"""

    entity_type = "persistent_object_type"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID | None = Field(
        None, title="Internal identifier of custom_object_type"
    )


class RelatedObject(Entity):
    """Content of related_custom_object"""

    entity_type: Literal["related_object"] = "related_object"
    entity_relationships: list[str] = ["object_type"]
    entity_id__fields: list[str] = ["uuid"]

    # Properties
    uuid: UUID = Field(..., title="Internal identifier of the custom_object")

    # Relationship
    object_type: RelatedObjectObjectType | None = Field(
        ..., title="object_type of related_custom_object"
    )

    related_to_magic_strings: list[str] = Field(
        ...,
        title="Magic string of the custom field in this object this relation originates from",
    )
    related_from_magic_strings: list[str] = Field(
        ...,
        title="Magic string of the custom field in the other object that this relation originates from",
    )
    relation_source: list[UUID] = Field(
        ...,
        title="Source object(s) of this relation; used to do the unrelate call correctly",
    )
