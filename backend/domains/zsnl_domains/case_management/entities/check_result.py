# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class CheckResult(Entity):
    """
    Entity that represents the result of a boolean check.

    Example usage:
        * A query that checks whether the user can perform a certain action.
    """

    entity_type = "check_result"
    entity_id__fields: list[str] = ["uuid"]
    check_result: bool = Field(..., title="Check Result")
    warnings: str | None = Field(None, title="Warnings related to the check")
    uuid: UUID | None = Field(None, title="UUID of object")
