# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.object
from .. import entities
from ._shared import ContactType
from minty import cqrs
from minty.entity import EntityBase
from uuid import UUID


class TaskAssigneeInputData(minty.object.IntrospectableObject):
    type: ContactType
    id: UUID


class TaskAssigneeData(TaskAssigneeInputData):
    display_name: str


class Task(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        title: str | None,
        description: str | None,
        due_date: datetime.date | None,
        completed: bool | None,
        user_defined: bool | None,
        assignee: TaskAssigneeData | None,
        case: dict | None,
        case_type: dict | None,
        phase: int | None,
        department: dict | None,
        product_code: str | None,
        can_assign_externally: bool = False,
        dso_action_request: bool = False,
    ):
        self.uuid = uuid
        self.title = title
        self.description = description
        self.due_date = due_date
        self.completed = completed
        self.user_defined = user_defined
        self.case = case
        self.case_type = case_type
        self.assignee = assignee
        self.phase = phase
        self.department = department
        self.can_assign_externally = can_assign_externally
        self.product_code = product_code
        self.dso_action_request = dso_action_request

    @cqrs.event("TaskCreated", extra_fields=["case", "title"])
    def create(self, title: str, case: entities.Case, phase: int):
        case_dict = {
            "id": case.id,
            "uuid": case.uuid,
            "status": case.status,
            "milestone": case.milestone,
        }
        self.case = case_dict

        self.title = title
        self.phase = phase
        self.completed = False
        self.user_defined = True
        self.description = ""

    @property
    def is_editable(self) -> bool:
        """Check to see if task is editable.

        Will return 'true' if the case is resolved, if the current milestone of
        the case is higher than the task's phase or if the task is NOT
        user-defined.

        :return: True/False
        :rtype: bool
        """
        if self.case["status"] == "resolved":
            return False
        elif self.case["milestone"] > self.phase:
            return False
        else:
            return True

    @property
    def can_set_completion(self) -> bool:
        return self.is_editable

    @cqrs.event("TaskDeleted", extra_fields=["case", "title"])
    def delete(self):
        self.date_deleted = datetime.date.today()

    @cqrs.event("TaskUpdated", extra_fields=["case", "title"])
    def update(
        self,
        title: str | None,
        description: str | None,
        due_date: datetime.date | None,
        assignee: TaskAssigneeInputData | None,
        product_code: str | None,
        dso_action_request: bool,
    ):
        self.title = title
        self.description = description
        self.due_date = due_date
        self.assignee = assignee
        self.product_code = product_code
        self.dso_action_request = dso_action_request

    @cqrs.event("TaskCompletionSet", extra_fields=["case"])
    def set_completion(self, completed: bool):
        self.completed = completed
