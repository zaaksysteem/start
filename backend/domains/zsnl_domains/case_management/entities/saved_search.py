# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import minty.cqrs
from ...shared.custom_field import CustomFieldAttributeTypes
from ...shared.types import FilterOperator, ValidGender
from ._shared import AuthorizationLevel, ContactType
from datetime import date, datetime, timezone
from minty import entity
from pydantic.v1 import Field, confloat, conint, constr
from pydantic.v1.generics import GenericModel
from typing import Annotated, Generic, Literal, Optional, TypeVar
from uuid import UUID
from zsnl_domains.admin.catalog.entities.versioned_casetype import (
    DesignationOfConfidentialityType,
)
from zsnl_domains.case_management.entities.custom_object import (
    ValidArchiveStatus,
    ValidObjectStatus,
)
from zsnl_domains.case_management.entities.saved_search_label import (
    SavedSearchLabel,
)
from zsnl_domains.shared import types
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
    ValidTypeOfArchiving,
    ValidUnacceptedChangesTypes,
)
from zsnl_domains.shared.types import CocNumberValidation

ISO8601_PERIOD_REGEX = (
    r"^([?+-])?"
    r"P(?!\b)"
    r"([?0-9]+([,.][0-9]+)?Y)s?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?W)?"
    r"([?0-9]+([,.][0-9]+)?D)?"
    r"((T)([?0-9]+([,.][0-9]+)?H)?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?S)?)?$"
)

SimpleFilterType = TypeVar("SimpleFilterType")
MinZeroInteger = conint(ge=0)
MinZeroFloat = confloat(ge=0)
CustomISODuration = constr(regex=ISO8601_PERIOD_REGEX)


class SimpleFilterObject(GenericModel, Generic[SimpleFilterType]):
    label: str | None = Field(
        None, title="Human-readable preview value of the filter value"
    )
    value: SimpleFilterType = Field(..., title="Value to filter for")

    class Config:
        @staticmethod
        def schema_extra(schema):
            for prop in schema.get("properties", {}).values():
                if (
                    "type" in prop
                    and prop["type"] == "array"
                    and "items" in schema["properties"]["value"]["items"]
                ):
                    updated_array = {
                        "oneOf": schema["properties"]["value"]["items"][
                            "items"
                        ]
                    }
                    schema["properties"]["value"]["items"]["items"] = (
                        updated_array
                    )


class AbsoluteDate(entity.ValueObject):
    type: Literal["absolute"]
    value: datetime | None
    operator: types.ComparisonFilterOperator


class RelativeDate(entity.ValueObject):
    type: Literal["relative"]
    value: CustomISODuration  # type: ignore
    operator: types.ComparisonFilterOperator


class RangeDate(entity.ValueObject):
    type: Literal["range"]
    start_value: datetime
    end_value: datetime
    time_set_by_user: bool


class AttributeValueItem(entity.ValueObject):
    label: str | None
    value: str
    type: str | None
    id: str | None


class AttributeValue(entity.ValueObject):
    type: CustomFieldAttributeTypes | None
    operator: types.FilterOperator | None
    magic_string: str
    label: str | None
    values: list[AttributeValueItem]
    attributeUuid: UUID | None


class CaseTypeVersionValues(entity.ValueObject):
    casetype_name: str | None
    casetype_uuid: UUID | None
    casetype_version_number: int | None
    casetype_version_uuid: UUID
    casetype_version_date: datetime | None


class CustomObjectTypeFilter(entity.ValueObject):
    type: Literal["relationship.custom_object_type"]
    values: list[SimpleFilterObject[UUID]]
    options: dict | None


class CustomObjectStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    values: list[ValidObjectStatus]
    options: dict | None


class CustomObjectArchiveStatusFilter(entity.ValueObject):
    type: Literal["attributes.archive_status"]
    values: list[ValidArchiveStatus]
    options: dict | None


class ContactParameters(entity.ValueObject):
    label: str
    value: UUID
    type: ContactType


class CustomObjectLastModifiedFilter(entity.ValueObject):
    type: Literal["attributes.last_modified"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CustomObjectAttributeValueFilter(entity.ValueObject):
    type: Literal["attributes.value"]
    values: AttributeValue
    options: dict | None


class CaseTypeUuidFilter(entity.ValueObject):
    type: Literal["relationship.case_type.id"]
    values: list[SimpleFilterObject[UUID]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    values: list[ValidCaseStatus]
    operator: types.FilterOperator | None
    options: dict | None


class CaseAssigneeUuidsFilter(entity.ValueObject):
    type: Literal["relationship.assignee.id"]
    values: list[ContactParameters]
    operator: types.FilterOperator | None
    options: dict | None


class CaseCoordinatorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.coordinator.id"]
    values: list[ContactParameters]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.requestor.id"]
    values: list[ContactParameters]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRegistrationDateFilter(entity.ValueObject):
    type: Literal["attributes.registration_date"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseCompletionDateFilter(entity.ValueObject):
    type: Literal["attributes.completion_date"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseLastModifiedFilter(entity.ValueObject):
    type: Literal["attributes.last_modified"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ]
    ]
    options: dict | None


class CasePaymentStatusFilter(entity.ValueObject):
    type: Literal["attributes.payment_status"]
    values: list[ValidCasePaymentStatus]
    operator: types.FilterOperator | None
    options: dict | None


class CaseChannelOfContactFilter(entity.ValueObject):
    type: Literal["attributes.channel_of_contact"]
    values: list[ValidContactChannel]
    operator: types.FilterOperator | None
    options: dict | None


class CaseConfidentialityFilter(entity.ValueObject):
    type: Literal["attributes.confidentiality"]
    values: list[ValidCaseConfidentiality]
    operator: types.FilterOperator | None
    options: dict | None


class CaseArchivalStateFilter(entity.ValueObject):
    type: Literal["attributes.archival_state"]
    values: list[ValidCaseArchivalState]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRetentionPeriodSourceDateFilter(entity.ValueObject):
    type: Literal["attributes.retention_period_source_date"]
    values: list[ValidCaseRetentionPeriodSourceDate]
    operator: types.FilterOperator | None
    options: dict | None


class CaseResultFilter(entity.ValueObject):
    type: Literal["attributes.result"]
    values: list[ValidCaseResult]
    operator: types.FilterOperator | None
    options: dict | None


class CaseNumUnreadMessagesFilter(entity.ValueObject):
    type: Literal["attributes.num_unread_messages"]
    values: list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]  # type: ignore
    options: dict | None


class CaseNumUnacceptedFilesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_files"]
    values: list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]  # type: ignore
    options: dict | None


class CaseNumUnacceptedUpdatesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_updates"]
    values: list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]  # type: ignore
    options: dict | None


class CasePeriodOfPreservationActiveFilter(entity.ValueObject):
    type: Literal["attributes.period_of_preservation_active"]
    values: list[str]
    options: dict | None


class CaseSubjectFilter(entity.ValueObject):
    type: Literal["attributes.subject"]
    values: list[SimpleFilterObject[str]] | None
    operator: types.FilterOperator | None
    options: dict | None


class KeywordFilter(entity.ValueObject):
    type: Literal["keyword"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class TitleFilter(entity.ValueObject):
    type: Literal["attributes.title"]
    values: list[str] | None
    operator: types.FilterOperator | None
    options: dict | None


class SubTitleFilter(entity.ValueObject):
    type: Literal["attributes.subtitle"]
    values: list[str] | None
    operator: types.FilterOperator | None
    options: dict | None


class ExternalReferenceFilter(entity.ValueObject):
    type: Literal["attributes.external_reference"]
    values: list[str] | None
    operator: types.FilterOperator | None
    options: dict | None


class CaseUrgencyFilter(entity.ValueObject):
    type: Literal["attributes.urgency"]
    values: list[ValidCaseUrgency]
    operator: types.FilterOperator | None
    options: dict | None


class CaseDepartmentRoleFilter(entity.ValueObject):
    class DepartmentRoleValues(entity.ValueObject):
        department: SimpleFilterObject[UUID]
        role: SimpleFilterObject[UUID] | None

    type: Literal["attributes.department_role"]
    values: list[DepartmentRoleValues]
    operator: types.FilterOperator | None
    options: dict | None


class CaseAddressFilter(entity.ValueObject):
    class AddressFilterValues(entity.ValueObject):
        label: str | None
        value: str
        type: str
        id: str

    type: Literal["attributes.case_location"]
    operator: types.FilterOperator | None
    values: list[AddressFilterValues]
    options: dict | None


class HasUnacceptedChangesFilter(entity.ValueObject):
    type: Literal["attributes.has_unaccepted_changes"]
    values: list[ValidUnacceptedChangesTypes]
    operator: types.FilterOperator | None
    options: dict | None


class CaseStalledUntilDateFilter(entity.ValueObject):
    type: Literal["attributes.stalled_until"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseStalledSinceDateFilter(entity.ValueObject):
    type: Literal["attributes.stalled_since"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseParentNumberFilter(entity.ValueObject):
    type: Literal["attributes.parent_number"]
    values: list[SimpleFilterObject[int]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseTargetDateFilter(entity.ValueObject):
    type: Literal["attributes.target_date"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseDestructionDateFilter(entity.ValueObject):
    type: Literal["attributes.destruction_date"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseExternalReferenceFilter(entity.ValueObject):
    type: Literal["attributes.external_reference"]
    values: list[str] | None
    operator: types.FilterOperator | None
    options: dict | None


class CasePhaseFilter(entity.ValueObject):
    type: Literal["attributes.case_phase"]
    values: list[SimpleFilterObject[str]] | None
    operator: types.FilterOperator | None
    options: dict | None


class CaseNumberFilter(entity.ValueObject):
    type: Literal["attributes.case_number"]
    values: list[SimpleFilterObject[int]] | None
    operator: types.FilterOperator | None
    options: dict | None


class CaseNumberMasterFilter(entity.ValueObject):
    type: Literal["attributes.number_master"]
    values: list[int]
    operator: types.FilterOperator | None
    options: dict | None


class CasePriceFilter(entity.ValueObject):
    type: Literal["attributes.case_price"]
    values: list[tuple[types.ComparisonFilterOperator, MinZeroFloat]]  # type: ignore
    operator: types.FilterOperator | None
    options: dict | None


class CaseCustomNumberFilter(entity.ValueObject):
    type: Literal["attributes.custom_number"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseTypeIdentificationFilter(entity.ValueObject):
    type: Literal["relationship.case_type.identification"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseTypeConfidentialityFilter(entity.ValueObject):
    type: Literal["relationship.case_type.confidentiality"]
    values: list[DesignationOfConfidentialityType]
    operator: types.FilterOperator | None
    options: dict | None


class CaseTypeVersionFilter(entity.ValueObject):
    type: Literal["relationship.case_type.version"]
    values: list[CaseTypeVersionValues]
    operator: types.FilterOperator | None
    options: dict | None


class CaseCustomObjectUuidFilter(entity.ValueObject):
    type: Literal["relationship.custom_object.id"]
    values: list[SimpleFilterObject[UUID]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorZipcodeFilter(entity.ValueObject):
    type: Literal["relationship.requestor.zipcode"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCocNumberFilter(entity.ValueObject):
    type: Literal["relationship.requestor.coc_number"]
    values: list[SimpleFilterObject[CocNumberValidation]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorNobleTitleFilter(entity.ValueObject):
    type: Literal["relationship.requestor.noble_title"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCocLocationNumberFilter(entity.ValueObject):
    type: Literal["relationship.requestor.coc_location_number"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorTradeNameFilter(entity.ValueObject):
    type: Literal["relationship.requestor.trade_name"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorDateOfBirth(entity.ValueObject):
    type: Literal["relationship.requestor.date_of_birth"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseRequestorDateOfDeath(entity.ValueObject):
    type: Literal["relationship.requestor.date_of_death"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseRequestorDateOfMarriage(entity.ValueObject):
    type: Literal["relationship.requestor.date_of_marriage"]
    values: list[
        Annotated[
            (AbsoluteDate | RangeDate | RelativeDate),
            Field(discriminator="type"),
        ],
    ]
    options: dict | None


class CaseRequestorGenderFilter(entity.ValueObject):
    type: Literal["relationship.requestor.gender"]
    values: list[ValidGender]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorInvestigationFilter(entity.ValueObject):
    type: Literal["relationship.requestor.investigation"]
    values: bool
    options: dict | None


class CaseRequestorIsSecretFilter(entity.ValueObject):
    type: Literal["relationship.requestor.is_secret"]
    values: bool
    options: dict | None


class CaseRequestorDepartmentFilter(entity.ValueObject):
    type: Literal["relationship.requestor.department"]
    values: list[SimpleFilterObject[UUID]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCorrespondenceZipcodeFilter(entity.ValueObject):
    type: Literal["relationship.requestor.correspondence_zipcode"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCorrespondenceStreetFilter(entity.ValueObject):
    type: Literal["relationship.requestor.correspondence_street"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCorrespondenceCityFilter(entity.ValueObject):
    type: Literal["relationship.requestor.correspondence_city"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorCityFilter(entity.ValueObject):
    type: Literal["relationship.requestor.city"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class CaseRequestorStreetFilter(entity.ValueObject):
    type: Literal["relationship.requestor.street"]
    values: list[SimpleFilterObject[str]]
    operator: types.FilterOperator | None
    options: dict | None


class ValidSavedSearchTemplate(enum.StrEnum):
    standard = "standard"
    archive_export = "archive_export"


class CustomFieldAttributeItemValue(entity.ValueObject):
    type: str | None
    label: str | None
    id: str | None
    value: str


class CustomFieldAttributeValue(entity.ValueObject):
    type: CustomFieldAttributeTypes | None
    magic_string: str
    label: str | None
    values: (
        list[
            Annotated[
                (AbsoluteDate | RangeDate | RelativeDate),
                Field(discriminator="type"),
            ]
            | CustomFieldAttributeItemValue
            | tuple[types.ComparisonFilterOperator, MinZeroFloat]
        ]
        | None
    )
    attributeUuid: UUID | None
    operator: FilterOperator | None
    additional_data: dict | None


class CaseCustomFieldAttribute(entity.ValueObject):
    type: Literal["attributes.value"]
    values: CustomFieldAttributeValue
    options: dict | None


class CaseTypeOfArchivingFilter(entity.ValueObject):
    type: Literal["attributes.type_of_archiving"]
    values: list[ValidTypeOfArchiving]
    options: dict | None


class CaseIntakeFilter(entity.ValueObject):
    "Saved search filter: show case intake."

    type: Literal["intake"]
    values: bool


class CustomObjectSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["custom_object"]
    operator: FilterOperator | None
    filters: list[
        Annotated[
            (
                KeywordFilter
                | CustomObjectTypeFilter
                | CustomObjectStatusFilter
                | CustomObjectArchiveStatusFilter
                | CustomObjectLastModifiedFilter
                | TitleFilter
                | SubTitleFilter
                | ExternalReferenceFilter
                | CustomObjectAttributeValueFilter
                | None
            ),
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class CaseSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["case"]
    custom_fields_operator: Literal["and"] | Literal["or"] | None
    operator: FilterOperator | None
    filters: list[
        Annotated[
            (
                KeywordFilter
                | CaseTypeUuidFilter
                | CaseStatusFilter
                | CaseAssigneeUuidsFilter
                | CaseCoordinatorUuidsFilter
                | CaseRequestorUuidsFilter
                | CaseRegistrationDateFilter
                | CaseCompletionDateFilter
                | CaseLastModifiedFilter
                | CasePaymentStatusFilter
                | CaseChannelOfContactFilter
                | CaseConfidentialityFilter
                | CaseArchivalStateFilter
                | CaseRetentionPeriodSourceDateFilter
                | CaseResultFilter
                | CaseNumUnreadMessagesFilter
                | CaseNumUnacceptedFilesFilter
                | CaseNumUnacceptedUpdatesFilter
                | CasePeriodOfPreservationActiveFilter
                | CaseSubjectFilter
                | CaseUrgencyFilter
                | CaseDepartmentRoleFilter
                | CaseAddressFilter
                | HasUnacceptedChangesFilter
                | CaseStalledUntilDateFilter
                | CaseStalledSinceDateFilter
                | CaseParentNumberFilter
                | CaseTargetDateFilter
                | CaseDestructionDateFilter
                | CaseExternalReferenceFilter
                | CaseNumberFilter
                | CaseNumberMasterFilter
                | CasePhaseFilter
                | CasePriceFilter
                | CaseCustomNumberFilter
                | CaseTypeIdentificationFilter
                | CaseTypeConfidentialityFilter
                | CaseTypeVersionFilter
                | CaseRequestorZipcodeFilter
                | CaseRequestorCocNumberFilter
                | CaseRequestorNobleTitleFilter
                | CaseRequestorCocLocationNumberFilter
                | CaseRequestorTradeNameFilter
                | CaseRequestorInvestigationFilter
                | CaseRequestorIsSecretFilter
                | CaseRequestorDateOfBirth
                | CaseRequestorDateOfDeath
                | CaseRequestorDateOfMarriage
                | CaseRequestorGenderFilter
                | CaseRequestorDepartmentFilter
                | CaseRequestorCorrespondenceZipcodeFilter
                | CaseRequestorCorrespondenceStreetFilter
                | CaseRequestorCorrespondenceCityFilter
                | CaseRequestorCityFilter
                | CaseRequestorStreetFilter
                | CaseCustomFieldAttribute
                | CaseTypeOfArchivingFilter
                | CaseIntakeFilter
                | CaseCustomObjectUuidFilter
                | None
            ),
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchPermission(enum.StrEnum):
    """Permissions a user can give to a group/role on a saved search."""

    read = "read"
    write = "write"


class SortOrder(enum.StrEnum):
    asc = "asc"
    desc = "desc"


class SavedSearchKind(enum.StrEnum):
    case = "case"
    custom_object = "custom_object"


class SavedSearchPermissionDefinition(entity.ValueObject):
    group_id: UUID = Field(
        ..., title="Identifier of the group this permission applies to"
    )
    role_id: UUID = Field(
        ..., title="Identifier of the role this permission applies to"
    )
    permission: set[SavedSearchPermission] = Field(
        ...,
        title="Permissions the specified group + role has for this saved search",
    )
    sort_order: Optional[int] = Field(
        None, title="Order of saved search permissions"
    )


class SavedSearchColumnDefinition(entity.ValueObject):
    source: list[str] = Field(
        ...,
        title="Path to retrieve the field from the case or custom_object entity",
    )
    label: str = Field(..., title="Human-readable name for this column")
    type: str = Field(
        ...,
        title="Type of the field; can be used to determine how to show the 'raw' value",
    )
    magic_string: str | None = Field(
        None, title="Magic string for this column"
    )
    visible: bool = Field(True, title="Decide visibility of column")

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchOwner(entity.Entity):
    entity_type = "employee"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class SavedSearchUpdatedBy(entity.Entity):
    entity_type = "employee"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID | None = Field(None, title="Identifier of the last updater")
    summary: str | None = Field(None, title="Summary of last updater")


class SavedSearch(entity.Entity):
    """
    Represents a saved state of the "advanced search" screen.

    Users can create (and save) multiple search queries, for quick access
    while working with the system.
    """

    entity_type = "saved_search"
    entity_id__fields: list[str] = ["uuid"]
    entity_relationships: list[str] = ["owner"]
    entity_meta__fields: list[str] = [
        "entity_meta_summary",
        "entity_meta_authorizations",
    ]

    entity_meta_authorizations: set[AuthorizationLevel] | None = Field(
        None,
        title="The set of rights/authorizations the current user has for the saved search",
    )

    uuid: UUID = Field(..., title="Identifier for this saved search")
    name: str = Field(..., title="Unique name of this search")

    owner: SavedSearchOwner = Field(..., title="Owner of this saved search")

    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )

    filters: Annotated[
        (CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition),
        Field(discriminator="kind_type"),
    ]
    permissions: list[SavedSearchPermissionDefinition] = Field(
        ..., title="Group/role/permission"
    )

    columns: list[SavedSearchColumnDefinition] = Field(
        ..., title="Columns visible when using this search"
    )
    sort_column: Optional[str] = Field(
        None, title="Column to sort the result on, when using this search"
    )
    sort_order: Optional[SortOrder] = Field(
        SortOrder.desc,
        title="Order for sorting results (ascending or descending)",
    )
    date_deleted: date | None = Field(
        None, title="Date the saved search is deleted"
    )
    labels: list[SavedSearchLabel] = Field(
        ..., title="Labels for saved search"
    )

    date_created: datetime | None = Field(
        None, title="Date the saved search is created"
    )

    date_updated: datetime | None = Field(
        None, title="Date the saved search is last updated"
    )

    updated_by: SavedSearchUpdatedBy | None = Field(
        None, title="Updated last of this saved search"
    )

    template: ValidSavedSearchTemplate = Field(
        ValidSavedSearchTemplate.standard,
        title="template type of saved search",
    )

    @classmethod
    @entity.Entity.event(name="SavedSearchCreated", fire_always=True)
    def create(
        cls,
        uuid: UUID,
        name: str,
        kind: SavedSearchKind,
        owner: SavedSearchOwner,
        template: ValidSavedSearchTemplate,
        filters: (
            CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition
        ),
        permissions: list[SavedSearchPermissionDefinition],
        columns: list[SavedSearchColumnDefinition],
        sort_column: str | None,
        sort_order: SortOrder,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "name": name,
                "kind": kind,
                "owner": owner,
                "template": template,
                "filters": filters,
                "permissions": permissions,
                "columns": columns,
                "labels": [],
                "sort_column": sort_column,
                "sort_order": sort_order,
                "_event_service": event_service,
                "date_created": datetime.now(timezone.utc),
            }
        )

    @entity.Entity.event(name="SavedSearchDeleted", fire_always=True)
    def delete(self):
        self.date_deleted = date.today()
        self.labels = []

    @entity.Entity.event(name="SavedSearchUpdated", fire_always=True)
    def update(
        self,
        name: str,
        filters: (
            CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition
        ),
        columns: list[SavedSearchColumnDefinition],
        sort_column: str | None,
        sort_order: SortOrder,
        updated_by: UUID,
        template: ValidSavedSearchTemplate,
    ):
        self.name = name
        self.filters = filters
        self.columns = columns
        self.sort_column = sort_column
        self.sort_order = sort_order
        self.date_updated = datetime.now(timezone.utc)
        self.updated_by = {"uuid": updated_by}
        self.template = template

    @entity.Entity.event(
        name="SavedSearchPermissionsUpdated", fire_always=True
    )
    def update_permissions(
        self, permissions: list[SavedSearchPermissionDefinition]
    ):
        self.permissions = permissions

    @entity.Entity.event("SavedSearchLabelsApplied", fire_always=True)
    def set_labels(self, labels: list[SavedSearchLabel]):
        self.labels = labels
