# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ... import ZaaksysteemRepositoryBase
from ...shared.case_filters import CaseFilter, CaseFilterApplicator
from ...shared.repositories import case_acl
from collections.abc import Sequence
from sqlalchemy import sql, types
from typing import Final
from uuid import UUID
from zsnl_domains.database import schema

CASE_ALIAS: Final = sql.alias(schema.Case.__table__)
ZAAK_BETROKKENEN_ASSIGNEE_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_BETROKKENEN_COORDINATOR_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_BETROKKENEN_REQUESTOR_ALIAS: Final = sql.alias(
    schema.ZaakBetrokkenen.__table__
)
ZAAK_ZAAKTYPE_RESULTATEN_ALIAS: Final = sql.alias(
    schema.ZaaktypeResultaten.__table__
)
ZAAK_BAG_ALIAS: Final = sql.alias(schema.ZaakBag.__table__)
NATUURLIJK_PERSOON_ALIAS: Final = sql.alias(schema.NatuurlijkPersoon.__table__)

CASE_QUERY_BASE: Final = (
    sql.select(
        sql.cast(CASE_ALIAS.c.uuid, types.TEXT).label("uuid"),
        CASE_ALIAS.c.id,
    )
    .select_from(
        sql.join(
            CASE_ALIAS,
            schema.ZaaktypeNode,
            CASE_ALIAS.c.zaaktype_node_id == schema.ZaaktypeNode.id,
        )
        .join(
            schema.CaseMeta,
            CASE_ALIAS.c.id == schema.CaseMeta.zaak_id,
        )
        .join(schema.Zaaktype, CASE_ALIAS.c.zaaktype_id == schema.Zaaktype.id)
        .join(
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS,
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS.c.id == CASE_ALIAS.c.behandelaar,
            isouter=True,
        )
        .join(
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS,
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS.c.id
            == CASE_ALIAS.c.coordinator,
            isouter=True,
        )
        .join(
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS,
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS.c.id == CASE_ALIAS.c.aanvrager,
        )
        .join(
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS,
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS.c.id == CASE_ALIAS.c.resultaat_id,
            isouter=True,
        )
        .join(
            ZAAK_BAG_ALIAS,
            ZAAK_BAG_ALIAS.c.id == CASE_ALIAS.c.locatie_zaak,
            isouter=True,
        )
    )
    .order_by(CASE_ALIAS.c.id)
)


class CaseRepository(ZaaksysteemRepositoryBase):
    def get_list(self, uuids: list[UUID], user_info: minty.cqrs.UserInfo):
        query = CASE_QUERY_BASE.where(
            case_acl.user_allowed_cases_subquery(
                user_info=user_info,
                permission="write",
                case_alias=CASE_ALIAS.c,
            )
        ).where(CASE_ALIAS.c.uuid.in_(uuids))

        return self.session_ro.execute(query).tuples().all()

    def get_filtered(
        self,
        user_info: minty.cqrs.UserInfo,
        filters: CaseFilter,
        page_size: int,
        last_id_seen: int | None,
    ) -> Sequence[tuple[str, int]]:
        query = CASE_QUERY_BASE.where(
            case_acl.user_allowed_cases_subquery(
                user_info=user_info,
                permission="write",
                case_alias=CASE_ALIAS.c,
            )
        )

        cf = CaseFilterApplicator(
            CASE_ALIAS,
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS,
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS,
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS,
            ZAAK_ZAAKTYPE_RESULTATEN_ALIAS,
            ZAAK_BAG_ALIAS,
            NATUURLIJK_PERSOON_ALIAS,
        )

        query = cf.apply_to_query(
            query, filters=filters.dict(), user_info=user_info
        )

        if last_id_seen:
            query = query.where(CASE_ALIAS.c.id > last_id_seen)

        query = query.limit(page_size)

        return self.session_ro.execute(query).tuples().all()
