# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import csv
import datetime
import io
import json
import minty.cqrs
import minty.exceptions
import os
import redis
import tempfile
import time
import zipfile
from ... import ZaaksysteemRepositoryBase
from ..entities.job import Job, JobDescription, JobStatus
from collections.abc import Sequence
from minty_infra_storage.s3 import S3Infrastructure, S3Wrapper
from typing import Final, Literal, TypeAlias, cast
from typing_extensions import TypedDict
from uuid import UUID, uuid4

OkResult: TypeAlias = tuple[tuple[str, ...], None]
ErrorResult: TypeAlias = tuple[None, str]
Result: TypeAlias = OkResult | ErrorResult


JOB_EXPIRATION_SECONDS: Final = 60 * 60 * 24 * 3  # 3 days, in seconds
MAX_USER_JOBS: Final = 4
MAX_INSTANCE_JOBS: Final = 20

# Command jobs: store output (static) in Redis
REDIS_RESULT_JOBS: Final = ("delete_case", "simulate_delete_case")
ARCHIVE_EXPORT_RESULT_JOBS: Final = "run_archive_export"

EXTENSION_FOR_TYPE: Final = {
    "text/csv": ".csv",
    "application/zip": ".zip",
}


class JobItemResult(TypedDict):
    type: Literal["success", "error"]
    value: str


# Redis layout/keys for jobs:
#
# Everything in Redis has a TTL/Expiration set to the job's expiration time.
# That way things auto-disappear.
#
# jobs:<instance_uuid>:<user_uuid>:<job_uuid>
#
#   A redis "hashmap", containing the job entity (as JSON), the user_info of
#   the user who started the job, and the "context" (hostname) where the job
#   was started.
#
# job_batch:<instance_uuid>:<job_uuid>:<state>
#
#   Contains the "batches" for the job, in each <state>:
#     waiting    - A list, contains items that need to be worked on for this job
#     processing - A list, contains items that are being worked on
#     done       - A list, contains items that are finished
#
#   Once a batch/item is done, it is also put in `job_result`:
#
# job_result:<instance_uuid>:<job_uuid>:success
# job_result:<instance_uuid>:<job_uuid>:error
#
#   Redis hashmap containing for each "done" batch/item, success/failure details
#   Contents can vary per job type
#     - For queries (exports) can contain filenames in S3 (also time-limited!)
#     - For commands (bulk actions) can contain just a simple indicator for
#       success, or an error message
#
#
# Flow:
#  - New batches are placed in 'waiting'
#  - Event "job is prepared" goes out
#  - Consumer picks up items from "processing" (if any) and "waiting" (and puts them into "processing")
#    - If the consumer gets killed:
#      - Event goes back on the bus
#      - Items stay in "processing"
#      - Restarted consumer continues where previous one left off.
#      - When a batch is done, it's removed from "processing" and moved to
#        job_result:*
#  - New event "batch processed" goes out
#  - Consumer picks up next batch
#    - etc.
#  - New event "processed" goes out if last batch is done
#    - Post-actions run (combine into output files etc.)
#  - New event "finished" goes out


class JobRepository(ZaaksysteemRepositoryBase):
    REQUIRED_INFRASTRUCTURE = {
        **ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE,
        "s3": S3Infrastructure(),
    }

    _for_entity = "Job"
    _events_to_calls: dict[str, str] = {
        "JobCreated": "_save_create_job",
        "JobDeleted": "_save_delete_job",
        "JobCancelled": "_save_cancel_job",
        "JobPrepared": "_save_job_prepared",
        "JobProgressed": "_save_job_progressed",
        "JobProcessed": "_save_job_processed",
        "JobFinished": "_save_job_finished",
    }

    def _get_redis_key_for_batch(
        self,
        job_uuid: UUID,
        state: Literal["waiting", "processing", "done", "result"],
    ) -> str:
        configuration: dict = self.infrastructure_factory.get_config(
            self.context
        )
        instance_uuid: str = configuration["instance_uuid"]

        return ":".join(
            ["job_batch", instance_uuid, str(job_uuid), state],
        )

    def _get_redis_key_for_job(self, job_uuid: UUID, user_uuid: UUID) -> str:
        return ":".join(
            [self._get_redis_key_jobs_user(user_uuid), str(job_uuid)]
        )

    def _get_redis_key_for_job_list(self, user_uuid: UUID) -> str:
        # "all" is never a valid UUID, so this doesn't clash even though it's
        # in the same "key namespace"
        return ":".join([self._get_redis_key_jobs_user(user_uuid), "all"])

    def _get_redis_key_jobs_user(self, user_uuid: UUID) -> str:
        instance_redis_key: str = self._get_redis_key_jobs_instance()
        return ":".join([instance_redis_key, str(user_uuid)])

    def _get_redis_key_jobs_instance(self) -> str:
        configuration: dict = self.infrastructure_factory.get_config(
            self.context
        )
        instance_uuid: str = configuration["instance_uuid"]

        return ":".join(["jobs", instance_uuid])

    def _count_active_jobs(
        self,
        redis: redis.Redis,
        job_key_pattern: str,
    ) -> int:
        found_jobs = 0
        for job_key in redis.scan_iter(job_key_pattern, count=10000):
            if job_key.endswith(b":all"):
                # This is the key that lists all jobs
                # (to help make get_jobs faster)
                # It has a different type (sorted set) and structure, so skip it.
                continue

            try:
                job = self._get_job_from_redis(redis, job_key)

                if job["status"] in (
                    JobStatus.pending,
                    JobStatus.running,
                ):
                    found_jobs += 1
            except minty.exceptions.NotFound:  # pragma: no cover
                # Key expired or was deleted between "scan" and "get".
                continue

        return found_jobs

    def _count_running_jobs_user(
        self, user_info: minty.cqrs.UserInfo, redis: redis.Redis
    ) -> int:
        instance_key = self._get_redis_key_jobs_user(user_info.user_uuid)
        job_key_pattern: str = f"{instance_key}:*"

        return self._count_active_jobs(redis, job_key_pattern)

    def _count_running_jobs_instance(self, redis: redis.Redis) -> int:
        instance_key = self._get_redis_key_jobs_instance()
        job_key_pattern: str = ":".join([instance_key, "*"])

        return self._count_active_jobs(redis, job_key_pattern)

    def _get_job_from_redis(
        self, redis_handle: redis.Redis, job_key: str | bytes
    ) -> dict:
        job_json_bytes = cast(
            bytes | None, redis_handle.hget(job_key, "entity")
        )

        if not job_json_bytes:
            raise minty.exceptions.NotFound(
                f"Job not found with key={job_key}", "jobs/not_found"
            )

        job_json_string: str = job_json_bytes.decode()
        job = json.loads(job_json_string)

        waiting_key = self._get_redis_key_for_batch(job["uuid"], "waiting")
        processing_key = self._get_redis_key_for_batch(
            job["uuid"], "processing"
        )
        done_key = self._get_redis_key_for_batch(job["uuid"], "done")

        waiting_len: int = redis_handle.llen(waiting_key)
        processing_len: int = redis_handle.llen(processing_key)
        done_len: int = redis_handle.llen(done_key)

        total_len = done_len + waiting_len + processing_len

        job["total_item_count"] = total_len

        if total_len > 0:
            job["progress_percent"] = (done_len / total_len) * 100.0
        else:
            if job["status"] == JobStatus.finished:
                job["progress_percent"] = 100.0
            else:
                job["progress_percent"] = 0.0

        return job

    def create_job(
        self,
        job_uuid: UUID,
        job_description: JobDescription,
        friendly_name: str,
        user_info: minty.cqrs.UserInfo,
    ) -> Job:
        redis_handle: redis.Redis = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        if (
            self._count_running_jobs_user(user_info, redis=redis_handle)
            >= MAX_USER_JOBS
        ):
            raise minty.exceptions.Conflict(
                f"User already has {MAX_USER_JOBS} active jobs. "
                "Wait until one finishes or stop a running job.",
                "jobs/user_too_many_jobs",
            )
        if (
            self._count_running_jobs_instance(redis=redis_handle)
            >= MAX_INSTANCE_JOBS
        ):
            raise minty.exceptions.Conflict(
                f"This environment already has {MAX_INSTANCE_JOBS} active jobs. "
                "Wait until one finishes or stop a running job.",
                "jobs/instance_too_many_jobs",
            )

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        job = Job.create(
            uuid=job_uuid,
            job_description=job_description,
            friendly_name=friendly_name,
            started_at=now,
            expires_at=(
                now + datetime.timedelta(seconds=JOB_EXPIRATION_SECONDS)
            ),
            event_service=self.event_service,
        )

        return job

    def get_job(self, job_uuid: UUID, user_info: minty.cqrs.UserInfo) -> Job:
        "Retrieve a single job from storage"

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )
        job_key = self._get_redis_key_for_job(job_uuid, user_info.user_uuid)

        job_data = self._get_job_from_redis(redis_handle, job_key)
        job = self._entity_from_json_data(job_data)

        return job

    def get_job_result_download_url(
        self, job_uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> str:
        job = self.get_job(job_uuid, user_info)

        if not (
            job.results_file
            and job.results_file_name
            and job.results_file_location
            and job.results_file_type
        ):
            raise minty.exceptions.NotFound(
                f"Results file not found for job {job_uuid}",
                "job/download_not_found",
            )

        event = minty.cqrs.events.Event.create_basic(
            domain="zsnl_domains_jobs",
            context=self.context,
            entity_type="Job",
            entity_id=job.uuid,
            event_name="JobResultsDownloaded",
            entity_data={"friendly_name": job.friendly_name},
            user_info=user_info,
        )
        self.send_query_event(event)

        storage: S3Wrapper = self._get_infrastructure("s3")
        return storage.get_download_url(
            uuid=job.results_file,
            storage_location=job.results_file_location,
            filename=job.results_file_name,
            mime_type=job.results_file_type,
            download=True,
        )

    def get_job_errors_download_url(
        self, job_uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> str:
        job = self.get_job(job_uuid, user_info)

        if not (
            job.errors_file
            and job.errors_file_name
            and job.errors_file_location
        ):
            raise minty.exceptions.NotFound(
                f"Errors file not found for job {job_uuid}",
                "job/download_not_found",
            )

        event = minty.cqrs.events.Event.create_basic(
            domain="zsnl_domains_jobs",
            context=self.context,
            entity_type="Job",
            entity_id=job.uuid,
            event_name="JobErrorsDownloaded",
            entity_data={"friendly_name": job.friendly_name},
            user_info=user_info,
        )
        self.send_query_event(event)

        storage: S3Wrapper = self._get_infrastructure("s3")
        return storage.get_download_url(
            uuid=job.errors_file,
            storage_location=job.errors_file_location,
            filename=job.errors_file_name,
            mime_type="text/csv",
            download=True,
        )

    def get_jobs_for_user(self, user_info: minty.cqrs.UserInfo) -> list[Job]:
        "Retrieve a list of jobs from storage"

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )
        job_list_key: str = self._get_redis_key_for_job_list(
            user_uuid=user_info.user_uuid,
        )

        jobs: list[Job] = []
        for key in [
            k.decode()
            for k in redis_handle.zrangebyscore(
                job_list_key, time.time(), "inf"
            )
        ]:
            job_key = self._get_redis_key_for_job(key, user_info.user_uuid)
            try:
                job_json_data: dict = self._get_job_from_redis(
                    redis_handle, job_key
                )
            except minty.exceptions.NotFound:  # pragma: no cover
                # If a job returned by "SCAN" is not found with "GET", it
                # means it was deleted (or expired) during the scan.
                # This is fine, we just skip it.
                #
                # This is a rare race condition that makes it basically
                # impossible to test.
                continue

            job: Job = self._entity_from_json_data(job_json_data)
            jobs.append(job)

        return jobs

    def add_batch(
        self, job: Job, batch: Sequence[str], user_info: minty.cqrs.UserInfo
    ) -> None:
        """
        Add a batch of UUIDs to the job.

        This is done immediately and works around the "event" cycle (update
        entity, save, etc.) because there can be a lot of UUIDs, which would
        lead to some very big events being sent across the message bus, and
        then ignored.

        Callers of this method should also call `job.finish_preparing()`
        to send out an event that allows job workers to start work.
        """
        # set TTL on batch key
        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        redis_key = self._get_redis_key_for_batch(job.entity_id, "waiting")

        pipeline = redis_handle.pipeline()
        pipeline.rpush(
            redis_key,
            *batch,
        )
        # Make sure batches get cleaned up along with the job itself
        pipeline.expireat(redis_key, job.expires_at)
        pipeline.execute()

    def copy_batch(
        self, job: Job, source_job_uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> None:
        """
        Copy the source job's batches to the new job
        """
        # set TTL on batch key
        redis_handle: redis.Redis = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        dest_redis_key = self._get_redis_key_for_batch(
            job.entity_id, "waiting"
        )
        source_redis_key = self._get_redis_key_for_batch(
            source_job_uuid, "done"
        )

        pipeline = redis_handle.pipeline()
        pipeline.copy(source_redis_key, dest_redis_key)

        # Make sure batches get cleaned up along with the job itself
        pipeline.expireat(dest_redis_key, job.expires_at)
        pipeline.execute()

    def get_item_from_batch(self, job: Job) -> str | None:
        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        waiting_redis_key = self._get_redis_key_for_batch(
            job.entity_id, "waiting"
        )
        processing_redis_key = self._get_redis_key_for_batch(
            job.entity_id, "processing"
        )

        processing_item: list[bytes] = redis_handle.lrange(
            processing_redis_key, 0, 0
        )

        if not processing_item:
            # No items were stuck "processing", get a new one from "waiting"
            # and put it in "processing"
            item = redis_handle.lmove(
                waiting_redis_key,
                processing_redis_key,
                "LEFT",
                "RIGHT",
            )
            redis_handle.expireat(processing_redis_key, job.expires_at)

            if item is None:
                return None

            assert isinstance(item, bytes)
            return item.decode("utf-8")

        return processing_item[0].decode("utf-8")

    def finish_item(self, job: Job, batch_item: str, result: Result) -> None:
        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        processing_key = self._get_redis_key_for_batch(
            job.entity_id, "processing"
        )
        done_key = self._get_redis_key_for_batch(job.entity_id, "done")

        result_key = self._get_redis_key_for_batch(job.entity_id, "result")

        match result:
            case (None, error):
                stored_result: JobItemResult = {
                    "type": "error",
                    "value": error,
                }
            case (success, None):
                stored_result: JobItemResult = {
                    "type": "success",
                    "value": success,
                }

        redis_handle.hset(result_key, batch_item, json.dumps(stored_result))
        redis_handle.expireat(result_key, job.expires_at)

        # We could do an extra check here to verify that the moved key equals
        # `batch_item`?
        redis_handle.lmove(
            processing_key,
            done_key,
            "LEFT",
            "RIGHT",
        )
        redis_handle.expireat(done_key, job.expires_at)

    def _entity_from_json_data(self, json_data: dict) -> Job:
        return Job(
            **json_data,
            entity_id=json_data["uuid"],
            _event_service=self.event_service,
        )

    def _save_create_job(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        "Save a 'JobCreated' event to storage"

        changes = event.format_changes()

        redis_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )
        job_list_key: str = self._get_redis_key_for_job_list(
            user_uuid=userinfo.user_uuid,
        )

        entity_data = {
            "uuid": changes["uuid"],
            "status": changes["status"],
            "results_file": None,
            "errors_file": None,
            "job_description": changes["job_description"],
            "friendly_name": changes["friendly_name"],
            "started_at": changes["started_at"],
            "expires_at": changes["expires_at"],
        }

        expires_at = datetime.datetime.fromisoformat(changes["expires_at"])

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        pipeline = redis_handle.pipeline()
        pipeline.hset(
            redis_key,
            mapping={
                "entity": json.dumps(entity_data),
                "user_info": userinfo.json(),
                "context": event.context,
            },
        )
        pipeline.expireat(redis_key, expires_at)

        # Clear out any old/expired jobs from the user's job list (sorted set)
        pipeline.zremrangebyscore(job_list_key, "-inf", time.time())

        # Add job to the sorted set for quick retrieval
        pipeline.zadd(job_list_key, {event.entity_id: expires_at.timestamp()})

        # Update the sorted set of jobs' expiration, so the whole thing disappears
        # after the last one expires:
        pipeline.expireat(job_list_key, expires_at)

        pipeline.execute()

        return

    def _save_delete_job(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        "Save a 'JobDeleted' event to storage"

        redis_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        redis_handle.delete(redis_key)

    def _save_cancel_job(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        "Save a 'JobCancelled' event to storage"

        redis_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        changes = event.format_changes()

        pipeline = redis_handle.pipeline()

        # If something else touches the Redis key between getting the job data and saving it
        # back, cancel this whole transaction.
        #
        # This can happen when a "cancel" and a "update status to finished"
        # run at the exact same time. In that case the job is done and no longer
        # cancellable.
        pipeline.watch(redis_key)

        job_data = self._get_job_from_redis(pipeline, redis_key)

        if job_data["status"] not in (
            JobStatus.running,
            JobStatus.pending,
        ):
            pipeline.unwatch()
            raise minty.exceptions.Conflict(
                "Cannot cancel: job is not running or pending",
                "jobs/cancel/wrong_status",
            )

        job_data["status"] = JobStatus.cancelled
        job_data["cancel_reason"] = changes.get("cancel_reason", None)

        pipeline.multi()
        pipeline.hset(redis_key, "entity", json.dumps(job_data))
        pipeline.execute()

    def _save_job_prepared(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        "Save a 'JobPrepared' event to storage"

        redis_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        pipeline = redis_handle.pipeline()

        # If something else touches the Redis key between getting the job data and saving it
        # back, cancel this whole transaction.
        #
        # This can happen when a "update status to running" and a "cancel"
        # run at the exact same time. In that case, the status should not be
        # updated and stay cancelled.
        pipeline.watch(redis_key)

        job_data = self._get_job_from_redis(pipeline, redis_key)

        if job_data["status"] != JobStatus.pending:
            pipeline.unwatch()
            raise minty.exceptions.Conflict(
                "Cannot finish preparing: job not pending?",
                "jobs/cannot_finish_prepare",
            )

        job_data["status"] = JobStatus.running

        pipeline.multi()
        pipeline.hset(redis_key, "entity", json.dumps(job_data))
        pipeline.execute()

    def _save_job_progressed(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        pass

    def _save_job_processed(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        "Save a 'JobProcessed' event to storage"

        redis_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        pipeline = redis_handle.pipeline()

        # If something else touches the Redis key between getting the job data
        # and saving it back, cancel this whole transaction.
        #
        # This can happen when a "update status to processed" and a "cancel"
        # run at the exact same time. In that case, the status should not be
        # updated and stay cancelled.
        pipeline.watch(redis_key)

        job_data = self._get_job_from_redis(pipeline, redis_key)

        if job_data["status"] != JobStatus.running:
            pipeline.unwatch()
            raise minty.exceptions.Conflict(
                "Cannot finish processing: job not running?",
                "jobs/cannot_finish_processing",
            )

        job_data["status"] = JobStatus.processed

        pipeline.multi()
        pipeline.hset(redis_key, "entity", json.dumps(job_data))
        pipeline.execute()

    def _save_job_finished(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ) -> None:
        "Save a 'JobFinished' event to storage"

        job_key: str = self._get_redis_key_for_job(
            job_uuid=event.entity_id,
            user_uuid=userinfo.user_uuid,
        )
        result_key = self._get_redis_key_for_batch(event.entity_id, "result")

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )

        job_data = self._get_job_from_redis(redis_handle, job_key)

        # job type (command/query - redis/s3 stored)
        # extract and combine
        job_type = job_data["job_description"]["job_type"]

        # Generate filenames for success/errors
        job_data["results_file"] = str(uuid4())
        job_data["errors_file"] = str(uuid4())

        if job_type in REDIS_RESULT_JOBS:
            self._extract_redis_result(redis_handle, job_data)
        elif job_type in ARCHIVE_EXPORT_RESULT_JOBS:
            self._extract_archive_export_result(
                job_data,
                event.entity_id,
            )
        else:
            # Uncoverable, this is a guard for when we're developing new
            # job types, so we don't forget to implement the post-processing.
            raise NotImplementedError(  # pragma: no cover
                f"Unknown job type during post-processing: {job_type=}"
            )

        # This is run _after_ extraction, because different extractors
        # make different result file types (csv, zip, etc)
        job_start = datetime.datetime.fromisoformat(
            job_data["started_at"]
        ).isoformat(timespec="seconds")

        job_data["results_file_name"] = (
            job_start
            + "-"
            + job_data["job_description"]["job_type"]
            + "-results"
            + EXTENSION_FOR_TYPE.get(job_data["results_file_type"], ".bin")
        )

        job_data["errors_file_name"] = (
            job_start
            + "-"
            + job_data["job_description"]["job_type"]
            + "-errors"
            + ".csv"
        )

        job_data["status"] = JobStatus.finished

        redis_handle.hset(job_key, "entity", json.dumps(job_data))

        # Remove intermediate results (could be a lot of data)
        redis_handle.unlink(result_key)

    def _extract_archive_export_result(
        self,
        job_data: dict,
        entity_id: UUID,
    ):
        # Extract archive export created for each case and combine them into single export.
        storage: S3Wrapper = self._get_infrastructure("s3")

        job_data["results_file_type"] = "application/zip"

        redis_handle = cast(
            redis.Redis, self._get_infrastructure(name="redis")
        )
        result_key = self._get_redis_key_for_batch(job_data["uuid"], "result")

        job_items: list[bytes] = redis_handle.lrange(
            self._get_redis_key_for_batch(job_data["uuid"], "done"), 0, -1
        )

        with (
            tempfile.TemporaryFile(
                mode="w+b",
            ) as file_handle,
            zipfile.ZipFile(
                file_handle, "w", zipfile.ZIP_DEFLATED, compresslevel=9
            ) as new_archive,
            tempfile.TemporaryFile(
                mode="w+b",
            ) as errors_file,
        ):
            errors_buffer = io.TextIOWrapper(errors_file, write_through=True)
            errors_csv = csv.writer(errors_buffer, dialect="excel")

            for item in job_items:
                item_str = item.decode("utf-8")
                item_parts = json.loads(item_str)

                result = redis_handle.hget(result_key, item)
                result_data: JobItemResult = json.loads(result)

                match result_data["type"]:
                    case "success":
                        file_uuid = result_data["value"][0]["file_uuid"]
                        storage_location = result_data["value"][0][
                            "file_storage_location"
                        ]
                        tmp_handle = tempfile.NamedTemporaryFile(mode="w+b")

                        storage.download_file(
                            destination=tmp_handle,
                            file_uuid=file_uuid,
                            storage_location=storage_location,
                        )
                        tmp_handle.seek(0, os.SEEK_SET)

                        with zipfile.ZipFile(tmp_handle, "r") as source_zip:
                            for filename in source_zip.namelist():
                                if not filename.endswith("/"):
                                    with source_zip.open(
                                        filename
                                    ) as source_file:
                                        content = source_file.read()
                                        new_archive.writestr(filename, content)
                    case "error":
                        errors_csv.writerow(
                            [*item_parts, result_data["value"]]
                        )
            errors_file.flush()
            new_archive.close()

            upload_result = storage.upload(
                file_handle=file_handle,
                uuid=job_data["results_file"],
                tags={"type": "job_result"},
            )

            job_data["results_file_location"] = upload_result[
                "storage_location"
            ]
            job_data["results_file_size"] = upload_result["size"]
            upload_result = storage.upload(
                file_handle=errors_file,
                uuid=job_data["errors_file"],
                tags={"type": "job_result"},
            )
            job_data["errors_file_location"] = upload_result[
                "storage_location"
            ]
            job_data["errors_file_size"] = upload_result["size"]

    def _extract_redis_result(
        self,
        redis_handle: redis.Redis,
        job_data: dict,
    ):
        storage: S3Wrapper = self._get_infrastructure("s3")

        job_data["results_file_type"] = "text/csv"

        result_key = self._get_redis_key_for_batch(job_data["uuid"], "result")

        job_items: list[bytes] = redis_handle.lrange(
            self._get_redis_key_for_batch(job_data["uuid"], "done"), 0, -1
        )

        with (
            tempfile.TemporaryFile(
                mode="w+b",
            ) as results_file,
            tempfile.TemporaryFile(
                mode="w+b",
            ) as errors_file,
        ):
            results_buffer = io.TextIOWrapper(results_file, write_through=True)
            errors_buffer = io.TextIOWrapper(errors_file, write_through=True)
            results_csv = csv.writer(results_buffer, dialect="excel")
            errors_csv = csv.writer(errors_buffer, dialect="excel")

            for item in job_items:
                item_str = item.decode("utf-8")
                item_parts = json.loads(item_str)

                result = redis_handle.hget(result_key, item)
                if not result:
                    raise AssertionError(
                        f"Result {item} found in 'done' list but not in result hash"
                    )

                result_data: JobItemResult = json.loads(result)

                match result_data["type"]:
                    case "success":
                        results_csv.writerow(
                            [*item_parts, *result_data["value"]]
                        )
                    case "error":
                        errors_csv.writerow(
                            [*item_parts, result_data["value"]]
                        )

            results_file.flush()
            errors_file.flush()

            upload_result = storage.upload(
                file_handle=results_file,
                uuid=job_data["results_file"],
                tags={"type": "job_result"},
            )
            job_data["results_file_location"] = upload_result[
                "storage_location"
            ]
            job_data["results_file_size"] = upload_result["size"]

            upload_result = storage.upload(
                file_handle=errors_file,
                uuid=job_data["errors_file"],
                tags={"type": "job_result"},
            )
            job_data["errors_file_location"] = upload_result[
                "storage_location"
            ]
            job_data["errors_file_size"] = upload_result["size"]
