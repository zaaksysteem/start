# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from zsnl_domains.jobs.commands.base import JobCommand

JOBS_COMMANDS: dict[str, type[minty.cqrs.SplitCommandBase]] = (
    minty.cqrs.build_command_lookup_table(
        commands=set(JobCommand.__subclasses__())
    )
)
