# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.cqrs
from ..entities.job import (
    CaseJob,
    CaseSelectionFilter,
    CaseSelectionIdList,
    CaseSelectionOtherJob,
    Job,
    JobCancelReason,
    JobDescription,
    JobStatus,
)
from ..repositories.case import CaseRepository
from ..repositories.job import JobRepository
from pydantic.v1 import validate_arguments
from typing import Final, cast
from uuid import UUID

BATCH_PAGE_SIZE: Final = 1000


class JobCommand(minty.cqrs.SplitCommandBase):
    pass


class CreateJob(JobCommand):
    name = "create_job"

    @validate_arguments
    def __call__(
        self,
        job_uuid: UUID,
        job_description: JobDescription,
        friendly_name: str,
    ) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))

        repo.create_job(
            job_uuid=job_uuid,
            job_description=job_description,
            friendly_name=friendly_name,
            user_info=self.cmd.user_info,
        )
        repo.save(self.cmd.user_info)


class DeleteJob(JobCommand):
    name = "delete_job"

    @validate_arguments
    def __call__(self, job_uuid: UUID) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job.delete()
        repo.save(self.cmd.user_info)


class CancelJob(JobCommand):
    name = "cancel_job"

    @validate_arguments
    def __call__(
        self,
        job_uuid: UUID,
    ) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job.cancel(JobCancelReason.user_action)
        repo.save(self.cmd.user_info)


class PrepareJob(JobCommand):
    """
    Prepare a job for running.

    For example, retrieve all relevant case UUIDs using the search filters
    etc.
    """

    name = "prepare_job"

    def prepare_cases_from_selection(self, job: Job) -> None:
        if isinstance(job.job_description, CaseJob):
            match job.job_description.selection.type:
                case "filter":
                    self.prepare_cases_from_filters(job)
                case "source_job":
                    self.prepare_cases_from_source(job)
                case "id_list":
                    self.prepare_cases_from_id_list(job)
                case _:  # pragma: no cover
                    # Uncoverable: this helps when writing new types of
                    # "selection" for case jobs
                    raise AssertionError(
                        f"Unknown selection: {job.job_description.selection.type}"
                    )
        else:
            # Uncoverable: this helps developers who write new types of jobs
            # that apply to other objects than cases
            raise AssertionError(  # pragma: no cover
                f"Don't know how to handle job of type {type(job.job_description)}"
            )

    def prepare_cases_from_filters(self, job: Job) -> None:
        assert isinstance(job.job_description, CaseJob)
        assert isinstance(job.job_description.selection, CaseSelectionFilter)

        filters = job.job_description.selection.filters

        case_repo = cast(CaseRepository, self.get_repository("case"))
        jobs_repo = cast(JobRepository, self.get_repository("jobs"))

        page_size = BATCH_PAGE_SIZE
        last_id_seen: int | None = None

        while batch := case_repo.get_filtered(
            user_info=self.cmd.user_info,
            filters=filters,
            page_size=page_size,
            last_id_seen=last_id_seen,
        ):
            # Case batches are keyed on a JSON array with [case_uuid, case_id]
            # That way, both will end up in the result and error CSVs.
            # (UUID alone makes users unhappy)
            jobs_repo.add_batch(
                job=job,
                batch=[json.dumps([b[0], b[1]]) for b in batch],
                user_info=self.cmd.user_info,
            )
            # Batches are lists of batch items
            # Batch items are tuples of [case uuid, case id]
            # The "last seen id" is the second item from the last tuple in the list
            last_id_seen = batch[-1][1]

    def prepare_cases_from_source(self, job: Job) -> None:
        assert isinstance(job.job_description, CaseJob)
        assert isinstance(job.job_description.selection, CaseSelectionOtherJob)

        jobs_repo = cast(JobRepository, self.get_repository("jobs"))

        try:
            source_job = jobs_repo.get_job(
                job.job_description.selection.source_job,
                user_info=self.cmd.user_info,
            )
        except minty.exceptions.NotFound as e:
            self.logger.info(f"Source job not found: {e}")
            job.cancel(JobCancelReason.source_not_found)
            return

        if source_job.status != JobStatus.finished:
            job.cancel(JobCancelReason.source_not_finished)
            return

        jobs_repo.copy_batch(
            job=job,
            source_job_uuid=job.job_description.selection.source_job,
            user_info=self.cmd.user_info,
        )

    def prepare_cases_from_id_list(self, job: Job) -> None:
        assert isinstance(job.job_description, CaseJob)
        assert isinstance(job.job_description.selection, CaseSelectionIdList)

        id_list = job.job_description.selection.id_list
        case_repo = cast(CaseRepository, self.get_repository("case"))
        jobs_repo = cast(JobRepository, self.get_repository("jobs"))

        # CaseSelectionIdList has a maximum number of items so this should
        # never grow to be super big.
        cases: dict[str, int] = {
            case[0]: case[1]
            for case in case_repo.get_list(
                uuids=id_list,
                user_info=self.cmd.user_info,
            )
        }

        jobs_repo.add_batch(
            job=job,
            batch=[
                json.dumps([str(uuid), cases.get(str(uuid), -1)])
                for uuid in id_list
            ],
            user_info=self.cmd.user_info,
        )

    @validate_arguments
    def __call__(self, job_uuid: UUID) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job_type = job.job_description.job_type

        # Will only be "committed" on save below, but does some checks we
        # need (like "is this job in the right state to prepare now")
        job.finish_preparing()

        match job_type:
            case "delete_case" | "simulate_delete_case" | "run_archive_export":
                self.prepare_cases_from_selection(job)
            case _:  # pragma: no cover
                # This can only happen when developing a new type of
                # job, so this is here to help guide the developer.
                raise AssertionError(
                    f"prepare_job called with unknown job type {job_type}"
                )

        repo.save(self.cmd.user_info)


class ProgressJob(JobCommand):
    name = "progress_job"

    @validate_arguments
    def __call__(self, job_uuid: UUID) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job.mark_progress()
        repo.save(self.cmd.user_info)


class JobProcessed(JobCommand):
    name = "job_processed"

    @validate_arguments
    def __call__(self, job_uuid: UUID) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job.mark_processed()
        repo.save(self.cmd.user_info)


class FinishJob(JobCommand):
    name = "finish_job"

    @validate_arguments
    def __call__(self, job_uuid: UUID) -> None:
        repo = cast(JobRepository, self.get_repository("jobs"))
        job = repo.get_job(job_uuid, user_info=self.cmd.user_info)

        job.mark_finished()
        repo.save(self.cmd.user_info)
