# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .case import Case
from .contact import Contact
from collections.abc import Iterable, Mapping
from datetime import datetime, timezone
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from uuid import UUID


class Thread(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        id: int | None = None,
        thread_type: str | None = None,
        created=None,
        last_modified=None,
        last_message_cache=None,
        case: Case | None = None,
        contact: Contact | None = None,
        number_of_messages: int = 0,
        unread_pip_count: int | None = None,
        unread_employee_count: int | None = None,
        attachment_count: int | None = None,
        is_notification: bool = False,
        external_message_subject: str | None = None,
        external_message_participants: Iterable[Mapping[str, str]]
        | None = None,
        external_message_direction: str | None = None,
        external_message_message_date: datetime | None = None,
    ):
        self.uuid = uuid
        self.id = id
        self.thread_type = thread_type
        self.created = created
        self.last_modified = last_modified
        self.last_message_cache = last_message_cache or {}
        self.contact = contact
        self.case = case
        self.contact_displayname = None
        self.number_of_messages = number_of_messages
        self.unread_pip_count = unread_pip_count
        self.unread_employee_count = unread_employee_count
        self.attachment_count = attachment_count
        self.is_notification = is_notification
        self.external_message_subject = (external_message_subject,)
        self.external_message_participants = (external_message_participants,)
        self.external_message_direction = (external_message_direction,)
        self.external_message_message_date = (external_message_message_date,)

    @event("ThreadCreated")
    def create(
        self,
        thread_type: str,
        contact: Contact | None,
        case: Case | None,
        last_message_cache: dict | None = None,
        is_notification: bool = False,
    ):
        now = datetime.now(timezone.utc).isoformat()
        self.thread_type = thread_type

        if contact is not None:
            self.contact = contact
            self.contact_displayname = contact.name
        if case is not None:
            self.case = case
        self.last_message_cache = last_message_cache or {}
        self.created = now
        self.last_modified = now
        self.unread_pip_count = 0
        self.unread_employee_count = 0
        self.attachment_count = 0
        self.is_notification = is_notification

    @event("ThreadToCaseLinked")
    def link_thread_to_case(
        self,
        case: Case,
        external_message_type: str,
        external_message_subject: str | None,
        external_message_participants: Iterable[Mapping[str, str]] | None,
        external_message_direction: str | None,
        external_message_message_date: datetime | None,
    ):
        if self.last_message_cache["message_type"] != external_message_type:
            raise Conflict(
                f"Thread with message type '{self.last_message_cache['message_type']}' "
                f"could not be linked to another message type of '{external_message_type}'",
                "communication/thread/not_linked",
            )

        if self.case is not None:
            raise Conflict(
                f"Thread with uuid '{self.uuid}' could not be linked to case "
                + f"'{case.id}', because it's already linked to a case.",
                "communication/thread/not_linked",
            )

        now = datetime.now(timezone.utc).isoformat()
        self.case = case
        self.uuid = self.uuid
        self.last_message_cache = {"message_type": external_message_type}
        self.last_modified = now
        self.external_message_subject = external_message_subject
        self.external_message_message_date = external_message_message_date
        self.external_message_participants = external_message_participants
        self.external_message_direction = external_message_direction

    @event("ThreadDeleted")
    def delete(self):
        """Deletes the current thread"""
        pass

    @event("ThreadUnReadCountDecremented")
    def decrement_unread_count(self, context):
        if context == "pip":
            self.unread_pip_count = self.unread_pip_count - 1
        else:
            self.unread_employee_count = self.unread_employee_count - 1

    @event("ThreadUnReadCountIncremented")
    def increment_unread_count(self, context: str | None = None):
        if context is None:
            self.unread_employee_count = self.unread_employee_count + 1
            self.unread_pip_count = self.unread_pip_count + 1
        elif context == "pip":
            self.unread_pip_count = self.unread_pip_count + 1
        elif context == "employee":
            self.unread_employee_count = self.unread_employee_count + 1

    @event("ThreadAttachmentCountIncremented")
    def increment_attachment_count(self):
        self.attachment_count = self.attachment_count + 1

    @event("ThreadAttachmentCountDecremented")
    def decrement_attachment_count(self):
        self.attachment_count = self.attachment_count - 1
