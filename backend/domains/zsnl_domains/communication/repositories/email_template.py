# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities.email_template import EmailTemplate
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class EmailTemplateRepository(ZaaksysteemRepositoryBase):
    def get_email_template_by_uuid(
        self, template_uuid: UUID
    ) -> EmailTemplate | None:
        """Return EmailTemplate for a given `template_uuid`"""
        bibliotheek_notificatie_simple_query = sql.select(
            schema.BibliotheekNotificatie.id,
            schema.BibliotheekNotificatie.uuid,
            schema.BibliotheekNotificatie.sender_address,
            schema.BibliotheekNotificatie.sender,
        ).where(schema.BibliotheekNotificatie.uuid == template_uuid)
        result = self.session.execute(
            bibliotheek_notificatie_simple_query
        ).fetchone()

        return self._transform_to_entity(result) if result else None

    def _transform_to_entity(self, result) -> EmailTemplate:
        return EmailTemplate(
            id=result.id,
            uuid=result.uuid,
            sender_address=result.sender_address,
            sender=result.sender,
        )
