# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field
from uuid import UUID


class DocumentArchive(Entity):
    entity_type = "document_archive"
    entity_id__fields: list[str] = ["case_uuid"]

    case_uuid: UUID | None = Field(None, title="Case to zip document for")
    zip_all: bool | None = Field(
        None, title="should all files of the case be zipped"
    )

    document_uuids: list | None = Field(
        ..., title="List of documents uuids to zip"
    )
    directory_uuids: list | None = Field(
        ..., title="List of directory uuids to zip"
    )
    export_file_uuid: UUID | None = Field(..., title="export file uuid")

    @Entity.event(
        name="DocumentArchiveRequested",
        fire_always=True,
    )
    def document_archive_requested(
        self,
        case_uuid: UUID,
        zip_all: bool,
        directory_uuids: list[UUID],
        document_uuids: list[UUID],
        export_file_uuid: UUID,
    ):
        self.case_uuid = case_uuid
        self.zip_all = zip_all
        self.directory_uuids = directory_uuids
        self.document_uuids = document_uuids
        self.export_file_uuid = export_file_uuid

    @Entity.event(
        name="DocumentArchiveCreated",
        fire_always=True,
    )
    def document_archive_created(self, case_uuid, document_uuids):
        self.case_uuid = case_uuid
        self.document_uuids = document_uuids
