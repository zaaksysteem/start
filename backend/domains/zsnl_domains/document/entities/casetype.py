# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, ValueObject
from pydantic import Field
from typing import Literal
from uuid import UUID


class Attribute(ValueObject):
    id: int
    label: str
    is_group: bool
    magic_string: str


class Casetype(Entity):
    entity_type: Literal["casetype"] = "casetype"
    entity_id__fields: list[str] = ["uuid"]

    uuid: UUID = Field(
        ..., description="Unique uuid identifier of this casetype"
    )
    attributes: list[Attribute] = Field(
        description="List of casetype Attributes, including groups"
    )

    @property
    def attribute_match_dict(self) -> dict[str, str]:
        """Matches attributes to their groups label"""
        attribute_match_dict = {}
        current_group: str = ""
        for attribute in self.attributes:
            if attribute.is_group:
                current_group = attribute.label
            else:
                attribute_match_dict[attribute.magic_string] = current_group
        return attribute_match_dict
