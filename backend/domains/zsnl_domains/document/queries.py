# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.events
import minty.exceptions
from .repositories import CaseRegistrationFormRepository
from minty import exceptions
from minty.cqrs import QueryBase
from minty.entity import RedirectResponse
from minty.exceptions import Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_call
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import UUID
from zsnl_domains.document.entities.case_registration_form import (
    CaseRegistrationForm,
)
from zsnl_domains.document.repositories import DocumentRepository, constants


class Queries(QueryBase):
    """Queries class for document."""

    @property
    def repository(self) -> DocumentRepository:
        return self.get_repository("document")

    def assert_user_info_present(self):
        if not self.user_info:
            raise exceptions.Forbidden(
                "No user information", "document/user_required"
            )
        return

    @validate_with(get_data(__name__, "validation/get_document.json"))
    def get_document_by_uuid(
        self, document_uuid: UUID, integrity_check: bool = False
    ):
        """Get document entity as python dictionary by given `document uuid`.
        :param documen_uuid: identifier of document
        :type document_uuid: UUID
        """
        document = self.repository.get_document_by_uuid(
            document_uuid, self.user_info, integrity_check
        )
        return document

    @validate_arguments
    def search_document(
        self,
        case_uuid: UUID | None,
        keyword: str | None,
    ):
        """Search document by case_uuid or keyword.

        :param case_uuid: uuid of case.
        :type case_uuid: Optional[str]
        :param keyword: keyword for searching.
        :type keyword: Optional[str]
        """
        assert self.user_info, "User info required to search for documents"

        document = self.repository.search_document(
            case_uuid=case_uuid,
            user_info=self.user_info,
            keyword=keyword,
        )
        return document

    @validate_with(
        get_data(__name__, "validation/get_directory_entries_for_case.json")
    )
    def get_directory_entries_for_case(
        self,
        case_uuid: UUID,
        directory_uuid: UUID = None,
        search_term: str = None,
        no_empty_folders: bool = True,
    ):
        """Get the list of directory_entries for a case to show in document_tab.

        To retrive the list of documents and directories related to a case.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param directory_uuid: UUID of the parent_directory, defaults to None
        :type directory_uuid: UUID, optional
        :param search_term: Filter the results with search_term, defaults to None
        :type search_term: str, optional
        :param no_empty_folders: Boolean to filter out empty folders.
        :type no_empty_folders: bool, optional
        :return: List of directory entries.
        :rtype: typing.List[DirectoryEntry]
        """
        directory_entry_repo = self.get_repository("directory_entry")

        entries = directory_entry_repo.get_directory_entries_for_case(
            user_info=self.user_info,
            case_uuid=case_uuid,
            directory_uuid=directory_uuid,
            search_term=search_term,
            no_empty_folders=no_empty_folders,
        )

        return entries

    def get_parent_directories_for_directory(self, directory_uuid: UUID):
        """Get parent directories for directory.
        :param directory_uuid: directory_uuid
        :type directory_uuid: UUID
        """
        directory_repo = self.get_repository("directory")

        directories = directory_repo.get_parent_directories_for_directory(
            directory_uuid=directory_uuid
        )

        return directories

    @validate_arguments
    def get_document_download_link(self, document_uuid: UUID):
        """
        Get the download link for a document given by document UUID
        :param document_uuid: The document UUID
        :param user_uuid: The user UUID
        :return:
        """
        document_repo = self.repository

        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        url = document_repo.generate_download_url(
            document=document, user_info=self.user_info
        )
        entity_data = {}
        if document.case_uuid:
            entity_data = {"case_uuid": str(document.case_uuid)}

        event = minty.cqrs.events.Event.create_basic(
            domain="zsnl_domains_document",
            context=self.context,
            entity_type="Document",
            entity_id=document.document_uuid,
            event_name="DocumentDownloaded",
            entity_data=entity_data,
            user_info=self.user_info,
        )
        document_repo.send_query_event(event)

        return url

    @validate_with(
        get_data(__name__, "validation/get_directory_entries_for_intake.json")
    )
    def get_directory_entries_for_intake(
        self,
        page: int,
        page_size: int,
        sort: str | None = None,
        search_term: str | None = None,
        assigned: bool | None = None,
        assigned_to_self: bool | None = None,
    ):
        """Get the list of documents as directory_entries for a document intake.

        To retrive the list of documents not related to a case.

        :param search_term: Filter the results with search_term, defaults to None
        :type search_term: str, optional
        :return: List of directory entries.
        :rtype: typing.List[DirectoryEntry]
        """
        directory_entry_repo = self.get_repository("directory_entry")

        entries = directory_entry_repo.get_directory_entries_for_intake(
            user_info=self.user_info,
            page=page,
            page_size=page_size,
            sort=sort,
            search_term=search_term,
            assigned=assigned,
            assigned_to_self=assigned_to_self,
        )
        return entries

    @validate_call
    def get_directory_entries_for_intake_count(
        self,
        search_term: str | None = None,
        assigned: bool | None = None,
        assigned_to_self: bool | None = None,
    ):
        """Get the count of documents as directory_entries for a document intake.

        To retrieve the count of documents not related to a case.
        """
        directory_entry_repo = self.get_repository("directory_entry")

        count = directory_entry_repo.get_directory_entries_for_intake_count(
            user_info=self.user_info,
            search_term=search_term,
            assigned=assigned,
            assigned_to_self=assigned_to_self,
        )
        return count

    @validate_arguments
    def get_document_preview_link(self, document_uuid: UUID):
        """
        Get the preview link for a document given by document UUID
        :param document_uuid: The document UUID
        :param user_uuid: The user UUID
        :return:
        """
        self.assert_user_info_present()

        document_repo = self.repository
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        if (
            document.mimetype in constants.IMAGE_MIMETYPES
            or document.mimetype == constants.PDF_MIMETYPE
        ):
            preview_uuid = document.store_uuid
            preview_mimetype = document.mimetype
            preview_storage_location = document.storage_location
        else:
            preview_uuid = document.preview_uuid
            preview_mimetype = document.preview_mimetype
            preview_storage_location = document.preview_storage_location

        if not document.has_preview():
            if document.can_preview():
                event = minty.cqrs.events.Event.create_basic(
                    domain="zsnl_domains_document",
                    context=self.context,
                    entity_type="Document",
                    entity_id=document_uuid,
                    event_name="PreviewRequested",
                    entity_data={},
                    user_info=self.user_info,
                )
                document_repo.send_query_event(event)

                raise exceptions.NotFound(
                    f"Preview for document {document_uuid} is being created",
                    "document/preview_creating",
                )

            raise exceptions.NotFound(
                f"No preview found for document with uuid {document_uuid}",
                "document/preview_not_found",
            )

        url = document_repo.generate_preview_url(
            preview_uuid=preview_uuid,
            preview_storage_location=preview_storage_location[0],
            preview_mime_type=preview_mimetype,
            preview_filename=document.basename + document.extension,
        )
        return url

    @validate_with(
        get_data(__name__, "validation/get_document_labels_for_case.json")
    )
    def get_document_labels_for_case(self, case_uuid: UUID):
        """Get document_labels for a case by case_uuid.

        :param case_uuid: UUID of the case
        :type case_uuid: UUID
        """

        repo = self.get_repository("document_label")
        document_labels = repo.get_document_labels_for_case(
            case_uuid=case_uuid, user_uuid=self.user_uuid
        )
        return document_labels

    @validate_arguments
    def get_document_thumbnail_link(self, document_uuid: UUID):
        """
        Get the thumbnail link for a document given by document UUID
        :param document_uuid: The document UUID
        :type document_uuid: UUID
        :return: Redirect link to the preview of document.
        """

        self.assert_user_info_present()

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        if not document.has_thumbnail():
            if document.can_thumbnail():
                event = minty.cqrs.events.Event.create_basic(
                    domain="zsnl_domains_document",
                    context=self.context,
                    entity_type="Document",
                    entity_id=document_uuid,
                    event_name="ThumbnailRequested",
                    entity_data={},
                    user_info=self.user_info,
                )
                document_repo.send_query_event(event)

                raise exceptions.NotFound(
                    f"Thumbnail for document {document_uuid} is being created",
                    "document/thumbnail_creating",
                )

            raise exceptions.NotFound(
                f"No thumbnail found for document with uuid {document_uuid}",
                "document/thumbnail_not_found",
            )

        url = document_repo.generate_thumbnail_url(
            thumbnail_uuid=document.thumbnail_uuid,
            thumbnail_storage_location=document.thumbnail_storage_location[0],
            thumbnail_mimetype=document.thumbnail_mimetype,
        )
        return url

    def get_edit_document_url(self, document_uuid: UUID, user_info, save_url):
        """Get URL to edit document online.

        :param document_uuid: UUID of the document.
        :type document_uuid: UUID
        :param user_info: user_info
        :type user_info: object
        :param save_url: url to save document
        :type save_url: str
        :rtype: URL to open document for editing
        """

        document_repo = self.get_repository("document")

        url = document_repo.generate_document_edit_url(
            document_uuid=document_uuid, user_info=user_info, save_url=save_url
        )

        return url

    def get_ms_wopi_configuration(
        self,
        document_uuid: UUID,
        user_info: object,
        save_url: str,
        close_url: str,
        context: str,
        host_page_url: str,
        lock_acquire_url: str,
        lock_release_url: str,
        lock_extend_url: str,
    ):
        """Get WOPI configuration to edit document with ms online"""

        document_repo = self.get_repository("document")
        return document_repo.get_ms_wopi_configuration(
            document_uuid=document_uuid,
            user_info=user_info,
            save_url=save_url,
            close_url=close_url,
            host_page_url=host_page_url,
            context=context,
            lock_acquire_url=lock_acquire_url,
            lock_release_url=lock_release_url,
            lock_extend_url=lock_extend_url,
        )

    @validate_call
    def get_registration_form(self, case_uuid: UUID) -> CaseRegistrationForm:
        """
        Return CaseRegistrationForm entity for the specified case_uuid
        """
        repo = cast(
            CaseRegistrationFormRepository,
            self.get_repository("registration_form"),
        )

        self.assert_user_info_present()
        case_registration_form = repo.get_case_by_uuid(
            case_uuid=case_uuid, user_info=self.user_info
        )

        if not case_registration_form:
            raise NotFound(
                "The registration form does not yet exist, try again in 5 seconds"
            )
        elif case_registration_form == "Token expired or not present":
            raise Forbidden(
                "The download token has expired after 15 minutes or you do not have persmission to download this document"
            )

        return case_registration_form

    @validate_call
    def download_registration_form(
        self, case_uuid: UUID
    ) -> RedirectResponse | NotFound:
        """
        Return CaseRegistrationForm entity for the specified case_uuid with download_link
        """
        repo = cast(
            CaseRegistrationFormRepository,
            self.get_repository("registration_form"),
        )

        case_registration_form = repo.get_case_by_uuid(
            case_uuid=case_uuid, user_info=self.user_info
        )

        if not case_registration_form:
            raise NotFound(
                "The registration form does not exist, check if it exists with get_registration_form"
            )

        download_url = repo.get_download_link(case_registration_form)

        return RedirectResponse(download_url)
