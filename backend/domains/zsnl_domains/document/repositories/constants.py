# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

PDF_MIMETYPE = "application/pdf"
IMAGE_MIMETYPES = ["image/png", "image/gif", "image/bmp", "image/jpeg"]
