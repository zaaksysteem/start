# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from .. import entities
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import allowed_cases_subquery

document_label_base_query = sql.select(
    schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid.label("uuid"),
    schema.ZaaktypeDocumentKenmerkenMap.name,
    schema.ZaaktypeDocumentKenmerkenMap.public_name,
    schema.ZaaktypeDocumentKenmerkenMap.magic_string,
    schema.BibliotheekKenmerk.uuid.label("attribute_id"),
)
document_label_query = document_label_base_query.select_from(
    sql.join(
        schema.ZaaktypeDocumentKenmerkenMap,
        schema.Case,
        schema.ZaaktypeDocumentKenmerkenMap.zaaktype_node_id
        == schema.Case.zaaktype_node_id,
    ).join(
        schema.BibliotheekKenmerk,
        schema.BibliotheekKenmerk.id
        == schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id,
        isouter=True,
    )
)


class DocumentLabelRepository(ZaaksysteemRepositoryBase):
    def get_document_labels_for_case(
        self, case_uuid: UUID, user_uuid: UUID
    ) -> list[entities.DocumentLabel]:
        """Get document labels for a case by case_uuid.

        :param uuid: case uuid
        :param user_uuid: user uuid
        """

        qry = document_label_query.where(
            sql.and_(
                schema.Case.uuid == case_uuid,
                allowed_cases_subquery(
                    db=self.session, user_uuid=user_uuid, permission="write"
                ),
            )
        )
        result = self.session.execute(qry).fetchall()
        return [self._entity_from_row(row) for row in result]

    def get_document_labels_for_case_magicstrings(
        self,
        case_uuid: UUID,
        user_uuid: UUID,
        magic_strings: list[str],
        is_system: bool = False,
    ) -> list[entities.DocumentLabel]:
        """Get document labels for a case by case_uuid + magic string list."""

        qry = document_label_query.where(
            sql.and_(
                schema.Case.uuid == case_uuid,
                schema.ZaaktypeDocumentKenmerkenMap.magic_string.in_(
                    magic_strings
                ),
            )
        )
        acl_query = (
            None
            if is_system
            else allowed_cases_subquery(
                db=self.session,
                user_uuid=user_uuid,
                permission="write",
            )
        )

        if acl_query is not None:
            qry = qry.where(acl_query)

        result = self.session.execute(qry).fetchall()

        return [self._entity_from_row(row) for row in result]

    def get_document_labels_by_uuid(self, uuids: list[UUID], case_uuid: UUID):
        """Get document labels by label UUIDs."""

        qry = document_label_query.where(
            sql.and_(
                sql.or_(
                    schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid.in_(
                        uuids
                    ),
                    schema.BibliotheekKenmerk.uuid.in_(uuids),
                ),
                schema.Case.uuid == case_uuid,
            )
        )
        result = self.session.execute(qry).fetchall()
        return [self._entity_from_row(row) for row in result]

    def _entity_from_row(self, row) -> entities.DocumentLabel:
        """Create DocumentLabel entity from database row.

        :param row: document_label database row.
        """

        return entities.DocumentLabel(
            uuid=row.uuid,
            entity_id=row.uuid,
            name=row.name,
            public_name=row.public_name,
            magic_string=row.magic_string,
            attribute_id=row.attribute_id,
            #
            _event_service=self.event_service,
        )
