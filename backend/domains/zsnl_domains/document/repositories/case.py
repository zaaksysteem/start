# SPDX-FileCopyrightText: xxllnc Zaakgericht B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import minty.exceptions
from ... import ZaaksysteemRepositoryBase
from ..entities.case import Case
from ..infrastructures.pdf_generator import (
    Address,
    EmptyDict,
    PDFGenerator,
    PDFGeneratorInfrastructure,
    PDFTemplateValues,
    PDFTemplateValuesAttributes,
    PDFTemplateValuesMeta,
    PDFTemplateValuesPreparedRequest,
)
from datetime import datetime
from minty.cqrs import Event, UserInfo
from redis import Redis
from secrets import token_hex
from sqlalchemy import sql
from sqlalchemy.dialects.postgresql import JSON, aggregate_order_by
from typing import Any, Final, cast
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.document.entities.casetype import Attribute, Casetype
from zsnl_domains.shared.util import HTMLTagStripper

ADDRESS_TYPES: Final[set[str]] = {
    "address_v2",
    "bag_adres",
    "bag_adressen",
    "bag_openbareruimte",
    "bag_openbareruimtes",
    "bag_straat_adres",
    "bag_straat_adressen",
}
NATUURLIJK_PERSOON_ATTRIBUTES: Final[list[str]] = [
    "name",
    "street",
    "streetnumber",
    "streetletter",
    "streetnumberaddition",
    "zipcode",
    "city",
    "country",
]
BEDRIJF_ATTRIBUTES: Final[list[str]] = [
    "name",
    "coc_number",
    "street",
    "streetnumber",
    "streetletter",
    "streetnumberaddition",
    "zipcode",
    "city",
    "country",
]
REQUESTOR_ATTRIBUTE_TRANSLATIONS: Final[dict[str, str]] = {
    "name": "Naam",
    "country": "Land",
    "street": "Straat",
    "streetnumber": "Huisnummer",
    "streetletter": "Huisletter",
    "streetnumberaddition": "Huisnummertoevoeging",
    "zipcode": "Postcode",
    "city": "Plaats",
    "coc_number": "KvK nummer",
}
REGISTRATION_FORM_TOKEN_VALIDITY_SECONDS: Final = 300
REGISTRATION_FORM_MAGIC_STRING: Final = "pdf_webformulier"

_bk = schema.BibliotheekKenmerk
_c = schema.Case
_zk = schema.ZaaktypeKenmerk
_zn = schema.ZaaktypeNode
_zs = schema.ZaaktypeStatus


CASE_QUERY = sql.select(
    _c.uuid,
    _c.id,
    sql.select(
        sql.func.json_build_object(
            "label",
            _zn.titel,
            "needs_registration_form",
            sql.case(
                (
                    sql.cast(_zn.properties, JSON)[
                        "pdf_registration_form"
                    ].astext
                    == "1",
                    True,
                ),
                else_=False,
            ),
            "custom_fields",
            sql.select(
                sql.func.json_agg(
                    aggregate_order_by(
                        sql.func.json_build_object(
                            "phase_number",
                            _zs.status,
                            "magic_string",
                            _bk.magic_string,
                            "value_type",
                            _bk.value_type,
                            "is_internal",
                            _zk.is_systeemkenmerk,
                            "label",
                            sql.func.coalesce(
                                sql.func.nullif(_zk.label, ""),
                                sql.func.nullif(_bk.naam_public, ""),
                                _bk.naam,
                            ),
                        ),
                        _zs.status,
                        _zk.id,
                    )
                )
            )
            .select_from(
                sql.join(
                    _zk, _bk, _zk.bibliotheek_kenmerken_id == _bk.id
                ).join(_zs, _zk.zaak_status_id == _zs.id)
            )
            .where(_zk.zaaktype_node_id == _zn.id)
            .scalar_subquery(),
        )
    )
    .select_from(_zn)
    .where(_zn.id == _c.zaaktype_node_id)
    .scalar_subquery()
    .label("case_type"),
).select_from(schema.Case)

ATTRIBUTES_JOIN = (
    sql.join(_zk, _zn, _zk.zaaktype_node_id == _zn.id)
    .join(_bk, _zk.bibliotheek_kenmerken_id == _bk.id, isouter=True)
    .join(_zs, _zs.id == _zk.zaak_status_id)
)


class CaseRepository(ZaaksysteemRepositoryBase):
    _for_entity: str = "Case"
    _events_to_calls: dict[str, str] = {
        "RegistrationFormRequested": "_create_registration_form"
    }
    ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE.update(
        {
            "pdf_generator": PDFGeneratorInfrastructure(),
        }
    )

    def get_case_by_uuid_unsafe(self, case_uuid: UUID) -> Case:
        """
        Retrieve a Case entity for the document domain.

        This call does NOT do permission checks on the case.
        """

        case_query = CASE_QUERY.where(_c.uuid == case_uuid)

        row = self.session.execute(case_query).fetchone()

        if not row:
            raise minty.exceptions.NotFound(
                f"Case {case_uuid} not found", "case/not_found"
            )

        return Case(
            entity_id=row.uuid,
            uuid=row.uuid,
            id=row.id,
            case_type=row.case_type,
            # Inject the event service, so the entity can make its own events:
            _event_service=self.event_service,
        )

    def get_casetype_by_version_uuid_unsafe(
        self, casetype_version_uuid: UUID
    ) -> None | Casetype:
        attributes_select_query = (
            sql.select(_zk.id, _zk.label, _zk.is_group, _bk.magic_string)
            .select_from(ATTRIBUTES_JOIN)
            .where(
                sql.and_(_zn.uuid == casetype_version_uuid, _zs.status == 1)
            )
            .order_by(_zk.id.asc())
        )

        rows = self.session.execute(attributes_select_query).fetchall()

        return Casetype(
            entity_id=casetype_version_uuid,
            uuid=casetype_version_uuid,
            attributes=[
                Attribute(
                    id=row.id,
                    label=row.label if row.label else "",
                    is_group=row.is_group,
                    magic_string=row.magic_string if row.magic_string else "",
                )
                for row in rows
            ],
        )

    def _build_requestor_info(
        self, requestor_info: dict
    ) -> tuple[list[PDFTemplateValuesAttributes], int]:
        requestor_type = requestor_info.get("type", None)
        has_foreign_address = requestor_info.get("address", False)
        if requestor_type == "natuurlijk_persoon":
            if has_foreign_address:
                pdf_template_values_attributes: list[
                    PDFTemplateValuesAttributes
                ] = [
                    {
                        "address": [{}],
                        "name": "Naam",
                        "sequence": 1,
                        "type": "text",
                        "value": requestor_info.get("name", ""),
                    },
                    {
                        "address": [{}],
                        "name": "Land",
                        "sequence": 2,
                        "type": "text",
                        "value": requestor_info.get("country", ""),
                    },
                    {
                        "address": [{}],
                        "name": "Adres",
                        "sequence": 3,
                        "type": "text",
                        "value": requestor_info.get("address", ""),
                    },
                ]
            else:
                pdf_template_values_attributes: list[
                    PDFTemplateValuesAttributes
                ] = [
                    {
                        "address": [{}],
                        "name": REQUESTOR_ATTRIBUTE_TRANSLATIONS.get(
                            attribute, ""
                        ),
                        "sequence": sequence,
                        "type": "text",
                        "value": requestor_info.get(attribute, "")
                        if requestor_info.get(attribute, None)
                        else "",
                    }
                    for sequence, attribute in enumerate(
                        NATUURLIJK_PERSOON_ATTRIBUTES, start=1
                    )
                ]
        elif requestor_type == "bedrijf":
            if has_foreign_address:
                pdf_template_values_attributes: list[
                    PDFTemplateValuesAttributes
                ] = [
                    {
                        "address": [{}],
                        "name": "Naam",
                        "sequence": 1,
                        "type": "text",
                        "value": requestor_info.get("name", ""),
                    },
                    {
                        "address": [{}],
                        "name": "Land",
                        "sequence": 2,
                        "type": "text",
                        "value": requestor_info.get("country", ""),
                    },
                    {
                        "address": [{}],
                        "name": "Adres",
                        "sequence": 3,
                        "type": "text",
                        "value": requestor_info.get("address", ""),
                    },
                    {
                        "address": [{}],
                        "name": "KvK nummer",
                        "sequence": 4,
                        "type": "text",
                        "value": requestor_info.get("coc_number", ""),
                    },
                ]
            else:
                pdf_template_values_attributes: list[
                    PDFTemplateValuesAttributes
                ] = [
                    {
                        "address": [{}],
                        "name": REQUESTOR_ATTRIBUTE_TRANSLATIONS.get(
                            attribute, ""
                        ),
                        "sequence": sequence,
                        "type": "text",
                        "value": requestor_info.get(attribute, "")
                        if requestor_info.get(attribute, None)
                        else "",
                    }
                    for sequence, attribute in enumerate(
                        BEDRIJF_ATTRIBUTES, start=1
                    )
                ]
        last_attribute_sequence = pdf_template_values_attributes[-1][
            "sequence"
        ]
        return pdf_template_values_attributes, last_attribute_sequence

    def _build_attribute(
        self,
        name: str,
        sequence_nr: int,
        value_type: str,
        value: Any,
    ) -> PDFTemplateValuesAttributes:
        return {
            "name": name,
            "sequence": sequence_nr,
            "type": "address" if value_type in ADDRESS_TYPES else value_type,
            "address": _format_as_address(
                type=value_type,
                value=value,
            ),
            "value": _format_value(
                value,
                value_type,
            ),
        }

    def _build_attributes(
        self,
        custom_field_values: dict,
        included_custom_fields: list[dict[str, str]],
        starting_index: int,
        casetype_with_attributes: Casetype,
    ) -> list[PDFTemplateValuesAttributes]:
        attribute_list: list[PDFTemplateValuesAttributes] = []
        current_parent = casetype_with_attributes.attribute_match_dict.get(
            included_custom_fields[0]["magic_string"]
        )
        sequence_nr = starting_index + 1
        first_group_attribute = self._build_attribute(
            current_parent,
            sequence_nr,
            "header",
            "",
        )
        attribute_list.append(first_group_attribute)
        sequence_nr += 1
        for field in included_custom_fields:
            field_parent = casetype_with_attributes.attribute_match_dict.get(
                field["magic_string"]
            )
            if field_parent != current_parent:
                current_parent = field_parent
                group_attribute = self._build_attribute(
                    field_parent,
                    sequence_nr,
                    "header",
                    "",
                )
                attribute_list.append(group_attribute)
                sequence_nr += 1
            attribute = self._build_attribute(
                field["label"],
                sequence_nr,
                field["value_type"],
                custom_field_values[field["magic_string"]],
            )
            attribute_list.append(attribute)
            sequence_nr += 1
        return attribute_list

    def _build_meta(
        self,
        case_number: int,
        casetype_name: str,
        hostname: str,
        date: str,
    ) -> PDFTemplateValuesMeta:
        return {
            "case_number": case_number,
            "casetype_name": casetype_name,
            "customer": hostname,
            "date": date,
        }

    def _build_prepared_request(
        self, hostname: str, callback_api_token: str
    ) -> PDFTemplateValuesPreparedRequest:
        return {
            "token": callback_api_token,
            "upload_url": f"https://{hostname}/api/v2/document/create_document_with_token",
        }

    def _build_template_values(
        self,
        requestor_info: dict,
        custom_field_values: dict,
        included_custom_fields: list[dict[str, str]],
        case_number: int,
        casetype_name: str,
        hostname: str,
        date: str,
        callback_api_token: str,
        casetype_with_attributes: Casetype,
    ) -> PDFTemplateValues:
        starting_index = 1
        requestor_attributes: list[PDFTemplateValuesAttributes] = []
        user_is_anonymous = (
            True if requestor_info.get("is_anonymous") == 1 else False
        )
        if not user_is_anonymous:
            requestor_attributes, starting_index = self._build_requestor_info(
                requestor_info
            )
        attributes = self._build_attributes(
            custom_field_values,
            included_custom_fields,
            starting_index,
            casetype_with_attributes,
        )
        all_attributes: list[PDFTemplateValuesAttributes] = []
        if requestor_attributes:
            all_attributes.extend(requestor_attributes)
        all_attributes.extend(attributes)
        return {
            "attributes": all_attributes,
            "meta": self._build_meta(
                case_number, casetype_name, hostname, date
            ),
            "prepared_request": self._build_prepared_request(
                hostname, callback_api_token
            ),
        }

    def _create_registration_form(
        self,
        event: Event,
        user_info: UserInfo | None = None,
        dry_run: bool = False,
    ):
        assert user_info

        case_type: dict[str, Any] = event.entity_data["case_type"]
        case_id = event.entity_data["id"]
        form_date = event.created_date.strftime("%Y-%m-%d")

        changes = event.format_changes()
        field_values: dict[str, Any] = changes.get("field_values", {})
        requestor_info = field_values.get("requestor", {})
        custom_field_values = field_values.get("attributes", {})
        hostname = field_values["hostname"]
        casetype_with_attributes = self.get_casetype_by_version_uuid_unsafe(
            field_values.get("casetype_version_uuid", "")
        )
        if not casetype_with_attributes.attributes:
            raise minty.exceptions.NotFound(
                f"Did not get a Casetype entity back from the database, cannot create the registration form for case {case_id}",
                "casetype/not_found",
            )

        pdf_generator = cast(
            PDFGenerator,
            self._get_infrastructure("pdf_generator"),
        )

        redis = cast(
            Redis,
            self._get_infrastructure("redis"),
        )

        configuration = self.infrastructure_factory.get_config(self.context)
        instance_identifier = configuration["instance_uuid"]

        # Store a single-use token in Redis, so we know it's the PDF generator
        # sending us the PDF.
        callback_api_token = token_hex()
        case_uuid_string = str(field_values["case_uuid"])

        # inject "system" privileges for one-use authentication token so upload
        # from pdf_generator always works, currently "system" role is only used
        # by pdf creation feature

        user_info.permissions["system"] = True

        redis.set(
            f"{instance_identifier}:tokens:{callback_api_token}",
            value=json.dumps(
                {
                    "case_uuid": case_uuid_string,
                    "user_info": user_info.to_dict(),
                    "type": "registration_form",
                    "magic_string": REGISTRATION_FORM_MAGIC_STRING,
                    "filename": f"{case_id}-{form_date}-{case_type['label']}.pdf",
                },
            ),
            ex=REGISTRATION_FORM_TOKEN_VALIDITY_SECONDS,
        )

        # Store case_uuid as an authentication token that expires after 15 minutes

        redis.set(
            f"{instance_identifier}:tokens:pdf_form:{case_uuid_string}",
            value=case_uuid_string,
            ex=REGISTRATION_FORM_TOKEN_VALIDITY_SECONDS,
        )

        included_custom_fields = [
            custom_field
            for custom_field in case_type["custom_fields"]
            if (
                custom_field["phase_number"] == 1
                and (not custom_field["is_internal"])
                and (custom_field["magic_string"] in custom_field_values)
            )
        ]

        pdf_template_values = self._build_template_values(
            requestor_info,
            custom_field_values,
            included_custom_fields,
            case_id,
            case_type["label"],
            hostname,
            event.created_date.isoformat(timespec="milliseconds"),
            callback_api_token,
            casetype_with_attributes,
        )
        pdf_generator.generate_pdf(pdf_template_values)

        return


def _format_as_address(
    type: str, value: Any
) -> list[Address] | list[EmptyDict]:
    if type not in ADDRESS_TYPES or value is None:
        return [EmptyDict()]

    address_list: list[Address] | list[EmptyDict] = []

    if not isinstance(value, list):
        value = [value]

    for _value in value:
        if type == "address_v2":
            address = _value.get("address", {})
            human_value = address.get("full", "")

            bag = _value.get("bag", {})
            bag_id = bag.get("id", "")

            try:
                coords = reversed(
                    _value["geojson"]["features"][0]["geometry"]["coordinates"]
                )
                coordinates = ",".join(map(str, coords))
            except (KeyError, IndexError, AttributeError):
                coordinates = ""
        elif type.startswith("bag_"):
            human_value = _value.get("human_identifier", "")
            bag_id = _value.get("bag_id", "")

            address_data = _value.get("address_data", {})
            coordinates = address_data.get("gps_lat_lon", "")
        else:
            raise AssertionError("Unknown address type")  # pragma: no cover
        address_list.append(
            Address(
                human_value=human_value, bag_id=bag_id, coordinates=coordinates
            )
        )

    return address_list


def _format_value(attribute_value: str | list, attribute_type: str) -> str:
    if attribute_type in ADDRESS_TYPES:
        return ""
    if attribute_type == "richtext" and attribute_value:
        parser = HTMLTagStripper(attribute_value, rich_text_field_value=True)
        return parser.stripped_text.strip()
    elif attribute_type == "file" and attribute_value:
        file: dict
        file_names = []
        for file in attribute_value:
            file_name = file.get("filename", "")
            file_names.append(file_name)
        return ", ".join(file_names)
    elif attribute_type == "date" and attribute_value:
        datetime_object = datetime.strptime(
            attribute_value, "%Y-%m-%dT%H:%M:%S"
        )
        return datetime_object.strftime("%d-%m-%Y")
    return attribute_value if attribute_value else ""
