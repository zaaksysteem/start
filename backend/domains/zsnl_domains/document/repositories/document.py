# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import hashlib
import json
import minty.cqrs
import minty.repository
import os
import secrets
import sqlalchemy.exc as sql_exc
import tempfile
from ... import ZaaksysteemRepositoryBase
from ...case_management.repositories import case_acl, pip_acl
from ...shared.util import LANGUAGE_DETECTOR, LANGUAGE_MAP
from ..entities.document import (
    THUMBNAILABLE_MIMETYPES_DIRECT,
    Document,
    DocumentStatus,
)
from ..infrastructures import (
    ConverterInfrastructure,
    ConverterWrapper,
    TikaInfrastructure,
    TikaWrapper,
    WopiInfrastructure,
    ZohoInfrastructure,
)
from . import event_placeholder
from .shared import FileStoreSource, clean_filename_part
from collections.abc import Sequence
from datetime import UTC, datetime, timedelta, timezone
from minty import exceptions
from minty.cqrs.events import Event
from minty_infra_misc import RedisInfrastructure
from minty_infra_storage import S3Infrastructure, s3
from sqlalchemy import JSON, func, sql
from sqlalchemy.dialects import postgresql
from typing import Final, cast
from typing_extensions import TypedDict
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.document.entities import shared
from zsnl_domains.shared.repositories.case_acl import (
    allowed_cases_subquery,
    user_allowed_cases_subquery,
)

FILESTORE_INFRA_NAME: Final = "s3"

# These "trust level" settings in file_metadata cause the "confidential" flag
# to be set on the document.
CONFIDENTIAL_TRUST_LEVELS: Final = {
    "Vertrouwelijk",
    "Confidentieel",
    "Geheim",
    "Zeer geheim",
}

# Hardcoded thumbnail size
THUMBNAIL_MAX_WIDTH: Final = 398
THUMBNAIL_MAX_HEIGHT: Final = 565

DOCUMENT_STATUS_FROM_DB_MAPPING: Final[dict[str, DocumentStatus]] = {
    "original": "Origineel",
    "copy": "Kopie",
    "replaced": "Vervangen",
    "converted": "Geconverteerd",
}
DOCUMENT_STATUS_TO_DB_MAPPING: Final[dict[DocumentStatus, str]] = {
    v: k for k, v in DOCUMENT_STATUS_FROM_DB_MAPPING.items()
}


class LockDescriptor(TypedDict):
    lock_timestamp: datetime
    lock_subject_id: UUID
    lock_subject_name: str
    shared: bool


def _document_query_base_unsafe(acl_clause: list) -> sql.Select:
    preview_derivative = sql.alias(schema.FileDerivative)
    preview_store = sql.alias(schema.Filestore)
    preview_filter = sql.and_(
        preview_derivative.c.file_id == schema.File.id,
        preview_derivative.c.type == "pdf",
        schema.Filestore.mimetype != "application/pdf",
        preview_store.c.id == preview_derivative.c.filestore_id,
    )

    preview_uuid_query = (
        sql.select(preview_store.c.uuid).where(preview_filter).limit(1)
    )
    preview_mimetype_query = (
        sql.select(preview_store.c.mimetype).where(preview_filter).limit(1)
    )
    preview_storage_location_query = (
        sql.select(preview_store.c.storage_location)
        .where(preview_filter)
        .limit(1)
    )

    thumbnail_derivative = sql.alias(schema.FileDerivative)
    thumbnail_store = sql.alias(schema.Filestore)
    thumbnail_filter = sql.and_(
        thumbnail_derivative.c.file_id == schema.File.id,
        thumbnail_derivative.c.type == "thumbnail",
        thumbnail_store.c.id == thumbnail_derivative.c.filestore_id,
    )
    thumbnail_uuid_query = (
        sql.select(thumbnail_store.c.uuid).where(thumbnail_filter).limit(1)
    )
    thumbnail_mimetype_query = (
        sql.select(thumbnail_store.c.mimetype).where(thumbnail_filter).limit(1)
    )
    thumbnail_storage_location_query = (
        sql.select(thumbnail_store.c.storage_location)
        .where(thumbnail_filter)
        .limit(1)
    )

    document_join = (
        sql.join(
            schema.File,
            schema.Filestore,
            schema.File.filestore_id == schema.Filestore.id,
        )
        .join(
            schema.Directory,
            schema.File.directory_id == schema.Directory.id,
            isouter=True,
        )
        .join(
            schema.Case,
            sql.and_(schema.File.case_id == schema.Case.id),
            isouter=True,
        )
        .join(
            schema.FileMetaData,
            schema.FileMetaData.id == schema.File.metadata_id,
            isouter=True,
        )
        .join(
            schema.Group,
            schema.Group.id == schema.File.intake_group_id,
            isouter=True,
        )
        .join(
            schema.Role,
            schema.Role.id == schema.File.intake_role_id,
            isouter=True,
        )
    )

    document_labels_query = func.array(
        sql.select(
            func.json_build_object(
                "uuid",
                schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid,
                "name",
                schema.ZaaktypeDocumentKenmerkenMap.name,
                "public_name",
                schema.ZaaktypeDocumentKenmerkenMap.public_name,
                "magic_string",
                schema.ZaaktypeDocumentKenmerkenMap.magic_string,
            )
        )
        .select_from(
            sql.join(
                schema.ZaaktypeDocumentKenmerkenMap,
                sql.join(
                    schema.FileCaseDocument,
                    schema.Case,
                    schema.Case.id == schema.FileCaseDocument.case_id,
                ),
                sql.and_(
                    schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                    == schema.FileCaseDocument.bibliotheek_kenmerken_id,
                    schema.ZaaktypeDocumentKenmerkenMap.zaaktype_node_id
                    == schema.Case.zaaktype_node_id,
                ),
            )
        )
        .where(
            sql.and_(
                schema.FileCaseDocument.file_id == schema.File.id,
                schema.File.case_id == schema.Case.id,
            )
        )
        .label("document_labels_subquery")
    )

    document_query = (
        sql.select(
            schema.File.uuid.label("document_uuid"),
            schema.Filestore.uuid.label("store_uuid"),
            schema.Filestore.mimetype,
            schema.Filestore.size,
            schema.Filestore.storage_location,
            schema.Filestore.md5,
            schema.Filestore.is_archivable,
            schema.Filestore.virus_scan_status,
            schema.Directory.uuid.label("directory_uuid"),
            schema.Case.uuid.label("case_uuid"),
            schema.Case.id.label("case_display_number"),
            schema.File.name.label("filename"),
            schema.File.extension,
            schema.File.accepted,
            schema.File.version,
            schema.File.lock_subject_id.label("lock_user_uuid"),
            schema.File.lock_subject_name.label("lock_user_display_name"),
            schema.File.lock_timestamp.label("lock_timestamp"),
            schema.File.shared.label("lock_shared"),
            schema.File.publish_pip.label("publish_pip"),
            schema.File.publish_website.label("publish_website"),
            schema.Group.uuid.label("intake_group_uuid"),
            schema.Role.uuid.label("intake_role_uuid"),
            sql.func.get_subject_by_legacy_id(schema.File.intake_owner).label(
                "intake_owner_uuid"
            ),
            sql.func.get_subject_by_legacy_id(schema.File.created_by).label(
                "creator_uuid"
            ),
            sql.func.get_subject_display_name_by_uuid(
                sql.func.get_subject_by_legacy_id(schema.File.created_by)
            ).label("creator_displayname"),
            schema.File.date_modified,
            schema.File.id,
            preview_uuid_query.label("preview_uuid"),
            preview_storage_location_query.label("preview_storage_location"),
            preview_mimetype_query.label("preview_mimetype"),
            schema.File.document_status,
            schema.FileMetaData.description,
            schema.FileMetaData.origin,
            schema.FileMetaData.origin_date,
            schema.FileMetaData.trust_level.label("confidentiality"),
            schema.FileMetaData.document_category,
            schema.FileMetaData.document_source,
            schema.FileMetaData.pronom_format,
            schema.FileMetaData.appearance,
            schema.FileMetaData.structure,
            thumbnail_uuid_query.label("thumbnail_uuid"),
            thumbnail_mimetype_query.label("thumbnail_mimetype"),
            thumbnail_storage_location_query.label(
                "thumbnail_storage_location"
            ),
            document_labels_query.label("labels"),
            sql.case(
                (schema.File.search_index.is_(None), False), else_=True
            ).label("has_search_index"),
        )
        .select_from(document_join)
        .where(
            sql.and_(
                schema.File.active_version.is_(True),
                schema.File.destroyed.is_(False),
                *acl_clause,
            )
        )
    )

    return document_query


def _document_query_base(user_info: minty.cqrs.UserInfo) -> sql.Select:
    if user_info.permissions.get("pip_user", False):
        # If a user is a PIP user, the acl_clause must not be added
        # to the get_document_by_uuid query because PIP users don't have ACL
        acl_clause = [
            pip_acl.allowed_cases_subquery(user_uuid=user_info.user_uuid)
        ]
    elif user_info.permissions.get("admin", False):
        # Admins can see everything without a permission check
        acl_clause = []
    else:
        document_confidential_join = (
            sql.join(
                schema.Case,
                schema.ZaaktypeAuthorisation,
                sql.and_(
                    schema.ZaaktypeAuthorisation.zaaktype_id
                    == schema.Case.zaaktype_id,
                    schema.ZaaktypeAuthorisation.confidential.is_(True),
                    schema.ZaaktypeAuthorisation.recht == "zaak_read",
                ),
            )
            .join(
                schema.SubjectPositionMatrix,
                sql.and_(
                    schema.SubjectPositionMatrix.role_id
                    == schema.ZaaktypeAuthorisation.role_id,
                    schema.SubjectPositionMatrix.group_id
                    == schema.ZaaktypeAuthorisation.ou_id,
                ),
            )
            .join(
                schema.Subject,
                schema.Subject.id == schema.SubjectPositionMatrix.subject_id,
            )
        )

        document_confidential_access_check = (
            sql.select(1)
            .select_from(document_confidential_join)
            .where(
                sql.and_(
                    schema.Case.id == schema.File.case_id,
                    schema.Subject.uuid == user_info.user_uuid,
                )
            )
        )

        acl_clause = [
            sql.or_(
                schema.File.case_id.is_(
                    None
                ),  # Allow access if file.case_id is not set
                sql.and_(
                    user_allowed_cases_subquery(
                        user_info=user_info,
                        permission="read",
                    ),
                    sql.or_(
                        # Document is part of a case you can access, and document
                        # is not confidential: you have access
                        schema.File.confidential.is_(False),
                        # Document is marked "confidential"; do extra checks to
                        # see if you _would have_ access to the case if it was
                        # marked "confidential", and allow only then.
                        sql.and_(
                            schema.File.confidential.is_(True),
                            sql.exists(document_confidential_access_check),
                        ),
                    ),
                ),
            )
        ]

    return _document_query_base_unsafe(acl_clause)


class DocumentRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Document"
    _events_to_calls = {
        "DocumentAccepted": "_accept_document",
        "DocumentAddedToCase": "_add_document_to_case",
        "DocumentAssignedToRole": "_assign_document_to_role",
        "DocumentAssignedToUser": "_assign_document_to_user",
        "DocumentAssignmentRejected": "_reject_document_assignment",
        "DocumentCreated": "_create_document",
        "DocumentDeleted": "_delete_document",
        "DocumentFromAttachmentCreated": "_create_document_from_attachment",
        "DocumentMoved": "_move_document",
        "DocumentUpdated": "_update_document",
        "LabelsApplied": "_set_labels",
        "LabelsRemoved": "_set_labels",
        "LockAcquired": "_acquire_lock",
        "LockExtended": "_extend_lock",
        "LockReleased": "_release_lock",
        "PreviewCreated": "_create_preview",
        "SearchIndexSet": "_set_search_terms",
        "SearchIndexSetDelayed": "_set_search_terms_delayed",
        "ThumbnailCreated": "_create_thumbnail",
    }

    ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE.update(
        {
            "converter": ConverterInfrastructure(),
            "redis": RedisInfrastructure(),
            "s3": S3Infrastructure(),
            "tika": TikaInfrastructure(),
            "wopi": WopiInfrastructure(),
            "zoho": ZohoInfrastructure(),
        }
    )

    def _get_case_id_assignee_uuid(
        self, case_uuid, user_uuid, is_pip_user=False, is_system=False
    ):
        case_query = sql.select(
            schema.Case.id, schema.Case.status, schema.Case.behandelaar_gm_id
        ).where(schema.Case.uuid == case_uuid)
        if is_system:
            acl_query = None
            # do nothing, runs case_query without acl check
        elif is_pip_user:
            acl_query = pip_acl.allowed_cases_subquery(user_uuid=user_uuid)
        else:
            acl_query = allowed_cases_subquery(
                db=self.session, user_uuid=user_uuid, permission="read"
            )

        if acl_query is not None:
            case_query = case_query.where(acl_query)

        case = self.session.execute(case_query).fetchone()

        if not case:
            raise exceptions.NotFound(
                f"Case with uuid: '{case_uuid}' not found.", "case/not_found"
            )

        if case.behandelaar_gm_id is None:
            return {
                "case_id": case.id,
                "case_status": case.status,
                "assignee_uuid": None,
            }

        subject_result = case_acl.get_subject_by_id(
            case.behandelaar_gm_id, self.session
        )

        if subject_result is None:
            raise exceptions.NotFound(
                f"Subject with id: '{case.behandelaar_gm_id}' not found.",
                "behandelaar/not_found",
            )

        return {
            "case_id": case.id,
            "case_status": case.status,
            "assignee_uuid": subject_result.uuid,
        }

    def _get_directory_id(self, directory_uuid):
        if directory_uuid is None:
            return None

        stmt = sql.select(schema.Directory.id).where(
            schema.Directory.uuid == directory_uuid
        )

        directory_result = self.session.execute(stmt).fetchone()

        if directory_result is None:
            raise exceptions.NotFound(
                f"Directory with uuid: '{directory_uuid}' not found.",
                "directory/not_found",
            )
        return directory_result.id

    def _get_legacy_contact_identifier(self, user_uuid):
        select_employee = sql.select(
            schema.Subject.id,
            sql.expression.literal("medewerker").label("subject_type"),
        ).where(
            sql.and_(
                schema.Subject.uuid == user_uuid,
                schema.Subject.subject_type == "employee",
            )
        )
        select_person = sql.select(
            schema.NatuurlijkPersoon.id,
            sql.expression.literal("natuurlijk_persoon").label("subject_type"),
        ).where(schema.NatuurlijkPersoon.uuid == user_uuid)

        select_organization = sql.select(
            schema.Bedrijf.id,
            sql.expression.literal("bedrijf").label("subject_type"),
        ).where(schema.Bedrijf.uuid == user_uuid)

        user_result = self.session.execute(
            sql.union_all(select_employee, select_person, select_organization)
        ).fetchone()

        if not user_result:
            raise exceptions.NotFound(
                f"Subject with uuid '{user_uuid}' not found",
                "subject/not_found",
            )

        return "-".join(
            ["betrokkene", user_result.subject_type, str(user_result.id)]
        )

    def _get_root_file_id(self, name: str, extension: str, case_id: int):
        # First, retrieve the current version of a potential "root file" by name
        # - Name & extension & case_id match
        # - is current version
        # - is not deleted
        stmt = sql.select(
            schema.File.id,
            schema.File.root_file_id,
            schema.File.uuid,
        ).where(
            sql.and_(
                schema.File.accepted.is_(True),
                schema.File.date_deleted.is_(None),
                schema.File.name == name,
                schema.File.extension == extension,
                schema.File.case_id == case_id,
                schema.File.active_version.is_(True),
            )
        )
        file_result = self.session.execute(stmt).one_or_none()

        # If a result is found, use its root_file_id, or id if it's a root itself (version 1)
        if file_result:
            return (
                file_result.root_file_id or file_result.id,
                file_result.uuid,
            )

        return (None, None)

    def create_document_v2(
        self,
        user_info: minty.cqrs.UserInfo,
        document_uuid: UUID,
        local_filename: str,
        name: str,
        case_uuid: UUID,
        auto_accept: bool = False,
    ) -> Document:
        """
        Create document entity
        """

        filestore_infra = cast(
            s3.S3Wrapper, self._get_infrastructure(FILESTORE_INFRA_NAME)
        )

        store_uuid = uuid4()

        with open(local_filename, "rb") as fh:
            upload_result = filestore_infra.upload(
                file_handle=fh, uuid=store_uuid
            )

        document = self.create_document(
            document_uuid=document_uuid,
            filename=name,
            case_uuid=case_uuid,
            store_uuid=store_uuid,
            mimetype=upload_result["mime_type"],
            size=upload_result["size"],
            storage_location=upload_result["storage_location"],
            md5=upload_result["md5"],
            editor_update=False,
            directory_uuid=None,
            is_pip_user=user_info.permissions.get("pip_user", False),
            is_system=user_info.permissions.get("system", False),
            skip_intake=False,
            auto_accept=auto_accept,
        )
        return document

    def create_document(
        self,
        document_uuid: UUID,
        filename: str,
        store_uuid: UUID,
        case_uuid: UUID,
        mimetype: str,
        size: int,
        storage_location: str,
        md5: str,
        editor_update: bool | None = False,
        directory_uuid: UUID | None = None,
        is_pip_user: bool = False,
        is_system: bool = False,
        skip_intake: bool = False,
        auto_accept: bool = False,
        accept_extended_mimetypes: bool = False,
        replaces=None,
    ) -> Document:
        entity_uuid = document_uuid

        if auto_accept and not case_uuid:
            raise exceptions.Conflict(
                "Cannot auto-accept documents without a case",
                "create_document/auto_accept/no_case",
            )
        if replaces:
            entity_uuid = uuid4()

        document = Document(document_uuid=entity_uuid)
        document.event_service = self.event_service
        user_uuid = self.event_service.user_uuid

        (basename, extension) = os.path.splitext(filename)
        basename = clean_filename_part(basename)
        extension = clean_filename_part(extension)

        accepted = False
        previous_version_uuid = None
        if case_uuid is not None:
            case_data = self._get_case_id_assignee_uuid(
                case_uuid=case_uuid,
                user_uuid=user_uuid,
                is_pip_user=is_pip_user,
                is_system=is_system,
            )

            (root_file_id, root_file_uuid) = self._get_root_file_id(
                basename,
                extension,
                case_data["case_id"],
            )

            if auto_accept:
                # When auto_accept is true, the document_uuid is the "target"
                # document uuid
                previous_version_uuid = document_uuid

            accepted = self._is_file_accepted(
                root_file_id=root_file_id,
                case_data=case_data,
                user_uuid=user_uuid,
                case_uuid=case_uuid,
            )
            self.logger.debug(f"{accepted=}")

        new_version_needed = True
        virus_scan_status = "pending"
        if editor_update:
            virus_scan_status = "ok"
            new_version_needed = self._document_update_from_editor(
                store_uuid=store_uuid,
                basename=basename,
                extension=extension,
                mimetype=mimetype,
                size=size,
                storage_location=storage_location,
                md5=md5,
                is_archivable=shared.is_archivable(extension, mimetype),
                document_uuid=document_uuid,
                virus_scan_status=virus_scan_status,
            )

        if new_version_needed:
            creator_type = "pip" if is_pip_user else "employee"

            document.create(
                basename=basename,
                extension=extension,
                accepted=False,
                store_uuid=store_uuid,
                directory_uuid=directory_uuid,
                case_uuid=case_uuid,
                mimetype=mimetype,
                size=size,
                storage_location=storage_location,
                md5=md5,
                creator_type=creator_type,
                skip_intake=skip_intake,
                auto_accept=auto_accept,
                virus_scan_status=virus_scan_status,
                accept_extended_mimetypes=accept_extended_mimetypes,
            )

            if previous_version_uuid:
                document.accept_document(as_version_of=previous_version_uuid)
            elif accepted:
                document.accept_document(as_version_of=None)

        return document

    def create_document_from_attachment(self, attachment_uuid: UUID):
        document = Document(document_uuid=uuid4())
        document.event_service = self.event_service
        document.create_document_from_attachment(
            attachment_uuid=attachment_uuid
        )
        return document

    def get_documents_by_file_uuid_unsafe(
        self, file_uuid: UUID
    ) -> list[Document]:
        """
        Retrieve all `Document`s that are based on the `File` with the
        specified UUID.

        !!! Only use this for internal processes that always need access to
            the document, like "generate thumbnail" etc.
            Users of this function are not allowed to directly expose
            sensitive information to end-users.
        """
        document_query = _document_query_base_unsafe([]).where(
            schema.Filestore.uuid == file_uuid
        )

        documents = [doc for doc in self.session.execute(document_query)]

        return [
            self._transform_to_entity(document_row=document)
            for document in documents
        ]

    def get_document_by_uuid(
        self,
        document_uuid: UUID,
        user_info: minty.cqrs.UserInfo,
        integrity_check: bool = False,
    ):
        """
        Get a document by UUID
        """
        document_query = _document_query_base(user_info).where(
            schema.File.uuid == document_uuid
        )

        document_row = self.session.execute(document_query).fetchone()

        if not document_row:
            raise exceptions.NotFound(
                f"No document found with uuid={document_uuid}",
                "document/not_found",
            )

        integrity_check_successful = None
        if integrity_check:
            integrity_check_successful = self._check_file_integrity(
                file_uuid=document_row.store_uuid,
                storage_location=document_row.storage_location,
                md5=document_row.md5,
            )

        return self._transform_to_entity(
            document_row, integrity_check_successful
        )

    def get_document_by_uuid_unsafe(self, document_uuid: UUID):
        """
        Get a document by UUID, without ACL checks

        !!! Only use this for internal processes that always need access to
            the document, like "generate thumbnail" etc.
        """
        document_query = _document_query_base_unsafe([]).where(
            schema.File.uuid == document_uuid
        )

        document_row = self.session.execute(document_query).fetchone()

        if not document_row:
            raise exceptions.NotFound(
                f"No document found with uuid={document_uuid}",
                "document/not_found",
            )

        return self._transform_to_entity(document_row, None)

    def __sync_unaccepted_files_case_property(
        self,
        case_uuid: UUID,
    ):
        unaccepted_files_count_query = (
            sql.select(sql.func.count(1).label("count"))
            .select_from(
                sql.join(
                    schema.File,
                    schema.Case,
                    schema.File.case_id == schema.Case.id,
                )
            )
            .where(
                sql.and_(
                    schema.File.accepted.is_(False),
                    schema.File.destroyed.is_(False),
                    schema.File.date_deleted.is_(None),
                    schema.Case.uuid == case_uuid,
                )
            )
        )

        num_unaccepted_documents = self.session.execute(
            unaccepted_files_count_query
        ).scalar_one()

        zaak_meta_update = (
            sql.update(schema.CaseMeta)
            .where(
                sql.and_(
                    schema.CaseMeta.zaak_id == schema.Case.id,
                    schema.Case.uuid == case_uuid,
                )
            )
            .values(unaccepted_files_count=num_unaccepted_documents)
            .execution_options(synchronize_session=False)
        )

        self.session.execute(zaak_meta_update)

    def _get_filestore_by_uuid(self, uuid: UUID, allow_multiple: bool = False):
        filestore_join = sql.join(
            schema.Filestore,
            schema.File,
            sql.and_(
                schema.Filestore.id == schema.File.filestore_id,
                schema.File.date_deleted.is_(None),
            ),
            isouter=True,
        )
        filestore_query = (
            sql.select(
                schema.Filestore.id,
                schema.Filestore.uuid,
                schema.Filestore.original_name.label("filename"),
                schema.File.id.label("file_id"),
                schema.File.filestore_id.label("filestore_id"),
                schema.File.uuid.label("file_uuid"),
                schema.File.case_id,
                schema.File.name.label("basename"),
                schema.File.accepted,
                schema.File.created_by,
            )
            .select_from(filestore_join)
            .where(schema.Filestore.uuid == uuid)
        )

        filestore_row = self.session.execute(filestore_query).fetchone()

        if not filestore_row:
            raise exceptions.NotFound(
                f"Filestore with uuid '{uuid}' not found.",
                "document/filestore/not_found",
            )

        if filestore_row.case_id is not None and not allow_multiple:
            raise exceptions.Conflict(
                f"Filestore is already linked to case number {filestore_row.case_id}",
                "document/filestore/not_allowed",
            )
        return filestore_row

    def _prepare_event_values(self, event):
        values = {}

        for change in event.changes:
            values[change["key"]] = change["new_value"]

        for key, value in event.entity_data.items():
            if key not in values:
                values[key] = value
        return values

    def _prepare_logging_values(
        self,
        file_uuid,
        filestore_id,
        basename,
        accepted,
        file_id,
        created_by,
        case_data,
        user_uuid,
    ):
        return {
            "uuid": file_uuid,
            "filestore_id": filestore_id,
            "name": basename,
            "case_id": case_data["case_id"],
            "accepted": accepted,
            "file_id": file_id,
            "user_uuid": user_uuid,
            "case_assignee_uuid": case_data["assignee_uuid"],
            "created_by": created_by,
        }

    def _prepare_file_database_values(
        self,
        file_uuid,
        filestore_id,
        case_id,
        basename,
        extension,
        accepted,
        event,
        created_by,
        root_file_id,
        is_duplicate_name,
        directory_id,
        version,
        trust_level: str,
        lock: LockDescriptor | None,
        skip_intake=False,
        publish_pip=False,
    ):
        confidential = (
            True if trust_level in CONFIDENTIAL_TRUST_LEVELS else False
        )

        return {
            "uuid": file_uuid,
            "filestore_id": filestore_id,
            "name": basename,
            "search_term": basename,
            "case_id": case_id,
            "accepted": accepted,
            "extension": extension,
            "version": version,
            "creation_reason": basename + " toegevoegd",
            "reject_to_queue": False,
            "publish_pip": publish_pip,
            "publish_website": False,
            "date_created": str(event.created_date),
            "created_by": created_by,
            "date_modified": str(event.created_date),
            "modified_by": created_by,
            "destroyed": False,
            "active_version": True,
            "queue": True,
            "document_status": "original",
            "is_duplicate_of": root_file_id,
            "is_duplicate_name": is_duplicate_name,
            "directory_id": directory_id,
            "skip_intake": skip_intake,
            "root_file_id": root_file_id,
            "confidential": confidential,
            **(lock if lock else {}),
        }

    def _insert_file_document(self, database_values):
        exec_result = self.session.execute(
            sql.insert(schema.File).values(database_values)
        )

        return exec_result.inserted_primary_key[0]

    def _is_file_accepted(self, root_file_id, case_data, user_uuid, case_uuid):
        if case_uuid is not None:
            if (
                str(case_data["assignee_uuid"]) == str(user_uuid)
                and root_file_id is None
            ):
                return True

        return False

    def get_file_basename_and_extention(self, filename):
        if filename is None:
            return None, None

        (basename, extension) = os.path.splitext(filename)
        basename = clean_filename_part(basename)
        extension = clean_filename_part(extension)

        return basename, extension

    def _update_document_case(
        self, file_id, filestore_id, case_id, modified_by
    ):
        update_stmt = (
            sql.update(schema.File)
            .where(
                sql.and_(
                    schema.File.id == file_id,
                    schema.File.filestore_id == filestore_id,
                )
            )
            .values({"case_id": case_id, "modified_by": modified_by})
            .execution_options(synchronize_session=False)
        )
        self.session.execute(update_stmt)

    def _do_logging(self, file_id, database_values):
        if file_id is not None:
            event_placeholder.insert_logging_and_message(
                self.session, database_values
            )

    def _get_attachment_by_uuid(self, attachment_uuid: UUID):
        thread_message_attachment_join = sql.join(
            schema.ThreadMessageAttachment,
            schema.ThreadMessage,
            schema.ThreadMessageAttachment.thread_message_id
            == schema.ThreadMessage.id,
            isouter=True,
        )

        thread_message_join = thread_message_attachment_join.join(
            schema.Thread,
            schema.ThreadMessage.thread_id == schema.Thread.id,
            isouter=True,
        )

        filestore_join = thread_message_join.join(
            schema.Filestore,
            schema.Filestore.id == schema.ThreadMessageAttachment.filestore_id,
            isouter=True,
        )

        case_join = filestore_join.join(
            schema.Case, schema.Thread.case_id == schema.Case.id, isouter=True
        )

        attachment_query = (
            sql.select(
                schema.ThreadMessageAttachment.thread_message_id,
                schema.ThreadMessageAttachment.filestore_id,
                schema.Thread.case_id,
                schema.Case.uuid.label("case_uuid"),
                schema.Filestore.uuid.label("filestore_uuid"),
            )
            .select_from(case_join)
            .where(schema.ThreadMessageAttachment.uuid == attachment_uuid)
        )

        attachment_row = self.session.execute(attachment_query).fetchone()

        if not attachment_row:
            raise exceptions.NotFound(
                f"Attachment with uuid '{attachment_uuid}' not found.",
                "document/attachment/not_found",
            )

        if attachment_row.case_id is None:
            raise exceptions.Conflict(
                f"Attachment with uuid '{attachment_uuid}' cannot be linked.",
                "document/filestore/not_allowed",
            )

        return FileStoreSource(
            case_uuid=attachment_row.case_uuid,
            filestore_uuid=attachment_row.filestore_uuid,
            subject=None,
        )

    def _create_document_from_file(
        self,
        event: Event,
        values: dict,
        filestore_source_row: FileStoreSource,
        filename=None,
    ):
        """A generic method to create a document from a file. Add logging for that and Notification.

        :param event:
        :param values: prepared event values/database values
        :param filestore_source_row: A tuple with 2 UUID's case and filestore_uuid
        :param filename: If subject is provided will be used as a basename for the file.
        Like the email file which needs to have a human readable name.
        """

        case_data = self._get_case_id_assignee_uuid(
            filestore_source_row.case_uuid, event.user_uuid
        )

        if case_data["case_status"] == "resolved":
            raise exceptions.Conflict(
                "Not allowed to create document for 'resolved' case.",
                "communication/case/case_resolved",
            )
        filestore_row = self._get_filestore_by_uuid(
            uuid=filestore_source_row.filestore_uuid
        )

        created_by = self._get_legacy_contact_identifier(event.user_uuid)

        # update only case when the file and document are linked without a case.
        if filestore_row.filestore_id:
            self._update_document_case(
                file_id=filestore_row.file_id,
                filestore_id=filestore_row.id,
                case_id=case_data["case_id"],
                modified_by=created_by,
            )

        accepted = None
        file_id = None
        basename = None

        # create a document out of a file with a case(ACL'ed)
        if not filestore_row.filestore_id:
            directory_id = self._get_directory_id(
                values.get("directory_uuid", None)
            )
            basename, extension = self.get_file_basename_and_extention(
                filename=filestore_row.filename
            )
            (root_file_id, _) = self._get_root_file_id(
                basename, extension, case_data["case_id"]
            )
            accepted = self._is_file_accepted(
                root_file_id=root_file_id,
                case_data=case_data,
                user_uuid=event.user_uuid,
                case_uuid=filestore_source_row.case_uuid,
            )
            is_duplicate_name = True if root_file_id is not None else False
            root_file_id, is_duplicate_name = self._allow_rename_email_message(
                is_duplicate_name, root_file_id, extension
            )

            basename = filename or basename

            file_database_values = self._prepare_file_database_values(
                file_uuid=event.entity_id,
                filestore_id=filestore_row.id,
                case_id=case_data["case_id"],
                basename=basename,
                extension=extension,
                accepted=accepted,
                event=event,
                created_by=created_by,
                root_file_id=root_file_id,
                is_duplicate_name=is_duplicate_name,
                directory_id=directory_id,
                version=1,
                trust_level="",
                lock=None,  # Initial create: not locked
            )

            file_id = self._insert_file_document(
                database_values=file_database_values
            )

        logging_database_values = self._prepare_logging_values(
            file_uuid=filestore_row.uuid,
            filestore_id=filestore_row.id,
            basename=(basename or filestore_row.basename),
            accepted=(accepted or filestore_row.accepted),
            file_id=(file_id or filestore_row.file_id),
            created_by=created_by,
            case_data=case_data,
            user_uuid=event.user_uuid,
        )

        self._do_logging(
            file_id=(file_id or filestore_row.file_id),
            database_values=logging_database_values,
        )

        self.__sync_case_properties(case_uuid=filestore_source_row.case_uuid)

    def _create_document_from_attachment(
        self, event, user_info=None, dry_run=False
    ):
        values = self._prepare_event_values(event)
        attachment_row = self._get_attachment_by_uuid(
            attachment_uuid=values["attachment_uuid"]
        )

        self._create_document_from_file(
            event=event, values=values, filestore_source_row=attachment_row
        )

    def _allow_rename_email_message(
        self, is_duplicate_name, root_file_id, extension
    ):
        # Email messages will get unpacked; we don't want them to
        # "stack" as versions of the same document, so this tells
        # the unpacking code to rename it instead.
        is_email_message = is_duplicate_name and extension in [".eml", ".msg"]
        root_file_id = None if is_email_message else root_file_id
        is_duplicate_name = False if is_email_message else is_duplicate_name

        return root_file_id, is_duplicate_name

    def _create_document(self, event, user_info=None, dry_run=False):
        current_changes = self._prepare_event_values(event)
        file_uuid = event.entity_id
        auto_accept = current_changes.get("auto_accept", False)

        try:
            filestore_data = self._get_filestore_by_uuid(
                current_changes["store_uuid"], allow_multiple=True
            )
            filestore_id = filestore_data.id
        except exceptions.NotFound:
            exec_result = self.session.execute(
                sql.insert(schema.Filestore).values(
                    {
                        "uuid": current_changes["store_uuid"],
                        "original_name": current_changes["basename"]
                        + current_changes["extension"],
                        "size": current_changes["size"],
                        "mimetype": current_changes["mimetype"],
                        "storage_location": [
                            current_changes["storage_location"]
                        ],
                        "md5": current_changes["md5"],
                        "is_archivable": current_changes["is_archivable"],
                        "virus_scan_status": current_changes[
                            "virus_scan_status"
                        ],
                    },
                )
            )
            filestore_id = exec_result.inserted_primary_key[0]

        is_pip_user = current_changes["creator_type"] == "pip"
        current_changes["case_uuid"] = current_changes.get("case_uuid", None)
        current_changes["directory_id"] = self._get_directory_id(
            current_changes.get("directory_uuid", None)
        )

        case_data = {}
        version = 1
        root_file_id = None
        is_duplicate_name = False
        lock = None

        if current_changes["case_uuid"]:
            case_data = self._get_case_id_assignee_uuid(
                current_changes["case_uuid"],
                event.user_uuid,
                is_pip_user,
                is_system=user_info.permissions.get("system", False)
                if user_info
                else False,
            )
            (root_file_id, _) = self._get_root_file_id(
                current_changes["basename"],
                current_changes["extension"],
                case_data["case_id"],
            )

            is_duplicate_name = True if root_file_id is not None else False
            (
                root_file_id,
                is_duplicate_name,
            ) = self._allow_rename_email_message(
                is_duplicate_name, root_file_id, current_changes["extension"]
            )

            if auto_accept:
                (
                    version,
                    root_file_id,
                    file_uuid,
                    lock,
                ) = self._update_version(event.entity_id)
                self.logger.debug(f"Updating document. New version: {version}")

        created_by = self._get_legacy_contact_identifier(event.user_uuid)

        # file_uuid could be updated by the `_update_version` call a above.
        current_metadata = self._get_file_metadata(file_uuid)
        current_publish_settings = self._get_file_publishing_setting(file_uuid)

        insert_params = self._prepare_file_database_values(
            file_uuid=file_uuid,
            filestore_id=filestore_id,
            case_id=case_data.get("case_id", None),
            basename=current_changes["basename"],
            extension=current_changes["extension"],
            accepted=current_changes["accepted"],
            event=event,
            created_by=created_by,
            root_file_id=root_file_id,
            is_duplicate_name=is_duplicate_name,
            directory_id=current_changes["directory_id"],
            publish_pip=is_pip_user,
            skip_intake=current_changes.get("skip_intake", False),
            version=version,
            trust_level=current_metadata.get("trust_level", "")
            if current_metadata
            else "",
            lock=lock,
        )

        self.session.execute(sql.insert(schema.File).values(insert_params))

        if root_file_id is not None:
            self.__sync_flags(
                file_uuid, current_metadata, current_publish_settings
            )

        self.__sync_case_properties(case_uuid=current_changes["case_uuid"])

    def __update_document_labels(self, document_uuid: UUID):
        """
        Update the file_case_document table to move the label to the latest
        version of the document.
        """
        self.session.execute(
            sql.update(schema.FileCaseDocument)
            .where(
                schema.FileCaseDocument.file_id.in_(
                    sql.select(schema.File.id)
                    .where(schema.File.uuid == document_uuid)
                    .scalar_subquery(),
                )
            )
            .values(
                file_id=sql.select(schema.File.id)
                .where(
                    sql.and_(
                        schema.File.uuid == document_uuid,
                        schema.File.active_version.is_(True),
                    ),
                )
                .scalar_subquery()
            )
            .execution_options(synchronize_session=False)
        )

    def _accept_document(
        self,
        event: Event,
        user_info=None,
        dry_run=False,
    ):
        try:
            original_document_uuid = event.previous_value("document_uuid")
            as_version_of_uuid = event.new_value("document_uuid")
        except IndexError:
            original_document_uuid = event.entity_id
            as_version_of_uuid = None

        update_values = {
            "accepted": True,
            "active_version": True,
            "is_duplicate_name": False,
            "is_duplicate_of": None,
            "rejection_reason": None,
        }

        if as_version_of_uuid and original_document_uuid != as_version_of_uuid:
            # "un-active-version" the old active version
            # Retrieve its id (so we can use it in `root_file_id`)
            deactivate_old_version_query = (
                sql.update(schema.File)
                .values(active_version=False)
                .where(
                    schema.File.uuid == as_version_of_uuid,
                    schema.File.active_version.is_(True),
                    schema.File.date_deleted.is_(None),
                    schema.File.destroyed.is_(False),
                    schema.File.case_id
                    == sql.select(schema.File.case_id)
                    .where(schema.File.uuid == original_document_uuid)
                    .limit(1)
                    .scalar_subquery(),
                )
            ).returning(
                schema.File.root_file_id,
                schema.File.id,
                schema.File.version,
                schema.File.lock_timestamp,
                schema.File.lock_subject_id,
                schema.File.lock_subject_name,
                schema.File.shared,
                schema.File.publish_pip,
                schema.File.publish_website,
                schema.File.metadata_id,
            )

            try:
                result = self.session.execute(
                    deactivate_old_version_query
                ).one()
            except sql_exc.NoResultFound as e:
                raise exceptions.Conflict(
                    "Previous document version not found (in the same case, or not at all)",
                    "document/accept/not_found",
                ) from e

            update_values["uuid"] = as_version_of_uuid
            update_values["root_file_id"] = result.root_file_id or result.id
            update_values["version"] = result.version + 1
            update_values["lock_timestamp"] = result.lock_timestamp
            update_values["lock_subject_id"] = result.lock_subject_id
            update_values["lock_subject_name"] = result.lock_subject_name
            update_values["shared"] = result.shared
            update_values["publish_pip"] = result.publish_pip
            update_values["publish_website"] = result.publish_website
            update_values["metadata_id"] = result.metadata_id

        update_stmt = (
            sql.update(schema.File)
            .values(**update_values)
            .where(
                sql.and_(
                    schema.File.accepted.is_(False),
                    schema.File.uuid == original_document_uuid,
                )
            )
            .returning(schema.File.id, schema.File.case_id)
        )

        try:
            # Set new document as "active version"
            (_updated_id, case_id) = self.session.execute(update_stmt).one()
            (case_uuid, case_status) = self.session.execute(
                sql.select(schema.Case.uuid, schema.Case.status).where(
                    sql.and_(
                        schema.Case.id == case_id,
                        schema.Case.status != "deleted",
                        schema.Case.deleted.is_(None),
                    )
                )
            ).one()
        except sql_exc.NoResultFound as e:
            raise exceptions.Conflict(
                "Could not accept document.",
                "document/accept_failed",
            ) from e

        if case_status == "resolved":
            raise exceptions.Conflict(
                f"Cannot accept document in case with status '{case_status}'",
                "document/cannot_accept_in_resolved_case",
            )

        # Check for duplicates at the very end. That way it doesn't matter if
        # the document was accepted as a "new document" or a "new version",
        # if the new name is a dupe, it's rejected.
        #
        # And because it's all in a transaction, raising the exception will
        # just undo everything.
        docs_with_same_name = self.session.execute(
            sql.select(sql.func.count(sql.literal("*")))
            .select_from(schema.File)
            .where(
                sql.and_(
                    schema.File.name == event.entity_data["basename"],
                    schema.File.extension == event.entity_data["extension"],
                    schema.File.active_version.is_(True),
                    schema.File.destroyed.is_(False),
                    schema.File.date_deleted.is_(None),
                    schema.File.case_id == case_id,
                    schema.File.accepted.is_(True),
                )
            )
        ).scalar_one()
        if docs_with_same_name != 1:
            raise exceptions.Conflict(
                f"Cannot accept document: {docs_with_same_name} document(s) with the same name already exists",
                "document/duplicate_name",
            )

        self.__update_document_labels(
            document_uuid=as_version_of_uuid or original_document_uuid
        )

        self.__sync_unaccepted_files_case_property(case_uuid)

    def __sync_flags(
        self, file_uuid: UUID, current_metadata, current_publish_settings
    ):
        if current_metadata is not None:
            self.__sync_metadata(
                file_uuid=file_uuid, metadata=current_metadata
            )

        if current_publish_settings is not None:
            self.__sync_publish_settings(
                file_uuid=file_uuid,
                publish_settings=current_publish_settings,
            )

    def __sync_case_properties(self, case_uuid: UUID | None):
        if not case_uuid:
            return

        self.__sync_unaccepted_files_case_property(case_uuid=case_uuid)

    def __sync_metadata(self, file_uuid: UUID, metadata: dict):
        file_metadata_id = self.session.execute(
            sql.insert(schema.FileMetaData)
            .values(metadata)
            .returning(schema.FileMetaData.id)
        ).scalar_one()

        self.session.execute(
            sql.update(schema.File)
            .values({"metadata_id": file_metadata_id})
            .where(
                sql.and_(
                    schema.File.uuid == file_uuid,
                    schema.File.active_version.is_(True),
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _get_file_metadata(self, file_uuid: UUID):
        file_metadata_result = None
        file_metadata = self.session.execute(
            sql.select(
                schema.FileMetaData.id,
                schema.FileMetaData.description,
                schema.FileMetaData.trust_level,
                schema.FileMetaData.document_category,
                schema.FileMetaData.origin_date,
                schema.FileMetaData.pronom_format,
                schema.FileMetaData.appearance,
                schema.FileMetaData.structure,
                schema.FileMetaData.origin,
            ).where(
                schema.FileMetaData.id
                == sql.select(schema.File.metadata_id)
                .where(
                    sql.and_(
                        schema.File.uuid == file_uuid,
                        schema.File.date_deleted.is_(None),
                    )
                )
                .order_by(schema.File.version.desc())
                .limit(1)
                .scalar_subquery()
            )
        ).fetchone()
        if file_metadata is not None:
            file_metadata_result = {
                "description": file_metadata.description,
                "origin": file_metadata.origin,
                "origin_date": file_metadata.origin_date,
                "trust_level": file_metadata.trust_level,
                "document_category": file_metadata.document_category,
                "pronom_format": file_metadata.pronom_format,
                "appearance": file_metadata.appearance,
                "structure": file_metadata.structure,
            }

        return file_metadata_result

    def _get_file_publishing_setting(self, file_uuid: UUID):
        setting = self.session.execute(
            sql.select(schema.File.publish_pip, schema.File.publish_website)
            .where(
                sql.and_(
                    schema.File.uuid == file_uuid,
                    schema.File.date_deleted.is_(None),
                )
            )
            .order_by(schema.File.version.desc())
            .limit(1)
        ).fetchone()

        if setting:
            return {
                "publish_pip": setting.publish_pip,
                "publish_website": setting.publish_website,
            }

        return None

    def __sync_publish_settings(self, file_uuid: UUID, publish_settings: dict):
        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "publish_pip": publish_settings["publish_pip"],
                    "publish_website": publish_settings["publish_website"],
                }
            )
            .where(
                sql.and_(
                    schema.File.uuid == file_uuid,
                    schema.File.active_version.is_(True),
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _update_version(self, document_uuid):
        """
        This function generates new file version when the file is updated
        with an external editor.

        It sets active_version=False for current version of the document and
        returns incremented version number. It also returns the "root file id",
        so versions can properly linked together.

        :param document_uuid: UUID of the document
        :type document_uuid: UUID
        :return: Incremented document version
        :rtype: int
        """
        try:
            file = self._get_file_row_by_document_uuid(
                document_uuid=document_uuid
            )

            new_version = file.version + 1
            # Mark the old current version as no longer current
            self.session.execute(
                sql.update(schema.File)
                .where(schema.File.id == file.id)
                .values(active_version=False)
                .execution_options(synchronize_session=False)
            )

            new_root_file_id = file.root_file_id or file.id
            new_uuid = file.uuid
            lock = LockDescriptor(
                lock_timestamp=file.lock_timestamp,
                lock_subject_id=file.lock_subject_id,
                lock_subject_name=file.lock_subject_name,
                shared=file.shared,
            )
        except exceptions.Conflict:
            # There is no existing version
            new_version = 1
            new_root_file_id = None
            new_uuid = document_uuid
            lock = None  # New file = not locked

        return new_version, new_root_file_id, new_uuid, lock

    def search_document(
        self,
        user_info: minty.cqrs.UserInfo,
        case_uuid: UUID | None,
        keyword: str | None,
    ):
        """Search document by case_uuid or keyword.

        :param case_uuid: uuid of the case.
        :type keyword: Optional[str]
        :param keyword: keyword for searching.
        :type keyword: Optional[str]
        :param filter: filter used for searching.
        :type filter: Optional[str]
        """
        search_conditions = [
            schema.File.date_deleted.is_(None),
            schema.File.destroyed.is_(False),
            schema.File.active_version.is_(True),
            schema.File.accepted.is_(True),
        ]

        if keyword:
            search_conditions.append(
                schema.File.search_term.ilike(f"%{keyword}%"),
            )

        if case_uuid:
            search_conditions.extend(
                [
                    schema.Case.uuid == case_uuid,
                    schema.File.case_id == schema.Case.id,
                ]
            )

        search_query = _document_query_base(user_info).where(
            sql.and_(*search_conditions)
        )
        documents = self.session.execute(search_query).fetchall()

        result = []
        for document_row in documents:
            document = self._transform_to_entity(document_row=document_row)
            result.append(document)
        return result

    def _transform_to_entity(
        self, document_row, integrity_check_successful=None
    ):
        """Transform document row to Document entity.

        :param document_row: Database row of the document.
        :type document_row: ResultProxy
        """

        preview_uuid = document_row.preview_uuid
        preview_storage_location = document_row.preview_storage_location
        preview_mimetype = document_row.preview_mimetype

        current_version = document_row.version

        description = document_row.description
        origin = document_row.origin
        origin_date = document_row.origin_date
        confidentiality = document_row.confidentiality
        document_category = document_row.document_category
        document_source = document_row.document_source

        thumbnail_uuid = document_row.thumbnail_uuid
        thumbnail_storage_location = document_row.thumbnail_storage_location
        thumbnail_mimetype = document_row.thumbnail_mimetype

        labels = document_row.labels
        lock = {
            "user_uuid": document_row.lock_user_uuid,
            "user_display_name": document_row.lock_user_display_name,
            "timestamp": document_row.lock_timestamp,
            "case_id": document_row.case_display_number,
            "shared": document_row.lock_shared,
        }

        document = Document(
            document_uuid=document_row.document_uuid,
            basename=document_row.filename,
            extension=document_row.extension,
            store_uuid=document_row.store_uuid,
            directory_uuid=document_row.directory_uuid,
            case_uuid=document_row.case_uuid,
            case_display_number=document_row.case_display_number,
            mimetype=document_row.mimetype,
            size=document_row.size,
            storage_location=document_row.storage_location,
            md5=document_row.md5,
            is_archivable=document_row.is_archivable,
            virus_scan_status=document_row.virus_scan_status,
            accepted=document_row.accepted,
            date_modified=document_row.date_modified,
            thumbnail="/".join(
                ["", "file", "thumbnail", "file_id", str(document_row.id)]
            ),
            creator_uuid=document_row.creator_uuid,
            creator_displayname=document_row.creator_displayname,
            preview_uuid=preview_uuid,
            preview_storage_location=preview_storage_location,
            preview_mimetype=preview_mimetype,
            description=description,
            origin=origin,
            origin_date=origin_date,
            confidentiality=confidentiality,
            document_category=document_category,
            document_number=document_row.id,
            current_version=current_version,
            integrity_check_successful=integrity_check_successful,
            thumbnail_uuid=thumbnail_uuid,
            thumbnail_storage_location=thumbnail_storage_location,
            thumbnail_mimetype=thumbnail_mimetype,
            labels=labels,
            lock=lock,
            intake_owner_uuid=document_row.intake_owner_uuid,
            intake_group_uuid=document_row.intake_group_uuid,
            intake_role_uuid=document_row.intake_role_uuid,
            has_search_index=document_row.has_search_index,
            publish={
                "pip": document_row.publish_pip,
                "website": document_row.publish_website,
            },
            document_source=document_source,
            status=DOCUMENT_STATUS_FROM_DB_MAPPING[
                document_row.document_status
            ],
            pronom_format=document_row.pronom_format,
            appearance=document_row.appearance,
            structure=document_row.structure,
        )

        document.event_service = self.event_service
        return document

    def generate_download_url(self, document: Document, user_info) -> str:
        """Generate temporary download url for given file.

        :param document_uuid: document UUID
        :param user_uuid: User UUID
        :return:  url to download file
        :rtype: str
        """

        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))

        if document.virus_scan_status != "ok":
            raise exceptions.Forbidden(
                f"Document with uuid '{document.document_uuid}'is not scanned, can not be downloaded",
                "document/download/not_allowed",
            )
        url = store.get_download_url(
            uuid=document.store_uuid,
            storage_location=document.storage_location[0],
            mime_type=document.mimetype,
            filename=document.basename + document.extension,
            download=True,
        )

        return url

    def generate_preview_url(
        self,
        preview_uuid: UUID,
        preview_storage_location: str,
        preview_mime_type: str,
        preview_filename: str | None = None,
    ) -> str:
        """Generate temporary preview url for given file.

        :param document_uuid: document UUID
        :type document_uuid: UUID
        :param preview_uuid: filestore UUID of preview
        :type preview_uuid: UUID
        :param preview_storage_location: preview storage location of document
        :type preview_storage_location: str
        :param preview_mime_type: preview mimetype of document
        :type preview_mime_type: str
        :return:  url to preview file
        :rtype: str
        """

        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))

        url = store.get_download_url(
            uuid=preview_uuid,
            storage_location=preview_storage_location,
            mime_type=preview_mime_type,
            filename=preview_filename,
            download=False,
        )

        return url

    def _add_document_to_case(self, event, user_info=None, dry_run=False):
        """Add document from document_intake(without any case) to a case.

        :param event: AddedToCase event from Document entity.
        :type event: minty.cqrs.event
        """
        document_uuid = event.entity_id
        changes = self._prepare_event_values(event)

        case = self.session.execute(
            sql.select(schema.Case.id, schema.Case.status).where(
                sql.and_(
                    schema.Case.uuid == changes["case_uuid"],
                    schema.Case.deleted.is_(None),
                )
            )
        ).fetchone()

        case_uuid = changes["case_uuid"]

        if not case:
            raise exceptions.NotFound(
                f"Case not found: '{case_uuid}'",
                "document/case_not_found",
            )
        elif case.status == "resolved":
            raise exceptions.Conflict(
                f"Document cannot be added to resolved case with uuid '{case_uuid}'",
                "document/cannot_add_to_resolved_case",
            )

        # Update document with case
        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "case_id": sql.select(schema.Case.id)
                    .where(schema.Case.uuid == changes["case_uuid"])
                    .scalar_subquery(),
                }
            )
            .where(schema.File.uuid == document_uuid)
            .execution_options(synchronize_session=False)
        )

        self.__sync_unaccepted_files_case_property(changes["case_uuid"])

    def _update_document(self, event, user_info=None, dry_run=False):
        """
        Update document metadata. Implemented only for intake documents.
        """
        document_uuid = event.entity_id
        changes = self._prepare_event_values(event)
        file_metadata_id = self._update_document_metadata(
            document_uuid, changes
        )

        # Update document with name and metadata.
        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "name": changes["basename"],
                    "metadata_id": file_metadata_id,
                    "date_modified": sql.func.current_timestamp().op(
                        "AT TIME ZONE"
                    )("UTC"),
                    "document_status": DOCUMENT_STATUS_TO_DB_MAPPING[
                        changes["status"]
                    ],
                    "publish_pip": changes["publish"]["pip"],
                    "publish_website": changes["publish"]["website"],
                    "modified_by": self._get_legacy_contact_identifier(
                        user_uuid=event.user_uuid
                    ),
                    "search_term": sql.func.concat_ws(
                        " ",
                        schema.File.name,
                        sql.select(schema.FileMetaData.description)
                        .where(
                            schema.File.metadata_id == schema.FileMetaData.id
                        )
                        .scalar_subquery(),
                    ),
                }
            )
            .where(schema.File.uuid == document_uuid)
            .execution_options(synchronize_session=False)
        )

    def _update_document_metadata(self, document_uuid: UUID, changes: dict):
        """Update metadata of the document in file_metadata table.

        :param document_uuid: UUID of the document.
        :type document_uuid: UUID
        :param changes: Updated event changes
        :type changes: dict
        :return: metadata_id of the document
        :rtype: int
        """
        metadata_values = {
            "origin": changes.get("origin"),
            "trust_level": changes.get("confidentiality")
            or "Zaakvertrouwelijk",
            "document_category": changes.get("document_category"),
        }

        fields_to_update_if_changed: Final = (
            "description",
            "origin_date",
            "document_source",
            "pronom_format",
            "appearance",
            "structure",
        )
        for field_name in fields_to_update_if_changed:
            if field_name in changes:
                metadata_values[field_name] = changes[field_name]

        file_metadata = self.session.execute(
            sql.select(schema.FileMetaData.id).where(
                sql.and_(
                    schema.File.active_version.is_(True),
                    schema.File.uuid == document_uuid,
                    schema.File.metadata_id == schema.FileMetaData.id,
                )
            )
        ).fetchone()

        # if document has metadata update it, else insert metadata for document.
        if file_metadata:
            file_metadata_id = file_metadata.id
            self.session.execute(
                sql.update(schema.FileMetaData)
                .values(metadata_values)
                .where(schema.FileMetaData.id == file_metadata_id)
                .execution_options(synchronize_session=False)
            )
        else:
            file_metadata_id = self.session.execute(
                sql.insert(schema.FileMetaData)
                .values(
                    metadata_values,
                )
                .returning(schema.FileMetaData.id)
            ).scalar_one()

        return file_metadata_id

    def _check_file_integrity(
        self, file_uuid: UUID, storage_location: list, md5: str
    ):
        """Check integrity of a file. Its done by comapring the md5 in filestore table and md5 of file downloaded from s3.
            If they are same the file hasn't changed thus validated the integrity.

        :param file_uuid: UUID of file.
        :type file_uuid: UUID
        :param storage_location: Storage location of file.
        :type storage_location: List
        :param md5: md5 of file.
        :type md5: str
        :return: True if the integrity of file is validated else False.
        :rtype: bool
        """
        storage_infra = self._get_infrastructure("s3")

        # download file form s3.
        file_fh = tempfile.NamedTemporaryFile()
        storage_infra.download_file(
            destination=file_fh,
            file_uuid=file_uuid,
            storage_location=storage_location[0],
        )
        file_fh.seek(0, os.SEEK_SET)

        # compare md5 of s3 downloaded file with the md5 in filestore table.
        hash_md5 = hashlib.md5()
        for chunk in iter(lambda: file_fh.read(4096), b""):
            hash_md5.update(chunk)
        s3_md5 = hash_md5.hexdigest()

        return s3_md5 == md5

    def _delete_document(self, event, user_info=None, dry_run=False):
        """
        Destroy a document
        :param event:
        :return:
        """

        altered_by = self._get_legacy_contact_identifier(event.user_uuid)
        current_datetime = datetime.now(timezone.utc)

        # Get the lastest/active version of a document
        # by UUID. The with_for_update() keeps the record
        # locked for making changes, until commit()

        try:
            file = (
                self.session.query(schema.File)
                .filter(
                    sql.and_(
                        schema.File.uuid == event.entity_id,
                        schema.File.active_version.is_(True),
                    )
                )
                .with_for_update()
                .order_by(sql.desc(schema.File.version))
                .one()
            )
        except sql_exc.NoResultFound as e:
            raise exceptions.Conflict(
                f"Deletable document with UUID '{event.entity_id}' not found",
                "document/delete_document/not_found",
            ) from e

        if file.case_id is not None:
            raise exceptions.Conflict(
                f"Document '{file.uuid}' is assigned to case '{file.case_id}'",
                "document/delete_document/assigned_to_case",
            )

        file.destroyed = True

        file.modified_by = altered_by
        file.date_modified = current_datetime

        file.deleted_by = altered_by
        file.date_deleted = current_datetime

        self.session.add(file)

    def _assign_document_to_user(self, event, user_info=None, dry_run=False):
        """Assign document to user. Implemented only for intake documents.

        :param event: DocumentAssignedToUser event from Document entity.
        :type event: minty.cqrs.event
        """
        document_uuid = event.entity_id
        changes = self._prepare_event_values(event)
        intake_owner_uuid = changes["intake_owner_uuid"]
        rejection_reason = changes["rejection_reason"]
        subject = self.session.execute(
            sql.select(schema.Subject.id).where(
                schema.Subject.uuid == intake_owner_uuid
            )
        ).fetchone()
        if subject is None:
            raise exceptions.NotFound(
                f"Subject with uuid '{intake_owner_uuid}' not found",
                "subject/not_found",
            )
        subject_legacy_id = "betrokkene-medewerker-" + str(subject.id)

        # Update document with intake_owner. if the document is assigned to a role, unset intake_group and intake_role
        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "intake_owner": subject_legacy_id,
                    "intake_group_id": None,
                    "intake_role_id": None,
                    "rejection_reason": rejection_reason,
                }
            )
            .where(schema.File.uuid == document_uuid)
            .execution_options(synchronize_session=False)
        )

    def _assign_document_to_role(self, event, user_info=None, dry_run=False):
        """Assign document to role and group. Implemented only for intake documents.

        :param event: DocumentAssignedToRole event from Document entity.
        :type event: minty.cqrs.event
        """
        document_uuid = event.entity_id
        changes = self._prepare_event_values(event)
        intake_role_uuid = changes["intake_role_uuid"]
        intake_group_uuid = changes["intake_group_uuid"]
        rejection_reason = changes["rejection_reason"]
        role_id = (
            sql.select(schema.Role.id)
            .where(schema.Role.uuid == intake_role_uuid)
            .scalar_subquery()
        )
        group_id = (
            sql.select(schema.Group.id)
            .where(schema.Group.uuid == intake_group_uuid)
            .scalar_subquery()
        )

        # Update document with intake_group and intake_role. if the document is assigned to a user,unset intake_owner
        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "intake_group_id": group_id,
                    "intake_role_id": role_id,
                    "intake_owner": None,
                    "rejection_reason": rejection_reason,
                }
            )
            .where(schema.File.uuid == document_uuid)
            .execution_options(synchronize_session=False)
        )

    def _reject_document_assignment(
        self, event, user_info: minty.cqrs.UserInfo, dry_run=False
    ):
        document_uuid = event.entity_id
        changes = self._prepare_event_values(event)

        self.session.execute(
            sql.update(schema.File)
            .values(
                {
                    "intake_group_id": None,
                    "intake_role_id": None,
                    "intake_owner": None,
                    "rejection_reason": changes["rejection_reason"],
                    "rejected_by_display_name": sql.select(
                        sql.cast(schema.Subject.properties, postgresql.JSON)[
                            "displayname"
                        ]
                    )
                    .where(schema.Subject.uuid == user_info.user_uuid)
                    .scalar_subquery(),
                }
            )
            .where(schema.File.uuid == document_uuid)
            .execution_options(synchronize_session=False)
        )

    def generate_thumbnail_url(
        self,
        thumbnail_uuid: UUID,
        thumbnail_storage_location: str,
        thumbnail_mimetype: str,
    ) -> str:
        """Generate thumbnail url for given file."""

        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))

        url = store.get_download_url(
            uuid=thumbnail_uuid,
            storage_location=thumbnail_storage_location,
            mime_type=thumbnail_mimetype,
            filename=None,
            download=False,
        )

        return url

    def generate_document_edit_url(
        self, document_uuid: UUID, user_info, save_url
    ):
        """Generate URL to edit file using zoho editor.

        :param document_uuid: UUID of the document.
        :type document_uuid: UUID
        :param user_info: user_info
        :type user_info: object
        :param save_url: Url to save document
        :type save_url: str
        :rtype: URL to open document for editing
        """
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        db = self._get_infrastructure("database")

        token = secrets.token_urlsafe()

        user_name = self._get_user_name(user_info.user_uuid)

        doc_edit_premission_query = sql.select(schema.Config.value).where(
            schema.Config.parameter == "edit_document_online"
        )
        is_doc_edit_enabled = db.execute(doc_edit_premission_query).fetchone()[
            0
        ]

        if int(is_doc_edit_enabled) == 0:
            raise exceptions.Forbidden(
                "Payment required to use document editor",
                "document/edit/not_enabled",
            )

        document = self.get_document_by_uuid(
            document_uuid=document_uuid, user_info=user_info
        )

        case_uuid = document.case_uuid
        if case_uuid is None:
            raise exceptions.Conflict(
                "Document is not linked to any case",
                "document/case/not_exists",
            )

        document_name = document.basename + document.extension
        document_url = store.get_download_url(
            uuid=document.store_uuid,
            storage_location=document.storage_location[0],
            mime_type=document.mimetype,
            filename=document_name,
            download=True,
        )

        configuration = self.infrastructure_factory.get_config(self.context)
        shortname = configuration["instance_uuid"]

        redis = self._get_infrastructure("redis")
        redis_key = f"zoho_authentication_token:edit_document_online:{shortname}:{document_uuid}"
        redis_data = {
            "token": token,
            "user_uuid": str(user_info.user_uuid),
            "case_uuid": str(case_uuid),
        }
        redis.set(redis_key, json.dumps(redis_data), 3600)
        document_id = f"{document_uuid}-{shortname}"
        document_info = {
            "document_name": document_name,
            "document_id": document_id,
        }

        save_format = document.extension.split(".")[-1]
        callback_settings = {
            "save_format": save_format,
            "save_url": save_url,
            "context_info": "Edit document",
            "save_url_params": {
                "document_uuid": document_uuid,
                "authentication_token": token,
                "editor_type": "zoho",
            },
        }
        user_info = {"user_id": user_info.user_uuid, "display_name": user_name}
        options = {
            "editor_settings": '{ "unit": "in", "language": "nl", "view": "pageview" }',
            "permissions": '{ "document.export": true, "document.print": true, "document.edit": true, "review.changes.resolve": false, "review.comment": true, "collab.chat": true }',
            "callback_settings": str(callback_settings),
            "document_info": str(document_info),
            "user_info": str(user_info),
            "document_defaults": '{ "orientation": "portrait", "paper_size": "A4", "font_name": "Lato", "font_size": 12, "track_changes": "disabled" }',
            "url": document_url,
        }
        zoho = self._get_infrastructure("zoho")
        edit_url = zoho.edit_document(options)
        return edit_url

    def get_ms_wopi_configuration(
        self,
        document_uuid: UUID,
        user_info: minty.cqrs.UserInfo,
        save_url: str,
        close_url: str,
        host_page_url: str,
        context: str,
        lock_acquire_url: str,
        lock_release_url: str,
        lock_extend_url: str,
    ):
        """Get Microsoft WOPI configuration to get edit document with ms online"""
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        redis = self._get_infrastructure("redis")
        wopi = self._get_infrastructure("wopi")
        configuration = self.infrastructure_factory.get_config(self.context)

        document = self.get_document_by_uuid(
            document_uuid=document_uuid, user_info=user_info
        )
        if document.case_uuid is None:
            raise exceptions.Conflict(
                "Document is not linked to any case",
                "document/case/not_exists",
            )
        user_uuid = str(user_info.user_uuid)
        user_name = self._get_user_name(user_uuid=user_uuid)
        is_locked = self.is_locked(document_lock=document.lock)
        if is_locked and not document.lock["shared"]:
            raise exceptions.Conflict(
                "Document is locked",
                "document/locked/non_editable",
            )

        document_name = document.basename + document.extension
        document_extension = document.extension.split(".")[-1].lower()
        document_url = store.get_download_url(
            uuid=document.store_uuid,
            storage_location=document.storage_location[0],
            mime_type=document.mimetype,
            filename=document_name,
            download=True,
        )

        shortname = configuration["instance_uuid"]

        redis_key = f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"
        redis_data = redis.get(redis_key)
        if redis_data:
            # Authentication_token is scoped to a single user and resource combination.
            # Authentication_token unique for a user document combination.
            token = json.loads(redis_data.decode("utf-8"))["token"]
        else:
            # On co-authoring session each users will have their own authentication_tokens.
            token = secrets.token_urlsafe()
            redis.set(
                redis_key,
                json.dumps(
                    {
                        "token": token,
                        "case_uuid": str(document.case_uuid),
                        "user_info": json.loads(user_info.json()),
                        "user_uuid": str(user_info.user_uuid),
                        "user_permissions": user_info.permissions,
                    }
                ),
                3600,
            )

        document_info = {
            "extension": document_extension,
            "filename": document_name,
            "version": document.current_version,
            "size": document.size,
            "file_url": document_url,
        }
        callback_settings = {
            "save_url": save_url,
            "save_url_params": {
                "document_uuid": document_uuid,
                "authentication_token": token,
                "editor_type": "msonline",
                "directory_uuid": str(document.directory_uuid)
                if document.directory_uuid
                else None,
                "user_uuid": user_uuid,
            },
            "close_url": close_url,
            "host_page_url": host_page_url,
            "lock_acquire_url": lock_acquire_url,
            "lock_release_url": lock_release_url,
            "lock_extend_url": lock_extend_url,
        }
        options = {
            "document_uuid": document_uuid,
            "context": context,
            "app": self._wopi_app_mapping(document_extension),
            "action": "edit",
            "user_uuid": user_uuid,
            "user_display_name": user_name,
            "owner_uuid": str(document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }
        wopi_configuration = wopi.edit_document(options)
        return {
            "fav_icon_url": wopi_configuration["favIconUrl"],
            "action_url": wopi_configuration["urlsrc"],
            "access_token": wopi_configuration["access_token"],
            "access_token_ttl": wopi_configuration["access_token_ttl"],
        }

    def _get_user_name(self, user_uuid):
        """Get User display name by user_uuid"""
        user_name = self.session.execute(
            sql.select(
                sql.cast(schema.Subject.properties, JSON)["displayname"].label(
                    "name"
                )
            ).where(schema.Subject.uuid == user_uuid)
        ).fetchone()

        if not user_name:
            raise exceptions.NotFound(
                f"No user found with uuid '{user_uuid}'",
                "user/not_found",
            )

        return user_name[0]

    WORD_EXTENSIONS = {"odt", "docm", "docx"}
    EXCEL_EXTENSIONS = {"ods", "xlsb", "xlsm", "xlsx"}
    PPOINT_EXTENSIONS = {"odp", "ppsx", "pptx"}
    TEST_EXTENSIONS = {"wopitest", "wopitestx"}

    def _wopi_app_mapping(self, extension):
        """Get WOPI App by document extension"""

        if extension in self.WORD_EXTENSIONS:
            return "Word"
        elif extension in self.EXCEL_EXTENSIONS:
            return "Excel"
        elif extension in self.PPOINT_EXTENSIONS:
            return "PowerPoint"
        elif extension in self.TEST_EXTENSIONS:
            return "WopiTest"
        else:
            raise exceptions.Forbidden(
                f"Document with extension '.{extension}' cannot be edited with Microsoft Online.",
                "document/cannot_be_edited",
            )

    def __apply_label(
        self, document_uuid: UUID, case_uuid: UUID, label_uuid: UUID
    ):
        """
        Insert a new file_case_document row for a document + case + label
        combination.
        """
        self.session.execute(
            sql.insert(schema.FileCaseDocument).values(
                {
                    "file_id": sql.select(schema.File.id)
                    .where(
                        sql.and_(
                            schema.File.uuid == document_uuid,
                            schema.File.active_version.is_(True),
                        )
                    )
                    .scalar_subquery(),
                    "bibliotheek_kenmerken_id": sql.select(
                        schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                    )
                    .where(
                        schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid
                        == label_uuid
                    )
                    .scalar_subquery(),
                    "magic_string": sql.select(
                        schema.ZaaktypeDocumentKenmerkenMap.magic_string
                    )
                    .where(
                        schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid
                        == label_uuid
                    )
                    .scalar_subquery(),
                    "case_id": sql.select(schema.Case.id)
                    .select_from(schema.Case)
                    .where(schema.Case.uuid == case_uuid)
                    .scalar_subquery(),
                },
            )
        )

    def __remove_label(
        self, document_uuid: UUID, case_uuid: UUID, label_uuid: UUID
    ):
        """
        Remove the file_case_document row for a document + case + label
        combination.
        """
        self.session.execute(
            sql.delete(schema.FileCaseDocument)
            .where(
                sql.and_(
                    schema.FileCaseDocument.file_id == schema.File.id,
                    schema.FileCaseDocument.case_id == schema.Case.id,
                    schema.FileCaseDocument.bibliotheek_kenmerken_id
                    == schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id,
                )
            )
            .where(schema.File.uuid == document_uuid)
            .where(schema.Case.uuid == case_uuid)
            .where(
                schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid
                == label_uuid
            )
            .execution_options(synchronize_session=False)
        )

    def __get_case_uuid_from_document_uuid(self, document_uuid: UUID) -> UUID:
        case_uuid = self.session.execute(
            sql.select(schema.Case.uuid)
            .select_from(
                sql.join(
                    schema.File,
                    schema.Case,
                    schema.Case.id == schema.File.case_id,
                )
            )
            .where(
                sql.and_(
                    schema.File.uuid == document_uuid,
                    schema.Case.uuid.isnot(None),
                )
            )
        ).scalar()

        # Guarded by the WHERE in the query above
        assert case_uuid

        return case_uuid

    def __get_all_case_uuids_for_label(self, case_uuid, label_uuid: UUID):
        bibliotheek_kenmerken_query = (
            sql.select(schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id)
            .where(schema.ZaaktypeKenmerk.uuid == label_uuid)
            .scalar_subquery()
        )

        subcase_query = (
            sql.select(
                schema.Case.id,
                schema.Case.pid,
                schema.Case.uuid,
                postgresql.array([schema.Case.id]).label("path"),
                sql.literal(False).label("referential"),
                sql.case(
                    (
                        schema.ZaaktypeDocumentKenmerkenMap.case_document_id.isnot(
                            None
                        ),
                        True,
                    ),
                    else_=False,
                ).label("case_has_attribute"),
            )
            .select_from(
                sql.join(
                    schema.Case,
                    schema.ZaaktypeDocumentKenmerkenMap,
                    sql.and_(
                        schema.ZaaktypeDocumentKenmerkenMap.zaaktype_node_id
                        == schema.Case.zaaktype_node_id,
                        schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                        == bibliotheek_kenmerken_query,
                    ),
                    isouter=True,
                )
            )
            .where(schema.Case.uuid == case_uuid)
            .cte(recursive=True)
        )

        subcase_query = subcase_query.union(
            sql.select(
                schema.Case.id,
                schema.Case.pid,
                schema.Case.uuid,
                (
                    postgresql.array([schema.Case.id]) + subcase_query.c.path
                ).label("path"),
                sql.func.coalesce(
                    schema.ZaaktypeDocumentKenmerkenMap.referential, True
                ).label("referential"),
                sql.case(
                    (
                        schema.ZaaktypeDocumentKenmerkenMap.case_document_id.isnot(
                            None
                        ),
                        True,
                    ),
                    else_=False,
                ).label("case_has_attribute"),
            )
            .select_from(
                sql.join(
                    schema.Case,
                    schema.ZaaktypeDocumentKenmerkenMap,
                    sql.and_(
                        schema.ZaaktypeDocumentKenmerkenMap.zaaktype_node_id
                        == schema.Case.zaaktype_node_id,
                        schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                        == bibliotheek_kenmerken_query,
                    ),
                    isouter=True,
                )
            )
            .where(
                sql.and_(
                    subcase_query.c.id == schema.Case.pid,
                    sql.func.coalesce(
                        schema.ZaaktypeDocumentKenmerkenMap.referential, True
                    ).is_(True),
                    sql.not_(
                        subcase_query.c.path.contains(
                            postgresql.array([schema.Case.id])
                        )
                    ),
                ),
            )
        )

        case_uuids = self.session.execute(
            sql.select(subcase_query.c.uuid)
            .select_from(subcase_query)
            .where(subcase_query.c.case_has_attribute.is_(True))
        ).fetchall()

        return [row.uuid for row in case_uuids]

    def __diff_document_labels(
        self, old_labels: Sequence, new_labels: Sequence
    ):
        labels_to_add = []
        labels_to_remove = []

        for old_label in old_labels:
            if old_label not in new_labels:
                labels_to_remove.append(old_label)

        for new_label in new_labels:
            if new_label not in old_labels:
                labels_to_add.append(new_label)

        return (labels_to_add, labels_to_remove)

    def _set_labels(self, event: Event, user_info=None, dry_run=False):
        """(Re)set labels on a document"""
        old_labels = event.previous_value("labels")
        new_labels = event.new_value("labels")

        (labels_to_add, labels_to_remove) = self.__diff_document_labels(
            old_labels, new_labels
        )

        case_uuid_query = self.__get_case_uuid_from_document_uuid(
            document_uuid=event.entity_id
        )

        for document_label in labels_to_add:
            # Get all cases the label applies to
            case_uuids = self.__get_all_case_uuids_for_label(
                case_uuid=case_uuid_query, label_uuid=document_label["uuid"]
            )

            for case_uuid in case_uuids:
                self.__apply_label(
                    document_uuid=event.entity_id,
                    label_uuid=document_label["uuid"],
                    case_uuid=case_uuid,
                )

        for document_label in labels_to_remove:
            # Get all cases the label applies to
            case_uuids = self.__get_all_case_uuids_for_label(
                case_uuid=case_uuid_query, label_uuid=document_label["uuid"]
            )

            for case_uuid in case_uuids:
                self.__remove_label(
                    document_uuid=event.entity_id,
                    label_uuid=document_label["uuid"],
                    case_uuid=case_uuid,
                )

        if len(new_labels) == 1 and len(old_labels) == 0:
            # Default only apply if there is a single label, and only when adding it.
            # Otherwise, we would need to handle conflicting settings etc.
            self._apply_label_defaults(
                document_uuid=event.entity_id, label_uuid=new_labels[0]["uuid"]
            )
        else:
            self.logger.debug(
                "Not applying metadata from label defaults. "
                f"{len(new_labels)=}, {len(old_labels)=}"
            )

    def _create_preview(
        self, event: Event, user_info=None, dry_run=False
    ) -> None:
        """
        Create a preview for the document and store its info in the database.
        """

        preview_uuid = uuid4()

        exec_result = self.session.execute(
            sql.insert(schema.Filestore)
            .values(
                {
                    "uuid": preview_uuid,
                    "original_name": f"preview_{event.entity_id}.pdf",
                    "size": 0,
                    "mimetype": "application/pdf",
                    "storage_location": [],
                    "md5": "",
                    "is_archivable": True,
                    # We assume files we generate are virus-scanned
                    "virus_scan_status": "ok",
                },
            )
            .returning(schema.Filestore.id)
        )
        row = exec_result.fetchone()
        filestore_id = row[0]

        try:
            # By inserting this row, other transactions doing the same will
            # block until we're done, and then raise a "duplicate key" error.
            #
            # This is done so we don't have to do a conversion if it's going
            # to be discarded (because it's a dupe) anyway.
            self.session.execute(
                sql.insert(schema.FileDerivative).values(
                    {
                        "file_id": sql.select(schema.File.id)
                        .where(
                            sql.and_(
                                schema.File.uuid == event.entity_id,
                                schema.File.active_version.is_(True),
                            )
                        )
                        .scalar_subquery(),
                        "filestore_id": filestore_id,
                        "type": "pdf",
                        "max_width": 0,
                        "max_height": 0,
                    }
                )
            )
        except sql_exc.IntegrityError:
            self.logger.debug(
                "Duplicate key when trying to create derivative for "
                f"{event.entity_id}. Most likely cause: multiple requested at "
                "the same time. Aborting."
            )
            return

        storage_infra = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        converter_infra = cast(
            ConverterWrapper, self._get_infrastructure("converter")
        )

        with (
            tempfile.TemporaryFile() as input_handle,
            tempfile.TemporaryFile() as output_handle,
        ):
            storage_infra.download_file(
                destination=input_handle,
                file_uuid=event.entity_data["store_uuid"],
                storage_location=event.entity_data["storage_location"][0],
            )
            input_handle.seek(0, os.SEEK_SET)

            converter_infra.convert_file_handle(
                to_type="application/pdf",
                input_handle=input_handle,
                output_handle=output_handle,
                options={},
            )
            output_handle.seek(0, os.SEEK_SET)

            storage_info = storage_infra.upload(
                uuid=preview_uuid,
                file_handle=output_handle,
            )

        self.session.execute(
            sql.update(schema.Filestore)
            .values(
                md5=storage_info["md5"],
                size=storage_info["size"],
                storage_location=[storage_info["storage_location"]],
            )
            .where(schema.Filestore.id == filestore_id)
        )

    def _create_thumbnail(self, event: Event, user_info=None, dry_run=False):
        """
        Create a thumbnail for the document and store its info in the database.
        """

        thumbnail_uuid = uuid4()

        exec_result = self.session.execute(
            sql.insert(schema.Filestore)
            .values(
                {
                    "uuid": thumbnail_uuid,
                    "original_name": f"thumbnail_{event.entity_id}.png",
                    "size": 0,
                    "mimetype": "image/png",
                    "storage_location": [],
                    "md5": "",
                    "is_archivable": True,
                    # We assume files we generate are virus-scanned
                    "virus_scan_status": "ok",
                },
            )
            .returning(schema.Filestore.id)
        )
        row = exec_result.fetchone()
        filestore_id = row[0]

        try:
            # By inserting this row, other transactions doing the same will
            # block until we're done, and then raise a "duplicate key" error.
            #
            # This is done so we don't have to do a conversion if it's going
            # to be discarded (because it's a dupe) anyway.
            self.session.execute(
                sql.insert(schema.FileDerivative).values(
                    {
                        "file_id": sql.select(schema.File.id)
                        .where(
                            sql.and_(
                                schema.File.uuid == event.entity_id,
                                schema.File.active_version.is_(True),
                            )
                        )
                        .scalar_subquery(),
                        "filestore_id": filestore_id,
                        "type": "thumbnail",
                        "max_width": THUMBNAIL_MAX_WIDTH,
                        "max_height": THUMBNAIL_MAX_HEIGHT,
                    }
                )
            )
        except sql_exc.IntegrityError:
            self.logger.debug(
                "Duplicate key when trying to create derivative for "
                f"{event.entity_id}. Most likely cause: multiple requested at "
                "the same time. Aborting."
            )
            return

        storage_infra = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        converter_infra = cast(
            ConverterWrapper, self._get_infrastructure("converter")
        )

        with (
            tempfile.TemporaryFile() as input_handle,
            tempfile.TemporaryFile() as output_handle,
        ):
            extension: str = event.entity_data["extension"] or ""
            extension = extension.lower()
            mimetype = event.entity_data["mimetype"]

            if (extension, mimetype) in THUMBNAILABLE_MIMETYPES_DIRECT:
                storage_uuid = event.entity_data["store_uuid"]
                storage_location = event.entity_data["storage_location"][0]
            else:
                storage_uuid = event.entity_data["preview_uuid"]
                storage_location = event.entity_data[
                    "preview_storage_location"
                ][0]

            storage_infra.download_file(
                destination=input_handle,
                file_uuid=storage_uuid,
                storage_location=storage_location,
            )
            input_handle.seek(0, os.SEEK_SET)

            converter_infra.convert_file_handle(
                to_type="image/png",
                input_handle=input_handle,
                output_handle=output_handle,
                options={
                    "width": THUMBNAIL_MAX_WIDTH,
                    "height": THUMBNAIL_MAX_HEIGHT,
                },
            )
            output_handle.seek(0, os.SEEK_SET)

            storage_info = storage_infra.upload(
                uuid=thumbnail_uuid,
                file_handle=output_handle,
            )

        self.session.execute(
            sql.update(schema.Filestore)
            .values(
                md5=storage_info["md5"],
                size=storage_info["size"],
                storage_location=[storage_info["storage_location"]],
            )
            .where(schema.Filestore.id == filestore_id)
        )

    def _set_search_terms(
        self, event: Event, user_info=None, dry_run=False
    ) -> None:
        storage_uuid = event.entity_data["store_uuid"]
        storage_location = event.entity_data["storage_location"][0]

        storage_infra = cast(s3.S3Wrapper, self._get_infrastructure("s3"))

        tika_infra = cast(TikaWrapper, self._get_infrastructure("tika"))

        with tempfile.TemporaryFile() as temp_handle:
            mimetype = event.entity_data["mimetype"]

            storage_infra.download_file(
                destination=temp_handle,
                file_uuid=storage_uuid,
                storage_location=storage_location,
            )
            temp_handle.seek(0, os.SEEK_SET)

            document_text = tika_infra.get_text(
                mime_type=mimetype, handle=temp_handle
            )

        language = LANGUAGE_DETECTOR.detect_language_of(document_text)

        # The language detector is created from a list of supported languages
        # so detected language will always be in the LANGUAGE_MAP (or None)
        detected_language = (
            LANGUAGE_MAP[language.iso_code_639_1] if language else "dutch"
        )

        self.logger.debug(f"Detected language: {detected_language}")

        update_stmt = (
            sql.update(schema.File)
            .values(
                search_index=sql.func.to_tsvector(
                    detected_language,
                    document_text,
                ),
                search_term=sql.func.concat_ws(
                    " ",
                    schema.File.name,
                    sql.select(schema.FileMetaData.description)
                    .where(schema.File.metadata_id == schema.FileMetaData.id)
                    .scalar_subquery(),
                ),
            )
            .where(
                sql.and_(
                    schema.File.uuid == event.entity_id,
                    schema.File.active_version.is_(True),
                )
            )
        )
        self.session.execute(update_stmt)

    def _set_search_terms_delayed(
        self, event: Event, user_info=None, dry_run=False
    ) -> None:
        """Event is handled by the document processing consumer"""

    def _move_document(
        self, event: Event, user_info=None, dry_run=False
    ) -> None:
        """Move document to directory"""
        current_changes = self._prepare_event_values(event)
        directory_uuid = current_changes.get("directory_uuid", None)
        directory_id = self._get_directory_id(directory_uuid)
        # Update directory_id for given document
        self.session.execute(
            sql.update(schema.File)
            .values({schema.File.directory_id: directory_id})
            .where(
                sql.and_(
                    schema.File.date_deleted.is_(None),
                    schema.File.uuid == event.entity_id,
                )
            )
            .execution_options(synchronize_session=False)
        )
        self.session.commit()

    def _apply_label_defaults(self, document_uuid: UUID, label_uuid: UUID):
        self.logger.debug(
            f"Applying label defaults: {document_uuid=}, {label_uuid=}"
        )

        label = self.session.execute(
            sql.select(
                schema.ZaaktypeDocumentKenmerkenMap.show_on_pip,
                schema.ZaaktypeDocumentKenmerkenMap.show_on_website,
                schema.FileMetaData.trust_level,
                schema.FileMetaData.document_category,
                schema.FileMetaData.origin,
            )
            .select_from(
                sql.join(
                    schema.ZaaktypeDocumentKenmerkenMap,
                    schema.BibliotheekKenmerk,
                    schema.ZaaktypeDocumentKenmerkenMap.bibliotheek_kenmerken_id
                    == schema.BibliotheekKenmerk.id,
                ).join(
                    schema.FileMetaData,
                    schema.FileMetaData.id
                    == schema.BibliotheekKenmerk.file_metadata_id,
                )
            )
            .where(
                schema.ZaaktypeDocumentKenmerkenMap.case_document_uuid
                == label_uuid
            )
        ).fetchone()

        if label:
            new_metadata = {
                "confidentiality": label.trust_level,
                "document_category": label.document_category,
                "origin": label.origin,
            }

            metadata_id = self._update_document_metadata(
                document_uuid=document_uuid, changes=new_metadata
            )
            confidential = (
                True
                if label.trust_level in CONFIDENTIAL_TRUST_LEVELS
                else False
            )

            self.session.execute(
                sql.update(schema.File)
                .where(
                    sql.and_(
                        schema.File.uuid == document_uuid,
                        schema.File.active_version.is_(True),
                    )
                )
                .values(
                    publish_pip=label.show_on_pip,
                    publish_website=label.show_on_website,
                    metadata_id=metadata_id,
                    confidential=confidential,
                )
                .execution_options(synchronize_session=False)
            )

        return

    def _get_file_row_by_document_uuid(self, document_uuid):
        file = self.session.execute(
            sql.select(
                schema.File.id,
                schema.File.uuid,
                schema.File.version,
                schema.File.root_file_id,
                schema.File.date_created,
                schema.File.lock_timestamp,
                schema.File.lock_subject_id,
                schema.File.lock_subject_name,
                schema.File.shared,
            )
            .where(
                sql.and_(
                    schema.File.uuid == document_uuid,
                    schema.File.active_version.is_(True),
                )
            )
            .with_for_update()
            .order_by(sql.desc(schema.File.version))
        ).fetchone()

        if file is None:
            raise exceptions.Conflict(
                f"Document with UUID '{document_uuid}' not found",
                "document/update_document/not_found",
            )
        return file

    def _editor_update_new_version_check(self, file) -> bool:
        try:
            if datetime.now(tz=timezone.utc) - file.date_created < timedelta(
                minutes=5
            ):
                return False

        except TypeError:
            pass

        return True

    def _document_update_from_editor(
        self,
        store_uuid,
        basename,
        extension,
        size,
        mimetype,
        storage_location,
        md5,
        is_archivable,
        document_uuid,
        virus_scan_status,
    ):
        file = self._get_file_row_by_document_uuid(document_uuid=document_uuid)
        new_version_needed = self._editor_update_new_version_check(file)

        if not new_version_needed:
            try:
                filestore_data = self._get_filestore_by_uuid(store_uuid)
                filestore_id = filestore_data.id
            except exceptions.NotFound:
                exec_result = self.session.execute(
                    sql.insert(schema.Filestore).values(
                        {
                            "uuid": store_uuid,
                            "original_name": basename + extension,
                            "size": size,
                            "mimetype": mimetype,
                            "storage_location": [storage_location],
                            "md5": md5,
                            "is_archivable": is_archivable,
                            "virus_scan_status": virus_scan_status,
                        },
                    )
                )
                filestore_id = exec_result.inserted_primary_key[0]

            # Update current file date and filestore without increasing version
            self.session.execute(
                sql.update(schema.File)
                .where(schema.File.id == file.id)
                .values(
                    filestore_id=filestore_id,
                    date_modified=datetime.now(timezone.utc),
                )
                .execution_options(synchronize_session=False)
            )

            # Delete FileDerivatives to regenerate preview and thumbnail
            self.session.execute(
                sql.delete(schema.FileDerivative)
                .where(
                    schema.FileDerivative.file_id == file.id,
                )
                .execution_options(synchronize_session=False)
            )

        return new_version_needed

    def get_document_uuids_by_directory_id(
        self,
        directory_id: int,
    ) -> list[UUID]:
        """
        Return a list of document uuids that are part of a directory,
        based on the directory id.
        """
        document_query = sql.select(
            schema.File.uuid.label("document_uuid"),
        ).where(
            sql.and_(
                schema.File.active_version.is_(True),
                schema.File.directory_id == directory_id,
                schema.File.destroyed.is_(False),
            )
        )

        document_rows = self.session.execute(document_query).fetchall()

        return [item.document_uuid for item in document_rows]

    def get_document_uuids_by_case_uuid(
        self,
        case_uuid: UUID,
    ) -> list[UUID]:
        """
        Return a list of document uuids that are part of a case,
        based on the case uuid.
        """
        document_query = (
            sql.select(
                schema.File.uuid.label("document_uuid"),
            )
            .select_from(
                sql.join(
                    schema.File,
                    schema.Case,
                    schema.File.case_id == schema.Case.id,
                )
            )
            .where(
                sql.and_(
                    schema.File.active_version.is_(True),
                    schema.Case.uuid == case_uuid,
                    schema.File.destroyed.is_(False),
                )
            )
        )

        document_rows = self.session.execute(document_query).fetchall()

        return [item.document_uuid for item in document_rows]

    def is_locked(
        self,
        document_lock,
    ) -> bool:
        if document_lock["case_id"] is None:
            return False

        if document_lock["timestamp"] is None:
            return False

        if datetime.now(tz=UTC) > document_lock["timestamp"]:
            return False

        return True

    def _acquire_lock(
        self,
        event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run=False,
    ):
        changes = event.format_changes()
        document_uuid = event.entity_id
        lock = changes["lock"]

        self.session.execute(
            sql.update(schema.File)
            .values(
                lock_timestamp=lock["timestamp"],
                lock_subject_id=lock["user_uuid"],
                lock_subject_name=lock["user_display_name"],
                shared=lock["shared"],
            )
            .where(
                sql.and_(
                    schema.File.active_version.is_(True),
                    schema.File.uuid == document_uuid,
                )
            )
        )

    def _release_lock(
        self,
        event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run=False,
    ):
        changes = event.format_changes()
        document_uuid = event.entity_id

        lock = changes["lock"]

        self.session.execute(
            sql.update(schema.File)
            .values(
                lock_timestamp=lock["timestamp"],
                lock_subject_id=lock["user_uuid"],
                lock_subject_name=lock["user_display_name"],
                shared=lock["shared"],
            )
            .where(
                sql.and_(
                    schema.File.active_version.is_(True),
                    schema.File.uuid == document_uuid,
                )
            )
        )

    def _extend_lock(
        self,
        event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run=False,
    ):
        changes = event.format_changes()
        document_uuid = event.entity_id

        lock = changes["lock"]

        self.session.execute(
            sql.update(schema.File)
            .values(lock_timestamp=lock["timestamp"], shared=lock["shared"])
            .where(
                sql.and_(
                    schema.File.active_version.is_(True),
                    schema.File.uuid == document_uuid,
                )
            )
        )
