# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from minty.entity import Entity
from pydantic.v1 import Field


class Tenant(Entity):
    "Represents the tenant for current instance"

    name: str = Field(..., title="Name of the tenant")
    template: str | None = Field(None, title="Name of the active template")

    entity_type = "tenant"
