# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import mimetypes
import minty.cqrs
from ..entities.style_configuration import (
    MAX_FILE_SIZE,
    VALID_CONFIG_FILE_TYPES,
)
from ..repositories import StyleConfigurationRepository
from minty.exceptions import ValidationError
from os import path
from pydantic.v1 import validate_arguments
from typing import cast
from uuid import uuid4
from zipfile import ZipFile


class UploadTemplate(minty.cqrs.SplitCommandBase):
    name = "upload_template"

    def create_style_configuration(
        self, tenant, name, extension, contents, mimetype
    ):
        repo = cast(
            StyleConfigurationRepository,
            self.get_repository("style_configuration"),
        )

        repo.create_style_configuration(
            uuid=uuid4(),
            tenant=tenant,
            name=name,
            extension=extension,
            content=contents,
            mimetype=mimetype,
        )
        repo.save()

    @validate_arguments(config=dict(arbitrary_types_allowed=True))
    def __call__(
        self,
        file,
        file_name,
        tenant: str = None,
    ):
        if tenant is None:
            tenant = self.cmd.context

        repo = cast(
            StyleConfigurationRepository,
            self.get_repository("style_configuration"),
        )
        repo.validate_tenant(tenant)

        # Store entire zip file with all files
        zip_mimetype = "application/zip"
        basename = path.splitext(file_name)[0]
        extension = path.splitext(file_name)[1].split(".")[-1]

        self.create_style_configuration(
            tenant=tenant,
            name=basename,
            extension=extension,
            contents=base64.b64encode(file.read()).decode("utf-8"),
            mimetype=zip_mimetype,
        )

        with ZipFile(file, "r") as zip:
            size = (
                sum([zinfo.compress_size for zinfo in zip.filelist]) / 1000000
            )

            if size > MAX_FILE_SIZE:
                raise ValidationError("Maximum allowed file size is 10MB.")

            file_list = zip.namelist()

            for file_name in file_list:
                # Extract filename without path
                name_without_path = file_name.split("/")[-1]
                if name_without_path in VALID_CONFIG_FILE_TYPES:
                    with zip.open(file_name) as f:
                        mimetype, _ = mimetypes.guess_type(file_name)
                        b = f.read()
                        encoded_contents = base64.b64encode(b).decode("utf-8")
                        self.create_style_configuration(
                            tenant=tenant,
                            name=name_without_path.rsplit(".", 1)[0],
                            extension=name_without_path.rsplit(".", 1)[1],
                            contents=encoded_contents,
                            mimetype=mimetype,
                        )
