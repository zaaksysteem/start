# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import StyleConfigurationRepository
from typing import Optional, cast


class GetTenants(minty.cqrs.SplitQueryBase):
    name = "get_tenants"

    def __call__(self):
        repo = cast(
            StyleConfigurationRepository,
            self.get_repository("style_configuration"),
        )
        return repo.get_tenants()


class GetContent(minty.cqrs.SplitQueryBase):
    name = "get_content"

    def __call__(self, name: str, tenant: Optional[str] = None):
        if tenant is None:
            tenant = self.qry.context

        repo = cast(
            StyleConfigurationRepository,
            self.get_repository("style_configuration"),
        )
        return repo.get_content(name=name, tenant=tenant)
