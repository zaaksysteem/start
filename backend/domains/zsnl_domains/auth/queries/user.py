# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from ..entities import User
from ..repositories import UserRepository
from pydantic.v1 import validate_arguments
from typing import cast


class FindUserByName(minty.cqrs.SplitQueryBase):
    name = "find_user_by_name"

    @validate_arguments
    def __call__(
        self,
        name: str,
    ) -> User | None:
        """
        Find user by name.

        Args:
            name (str): The name

        Returns:
            User or None
        """

        self.logger.debug(f"Find user by name {name}")

        repo = cast(
            UserRepository,
            self.get_repository("user"),
        )

        return repo.find_user_by_name(
            name=name,
        )


class FindActiveInterface(minty.cqrs.SplitQueryBase):
    name = "find_active_interface"

    @validate_arguments
    def __call__(
        self,
    ) -> bool:
        """
        Find active interface.

        Returns:
            true if active interface exists
            false otherwise
        """

        self.logger.debug("Find active interface")

        repo = cast(
            UserRepository,
            self.get_repository("user"),
        )

        return repo.find_active_interface()
