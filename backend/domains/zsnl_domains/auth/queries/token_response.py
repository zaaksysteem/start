# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from ..entities import TokenResponse
from ..repositories import TokenResponseRepository
from pydantic.v1 import validate_arguments
from typing import cast


class GetToken(minty.cqrs.SplitQueryBase):
    name = "get_token"

    @validate_arguments
    def __call__(
        self,
        code: str,
        hostname: str,
        client_id: str,
        client_secret: str,
        domain: str,
    ) -> TokenResponse:
        """
        Exchange an Authorization Code for a Token.

        Args:
            code (str): The Authorization Code received from the authorize call
            hostname (str): The hostname to redirect to
            client_id (str): The application's client ID
            client_secret (str): The application's client secret
            domain (str): The domain of the Auth0 tenant

        Returns:
            TokenResponse
        """

        self.logger.debug(
            f"GetToken code = {code}, hostname = {hostname}, client_id = "
            f"{client_id}, client_secret = {client_secret}, domain = {domain}"
            f" and self.context = {self.context}"
        )

        repo = cast(
            TokenResponseRepository,
            self.get_repository("token_response"),
        )

        return repo.get_token(
            code=code,
            hostname=hostname,
            client_id=client_id,
            client_secret=client_secret,
            domain=domain,
        )
