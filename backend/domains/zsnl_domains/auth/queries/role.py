# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from ..entities import Role
from ..repositories import RoleRepository
from minty.entity import EntityCollection
from pydantic.v1 import validate_arguments
from typing import cast


class GetPermissionsForRoles(minty.cqrs.SplitQueryBase):
    name = "get_permissions_for_roles"

    @validate_arguments
    def __call__(
        self,
        role_ids: list[str],
    ) -> EntityCollection[Role] | None:
        """
        Find all roles for a list of ids.

        Args:
            role_ids (list(str)): the role ids

        Returns:
            All roles or None
        """

        self.logger.debug(f"GetPermissionsForRole role_ids = {role_ids}")

        repo = cast(
            RoleRepository,
            self.get_repository("role"),
        )

        return repo.get_permissions_for_roles(
            role_ids=role_ids,
        )
