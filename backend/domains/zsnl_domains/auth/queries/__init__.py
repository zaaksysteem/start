# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .role import GetPermissionsForRoles
from .token_response import GetToken
from .user import FindActiveInterface, FindUserByName

AUTH_QUERIES = minty.cqrs.build_query_lookup_table(
    queries={
        FindActiveInterface,
        FindUserByName,
        GetPermissionsForRoles,
        GetToken,
    }
)
