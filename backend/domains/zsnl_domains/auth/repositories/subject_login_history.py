# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import SubjectLoginHistory
from ..exceptions import CreateSubjectLoginHistoryError
from datetime import datetime, timezone
from minty.cqrs import Event, UserInfo
from sqlalchemy import sql
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from uuid import UUID
from zsnl_domains.database import schema


class SubjectLoginHistoryRepository(ZaaksysteemRepositoryBase):
    _for_entity = "SubjectLoginHistory"
    _events_to_calls = {
        "Auth0SubjectLoginHistoryCreated": "_create_subject_login_history",
    }

    def create_subject_login_history(
        self,
        ip: str,
        subject_username: str,
        subject_id: int,
        subject_uuid: UUID,
        success: bool,
    ) -> SubjectLoginHistory:
        """Create a subject login history entry.

        Args:
            ip (str): IP address of the client performing the login attempt
            subject_username (str): Name of subject performing the
                                    login attempt
            subject_id (int): ID of the subject performing the login attempt
            subject_uuid (uuid): Internal identifier of subject performing
                                 the login attempt
            success (bool) : Indication of whether the login attempt was
                             successful

        Returns:
            SubjectLoginHistory

        Raises:
            CreateSubjectLoginHistoryError: If creating a new entry fails.
        """

        self.logger.debug(
            f"Create a subject login history entry with ip {ip}, "
            f"subject_username {subject_username}, subject_id {subject_id}, "
            f"subject_uuid {subject_uuid} and success {success}"
        )

        subject_login_history: SubjectLoginHistory = SubjectLoginHistory(
            ip=ip,
            subject_id=subject_id,
            subject_uuid=subject_uuid,
            success=success,
        )
        subject_login_history.create(
            ip=ip,
            subject_username=subject_username,
            subject_id=subject_id,
            subject_uuid=subject_uuid,
            success=success,
            method="AUTH0",
            _event_service=self.event_service,
        )
        return subject_login_history

    def _create_subject_login_history(
        self, event: Event, user_info: UserInfo, dry_run: bool = False
    ):
        changes = event.format_changes()

        try:
            self.session.execute(
                sql.insert(schema.SubjectLoginHistory).values(
                    ip=str(changes.get("ip")),
                    subject_id=changes.get("subject_id"),
                    subject_uuid=changes.get("subject_uuid"),
                    success=changes.get("success"),
                    method=changes.get("method"),
                    date_attempt=datetime.now(timezone.utc),
                )
            )
        except (SQLAlchemyError, DBAPIError) as err:
            raise CreateSubjectLoginHistoryError(
                changes.get("subject_username")
            ) from err
