# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Event


class EventRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Event"
    _events_to_calls = {}

    def get_event(self):
        return Event(
            username="",
            interface_name="",
            _event_service=self.event_service,
        )
