# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic.v1 import Field
from typing import Self
from uuid import UUID


class User(Entity):
    "User"

    entity_id__fields: list[str] = ["uuid"]

    id: int = Field(..., title="Internal Identifier for the user")
    uuid: UUID = Field(..., title="Internal Identifier for the user")
    name: str = Field(..., title="Name of the user")
    login_entity_id: str = Field(..., title="Id of the user")
    role_ids: list[int] = Field(..., title="Role ids of subject")
    active: bool = Field(..., title="Indication if user is active")
    date_deleted: datetime | None = Field(
        default=None, title="Deleted date for the user"
    )

    entity_type = "user"

    @classmethod
    def create(cls, **kwargs) -> Self:
        user: Self = cls(**kwargs)
        return user
