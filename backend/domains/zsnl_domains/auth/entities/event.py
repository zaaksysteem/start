# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic.v1 import Field


class Event(Entity):
    "Event"

    entity_type = "event"

    username: str | None = Field(default=None, title="The username")
    interface_name: str | None = Field(
        default=None, title="The interface name"
    )

    @Entity.event(name="Auth0InterfaceNotActive", fire_always=True)
    def add_event_auth0_interface_not_active(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        self.username = username
        self.interface_name = interface_name

    @Entity.event(name="Auth0UserNotActive", fire_always=True)
    def add_event_auth0_user_not_active(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        self.username = username
        self.interface_name = interface_name

    @Entity.event(name="Auth0UserMarkedAsDeleted", fire_always=True)
    def add_event_auth0_user_marked_as_deleted(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        self.username = username
        self.interface_name = interface_name
