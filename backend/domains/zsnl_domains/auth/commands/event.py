# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty.cqrs
import minty.exceptions
from ..repositories import EventRepository
from pydantic.v1 import validate_arguments
from typing import cast

logger = logging.getLogger(__name__)


class AddEventAuth0yInterfaceNotActive(minty.cqrs.SplitCommandBase):
    name = "add_event_auth0_interface_not_active"

    @validate_arguments
    def __call__(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        logger.debug(
            f"Add event auth0 interface not active. "
            f"Username '{username}', interface_name '{interface_name}'"
            f"and context '{self.context}'"
        )

        repo = cast(
            EventRepository,
            self.get_repository("event"),
        )

        event = repo.get_event()

        event.add_event_auth0_interface_not_active(
            username=username,
            interface_name=interface_name,
        )


class AddEventAuth0UserNotActive(minty.cqrs.SplitCommandBase):
    name = "add_event_auth0_user_not_active"

    @validate_arguments
    def __call__(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        logger.debug(
            f"Add event auth0 user not active. "
            f"Username '{username}', interface_name '{interface_name}'"
            f"and context '{self.context}'"
        )

        repo = cast(
            EventRepository,
            self.get_repository("event"),
        )

        event = repo.get_event()

        event.add_event_auth0_user_not_active(
            username=username,
            interface_name=interface_name,
        )


class AddEventAuth0yUserMarkedAsDeleted(minty.cqrs.SplitCommandBase):
    name = "add_event_auth0_user_marked_as_deleted"

    @validate_arguments
    def __call__(
        self,
        username: str | None,
        interface_name: str | None,
    ):
        logger.debug(
            f"Add event auth0 user marked as deleted. "
            f"Username '{username}', interface_name '{interface_name}'"
            f"and context '{self.context}'"
        )

        repo = cast(
            EventRepository,
            self.get_repository("event"),
        )

        event = repo.get_event()

        event.add_event_auth0_user_marked_as_deleted(
            username=username,
            interface_name=interface_name,
        )
