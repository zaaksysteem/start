# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import minty.cqrs
import minty.exceptions
from ..repositories import SubjectRepository
from pydantic.v1 import validate_arguments
from typing import cast

logger = logging.getLogger(__name__)


class CreateUser(minty.cqrs.SplitCommandBase):
    name = "create_user"

    @validate_arguments
    def __call__(
        self,
        username: str,
    ):
        logger.debug(
            f"CreateUser for username {username} and context {self.context}"
        )

        repo = cast(
            SubjectRepository,
            self.get_repository("subject"),
        )

        repo.create_user(username=username)

        repo.save()
