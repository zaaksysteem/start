# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


def split_comma_separated_field(v: str | None) -> list[str] | None:
    """
    Validator for list fields in Pydantic models that turns strings with
    comma-separated values into lists of strings.

    Can be used as follows:

    ```
    class Something(BaseModel):
        some_field: List[str] = Field(...)

        _validate_some_field = validator(
            "some_field",
            allow_reuse=True,
            pre=True,
        )(split_comma_separated_field)

    v = Something(some_field="some,words,here")
    assert v.some_field == ["some", "words", "here"]
    ```
    """

    if v is None:
        return
    return v.split(",")


def split_comma_separated_field_dict(v: str | None) -> list[str] | None:
    """
    Validator for list of objects fields in Pydantic models that turns
    objects as strings with comma-separated values into lists of strings.
    """
    if v is None:
        return
    v = v.replace("},{", "}},{{")
    return v.split("},{")
