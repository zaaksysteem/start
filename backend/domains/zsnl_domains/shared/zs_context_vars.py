# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import contextvars

rule_engine: contextvars.ContextVar = contextvars.ContextVar("rule_engine")
template_service: contextvars.ContextVar = contextvars.ContextVar(
    "template_service"
)


def try_get_template_service():
    try:
        return template_service.get()
    except LookupError:
        return None
