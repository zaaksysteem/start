# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import logging
import minty.cqrs
import operator
from .filters import apply_comparison_filter, apply_operator_to_filters
from .types import (
    CocNumberValidation,
    ComparisonFilterCondition,
    ComparisonFilterConditionStr,
    DepartmentRoleTypedDict,
    FilterMultipleValuesWithOperator,
    FilterOperator,
    RequestorType,
    ValidGender,
)
from pydantic.v1 import BaseModel, Field
from sqlalchemy import intersect, sql, union
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql import expression, selectable
from typing import Any, Callable, Final
from uuid import UUID
from zsnl_domains.admin.catalog.entities.versioned_casetype import (
    DesignationOfConfidentialityType,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
    ValidTypeOfArchiving,
)
from zsnl_domains.shared.util import (
    escape_term_for_like,
    get_operator_values_from_filter,
    prepare_search_term,
)

logger = logging.getLogger(__name__)


class CaseFilter(BaseModel):
    operator: str | None = Field(default=None, alias="operator")

    filter_status: set[ValidCaseStatus] | None = Field(
        default=None, alias="attributes.status"
    )

    case_type_uuids: set[UUID] | None = Field(
        default=None, alias="relationship.case_type.id"
    )

    assignee_uuids: set[UUID] | None = Field(
        default=None, alias="relationship.assignee.id"
    )
    coordinator_uuids: set[UUID] | None = Field(
        default=None, alias="relationship.coordinator.id"
    )
    requestor_uuids: set[UUID] | None = Field(
        default=None, alias="relationship.requestor.id"
    )

    filter_registration_date: None | (list[ComparisonFilterConditionStr]) = (
        Field(default=None, alias="attributes.registration_date")
    )

    filter_completion_date: None | (list[ComparisonFilterConditionStr]) = (
        Field(default=None, alias="attributes.completion_date")
    )

    filter_last_modified: None | (list[ComparisonFilterConditionStr]) = Field(
        default=None, alias="attributes.last_modified"
    )

    filter_payment_status: set[ValidCasePaymentStatus] | None = Field(
        default=None, alias="attributes.payment_status"
    )
    filter_channel_of_contact: set[ValidContactChannel] | None = Field(
        default=None, alias="attributes.channel_of_contact"
    )
    filter_confidentiality: set[ValidCaseConfidentiality] | None = Field(
        default=None, alias="attributes.confidentiality"
    )
    filter_archival_state: set[ValidCaseArchivalState] | None = Field(
        default=None, alias="attributes.archival_state"
    )
    filter_retention_period_source_date: (
        None | (list[ValidCaseRetentionPeriodSourceDate])
    ) = Field(default=None, alias="attributes.retention_period_source_date")

    filter_result: set[ValidCaseResult] | None = Field(
        default=None, alias="attributes.result"
    )
    filter_case_location: FilterMultipleValuesWithOperator[str] | None = Field(
        default=None, alias="attributes.case_location"
    )
    filter_num_unread_messages: None | (list[ComparisonFilterConditionStr]) = (
        Field(default=None, alias="attributes.num_unread_messages")
    )

    filter_num_unaccepted_files: (
        None | (list[ComparisonFilterConditionStr])
    ) = Field(default=None, alias="attributes.num_unaccepted_files")

    filter_num_unaccepted_updates: (
        None | (list[ComparisonFilterConditionStr])
    ) = Field(default=None, alias="attributes.num_unaccepted_updates")

    filter_keyword: FilterMultipleValuesWithOperator[str] | None = Field(
        default=None, alias="keyword"
    )
    filter_period_of_preservation_active: bool | None = Field(
        default=None, alias="attributes.period_of_preservation_active"
    )
    filter_subject: FilterMultipleValuesWithOperator[str] | None = Field(
        default=None, alias="attributes.subject"
    )
    filter_urgency: list[ValidCaseUrgency] | None = Field(
        default=None, alias="attributes.urgency"
    )
    filter_department_role: (
        FilterMultipleValuesWithOperator[DepartmentRoleTypedDict] | None
    ) = Field(default=None, alias="attributes.department_role")
    filter_case_phase: FilterMultipleValuesWithOperator[str] | None = Field(
        default=None, alias="attributes.case_phase"
    )
    filter_case_price: (
        None | FilterMultipleValuesWithOperator[ComparisonFilterConditionStr]
    ) = Field(default=None, alias="attributes.case_price")
    filter_case_custom_number: None | list[str] = Field(
        default=None, alias="attributes.custom_number"
    )

    filter_stalled_until: list[ComparisonFilterConditionStr] | None = Field(
        default=None, alias="attributes.stalled_until"
    )
    filter_stalled_since: list[ComparisonFilterConditionStr] | None = Field(
        default=None, alias="attributes.stalled_since"
    )
    filter_parent_number: list[int] | None = Field(
        default=None, alias="attributes.parent_number"
    )
    filter_target_date: None | (list[ComparisonFilterConditionStr]) = Field(
        default=None, alias="attributes.target_date"
    )
    filter_destruction_date: None | (list[ComparisonFilterConditionStr]) = (
        Field(default=None, alias="attributes.destruction_date")
    )
    filter_external_reference: FilterMultipleValuesWithOperator[str] | None = (
        Field(default=None, alias="attributes.external_reference")
    )
    filter_case_number: list[int] | None = Field(
        default=None, alias="attributes.case_number"
    )
    filter_number_master: list[int] | None = Field(
        default=None, alias="attributes.number_master"
    )
    filter_case_type_identification: list[str] | None = Field(
        default=None, alias="relationship.case_type.identification"
    )
    filter_case_type_confidentiality: (
        None | (list[DesignationOfConfidentialityType])
    ) = Field(default=None, alias="relationship.case_type.confidentiality")
    filter_case_type_version: None | list[UUID] = Field(
        default=None, alias="relationship.case_type.version"
    )
    filter_requestor_post_code: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.post_code"
    )
    filter_requestor_coc_number: None | (list[CocNumberValidation]) = Field(
        default=None, alias="relationship.requestor.coc_number"
    )
    filter_requestor_zipcode: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.zipcode"
    )
    filter_requestor_noble_title: None | list[str] = Field(
        default=None, alias="relationship.requestor.noble_title"
    )
    filter_requestor_coc_location_number: None | list[str] = Field(
        default=None, alias="relationship.requestor.coc_location_number"
    )
    filter_requestor_trade_name: None | list[str] = Field(
        default=None, alias="relationship.requestor.trade_name"
    )
    filter_requestor_investigation: bool | None = Field(
        default=None, alias="relationship.requestor.investigation"
    )
    filter_requestor_is_secret: bool | None = Field(
        default=None, alias="relationship.requestor.is_secret"
    )
    filter_requestor_date_of_birth: (
        None | (list[ComparisonFilterConditionStr])
    ) = Field(default=None, alias="relationship.requestor.date_of_birth")
    filter_requestor_date_of_death: (
        None | (list[ComparisonFilterConditionStr])
    ) = Field(default=None, alias="relationship.requestor.date_of_death")
    filter_requestor_date_of_marriage: (
        None | (list[ComparisonFilterConditionStr])
    ) = Field(default=None, alias="relationship.requestor.date_of_marriage")
    filter_requestor_gender: None | list[ValidGender] = Field(
        default=None, alias="relationship.requestor.gender"
    )
    filter_requestor_department: list[str] | None = Field(
        default=None, alias="relationship.requestor.department"
    )
    filter_requestor_correspondence_zipcode: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.correspondence_zipcode"
    )
    filter_requestor_correspondence_street: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.correspondence_street"
    )
    filter_requestor_correspondence_city: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.correspondence_city"
    )
    filter_requestor_city: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.city"
    )
    filter_requestor_street: None | (list[str]) = Field(
        default=None, alias="relationship.requestor.street"
    )
    filter_custom_fields: None | FilterMultipleValuesWithOperator = Field(
        default=None, alias="attributes.value"
    )
    filter_type_of_archiving: None | list[ValidTypeOfArchiving] = Field(
        default=None, alias="attributes.type_of_archiving"
    )
    filter_custom_object_uuid: None | list[UUID] = Field(
        default=None, alias="relationship.custom_object.id"
    )

    filter_intake: None | bool = Field(default=None, alias="intake")

    class Config:
        validate_all = True
        extra = "forbid"

    # Override "dict" method so empty filters are not included in re-serialization
    def dict(self, *args, **kwargs) -> dict[str, Any]:
        kwargs.pop("exclude_none", None)
        return super().dict(*args, exclude_none=True, **kwargs)


class CaseFilterApplicator:
    def __init__(
        self,
        case_alias: selectable.NamedFromClause,
        zaak_betrokkenen_assignee_alias: selectable.NamedFromClause,
        zaak_betrokkenen_coordinator_alias: selectable.NamedFromClause,
        zaak_betrokkenen_requestor_alias: selectable.NamedFromClause,
        zaak_zaaktype_resultaten: selectable.NamedFromClause,
        zaak_bag: selectable.NamedFromClause,
        natuurlijk_persoon: selectable.NamedFromClause,
    ):
        self.case_alias = case_alias
        self.zaak_betrokkenen_assignee_alias = zaak_betrokkenen_assignee_alias
        self.zaak_betrokkenen_coordinator_alias = (
            zaak_betrokkenen_coordinator_alias
        )
        self.zaak_betrokkenen_requestor_alias = (
            zaak_betrokkenen_requestor_alias
        )
        self.zaak_zaaktype_resultaten = zaak_zaaktype_resultaten
        self.zaak_bag = zaak_bag
        self.natuurlijk_persoon = natuurlijk_persoon

    def _apply_filter(
        self,
        search_filters_subquery: list,
        natural_person_subquery: list,
        global_operator: FilterOperator,
        filter_name: str,
        filter,
        user_info: minty.cqrs.UserInfo,
    ):
        if filter_name in NATURAL_PERSON_FILTERS:
            natural_person_subquery.append(
                FILTERS_MAPPING[filter_name](self, filter)
            )
        elif filter_name in FILTERS_MAPPING:
            subquery = FILTERS_MAPPING[filter_name](self, filter)
            if subquery is not None:
                search_filters_subquery.append(subquery)
        elif filter_name in FILTERS_MAPPING_WITH_USERINFO:
            subquery = FILTERS_MAPPING_WITH_USERINFO[filter_name](
                self, filter, user_info=user_info
            )
            if subquery is not None:
                search_filters_subquery.append(subquery)

        elif filter_name in FILTERS_MAPPING_WITH_GLOBAL_OPERATOR:
            subquery = FILTERS_MAPPING_WITH_GLOBAL_OPERATOR[filter_name](
                self, global_operator, filter
            )
            if subquery is not None:
                search_filters_subquery.append(subquery)
        else:  # pragma: no cover
            # This is not covered, because all filters _should_ be in one of
            # the lists.
            # This can only be hit during development when a new filter has
            # been created in CaseFilter, but it has not yet been implemented
            # fully)
            raise AssertionError(f"Unknown search filter: {filter=}")

    def apply_to_query(
        self,
        case_search_query: selectable.Select,
        filters: dict,
        user_info: minty.cqrs.UserInfo,
    ):
        """
        Apply a set of "advanced search" style filters to a case search query
        """
        # Filter out empty (None) filters:
        filters = {k: v for k, v in filters.items() if v is not None}

        search_filters_subquery = []
        natural_person_subquery = []

        global_operator = filters.get("operator", FilterOperator.and_operator)

        for filter_name in filters:
            if filter_name == "operator":
                # This is not a filter, it's the global operator (extracted above)
                continue

            self._apply_filter(
                search_filters_subquery,
                natural_person_subquery,
                global_operator,
                filter_name,
                filters[filter_name],
                user_info,
            )

        if natural_person_subquery:
            natural_person_query = apply_operator_to_filters(
                global_operator, natural_person_subquery
            )
            search_filters_subquery.append(
                self._natural_person_subquery(natural_person_query)
            )

        if search_filters_subquery:
            case_search_filters_query = apply_operator_to_filters(
                global_operator, search_filters_subquery
            )
            case_search_query = case_search_query.where(
                case_search_filters_query
            )

        return case_search_query

    def _natural_person_subquery(self, filter_subquery):
        natural_person_subquery = (
            sql.select(self.natuurlijk_persoon.c.id)
            .where(filter_subquery)
            .scalar_subquery()
        )
        return sql.and_(
            self.case_alias.c.aanvrager_gm_id.in_(natural_person_subquery),
            self.case_alias.c.aanvrager_type == RequestorType.person,
        )

    def _apply_filter_status(self, filter: list[str]):
        return self.case_alias.c.status.in_(filter)

    def _apply_filter_case_type_uuids(self, filter: list):
        return schema.Zaaktype.uuid.in_(filter)

    def _apply_filter_requestor_uuids(self, filter: list):
        return self.zaak_betrokkenen_requestor_alias.c.subject_id.in_(filter)

    def _apply_filter_assignee_uuids(self, filter: list):
        return self.zaak_betrokkenen_assignee_alias.c.subject_id.in_(filter)

    def _apply_filter_coordinator_uuids(self, filter: list):
        return self.zaak_betrokkenen_coordinator_alias.c.subject_id.in_(filter)

    def _apply_filter_registration_date(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.registratiedatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_completion_date(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.afhandeldatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_last_modified(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.last_modified,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_payment_status(self, filter: list[str]):
        return self.case_alias.c.payment_status.in_(filter)

    def _apply_filter_channel_of_contact(self, filter: list):
        return self.case_alias.c.contactkanaal.in_(filter)

    def _apply_filter_confidentiality(self, filter: list[str]):
        return self.case_alias.c.confidentiality.in_(filter)

    def _apply_filter_archival_state(self, filter: list[str]):
        archival_state_subquery = []
        if ValidCaseArchivalState.overdragen in filter:
            archival_state_subquery.append(
                self.case_alias.c.archival_state
                == ValidCaseArchivalState.overdragen
            )

        if ValidCaseArchivalState.vernietigen in filter:
            archival_state_subquery.append(
                sql.and_(
                    self.case_alias.c.archival_state
                    == ValidCaseArchivalState.vernietigen,
                    self.case_alias.c.status == ValidCaseStatus.resolved,
                    self.case_alias.c.vernietigingsdatum
                    < datetime.datetime.now(tz=datetime.timezone.utc),
                ).self_group()
            )

        if archival_state_subquery:
            query = sql.or_(*archival_state_subquery)
            return query

    def _apply_filter_retention_period_source_date(self, filter: list[str]):
        return self.zaak_zaaktype_resultaten.c.ingang.in_(filter)

    def _apply_filter_case_location(self, filter: dict):
        location_ids = list(filter.get("values", []))
        nummeraanduiding_bag_ids = openbareruimte_bag_ids = []
        for id in location_ids:
            if "nummeraanduiding-" in id:
                nummeraanduiding_bag_ids.append(
                    id.lower().replace("nummeraanduiding-", "")
                )
            elif "openbareruimte-" in id:
                openbareruimte_bag_ids.append(
                    id.lower().replace("openbareruimte-", "")
                )

        case_search_query = sql.or_(
            self.zaak_bag.c.bag_nummeraanduiding_id.in_(
                nummeraanduiding_bag_ids
            ),
            self.zaak_bag.c.bag_openbareruimte_id.in_(openbareruimte_bag_ids),
        )

        return case_search_query

    def _apply_filter_num_unread_messages(self, filter: list):
        return apply_comparison_filter(
            column=schema.CaseMeta.unread_communication_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_num_unaccepted_files(self, filter: list):
        return apply_comparison_filter(
            column=schema.CaseMeta.unaccepted_files_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_num_unaccepted_updates(self, filter: list):
        return apply_comparison_filter(
            column=schema.CaseMeta.unaccepted_attribute_update_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_result(self, filter: list):
        return self.case_alias.c.resultaat.in_(filter)

    def _apply_filter_keyword(
        self, filter: FilterMultipleValuesWithOperator[str]
    ):
        operator, keyword_values = get_operator_values_from_filter(filter)
        keyword_filters_subquery = []
        for filter_value in keyword_values:
            prepared_search_term = prepare_search_term(
                search_term=filter_value
            )
            keyword_filters_subquery.append(
                sql.and_(
                    self.case_alias.c.id == schema.CaseMeta.zaak_id,
                    schema.CaseMeta.text_vector.bool_op("@@")(
                        sql.func.to_tsquery(prepared_search_term)
                    ),
                )
            )
        if operator == FilterOperator.and_operator:
            query = sql.and_(*keyword_filters_subquery)
        elif operator == FilterOperator.or_operator:
            query = sql.or_(*keyword_filters_subquery)
        return query

    def _apply_filter_period_of_preservation_active(self, filter: bool):
        if not filter:
            return sql.or_(
                self.zaak_zaaktype_resultaten.c.trigger_archival.is_(filter),
                self.case_alias.c.resultaat_id.is_(None),
            )

        return self.zaak_zaaktype_resultaten.c.trigger_archival.is_(filter)

    def _apply_filter_subject(
        self, filter: FilterMultipleValuesWithOperator[str]
    ):
        operator, keyword_values = get_operator_values_from_filter(filter)
        filters_subquery = []
        for filter_value in keyword_values:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.onderwerp.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        if operator == FilterOperator.and_operator:
            query = sql.and_(*filters_subquery)
        elif operator == FilterOperator.or_operator:
            query = sql.or_(*filters_subquery)
        return query

    def _apply_filter_urgency(self, filter: list):
        urgency_query_conditions = {
            ValidCaseUrgency.normal: sql.func.coalesce(
                self.case_alias.c.afhandeldatum,
                sql.func.timezone("UTC", sql.func.now()),
            )
            <= self.case_alias.c.urgency_date_medium,
            ValidCaseUrgency.high: sql.and_(
                sql.func.coalesce(
                    self.case_alias.c.afhandeldatum,
                    sql.func.timezone("UTC", sql.func.now()),
                )
                >= self.case_alias.c.urgency_date_high,
                sql.func.coalesce(
                    self.case_alias.c.afhandeldatum,
                    sql.func.timezone("UTC", sql.func.now()),
                )
                < self.case_alias.c.streefafhandeldatum,
            ),
            ValidCaseUrgency.late: sql.func.coalesce(
                self.case_alias.c.afhandeldatum,
                sql.func.timezone("UTC", sql.func.now()),
            )
            >= self.case_alias.c.streefafhandeldatum,
            ValidCaseUrgency.medium: sql.and_(
                sql.func.coalesce(
                    self.case_alias.c.afhandeldatum,
                    sql.func.timezone("UTC", sql.func.now()),
                )
                > self.case_alias.c.urgency_date_medium,
                sql.func.coalesce(
                    self.case_alias.c.afhandeldatum,
                    sql.func.timezone("UTC", sql.func.now()),
                )
                < self.case_alias.c.urgency_date_high,
            ),
        }
        urgencies_list = {"normal", "high", "late", "medium"}

        if len(urgencies_list.difference(filter)) > 0:
            needed_filters = urgencies_list.intersection(filter)
            urgency_filter_subquery = []
            urgency_filter_subquery.append(
                sql.or_(
                    urgency_query_conditions[item] for item in needed_filters
                )
            )
            urgency_filter_subquery.append(
                self.case_alias.c.status != "stalled"
            )
            query = sql.and_(*urgency_filter_subquery)
        return query

    def _apply_filter_department_role(
        self, filter: FilterMultipleValuesWithOperator[DepartmentRoleTypedDict]
    ):
        filters_subquery = []
        filter_values = filter["values"]
        for filter_value in filter_values:
            department_uuid = filter_value["department_uuid"]
            role_uuid = filter_value.get("role_uuid")
            if department_uuid and role_uuid:
                department_id = (
                    sql.select(schema.Group.id)
                    .where(schema.Group.uuid == department_uuid)
                    .scalar_subquery()
                )
                role_id = (
                    sql.select(schema.Role.id)
                    .where(schema.Role.uuid == role_uuid)
                    .scalar_subquery()
                )
                filters_subquery.append(
                    sql.and_(
                        self.case_alias.c.route_ou == department_id,
                        self.case_alias.c.route_role == role_id,
                    )
                )
            else:
                department_id = (
                    sql.select(schema.Group.id)
                    .where(schema.Group.uuid == department_uuid)
                    .scalar_subquery()
                )
                filters_subquery.append(
                    sql.and_(
                        self.case_alias.c.route_ou == department_id,
                    )
                )

        query = sql.or_(*filters_subquery)
        return query

    def _apply_filter_stalled_until(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.stalled_until,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_stalled_since(self, filter: list):
        return apply_comparison_filter(
            column=schema.CaseMeta.stalled_since,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_parent_number(self, filter: list[int]):
        return self.case_alias.c.pid.in_(filter)

    def _apply_filter_target_date(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.streefafhandeldatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_destruction_date(self, filter: list):
        return apply_comparison_filter(
            column=self.case_alias.c.vernietigingsdatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )

    def _apply_filter_external_reference(
        self, filter: FilterMultipleValuesWithOperator[str]
    ):
        operator, keyword_values = get_operator_values_from_filter(filter)
        filters_subquery = []
        for filter_value in keyword_values:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.onderwerp_extern.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        if operator == FilterOperator.and_operator:
            query = sql.and_(*filters_subquery)
        elif operator == FilterOperator.or_operator:
            query = sql.or_(*filters_subquery)
        return query

    def _apply_filter_case_number(self, filter: list[int]):
        return self.case_alias.c.id.in_(filter)

    def _apply_filter_number_master(self, filter: list[int]):
        return self.case_alias.c.number_master.in_(filter)

    def _apply_filter_requestor_department(self, filter: list[UUID]):
        subject_uuid_subquery = (
            sql.select(schema.Subject.id)
            .select_from(
                sql.join(
                    schema.Group,
                    schema.Subject,
                    schema.Group.id == schema.Subject.group_ids[1],
                )
            )
            .where(schema.Group.uuid.in_(filter))
            .scalar_subquery()
        )

        return sql.and_(
            self.case_alias.c.aanvrager_gm_id.in_(subject_uuid_subquery),
            self.case_alias.c.aanvrager_type == RequestorType.employee,
        )

    def _apply_filter_case_phase(
        self, filter: FilterMultipleValuesWithOperator[str]
    ):
        operator, keyword_values = get_operator_values_from_filter(filter)
        filters_subquery = []
        for filter_value in keyword_values:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                schema.ZaaktypeStatus.fase.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        if operator == FilterOperator.and_operator:
            phase_name_query = sql.and_(*filters_subquery)
        elif operator == FilterOperator.or_operator:
            phase_name_query = sql.or_(*filters_subquery)

        zaaktype_status_subquery = (
            sql.select(schema.ZaaktypeStatus.status - 1)
            .where(
                sql.and_(
                    schema.ZaaktypeStatus.zaaktype_node_id
                    == self.case_alias.c.zaaktype_node_id,
                    phase_name_query,
                )
            )
            .scalar_subquery()
        )
        return self.case_alias.c.milestone.in_(zaaktype_status_subquery)

    def _apply_filter_case_type_identification(self, filter: list[str]):
        filters_subquery = []
        for filter_value in filter:
            escaped_value = escape_term_for_like(filter_value)
            filters_subquery.append(
                schema.ZaaktypeNode.code.ilike(
                    f"%{escaped_value}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)

        return query

    def _apply_filter_case_price(
        self, filter: FilterMultipleValuesWithOperator
    ):
        operator, filter_values = get_operator_values_from_filter(
            filter, FilterOperator.and_operator
        )
        return apply_comparison_filter(
            column=self.case_alias.c.payment_amount,
            comparison_filters=[
                ComparisonFilterCondition[float].from_str(filter_value)
                for filter_value in filter_values
            ],
            filter_operator=operator,
        )

    def _apply_filter_case_custom_number(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.prefix.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )

        query = sql.or_(*filters_subquery)
        return query

    def _apply_filter_case_type_confidentiality(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            filters_subquery.append(
                sql.cast(schema.ZaaktypeNode.properties, sqltypes.JSON).op(
                    "->>"
                )("vertrouwelijkheidsaanduiding")
                == str(filter_value)
            )
        query = sql.or_(*filters_subquery)

        return query

    def _apply_filter_case_type_version(
        self, filter: list
    ) -> expression.BinaryExpression[bool]:
        return schema.ZaaktypeNode.uuid.in_(filter)

    def _apply_filter_custom_object_uuid(
        self, filter: list
    ) -> expression.BinaryExpression[bool]:
        return sql.exists(
            sql.select(1)
            .select_from(schema.CustomObjectRelationship)
            .where(
                sql.and_(
                    schema.CustomObject.uuid.in_(filter),
                    schema.CustomObjectRelationship.custom_object_id
                    == schema.CustomObject.id,
                    schema.CustomObjectRelationship.relationship_type
                    == "case",
                    schema.CustomObjectRelationship.related_case_id
                    == self.case_alias.c.id,
                )
            )
        )

    def _apply_filter_requestor_coc_number(self, filter: list[str]):
        query = self.case_alias.c.requestor_v1_json["instance"]["subject"][
            "instance"
        ]["coc_number"].astext.in_(filter)
        return query

    def _apply_filter_requestor_zipcode(self, filter: list[str]):
        filter_values = [value.replace(" ", "") for value in filter]
        query = self.case_alias.c.requestor_v1_json["instance"]["subject"][
            "instance"
        ]["address_residence"]["instance"]["zipcode"].astext.in_(filter_values)
        return query

    def _apply_filter_requestor_noble_title(
        self, filter: list[str]
    ) -> expression.BinaryExpression[bool]:
        filter = [value.lower() for value in filter]
        query = sql.func.lower(
            self.case_alias.c.requestor_v1_json["instance"]["subject"][
                "instance"
            ]["noble_title"].astext
        ).in_(filter)
        return query

    def _apply_filter_requestor_coc_location_number(self, filter: list[str]):
        filter_values = []
        for filter_value in filter:
            filter_values.append(filter_value.zfill(12))

        query = self.case_alias.c.requestor_v1_json["instance"]["subject"][
            "instance"
        ]["coc_location_number"].astext.in_(filter_values)
        return query

    def _apply_filter_requestor_trade_name(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_value = escape_term_for_like(filter_value)
            filters_subquery.append(
                schema.Bedrijf.handelsnaam.ilike(
                    f"%{escaped_value}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)
        organization_subquery = (
            sql.select(schema.Bedrijf.id).where(query).scalar_subquery()
        )
        return sql.and_(
            self.case_alias.c.aanvrager_gm_id.in_(organization_subquery),
            self.case_alias.c.aanvrager_type == "bedrijf",
        )

    def _apply_filter_requestor_date_of_birth(
        self, filter: list
    ) -> sql.ColumnElement[bool]:
        filter_subquery = apply_comparison_filter(
            column=self.natuurlijk_persoon.c.geboortedatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
        )
        return filter_subquery

    def _apply_filter_requestor_date_of_death(
        self, filter: list
    ) -> sql.ColumnElement[bool]:
        filter_subquery = apply_comparison_filter(
            column=self.natuurlijk_persoon.c.datum_overlijden,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )
        return filter_subquery

    def _apply_filter_requestor_date_of_marriage(
        self, filter: list
    ) -> sql.ColumnElement[bool]:
        filter_subquery = apply_comparison_filter(
            column=self.natuurlijk_persoon.c.datum_huwelijk,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
            filter_operator=FilterOperator.or_operator,
        )
        return filter_subquery

    def _apply_filter_requestor_gender(self, filter: list[str]):
        if "F" in filter:
            filter.append("V")
        elif "X" in filter:
            filter.append("O")
        query = self.case_alias.c.requestor_v1_json["instance"]["subject"][
            "instance"
        ]["gender"].astext.in_(filter)

        return query

    def _apply_filter_requestor_investigation(
        self, filter: bool
    ) -> expression.BinaryExpression[bool]:
        filter_value = filter if filter else None
        return self.natuurlijk_persoon.c.onderzoek_persoon.is_(filter_value)

    def _apply_filter_requestor_is_secret(
        self, filter: bool
    ) -> expression.BinaryExpression[bool]:
        query = sql.cast(
            self.case_alias.c.requestor_v1_json["instance"]["subject"][
                "instance"
            ]["is_secret"],
            sqltypes.Boolean,
        ).is_(filter)
        return query

    def _apply_filter_requestor_correspondence_zipcode(
        self, filter: list[str]
    ):
        filter_values = [value.replace(" ", "") for value in filter]
        query = self.case_alias.c.requestor_v1_json["instance"]["subject"][
            "instance"
        ]["address_correspondence"]["instance"]["zipcode"].astext.in_(
            filter_values
        )
        return query

    def _apply_filter_requestor_correspondence_street(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.requestor_v1_json["instance"]["subject"][
                    "instance"
                ]["address_correspondence"]["instance"]["street"].astext.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)

        return query

    def _apply_filter_requestor_correspondence_city(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.requestor_v1_json["instance"]["subject"][
                    "instance"
                ]["address_correspondence"]["instance"]["city"].astext.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)

        return query

    def _apply_filter_requestor_city(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.requestor_v1_json["instance"]["subject"][
                    "instance"
                ]["address_residence"]["instance"]["city"].astext.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)

        return query

    def _apply_filter_requestor_street(
        self, filter: list[str]
    ) -> sql.ColumnElement[bool]:
        filters_subquery = []
        for filter_value in filter:
            escaped_keyword = escape_term_for_like(filter_value)
            filters_subquery.append(
                self.case_alias.c.requestor_v1_json["instance"]["subject"][
                    "instance"
                ]["address_residence"]["instance"]["street"].astext.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        query = sql.or_(*filters_subquery)

        return query

    # Attribute filters
    # {
    def _apply_filter_attributes_value(
        self, global_operator, filter_attributes_value
    ) -> expression.BinaryExpression[bool] | None:
        file_filters = []
        filters = filter_attributes_value.get("values", [])
        for filter in filters:
            value_type = filter["type"]
            if value_type == "file":
                file_filters.append(filter)

        case_ids = self._search_custom_fields(
            filter_attributes_value, global_operator
        )

        if file_filters:
            result_case_ids = self._apply_filter_attributes_file(
                global_operator, file_filters, case_ids
            )
            return self.case_alias.c.id.in_(sql.select(result_case_ids.c.id))
        else:
            if case_ids is not None:
                result_case_ids = case_ids.subquery()
                return self.case_alias.c.id.in_(
                    sql.select(result_case_ids.c.id)
                )

    def _apply_filter_attributes_file(
        self, global_operator, file_filters, case_ids
    ):
        result_case_ids = None
        file_case_ids = apply_filter_file(file_filters, global_operator)
        if case_ids is not None:
            if file_case_ids is not None:
                if global_operator == FilterOperator.and_operator:
                    result_case_ids = intersect(
                        case_ids, file_case_ids
                    ).subquery()
                else:
                    result_case_ids = union(case_ids, file_case_ids).subquery()
            else:
                result_case_ids = case_ids.subquery()
        else:
            if file_case_ids is not None:
                result_case_ids = file_case_ids.subquery()

        return result_case_ids

    def _apply_filter_type_of_archiving(self, filter: list[str]):
        return self.zaak_zaaktype_resultaten.c.archiefnominatie.in_(filter)

    def _apply_filter_intake(
        self, filter: bool, user_info: minty.cqrs.UserInfo
    ) -> sql.ColumnElement[bool] | None:
        if filter is False:
            return

        case_is_new = self.case_alias.c.status == "new"
        case_registered_in_past = (
            self.case_alias.c.registratiedatum
            <= sql.func.timezone("UTC", sql.func.now())
        )

        # Users with this role get to see _all_ unassigned cases in their intake
        # Others get only cases assigned to one of their group/role combos
        has_role = user_info.permissions.get(
            "zaak_route_default"
        ) or user_info.permissions.get("admin")

        intake_assignment_filter = [
            self.case_alias.c.behandelaar.is_(None),
        ]

        if not has_role:
            intake_assignment_filter.extend(
                [
                    self.case_alias.c.route_ou.in_(
                        sql.select(
                            sql.func.unnest(schema.Subject.group_ids)
                        ).where(schema.Subject.uuid == user_info.user_uuid)
                    ),
                    self.case_alias.c.route_role.in_(
                        sql.select(
                            sql.func.unnest(schema.Subject.role_ids)
                        ).where(schema.Subject.uuid == user_info.user_uuid)
                    ),
                ]
            )

        case_in_user_intake = sql.or_(
            self.zaak_betrokkenen_assignee_alias.c.subject_id
            == user_info.user_uuid,
            sql.and_(*intake_assignment_filter),
        )

        query = sql.and_(
            case_is_new,
            case_registered_in_past,
            case_in_user_intake,
        )

        return query

    def _search_custom_fields(self, filters, operator):
        filters = filters.get("values", [])
        search_filters_subquery = []
        file_filters_nr = 0
        relationship_filters = False
        for filter in filters:
            value_type = filter["type"]
            try:
                if value_type == "file":
                    file_filters_nr += 1
                elif value_type in (
                    "relationship_subject",
                    "relationship_object",
                ):
                    relationship_filters = True
                search_filters_subquery.append(
                    CUSTOM_FIELDS_FILTER_MAPPING[value_type](filter)
                )
            except KeyError:
                logger.info(
                    f"Filter for attribute type {value_type} not implemented"
                )

        case_ids_subquery = None
        nr_of_filters = len(filters) - file_filters_nr

        if relationship_filters:
            select_from = sql.join(
                schema.ZaakKenmerk,
                schema.BibliotheekKenmerk,
                schema.ZaakKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
            )
        else:
            select_from = schema.ZaakKenmerk

        if search_filters_subquery:
            query = sql.and_(sql.or_(*search_filters_subquery))
            if operator == FilterOperator.and_operator and nr_of_filters > 1:
                case_ids_subquery = (
                    sql.select((schema.ZaakKenmerk.zaak_id.label("id")))
                    .select_from(select_from)
                    .where(query)
                    .group_by(schema.ZaakKenmerk.zaak_id)
                    .having(
                        sql.func.count(
                            sql.func.distinct(schema.ZaakKenmerk.value)
                        )
                        >= nr_of_filters
                    )
                )
            else:
                case_ids_subquery = (
                    sql.select((schema.ZaakKenmerk.zaak_id.label("id")))
                    .select_from(select_from)
                    .where(query)
                )
        return case_ids_subquery


# Custom field filters
def _apply_filter_address_v2(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], postgresql.JSONB)
            .op("->")("bag")
            .op("->>")("id")
            == value
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_bag_openbareruimte(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Text) == value
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_bag_openbareruimtes(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Text).contains(
                value
            )
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_checkbox(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values", [])
    magic_string = filter.get("magic_string")
    operator = filter.get("operator", FilterOperator.or_operator)

    if operator == FilterOperator.or_operator:
        values_query = []
        for value in values:
            values_query.append(schema.ZaakKenmerk.value.contains([value]))
        values_subquery = sql.or_(*values_query)

    else:
        values_subquery = schema.ZaakKenmerk.value.contains(values)

    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_option(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Text) == value
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_relationship_subject(filter) -> sql.ColumnElement[bool]:
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], postgresql.JSONB).op("->>")(
                "value"
            )
            == value
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.BibliotheekKenmerk.relationship_type == "subject",
        schema.ZaakKenmerk.value_type == "relationship",
        values_subquery,
    ).self_group()
    return query


def _apply_filter_relationship_custom_object(
    filter,
) -> sql.ColumnElement[bool]:
    magic_string = filter.get("magic_string")

    query = sql.and_(
        schema.ZaakKenmerk.value != "{}",
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.BibliotheekKenmerk.relationship_type == "custom_object",
        schema.ZaakKenmerk.value_type == "relationship",
    ).self_group()

    return query


def _apply_filter_date(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values", [])
    magic_string = filter.get("magic_string")
    filter_operator = filter.get("operator", FilterOperator.and_operator)

    column_patterns = [
        (
            sql.func.immutable_to_date(
                schema.ZaakKenmerk.value[1], "DD-MM-YYYY"
            ).cast(sqltypes.Date),
            r"^\d{2}-\d{2}-\d{4}$",
        ),
        (
            sql.func.immutable_text_to_date(schema.ZaakKenmerk.value[1]),
            r"^\d{4}-\d{2}-\d{2}$",
        ),
        (
            sql.func.immutable_to_date(
                schema.ZaakKenmerk.value[1], "DD-M-YYYY"
            ).cast(sqltypes.Date),
            r"^\d{2}-\d{1}-\d{4}$",
        ),
    ]

    subqueries = []
    for filter_value in values:
        if isinstance(filter_value, list):
            subqueries.append(
                sql.and_(
                    *[
                        _build_date_subquery(
                            *value.split(" ", maxsplit=1),
                            column_patterns,
                        )
                        for value in filter_value
                    ]
                ).self_group()
            )
        else:
            subqueries.append(
                _build_date_subquery(
                    *filter_value.split(" ", maxsplit=1),
                    column_patterns,
                )
            )

    subquery = (
        sql.or_(*subqueries)
        if filter_operator == FilterOperator.or_operator
        else sql.and_(*subqueries)
    )

    return sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        subquery,
    ).self_group()


def _apply_filter_email(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.func.lower(
                sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Text)
            )
            == value.lower()
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()

    return query


def _apply_filter_valuta(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")
    filter_operator = filter.get("operator", FilterOperator.and_operator)

    column = sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Float)
    subqueries = []
    for value in values:
        (value_operator, operand) = value.split(" ", maxsplit=1)
        subqueries.append(
            getattr(operator, value_operator)(column, float(operand))
        )
    if filter_operator == FilterOperator.or_operator:
        subquery = sql.or_(*subqueries)
    else:
        subquery = sql.and_(*subqueries)

    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        subquery,
    ).self_group()

    return query


def _apply_filter_numeric(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(str(value) == sql.any_(schema.ZaakKenmerk.value))

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_bankaccount(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")

    values_query = []
    for value in values:
        values_query.append(
            sql.cast(schema.ZaakKenmerk.value[1], sqltypes.Text) == value
        )

    values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_text(filter) -> sql.ColumnElement[bool]:
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")
    values_operator = filter.get("operator", FilterOperator.or_operator)

    values_query = []

    for filter_value in values:
        escaped_keyword = escape_term_for_like(filter_value)
        values_query.append(
            sql.func.immutable_array_to_string(schema.ZaakKenmerk.value).ilike(
                f"%{escaped_keyword}%", escape="~"
            )
        )

    if values_operator == FilterOperator.and_operator:
        values_subquery = sql.and_(*values_query)
    else:
        values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()
    return query


def _apply_filter_richtext(filter):
    value_type = filter.get("type")
    values = filter.get("values")
    magic_string = filter.get("magic_string")
    values_operator = filter.get("operator", FilterOperator.or_operator)

    values_query = []
    for filter_value in values:
        escaped_keyword = escape_term_for_like(filter_value)
        values_query.append(
            sql.func.attribute_richtext_strip_html_tags(
                schema.ZaakKenmerk.value
            ).ilike(f"%{escaped_keyword}%", escape="~")
        )

    if values_operator == FilterOperator.and_operator:
        values_subquery = sql.and_(*values_query)
    else:
        values_subquery = sql.or_(*values_query)
    query = sql.and_(
        schema.ZaakKenmerk.magic_string == magic_string,
        schema.ZaakKenmerk.value_type == value_type,
        values_subquery,
    ).self_group()

    return query


def apply_filter_file(file_filters, operator):
    magic_strings = []
    for file_filter in file_filters:
        magic_strings.append(file_filter.get("magic_string"))

    if operator == FilterOperator.and_operator:
        case_ids_subquery = (
            sql.select((schema.FileCaseDocument.case_id.label("id")))
            .select_from(schema.FileCaseDocument)
            .where(schema.FileCaseDocument.magic_string.in_(magic_strings))
            .group_by(schema.FileCaseDocument.case_id)
            .having(
                sql.func.count(
                    sql.func.distinct(schema.FileCaseDocument.magic_string)
                )
                >= len(magic_strings)
            )
        )
    else:
        case_ids_subquery = (
            sql.select((schema.FileCaseDocument.case_id.label("id")))
            .select_from(schema.FileCaseDocument)
            .where(schema.FileCaseDocument.magic_string.in_(magic_strings))
            .group_by(schema.FileCaseDocument.case_id)
        )
    return case_ids_subquery


def _build_date_subquery(value_operator, operand, column_patterns):
    date_value = datetime.datetime.strptime(operand, "%Y-%m-%d").date()
    return sql.or_(
        *[
            sql.and_(
                schema.ZaakKenmerk.value[1].op("~")(pattern),
                getattr(operator, value_operator)(col, date_value),
            )
            for col, pattern in column_patterns
        ]
    )


CUSTOM_FIELDS_FILTER_MAPPING: dict[
    str, Callable[[list], expression.ColumnElement[bool]]
] = {
    "address_v2": _apply_filter_address_v2,
    "bag_openbareruimte": _apply_filter_bag_openbareruimte,
    "bag_openbareruimtes": _apply_filter_bag_openbareruimtes,
    "bag_straat_adres": _apply_filter_bag_openbareruimte,
    "bag_straat_adressen": _apply_filter_bag_openbareruimtes,
    "bag_adres": _apply_filter_bag_openbareruimte,
    "bag_adressen": _apply_filter_bag_openbareruimtes,
    "checkbox": _apply_filter_checkbox,
    "option": _apply_filter_option,
    "select": _apply_filter_option,
    "relationship_subject": _apply_filter_relationship_subject,
    "relationship_custom_object": _apply_filter_relationship_custom_object,
    "email": _apply_filter_email,
    "date": _apply_filter_date,
    "numeric": _apply_filter_numeric,
    "bankaccount": _apply_filter_bankaccount,
    "valuta": _apply_filter_valuta,
    "valutaex": _apply_filter_valuta,
    "valutaex21": _apply_filter_valuta,
    "valutaex6": _apply_filter_valuta,
    "valutain": _apply_filter_valuta,
    "valutain21": _apply_filter_valuta,
    "valutain6": _apply_filter_valuta,
    "text": _apply_filter_text,
    "textarea": _apply_filter_text,
    "richtext": _apply_filter_richtext,
}

FILTERS_MAPPING: Final = {
    "filter_status": CaseFilterApplicator._apply_filter_status,
    "case_type_uuids": CaseFilterApplicator._apply_filter_case_type_uuids,
    "requestor_uuids": CaseFilterApplicator._apply_filter_requestor_uuids,
    "assignee_uuids": CaseFilterApplicator._apply_filter_assignee_uuids,
    "coordinator_uuids": CaseFilterApplicator._apply_filter_coordinator_uuids,
    "filter_registration_date": CaseFilterApplicator._apply_filter_registration_date,
    "filter_completion_date": CaseFilterApplicator._apply_filter_completion_date,
    "filter_last_modified": CaseFilterApplicator._apply_filter_last_modified,
    "filter_payment_status": CaseFilterApplicator._apply_filter_payment_status,
    "filter_channel_of_contact": CaseFilterApplicator._apply_filter_channel_of_contact,
    "filter_confidentiality": CaseFilterApplicator._apply_filter_confidentiality,
    "filter_archival_state": CaseFilterApplicator._apply_filter_archival_state,
    "filter_retention_period_source_date": CaseFilterApplicator._apply_filter_retention_period_source_date,
    "filter_result": CaseFilterApplicator._apply_filter_result,
    "filter_case_location": CaseFilterApplicator._apply_filter_case_location,
    "filter_num_unread_messages": CaseFilterApplicator._apply_filter_num_unread_messages,
    "filter_num_unaccepted_files": CaseFilterApplicator._apply_filter_num_unaccepted_files,
    "filter_num_unaccepted_updates": CaseFilterApplicator._apply_filter_num_unaccepted_updates,
    "filter_keyword": CaseFilterApplicator._apply_filter_keyword,
    "filter_period_of_preservation_active": CaseFilterApplicator._apply_filter_period_of_preservation_active,
    "filter_subject": CaseFilterApplicator._apply_filter_subject,
    "filter_urgency": CaseFilterApplicator._apply_filter_urgency,
    "filter_department_role": CaseFilterApplicator._apply_filter_department_role,
    "filter_stalled_until": CaseFilterApplicator._apply_filter_stalled_until,
    "filter_stalled_since": CaseFilterApplicator._apply_filter_stalled_since,
    "filter_parent_number": CaseFilterApplicator._apply_filter_parent_number,
    "filter_target_date": CaseFilterApplicator._apply_filter_target_date,
    "filter_destruction_date": CaseFilterApplicator._apply_filter_destruction_date,
    "filter_external_reference": CaseFilterApplicator._apply_filter_external_reference,
    "filter_case_number": CaseFilterApplicator._apply_filter_case_number,
    "filter_number_master": CaseFilterApplicator._apply_filter_number_master,
    "filter_case_phase": CaseFilterApplicator._apply_filter_case_phase,
    "filter_case_price": CaseFilterApplicator._apply_filter_case_price,
    "filter_case_custom_number": CaseFilterApplicator._apply_filter_case_custom_number,
    "filter_case_type_identification": CaseFilterApplicator._apply_filter_case_type_identification,
    "filter_case_type_confidentiality": CaseFilterApplicator._apply_filter_case_type_confidentiality,
    "filter_case_type_version": CaseFilterApplicator._apply_filter_case_type_version,
    "filter_requestor_coc_number": CaseFilterApplicator._apply_filter_requestor_coc_number,
    "filter_requestor_zipcode": CaseFilterApplicator._apply_filter_requestor_zipcode,
    "filter_requestor_noble_title": CaseFilterApplicator._apply_filter_requestor_noble_title,
    "filter_requestor_coc_location_number": CaseFilterApplicator._apply_filter_requestor_coc_location_number,
    "filter_requestor_trade_name": CaseFilterApplicator._apply_filter_requestor_trade_name,
    "filter_requestor_investigation": CaseFilterApplicator._apply_filter_requestor_investigation,
    "filter_requestor_is_secret": CaseFilterApplicator._apply_filter_requestor_is_secret,
    "filter_requestor_date_of_birth": CaseFilterApplicator._apply_filter_requestor_date_of_birth,
    "filter_requestor_date_of_death": CaseFilterApplicator._apply_filter_requestor_date_of_death,
    "filter_requestor_date_of_marriage": CaseFilterApplicator._apply_filter_requestor_date_of_marriage,
    "filter_requestor_gender": CaseFilterApplicator._apply_filter_requestor_gender,
    "filter_requestor_department": CaseFilterApplicator._apply_filter_requestor_department,
    "filter_requestor_correspondence_zipcode": CaseFilterApplicator._apply_filter_requestor_correspondence_zipcode,
    "filter_requestor_correspondence_street": CaseFilterApplicator._apply_filter_requestor_correspondence_street,
    "filter_requestor_correspondence_city": CaseFilterApplicator._apply_filter_requestor_correspondence_city,
    "filter_requestor_city": CaseFilterApplicator._apply_filter_requestor_city,
    "filter_requestor_street": CaseFilterApplicator._apply_filter_requestor_street,
    "filter_type_of_archiving": CaseFilterApplicator._apply_filter_type_of_archiving,
    "filter_custom_object_uuid": CaseFilterApplicator._apply_filter_custom_object_uuid,
}

FILTERS_MAPPING_WITH_USERINFO: Final = {
    "filter_intake": CaseFilterApplicator._apply_filter_intake,
}

FILTERS_MAPPING_WITH_GLOBAL_OPERATOR: Final = {
    "filter_custom_fields": CaseFilterApplicator._apply_filter_attributes_value,
}

NATURAL_PERSON_FILTERS: Final = [
    "filter_requestor_date_of_birth",
    "filter_requestor_date_of_death",
    "filter_requestor_date_of_marriage",
    "filter_requestor_investigation",
]
