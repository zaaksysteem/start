# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import dateutil
import dateutil.parser
import pytz
from datetime import timezone


def format_date(date):
    if date is not None:
        date = dateutil.parser.isoparse(str(date))
        date = date.replace(tzinfo=timezone.utc).astimezone(
            tz=pytz.timezone("Europe/Amsterdam")
        )
    return date
