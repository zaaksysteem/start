# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .commands import COMMANDS
from .queries import Queries
from .repositories import (
    email_integration,
    integration,
    transaction,
    transaction_data,
    transaction_record,
)

REQUIRED_REPOSITORIES = {
    "integration": integration.IntegrationRepository,
    "email_integration": email_integration.EmailIntegrationRepository,
    "transaction": transaction.TransactionRepository,
    "transaction_data": transaction_data.TransactionDataRepository,
    "transaction_record": transaction_record.TransactionRecordRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return minty.cqrs.DomainCommandContainer(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
        command_lookup_table=COMMANDS,
    )
