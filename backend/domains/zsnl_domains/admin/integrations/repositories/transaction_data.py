# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
from .... import ZaaksysteemRepositoryBase
from ..entities.transaction import TransactionData
from sqlalchemy import exc as sql_exc
from sqlalchemy import sql
from typing import Final
from uuid import UUID
from zsnl_domains.database import schema

TRANSACTION_DATA_QUERY: Final = sql.select(
    schema.Transaction.uuid,
    schema.Transaction.input_data,
).select_from(
    sql.join(
        schema.Transaction,
        schema.Interface,
        sql.and_(
            schema.Transaction.interface_id == schema.Interface.id,
            schema.Interface.date_deleted.is_(None),
        ),
    )
)


class TransactionDataRepository(ZaaksysteemRepositoryBase):
    _for_entity = "Transaction"
    _events_to_calls = {}

    def get(self, uuid: UUID) -> TransactionData:
        try:
            transaction_row = self.session.execute(
                TRANSACTION_DATA_QUERY.where(schema.Transaction.uuid == uuid)
            ).one()
        except sql_exc.NoResultFound as e:
            raise minty.exceptions.NotFound(
                "No transaction found with that id", "transaction/not_found"
            ) from e

        return self._inflate_from_row(transaction_row)

    def _inflate_from_row(self, row) -> TransactionData:
        return TransactionData(
            entity_id=row.uuid,
            uuid=row.uuid,
            input_data=row.input_data,
        )
