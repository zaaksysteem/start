{
  "openapi": "3.0.0",
  "info": {
    "title": "Zaaksysteem Catalog API",
    "version": "v1"
  },
  "servers": [
    {
      "description": "Local development environment",
      "url": "https://dev.zaaksysteem.nl"
    }
  ],
  "paths": {
    "/api/v2/admin/catalog/get_folder_contents": {
      "get": {
        "x-view": "views",
        "tags": [
          "catalog"
        ],
        "operationId": "get_folder_contents",
        "summary": "Retrieve the contents of a catalog folder",
        "parameters": [
          {
            "name": "folder_uuid",
            "in": "query",
            "description": "Folder to retrieve contents of. If this is not specified, the root folder will be retrieved.",
            "required": false,
            "schema": {
              "type": "string",
              "format": "uuid"
            }
          },
          {
            "name": "page",
            "in": "query",
            "description": "Number of page to be retrieved",
            "required": false,
            "schema": {
              "type": "integer"
            }
          },
          {
            "name": "page_size",
            "in": "query",
            "description": "Number of records to be retrieved",
            "required": false,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Folder contents",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CatalogFolderResponse"
                }
              }
            }
          },
          "400": {}
        }
      }
    },
    "/api/v2/admin/catalog/get_entry_detail": {
      "get": {
        "x-views": "views",
        "tags": [
          "catalog"
        ],
        "operationId": "get_entry_detail",
        "summary": "Retrieve detailed information about a catalog entry",
        "parameters": [
          {
            "name": "item_uuid",
            "in": "query",
            "description": "Folder to retrieve contents of. If this is not specified, the root folder will be retrieved.",
            "required": true,
            "schema": {
              "type": "string",
              "format": "uuid"
            }
          },
          {
            "name": "type",
            "in": "query",
            "description": "Type of item to retrieve contents of.",
            "required": true,
            "schema": {
              "$ref": "#/components/schemas/CatalogEntry"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Item details",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CatalogEntryDetailResponse"
                }
              }
            }
          },
          "400": {}
        }
      }
    }
  },
  "components": {
    "schemas": {
      "CatalogFolderResponse": {
        "type": "object",
        "required": [
          "data",
          "links"
        ],
        "properties": {
          "data": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/CatalogEntry"
            }
          },
          "links": {
            "$ref": "#/components/schemas/JsonAPILinks"
          }
        }
      },
      "CatalogEntryDetailResponse": {
        "type": "object",
        "required": [
          "data",
          "links"
        ],
        "properties": {
          "data": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/FolderDetail"
              },
              {
                "$ref": "#/components/schemas/CaseTypeDetail"
              },
              {
                "$ref": "#/components/schemas/ObjectTypeDetail"
              },
              {
                "$ref": "#/components/schemas/AttributeDetail"
              },
              {
                "$ref": "#/components/schemas/EmailTemplateDetail"
              },
              {
                "$ref": "#/components/schemas/DocumentTemplateDetail"
              }
            ]
          },
          "links": {
            "$ref": "#/components/schemas/JsonAPILinks"
          }
        }
      },
      "FolderDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "folder"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              }
            }
          }
        }
      },
      "CaseTypeDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "casetype"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              },
              "active": {
                "type": "boolean"
              },
              "api_v1_endpoint": {
                "type": "string",
                "format": "uri"
              },
              "stuf_identification": {
                "type": "string"
              },
              "current_version": {
                "type": "integer"
              },
              "last_modified": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "relationships": {
            "type": "object",
            "properties": {
              "casetypes": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryCaseTypeRelation"
                }
              },
              "object_types": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryObjectTypeRelation"
                }
              }
            }
          }
        }
      },
      "ObjectTypeDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "object_type"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              },
              "last_modified": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "relationships": {
            "type": "object",
            "properties": {
              "casetypes": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryCaseTypeRelation"
                }
              }
            }
          }
        }
      },
      "AttributeDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "attribute"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              },
              "magic_string": {
                "type": "string"
              },
              "entry_type": {
                "type": "string"
              },
              "is_multiple": {
                "type": "boolean"
              },
              "last_modified": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "relationships": {
            "type": "object",
            "properties": {
              "casetypes": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryCaseTypeRelation"
                }
              },
              "object_types": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryObjectTypeRelation"
                }
              }
            }
          }
        }
      },
      "EmailTemplateDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "email_template"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              },
              "last_modified": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "relationships": {
            "type": "object",
            "properties": {
              "casetypes": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryCaseTypeRelation"
                }
              }
            }
          }
        }
      },
      "DocumentTemplateDetail": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "relationships"
        ],
        "properties": {
          "type": {
            "enum": [
              "email_template"
            ],
            "type": "string"
          },
          "attributes": {
            "type": "object",
            "properties": {
              "uuid": {
                "type": "string",
                "format": "uuid"
              },
              "name": {
                "type": "string"
              },
              "download_uri": {
                "type": "string",
                "format": "uri"
              },
              "last_modified": {
                "type": "string",
                "format": "date-time"
              }
            }
          },
          "relationships": {
            "type": "object",
            "properties": {
              "casetypes": {
                "type": "array",
                "items": {
                  "$ref": "#/components/schemas/CatalogEntryCaseTypeRelation"
                }
              }
            }
          }
        }
      },
      "CatalogEntryCaseTypeRelation": {
        "type": "object",
        "properties": {
          "uuid": {
            "type": "string",
            "format": "uuid"
          },
          "name": {
            "type": "string"
          },
          "version": {
            "type": "integer"
          },
          "active": {
            "type": "boolean"
          }
        }
      },
      "CatalogEntryObjectTypeRelation": {
        "type": "object",
        "properties": {
          "uuid": {
            "type": "string",
            "format": "uuid"
          },
          "name": {
            "type": "string"
          }
        }
      },
      "CatalogEntry": {
        "type": "object",
        "required": [
          "type",
          "attributes",
          "links"
        ],
        "properties": {
          "type": {
            "enum": [
              "folder",
              "casetype",
              "object_type",
              "attribute",
              "email_template",
              "document_template"
            ],
            "type": "string"
          },
          "attributes": {
            "$ref": "#/components/schemas/CatalogEntryAttributes"
          },
          "links": {
            "$ref": "#/components/schemas/CatalogEntryLinks"
          }
        }
      },
      "CatalogEntryAttributes": {
        "type": "object",
        "required": [
          "id",
          "name",
          "active"
        ],
        "properties": {
          "id": {
            "type": "string",
            "format": "uuid"
          },
          "name": {
            "type": "string"
          },
          "active": {
            "type": "boolean"
          }
        }
      },
      "CatalogEntryLinks": {
        "type": "object",
        "required": [
          "self"
        ],
        "properties": {
          "self": {
            "type": "string",
            "format": "uri"
          },
          "meta": {
            "type": "string",
            "format": "uri"
          }
        }
      },
      "JsonAPILinks": {
        "type": "object",
        "required": [
          "self"
        ],
        "properties": {
          "self": {
            "type": "string",
            "format": "uri"
          }
        }
      }
    }
  }
}