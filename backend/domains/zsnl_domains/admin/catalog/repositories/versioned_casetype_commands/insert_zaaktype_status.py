# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeStatusCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            department_id = None
            if self.get_context().get_group(
                getattr_from_dict(phase, "assignment.department_uuid")
            ):
                department_id = (
                    self.get_context()
                    .get_group(
                        getattr_from_dict(phase, "assignment.department_uuid")
                    )
                    .id
                )
            role_id = None
            if self.get_context().get_role(
                getattr_from_dict(phase, "assignment.role_uuid")
            ):
                role_id = (
                    self.get_context()
                    .get_role(getattr_from_dict(phase, "assignment.role_uuid"))
                    .id
                )

            id = self.session.execute(
                sql.insert(schema.ZaaktypeStatus)
                .values(
                    zaaktype_node_id=self.get_context().get_zaaktype_node_id(),
                    status=count,
                    naam=getattr_from_dict(phase, "milestone_name"),
                    ou_id=department_id,
                    role_id=role_id,
                    fase=getattr_from_dict(phase, "phase_name"),
                    role_set=util.bool_to_int(
                        getattr_from_dict(phase, "assignment.enabled")
                    ),
                    termijn=getattr_from_dict(phase, "term_in_days"),
                    # id             filled by db
                    # checklist      not used
                    # created        filled by db
                    # last_modified  filled by db
                    # status_type    not used
                )
                .returning(schema.ZaaktypeStatus.id)
            ).fetchone()
            self.get_context().set_phase_id(status=count, db_id=id.id)
