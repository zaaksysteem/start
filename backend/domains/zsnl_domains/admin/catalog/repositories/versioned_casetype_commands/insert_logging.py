# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .....shared.util import getattr_from_dict
from . import util
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql  # type: ignore
from typing import TypedDict
from uuid import UUID
from zsnl_domains.database import schema

LoggingEventData = TypedDict(
    "LoggingEventData",
    {
        "case_type": int,
        "commit_message": str,
        "components": list[str],
        "title": str,
    },
)

logger = logging.getLogger(__name__)


class InsertLoggingCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        created_by: str | None = None
        created_by_name_cache: None = None

        current_user_uuid: UUID = self.get_context().get_current_user_uuid()

        # Always an employee
        created_by = util.create_legacy_subject_id(
            session=self.session,
            subject_uuid=current_user_uuid,
            subject_type="employee",
        )

        id = self.session.execute(
            sql.insert(schema.Logging)
            .values(
                component="zaaktype",
                component_id=self.get_context().get_zaaktype_id(),
                onderwerp=self._create_onderwerp(changes=changes),
                event_type="casetype/mutation",
                event_data=self._create_event_data(changes=changes),
                created_by=created_by,
                created_by_name_cache=created_by_name_cache,
            )
            .returning(schema.Logging.id)
        ).fetchone()
        self.get_context().set_logging_id(id.id)

    def _create_onderwerp(self, changes: dict) -> str:
        """Format the onderwerp for logging."""

        casetype_node_titel: str = getattr_from_dict(
            changes, "general_attributes.name"
        )

        onderwerp: str = (
            f"Zaaktype {casetype_node_titel}"
            f" ({self.get_context().get_zaaktype_id()})"
            f" is opgeslagen"
        )

        if getattr_from_dict(changes, "change_log"):
            if getattr_from_dict(changes, "change_log.update_components"):
                update_components: list[str] = sorted(
                    set(
                        getattr_from_dict(
                            changes, "change_log.update_components"
                        )
                    )
                )
                if len(update_components) == 1:
                    onderwerp += ". Aangepast component is: "
                    onderwerp += ", ".join(update_components)
                if len(update_components) > 1:
                    onderwerp += ". Aangepaste componenten zijn: "
                    onderwerp += ", ".join(update_components)

            if getattr_from_dict(changes, "change_log.update_description"):
                onderwerp += f". Opmerkingen: {getattr_from_dict(changes, 'change_log.update_description')}"

        return onderwerp

    def _create_event_data(self, changes: dict) -> LoggingEventData:
        """Format the event_data for logging."""

        casetype_id: int = self.get_context().get_zaaktype_id()

        update_description: str = ""
        update_components: list[str] = []

        if getattr_from_dict(changes, "change_log"):
            if getattr_from_dict(changes, "change_log.update_components"):
                update_components = sorted(
                    set(
                        getattr_from_dict(
                            changes, "change_log.update_components"
                        )
                    )
                )

            if getattr_from_dict(changes, "change_log.update_description"):
                update_description = getattr_from_dict(
                    changes, "change_log.update_description"
                )

        # the casetype node titel
        casetype_node_titel: str = getattr_from_dict(
            changes, "general_attributes.name"
        )

        logging_event_data: LoggingEventData = {
            "case_type": casetype_id,
            "commit_message": update_description,
            "components": update_components,
            "title": casetype_node_titel,
        }

        return logging_event_data
