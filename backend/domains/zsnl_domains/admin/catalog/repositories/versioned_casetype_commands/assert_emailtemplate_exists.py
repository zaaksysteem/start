# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbTemplate
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertEmailTemplatesExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        self.__assert_email_template_by_uuid(changes)

    def __assert_email_template_by_uuid(self, changes: dict):
        """Check for changes if all email templates, referenced by uuid exist in the db
        If one of the email templates does not exist, raise an exception
        If all email email templates exist, the id, uuid and of name the email template is stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then
        """
        email_template_uuids: set = set()

        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for template in getattr_from_dict(
                phase, "email_templates", default_val=[]
            ):
                email_template_uuids.add(
                    getattr_from_dict(template, "catalog_message_uuid")
                )

        # remove None values
        email_template_uuids = {
            UUID(k) for k in email_template_uuids if k is not None
        }

        # select kenmerken from db and store result in context
        email_templates_db = [
            DbTemplate(
                id=r.id,
                name=r.label,
                uuid=r.uuid,
            )
            for r in self.session.execute(
                sql.select(
                    schema.BibliotheekNotificatie.id,
                    schema.BibliotheekNotificatie.label,
                    schema.BibliotheekNotificatie.uuid,
                ).where(
                    sql.and_(
                        schema.BibliotheekNotificatie.deleted.is_(None),
                        schema.BibliotheekNotificatie.uuid.in_(
                            email_template_uuids
                        ),
                    )
                )
            ).fetchall()
        ]
        diff = email_template_uuids.difference(
            [UUID(str(t.uuid)) for t in email_templates_db if t is not None]
        )
        if len(diff) > 0:
            raise NotFound(
                f"Email template(s) with uuid {', '.join([str(d) for d in diff])} not found."
            )

        for t in email_templates_db:
            self.get_context().add_email_template(email_template=t)
