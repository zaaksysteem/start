# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class InsertZaaktypeTemplatesCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        for phase_count, phase in enumerate(
            getattr_from_dict(changes, "phases", default_val=[]), 1
        ):
            templates = getattr_from_dict(phase, "templates")
            templates_to_insert = []
            for template in templates or []:
                attribute = self.get_context().get_attribute_by_uuid(
                    template["document_attribute_uuid"]
                )
                attribute_id = attribute.id if attribute else None
                templates_to_insert.append(
                    {
                        "zaaktype_node_id": self.get_context().get_zaaktype_node_id(),
                        "bibliotheek_sjablonen_id": self.get_context()
                        .get_template(template["catalog_template_uuid"])
                        .id,
                        "zaak_status_id": self.get_context().get_phase_id(
                            phase_count
                        ),
                        "automatisch_genereren": 1
                        if template.get("auto_generate", False)
                        else None,  # in db, int 1 = True, null = False
                        "target_format": template["target_format"],
                        "custom_filename": template["custom_filename"],
                        "label": template["label"],
                        "allow_edit": template.get("allow_edit", False),
                        "help": template["description"],
                        "bibliotheek_kenmerken_id": attribute_id,
                    }
                )

            if len(templates_to_insert) > 0:
                self.session.execute(
                    sql.insert(schema.ZaaktypeSjabloon).values(
                        templates_to_insert
                    )
                )
