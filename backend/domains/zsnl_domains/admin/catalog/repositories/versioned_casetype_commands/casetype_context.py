# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from uuid import UUID


class DbEntry:
    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name

    id: int
    name: str


class DbKenmerk(DbEntry):
    def __init__(self, id: int, name: str, magic_string: str, uuid: UUID):
        super().__init__(id=id, name=name)
        self.magic_string = magic_string
        self.uuid = uuid

    magic_string: str
    uuid: UUID


class DbTemplate(DbEntry):
    def __init__(self, id: int, name: str, uuid: UUID):
        super().__init__(id=id, name=name)
        self.uuid = uuid

    uuid: UUID


class DbCaseType(DbEntry):
    def __init__(self, id: int, uuid: UUID):
        super().__init__(id=id, name="")
        self.uuid = uuid

    uuid: UUID


class CasetypeContext:
    __zaaktype_id: int | None = None
    __zaaktype_definitie_id: int | None = None
    __zaaktype_node_id: int | None = None
    __logging_id: int | None = None
    __current_user_uuid: UUID | None = None
    __phase_ids: dict = {}  # stores phase status as key, db id as value
    __groups: dict = {}  # stores group uuid as key, DbEntry as value
    __roles: dict = {}  # stores role uuid as key, DbEntry as value
    __attributes: list[DbKenmerk] = []  # stores attributes
    __templates: list[DbTemplate] = []  # stores templates
    __email_templates: list[DbTemplate] = []  # stores email templates
    __casetypes: list[DbCaseType] = []  # stores all casetypes

    def get_zaaktype_id(self) -> int:
        return self.__zaaktype_id

    def set_zaaktype_id(self, zaaktype_id: int) -> None:
        self.__zaaktype_id = zaaktype_id

    def get_zaaktype_definitie_id(self) -> int:
        return self.__zaaktype_definitie_id

    def set_zaaktype_definitie_id(self, zaaktype_definitie_id: int) -> None:
        self.__zaaktype_definitie_id = zaaktype_definitie_id

    def get_zaaktype_node_id(self) -> int:
        return self.__zaaktype_node_id

    def set_zaaktype_node_id(self, zaaktype_node_id: int) -> None:
        self.__zaaktype_node_id = zaaktype_node_id

    def get_logging_id(self) -> int:
        return self.__logging_id

    def set_logging_id(self, logging_id: int) -> None:
        self.__logging_id = logging_id

    def get_current_user_uuid(self) -> UUID:
        return self.__current_user_uuid

    def set_current_user_uuid(self, current_user_uuid: UUID) -> None:
        self.__current_user_uuid = current_user_uuid

    def get_phase_id(self, status: int) -> int:
        return self.__phase_ids.get(status)

    def set_phase_id(self, status: int, db_id: int):
        self.__phase_ids[status] = db_id

    def get_group(self, group_uuid: UUID | str) -> DbEntry:
        return self.__groups.get(str(group_uuid))

    def set_group(self, group_uuid: UUID | str, group: DbEntry):
        self.__groups[str(group_uuid)] = group

    def get_role(self, role_uuid: UUID | str) -> DbEntry:
        return self.__roles.get(str(role_uuid))

    def set_role(self, role_uuid: UUID | str, role: DbEntry):
        self.__roles[str(role_uuid)] = role

    def get_attribute_by_uuid(self, uuid: UUID) -> DbKenmerk | None:
        for attribute in self.__attributes:
            if str(attribute.uuid) == str(uuid):
                return attribute

    def add_attribute(self, attribute: DbKenmerk):
        self.__attributes.append(attribute)

    def get_template(self, template_uuid: UUID | str) -> DbTemplate:
        for template in self.__templates:
            if str(template.uuid) == str(template_uuid):
                return template

    def add_template(self, template: DbTemplate) -> None:
        self.__templates.append(template)

    def get_email_template(
        self, email_template_uuid: UUID | str
    ) -> DbTemplate:
        for template in self.__email_templates:
            if str(template.uuid) == str(email_template_uuid):
                return template

    def add_email_template(self, email_template: DbTemplate) -> None:
        self.__email_templates.append(email_template)

    def get_casetype(self, uuid: UUID | str) -> DbCaseType:
        for casetype in self.__casetypes:
            if (str(casetype.uuid)) == str(uuid):
                return casetype

    def add_casetype(self, casetype: DbCaseType):
        self.__casetypes.append(casetype)
