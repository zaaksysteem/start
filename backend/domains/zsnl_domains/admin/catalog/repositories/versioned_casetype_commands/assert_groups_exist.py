# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbEntry
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertGroupsExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        """Check for changes if all groups exist in the db
        If one of the groups does not exist, raise an exception
        If all groups exist, the id of the groups is stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then
        """
        group_uuids: set = set()

        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            group_uuids.add(
                getattr_from_dict(phase, "assignment.department_uuid")
            )
            # collect all group uuids used in attributes
            for custom_field in getattr_from_dict(
                phase, "custom_fields", default_val=[]
            ):
                for permission in getattr_from_dict(
                    custom_field, "permissions", default_val=[]
                ):
                    group_uuids.add(
                        getattr_from_dict(permission, "department_uuid")
                    )

            # collect all group uuids for cases configures in a phase
            for case in getattr_from_dict(phase, attr="cases", default_val=[]):
                group_uuids.add(
                    getattr_from_dict(case, "allocation.department_uuid")
                )

        # collect all group uuids for zaaktype_autorization
        for autorisation in getattr_from_dict(changes, "authorization"):
            group_uuids.add(getattr_from_dict(autorisation, "department_uuid"))

        # remove None value from set
        group_uuids = {g for g in group_uuids if g is not None}

        # select groups from db and store result in context
        groups_db = {
            str(r.uuid): DbEntry(id=r.id, name=r.name)
            for r in self.session.execute(
                sql.select(
                    schema.Group.id, schema.Group.uuid, schema.Group.name
                ).where(schema.Group.uuid.in_(group_uuids))
            ).fetchall()
        }
        diff = group_uuids.difference(groups_db.keys())
        if len(diff) > 0:
            raise NotFound(
                f"Department(s) with uuid {', '.join(diff)} not found."
            )

        for k, v in groups_db.items():
            self.get_context().set_group(group_uuid=UUID(k), group=v)
