# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbTemplate
from .db_command import DbCommand
from minty.exceptions import NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertTemplatesExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        self.__assert_template_by_uuid(changes)

    def __assert_template_by_uuid(self, changes: dict):
        """Check for changes if all templates, referenced by uuid exist in the db
        If one of the templates does not exist, raise an exception
        If all templates exist, the id, uuid and name of the template is stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then
        """
        template_uuids: set = set()

        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for template in getattr_from_dict(
                phase, "templates", default_val=[]
            ):
                template_uuids.add(
                    getattr_from_dict(template, "catalog_template_uuid")
                )

        # remove None values
        template_uuids = {UUID(k) for k in template_uuids if k is not None}

        # select kenmerken from db and store result in context
        templates_db = [
            DbTemplate(
                id=r.id,
                name=r.naam,
                uuid=r.uuid,
            )
            for r in self.session.execute(
                sql.select(
                    schema.BibliotheekSjabloon.id,
                    schema.BibliotheekSjabloon.naam,
                    schema.BibliotheekSjabloon.uuid,
                ).where(
                    sql.and_(
                        schema.BibliotheekSjabloon.deleted.is_(None),
                        schema.BibliotheekSjabloon.uuid.in_(template_uuids),
                    )
                )
            ).fetchall()
        ]
        diff = template_uuids.difference(
            [UUID(str(t.uuid)) for t in templates_db if t is not None]
        )
        if len(diff) > 0:
            raise NotFound(
                f"Template(s) with uuid {', '.join([str(d) for d in diff])} not found."
            )

        for t in templates_db:
            self.get_context().add_template(template=t)
