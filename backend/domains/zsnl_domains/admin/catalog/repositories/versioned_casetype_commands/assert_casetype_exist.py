# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from .....shared.util import getattr_from_dict
from .casetype_context import CasetypeContext, DbCaseType
from .db_command import DbCommand
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class AssertCasetypeExist(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        """Check for changes if all casetypes, referenced by uuid exist in the db
        If one of the casetypes does not exist, raise an exception
        If all casetypes exist, the id and uuid of the casetype are stored in the CasetypeContext,
        so they can be used when inserting the casetype records. Query doesn't have to be performed again then

        Next to that, also check if the configured attribute mapping does exist in the source and target casetype.
        """
        casetype_uuids: set = set()
        source_casetype_attributes: set = (
            set()
        )  # set of uuids for source casetype configured in cases configuration

        target_casetype_attributes: dict = {}  # dict subcasetype_uuid : set[attribute uuid]
        for phase in getattr_from_dict(changes, "phases", default_val=[]):
            for case in getattr_from_dict(phase, "cases", default_val=[]):
                related_casetype_uuid = getattr_from_dict(
                    case, "related_casetype"
                )
                casetype_uuids.add(related_casetype_uuid)
                mapping = case.get("copy_attributes_mapping", {})
                if mapping:
                    for k in mapping.keys():
                        try:
                            UUID(
                                k
                            )  # raises an value error if key is not of type UUID
                        except ValueError as error:
                            raise Conflict(
                                f"Key {k} in copy_attribute_mapping is not of type UUID."
                            ) from error
                    source_casetype_attributes.update(mapping.keys())
                    expected_uuids = set(
                        target_casetype_attributes.get(
                            related_casetype_uuid, []
                        )
                    )
                    expected_uuids.update(mapping.values())
                    target_casetype_attributes[related_casetype_uuid] = (
                        expected_uuids
                    )

        # remove None values
        casetype_uuids = {UUID(k) for k in casetype_uuids if k is not None}
        self._assert_related_casetype_exist(
            related_casetype_uuids=casetype_uuids
        )
        self._assert_attributes_source_casetype_exist(
            source_attributes=source_casetype_attributes
        )
        self._assert_attributes_target_casetype_exist(
            target_casetype_attributes=target_casetype_attributes
        )

    def _assert_attributes_source_casetype_exist(self, source_attributes: set):
        # check if the configured attributes are present in the current_casetype.
        # all configured attributes are already present in the context, so we can look them up there.

        not_found = [
            uuid
            for uuid in source_attributes
            if not self.get_context().get_attribute_by_uuid(uuid=uuid)
        ]
        if not_found:
            raise Conflict(
                f"Casetype configuration for cases invalid. The supplied source attribute uuid(s) {not_found} are not present in this casetype."
            )

    def _assert_attributes_target_casetype_exist(
        self, target_casetype_attributes: dict
    ):
        for (
            related_casetype_uuid,
            expected_attribute_uuids,
        ) in target_casetype_attributes.items():
            find_related_attributes_query = (
                sql.select(schema.BibliotheekKenmerk.uuid)
                .select_from(
                    sql.join(
                        schema.ZaaktypeKenmerk,
                        schema.BibliotheekKenmerk,
                        schema.BibliotheekKenmerk.id
                        == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                    ).join(
                        schema.Zaaktype,
                        schema.Zaaktype.zaaktype_node_id
                        == schema.ZaaktypeKenmerk.zaaktype_node_id,
                    )
                )
                .where(
                    sql.and_(
                        schema.BibliotheekKenmerk.uuid.in_(
                            expected_attribute_uuids
                        ),
                        schema.Zaaktype.uuid == related_casetype_uuid,
                    )
                )
            )
            found_attributes_by_uuid = set(
                [
                    str(r.uuid)
                    for r in self.session.execute(
                        find_related_attributes_query
                    ).fetchall()
                ]
            )
            missing = expected_attribute_uuids - found_attributes_by_uuid
            if missing:
                raise Conflict(
                    f"Casetype configuration for cases with casetype uuid {related_casetype_uuid} invalid. The provided attribute uuid's {missing} are not present in the target casetype."
                )

    def _assert_related_casetype_exist(self, related_casetype_uuids: set):
        # select casetypes from db and store result in context
        casetypes_db = [
            DbCaseType(
                id=r.id,
                uuid=r.uuid,
            )
            for r in self.session.execute(
                sql.select(
                    schema.Zaaktype.id,
                    schema.Zaaktype.uuid,
                ).where(
                    sql.and_(
                        schema.Zaaktype.deleted.is_(None),
                        schema.Zaaktype.uuid.in_(related_casetype_uuids),
                    )
                )
            ).fetchall()
        ]
        diff = related_casetype_uuids.difference(
            [UUID(str(t.uuid)) for t in casetypes_db if t is not None]
        )
        if len(diff) > 0:
            raise NotFound(
                f"Casetypes(s) with uuid {', '.join([str(d) for d in diff])} not found."
            )

        for ct in casetypes_db:
            self.get_context().add_casetype(
                casetype=DbCaseType(id=ct.id, uuid=ct.uuid)
            )
