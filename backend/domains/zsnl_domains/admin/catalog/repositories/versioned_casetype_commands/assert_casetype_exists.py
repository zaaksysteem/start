# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .casetype_context import CasetypeContext
from .db_command import DbCommand
from minty.exceptions import Conflict, NotFound
from sqlalchemy import sql
from zsnl_domains.database import schema


class AssertZaaktypeExistsCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        row = self.session.execute(
            sql.select(schema.Zaaktype.id).where(
                schema.Zaaktype.uuid == changes.get("casetype_uuid")
            )
        ).fetchone()
        if not row:
            raise NotFound(
                f"Casetype with uuid {changes.get('casetype_uuid')} does not exist"
            )
        self.get_context().set_zaaktype_id(row.id)

        # check if casetype_node uuid doesn't exist
        row = self.session.execute(
            sql.select(
                schema.ZaaktypeNode.uuid,
            ).where(schema.ZaaktypeNode.uuid == changes.get("uuid"))
        ).fetchone()

        if row:
            raise Conflict(
                f"Casetypeversion not unique. Casetypeversion with uuid {changes.get('uuid')} already exists"
            )
