# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .casetype_context import CasetypeContext
from .db_command import DbCommand
from sqlalchemy import sql
from zsnl_domains.database import schema


class UpdateZaaktypeNodeIdCommand(DbCommand[dict, CasetypeContext]):
    def execute(
        self,
        changes: dict,
    ) -> None:
        # We update the casetype row to ensure that a trigger gets executed
        # so we have v1_json data available after casetype creation.
        self.session.execute(
            sql.update(schema.ZaaktypeNode)
            .values(
                id=self.get_context().get_zaaktype_node_id(),
            )
            .where(
                schema.ZaaktypeNode.id
                == self.get_context().get_zaaktype_node_id()
            )
        )
