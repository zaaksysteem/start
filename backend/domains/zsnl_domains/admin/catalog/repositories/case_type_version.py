# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from .. import entities
from . import database_queries
from minty.exceptions import NotFound
from uuid import UUID


class CaseTypeVersionRepository(ZaaksysteemRepositoryBase):
    case_type_mapping: dict[str, str] = {
        "id": "id",
        "name": "name",
        "active": "active",
        "reason": "reason",
    }

    def get_case_type_versions_history_by_uuid(self, uuid: UUID):
        """Get Case Type version history for a case type from database.

        :param uuid: case type uuid
        :type uuid: UUID
        :raises NotFound: record not found
        :return: List of entities.CaseTypeVersion
        :rtype: list
        """
        case_type_version_query_results = self.session.execute(
            database_queries.case_type_versions_history(uuid)
        ).fetchall()

        case_type_versions = []
        for ctv in case_type_version_query_results:
            commit_message = None
            components = None
            if ctv.event_data is not None:
                event_data = ctv.event_data
                commit_message = event_data.get("commit_message", "")
                components = event_data.get("components", [])
            case_type_versions.append(
                entities.CaseTypeVersion(
                    uuid=ctv.uuid,
                    case_type_uuid=ctv.case_type_uuid,
                    name=ctv.name,
                    username=ctv.username,
                    display_name=ctv.display_name,
                    active=ctv.active,
                    version=ctv.version,
                    created=ctv.created,
                    last_modified=ctv.last_modified,
                    reason=ctv.reason,
                    change_note=commit_message,
                    modified_components=components,
                )
            )

        return case_type_versions

    def get_case_type_version_by_uuid(self, uuid: UUID):
        """Get Case Type version by uuid from database.

        :param uuid: case type uuid
        :type uuid: UUID
        :raises NotFound: record not found
        :return: an Object of entities.CaseTypeVersion
        :rtype: entities.CaseTypeVersion
        """

        query_result = self.session.execute(
            database_queries.case_type_version(uuid)
        ).fetchone()
        if query_result is None:
            raise NotFound(
                f"Case type version with uuid {uuid} not found",
                "case_type_version/not_found",
            )

        return self._sqla_to_entity(query_result)

    def save(self):
        """Save changes on case type version entity back to database."""
        raise NotImplementedError

    def _sqla_to_entity(self, query_result) -> entities.CaseTypeVersionEntity:
        """Initialize case type version entity from sqla objects.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type_version
        :rtype: entities.CaseTypeEntity
        """
        case_type_sqla = query_result
        case_type_version = entities.CaseTypeVersionEntity(
            id=case_type_sqla.id,
            uuid=case_type_sqla.uuid,
            name=case_type_sqla.name,
            case_type_uuid=case_type_sqla.case_type_uuid,
            active=case_type_sqla.active,
            version=case_type_sqla.version,
            created=case_type_sqla.created,
            last_modified=case_type_sqla.last_modified,
        )
        return case_type_version
