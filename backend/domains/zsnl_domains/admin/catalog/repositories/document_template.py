# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import re
from .... import ZaaksysteemRepositoryBase
from ..entities.document_template import DocumentTemplate
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import and_, func, insert, join, select, update
from uuid import UUID
from zsnl_domains.database.schema import (
    BibliotheekCategorie,
    BibliotheekSjabloon,
    Filestore,
    Interface,
    Zaaktype,
    ZaaktypeNode,
    ZaaktypeSjabloon,
)


class DocumentTemplateRepository(ZaaksysteemRepositoryBase):
    def get_document_template_details_by_uuid(
        self, uuid: UUID
    ) -> DocumentTemplate:
        """Get detailed information about a document template by uuid.

        :param DatabaseRepositoryBase.
        :type DatabaseRepositoryBase: DatabaseRepositoryBase
        :param uuid: UUID of the document template
        :type uuid: UUID
        :raises NotFound: When document template with uuid not found
        :return: DocumentTemplate entity
        :rtype: DocumentTemplate
        """

        try:
            query_result = (
                self.session.query(
                    BibliotheekSjabloon,
                    BibliotheekCategorie,
                    Filestore,
                    Interface,
                )
                .outerjoin(BibliotheekCategorie)
                .outerjoin(Filestore)
                .outerjoin(Interface)
                .filter(
                    and_(
                        BibliotheekSjabloon.uuid == uuid,
                        BibliotheekSjabloon.deleted.is_(None),
                    )
                )
                .one()
            )
        except NoResultFound as err:
            raise NotFound(
                f"Document template with uuid {uuid} not found",
                "document_template/not_found",
            ) from err

        document_template = query_result.BibliotheekSjabloon
        bibliotheek_categorie = query_result.BibliotheekCategorie
        filestore = query_result.Filestore
        interface = query_result.Interface

        return self._transform_to_entity(
            document_template=document_template,
            filestore=filestore,
            bibliotheek_categorie=bibliotheek_categorie,
            interface=interface,
        )

    def _transform_to_entity(
        self,
        document_template: BibliotheekSjabloon,
        filestore: Filestore,
        bibliotheek_categorie: BibliotheekCategorie,
        interface: Interface,
    ):
        """Transform an document template object to an DocumentTemplate Entity.

        :param document_template: DocumentTemplate object
        :type email_template: BibliotheekNotificatie
        :param bibliotheek_categorie: Parent category of document_template
        :type email_template: BibliotheekCategorie
        :param interface: Interface
        :type interface: Interface
        :return: DocumentTemplate Entity
        :rtype: DocumentTemplate
        """

        entity = DocumentTemplate(
            uuid=document_template.uuid,
            id=document_template.id,
            name=document_template.naam,
            file_uuid=getattr(filestore, "uuid", None),
            file_name=getattr(filestore, "original_name", None),
            integration_uuid=getattr(interface, "uuid", None),
            integration_reference=document_template.template_external_name,
            category_uuid=getattr(bibliotheek_categorie, "uuid", None),
            help=document_template.help,
        )
        entity.event_service = self.event_service
        return entity

    def create_new_document_template(self, uuid: UUID, fields: dict):
        """Create new document template.

        :param uuid: UUID of the document template to be created
        :type uuid: UUID
        :param fields: fields of the document template to be created
        :type fields: dict
        :return: DocumentTemplate Entity
        :rtype: DocumentTemplate
        """
        document_template_entity = DocumentTemplate(
            uuid=uuid,
            id=None,
            name=None,
            file_uuid=None,
            file_name=None,
            integration_uuid=None,
            integration_reference=None,
            help=None,
            category_uuid=None,
        )
        document_template_entity.event_service = self.event_service
        document_template_entity.create(fields=fields)
        return document_template_entity

    def delete_document_template(self, uuid: UUID, reason: str):
        document_template_entity = DocumentTemplate(
            uuid=uuid,
            id=None,
            name=None,
            file_uuid=None,
            file_name=None,
            integration_uuid=None,
            integration_reference=None,
            help=None,
            category_uuid=None,
        )
        document_template_entity.event_service = self.event_service
        document_template_entity.delete(reason=reason)
        return document_template_entity

    def save(self):
        """Save changes in Database"""
        for event in self.event_service.event_list:
            if event.entity_type == "DocumentTemplate":
                formatted_changes = {}
                for change in event.changes:
                    formatted_changes[change["key"]] = change["new_value"]

                if event.event_name == "DocumentTemplateCreated":
                    self._insert_document_template(
                        uuid=event.entity_id,
                        formatted_changes=formatted_changes,
                    )
                elif event.event_name == "DocumentTemplateEdited":
                    self._edit_document_template(
                        uuid=event.entity_id,
                        formatted_changes=formatted_changes,
                    )
                elif event.event_name == "DocumentTemplateDeleted":
                    self._delete_document_template(
                        uuid=event.entity_id,
                        formatted_changes=formatted_changes,
                    )

    def _insert_document_template(self, uuid: UUID, formatted_changes: dict):
        insert_values = self._generate_database_values(
            formatted_changes=formatted_changes
        )

        insert_values["uuid"] = uuid
        insert_stmt = insert(BibliotheekSjabloon).values(**insert_values)
        try:
            self.session.execute(insert_stmt)
        except IntegrityError as err:
            raise Conflict(
                f"Document template with uuid {uuid} already exists",
                "document_template/already_exists_with_uuid",
            ) from err

    def _edit_document_template(self, uuid: UUID, formatted_changes: dict):
        document_template_uuid = uuid
        update_values = self._generate_database_values(
            formatted_changes=formatted_changes
        )
        update_stmt = (
            update(BibliotheekSjabloon)
            .where(BibliotheekSjabloon.uuid == document_template_uuid)
            .values(**update_values)
        )
        self.session.execute(update_stmt)

    def _delete_document_template(self, uuid: UUID, formatted_changes: dict):
        document_template_uuid = uuid
        if self._get_case_types_related_to_document_template(
            uuid=document_template_uuid
        ):
            raise Conflict(
                "Document template used in case types",
                "document_template/used_in_case_types",
            )

        delete_values = self._generate_database_values(
            formatted_changes=formatted_changes
        )
        delete_values["bibliotheek_categorie_id"] = None
        delete_stmt = (
            update(BibliotheekSjabloon)
            .where(BibliotheekSjabloon.uuid == document_template_uuid)
            .values(**delete_values)
        )
        self.session.execute(delete_stmt)

    def _generate_database_values(self, formatted_changes: dict):
        database_mapping = {
            "category_uuid": "bibliotheek_categorie_id",
            "name": "naam",
            "file_uuid": "filestore_id",
            "file_name": None,
            "integration_uuid": "interface_id",
            "integration_reference": "template_external_name",
            "help": "help",
            "commit_message": None,
            "deleted": "deleted",
        }
        values = {}
        for key in formatted_changes:
            db_fieldname = database_mapping[key]

            if key == "category_uuid" and formatted_changes.get(
                "category_uuid"
            ):
                stmt = (
                    select(BibliotheekCategorie.id)
                    .where(
                        BibliotheekCategorie.uuid
                        == formatted_changes["category_uuid"]
                    )
                    .scalar_subquery()
                )
                values[db_fieldname] = stmt

            elif key == "integration_uuid" and formatted_changes.get(
                "integration_uuid"
            ):
                self._check_integration_reference(
                    integration_uuid=formatted_changes["integration_uuid"],
                    integration_reference=formatted_changes[
                        "integration_reference"
                    ],
                )
                stmt = (
                    select(Interface.id)
                    .where(
                        Interface.uuid == formatted_changes["integration_uuid"]
                    )
                    .scalar_subquery()
                )
                values[db_fieldname] = stmt

            elif key == "file_uuid" and formatted_changes.get("file_uuid"):
                if formatted_changes.get("integration_uuid"):
                    continue
                stmt = (
                    select(Filestore.id)
                    .where(Filestore.uuid == formatted_changes["file_uuid"])
                    .scalar_subquery()
                )
                values[db_fieldname] = stmt

            elif db_fieldname is None:
                pass
            else:
                values[db_fieldname] = formatted_changes[key]
        return values

    def _check_integration_reference(
        self, integration_uuid: UUID, integration_reference: str
    ):
        integration = (
            self.session.query(Interface)
            .filter(Interface.uuid == integration_uuid)
            .one()
        )

        if integration.module == "xential":
            UUID_PATTERN = re.compile(
                r"^[\da-f]{8}-([\da-f]{4}-){3}[\da-f]{12}$", re.IGNORECASE
            )
            if not UUID_PATTERN.match(integration_reference):
                raise Conflict(
                    f"Incorrect template_name '{integration_reference}'",
                    "document_template/incorrect_template_name",
                )
        elif integration.module == "stuf_dcr":
            if re.match(r"^\s+$", integration_reference):
                raise Conflict(
                    f"Incorrect template_name '{integration_reference}'",
                    "document_template/incorrect_template_name",
                )

    def _get_case_types_related_to_document_template(self, uuid: UUID):
        """Get case types related to an document.
        :param document_template_uuid: document_template uuid
        :type document_template_uuid: UUID
        :return: List of casetypes related to the document_template
        :rtype: list
        """
        document_template_id = (
            select(BibliotheekSjabloon.id)
            .where(BibliotheekSjabloon.uuid == uuid)
            .scalar_subquery()
        )

        zaaktype_joined = join(
            ZaaktypeSjabloon,
            ZaaktypeNode,
            ZaaktypeSjabloon.zaaktype_node_id == ZaaktypeNode.id,
        ).join(Zaaktype, ZaaktypeNode.zaaktype_id == Zaaktype.id)

        case_types_qry_stmt = (
            select(func.count().label("cnt"))
            .select_from(zaaktype_joined)
            .where(
                and_(
                    Zaaktype.deleted.is_(None),
                    ZaaktypeSjabloon.bibliotheek_sjablonen_id
                    == document_template_id,
                )
            )
        )

        related_case_types = self.session.execute(
            case_types_qry_stmt
        ).fetchone()

        return related_case_types.cnt
