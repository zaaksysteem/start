# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .catalog_entities import EntryType, Folder
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from uuid import UUID


class FolderEntry(EntityBase):
    """An entry in a folder in the catalog.

    Similar to a directory entry on a file system, this represents a single item
    in a collection (folder) of catalog items.
    """

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        entry_type: EntryType,
        uuid: UUID,
        name: str,
        active: bool,
        folder_id: UUID = None,
        id: int = None,
    ):
        self.entry_type = entry_type
        self.uuid = uuid
        self.name = name
        self.active = active
        self.folder_id = folder_id
        self.id = id

    @event("FolderEntryMoved", extra_fields=["name", "entry_type"])
    def move(self, destination_folder: Folder):
        """Move `FolderEntry` to destination folder.

        :param destination_folder: destination folder
        :type destination_folder: Folder
        :raises Conflict: When move is invalid.
        """
        if self.entry_type == "folder":
            if (
                self.id in destination_folder.parent_ids
                or self.id == destination_folder.id
            ):
                raise Conflict(
                    f"Can't move folder '{self.name}' into child folder of itself",
                    "folder_entry/can't_move_to_child",
                )
        self.folder_id = destination_folder.id
