#!/usr/bin/env python

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = f.read().splitlines()

with open("requirements/test.txt") as f:
    test_requirements = f.read().splitlines()

setup(
    author="Jesse Burger",
    author_email="jesse@mintlab.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="QueueRunnner is a multithreaded RabbitMQ consumer client",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    license="EUPL license",
    long_description=readme,
    include_package_data=True,
    keywords="queue_runner_v2",
    name="queue_runner_v2",
    packages=find_packages(include=["queue_runner_v2"]),
    test_suite="tests",
    tests_require=test_requirements,
    url="https://gitlab.com/minty-python/queue_runner_v2",
    version="0.0.1",
    zip_safe=False,
)
