# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

#####################################################################
### Stage 1: Setup base python with requirements
#####################################################################
FROM python:3.11-slim-bookworm AS base

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
  git \
  netcat-traditional \
  psmisc

COPY backend/consumer-queue_runner/requirements/base.txt /opt/consumer-queue_runner/requirements.txt

WORKDIR /opt/consumer-queue_runner

RUN python -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install --upgrade pip \
  && pip install -r requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

COPY backend/consumer-queue_runner /opt/consumer-queue_runner

#####################################################################
## Stage 2: Production build
#####################################################################
FROM python:3.11-slim-bookworm AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"

RUN apt-get update && apt-get install -y netcat-traditional

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/consumer-queue_runner
RUN pip install -e .
ENTRYPOINT ["/opt/consumer-queue_runner/startup.sh"]

#####################################################################
## Stage 3: QA environment
#####################################################################
FROM base AS quality-and-testing

ENV OTAP=test

COPY backend/consumer-queue_runner/requirements/test.txt /opt/consumer-queue_runner/requirements-test.txt
WORKDIR /opt/consumer-queue_runner

RUN pip install -r requirements-test.txt \
  && pip install -e .

RUN  mkdir -p /root/test_result \
  && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

ENTRYPOINT ["/opt/consumer-queue_runner/startup.sh"]

#####################################################################
## Stage 4: Development image
#####################################################################
FROM quality-and-testing AS development

ENV OTAP=development

COPY backend/consumer-queue_runner/requirements/dev.txt /opt/consumer-queue_runner/requirements-dev.txt
WORKDIR /opt/consumer-queue_runner

RUN pip install -r requirements-dev.txt
