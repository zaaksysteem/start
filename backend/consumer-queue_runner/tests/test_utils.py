# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from queue_runner_v2.utils import parse_arguments


class TestUtils:
    def test_parse_arguments(self):
        rabbitmq_url = "https://example.rabbitmq.com"
        statsd_host = "https://example.statsd.com"
        statsd_port = "1234"
        number_of_threads = "9999"
        heartbeats = "800"
        arguments = [
            "--rabbitmq-url",
            rabbitmq_url,
            "--statsd-host",
            statsd_host,
            "--statsd-port",
            statsd_port,
            "--number-of-threads",
            number_of_threads,
            "--heartbeats",
            heartbeats,
        ]

        args = parse_arguments(arguments)
        assert args.rabbitmq_url == rabbitmq_url
        assert args.statsd_host == statsd_host
        assert args.statsd_port == statsd_port
        assert args.debug is False
        assert args.number_of_threads == number_of_threads
        assert args.heartbeats == heartbeats

        arguments_2 = ["--debug"]
        args2 = parse_arguments(arguments_2)
        assert args2.debug is True
