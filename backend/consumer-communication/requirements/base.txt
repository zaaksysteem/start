setuptools>=69.2.0

## Framework
../minty
../minty-amqp

../minty-infra-amqp
../minty-infra-email
../minty-infra-misc
../minty-infra-sqlalchemy
../minty-infra-storage

python-json-logger~=2.0

##  Project specific requirements
../domains
