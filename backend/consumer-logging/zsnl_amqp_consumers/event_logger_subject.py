# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    PersonInformation,
    get_employee,
    get_organization,
    get_person,
    get_user,
)
from uuid import UUID
from zsnl_domains.database.schema import Logging

MAP_UPDATED_FIELD_NAMES = {
    "phone_number": "telefoonnummer",
    "mobile_number": "telefoonnummer (mobiel)",
    "internal_note": "interne notitie",
    "email": "e-mailadres",
    "is_anonymous_contact": "is anoniem/vergrendeld",
    "preferred_contact_channel": "contactkanaal-voorkeur",
}

MAP_UPDATED_PERSON_FIELD_NAMES = {
    "first_names": "voornamen",
    "family_name": "familie naam",
    "surname": "naamgebruik",
    "initials": "initialen",
    "inside_municipality": "binnengemeentelijk",
    "insertions": "tussenvoegsels",
    "noble_title": "adellijke titel",
    "gender": "geslacht",
    "country_code": "landcode",
}

MAP_ADDRESS = {
    "city": "stad",
    "country": "land",
    "country_code": "landcode",
    "geo_lat_long": "coordinaten",
    "is_foreign": "buitenlands",
    "street": "straat",
    "street_number": "straatnummer",
    "street_number_letter": "huisletter",
    "street_number_suffix": "huisnummertoevoeging",
    "zipcode": "postcode",
    "address_line_1": "adresregel 1",
    "address_line_2": "adresregel 2",
    "address_line_3": "adresregel 3",
}


class SubjectBase(BaseLogger):
    def __init__(self):
        self.variables = None
        self.event_type = None
        self.subject = None

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record."""

        if event["entity_type"] == "Person":
            subject_info = get_person(session=session, uuid=event["entity_id"])

        elif event["entity_type"] == "Organization":
            subject_info = get_organization(
                session=session, uuid=event["entity_id"]
            )

        elif event["entity_type"] == "Employee":
            subject_info = get_employee(
                session=session, uuid=event["entity_id"]
            )

        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)
        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            subject_info=subject_info,
            uuid=event["entity_id"],
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="betrokkene",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            created_for=event_parameters.get("betrokkene_identifier", None),
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message."""
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        raise NotImplementedError


class BsnRetrieved(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Inzage verleend op betrokkene '{}', veld 'bsn'."
        self.variables = ["name"]
        self.event_type = "subject/inspect"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class PersonUpdated(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Info van '{}' gewijzigd: {}."
        self.variables = ["name", "changes"]
        self.event_type = "subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""

        address_fields = ["correspondence_address", "residence_address"]

        changed_fields: list[str] = []
        for key, dutch_key in MAP_UPDATED_PERSON_FIELD_NAMES.items():
            key_old = f"{key}_old"
            new_value = entity_data.get(key)
            old_value = entity_data.get(key_old)
            if key == "country_code":
                new_value, old_value = str(new_value), str(old_value)
            if new_value != old_value:
                changed_fields.append(dutch_key)

        for address_field in address_fields:
            old_key = f"{address_field}_old"
            old_address_dict = (
                {}
                if not entity_data.get(old_key, {})
                else entity_data.get(old_key, {})
            )
            new_address_dict = (
                {}
                if not entity_data.get(address_field, {})
                else entity_data.get(address_field, {})
            )
            changes = self._compare_address(old_address_dict, new_address_dict)

            if changes:
                changed_fields.extend(changes)

        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
            "changes": ", ".join(changed_fields),
        }
        return event_parameters

    def _compare_address(
        self, address_dict_old: dict, address_dict_new: dict
    ) -> list:
        """Compares two address dictionaries to determine if they are equal, if not returns changed values as list"""
        changes: list[str] = []
        for key, dutch_key in MAP_ADDRESS.items():
            old_value = address_dict_old.get(key, "")
            new_value = address_dict_new.get(key, "")
            if old_value != new_value:
                changes.append(dutch_key)
        return changes


class NonAuthenticBsnUpdated(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "BSN van '{}' gewijzigd."
        self.variables = ["name"]
        self.event_type = "subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class NonAuthenticSedulaUpdated(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Sedula van '{}' gewijzigd."
        self.variables = ["name"]
        self.event_type = "subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class ContactInformationSaved(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Plusgegevens voor betrokkene '{}' gewijzigd: {}"
        self.variables = ["name", "updated_fields"]
        self.event_type = "subject/update_contact_data"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        updated_fields = [
            MAP_UPDATED_FIELD_NAMES.get(key, key)
            for key in entity_data["contact_information"]
            if entity_data["contact_information"][key]
            != entity_data["contact_information_old"][key]
        ]
        updated_fields = ", ".join(updated_fields)

        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
            "updated_fields": updated_fields,
        }
        return event_parameters


class ContactViewed(SubjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Contact geopend op  betrokkene '{}'"
        self.variables = ["name"]
        self.event_type = "subject/contact_viewed"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        if subject_info.type == "employee":
            betrokkene_type = "medewerker"
        else:
            betrokkene_type = subject_info.type

        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{betrokkene_type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters
