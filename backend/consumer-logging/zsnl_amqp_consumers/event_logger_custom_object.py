# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    CustomObjectInformation,
    UserInformation,
    get_case,
    get_custom_object,
    get_user,
)
from typing import Any, TypedDict
from zsnl_domains.database.schema import Logging


class CustomFieldDefinition(TypedDict):
    magic_string: str
    label: str


class CustomObjectBase(BaseLogger):
    def __init__(self):
        self.variables = None
        self.event_type = None
        self.subject = None

    def __call__(self, session, event) -> Logging:
        "Collect and necessary information before creating log record."

        formatted_entity_data = self.format_entity_data(event=event)

        if event["event_name"] == "CustomObjectDeleted":
            custom_object_info = CustomObjectInformation(
                title=formatted_entity_data["title_old"],
                custom_object_type=None,
                custom_object_uuid=formatted_entity_data[
                    "version_independent_uuid_old"
                ],
                version=0,
                custom_field_definition=[],
            )
        else:
            custom_object_info = get_custom_object(
                session=session, uuid=event["entity_id"]
            )

        user_info = get_user(session=session, uuid=event["user_uuid"])

        event_parameters = self.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            custom_object_info=custom_object_info,
            user_info=user_info,
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="custom_object",
            component_id=None,
            case_id=event_parameters.get("case_id", None),
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        "Generate subject line formatted with parameters from decoded message."

        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ):
        raise NotImplementedError


class CustomObjectCreated(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Object '{}' van objecttype '{}' is aangemaakt."
        self.variables = ["custom_object_name", "custom_object_type"]
        self.event_type = "custom_object/created"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }

        return event_params


class CustomObjectRetrieved(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Object '{}' van objecttype '{}' is opgevraagd."
        self.variables = [
            "custom_object_name",
            "custom_object_type",
        ]
        self.event_type = "custom_object/retrieved"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict[str, str]:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
            "custom_object_version": custom_object_info.version,
        }
        return event_params


class CustomObjectUpdated(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Object '{}' van objecttype '{}' is aangepast."
        self.variables = [
            "custom_object_name",
            "custom_object_type",
        ]
        self.event_type = "custom_object/updated"

    def _normalize_value(self, full_value: dict[str, Any] | None) -> Any:
        """
        Normalize custom field values for comparison, to determine if something
        was actually changed.

        Example values:

            full_value = {
                "type": "something",
                "value": "some text", # value
            }
            normalized = "some text"

            full_value = {
                "type": "something",
                "value": [           # value
                    "checkbox1",     # inner_value
                    "checkbox2"
                ],
            }
            normalized = {"checkbox1", "checkbox2"}

            full_value = {
                "type": "relationship",
                "value": [           # value
                    {                # inner_value
                        "label": "xxx",
                        "value": "de5016ff-abb6-45d7-98a6-071dad762eb4", # "v"
                    }
                ],
            }
            normalized = {"de5016ff-abb6-45d7-98a6-071dad762eb4"}
        """

        if full_value is None:
            return

        value = full_value.get("value")

        if isinstance(value, list):
            normalized = set()
            for inner_value in value:
                if isinstance(inner_value, dict) and (
                    v := inner_value.get("value")
                ):
                    normalized.add(v)
                else:
                    normalized.add(inner_value)

            return normalized

        return value

    def _summarize_custom_field_changes(
        self,
        custom_object_info: CustomObjectInformation,
        entity_data: dict[str, Any],
    ) -> list[CustomFieldDefinition]:
        changed_fields: list[CustomFieldDefinition] = []

        field_name_mapping: dict[str, str] = {
            field["magic_string"]: field["label"] or field["name"]
            for field in custom_object_info.custom_field_definition
        }

        custom_fields_old = entity_data.get("custom_fields_old", {})
        custom_fields_new = entity_data.get("custom_fields", {})
        for magic_string, label in field_name_mapping.items():
            old_value = custom_fields_old.get(magic_string)
            new_value = custom_fields_new.get(magic_string)

            if self._normalize_value(new_value) != self._normalize_value(
                old_value
            ):
                changed_fields.append(
                    CustomFieldDefinition(
                        magic_string=magic_string,
                        label=label or magic_string,
                    )
                )

        return changed_fields

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."
        self.subject = "Object '{}' van objecttype '{}' is aangepast."
        self.variables = [
            "custom_object_name",
            "custom_object_type",
        ]

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
            "custom_object_version": custom_object_info.version,
        }
        if entity_data.get("status") and (
            entity_data.get("status") != entity_data.get("status_old")
        ):
            event_params["custom_object_status"] = (
                "actief"
                if entity_data.get("status") == "active"
                else "inactief"
            )
            self.subject = "De status van object '{}' van objecttype '{}' is aangepast naar {}."
            self.variables.append("custom_object_status")

        changed_fields = self._summarize_custom_field_changes(
            custom_object_info,
            entity_data,
        )

        if changed_fields:
            event_params["changed_fields"] = changed_fields

            summary = ", ".join(
                [field["label"] for field in event_params["changed_fields"]]
            )
            self.subject = f"{self.subject}\n\nAangepaste kenmerken: {summary}"

        return event_params


class CustomObjectRelatedTo(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Object '{}' van objecttype '{}' is gerelateerd aan {}.{}"
        )
        self.variables = [
            "custom_object_name",
            "custom_object_type",
            "custom_object_related_items",
            "custom_object_values_copied",
        ]
        self.event_type = "custom_object/relatedTo"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."
        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
            "custom_object_values_copied": "",
        }

        related_items: list[str] = []
        related_uuids: list[str] = []

        if custom_objects := entity_data.get("custom_objects"):
            old_custom_objects = {
                co["uuid"] for co in entity_data.get("custom_objects_old", [])
            }

            for custom_object in custom_objects:
                if custom_object["uuid"] in old_custom_objects:
                    continue

                custom_object_info = get_custom_object(
                    session=session, uuid=custom_object["uuid"]
                )
                related_items.append(f"object '{custom_object_info.title}'")
                related_uuids.append(custom_object["uuid"])

        if cases := entity_data.get("cases"):
            old_cases = {
                case["uuid"] for case in entity_data.get("cases_old", [])
            }

            for case in cases:
                if case["uuid"] in old_cases:
                    continue
                if case.get("copy_values_to_case", False):
                    event_params["custom_object_values_copied"] = (
                        " Kenmerken uit het object zijn gekopieerd naar de zaak."
                    )

                case_info = get_case(session=session, uuid=case["uuid"])
                related_items.append(f"zaak {case_info.id}")
                related_uuids.append(case["uuid"])
                event_params["case_id"] = case_info.id

        event_params["custom_object_related_items"] = ", ".join(related_items)
        event_params["related_uuids"] = related_uuids

        return event_params


class CustomObjectUnrelatedFrom(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = "Relatie verwijderd vanuit object '{}' van objecttype '{}' naar {}."
        self.variables = [
            "custom_object_name",
            "custom_object_type",
            "custom_object_related_items",
        ]
        self.event_type = "custom_object/unrelatedFrom"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }
        related_items: list[str] = []
        related_uuids: list[str] = []

        if custom_objects := entity_data.get("custom_objects_old"):
            new_custom_objects = {
                co["uuid"] for co in entity_data.get("custom_objects", [])
            }

            for custom_object in custom_objects:
                if custom_object["uuid"] in new_custom_objects:
                    continue

                custom_object_info = get_custom_object(
                    session=session, uuid=custom_object["uuid"]
                )
                related_items.append(f"object '{custom_object_info.title}'")
                related_uuids.append(custom_object["uuid"])

        if cases := entity_data.get("cases_old"):
            new_cases = {case["uuid"] for case in entity_data.get("cases", [])}

            for case in cases:
                if case["uuid"] in new_cases:
                    continue

                case_info = get_case(session=session, uuid=case["uuid"])
                related_items.append(f"zaak {case_info.id}")
                related_uuids.append(case["uuid"])

        event_params["custom_object_related_items"] = ", ".join(related_items)
        event_params["related_uuids"] = related_uuids

        return event_params


class CustomObjectDeleted(CustomObjectBase):
    def __init__(self):
        super().__init__()
        self.subject = (
            "Object '{}' verwijderd door '{}'. Reden van verwijderen: '{}'"
        )
        self.variables = ["custom_object_name", "username", "delete_reason"]
        self.event_type = "custom_object/deleted"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        event_params = {
            "custom_object_name": custom_object_info.title,
            "username": user_info.display_name,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
            "delete_reason": entity_data.get(
                "delete_reason", "Geen reden opgegeven"
            ),
        }

        return event_params
