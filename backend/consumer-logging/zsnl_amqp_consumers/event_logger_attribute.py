# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import AttributeInformation, get_attribute, get_user
from zsnl_domains.database.schema import Logging


class AttributeBase(BaseLogger):
    def __init__(self):
        pass

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """
        attribute_info = get_attribute(
            session=session, uuid=event["entity_id"]
        )
        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)
        commit_message = event.get("entity_data").get("commit_message")

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            attribute_info=attribute_info,
            commit_message=commit_message,
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="kenmerk",
            component_id=event_parameters["attribute_id"],
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        attribute_info: AttributeInformation,
        commit_message: str,
    ):
        raise NotImplementedError


class AttributeEdited(AttributeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Kenmerk '{}' opgeslagen ({})"
        self.variables = ["magic_string", "reason"]
        self.event_type = "attribute/update"

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """

        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)

        if event_parameters.get("options"):
            # sort options in the order of sort_order
            options = sorted(
                event_parameters["options"], key=lambda i: i["sort_order"]
            )
            values_list = [
                option["value"]
                if option["active"]
                else option["value"] + "(inactief)"
                for option in options
            ]
            log_subject += ", Opties: " + (", ".join(values_list))

        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        attribute_info: AttributeInformation,
        commit_message: str,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        options = entity_data.get("attribute_values", [])

        event_parameters = {
            "attribute_id": attribute_info.id,
            "magic_string": attribute_info.magic_string,
            "reason": commit_message,
            "options": options,
        }

        return event_parameters


class AttributeCreated(AttributeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Kenmerk '{}' toegevoegd: {}"
        self.variables = ["magic_string", "reason"]
        self.event_type = "attribute/create"

    def generate_event_parameters(
        self,
        entity_data: dict,
        attribute_info: AttributeInformation,
        commit_message: str,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """

        options = entity_data.get("attribute_values", [])

        event_parameters = {
            "attribute_id": attribute_info.id,
            "magic_string": attribute_info.magic_string,
            "reason": commit_message,
            "options": options,
        }

        return event_parameters


class AttributeDeleted(AttributeBase):
    def __init__(self):
        super().__init__()
        self.subject = "Kenmerk {} (magic string {}) verwijderd: {}"
        self.variables = ["attribute_name", "magic_string", "reason"]
        self.event_type = "attribute/remove"

    def generate_event_parameters(
        self,
        entity_data: dict,
        attribute_info: AttributeInformation,
        commit_message: str,
    ):
        """Generate event parameters from entity data.

        :param entity_data: entity data
        :type entity_data: dict
        :return: event parameters
        :rtype: dict
        """
        event_parameters = {
            "attribute_id": attribute_info.id,
            "attribute_name": attribute_info.name,
            "magic_string": attribute_info.magic_string,
            "reason": entity_data["commit_message"],
        }

        return event_parameters
