# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import get_case, get_user
from zsnl_domains.database.schema import Logging


class CaseRelationBase(BaseLogger):
    def __init__(self):
        self.component: str
        self.event_type: str
        self.subject: str
        self.variables: list[str]
        self.event_type: str

    def __call__(self, session, event) -> list[Logging]:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        formatted_entity_data = self.format_entity_data(event=event)

        user_info = get_user(session=session, uuid=event["user_uuid"])

        (event_parameters_a, event_parameters_b) = (
            self.generate_event_parameters(
                session=session,
                entity_data=formatted_entity_data,
            )
        )
        event_parameters_a["correlation_id"] = event["correlation_id"]
        event_parameters_b["correlation_id"] = event["correlation_id"]

        restricted_a = bool(
            event_parameters_a.pop("confidentiality") != "public"
        )
        restricted_b = bool(
            event_parameters_b.pop("confidentiality") != "public"
        )

        return [
            self._create_logging_record(
                user_info=user_info,
                event_type=self.event_type,
                subject=self._generate_subject(event_parameters_a),
                component=self.component,
                component_id=None,
                case_id=event_parameters_b["linked_case_id"],
                created_date=event["created_date"],
                event_data=event_parameters_a,
                restricted=restricted_a,
            ),
            self._create_logging_record(
                user_info=user_info,
                event_type=self.event_type,
                subject=self._generate_subject(event_parameters_b),
                component=self.component,
                component_id=None,
                case_id=event_parameters_a["linked_case_id"],
                created_date=event["created_date"],
                event_data=event_parameters_b,
                restricted=restricted_b,
            ),
        ]

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
    ):
        raise NotImplementedError


class CaseRelationDeleted(CaseRelationBase):
    def __init__(self):
        super().__init__()
        self.component = "zaak"
        self.subject = "Relatie met zaak {} ({}) verwijderd"
        self.variables = ["linked_case_id", "linked_case_type_name"]
        self.event_type = "case/relation/remove"

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
    ):
        case_uuid_a = entity_data["object1_uuid"]
        case_info_a = get_case(session=session, uuid=case_uuid_a)

        case_uuid_b = entity_data["object2_uuid"]
        case_info_b = get_case(session=session, uuid=case_uuid_b)

        event_parameters_a = {
            "linked_case_id": case_info_b.id,
            "linked_case_type_name": case_info_b.casetype_name,
            # This is the relation in the direction "from a to b", which is
            # going to be logged at the confidentiality level of "a"
            "confidentiality": case_info_a.confidentiality,
        }
        event_parameters_b = {
            "linked_case_id": case_info_a.id,
            "linked_case_type_name": case_info_a.casetype_name,
            # This is the relation in the direction "from b to a", which is
            # going to be logged at the confidentiality level of "b"
            "confidentiality": case_info_b.confidentiality,
        }

        return [event_parameters_a, event_parameters_b]


class CaseRelationCreated(CaseRelationBase):
    def __init__(self):
        super().__init__()
        self.component = "zaak"
        self.event_type = "case/update/relation"
        self.subject = "Zaak {} ({}) gerelateerd"
        self.variables = ["linked_case_id", "linked_case_type_name"]

    def generate_event_parameters(
        self,
        session,
        entity_data: dict,
    ):
        case_uuid_a = entity_data["object1_uuid"]
        case_info_a = get_case(session=session, uuid=case_uuid_a)

        case_uuid_b = entity_data["object2_uuid"]
        case_info_b = get_case(session=session, uuid=case_uuid_b)

        event_parameters_a = {
            "linked_case_id": case_info_b.id,
            "linked_case_type_name": case_info_b.casetype_name,
            # This is the relation in the direction "from a to b", which is
            # going to be logged at the confidentiality level of "a"
            "confidentiality": case_info_a.confidentiality,
        }
        event_parameters_b = {
            "linked_case_id": case_info_a.id,
            "linked_case_type_name": case_info_a.casetype_name,
            # This is the relation in the direction "from b to a", which is
            # going to be logged at the confidentiality level of "b"
            "confidentiality": case_info_b.confidentiality,
        }
        return [event_parameters_a, event_parameters_b]
