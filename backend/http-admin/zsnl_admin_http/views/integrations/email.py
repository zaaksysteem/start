# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import pyramid.request
import pyramid.response
from minty_pyramid.session_manager import protected_route
from zsnl_domains.admin.integrations import entities


@protected_route("admin")
def start_oauth2_flow(
    request: pyramid.request.Request, user_info: minty.cqrs.UserInfo
):
    """
    Start an OAuth2 flow with an external provider

    Redirects the user to the OAuth2 provider's login page, and requests the
    appropriate scopes for use with the selected integration.
    """

    integration_uuid = request.params.get("integration_uuid")
    qry = request.get_query_instance(
        domain="zsnl_domains.admin.integrations",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    integration: entities.EmailIntegration = qry.get_email_integration(
        integration_uuid=integration_uuid
    )

    login_data = integration.generate_login_url(
        redirect_uri=request.route_url("finish_oauth2_flow", _scheme="https"),
    )

    # Save the code_verifier and state in the integration config, so they can
    # be used later in the login process.
    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.integrations",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    cmd.save_email_integration_login_state(
        integration_uuid=integration_uuid,
        code_verifier=login_data.code_verifier,
        state=login_data.state,
    )

    return pyramid.response.Response(
        status_int=302, location=login_data.login_url
    )


@protected_route("admin")
def finish_oauth2_flow(
    request: pyramid.request.Request, user_info: minty.cqrs.UserInfo
):
    """
    Finish an OAuth2 flow by accepting an access code and retrieving a refresh token

    Accepts the access code from the OAuth2 provider and exchanges it for
    a short-lived authorization token and a long-lived refresh token, which
    is then stored in the integration configuration.
    """

    cmd = request.get_command_instance(
        domain="zsnl_domains.admin.integrations",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    if error := request.params.get("error"):
        description = request.params.get("error_description")
        return f"Error: {error}: {description}"

    code = request.params.get("code")
    state = request.params.get("state")

    cmd.exchange_code_for_refresh_token(
        redirect_uri=request.route_url("finish_oauth2_flow", _scheme="https"),
        state=state,
        code=code,
    )

    return "Authorisatie geslaagd. Dit venster mag gesloten worden."
