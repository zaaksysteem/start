# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import serializers
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from pyramid.request import Request


@protected_route("beheer_zaaktype_admin")
def search_attribute_by_name(request: Request, user_info):
    """Search attribute by name and filter by type.

    :param request: request
    :type request: Request
    :param user_info: user info
    :type user_info: User Info
    :raises HTTPBadRequest: When params are not complete
    :return: list of attributes
    :rtype: JSON
    """
    attribute_type = request.params.get("type", None)
    if attribute_type == "":
        attribute_type = None

    try:
        search_string = request.params["search_string"]
    except KeyError as e:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": f"Required parameter not specified: '{e}'"}
                ]
            }
        ) from e
    qry = request.get_query_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    res = qry.search_attribute_details_by_name(
        search_string=search_string, attribute_type=attribute_type
    )
    return {
        "data": serializers.attribute_search_results(res),
        "links": {"self": f"{request.current_route_path()}"},
    }


@protected_route("beheer_zaaktype_admin")
def delete_attribute(request: Request, user_info):
    try:
        attribute_uuid = request.json_body["attribute_uuid"]
        reason = request.json_body["reason"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid": attribute_uuid, "reason": reason}

    cmd = request.get_command_instance(
        "zsnl_domains.admin.catalog", user_info.user_uuid
    )
    cmd.delete_attribute(**command_params)

    return {"data": {"success": True}}
