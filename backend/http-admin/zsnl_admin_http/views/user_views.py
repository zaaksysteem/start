# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class UserViews(JSONAPIEntityView):
    view_mapper = {
        "POST": {
            "rename_user": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.admin.users",
                "run": "rename_user",
                "from": {"json": {"_all": True}},
                "entity_type": "transaction",
                "command_primary": "integration_uuid",
            }
        },
    }
