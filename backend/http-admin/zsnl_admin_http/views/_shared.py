# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


def create_link_from_entity(
    self: JSONAPIEntityView, entity=None, entity_type=None, entity_id=None
) -> str | None:
    if entity and entity.entity_type == "versioned_casetype":
        return self.request.current_route_path()

    return None
