# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from .test_zsnl_admin_http import MockRequest
from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden
from unittest import mock
from uuid import uuid4
from zsnl_admin_http.views import case_type_views


class TestCaseTypeViews:
    def test_case_type_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )
        views = [case_type_views.delete_case_type]
        for view in views:
            with pytest.raises(HTTPForbidden):
                view(request)

    def test_delete_case_type(self):
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "case_type_uuid": str(uuid4()),
            "reason": "reason for delete",
        }
        res = case_type_views.delete_case_type(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = case_type_views.delete_case_type(request)
