# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from unittest import mock
from uuid import UUID, uuid4
from zsnl_admin_http.views import versioned_casetype_views


def _create_mock_entity(entity_type: str, entity_id: UUID):
    entity = mock.MagicMock()

    entity.result_type = None
    entity.type = None

    type(entity).entity_type = mock.PropertyMock(return_value=entity_type)
    type(entity).entity_id = mock.PropertyMock(return_value=entity_id)

    return entity


class TestViewShared(unittest.TestCase):
    def test_create_link_from_entity_unknown(self):
        request = mock.Mock(name="mock_request")
        entity_id = uuid4()

        entity = _create_mock_entity("something_unknown", entity_id)

        view = versioned_casetype_views.VersionedCaseTypeViews(
            context="test", request=request
        )

        view.create_link_from_entity(entity)

        request.current_route_path.assert_not_called()

    def test_create_link_from_entity_known(self):
        request = mock.Mock(name="mock_request")
        entity_id = uuid4()

        entity = _create_mock_entity("versioned_casetype", entity_id)

        view = versioned_casetype_views.VersionedCaseTypeViews(
            context="test", request=request
        )

        view.create_link_from_entity(entity)

        request.current_route_path.assert_called_once_with()
