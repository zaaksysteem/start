package Syzygy::Interface::ObjectReference;
our $VERSION = '0.006';
use Moose::Role;

=head1 NAME

Syzygy::Interface::ObjectReference - Interface role for object references

=head1 DESCRIPTION

This role specifies the interface requires for basic Syzygy object references

=cut

use BTTW::Tools;
use Syzygy::Types qw[Name UUID];

=head1 INTERFACE

=head2 id

Requires a L<UUID|Syzygy::Types/UUID> for the referenced object.

=head2 preview

Maybe requires a C<Str> for the referenced object.

=head2 type_name

Requires a L<name|Syzygy::Types/Name> for the referenced object's type.

=cut

requires qw[
    id
    preview
    type_name
];

#sig does not support MooseX::Types: sig id => '=> UUID';
around id => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_id = $self->$orig(@_);

    UUID->assert_valid($maybe_id);

    return $maybe_id;
};

sig preview => '=> Maybe[Str]';

#sig does not support MooseX::Types: sig type_name => '=> Name';
around type_name => sub {
    my $orig = shift;
    my $self = shift;

    my $maybe_type_name = $self->$orig(@_);

    Name->assert_valid($maybe_type_name);

    return $maybe_type_name;
};

=head1 PRIVATE METHODS

=head2 _ref

Internal reference instance builder.

=cut

sub _ref {
    my $self = shift;

    return $self->new(
        id => $self->id,
        preview => $self->preview,
        type_name => $self->type_name
    );
}

=head2 _as_string

Stringified reference representation.

For a reference with type name C<object> and id
C<b5281da2-9036-4990-9783-1d0e05ead07f> returns C<object(...ead07f)>.

=cut

sub _as_string {
    my $self = shift;

    return sprintf('%s(...%s)', $self->type_name, substr($self->id, -6));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
