package TestFor::General::Tools::Text;

use Test::More;

use BTTW::Tools::Text;
use BTTW::Tools::Text::PDFHack;
use BTTW::Tools::Text::OpenDocument;
use BTTW::Tools::Text::OOXML;

use FindBin qw($Bin);

{
    my $en = BTTW::Tools::Text->new(
        source => 'Here I am, building some English text. I sure hope the new library can identify me.'
    );

    is $en->language, 'en', 'English correctly detected';

    # Base tests
    ok grep { $_ eq 'english' } $en->stemmed_words;
    ok grep { $_ eq 'text' } $en->stemmed_words;
    ok grep { $_ eq 'identifi' } $en->stemmed_words;
    ok grep { $_ eq 'librari' } $en->stemmed_words;

    # Invert test, verify stoplist was used
    ok not grep { $_ eq 'i' } $en->stemmed_words;
    ok not grep { $_ eq 'am' } $en->stemmed_words;
}

{
    my $nl = BTTW::Tools::Text->new(
        source => 'Dit is een stukje tekst met hopelijk genoeg informatie om herkend te kunnen worden als Nederlands. Gebrabbel.'
    );

    is $nl->language, 'nl', 'Dutch correctly detected';

    # Base tests
    ok grep { $_ eq 'nederland' } $nl->stemmed_words;
    ok grep { $_ eq 'tekst' } $nl->stemmed_words;
    ok grep { $_ eq 'stukj' } $nl->stemmed_words;
    ok grep { $_ eq 'herkend' } $nl->stemmed_words;

    # Invert test, verify stoplist was used
    ok not grep { $_ eq 'is' } $nl->stemmed_words;
    ok not grep { $_ eq 'met' } $nl->stemmed_words;
}

{
    my $pdf_text = BTTW::Tools::Text->new(
        source => BTTW::Tools::Text::PDFHack->new(
            file => "$Bin/../t/data/openoffice_document.pdf"
        )
    );

    is $pdf_text->language, 'en', 'English correctly detected from PDF content';

    ok grep { $_ eq 'free' } $pdf_text->stemmed_words;
    ok grep { $_ eq 'manipul' } $pdf_text->stemmed_words;

    ok not grep { $_ eq 'is' } $pdf_text->stemmed_words;
}

{
    my $odt_text = BTTW::Tools::Text->new(
        source => BTTW::Tools::Text::OpenDocument->new(
            file => "$Bin/../t/data/openoffice_document.odt"
        )
    );

    is $odt_text->language, 'en', 'English correctly detected from ODT content';

    ok grep { $_ eq 'break' } $odt_text->stemmed_words;
    ok grep { $_ eq 'zaaksysteem' } $odt_text->stemmed_words;

    ok not grep { $_ eq 'is' } $odt_text->stemmed_words;
}

{
    my $docx_text = BTTW::Tools::Text->new(
        source => BTTW::Tools::Text::OOXML->new(
            file => "$Bin/../t/data/word.docx"
        )
    );

    is $docx_text->language, 'en', 'English correctly detected from DOCX content';

    ok grep { $_ eq 'break' } $docx_text->stemmed_words;
    ok grep { $_ eq 'zaaksysteem' } $docx_text->stemmed_words;

    ok not grep { $_ eq 'is' } $docx_text->stemmed_words;
}

done_testing;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
