package Syzygy::Test::Object::ValueTypes::Serial;

use Test::Class::Moose;

use Syzygy::Object::ValueTypes::Serial;

sub test_object_value_type_serial {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Serial->new }
        'serial value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Serial',
        'serial value type constructor retval';

    is $type->name, 'serial', 'serial value type has expected name';

    my $a = $type->new_value(123);
    my $b = $type->new_value("456");

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok !$type->equal($a, $b), 'a == b: negative equality';
    ok $type->not_equal($a, $b), 'a != b: positive inequality';
    ok !$type->not_equal($a, $a), 'a != a: negative inequality';

    is $type->deflate_value($a), 123,
        'type value deflation returns number';

    is $type->deflate_value($b), 456,
        'type value deflation returns number for string input';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
