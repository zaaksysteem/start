use Test::More;

use BTTW::Tools::File qw(fix_file_extension_for_fh);
use Sub::Override;

my $fh       = "somepath";
my $filename = "Foo.txt";


my $info = {
    mime_type => 'foo',
    DANS      => {
        archivable         => 1,
        allowed_extensions => [qw(.txt .crt)],
    }
};

my $override = Sub::Override->new(
    'File::ArchivableFormats::identify_from_path' => sub {
        return $info;
    }
);


is(
    fix_file_extension_for_fh($fh, "Foo.txt"),
    "Foo.txt",
    "Correct file ext, nothing done"
);
is(
    fix_file_extension_for_fh($fh, "Foo.crt"),
    "Foo.crt",
    ".. also for the alternative extension"
);

is(
    fix_file_extension_for_fh($fh, "Foo"),
    "Foo.txt",
    ".. als for the no extension given"
);

$info->{DANS}{preferred_extension} = '.foo';
is(
    fix_file_extension_for_fh($fh, "Foo"),
    "Foo.foo",
    ".. and sets the preferred extension"
);

$info = {
    mime_type => 'application/vnd.ms-outlook',
    DANS      => {
        archivable         => 0,
        allowed_extensions => [],
    }
};


is(
    fix_file_extension_for_fh($fh, "Foo.msg"),
    "Foo.msg",
    "Correct filename for msg",
);


is(
    fix_file_extension_for_fh($fh, "Foo"),
    "Foo.msg",
    ".. also without extension"
);


done_testing;
