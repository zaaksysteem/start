# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from unittest import mock
from uuid import uuid4
from zsnl_jobs_consumer.handlers import (
    JOBS_ROUTING_KEY_PREFIX,
    JobCreatedHandler,
    JobProcessedHandler,
)


def test_created_handler():
    cqrs = mock.Mock()
    user_info = minty.cqrs.UserInfo(user_uuid=uuid4(), permissions={})
    event = mock.Mock(
        entity_id=uuid4(),
        correlation_id=uuid4(),
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    handler = JobCreatedHandler(cqrs=cqrs)
    assert handler.routing_keys == [JOBS_ROUTING_KEY_PREFIX + ".JobCreated"]

    handler.handle(event)

    cqrs.get_command_instance.assert_called_once_with(
        event.correlation_id,
        "zsnl_domains.jobs",
        event.context,
        event.user_uuid,
        event.user_info,
    )
    cqrs.get_command_instance().prepare_job.assert_called_once_with(
        job_uuid=event.entity_id
    )


def test_processed_handler():
    cqrs = mock.Mock()
    user_info = minty.cqrs.UserInfo(user_uuid=uuid4(), permissions={})
    event = mock.Mock(
        entity_id=uuid4(),
        correlation_id=uuid4(),
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    handler = JobProcessedHandler(cqrs=cqrs)
    assert handler.routing_keys == [JOBS_ROUTING_KEY_PREFIX + ".JobProcessed"]

    handler.handle(event)

    cqrs.get_command_instance.assert_called_once_with(
        event.correlation_id,
        "zsnl_domains.jobs",
        event.context,
        event.user_uuid,
        event.user_info,
    )
    cqrs.get_command_instance().finish_job.assert_called_once_with(
        job_uuid=event.entity_id
    )
