# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty_infra_misc as mim
from unittest import mock


def test_redis_infrastructure():
    redis_infra = mim.RedisInfrastructure()

    assert redis_infra.name == "session"

    with mock.patch("minty_infra_misc.Redis") as mock_redis:
        redis_infra({"redis": {"session": {"redis_config": "goes here"}}})

    mock_redis.assert_called_once_with(redis_config="goes here")
