# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import base64
from minty.cqrs import UserInfo
from minty_pyramid.session_manager import protected_route
from pyramid.exceptions import HTTPBadRequest
from pyramid.request import Request
from pyramid.response import Response
from uuid import uuid4


@protected_route("admin")
def upload_template(request: Request, user_info: UserInfo):
    try:
        file = request.POST["file"].file
        tenant = request.POST.get("tenant", None)
    except (KeyError, AttributeError) as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error
    file_name = request.POST["file"].filename

    cmd_instance = request.get_command_instance(
        "zsnl_domains.configuration", user_info.user_uuid, user_info
    )
    cmd_instance.upload_template(file=file, tenant=tenant, file_name=file_name)

    return {"data": {"success": True}}


def get_content(request: Request):
    try:
        name = request.params["name"]
        tenant = request.params.get("tenant", None)
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.configuration", user_uuid=uuid4()
    )

    style_info = query_instance.get_content(name=name, tenant=tenant)
    decoded_content = base64.b64decode(style_info.content)

    file_name = f"{style_info.name}.{style_info.extension}"
    content_disposition = "inline"
    if style_info.extension == "zss":
        content_disposition = "attachment"

    headers = {
        "Content-Disposition": f"{content_disposition}; filename={file_name}",
        "Content-Type": style_info.mimetype,
    }

    response = Response(
        body=decoded_content,
        content_type=style_info.mimetype,
        charset="utf-8",
        headers=headers,
    )
    return response
