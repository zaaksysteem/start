{
    "openapi": "3.0.3",
    "servers": [
        {
            "url": "/"
        }
    ],
    "info": {
        "title": "Style configuration management API documentation",
        "description": "Retrieve styleJSON data associated with various kinds of entities in Zaaksysteem",
        "version": "v2",
        "x-package-name": "zsnl_style_http",
        "x-url-base": [
            "/api/v2/style/"
        ],
        "contact": {
            "name": "Zaaksysteem Development"
        }
    },
    "tags": [
        {
            "name": "StyleConfiguration"
        }
    ],
    "paths": {
        "/api/v2/style/upload_template": {
            "post": {
                "x-view": "views",
                "tags": [
                    "StyleConfiguration"
                ],
                "operationId": "upload_template",
                "summary": "Create a file record using an uploaded file.",
                "description": "Create a file record using an uploaded file.",
                "requestBody": {
                    "required": true,
                    "content": {
                        "multipart/form-data": {
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "file": {
                                        "type": "string",
                                        "format": "binary"
                                    },
                                    "tenant": {
                                        "type": "string"
                                    }
                                },
                                "required": [
                                    "file"
                                ]
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "description": "File created",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DefaultSuccess"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseError"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Permission denied",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseError"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "The current state does not match command expectations",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseError"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/api/v2/style/tenants": {
            "get": {
                "x-view": "style_config",
                "x-view-class": "StyleConfigViews",
                "tags": [
                    "StyleConfiguration"
                ],
                "operationId": "get_tenants",
                "summary": "Retrieve list of tenants for current instance",
                "description": "Retrieve list of tenants for current instance",
                "responses": {
                    "200": {
                        "description": "StyleConfiguration",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "allOf": [
                                        {
                                            "$ref": "#/components/schemas/DefaultSuccessMultiple"
                                        },
                                        {
                                            "$ref": "./generated_entities.json#/components/schemas/EntityTenantList"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseError"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/api/v2/style/get_content": {
            "get": {
                "x-view": "style_config",
                "x-view-class": "StyleConfigViews",
                "tags": [
                    "StyleConfiguration"
                ],
                "operationId": "get_content",
                "summary": "Retrieve file contents for required style configuration",
                "description": "Retrieve style configuration",
                "parameters": [
                    {
                        "name": "name",
                        "in": "query",
                        "description": "Name of the zss style config file(without extension) or one of the available values(name of the file within archive)",
                        "required": true,
                        "style": "form",
                        "schema": {
                            "type": "string",
                            "enum": [
                                "config",
                                "logo-pip",
                                "logo-login",
                                "favicon",
                                "stylesheet"
                            ]
                        }
                    },
                    {
                        "name": "tenant",
                        "in": "query",
                        "description": "Name of the tenant to get style for",
                        "required": false,
                        "style": "form",
                        "schema": {
                            "type": "string",
                            "example": "dev.zaaksysteem.nl"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "StyleConfiguration",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "allOf": [
                                        {
                                            "$ref": "#/components/schemas/DefaultSuccess"
                                        },
                                        {
                                            "$ref": "./generated_entities.json#/components/schemas/EntityStyleConfiguration"
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad request",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseError"
                                }
                            }
                        }
                    },
                    "304": {
                        "description": "Not Modified",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ResponseNotModified"
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "DefaultSuccess": {
                "type": "object",
                "required": [
                    "meta"
                ],
                "properties": {
                    "meta": {
                        "type": "object",
                        "required": [
                            "api_version"
                        ],
                        "properties": {
                            "api_version": {
                                "type": "integer",
                                "example": 2
                            }
                        }
                    }
                }
            },
            "DefaultSuccessMultiple": {
                "allOf": [
                    {
                        "$ref": "#/components/schemas/DefaultSuccess"
                    },
                    {
                        "type": "object",
                        "properties": {
                            "links": {
                                "type": "object",
                                "properties": {
                                    "self": {
                                        "type": "string",
                                        "example": "http://example.zs.nl/?page=2"
                                    },
                                    "prev": {
                                        "type": "string",
                                        "example": "http://example.zs.nl/?page=1"
                                    },
                                    "next": {
                                        "type": "string",
                                        "example": "http://example.zs.nl/?page=3"
                                    }
                                }
                            }
                        }
                    }
                ]
            },
            "ResponseError": {
                "required": [
                    "errors"
                ],
                "properties": {
                    "errors": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "title": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                }
            },
            "ResponseNotModified": {
                "required": [
                    "errors"
                ],
                "properties": {
                    "errors": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "title": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}