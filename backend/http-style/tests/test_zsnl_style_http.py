# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import importlib
import io
import pytest
from minty.cqrs import UserInfo
from pyramid.httpexceptions import HTTPBadRequest
from unittest import TestCase, mock
from webob import multidict
from zsnl_style_http.__init__ import log_record_factory
from zsnl_style_http.views.style_config import StyleConfigViews

user_info = UserInfo(user_uuid="subject_uuid", permissions="permissions")


def mock_protected_route(permission, *args):
    def view_wrapper(view):
        def request_wrapper(request, user_info):
            return view(request=request, user_info=user_info)

        return request_wrapper

    return view_wrapper


class TestStyleConfiguration(TestCase):
    """
    Class containing main tests for the ZSNL HTTP style Service
    """

    def test_StyleConfiguration(self):
        """
        Tests for the presence and accuracy of content within the StyleConfig
        View Class.
        """
        view = StyleConfigViews(mock.MagicMock(), mock.MagicMock())

        # Correct instance
        self.assertIsInstance(view, StyleConfigViews)

        # Correct class-variables
        self.assertTrue(hasattr(view, "create_link_from_entity"))
        self.assertTrue(hasattr(view, "view_mapper"))
        self.assertIsNone(view.create_link_from_entity(mock.MagicMock()))

    def test_log_record_factory(self):
        """
        Testing the log record factory
        """
        record = log_record_factory(
            name="foo",
            level="WARN",
            pathname="/dev/null",
            lineno=42,
            args={},
            msg="bar",
            exc_info=None,
        )

        assert record.msg == "bar"
        assert record.zs_component == "zsnl_style_http"

    def test_get_content(self):
        response = mock.MagicMock()
        response.configure_mock(status=200, headers={})

        event = mock.MagicMock()
        event.configure_mock(response=response)


class TestDocument:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            from zsnl_style_http.views import views

            importlib.reload(views)
            self.views = views

    def test_upload_template(self):
        request = mock.MagicMock()
        request.POST = {"file": None}

        with pytest.raises(HTTPBadRequest):
            self.views.upload_template(request, user_info=user_info)

        request = mock.MagicMock()

        req_post = mock.MagicMock()
        req_post.POST = multidict.MultiDict()
        req_post.POST["document_file"] = mock.Mock()
        req_post.POST["document_file"].filename = "test_file.zss.zip"
        req_post.POST["document_file"].file = io.BytesIO(b"test_content")
        mock_command_instance = mock.MagicMock()
        mock_command_instance.upload_template = mock.MagicMock()
        mock_command_instance.upload_template().return_value = True
        self.views.upload_template(request, user_info=user_info)
        mock_command_instance.upload_template.assert_called()

    def test_get_content(self):
        request = mock.MagicMock()
        request.params = {"file": None}

        with pytest.raises(HTTPBadRequest):
            self.views.get_content(request)

        request = mock.MagicMock()

        file_content = b"test_content"
        file_content_encoded = base64.b64encode(file_content).decode("utf-8")

        req_post = mock.MagicMock()
        req_post.POST = multidict.MultiDict()
        req_post.POST["document_file"] = mock.Mock()
        req_post.POST["document_file"].filename = "test_file.zss"
        req_post.POST["document_file"].file = file_content
        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_style_info = mock.MagicMock()
        mock_style_info.configure_mock(
            name="test_file",
            extension="zss",
            content=file_content_encoded,
        )
        mock_query.get_content.return_value = mock_style_info
        self.views.get_content(request)
        mock_query.get_content.assert_called()
