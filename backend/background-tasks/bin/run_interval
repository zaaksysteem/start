#! /bin/bash

function usage() {
    echo "Usage: $0 [program] -s redis|file -c redis_hostname|config_path] [-p parallel_threads] [-i interval_minutes]"
    echo ""
    echo "This script runs [parallel] copies of [program] in parallel."
    echo "The program will be run with the platform key as its first argument, and"
    echo "the hostname of a Zaaksysteem instance as its second argument."
    echo ""
    echo "After the program has run for all instances, the script will sleep until"
    echo "the next [interval]th minute, and do it all again."
    exit 1
}

PROGRAM=${1}
PROGRAMSLUG=$(basename ${1})
STATSD_HOST="${TELEGRAF_HOST:-telegraf.mintlab-system.svc.cluster.local}"
STATSD_PORT="${TELEGRAF_PORT:-8125}"


trap 'echo $(date +"%Y-%m-%d %H:%M:%S") Error ${LINENO} for $HOSTNAME && echo "${PROGRAMSLUG}.run.fail:1|c" | nc -w 1 -u ${TELEGRAF_HOST} ${TELEGRAF_PORT}' ERR

if [ -z "${PROGRAM}" ]; then
    usage
fi

shift

echo "$@"

while getopts ":c:i:p:s:" o; do
    case "${o}" in
        i)
            INTERVAL=${OPTARG}
            ;;
        p)
            PARALLEL=${OPTARG}
            ;;
        s)
            HOSTS_SOURCE=${OPTARG}
            ;;
        c)
            CONFIG_DATA=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ -z "${HOSTS_SOURCE}" ] || [ -z "${CONFIG_DATA}" ]; then
    usage
fi

. get_instances_${HOSTS_SOURCE}.sh

PLATFORM_KEY=$(grep zs_platform_key /etc/zaaksysteem/zaaksysteem.conf | sed -e 's/.*=[[:space:]]*"*\([^"]*\)"*$/\1/')

if [ -z "${PLATFORM_KEY}" ]; then
    echo "No zs_platform_key found in /etc/zaaksysteem/zaaksysteem.conf!"
    exit 1
fi

PARALLEL=${PARALLEL:-5};
INTERVAL=${INTERVAL:-15};
INTERVAL=$((INTERVAL * 60));

echo $(date +"%Y-%m-%d %H:%M:%S") "Startup: Running run_instances every $INTERVAL seconds";

while true; do
    SECONDS=0
    # Re-retrieve the list of hosts before every run, so new instances get picked up.
    get_hosts "${CONFIG_DATA}"

    parallel --lb -j ${PARALLEL} "${PROGRAM}" "${PLATFORM_KEY}" {} ::: ${hosts}

    SLEEP_TIME=$((INTERVAL - $(date +%s) % INTERVAL))
    echo $(date +"%Y-%m-%d %H:%M:%S") "Sleeping ${SLEEP_TIME} seconds until the next run."

    duration=$SECONDS
    echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
    echo "${PROGRAMSLUG}.run.time:${duration}|ms" | nc -w 1 -u "${TELEGRAF_HOST}" "${TELEGRAF_PORT}"
    echo "${PROGRAMSLUG}.run.ok:1|c" | nc -w 1 -u "${TELEGRAF_HOST}" "${TELEGRAF_PORT}"

    # Sleep until the next interval
    sleep ${SLEEP_TIME}
done
