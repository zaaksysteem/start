function get_hosts {
    local CONFIG_PATH=${1:-/etc/zaaksysteem/customer.d}

	hosts=$(
        awk 'FNR==1{print ""}1' "${CONFIG_PATH}"/*.conf \
            | grep -e '^<[^/]' \
            | sed -e 's/[<>]//g' \
            | sort \
            | uniq
    );
}
