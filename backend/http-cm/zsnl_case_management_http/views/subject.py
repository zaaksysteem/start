# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class SubjectViews(JSONAPIEntityView):
    # Defined in _shared, so not every view class has identical code to generate
    # links.
    create_link_from_entity = create_link_from_entity

    # Move this to openapi.json
    view_mapper = {
        "GET": {
            "get_related_objects_for_subject": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_related_custom_objects_for_subject",
                "from": {"request_params": {"subject_uuid": "subject_uuid"}},
            },
            "get_related_subjects_for_subject": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_related_subjects_for_subject",
                "from": {"request_params": {"subject_uuid": "subject_uuid"}},
            },
            "get_contact_related_cases": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_contact_related_cases",
                "from": {
                    "request_params": {
                        "contact_uuid": "contact_uuid",
                        "filter[attributes.status]": "status",
                        "sort": "sort",
                        "include_cases_on_location": "include_cases_on_location",
                        "filter[attributes.roles.not_contains]": "roles_to_exclude",
                        "filter[attributes.is_authorized]": "is_authorized",
                        "filter[attributes.roles.contains]": "roles_to_include",
                        "filter[keyword]": "keyword",
                    }
                },
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
        },
        "POST": {
            "create_log_for_bsn_retrieved": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_log_for_bsn_retrieved",
                "from": {"request_params": {"subject_uuid": "subject_uuid"}},
                "entity_type": "person",
                "command_primary": "subject_uuid",
            }
        },
    }
