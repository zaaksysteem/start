# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class SavedSearchViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_saved_search": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_saved_search",
                "from": {
                    "request_params": {
                        "uuid": "uuid",
                    }
                },
            },
            "list_saved_search": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "list_saved_search",
                "from": {"request_params": {"filter[]": "filter"}},
                "paging": {
                    "max_results": 50,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
            "get_saved_search_labels": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_saved_search_labels",
                "from": {},
            },
        },
        "POST": {
            "create_saved_search": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_saved_search",
                "from": {"json": {"_all": True}},
            },
            "update_saved_search": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_saved_search",
                "from": {"json": {"_all": True}},
            },
            "delete_saved_search": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "delete_saved_search",
                "from": {"json": {"_all": True}},
            },
            "set_labels_for_saved_search": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_labels_for_saved_search",
                "from": {"json": {"_all": True}},
            },
        },
    }
