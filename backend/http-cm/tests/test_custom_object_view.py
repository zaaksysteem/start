# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from zsnl_case_management_http.views import _shared, custom_object


class TestSubjectViews(unittest.TestCase):
    def test_subject_views(self):
        self.assertEqual(
            custom_object.CustomObjectViews.create_link_from_entity,
            _shared.create_link_from_entity,
        )
