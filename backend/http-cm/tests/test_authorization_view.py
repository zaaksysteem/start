# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from uuid import uuid4
from zsnl_case_management_http.views import authorization


class TestAuthorizationViews:
    def test_create_link_from_entity(self):
        request = mock.MagicMock()
        entity = mock.MagicMock()
        ot_uuid = uuid4()
        type(entity).entity_type = mock.PropertyMock(return_value="department")
        type(entity).entity_id = mock.PropertyMock(return_value=ot_uuid)
        ov = authorization.AuthorizationViews(context="test", request=request)

        link = ov.create_link_from_entity(entity)

        assert link is None

        type(entity).entity_type = mock.PropertyMock(return_value="role")

        link = ov.create_link_from_entity(entity)

        assert link is None
