=head1 Zaaksysteem Service HTTP Library Quickstart

=head2 Initializing a new project

    $ cd ~/devevelopment
    $ wget https://gitlab.com/zaaksysteem/zaaksysteem-service-http-library/blob/master/bin/ml_service_http_init.pl
    $ chmod a+x ml_service_http_init.pl
    $ ./ml_service_http_init.pl my_service_name <service_library_container_tag>

The above steps grabs the latest service library init tool, and initializes
a new project named C<my_service_name>.

The provided name will be used to inflect a package for the project. In the
example above, the generated package name will be
C<Zaaksysteem::MyServiceName>.

A directory C<zaaksysteem-my_service_name-service> will be created with
boilerplate for the following infrastructures;

=over 4

=item Docker

A Dockerfile will be created that can build the service. The Dockerfile will
use the provided tag as a base image for the project,
C<zaaksysteem-service-http-library:latest> for example.

The container will have its configuration at
C</etc/zaaksysteem-my_service_name-service/>

=item Docker Compose

A C<docker-compose.yml> file will be created for the project. By default, the
service is exposed on the host machine using port C<12380>.

For ease of development, the C<etc>, C<bin>, C<lib>, C<t>, and C<xt>
directories are bind-mounted in the container from the project directory.

=item Makefile.PL

A L<ExtUtils::MakeMaker>-based buildfile will be created, and requires your
name, and additional dependencies for the service before being useful.

=item Unittests

In C<t> a compiletest for the project will be created. C<xt> contains POD and
POD coverage tests for authors.

=item Gitlab CI/CD

A C<.gitlab-ci.yml> file will be created for the project. The CI/CD pipeline
contains three stages, C<test>, C<build>, and C<publish>. The test stage will
always be executed, but build and publish only for changes on the C<master>
branch of the project.

Builing and publishing use the first 8 characters of the C<HEAD> commit as
registry tag for the built images.

=item Service

The initialization script creates a stub service class, extending from
L<Zaaksysteem::Service::HTTP>. This class contains the service configuration
while running the requesthandlers. This is also the place where one would
add common code (model factories, helper functions, etc).

A stub requesthandler is also provided under
C<Zaaksysteem::MyServiceName::Service::RequestHandlers::Root>. This request
handler claims the service's root endpoint (C<http://my.service.host/>), and
responds with a L<Syzygy::Object::Types::Service> instance, consisely
describing the service.

=back
