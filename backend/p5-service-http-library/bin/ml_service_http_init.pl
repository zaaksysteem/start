#!/usr/bin/perl -w

use strict;
use v5.10;

use File::Path qw[make_path];
use DateTime;

sub die_usage {
    die sprintf('Usage: %s <service-name> <service-library-tag>', $0)
}

my ($name, $service_library_tag) = @ARGV;

unless (length $name && length $service_library_tag) {
    die_usage;
}

my $package_name = join '', map { ucfirst } split m[_], $name;
my $name_slug = join('-', split m[::], lc($name));
my $version = 'v0.0.1';
my $project = sprintf('zaaksysteem-%s-service', $name_slug);
my $libroot = join('/', $project, 'lib', 'Zaaksysteem', split(m[::], $package_name));
my $license_block = <<"EOL";
__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::${package_name} uses the EUPL license, for more information please have a look at the C<LICENSE> file.
EOL

sub new_file {
    my $path = shift;
    print STDOUT "Creating '$path'... ";
    open my $fh, '>', $path or die sprintf('Could not open "%s" for writing', $path);
    print $fh shift;
    close $fh;
    print STDOUT "done\n";
}

sub new_pm {
    my $module = shift;
    my $header = shift // '';
    my $content = shift // "# Code here\n";

    $header = "\n\n$header" if length $header;
    $content = "\n\n$content";

    my @ns_parts = split m[::], $module;
    my $filename = pop @ns_parts;

    my $path = join('/', $libroot, @ns_parts);

    unless (-d $path) {
        system "mkdir -p $path";
    }

    my $file = sprintf('%s/%s.pm', $path, $filename);

    new_file($file, <<"EOS");
package Zaaksysteem::${package_name}::${module};

use Moose;${header}
=head1 NAME

Zaaksysteem::${package_name}::${module} - One-line description here

=head1 DESCRIPTION

Description here

=head1 SYNOPSIS

    use Zaaksysteem::${package_name}::${module};

    # Example usage here

=cut

use BTTW::Tools;${content}
__PACKAGE__->meta->make_immutable;

${license_block}
EOS
}

if (-e $project) {
    die sprintf('Project "%s" already exists', $project);
}

say "Creating project '$name' in '$project'";
make_path((map { "$project/$_" } qw[bin etc t xt]), $libroot);

system "git init $project";
system "git --git-dir=$project/.git remote add origin git\@gitlab.com:zaaksysteem/$project";

new_file(sprintf('%s/LICENSE', $project), <<"EOS");
COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the
CONTRIBUTORS file.

The complete Zaaksysteem distribution is subject to the EUPL, Version
1.1 or - as soon they will be approved by the European Commission -
subsequent versions of the EUPL (the "Licence"); you may not use this
file except in compliance with the License. You may obtain a copy of the
License at <http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
License for the specific language governing rights and limitations under
the License.
EOS

new_file(sprintf('%s/CONTRIBUTORS', $project), <<"EOS");
Your Name Here <name\@mintlab.nl>
EOS

new_file(sprintf('%s/CHANGELOG', $project), sprintf(<<"EOS", DateTime->today->ymd));
0.0.1 %s

    * Project started
EOS

new_file(sprintf('%s/Makefile.PL', $project), <<"EOS");
#!/usr/bin/perl -w

use ExtUtils::MakeMaker;

WriteMakefile(
	NAME => 'Zaaksysteem::${package_name}',
    VERSION => '${version}',
    AUTHOR => 'Your Name Here <name\@mintlab.nl>',
    ABSTRACT => 'Zaaksysteem ${package_name} (micro)service',
    TEST_REQUIRES => {
        'Test::More' => 0,
        'Test::Class::Moose' => 0,
        'Test::Compile' => 0,
        'Test::Pod' => 0,
        'Test::Pod::Coverage' => 0
    },
    PREREQ_PM => {
    }
);

${license_block}
EOS

new_file(sprintf('%s/Dockerfile', $project), <<"EOS");
FROM registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:${service_library_tag}

ENV SERVICE_HOME /opt/${project}

WORKDIR \$SERVICE_HOME

ENV ZSVC_LOG4PERL_CONFIG=/etc/${project}/log4perl.conf \\
    ZSVC_SERVICE_CONFIG=/etc/${project}/${project}.conf

COPY Makefile.PL ./
COPY bin ./bin
COPY etc /etc/${project}
COPY lib ./lib
COPY t ./t
COPY xt ./xt

RUN cpanm --installdeps . && rm -rf ~/.cpanm

CMD starman \$SERVICE_HOME/bin/${project}.psgi

EOS

new_file(sprintf('%s/docker-compose.yml', $project), <<"EOS");

networks:
    servicenet:

services:
    ${name_slug}:
        build:
            context: "."
            dockerfile: "Dockerfile"
        networks:
            - "servicenet"
        ports:
            - target: 5000
              published: 12380
              protocol: tcp
              mode: host
        volumes:
            - "./etc:/etc/${project}"
            - "./bin:/opt/${project}/bin"
            - "./lib:/opt/${project}/lib"
            - "./t:/opt/${project}/t"
            - "./xt:/opt/${project}/xt"
EOS

new_file(sprintf('%s/.gitlab-ci.yml', $project), <<"EOS");
image: registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:${service_library_tag}

stages:
    - test
    - build
    - publish

test:
    script:
        - "cpanm --installdeps ."
        - "perl Makefile.PL"
        - "make test"

build:
    image: docker:latest
    only:
        - master
    script:
        - "docker login -u gitlab-ci-token -p \${CI_JOB_TOKEN} \${CI_REGISTRY}"
        - "docker build -t \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_SHA:0:8} ."

publish:
    image: docker:latest
    only:
        - master
    script:
        - "docker login -u gitlab-ci-token -p \${CI_JOB_TOKEN} \${CI_REGISTRY}"
        - "docker push \${CI_REGISTRY_IMAGE}:\${CI_COMMIT_SHA:0:8}"
EOS

new_file(sprintf('%s/.gitignore', $project), <<"EOS");
*.tmp
*.bak
*.swp
*.swo
*.un~

etc/*.conf
EOS

new_file(sprintf('%s/etc/%s.conf.dist', $project, $project), <<"EOS");
<Zaaksysteem::Service::HTTP>
    request_handler_namespace = Zaaksysteem::${package_name}::Service::RequestHandlers
</Zaaksysteem::Service::HTTP>
EOS

new_file(sprintf('%s/etc/log4perl.conf.dist', $project), <<"EOS");
log4perl.rootLogger = TRACE, Screen
log4perl.appender.Screen = Log::Log4perl::Appender::ScreenColoredLevels
log4perl.appender.Screen.layout = Log::Log4perl::Layout::PatternLayout
log4perl.appender.Screen.layout.ConversionPattern = -- %d{ISO8601} %C+%L:%n   %m{indent=3}%n
EOS

new_file(sprintf('%s/bin/%s.psgi', $project, $project), <<"EOS");
#!/usr/bin/perl -w

use strict;

use FindBin;
use lib "\$FindBin::Bin/../lib";

use Zaaksysteem::${package_name}::Service;

my \%args;

if (exists \$ENV{ ZSVC_LOG4PERL_CONFIG }) {
    \$args{ log4perl_config_file } = \$ENV{ ZSVC_LOG4PERL_CONFIG };
}

if (exists \$ENV{ ZSVC_SERVICE_CONFIG }) {
    \$args{ service_config_file } = \$ENV{ ZSVC_SERVICE_CONFIG };
}

Zaaksysteem::${package_name}::Service->new(%args)->to_app;

${license_block}
EOS

new_file(sprintf('%s/t/00-package.t', $project), <<"EOS");
use Test::More;
use Test::Compile;

Test::Compile->new->all_files_ok;

done_testing;

${license_block}
EOS

new_file(sprintf('%s/xt/00-pod.t', $project), <<"EOS");
use Test::More;
use Test::Pod;

all_pod_files_ok;

done_testing;

${license_block}
EOS

new_file(sprintf('%s/xt/01-pod_coverage.t', $project), <<"EOS");
use Test::More;
use Test::Pod::Coverage;

all_pod_coverage_ok({ trustme => [qw[BUILD]] });

done_testing;

${license_block}
EOS

new_pm('Service', <<"EOS");
use version; our \$VERSION = version->declare('${version}');

extends 'Zaaksysteem::Service::HTTP';
EOS

new_pm('Service::RequestHandlers::Root', <<"EOH", <<"EOS");
with qw[
    Zaaksysteem::Service::HTTP::RequestHandler
    MooseX::Log::Log4perl
];
EOH
use Syzygy::Object::Model;
use URI;
use Zaaksysteem::${package_name}::Service;

=head1 METHODS

=head2 name

Implements the name interface required by
L<Zaaksysteem::Service::HTTP::RequestHandler>.

Returns the empty string, this handler attaches to the root of the
application.

=cut

sub name { '' }

=head2 dispatch

Implements the dispatcher for the root handler. Returns a L<Plack::Response>
with metadata about the service.

=cut

sub dispatch {
    my (\$self, \$request, \$response) = \@_;

    return Syzygy::Object::Model->new_object(service => {
        name => 'Zaaksysteem ${package_name} service',
        repository => URI->new('https://gitlab.com/zaaksysteem/${project}'),
        version => "\$Zaaksysteem::${package_name}::Service::VERSION"
    });
}
EOS

say "Copying default configurations";
system "cp $project/etc/log4perl.conf.dist $project/etc/log4perl.conf";
system "cp $project/etc/$project.conf.dist $project/etc/$project.conf";

system "docker-compose -f ${project}/docker-compose.yml build ${name_slug}";
say "Building ${project}";

1;
