# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from unittest import TestCase, mock
from uuid import uuid4
from zsnl_document_processing_consumer.handlers import (
    DOCUMENT_PREFIX,
    SetDocumentSearchTermsRateLimited,
)


class TestCreateDocumentPreviewOnDocumentEventHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = SetDocumentSearchTermsRateLimited(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [
                DOCUMENT_PREFIX + "SearchIndexSetDelayed",
            ],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(
            name="Event",
            routing_key="zsnl.v2.zsnl_domains_document.Document.SearchIndexSetDelayed",
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().set_search_terms_for_document_delayed.assert_called_once_with(
            document_uuid=mock_event.entity_id,
        )
