# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "1.0.11"

import logging
import minty
import minty_pyramid
import os
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import DatabaseTransactionMiddleware
from zsnl_domains import communication
from zsnl_perl_migration import LegacyEventBroadcastMiddleware
from zsnl_pyramid import platform_key

ZS_COMPONENT = "zsnl_communication_http"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    return record


def main(*args, **kwargs):
    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    loader = minty_pyramid.Engine(
        domains=[communication],
        command_wrapper_middleware=[
            DatabaseTransactionMiddleware("database"),
            AmqpPublisherMiddleware(
                publisher_name="communication", infrastructure_name="amqp"
            ),
            LegacyEventBroadcastMiddleware(
                amqp_infra_name="amqp",
                # When a file is attached to an email message in communication tab,
                # we do a virus scanning on file right away.
                interesting_document_events=[
                    ("zsnl_domains.communication", "AttachedToMessage"),
                ],
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    # Enable platform key access to this service
    config.include(platform_key)

    routes.add_routes(config)
    return loader.main()
