# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest
from unittest import mock
from zsnl_consumer_notification import consumers


class TestConsumerNotification(unittest.TestCase):
    def test_consumer_routing(self):
        consumer = consumers.NotificationConsumer.__new__(
            consumers.NotificationConsumer
        )
        consumer.cqrs = mock.Mock()

        consumer._register_routing()

        self.assertEqual(len(consumer._known_handlers), 2)
