# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import handlers
from minty_amqp.consumer import BaseConsumer


class NotificationConsumer(BaseConsumer):
    def _register_routing(self):
        handler_classes = handlers.NotificationBaseHandler.__subclasses__()
        self._known_handlers: list[handlers.NotificationBaseHandler] = [
            handler_class(self.cqrs) for handler_class in handler_classes
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
