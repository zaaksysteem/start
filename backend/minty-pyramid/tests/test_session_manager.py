# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import codecs
import jose.exceptions
import json
import minty
import pytest
import requests.exceptions
import time
from minty_pyramid.session_manager import (
    ANONYMOUS_USER_UUID,
    SessionDataNotFoundError,
    SessionNotFoundError,
    SessionRetriever,
    get_logged_in_user,
    handle_oauth2,
    handle_session_data,
    protected_route,
    redis_from_config,
    session_manager_factory,
)
from pyramid.httpexceptions import (
    HTTPForbidden,
    HTTPInternalServerError,
    HTTPUnauthorized,
)
from unittest import mock
from uuid import uuid4


class MockInfra(minty.Base):
    def __init__(self):
        self.infra = {}

    def register_infrastructure(self, name, infrastructure):
        self.infra[name] = infrastructure

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]()


class TestSessionReader:
    """Test the session reader."""

    def test_retrieve(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() + 3600),
        }
        retriever = SessionRetriever(mock_redis)  # type:ignore

        session_data = retriever.retrieve("31337")

        assert session_data["username"] == "piet"

    def test_expired_session(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() - 3600),
        }
        retriever = SessionRetriever(mock_redis)  # type:ignore

        with pytest.raises(SessionNotFoundError):
            retriever.retrieve("31337")

    def test_nonexistent_session(self):
        mock_redis = {
            "json:session:31337": codecs.encode(
                b'{"username":"piet"}', "base64"
            ),
            "expires:31337": str(time.time() - 3600),
        }
        retriever = SessionRetriever(mock_redis)  # type:ignore

        with pytest.raises(SessionNotFoundError):
            retriever.retrieve("31338")

    def test_empty_session(self):
        mock_redis = {
            "json:session:31337": None,
            "expires:31337": str(time.time() + 3600),
        }
        retriever = SessionRetriever(mock_redis)  # type:ignore

        with pytest.raises(SessionDataNotFoundError):
            retriever.retrieve("31337")

    @mock.patch("minty_pyramid.session_manager.redis_from_config")
    def test_session_manager_factory(self, mock_redis_from_conf):
        mock_redis_from_conf.redis = "redis_from_config"
        mock_redis_from_conf.return_value = "redis"
        mock_infra_factory = MockInfra()
        m_retr = session_manager_factory(mock_infra_factory)
        assert m_retr.redis == "redis"

    @mock.patch("minty_pyramid.session_manager.StrictRedis")
    def test_redis_from_config(self, mock_redis):
        conf = {"redis": {"session": {"host": "localhost", "port": "1234"}}}
        mock_redis.return_value = "redisClient"
        mocked = redis_from_config(conf)
        assert mocked == "redisClient"
        mock_redis.assert_called_with(**{"host": "localhost", "port": "1234"})


class TestHandleSessionData:
    def test_handle_session_data_normal_user(self):
        logger = mock.MagicMock()
        uuid = "1a28af90-d3aa-11e9-8661-d39e5248b7f7"
        data = {
            "__user": json.dumps(
                {"subject_uuid": uuid, "permissions": {"gebruiker": True}}
            )
        }

        user_info = handle_session_data(data, logger)

        assert user_info.user_uuid == uuid
        assert user_info.permissions == {"gebruiker": True}

    def test_handle_session_data_pip_user(self):
        logger = mock.MagicMock()
        uuid = "1a28af90-d3aa-11e9-8661-d39e5248b7f7"

        data = {"pip": {"user_uuid": uuid}}

        user_info = handle_session_data(data, logger)

        assert user_info.user_uuid == uuid
        assert user_info.permissions == {"pip_user": True}

    def test_handle_session_data_keyerror(self):
        logger = mock.MagicMock()

        data = {}

        with pytest.raises(HTTPUnauthorized):
            handle_session_data(data, logger)

    def test_handle_session_data_anonymous_user(self):
        logger = mock.MagicMock()

        data = {"some_key": "not empty dict"}

        session_data = handle_session_data(data, logger)
        assert session_data.user_uuid == ANONYMOUS_USER_UUID
        assert session_data.permissions == {
            "anonymous": True,
            "pip_user": True,
        }

    def test_handle_session_no_session_data(self):
        logger = mock.MagicMock()

        data = {}

        with pytest.raises(HTTPUnauthorized):
            handle_session_data(data, logger)


class TestProtectedRoutes:
    def setup_method(self):
        @protected_route("admin")
        def m_view(request, user_info):
            assert user_info.user_uuid == 1234
            assert user_info.permissions == {"admin": True}
            return "return_value"

        @protected_route("nonexistent")
        def m_view_nonexistent_permission(request, user_info):
            pass

        self.mock_request = mock.MagicMock(authorization=None)
        self.mock_view = m_view
        self.mock_view_nonexistent_permission = m_view_nonexistent_permission

    def test_protected_routes(self):
        """Happy path: `UserInfo` is asserted in m_view function."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        self.mock_request.retrieve_session.return_value = {
            "__user": json.dumps(
                {"subject_uuid": 1234, "permissions": {"admin": True}}
            )
        }
        return_val = self.mock_view(request=self.mock_request)
        assert return_val == "return_value"

    def test_protected_routes_with_request_as_arg_instead_of_kwarg(self):
        """Happy path: `UserInfo` is asserted in m_view function."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        self.mock_request.retrieve_session.return_value = {
            "__user": json.dumps(
                {"subject_uuid": 1234, "permissions": {"admin": True}}
            )
        }
        return_val = self.mock_view(self.mock_request)
        assert return_val == "return_value"

    def test_protected_routes_not_allowed(self):
        """Happy path: `UserInfo` is asserted in m_view function."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        self.mock_request.retrieve_session.return_value = {
            "__user": json.dumps(
                {"subject_uuid": 1234, "permissions": {"admin": False}}
            )
        }
        with pytest.raises(HTTPForbidden):
            self.mock_view(request=self.mock_request)

    def test_protected_routes_no_session(self):
        """No session KeyError raises `HTTPUnauthorized`."""
        self.mock_request.cookies = {}
        self.mock_request.retrieve_session.side_effect = KeyError
        with pytest.raises(HTTPUnauthorized):
            self.mock_view(request=self.mock_request)

    def test_protected_routes_session_not_valid(self):
        """No session KeyError raises `HTTPUnauthorized`."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        with pytest.raises(HTTPUnauthorized):
            self.mock_request.retrieve_session.side_effect = (
                SessionNotFoundError
            )
            self.mock_view(request=self.mock_request)

    def test_protected_routes_json_decode(self):
        """JSONdecodeError raises `HTTPUnauthorized`."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        with pytest.raises(HTTPUnauthorized):
            self.mock_request.retrieve_session.return_value = {
                "__user": {"subject_uuid": 1234, "permissions": "admin"}
            }
            self.mock_view(request=self.mock_request)

    def test_protected_routes_user_info_incomplete(self):
        """No `__user_uuid` or `permissions` KeyError raises `HTTPUnauthorized`."""
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        with pytest.raises(HTTPUnauthorized):
            self.mock_request.retrieve_session.return_value = {
                "__user": {"us": 1234, "per": "admin"}
            }
            self.mock_view(request=self.mock_request)

    def test_protected_routes_invalid_permission(self):
        self.mock_request.cookies = {"zaaksysteem_session": "session"}
        self.mock_request.retrieve_session.return_value = {
            "__user": json.dumps(
                {"subject_uuid": 1234, "permissions": {"admin": True}}
            )
        }

        with pytest.raises(HTTPForbidden):
            self.mock_view_nonexistent_permission(request=self.mock_request)


def test_get_logged_in_user():
    user_uuid = uuid4()
    permissions = {"pip_user": False, "gebruiker": True, "admin": True}

    mock_request = mock.MagicMock()
    mock_logger = mock.MagicMock()

    # "assert_" method are special for magicmock
    mock_request.assert_platform_key = mock.MagicMock()
    mock_request.get_platform_user_uuid.return_value = (user_uuid, permissions)

    user_info = get_logged_in_user(request=mock_request, logger=mock_logger)

    assert user_info.user_uuid == user_uuid
    assert user_info.permissions["pip_user"] is False
    assert user_info.permissions["gebruiker"]
    assert user_info.permissions.get("admin")
    assert user_info.permissions["admin"]

    mock_logger.info.assert_called_once_with("Platform key used successfully")


@mock.patch("minty_pyramid.session_manager.handle_oauth2")
def test_get_logged_in_user_oauth2(mock_handle_oauth2):
    user_uuid = uuid4()
    mock_handle_oauth2.return_value = user_uuid

    mock_logger = mock.MagicMock()
    mock_request = mock.Mock()
    mock_request.assert_platform_key = mock.Mock()
    mock_request.assert_platform_key.side_effect = KeyError

    mock_request.authorization = "Bearer aG9p"

    user_info = get_logged_in_user(request=mock_request, logger=mock_logger)

    assert user_info == user_uuid
    mock_handle_oauth2.assert_called_once_with(mock_request)
    mock_request.retrieve_session.assert_not_called()

    mock_logger.debug.assert_called_once_with(
        "Verifying Authorization header (OAuth2)"
    )


def test_handle_oauth2_not_configured():
    mock_request = mock.Mock(configuration={})

    with pytest.raises(HTTPUnauthorized):
        handle_oauth2(mock_request)


@mock.patch("minty_pyramid.oidc.parse_token")
def test_handle_oauth2_http_error(mock_parse_token):
    mock_parse_token.side_effect = requests.exceptions.HTTPError

    instance_uuid = str(uuid4())

    mock_redis = mock.Mock(name="redis")
    mock_request = mock.Mock(
        name="request",
        configuration={
            "oidc_client_id": "test_id",
            "oidc_endpoint_config": "https://example.com/oidc",
            "instance_uuid": instance_uuid,
        },
    )

    mock_request.authorization.params = "access_token"
    mock_request.infrastructure_factory.get_infrastructure.return_value = (
        mock_redis
    )

    with pytest.raises(HTTPInternalServerError):
        handle_oauth2(mock_request)


@mock.patch("minty_pyramid.oidc.parse_token")
def test_handle_oauth2_token_error(mock_parse_token):
    mock_parse_token.side_effect = jose.exceptions.JOSEError

    instance_uuid = str(uuid4())

    mock_redis = mock.Mock(name="redis")
    mock_request = mock.Mock(
        name="request",
        configuration={
            "oidc_client_id": "test_id",
            "oidc_endpoint_config": "https://example.com/oidc",
            "instance_uuid": instance_uuid,
        },
    )

    mock_request.authorization.params = "access_token"
    mock_request.infrastructure_factory.get_infrastructure.return_value = (
        mock_redis
    )

    with pytest.raises(HTTPUnauthorized):
        handle_oauth2(mock_request)


@mock.patch("minty_pyramid.oidc.parse_token")
def test_handle_oauth2(mock_parse_token):
    instance_uuid = str(uuid4())

    mock_redis = mock.Mock(name="redis")
    mock_request = mock.Mock(
        name="request",
        configuration={
            "oidc_client_id": "test_id",
            "oidc_endpoint_config": "https://example.com/oidc",
            "instance_uuid": instance_uuid,
        },
    )

    mock_request.authorization.params = "access_token"
    mock_request.infrastructure_factory.get_infrastructure.return_value = (
        mock_redis
    )

    user_uuid = uuid4()
    mock_parse_token.return_value = {
        "user_uuid": str(user_uuid),
        "scope": {"zs:foo", "zs:bar"},
    }

    user_info = handle_oauth2(mock_request)

    assert user_info.user_uuid == user_uuid
    assert user_info.permissions == {"foo": True, "bar": True}

    mock_parse_token.assert_called_once_with(
        access_token="access_token",
        oidc_client_id="test_id",
        oidc_endpoint_config="https://example.com/oidc",
        instance_uuid=instance_uuid,
        cache=mock_redis,
    )


def test_get_logged_in_user_with_session_invitation():
    user_uuid = uuid4()
    permissions = {"pip_user": False, "gebruiker": True, "admin": True}

    mock_logger = mock.MagicMock()
    mock_request = mock.MagicMock()
    mock_request.assert_platform_key = mock.MagicMock()
    mock_request.assert_platform_key.side_effect = KeyError

    mock_request.assert_session_invitation = mock.MagicMock()
    user_info = {"user_uuid": user_uuid, "permissions": permissions}
    mock_request.get_session_invitation_user_info.return_value = user_info

    user_info = get_logged_in_user(request=mock_request, logger=mock_logger)

    assert user_info.user_uuid == user_uuid
    assert user_info.permissions["pip_user"] is False
    assert user_info.permissions["gebruiker"]
    assert user_info.permissions.get("admin")
    assert user_info.permissions["admin"]

    mock_logger.info.assert_called_once_with(
        "Session invitation used successfully"
    )
