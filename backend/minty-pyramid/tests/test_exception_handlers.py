# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pydantic.v1
from jsonschema.exceptions import ValidationError
from minty.exceptions import Conflict, CQRSException, Forbidden, NotFound
from minty_pyramid.exception_handlers import (
    exception_cqrs_catch_all,
    exception_cqrs_conflict,
    exception_cqrs_forbidden,
    exception_cqrs_not_found,
    exception_cqrs_timeout,
    exception_cqrs_validation,
    exception_cqrs_validation_pydantic,
    serialize_cqrs_exception,
)


class MockResponse:
    def __init__(self):
        self.status = 0


class MockRequest:
    def __init__(self):
        self.response = MockResponse()


class TestExceptionHandlers:
    def test_exception_cqrs_validation(self):
        exc = ValidationError(
            [
                {
                    "context": "format",
                    "message": "'something' is not a 'uuid'",
                    "cause": "badly formed hexadecimal UUID string",
                    "property": "/some_argument",
                },
                {
                    "context": "type",
                    "message": "'number' is not of type 'number'",
                    "cause": None,
                    "property": "/object_argument/sub_argument",
                },
            ]
        )
        request = MockRequest()

        resp = exception_cqrs_validation(exc=exc, request=request)

        assert request.response.status == 400
        assert resp == {
            "errors": [
                {
                    "title": "'something' is not a 'uuid'",
                    "detail": "badly formed hexadecimal UUID string",
                    "source": "/some_argument",
                    "code": "validation/format",
                },
                {
                    "title": "'number' is not of type 'number'",
                    "detail": None,
                    "source": "/object_argument/sub_argument",
                    "code": "validation/type",
                },
            ]
        }

    def test_exception_cqrs_catch_all(self):
        exc = Exception("catch_all")
        request = MockRequest()

        resp = exception_cqrs_catch_all(exc=exc, request=request)

        assert request.response.status == 500
        assert resp == {
            "errors": [
                {
                    "title": "The server encountered an internal error and was unable to complete your request."
                }
            ]
        }

    def test_serialize_cqrs_exception(self):
        exc = CQRSException("error_message", "error_code")

        res = serialize_cqrs_exception(exc)
        assert res == [{"title": "error_message", "code": "error_code"}]

    def test_serialize_cqrs_exception_no_error_code(self):
        exc = CQRSException("error_message")

        res = serialize_cqrs_exception(exc)
        assert res == [{"title": "error_message"}]

    def test_exception_cqrs_conflict(self):
        exc = Conflict("conflict", "error_code")
        request = MockRequest()

        resp = exception_cqrs_conflict(exc=exc, request=request)

        assert request.response.status == 409
        assert resp == {
            "errors": [{"title": "conflict", "code": "error_code"}]
        }

    def test_exception_cqrs_forbidden(self):
        exc = Forbidden("forbidden", "error_code")
        request = MockRequest()

        resp = exception_cqrs_forbidden(exc=exc, request=request)

        assert request.response.status == 403
        assert resp == {
            "errors": [{"title": "forbidden", "code": "error_code"}]
        }

    def test_exception_cqrs_not_found(self):
        exc = NotFound("something isn't found", "error_code")
        request = MockRequest()

        resp = exception_cqrs_not_found(exc=exc, request=request)
        assert request.response.status == 404
        assert resp == {
            "errors": [
                {"title": "something isn't found", "code": "error_code"}
            ]
        }

    def test_exception_cqrs_timeout(self):
        exc = TimeoutError("connection timeout", "error_code")
        request = MockRequest()

        resp = exception_cqrs_timeout(exc=exc, request=request)
        assert request.response.status == 504
        assert resp == {
            "errors": [{"title": "connection timeout", "code": "error_code"}]
        }

    def test_exception_cqrs_validation_pydantic(self):
        exc = pydantic.v1.ValidationError(
            model=pydantic.v1.BaseModel,
            errors=[
                pydantic.v1.error_wrappers.ErrorWrapper(
                    exc=ValidationError(message="field required"), loc=("bsn",)
                ),
            ],
        )
        request = MockRequest()

        resp = exception_cqrs_validation_pydantic(exc=exc, request=request)

        assert request.response.status == 400
        assert resp["errors"][0]["title"] == "field required"
        assert resp["errors"][0]["source"] == ("bsn",)
        assert resp["errors"][0]["code"] == "validation/value_error.validation"
