# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import (
    CreateDocumentArchiveRequestedHandler,
    CreateDocumentPreviewOnDocumentEventHandler,
    CreateDocumentPreviewOnVirusscanHandler,
    CreateDocumentThumbnailOnDocumentEventHandler,
    CreateDocumentThumbnailOnVirusscanHandler,
    CreatePDFForCaseRegistration,
    SetDocumentSearchTermsOnCreateHandler,
    SetDocumentSearchTermsOnVirusscanHandler,
)
from minty_amqp.consumer import BaseConsumer, BaseHandler


class DocumentsEventsConsumer(BaseConsumer):
    def _register_routing(self):
        # CQRS object always exists in this consumer; if it doesn't, that's a bug
        assert self.cqrs

        self._known_handlers: list[BaseHandler] = [
            CreateDocumentArchiveRequestedHandler(self.cqrs),
            CreateDocumentPreviewOnDocumentEventHandler(self.cqrs),
            CreateDocumentPreviewOnVirusscanHandler(self.cqrs),
            CreateDocumentThumbnailOnDocumentEventHandler(self.cqrs),
            CreateDocumentThumbnailOnVirusscanHandler(self.cqrs),
            CreatePDFForCaseRegistration(self.cqrs),
            SetDocumentSearchTermsOnCreateHandler(self.cqrs),
            SetDocumentSearchTermsOnVirusscanHandler(self.cqrs),
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
