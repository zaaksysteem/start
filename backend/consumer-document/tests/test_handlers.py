# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs.events import Event
from unittest import TestCase, mock
from uuid import uuid4
from zsnl_documents_events_consumer.constants import (
    DOCUMENT_ARCHIVE_REQUESTED_PREFIX,
    DOCUMENT_PREFIX,
    LEGACY_CASE_PREFIX,
    LEGACY_DOCUMENT_PREFIX,
    LEGACY_FILE_PREFIX,
)
from zsnl_documents_events_consumer.handlers import (
    CreateDocumentArchiveRequestedHandler,
    CreateDocumentPreviewOnDocumentEventHandler,
    CreateDocumentPreviewOnVirusscanHandler,
    CreateDocumentThumbnailOnDocumentEventHandler,
    CreateDocumentThumbnailOnVirusscanHandler,
    CreatePDFForCaseRegistration,
    SetDocumentSearchTermsOnCreateHandler,
    SetDocumentSearchTermsOnVirusscanHandler,
)


class DocumentArchiveHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreateDocumentArchiveRequestedHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [
                DOCUMENT_ARCHIVE_REQUESTED_PREFIX + "DocumentArchiveRequested",
            ],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event.entity_id = uuid4()
        case_uuid = uuid4()
        docu_uuid = uuid4()
        docu_2_uuid = uuid4()
        dir_uuid = uuid4()
        export_file_uuid = uuid4()

        mock_event.format_changes.return_value = {
            "case_uuid": case_uuid,
            "zip_all": True,
            "directory_uuids": [docu_uuid, docu_2_uuid],
            "document_uuids": [dir_uuid],
            "export_file_uuid": export_file_uuid,
        }

        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_document_archive.assert_called_once_with(
            case_uuid=case_uuid,
            zip_all=True,
            directory_uuids=[docu_uuid, docu_2_uuid],
            document_uuids=[dir_uuid],
            export_file_uuid=export_file_uuid,
        )


class TestCreateDocumentPreviewOnDocumentEventHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreateDocumentPreviewOnDocumentEventHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [
                DOCUMENT_PREFIX + "DocumentCreated",
                DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
                DOCUMENT_PREFIX + "PreviewRequested",
                LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
                LEGACY_DOCUMENT_PREFIX + "PreviewRequested",
            ],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(
            name="Event",
            routing_key="zsnl.v2.legacy.Document.PreviewRequested",
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_preview_for_document.assert_called_once_with(
            document_uuid=mock_event.entity_id,
            is_manual_request=True,
        )

    def test_handle_not_manual(self) -> None:
        mock_event = mock.Mock(
            name="Event",
            routing_key="zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_preview_for_document.assert_called_once_with(
            document_uuid=mock_event.entity_id,
            is_manual_request=False,
        )


class TestCreateDocumentPreviewOnVirusscanHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreateDocumentPreviewOnVirusscanHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [LEGACY_FILE_PREFIX + "VirusScanFinished"],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_preview_for_documents_for_file.assert_called_once_with(
            file_uuid=mock_event.entity_id
        )


class TestCreateDocumentThumbnailOnPreviewCreatedHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreateDocumentThumbnailOnDocumentEventHandler(
            self.cqrs
        )

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [
                DOCUMENT_PREFIX + "DocumentCreated",
                DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
                DOCUMENT_PREFIX + "PreviewCreated",
                DOCUMENT_PREFIX + "ThumbnailRequested",
                LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
                LEGACY_DOCUMENT_PREFIX + "ThumbnailRequested",
            ],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(
            name="Event",
            routing_key="zsnl.v2.legacy.Document.ThumbnailRequested",
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_thumbnail_for_document.assert_called_once_with(
            document_uuid=mock_event.entity_id,
            is_manual_request=True,
        )

    def test_handle_manual(self) -> None:
        mock_event = mock.Mock(
            name="Event",
            routing_key="zsnl.v2.zsnl_domains_document.Document.DocumentCreated",
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_thumbnail_for_document.assert_called_once_with(
            document_uuid=mock_event.entity_id,
            is_manual_request=False,
        )


class TestCreateDocumentThumbnailOnVirusscanHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreateDocumentThumbnailOnVirusscanHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [LEGACY_FILE_PREFIX + "VirusScanFinished"],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_thumbnail_for_documents_for_file.assert_called_once_with(
            file_uuid=mock_event.entity_id
        )


class TestSetDocumentSearchTermsOnCreateHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = SetDocumentSearchTermsOnCreateHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [
                DOCUMENT_PREFIX + "DocumentCreated",
                DOCUMENT_PREFIX + "DocumentFromAttachmentCreated",
                LEGACY_DOCUMENT_PREFIX + "DocumentCreated",
            ],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().set_search_terms_for_document.assert_called_once_with(
            document_uuid=mock_event.entity_id
        )


class TestSetDocumentSearchTermsOnVirusscanHandlerHandler(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = SetDocumentSearchTermsOnVirusscanHandler(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [LEGACY_FILE_PREFIX + "VirusScanFinished"],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().set_search_terms_for_documents_for_file.assert_called_once_with(
            file_uuid=mock_event.entity_id
        )


class TestCreatePDFForCaseRegistration(TestCase):
    def setUp(self) -> None:
        self.cqrs = mock.Mock()
        self.instance = CreatePDFForCaseRegistration(self.cqrs)

        return super().setUp()

    def test_routing_keys(self) -> None:
        self.assertEqual(
            self.instance.routing_keys,
            [LEGACY_CASE_PREFIX + "CaseCreated"],
            "Routing keys are correct",
        )

    def test_handle(self) -> None:
        mock_event = mock.Mock(name="Event")
        mock_event = Event(
            uuid=uuid4(),
            created_date=datetime.now(),
            correlation_id=uuid4(),
            domain="test.zaaksysteem.nl",
            context="",
            user_uuid=uuid4(),
            entity_type="event",
            entity_id=str(uuid4()),
            event_name="CreatePDFForCaseRegistration",
            changes=[
                {"key": "attributes", "old_value": None, "new_value": {}},
                {
                    "key": "hostname",
                    "old_value": None,
                    "new_value": "test.zaaksysteem.nl",
                },
                {"key": "origin", "old_value": None, "new_value": "pip"},
            ],
            entity_data={},
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        self.cqrs.get_command_instance().create_pdf_for_case_registration.assert_called_once_with(
            case_uuid=mock_event.entity_id,
            fields=mock_event.format_changes(),
        )

    def test_handle_no_host_or_origin(self) -> None:
        mock_event = Event(
            uuid=uuid4(),
            created_date=datetime.now(),
            correlation_id=uuid4(),
            domain="test.zaaksysteem.nl",
            context="",
            user_uuid=uuid4(),
            entity_type="event",
            entity_id=str(uuid4()),
            event_name="CreatePDFForCaseRegistration",
            changes=[
                {"key": "attributes", "old_value": None, "new_value": {}},
                {"key": "case_number", "old_value": None, "new_value": 1337},
            ],
            entity_data={},
        )
        mock_event.entity_id = uuid4()
        self.instance.handle(mock_event)

        assert not self.cqrs.get_command_instance().create_pdf_for_case_registration.called
